
// new relic monitoring -- Must be the first line of the app.js
require('newrelic');

/**
 * Module dependencies.
 */
var express = require("express"),
    engine = require("ejs-locals"),
    mongoose = require("mongoose"),
    context = require('../models/context'),
    middleware = require('../routers/middleware/middleware'),
    util = require("util"),
    http = require("http"),
    path = require("path"),
    //logger = require('winston'),
    socketio = require('socket.io'),
    fs = require('fs'),
    async = require('async'),
    _ = require('lodash'),
    config = require('../config')
    logger = config.logger;

    logger.silly('node env: ' + process.env.NODE_ENV);

mongoose.connect(config.mongodb.url());
var app = express();

logger.info('starting application...');
app.engine("ejs", engine);

app.set("port", process.env.PORT || 7450);
app.set("views", "../views");
app.set("view engine", "ejs");
//app.use(logger);
app.use(require('body-parser')());
app.use(require('method-override')());
app.use(require('cookie-parser')("your secret here"));
app.use(require('express-session')());
app.use(require('serve-favicon')('../public/images/favicon.ico'));
app.use(express.static(path.join("..", "public")));
app.use('/bower_components', express.static(path.join('..', 'bower_components')));
app.use(require('errorhandler')());

app.use(middleware.version);
app.use(middleware.activityLog);

//set up app param for district
app.param('district', function(req, res, next, districtShortCode) {
    //todo: verify valid session
    context.District.getCurrentByShortCode(districtShortCode, function(err, district) {
        //make sure district exists
        if (!district) {
            res.locals.districtCode = null;
            req.notFoundDistrict = districtShortCode;
            req.district = null;
            res.redirect('/error/districtNotFound');
        } else {
            req.district = district;
            res.locals.districtCode = districtShortCode;
            res.locals.multipleDistrictAccess = req.session.permissionsProfile.districts.length > 1 || res.locals.isAdministrator;

            //ensure user has permissions to the district
            if (!_.contains(_.keys(req.session.permissionsProfile.districts), districtShortCode)) {
                res.redirect('/error/permissionDenied');
            } else {
                //user has permissions and district exists, carry on
                res.locals.districtName = req.session.permissionsProfile.districts[districtShortCode].name;
                next();
            }
        }
    });
});

var server = http.createServer(app)
    .listen(app.get("port"), function() {
        logger.info("Express server listening on port " + app.get("port"));
    });

//socket setup
var io = socketio.listen(server);
io.set('store', new socketio.RedisStore);
require('../socketClients/import')(io);
require('../socketClients/export')(io);
require('../socketClients/upload')(io);
require('../socketClients/template')(io);
require('../socketClients/commit')(io);
require('../socketClients/debug')(io);
require('../socketClients/billing')(io);
require('../socketClients/remittance')(io);
//controller setup
var homeController = require('../controllers/home');
app.get('/login', homeController.getLogin);
app.get('/logout', homeController.getLogout);
app.post('/login', homeController.postLogin);

//password reset
var forgotPasswordController = require('../controllers/forgotPassword');
app.get('/resetPassword', forgotPasswordController.get);
app.get('/resetPassword/:slug/claim', forgotPasswordController.getConfirmation);
app.post('/resetPassword', forgotPasswordController.post);
app.post('/resetPassword/:slug/claim', forgotPasswordController.postConfirmation);

app.use('/admin', require('../routers/userAdministration'));
app.use(require('../routers/import'));
app.use('/districtSelection', require('../routers/districtSelection'));
app.use('/error', require('../routers/errorHandling'));
app.use('/:district', require('../routers/district'));

app.use('/', require('../routers/root'));

//run configs?
var seed = require('../config/seed');
var tasks = [
    seed.seedUsers(function(err, user) {}),
    seed.seedAdministratorArea(function(err, aa) {}),
    seed.obviousAdministrators(function(err, oa) {})
];
async.series(tasks, function(err, r) {
    logger.silly('seed tasks complete');
});
