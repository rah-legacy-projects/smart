
// new relic monitoring -- Must be the first line of the app.js
require('newrelic');

/**
 * Module dependencies.
 */
var express = require("express"),
    engine = require("ejs-locals"),
    mongoose = require("mongoose"),
    context = require('../models/context'),
    middleware = require('../routers/middleware/middleware'),
    util = require("util"),
    http = require("http"),
    path = require("path"),
    //logger = require('winston'),
    fs = require('fs'),
    async = require('async'),
    _ = require('lodash'),
    config = require('../config')
    logger = config.logger;

mongoose.connect(config.mongodb.url());
var app = express();

app.use(middleware.version);
logger.info('starting application...');
app.engine("ejs", engine);

app.set("port", process.env.PORT || 9988);
app.set("views", "../views");
app.set("view engine", "ejs");
//app.use(logger);
app.use(require('body-parser')());
app.use(require('method-override')());
app.use(require('cookie-parser')("your secret here"));
app.use(require('express-session')());
app.use(require('serve-favicon')('../public/images/favicon.ico'));
app.use(express.static(path.join('..', "public")));
app.use('/bower_components', express.static(path.join('..', 'bower_components')));
app.use(require('errorhandler')());

var server = http.createServer(app)
    .listen(app.get("port"), function() {
    });

app.use('/logging', require('../routers/logging'));

