
// new relic monitoring -- Must be the first line of the app.js
require('newrelic');

/**
 * Module dependencies.
 */
var express = require("express"),
    engine = require("ejs-locals"),
    mongoose = require("mongoose"),
    context = require('../models/context'),
    middleware = require('../routers/middleware/middleware'),
    util = require("util"),
    http = require("http"),
    path = require("path"),
    //logger = require('winston'),
    socketio = require('socket.io'),
    fs = require('fs'),
    async = require('async'),
    _ = require('lodash'),
    config = require('../config')
    logger = config.logger;

    logger.silly('node env: ' + process.env.NODE_ENV);

mongoose.connect(config.mongodb.url());
var app = express();

logger.info('starting application...');

app.set("port", process.env.PORT || 7451);
app.use(require('body-parser')());
app.use(require('method-override')());
app.use(require('errorhandler')());

app.use(middleware.version);

var server = http.createServer(app)
    .listen(app.get("port"), function() {
        logger.info("reporting server listening on port " + app.get("port"));
    });

//controller setup
app.use('/', require('../routers/reporting'));
