var pdf = require('pdf-extract'),
    fs = require('fs'),
    path = require('path'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    logger = require('winston'),
    moment = require('moment'),
    spreadsheetImport = require('spreadsheet-import'),
    xlsxutil = spreadsheetImport.util;

var pdfExtract = function() {
    var self = this;


    //{{{ make pdf record
    self.makePdfRecord = function(linetext, index) {
        var expressions = {
            lineOne: /^(\w+)\s+(\w+)\s+(\w+)\s+(.+?)\s{3,}(\d+\s+\w+)\s+(\w+)\s{3,}(.+?)\s{3,}(\w+)\s{3,}(\w+)\s{3,}(\w+)\s{3,}(\w+)\s+(\w+)\s*$/,
            lineOneNoLetters: /^(\w+)\s+(\w+)\s+(\w+)\s+(.+?)\s{3,}(\d+\s+\w+)\s+(.+?)\s{3,}(\w+)\s{3,}(\w+)\s{3,}(\w+)\s{3,}(\w+)\s+(\w+)\s*$/,
            lineTwo: /^\s*(\w+)\s+\/\s+([0-9a-zA-Z \t]+)/,
            lineTwoNoError: /^\s*(\w+)\s*\/\s*$/,
            lineTwoPhys: /^\s*(\w+)\s+\/PHYS\:\s*\w+\s{1}\w+\s{3,}([0-9a-zA-Z \t]+)$/,
            empty: /^\s*$/,
            remitFooter: /^\s*remit\s*$/i,
            errorTranslationLine: /^\s*(\d+)\s*(.+)$/i,
            errorTranslationHeader: /^\s*error\s*code\s*/i,
            headerLines: [
                /\s*ATTN\:/i,
                /\s*TO\:/i,
                /\s*M\s*M\s*D\s*D\s*Y\s*Y/i,
                /\s*RECIPIENT\s*NUMBER\s*DATES\s*OF\s*SERVICE/i,
                /\s*DRUG\s*CODE\s*AND\s*DESCRIPTIONS/i,
                /\s*RECIPIENT\s*NAME\s*UNITS/i,
                /\s*[A-Za-z]{2}\s+[0-9]{5}/i
            ]
        }
        if (expressions.lineOne.test(linetext)) {
            var m = expressions.lineOne.exec(linetext);

            var unitsAndProcCode = null;
            if (/(\d+)\s+(\w+)/.test(m[5])) {
                unitsAndProcCode = /(\d+)\s+(\w+)/.exec(m[5]);
            } else {
                unitsAndProcCode = [];
                logger.warn('unit proc code missing: ' + linetext);
            }

            var procedure = m[7];
            var length = null;
            if (/(.+?),\s*(\d+)/.test(m[7])) {
                var p = /(.+?),\s*(\d+)/.exec(m[7]);
                procedure = p[1];
                length = p[2];
            }

            var remittanceRecord = {
                isNewLine: true,
                recipientNumber: m[1],
                recipientLastName: m[2],
                initials: m[3],
                startDateOfService: m[4].split(' ')[0],
                endDateOfService: m[4].split(' ')[1],
                units: unitsAndProcCode[1],
                procedureCode: unitsAndProcCode[2],
                procedure: procedure,
                length: length,
                amountBilled: m[8],
                amountAllowed: m[9],
                deductions: m[10],
                amountPaid: m[11],
                controlNumber: m[12],
                match: 'line one'
            };
            return remittanceRecord;
        } else if (expressions.lineOneNoLetters.test(linetext)) {

            var m = expressions.lineOneNoLetters.exec(linetext);

            var unitsAndProcCode = null;
            if (/(\d+)\s+(\w+)/.test(m[5])) {
                unitsAndProcCode = /(\d+)\s+(\w+)/.exec(m[5]);
            } else {
                unitsAndProcCode = [];
                logger.warn('unit proc code missing: ' + linetext);
            }

            var procedure = m[6];
            var length = null;
            if (/(.+?),\s*(\d+)/.test(m[6])) {
                var p = /(.+?),\s*(\d+)/.exec(m[6]);
                procedure = p[1];
                length = p[2];
            }


            var remittanceRecord = {
                isNewLine: true,
                recipientNumber: m[1],
                recipientLastName: m[2],
                initials: m[3],
                startDateOfService: m[4].split(' ')[0],
                endDateOfService: m[4].split(' ')[1],
                units: unitsAndProcCode[1],
                procedureCode: unitsAndProcCode[2],
                procedure: procedure,
                length: length,
                amountBilled: m[7],
                amountAllowed: m[8],
                deductions: m[9],
                amountPaid: m[10],
                controlNumber: m[11],
                match: 'line one'
            };
            return remittanceRecord;
        } else if (expressions.lineTwo.test(linetext)) {
            var m = expressions.lineTwo.exec(linetext);

            var errorNumbers = _.filter(m[2].split(' '), function(part) {
                return !(/^\s*$/.test(part));
            });

            var condensedErrorNumbers = null;
            if (errorNumbers.length > 0) {
                condensedErrorNumbers = errorNumbers.join(', ');
            }
            var recordLineTwo = {
                isNewLine: false,
                echoId: m[1],
                errorNumber: condensedErrorNumbers,
                match: 'line two'
            };
            return recordLineTwo;
        } else if (expressions.lineTwoNoError.test(linetext)) {
            var m = expressions.lineTwoNoError.exec(linetext);
            var recordLineTwo = {
                isNewLine: false,
                echoId: m[1],
                match: 'line two - no errors'
            };
            return recordLineTwo;
        } else if (expressions.lineTwoPhys.test(linetext)) {
            var m = expressions.lineTwoPhys.exec(linetext);

            var errorNumbers = _.filter(m[2].split(' '), function(part) {
                return !(/^\s*$/.test(part));
            });

            var condensedErrorNumbers = null;
            if (errorNumbers.length > 0) {
                condensedErrorNumbers = errorNumbers.join(', ');
            }


            var recordLineTwo = {
                isNewLine: false,
                echoId: m[1],
                errorNumber: condensedErrorNumbers,
                match: 'line two'
            };
            return recordLineTwo;
        } else if (expressions.errorTranslationHeader.test(linetext)) {
            return {
                match: 'error translation header'
            };
        } else if (expressions.errorTranslationLine.test(linetext)) {
            var m = expressions.errorTranslationLine.exec(linetext);
            return {
                errorCode: m[1],
                errorDescription: m[2],
                match: 'error translation line'
            };
        } else if (expressions.empty.test(linetext)) {
            //ignore empty lines
            return {
                isNewLine: false,
                match: 'empty'
            };
        } else if (expressions.remitFooter.test(linetext)) {
            //ignore footer
            return {
                isNewLine: false,
                match: 'footer'
            };
        } else if (_.any(expressions.headerLines, function(ex) {
            return ex.test(linetext);
        })) {
            return {
                isNewLine: false,
                match: 'header'
            }
        } else {
            logger.warn('[no match] line ' + index + ': ' + linetext);
            return null;
        }
    };
    //}}} 

    //{{{ make pdf records
    self.makePdfRecords = function(data) {
        var records = {
            remittance: [],
            matches: [],
            errorCodes: [],
            noMatch: []
        };
        var errorTranslation = {};
        var currentRule = null;
        _.each(data.text_pages, function(page, pageIndex) {
            var pageLines = page.split('\n');
            var pageLine = _.findIndex(pageLines, function(line) {
                return /PAGE:\s*\d+/.test(line);
            });
            errorHeaderFound = false;
            if (pageLine > 0) {
                _.each(pageLines, function(linetext, index) {
                    //hack: remove linefeeds from text
                    linetext = linetext.replace(/\f/, '');
                    if (/approved original claims totals/i.test(linetext)) {
                        currentRule = 'approvedOriginalClaimsTotals';
                    } else if (/approved original claims/i.test(linetext)) {
                        currentRule = 'approvedOriginalClaims';
                    } else if (/denied claims/i.test(linetext)) {
                        currentRule = 'deniedClaims';
                    } else if (/claims\s+in\s+process/i.test(linetext)) {
                        currentRule = 'claimsInProcess';
                    } else {
                        var record = self.makePdfRecord(linetext, index);
                        if (!!record) {
                            if (record.match == 'error translation header') {
                                record.page = pageIndex;
                                errorHeaderFound = true;

                            } else if (record.match == 'error translation line') {
                                if (errorHeaderFound) {
                                    records.errorCodes.push({
                                        errorCode: record.errorCode,
                                        errorDescription: record.errorDescription
                                    });
                                } else {
                                    records.noMatch.push({
                                        page: pageIndex,
                                        line: index,
                                        text: linetext
                                    });
                                }
                            } else if (record.match == 'line one' || record.match == 'line two' || record.match == 'line two - no errors') {
                                if (record.isNewLine) {
                                    records.remittance.push(record);
                                } else {
                                    _.merge(records.remittance[records.remittance.length - 1], record);
                                }
                            } else {
                                records.matches.push({
                                    page: pageIndex,
                                    line: index,
                                    matchType: record.match,
                                    linetext: linetext
                                });
                            }
                        } else {
                            //no match
                            records.noMatch.push({
                                page: pageIndex,
                                line: index,
                                text: linetext
                            });
                        }
                    }
                });
            } else {
                logger.warn('page ' + pageIndex + ' did not have a page line')
            }

        });

        return records;
    };
    //}}}

    //{{{ make excel
    self.makeExcel = function(records, outputPath, cb) {
        //set up export definitions
        var exportdefs = require('../../exportDefinitions/remittance');
        var discardExport = new exportdefs.discard();
        var noMatchExport = new exportdefs.noMatch();
        var remittanceRecordExport = new exportdefs.remittanceRecords();
        var errorCodesExport = new exportdefs.errorCodes();

        //set up export data for those definitions
        discardExport.exportData = function(parameters, cb) {
            cb(null, records.matches);
        };
        noMatchExport.exportData = function(parameters, cb) {
            cb(null, records.noMatch);
        };
        remittanceRecordExport.exportData = function(parameters, cb) {
            cb(null, _.map(records.remittance, function(remit) {
                var lastInitial = remit.initials[0];
                var middleInitial = null;
                if (remit.initials.length > 1) {
                    middleInitial = remit.initials[0];
                    lastInitial = remit.initials[1];
                }

                var monetary = function(amt) {
                    return amt.slice(0, amt.length - 2) + '.' + amt.slice(amt.length - 2, amt.length)
                }

                var x = {
                    recipientNumber: remit.recipientNumber,
                    recipientLastName: remit.recipientLastName,
                    recipientLastInitial: lastInitial,
                    recipientMiddleInitial: middleInitial,
                    startDate: moment(remit.startDateOfService, 'MMDDYY')
                        .format('MM/DD/YYYY'),
                    endDate: moment(remit.endDateOfService, 'MMDDYY')
                        .format('MM/DD/YYYY'),
                    units: remit.units,
                    procedureCode: remit.procedureCode,
                    procedure: remit.procedure,
                    amountBilled: monetary(remit.amountBilled),
                    amountAllowed: monetary(remit.amountAllowed),
                    deductions: monetary(remit.deductions),
                    amountPaid: monetary(remit.amountPaid),
                    controlNumber: remit.controlNumber,
                    errorNumber: remit.errorNumber,
                    echoId: remit.echoId,
                    page: remit.page
                };

                return x;
            }));
        };
        errorCodesExport.exportData = function(parameters, cb) {
            cb(null, records.errorCodes);
        };

        async.parallel({
            discard: function(cb) {
                var xlsx = new spreadsheetImport.export(null, null);
                xlsx.on('row progress', function(e) {
                    if((e.current/e.total)*100 == 100){
                        self.emit('xl sheet complete', {sheet: 'Discard'});
                    }
                });
                xlsx.process(discardExport, null, function(err, sheets) {
                    cb(err, sheets);
                });
            },
            noMatch: function(cb) {
                var xlsx = new spreadsheetImport.export(null, null);
                xlsx.on('row progress', function(e) {
                    if((e.current/e.total)*100 == 100){
                        self.emit('xl sheet complete', {sheet: 'No Match'});
                    }
                });
                xlsx.process(noMatchExport, null, function(err, sheets) {
                    cb(err, sheets);
                });
            },
            remittance: function(cb) {
                var xlsx = new spreadsheetImport.export(null, null);
                xlsx.on('row progress', function(e) {
                    if((e.current/e.total)*100 == 100){
                        self.emit('xl sheet complete', {sheet: 'Remittance'});
                    }
                });
                xlsx.process(remittanceRecordExport, null, function(err, sheets) {
                    cb(err, sheets);
                });
            },
            errorCodes: function(cb) {
                var xlsx = new spreadsheetImport.export(null, null);
                xlsx.on('row progress', function(e) {
                    if((e.current/e.total)*100 == 100){
                        self.emit('xl sheet complete', {sheet: 'Error Code'});
                    }
                });
                xlsx.process(errorCodesExport, null, function(err, sheets) {
                    cb(err, sheets);
                });
            },
        }, function(err, sheets) {
            var allSheets = _.reduce(sheets, function(a, b) {
                return a.concat(b);
            }, []);
            xlsxutil.save(allSheets, outputPath, function() {
                cb();
            });
        });

    };
    //}}}

    self.start = function(pdfPath, outputXlPath, cb) {
        self.pdfProcessor = pdf(pdfPath, {
            type: 'text'
        }, function(err) {});

        self.pdfProcessor.on('page', function(p) {
            //catch the pdf page event and reemit
            self.emit('pdf page', p);
        });
        self.pdfProcessor.on('complete', function(data) {
            self.emit('xl start', {
                message: 'starting xl'
            });
            var records = self.makePdfRecords(data);
            self.makeExcel(records, outputXlPath, function(err) {
                cb(null);
            });
        });
    };
};
//make pdf extract emittable
pdfExtract.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);

module.exports = pdfExtract;
