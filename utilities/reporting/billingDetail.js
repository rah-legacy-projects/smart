var _ = require('lodash'),
    context = require('../../models/context'),
    async = require('async'),
    moment = require('moment-timezone'),
    dataUtil = require('./reportingDataUtil'),
    mongoose = require('mongoose'),
    logger = require('winston');

require('twix');

module.exports = function(options, cb) {
    _.defaults(options, {
        timezone: 'America/Chicago',
        yearStart: moment().tz('America/Chicago')
            .startOf('year'),
        yearEnd: moment().tz('America/Chicago')
            .endOf('year'),
    });

    context.rollup.billingDetail = mongoose.connection.collection('rpt_billingdetail');


    var tasks = [

        function(cb) {
            logger.silly('scorching mco auth rollup');
            //torch the reporting rollup
            context.rollup.billingDetail.remove({}, function(err, numberRemoved) {
                cb(err, numberRemoved);
            });
        },
        function(numberRemoved, cb) {
            context.rollup.Billed.find()
                .where('remittanceDate')
                .ne(null)
                .where('dateTime')
                .lte(moment(options.yearEnd).tz(options.timezone)
                    .toDate())
                .where('dateTime')
                .gte(moment(options.yearStart).tz(options.timezone)
                    .toDate())
                .exec(function(err, billed) {
                    cb(err, billed);
                })
        },
        function(billed, cb) {
            cb(null, _.map(billed, function(b) {
                var percentPaid = (b.amountPaid / b.amountBilled) * 100;
                return {
                    studentName: b.studentName,
                    therapist: b.therapistName,
                    districtCode: b.districtCode,
                    districtName: b.districtName,
                    therapy: b.therapy,
                    amountBilled: b.amountBilled,
                    amountPaid: b.amountPaid,
                    amountDenied: b.amountDenied,
                    percentPaid: percentPaid
                };
            }));

        }
    ]

    async.waterfall(tasks, function(err, flats) {
        if (!!err) {
            logger.error(err);
        }
        //put reportable records in the reporting collection
        async.parallel(_.map(flats, function(flat) {
            return function(cb) {
                context.rollup.billingDetail.save(flat, cb);
            };
        }), function(err, r) {
            cb(err, flats);
        });

    });

};
