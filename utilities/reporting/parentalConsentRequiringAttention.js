var _ = require('lodash'),
    context = require('../../models/context'),
    async = require('async'),
    moment = require('moment'),
    dataUtil = require('./reportingDataUtil'),
    mongoose = require('mongoose'),
    logger = require('winston');

module.exports = function(options, cb) {
    _.defaults(options, {
        timezone:'America/Chicago',
        days: 30,
        from: moment().tz('America/Chicago')
            .toDate()
    });

    context.rollup.parentalConsent = mongoose.connection.collection('rpt_parentalconsentsrequiringattention');
    var today = moment(options.from).tz(options.timezone)
        .startOf('day');
    var horizon = moment(options.from).tz(options.timezone)
        .add(options.days, 'days');

    var tasks = [

        function(cb) {
            //torch the reporting rollup
            context.rollup.parentalConsent.remove({}, function(err, numberRemoved) {
                cb(err, numberRemoved);
            });
        },
        function(numberRemoved, cb) {
            //identify the parental consents about to expire
            //to treat is inside the time horizon and after today
            //to bill is inside the time horizon and after today
            context.ParentalConsent.find({
                rangeActive: true
            })
                .or([{
                    $and: [{
                        'toTreat': {
                            $lte: moment(horizon)
                                .subtract(1, 'year')
                                .toDate()
                        }
                    }, {
                        'toTreat': {
                            $gte: moment(today)
                                .subtract(1, 'year')
                                .toDate()
                        }
                    }]
                }, {
                    $and: [{
                        'toBill:': {
                            $lte: moment(horizon)
                                .subtract(1, 'year')
                                .toDate()
                        }
                    }, {
                        'toBill': {
                            $gte: moment(today)
                                .subtract(1, 'year')
                                .toDate()
                        }
                    }]
                }])
                .exec(function(err, parentalConsents) {
                    //waterfall those ids
                    cb(err, _.pluck(parentalConsents, '_id'));
                });
        },
        function(parentalConsentIds, cb) {
            //identify students with those parental consents
            context.Student.find()
                .elemMatch('parentalConsents', {
                    $in: parentalConsentIds
                })
                .exec(function(err, students) {
                    cb(err, parentalConsentIds, students);
                });
        },
        function(parentalConsentIds, students, cb) {
            //populate student data
            async.waterfall([

                function(cb) {
                    context.Student.populate(students, {
                        path: 'parentalConsents',
                        model: 'ParentalConsent'
                    }, cb);
                },
                function(students, cb) {
                    context.Student.populate(students, {
                        path: 'rangedData',
                        model: 'StudentRange'
                    }, cb);
                },
                function(students, cb) {
                    context.Student.populate(students, {
                        path: 'rangedData.school',
                        model: 'School'
                    }, cb);
                },
                function(students, cb) {
                    context.Student.populate(students, {
                        path: 'rangedData.school.rangedData',
                        model: 'SchoolRange'
                    }, cb);
                },
                function(students, cb) {
                    context.Student.populate(students, {
                        path: 'rangedData.school.district',
                        model: 'District'
                    }, cb);
                },
                function(students, cb) {
                    context.Student.populate(students, {
                        path: 'rangedData.school.district.rangedData',
                        model: 'DistrictRange'
                    }, cb);
                }
            ], function(err, students) {
                cb(err, parentalConsentIds, students);
            });
        },
        function(parentalConsentIds, students, cb) {
            //flatten students down to reportable records
            var flats = _.chain(students)
                .map(function(student) {
                    //make the student lean
                    student = student.toObject();

                    //warning: the range may no longer be active
                    //warning: reporting get by date range sync _is very different_ than the model's date range sync
                    var flatStudent = dataUtil.getByDateRangeSync(student, 'rangedData', options.from, options.from);
                    var school = dataUtil.getByDateRangeSync(flatStudent.school, 'rangedData', options.from, options.from);
                    var district = dataUtil.getByDateRangeSync(school.district, 'rangedData', options.from, options.from);

                    //what were the problem ids?
                    var parentalConsentsAboutToExpire = _.filter(student.parentalConsents, function(parentalConsent) {
                        return _.any(parentalConsentIds, function(id) {
                            return id.toString() == parentalConsent._id;
                        });
                    });

                    //map the parental consents to the student in an array
                    return _.map(parentalConsentsAboutToExpire, function(parentalConsent) {
                        return {
                            districtCode: district.districtCode,
                            districtName: district.name,
                            studentId: student.studentId,
                            studentName: student.firstName + ' ' + student.lastName,
                            toBill: parentalConsent.toBill,
                            toTreat: parentalConsent.toTreat, 
                            endDate: moment(parentalConsent.endDate).tz(options.timezone).format('MM/DD/YYYY'),
                            internalStudentId: student._id,
                            created: moment().tz(options.timezone).toDate()
                        };
                    });
                })
                .reduce(function(a, b) {
                    //reduce the array of mapped parental consents to a single order array
                    return a.concat(b);
                }, [])
                .value();
            logger.silly('about to call back with ' + util.inspect(flats));
            cb(null, flats);
        }
    ];

    async.waterfall(tasks, function(err, flats) {
        //put reportable records in the reporting collection
        async.parallel(_.map(flats, function(flat) {
            return function(cb) {
                context.rollup.parentalConsent.save(flat, cb);
            };
        }), function(err, r) {
            cb(err,flats);
        });
    });
}
