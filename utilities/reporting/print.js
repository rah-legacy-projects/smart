var _ = require('lodash'),
    context = require('../../models/context'),
    async = require('async'),
    moment = require('moment'),
    dataUtil = require('./reportingDataUtil'),
    mongoose = require('mongoose'),
    Student = require('../../controllers/student'),
    logger = require('winston');
module.exports = function(options, cb) {
    logger.silly('opts in: ' + util.inspect(options));
    _.defaults(options, {
        timezone: 'America/Chicago',
        studentId: null,
        dateSlice: moment().tz('America/Chicago')
            .toDate()
    });

    logger.silly('merged opts: ' + util.inspect(options));

    context.rollup.print = mongoose.connection.collection('rpt_sessionhistory');
    var tasks = [

        function(cb) {
            //torch the reporting rollup
            context.rollup.print.remove({}, function(err, numberRemoved) {
                cb(err, numberRemoved);
            });
        },
        function(numberRemoved, cb) {

            var tasks = {
                sessions: function(cb) {
                    Student.getSessionHistory({
                        params: {
                            studentId: options.studentId
                        }
                    }, {
                        send: function(sessions) {
                            logger.silly('sending sessions');
                            sessions = sessions.value;
                            
                            _.each(sessions, function(s) {
                                s.isSession = true;
                                n.dateTime = moment(n.dateTime).tz(options.timezone).format('MM/DD/YYYY hh:mm a');
                            });
                            cb(null, sessions || []);
                        }
                    });
                },
                progressNotes: function(cb) {
                    Student.getProgressNotes({
                        params: {
                            studentId: options.studentId
                        }
                    }, {
                        send: function(notes) {
                            logger.silly('send prog notes');
                            notes = notes.value;
                            _.each(notes, function(n) {
                                n.isNote = true;
                                n.addedDate = moment(n.addedDate).tz(options.timezone).format('MM/DD/YYYY hh:mm a');
                            });
                            cb(null, notes || []);
                        }
                    });
                }
            }

            async.series(tasks, function(err, r) {
                if(!!err){
                    logger.error(err);
                }
                logger.silly('results: ' + util.inspect(r));
                var combined = r.sessions.concat(r.progressNotes);
                mapped = _.chain(combined)
                    .map(function(c) {
                        return {
                            date: (c.addedDate || c.dateTime),
                            isProgressNote: !!c.isNote,
                            isSession: !!c.isSession,
                            data: c
                        };
                    })
                    .sortBy(function(c) {
                        return moment(c.date)
                            .format('YYYYMMDD');
                    })
                    .value();

                cb(err, combined);
            });
        }
    ];

    async.waterfall(tasks, function(err, flats) {
        //put reportable records in the reporting collection
        async.parallel(_.map(flats, function(flat) {
            return function(cb) {
                context.rollup.print.save(flat, cb);
            };
        }), function(err, r) {
            cb(err, flats);
        });
    });
}
