module.exports = {
    mcoAuthorizationRequiringAttention: require("./mcoAuthorizationRequiringAttention"),
    medicaidInformationRequiringAttention: require("./medicaidInformationRequiringAttention"),
    parentalConsentRequiringAttention: require("./parentalConsentRequiringAttention"),
    prescriptionsRequiringAttention: require('./prescriptionsRequiringAttention'),
    servicesBilledPerformance: require('./servicesBilledPerformance'),
    therapistLicensing: require('./therapistLicensing'),
    billingDetail: require('./billingDetail'),
    billingSummary: require('./billingSummary'),
    authorizationSummary: require('./authorizationSummary'),
    print: require('./print'),
    summaryClaimingBilledPerformance: require('./summaryClaimingBilledPerformance')
}
