var moment = require('moment'),
    logger = require('winston'),
    async = require('async'),
    _ = require('lodash');
require('twix');
module.exports = exports = {
    getByDateRangeSync: function(object, rangeName, startDate, endDate) {
        //todo: make sure object has rangename
        //nb: object must be lean
        //todo: check for leanness of object

        var isOverlapping = function(objectRange) {
            var objStart = moment(objectRange.startDate),
                objEnd = moment(objectRange.endDate),
                rangeStart = moment(startDate),
                rangeEnd = moment(endDate);

            objRange = objStart.twix(objEnd);
            rangeRange = rangeStart.twix(rangeEnd);


            //dates are the same,
            var retval = (objStart.isSame(rangeStart) && objEnd.isSame(rangeEnd)) ||
                objRange.overlaps(rangeRange);
            return retval;
        };

        //filter to active ranges
        var currentRanges = _.filter(object[rangeName], function(objectRange) {
            if (!objectRange.rangeActive) {
                return false;
            }
            return isOverlapping(objectRange);
        });

        //if there are no current ranges, filter to inactive ranges
        if (currentRanges.length == 0) {
            currentRanges = _.filter(object[rangeName], function(objectRange) {
                if (!!objectRange.rangeActive) {
                    return false;
                }
                return isOverlapping(objectRange);
            });
        }

        //sort the ranges by last modified descending
        _.sortBy(currentRanges, function(r) {
            return moment(r.modifiedDate);
        });
        currentRanges.reverse();

        var flat = _.clone(object);
        delete flat[rangeName];

        //if there are active or inactive ranges,
        if (currentRanges.length > 0) {
            //use the topmost range (most recently modified)
            //clone the topmost range
            //remove the identifier as not to stomp the parent record's ID
            var currentRange = _.clone(currentRanges[0]);
            var rangeId = currentRange._id;
            delete currentRange._id;
            if (currentRanges.length > 1) {
                logger.warn('object has multiple current ranges, using the first');
            }

            //add the range ID back into the flat
            flat._rangeId = rangeId;

            //merge that data into the flattened object
            _.merge(flat, currentRange);
        }
        else{
            //otherwise, use the flattened object as is..?
        }

        //flat should now contain the most current, active or closest modified inactive, or (hopefully not) just the wrapper flat
        return flat;
    }
};
