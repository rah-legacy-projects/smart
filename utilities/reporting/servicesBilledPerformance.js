var _ = require('lodash'),
    context = require('../../models/context'),
    async = require('async'),
    moment = require('moment-timezone'),
    dataUtil = require('./reportingDataUtil'),
    mongoose = require('mongoose'),
    reject = require('../../billing/billable/reject'),
    manipulate = require('../../billing/billable/manipulate'),
    util = require('util'),
    logger = require('winston');

module.exports = function(options, cb) {
    _.defaults(options, {
        timezone: 'America/Chicago',
        start: moment()
            .tz('America/Chicago')
            .startOf('year')
            .toDate(),
        end: moment()
            .tz('America/Chicago')
            .endOf('year')
            .toDate(),
    });

    context.rollup.servicesBilledPerformance = mongoose.connection.collection('rpt_servicesbilledperformance');

    var rollupFunction = function(flatDistrict, flatUser, start, end, cb) {
        logger.silly(start.toString() + ' to ' + end.toString());
        async.waterfall([

            function(cb) {
                manipulate.retrieve({
                    startDate: moment(start)
                        .tz(options.timezone)
                        .toDate(),
                    endDate: moment(end)
                        .tz(options.timezone)
                        .toDate(),
                    schedulesOnly: true
                }, cb);
            },
            reject.session,
            manipulate.populate,
            manipulate.flatten
        ], function(err, flats) {
            logger.silly('#flats retrieved from billing guts: ' + flats.length);
            var districtFlats = _.filter(flats, function(flat) {
                logger.silly('\t\t\t ' + flat.district._id + ' == ' + flatDistrict._id);
                logger.silly('\t\t\t ' + flat.district.districtCode + ' == ' + flatDistrict.districtCode);
                return flat.district._id.toString() == flatDistrict._id.toString();
            });
            logger.silly('#district flats: ' + districtFlats.length);

            districtFlats = _.filter(flats, function(flat) {
                return flat.sessionProvider._id.toString() == flatUser._id.toString();
            });

            if (districtFlats.length > 0) {
                var therapies = _.map(districtFlats, function(flat) {
                    return flat.serviceType;
                });

                var aggregates = [];
                _.each(therapies, function(therapy) {
                    var billed = _.filter(districtFlats, function(flat) {
                        return !!flat.billedDate && flat.serviceType == therapy;
                    });
                    var unbilled = _.filter(districtFlats, function(flat) {
                        return !flat.billedDate && flat.serviceType == therapy;
                    });

                    aggregates.push({
                        districtCode: districtFlats[0].district.districtCode,
                        districtName: districtFlats[0].district.name,
                        therapist: districtFlats[0].sessionProvider.firstName + ' ' + districtFlats[0].sessionProvider.lastName,
                        therapistId: districtFlats[0].sessionProvider._id,
                        therapy: therapy,
                        monthNumber: moment(start)
                            .tz(options.timezone)
                            .format('MM'),
                        month: moment(start)
                            .tz(options.timezone)
                            .format('MMM'),
                        year: moment(start)
                            .tz(options.timezone)
                            .format('YYYY'),
                        billed: billed.length,
                        unbilled: unbilled.length,

                    });

                });
                logger.silly('# aggregates: ' + aggregates.length);
                cb(null, aggregates);

            } else {
                cb(null, null);
            }
        });
    };

    async.waterfall([

        function(cb) {
            logger.silly('removing svcs billed');
            //torch the reporting rollup
            context.rollup.servicesBilledPerformance.remove({}, function(err, numberRemoved) {
                cb(err, numberRemoved);
            });
        },
        function(numberRemoved, cb) {
            logger.silly('getting districts');
            context.District.find()
                .populate('rangedData')
                .lean()
                .exec(function(err, districts) {
                    logger.silly('#districts: ' + districts.length);
                    cb(err, districts);
                });
        },
        function(districts, cb) {
            logger.silly('getting users');
            context.User.find()
                .lean()
                .exec(function(err, users) {
                    cb(err, users, districts);
                });
        },
        function(users, districts, cb) {
            logger.silly('about to roll stuff up');
            var yearIterator = moment(options.start)
                .tz(options.timezone)
                .twix(moment(options.end)
                    .tz(options.timezone))
                .iterate('years');
            var tasks = [];
            while (yearIterator.hasNext()) {
                var startOfYearIteration = yearIterator.next();
                var endOfYearIteration = moment(startOfYearIteration)
                    .endOf('year');

                var monthIterator = moment(startOfYearIteration)
                    .twix(moment(endOfYearIteration))
                    .iterate('months');
                while (monthIterator.hasNext()) {
                    logger.silly('mi has next: ' + monthIterator.hasNext());
                    var startOfMonthIteration = monthIterator.next();
                    logger.silly('mi has next: ' + monthIterator.hasNext());
                    var endOfMonthIteration = moment(startOfMonthIteration)
                        .tz(options.timezone)
                        .endOf('month');
                    logger.silly('start of month: ' + startOfMonthIteration.toString());
                    _.each(users, function(user) {
                        _.each(districts, function(district) {
                            (function(startOfMonthIteration, endOfMonthIteration, district, user) {
                                logger.silly('start: ' + startOfMonthIteration.toString() + ', end: ' + endOfMonthIteration.toString());
                                tasks.push(function(cb) {

                                    var flatDistrict = dataUtil.getByDateRangeSync(district, 'rangedData', startOfMonthIteration, endOfMonthIteration);

                                    rollupFunction(flatDistrict, user, startOfMonthIteration, endOfMonthIteration, function(err, rolled) {
                                        cb(err, rolled);
                                    });
                                })
                            })(startOfMonthIteration, endOfMonthIteration, district, user);
                        });
                    });
                }
            }

            logger.silly('calling back with tasks');
            cb(null, tasks);
        }
    ], function(err, tasks) {
        logger.silly('about to run flats parallels');
        async.series(tasks, function(err, flats) {
            flats = _.compact(_.reduceRight(flats, function(a, b) {
                return a.concat(b);
            }, []));


            logger.silly('#flats: ' + flats.length);

            //make a placeholder
            var years = {};

            //group the flats by year
            var groupedByYear = _.groupBy(flats, function(flat) {
                return flat.year;
            });

            //for each year,
            _.chain(groupedByYear)
                .keys()
                .each(function(year) {
                    //group the flats by month
                    groupedByMonth = _.groupBy(groupedByYear[year], function(m) {
                        return m.monthNumber;
                    });

                    years[year] = {};

                    //for each month, 
                    _.chain(groupedByMonth)
                        .keys()
                        .each(function(month) {
                            var groupedByTherapist = _.groupBy(groupedByMonth[month], function(t) {
                                return t.therapistId;
                            });

                            years[year][month] = {};

                            //for each therapist,
                            _.chain(groupedByTherapist)
                                .keys()
                                .each(function(therapist) {
                                    groupedByTherapy = _.groupBy(groupedByTherapist[therapist], function(t) {
                                        return t.therapy;
                                    });

                                    years[year][month][therapist] = {};

                                    _.chain(groupedByTherapy)
                                        .keys()
                                        .each(function(therapy) {
                                            years[year][month][therapist][therapy] = {
                                                billed: 0,
                                                unbilled: 0
                                            };
                                            //determine the number billed
                                            years[year][month][therapist][therapy].billed += _.reduce(groupedByTherapy[therapy], function(sum, f) {
                                                return sum + f.billed;
                                            }, 0);

                                            //determine the number unbilled
                                            years[year][month][therapist][therapy].unbilled += _.reduce(groupedByTherapy[therapy], function(sum, f) {
                                                return sum + f.unbilled;
                                            }, 0);

                                        });
                                });


                        });
                });
            logger.silly(util.inspect(years, null, 10));

            //years should now have years containing months
            //months contain therapists
            //therapists have the unbilled and billed totals by month by year

            _.chain(years)
                .keys()
                .each(function(year) {
                    _.chain(years[year])
                        .keys()
                        .each(function(month) {
                            var monthNumber = parseInt(month);
                            _.chain(years[year])
                                .keys()
                                .each(function(innerMonth) {
                                    var innerMonthNumber = parseInt(innerMonth);
                                    if (monthNumber >= innerMonthNumber) {
                                        _.chain(years[year][month])
                                            .keys()
                                            .each(function(therapist) {
                                                _.chain(years[year][month][therapist])
                                                    .keys()
                                                    .each(function(therapy) {
                                                        //should that therapist not have a session in the month,
                                                        if (!years[year][month][therapist][therapy]) {
                                                            //add it.
                                                            years[year][month][therapist][therapy] = {
                                                                billed: 0,
                                                                unbilled: 0
                                                            };
                                                        }
                                                        //if the therapist does not have a billed/unbilled ytd, add it.
                                                        if (!years[year][month][therapist][therapy].billedYearToDate) {
                                                            years[year][month][therapist][therapy].billedYearToDate = 0;
                                                        }
                                                        if (!years[year][month][therapist][therapy].unbilledYearToDate) {
                                                            years[year][month][therapist][therapy].unbilledYearToDate = 0;
                                                        }

                                                        //add the previous month's billed/unbilled to the current months ytd values
                                                        years[year][month][therapist][therapy].billedYearToDate += years[year][innerMonth][therapist][therapy].billed;
                                                        years[year][month][therapist][therapy].unbilledYearToDate += years[year][innerMonth][therapist][therapy].unbilled;
                                                    })
                                            });
                                    }
                                });
                        });
                });

            logger.silly(util.inspect(years, null, 10));

            //years should now have years containing months
            //months contain therapists
            //therapists have the billed, unbilled and ytd totals

            //for each of the original flats,
            _.each(flats, function(flat) {
                logger.silly(flat.year + ' ' + flat.monthNumber + ' ' + flat.therapistId + ' ' + flat.therapy);
                //add in the year-to-date for billed and unbilled for the therapist
                flat.billedYearToDate = years[flat.year][flat.monthNumber][flat.therapistId][flat.therapy].billedYearToDate;
                flat.unbilledYearToDate = years[flat.year][flat.monthNumber][flat.therapistId][flat.therapy].unbilledYearToDate;
            });

            async.parallel(_.map(flats, function(flat) {
                return function(cb) {
                    context.rollup.servicesBilledPerformance.save(flat, cb);
                };
            }), function(err, r) {
                cb(err, flats);
            });
        });
    });
}
