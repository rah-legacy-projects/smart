var _ = require('lodash'),
    context = require('../../models/context'),
    async = require('async'),
    moment = require('moment-timezone'),
    dataUtil = require('./reportingDataUtil'),
    mongoose = require('mongoose'),
    logger = require('winston');

module.exports = function(options, cb) {
    _.defaults(options, {
        days: 30,
        from: moment().tz('America/Chicago')
            .toDate(),
        timezone: 'America/Chicago'
    });

    context.rollup.gds = mongoose.connection.collection('rpt_medicaidinformationrequiringattention');
    var today = moment(options.from).tz(options.timezone)
        .startOf('day');
    var horizon = moment(options.from).tz(options.timezone)
        .add(options.days, 'days');

    var tasks = [

        function(cb) {
            //torch the reporting rollup
            context.rollup.gds.remove({}, function(err, numberRemoved) {
                cb(err, numberRemoved);
            });
        },
        function(numberRemoved, cb) {
            //find soon to expire GDS data
            context.ranged.GDS.find({
                rangeActive: true
            })
                .where('endDate')
                .lte(horizon.toDate())
                .where('endDate')
                .gte(today.toDate())
                .exec(function(err, gds) {
                    //waterfall those ids
                    cb(err, _.map(gds, function(g) {
                        return g._id;
                    }));
                });

        },
        function(gdsRangeIds, cb) {
            //find the gds wrappers they belong to
            context.GDS.find()
                .elemMatch('rangedData', {
                    $in: gdsRangeIds
                })
                .exec(function(err, gds) {
                    cb(err, gds);
                });
        },
        function(gds, cb) {
            //populate the gds data
            async.waterfall([

                function(cb) {
                    context.GDS.populate(gds, {
                        path: 'student',
                        model: 'Student'
                    }, cb);
                },
                function(gds, cb) {
                    context.GDS.populate(gds, {
                        path: 'student.rangedData',
                        model: 'StudentRange'
                    }, cb);
                },
                function(gds, cb) {
                    context.GDS.populate(gds, {
                        path: 'student.rangedData.school',
                        model: 'School'
                    }, cb);
                },
                function(gds, cb) {
                    context.GDS.populate(gds, {
                        path: 'student.rangedData.school.rangedData',
                        model: 'SchoolRange'
                    }, cb);
                },
                function(gds, cb) {
                    context.GDS.populate(gds, {
                        path: 'student.rangedData.school.district',
                        model: 'District'
                    }, cb);
                },
                function(gds, cb) {
                    context.GDS.populate(gds, {
                        path: 'student.rangedData.school.district.rangedData',
                        model: 'DistrictRange'
                    }, cb);
                },
                function(gds, cb) {
                    context.GDS.populate(gds, {
                        path: 'rangedData',
                        model: 'GDSRange'
                    }, cb);
                },
                function(gds, cb) {
                    context.GDS.populate(gds, {
                        path: 'rangedData.mco',
                        model: 'MCO'
                    }, cb);
                },
                function(gsd, cb) {
                    context.GDS.populate(gds, {
                        path: 'rangedData.mco.rangedData',
                        model: 'MCORange'
                    }, cb);
                }
            ], function(err, r) {
                cb(err, r);
            });
        },
        function(gds, cb) {
            //flatten the gds data down for reporting consumption
            cb(null, _.map(gds, function(g) {
                g = g.toObject();

                //warning: the range may no longer be active
                //warning: reporting get by date range sync _is very different_ than the model's date range sync
                var flatStudent = dataUtil.getByDateRangeSync(g.student, 'rangedData', options.from, options.from);
                var school = dataUtil.getByDateRangeSync(flatStudent.school, 'rangedData', options.from, options.from);
                var district = dataUtil.getByDateRangeSync(school.district, 'rangedData', options.from, options.from);
                //hack: this can produce erroneous results if the start date is after today
                var flatGDS = dataUtil.getByDateRangeSync(g, 'rangedData', options.from, options.from);

                return {
                    districtCode: district.districtCode,
                    districtName: district.name,
                    studentId: flatStudent.studentId,
                    studentName: flatStudent.firstName + ' ' + flatStudent.lastName,
                    endDate: moment(flatGDS.endDate).tz(options.timezone).format('MM/DD/YYYY'),
                    internalStudentId: flatStudent._id,
                    created: moment().tz(options.timezone).toDate()
                };
            }));
        }
    ];

    async.waterfall(tasks, function(err, flats) {
        //put reportable records in the reporting collection
        async.parallel(_.map(flats, function(flat) {
            return function(cb) {
                context.rollup.gds.save(flat, cb);
            };
        }), function(err, r) {
            cb(err,flats);
        });
    });
}
