var _ = require('lodash'),
    context = require('../../models/context'),
    async = require('async'),
    moment = require('moment-timezone'),
    dataUtil = require('./reportingDataUtil'),
    mongoose = require('mongoose'),
    logger = require('winston');

module.exports = function(options, cb) {
    _.defaults(options, {
        timezone: 'America/Chicago',
        dateSlice: moment().tz('America/Chicago')
            .toDate()
    });

    context.rollup.therapistLicensing = mongoose.connection.collection('rpt_therapistlicensing');
    var tasks = [

        function(cb) {
            //torch the reporting rollup
            context.rollup.therapistLicensing.remove({}, function(err, numberRemoved) {
                cb(err, numberRemoved);
            });
        },
        function(numberRemoved, cb) {
            context.User.find({
                active: true
            })
                .exec(function(err, users) {
                    cb(err, users);
                });
        },
        function(users, cb) {
            //populate user data
            async.waterfall([

                function(cb) {
                    context.User.populate(users, {
                        path: 'mcoQualifications',
                        model: 'MCOQualification',
                        match: {
                            deleted: false
                        }
                    }, cb);
                },
                function(users, cb) {
                    context.User.populate(users, {
                        path: 'mcoQualifications.mco',
                        model: 'MCO',
                    }, cb);
                },
                function(users, cb) {
                    context.User.populate(users, {
                        path: 'mcoQualifications.mco.rangedData',
                        model: 'MCORange'
                    }, cb);
                },
                function(users, cb) {
                    context.User.populate(users, {
                        path: 'therapistQualifications',
                        model: 'TherapistQualification',
                        match: {
                            deleted: false
                        }
                    }, cb);
                }
            ], function(err, users) {
                cb(err, users);
            });
        },
        function(users, cb) {
            //add the user's permissions profile for all users
            async.parallel(_.map(users, function(user) {
                return function(cb) {
                    user.permissionsProfile(function(err, permissions) {
                        var x = user.toObject();
                        logger.silly('permissions: ' + util.inspect(permissions));
                        x.permissions = permissions;
                        cb(err, x);
                    });
                };
            }), function(err, users) {
                cb(err, users);
            });
        },
        function(users, cb) {
            //flatten users down to reportable records
            var flats = _.chain(users)
                .map(function(user) {
                    logger.silly(user.emailAddress + ' has perms: ' + !!user.permissions);
                    //map user permissions to districts to MCO qualification
                    var qual = [];
                    _.forIn(user.permissions.districts, function(district, districtCode) {
                        //get the closest to slice mco qual by district
                        var districtQuals = {
                            mcoQualifications: _.filter(user.mcoQualifications, function(q) {
                                return q.district == district._id;
                            })
                        };
                        var mcoQualification = dataUtil.getByDateRangeSync(districtQuals,
                            'mcoQualifications',
                            moment(options.dateSlice).tz(options.timezone),
                            moment(options.dateSlice).tz(options.timezone));
                        //get the closest to slice theraqual
                        var therapistQualification = dataUtil.getByDateRangeSync(user, 'therapistQualifications', options.dateSlice, options.dateSlice);


                        qual.push({
                            name: user.firstName + ' ' + user.lastName,
                            emailAddress: user.emailAddress,
                            hasNpiNumber: !!therapistQualification.npiNumber && /^\s*$/.test(therapistQualification.npiNumber),
                            npiNumber: therapistQualification.npiNumber,
                            hasStateLicense: therapistQualification.licensed,
                            districtCode: districtCode,
                            districtName: district.name,
                            created: moment().tz(options.timezone).toDate()
                        });
                    });
                    return qual;
                })
                .reduce(function(l, r) {
                    return l.concat(r);
                }, [])
                .value();
            logger.silly('about to call back with ' + util.inspect(flats));
            cb(null, flats);
        }
    ];

    async.waterfall(tasks, function(err, flats) {
        //put reportable records in the reporting collection
        async.parallel(_.map(flats, function(flat) {
            return function(cb) {
                context.rollup.therapistLicensing.save(flat, cb);
            };
        }), function(err, r) {
            cb(err), flats;
        });
    });
}
