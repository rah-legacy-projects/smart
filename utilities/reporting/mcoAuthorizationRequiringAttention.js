var _ = require('lodash'),
    context = require('../../models/context'),
    async = require('async'),
    moment = require('moment'),
    dataUtil = require('./reportingDataUtil'),
    mongoose = require('mongoose'),
    logger = require('winston');

module.exports = function(options, cb) {
    _.defaults(options, {
        days: 30,
        from: moment()
            .toDate(),
        timezone: 'America/Chicago'
    });
    logger.silly('mco auth rollup');

    context.rollup.mcoAuth = mongoose.connection.collection('rpt_mcopriorauthorizationrequiringattention');
    var today = moment(options.from).tz(options.timezone)
        .startOf('day');
    var horizon = moment(options.from).tz(options.timezone)
        .add(options.days, 'days');

    var tasks = [

        function(cb) {
            logger.silly('scorching mco auth rollup');
            //torch the reporting rollup
            context.rollup.mcoAuth.remove({}, function(err, numberRemoved) {
                cb(err, numberRemoved);
            });
        },
        function(numberRemoved, cb) {
            //identify the student mco authorizations about to expire
            context.ranged.StudentMCOInformation.find({
                rangeActive: true
            })
                .where('endDate')
                .lte(horizon)
                .where('endDate')
                .gte(today)
                .exec(function(err, mcoAuths) {
                    //waterfall those ids
                    cb(err, _.map(mcoAuths, function(a) {
                        return a._id;
                    }));
                });
        },
        function(mcoAuthIds, cb) {
            logger.silly('identifying students');
            //identify students with those mco auths
            context.Student.find()
                .elemMatch('mcoInformation', {
                    $in: mcoAuthIds
                })
                .exec(function(err, students) {
                    cb(err, mcoAuthIds, students);
                });
        },
        function(mcoAuthIds, students, cb) {
            logger.silly('populating mco auth info');
            //populate student data
            async.waterfall([

                function(cb) {
                    context.Student.populate(students, {
                        path: 'mcoInformation',
                        model: 'StudentMCOInformationRange'
                    }, cb);
                },
                function(students, cb) {
                    context.Student.populate(students, {
                        path: 'rangedData',
                        model: 'StudentRange'
                    }, cb);
                },
                function(students, cb) {
                    context.Student.populate(students, {
                        path: 'rangedData.school',
                        model: 'School'
                    }, cb);
                },
                function(students, cb) {
                    context.Student.populate(students, {
                        path: 'rangedData.school.rangedData',
                        model: 'SchoolRange'
                    }, cb);
                },
                function(students, cb) {
                    context.Student.populate(students, {
                        path: 'rangedData.school.district',
                        model: 'District'
                    }, cb);
                },
                function(students, cb) {
                    context.Student.populate(students, {
                        path: 'rangedData.school.district.rangedData',
                        model: 'DistrictRange'
                    }, cb);
                }
            ], function(err, students) {
                logger.silly('\t\t\t populated.');
                cb(err, mcoAuthIds, students);
            });
        },
        function(mcoAuthIds, students, cb) {
            var studentIds = _.pluck(students, function(student) {
                return student._id;
            });
            context.GDS.find()
                .where('student')
                .in(studentIds)
                .exec(function(err, gdses) {
                    async.waterfall([

                        function(cb) {
                            context.GDS.populate(gdses, {
                                path: 'rangedData',
                                model: 'GDSRange'
                            }, cb)
                        },
                        function(gdses, cb) {
                            context.GDS.populate(gdses, {
                                path: 'rangedData.mco',
                                model: 'MCO'
                            }, cb);
                        },
                        function(gdses, cb) {
                            context.GDS.populate(gdses, {
                                path: 'rangedData.mco.rangedData',
                                model: 'MCORange'
                            }, cb);
                        }
                    ], function(err, gdses) {
                        var temp = _.map(gdses, function(gds) {
                            gds = gds.toObject();
                            var flatGDS = dataUtil.getByDateRangeSync(gds, 'rangedData', options.from, options.from);
                            logger.silly(util.inspect(flatGDS));
                            flatGDS.mco = dataUtil.getByDateRangeSync(flatGDS.mco, 'rangedData', options.from, options.from);
                            return flatGDS;
                        })
                        cb(err, temp, mcoAuthIds, students);
                    })
                });
        },
        function(gdses, mcoAuthIds, students, cb) {
            logger.silly('flattening mco auths');
            //flatten students down to reportable records
            var flats = _.chain(students)
                .map(function(student) {
                    //make the student lean
                    student = student.toObject();

                    //warning: the range may no longer be active
                    //warning: reporting get by date range sync _is very different_ than the model's date range sync
                    var flatStudent = dataUtil.getByDateRangeSync(student, 'rangedData', options.from, options.from);
                    var school = dataUtil.getByDateRangeSync(flatStudent.school, 'rangedData', options.from, options.from);
                    var district = dataUtil.getByDateRangeSync(school.district, 'rangedData', options.from, options.from);
                    var flatGDS = _.find(gdses, function(gds){
                        logger.silly(gds.student + ' === ' + student._id);
                        return gds.student == student._id.toString();
                    });
                    logger.silly(util.inspect(flatGDS))

                    //what were the problem ids?
                    var mcoAuthsAboutToExpire = _.filter(student.mcoInformation, function(mcoAuth) {
                        return _.any(mcoAuthIds, function(id) {
                            return id.toString() == mcoAuth._id;
                        });
                    });

                    //map the mco auths to the student in an array
                    return _.map(mcoAuthsAboutToExpire, function(mcoAuth) {
                        return {
                            internalStudentId: student._id,
                            districtCode: district.districtCode,
                            districtName: district.name,
                            studentId: student.studentId,
                            studentName: student.firstName + ' ' + student.lastName,
                            specialty: mcoAuth.specialty,
                            numberOfSessions: mcoAuth.numberOfSessions,
                            approvalToTreat: mcoAuth.approvalToTreat,
                            endDate: moment(mcoAuth.endDate).tz(options.timezone).format('MM/DD/YYYY'),
                            mcoName: flatGDS.mco.name,
                            created: moment().tz(options.timezone)
                                .toDate()
                        };
                    });
                })
                .reduce(function(a, b) {
                    //reduce the array of mapped parental consents to a single order array
                    return a.concat(b);
                }, [])
                .value();
            cb(null, flats);
        }
    ];

    async.waterfall(tasks, function(err, flats) {
        if (!!err) {
            logger.error(err);
        }
        logger.info('mco auth rollups: ' + flats.length);
        //put reportable records in the reporting collection
        async.parallel(_.map(flats, function(flat) {
            return function(cb) {
                context.rollup.mcoAuth.save(flat, cb);
            };
        }), function(err, r) {
            cb(err,flats);
        });
    });
}
