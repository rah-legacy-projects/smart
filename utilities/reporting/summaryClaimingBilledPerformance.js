var _ = require('lodash'),
    context = require('../../models/context'),
    async = require('async'),
    moment = require('moment-timezone'),
    dataUtil = require('./reportingDataUtil'),
    mongoose = require('mongoose'),
    reject = require('../../billing/billable/reject'),
    manipulate = require('../../billing/billable/manipulate'),
    util = require('util'),
    logger = require('winston');
require('twix');



module.exports = function(options, cb) {
    _.defaults(options, {
        timezone: 'America/Chicago',
        start: moment()
            .tz('America/Chicago')
            .startOf('year'),
        end: moment()
            .tz('America/Chicago')
            .endOf('year'),
    });

    context.rollup.summaryClaimingBilledPerformance = mongoose.connection.collection('rpt_summaryclaimingbilledperformance');
    async.waterfall([

        function(cb) {
            manipulate.retrieve({
                startDate: moment(options.start)
                    .tz(options.timezone)
                    .toDate(),
                endDate: moment(options.end)
                    .tz(options.timezone)
                    .toDate(),
                schedulesOnly: true
            }, cb);
        },
        reject.session,
        manipulate.populate,
        manipulate.flatten
    ], function(err, flats) {

        var yearIterator = moment(options.start)
            .tz(options.timezone)
            .twix(moment(options.end)
                .tz(options.timezone))
            .iterate('years');
        var tasks = [];
        while (yearIterator.hasNext()) {
            var startOfYearIteration = yearIterator.next();
            var endOfYearIteration = moment(startOfYearIteration)
                .endOf('year');

            var monthIterator = moment(startOfYearIteration)
                .twix(moment(endOfYearIteration))
                .iterate('months');
            while (monthIterator.hasNext()) {
                var startOfMonthIteration = monthIterator.next();
                var endOfMonthIteration = moment(startOfMonthIteration)
                    .tz(options.timezone)
                    .endOf('month');

                var monthFlats = _.filter(flats, function(flat) {
                    return startOfMonthIteration.twix(endOfMonthIteration)
                        .contains(moment(flat.dateTime));
                });

                (function(startOfMonthIteration, startOfYearIteration, monthFlats) {
                    tasks.push(function(cb) {
                        context.rollup.summaryClaimingBilledPerformance.save({
                            monthNumber: moment(startOfMonthIteration)
                                .tz(options.timezone)
                                .format('MM'),
                            month: moment(startOfMonthIteration)
                                .tz(options.timezone)
                                .format('MMM'),
                            year: moment(startOfYearIteration)
                                .tz(options.timezone)
                                .format('YYYY'),
                            sessionsWithIep: monthFlats.length,
                            sessionsWithIepBilled: _.filter(monthFlats, function(flat) {
                                return !!flat.billedDate;
                            })
                                .length,
                            sessionsMedicaidEligible: _.filter(monthFlats, function(flat) {
                                return _.every(flat.errors, function(e) {
                                    return e.rejectionType != 'medicaidIneligible';
                                });
                            })
                                .length,
                            sessionsMedicaidEligibleBilled: _.filter(monthFlats, function(flat) {
                                _.every(flat.errors, function(e) {
                                    return e.rejectionType != 'medicaidIneligible';
                                }) && !!flat.billedDate;
                            })
                                .length
                        }, cb);
                    })
                })(startOfMonthIteration, startOfYearIteration, monthFlats);

            }
        }
        async.parallel(tasks,
            function(err) {
                cb(err);
            });

    });
}
