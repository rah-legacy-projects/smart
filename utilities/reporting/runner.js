var async = require('async'),
    config = require('../../config'),
    moment = require('moment'),
    mongoose = require('mongoose'),
    logger = config.logger;

logger.silly('node env: ' + process.env.NODE_ENV);

mongoose.connect(config.mongodb.url());

var rollup = require('./index');

async.series([

    /*
    function(cb) {
        logger.silly('mco auth');
        rollup.mcoAuthorizationRequiringAttention({}, cb);
    },
    function(cb) {
        logger.silly('medicaid info');
        rollup.medicaidInformationRequiringAttention({}, cb);
    },
    function(cb) {
        logger.silly('parental consent');
        rollup.parentalConsentRequiringAttention({}, cb);
    },
    function(cb) {
        logger.silly('rx');
        rollup.prescriptionsRequiringAttention({}, cb);
    },*/
    function(cb) {
        logger.silly('svcs billed performance');
        rollup.servicesBilledPerformance({}, cb);
    },/*
    function(cb){
        logger.silly('services claiming billing performance');
        rollup.summaryClaimingBilledPerformance({}, cb);
    },
    ,
    function(cb) {
        logger.silly('therapist licensing');
        rollup.therapistLicensing({}, cb);
    },
    function(cb){
        logger.silly('billing detail');
        rollup.billingDetail({}, cb);
    },
    function(cb){
        logger.silly('billing summary');
        rollup.billingSummary({},cb);
    },
    function(cb){
        logger.silly('auth summary');
        rollup.authorizationSummary({},cb);
    }*/
], function(err, r) {
    mongoose.disconnect();
    logger.silly('done');
});
