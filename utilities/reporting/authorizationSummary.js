var _ = require('lodash'),
    context = require('../../models/context'),
    async = require('async'),
    moment = require('moment'),
    dataUtil = require('./reportingDataUtil'),
    mongoose = require('mongoose'),
    logger = require('winston');

module.exports = function(options, cb) {
    _.defaults(options, {
        days: 30,
        timezone: 'America/Chicago',
        from: moment().tz('America/Chicago')
            .toDate(),
    });
    logger.silly('mco auth rollup');

    context.rollup.auth = mongoose.connection.collection('rpt_authorizationsummary');

    async.waterfall([

        function(cb) {
            context.rollup.auth.remove({}, function(err, numberRemoved) {
                cb(err, numberRemoved);
            })
        },
        function(numberRemoved, cb) {
            async.parallel({
                mcoAuths: function(cb) {
                    mongoose.connection.collection('rpt_mcopriorauthorizationrequiringattention')
                        .find(function(err, r) {
                            cb(err, r||[]);
                        });
                },
                parentalConsents: function(cb) {
                    mongoose.connection.collection('rpt_parentalconsentsrequiringattention')
                        .find(function(err, r) {
                            cb(err, r);
                        });

                },
                prescriptions: function(cb) {
                    mongoose.connection.collection('rpt_prescriptionsrequiringattention')
                        .find(function(err, r) {
                            cb(err, r);
                        });

                },
                medicaidInfos: function(cb) {
                    mongoose.connection.collection('rpt_medicaidinformationrequiringattention')
                        .find(function(err, r) {
                            cb(err, r);
                        });
                },
                students: function(cb) {
                    context.Student.find()
                        .populate('rangedData')
                        .exec(function(err, students) {
                            /*cb(err, _.map(students, function(student) {
                                return dataUtil.getByDateRangeSync(student, 'rangedData', options.from, options.from);
                            }));*/

                            async.waterfall([

                                function(cb) {
                                    context.Student.populate(students, {
                                        path: 'rangedData.school',
                                        model: 'School'
                                    }, cb);
                                },
                                function(students, cb) {
                                    context.Student.populate(students, {
                                        path: 'rangedData.school.rangedData',
                                        model: 'SchoolRange'
                                    }, cb);
                                },
                                function(students, cb) {
                                    context.Student.populate(students, {
                                        path: 'rangedData.school.district',
                                        model: 'District'
                                    }, cb);
                                },
                                function(students, cb) {
                                    context.Student.populate(students, {
                                        path: 'rangedData.school.district.rangedData',
                                        model: 'District'
                                    }, cb);
                                }
                            ], function(err, students) {
                                //make the student lean
                                cb(err, _.map(students, function(student) {
                                    student = student.toObject();

                                    //warning: the range may no longer be active
                                    //warning: reporting get by date range sync _is very different_ than the model's date range sync
                                    var flatStudent = dataUtil.getByDateRangeSync(student, 'rangedData', options.from, options.from);
                                    var school = dataUtil.getByDateRangeSync(flatStudent.school, 'rangedData', options.from, options.from);
                                    var district = dataUtil.getByDateRangeSync(school.district, 'rangedData', options.from, options.from);

                                    return {
                                        studentId: flatStudent.studentId,
                                        firstName: flatStudent.firstName,
                                        lastName: flatStudent.lastName,
                                        _id: flatStudent._id,
                                        districtCode: district.districtCode,
                                        districtName: district.districtName
                                    };
                                }));

                            });
                        });
                }
            }, function(err, r) {
                var flats = _.map(r.students, function(student) {
                    var mcoAuths = _.chain(r.mcoAuths)
                        .filter(function(a) {
                            
                            return (a||{}).internalStudentId == student._id;
                        })
                        .sortBy(function(a) {
                            return a.endDate;
                        })
                        .value();

                    var parentalConsents = _.chain(r.parentalConsents)
                        .filter(function(a) {
                            return (a||{}).internalStudentId == student._id;
                        })
                        .sortBy(function(a) {
                            return a.endDate;
                        })
                        .value();

                    var prescriptions = _.chain(r.prescriptions)
                        .filter(function(a) {
                            return (a||{}).internalStudentId == student._id;
                        })
                        .sortBy(function(a) {
                            return a.endDate;
                        })
                        .value();

                    var medicaidInfos = _.chain(r.medicaidInfos)
                        .filter(function(a) {
                            return (a||{}).internalStudentId == student._id;
                        })
                        .sortBy(function(a) {
                            return a.endDate;
                        })
                        .value();

                    var closestExpiration = {
                        mcoAuth: mcoAuths.length > 0 ? mcoAuths[0].endDate : null,
                        parentalConsent: parentalConsents.length > 0 ? parentalConsents[0].endDate : null,
                        prescription: prescriptions.length > 0 ? prescriptions[0].endDate : null,
                        medicaidInfo: medicaidInfos.length > 0 ? medicaidInfos[0].endDate : null
                    };

                    return {
                        studentId: student.studentId,
                        studentName: student.firstName + ' ' + student.lastName,
                        mcoAuthsAboutToExpire: mcoAuths.length,
                        mcoClosestExpiration: moment(closestExpiration.mcoAuth)
                            .tz(options.timezone)
                            .format('MM/DD/YYYY'),
                        parentalConsentsAboutToExpire: parentalConsents.length,
                        parentalConsentClosestExpiration: moment(closestExpiration.parentalConsent)
                            .tz(options.timezone)
                            .format('MM/DD/YYYY'),
                        prescriptionsAboutToExpire: prescriptions.length,
                        prescriptionClosestExpiration: moment(closestExpiration.prescription)
                            .tz(options.timezone)
                            .format('MM/DD/YYYY'),

                        //there should only ever be one medicaid info
                        medicaidInfoClosestExpiration: moment(closestExpiration.medicaidInfo)
                            .tz(options.timezone)
                            .format('MM/DD/YYYY'),

                        districtName: student.districtName,
                        districtCode: student.districtCode
                    };

                });

                cb(err, flats);
            });
        }
    ], function(err, flats) {

        if (!!err) {
            logger.error(err);
        }
        //put reportable records in the reporting collection
        async.parallel(_.map(flats, function(flat) {
            return function(cb) {
                context.rollup.auth.save(flat, cb);
            };
        }), function(err, r) {
            cb(err, flats);
        });

    });
}
