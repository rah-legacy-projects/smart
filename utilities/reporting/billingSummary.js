var _ = require('lodash'),
    context = require('../../models/context'),
    async = require('async'),
    moment = require('moment'),
    dataUtil = require('./reportingDataUtil'),
    mongoose = require('mongoose'),
    logger = require('winston');

require('twix');

module.exports = function(options, cb) {
    _.defaults(options, {
        yearStart: moment()
            .startOf('year'),
        yearEnd: moment()
            .endOf('year')
    });

    context.rollup.billingSummary = mongoose.connection.collection('rpt_billingsummary');

    var rollupFunction = function(district, start, end, cb) {
        var tasks = {
            billed: function(cb) {
                context.rollup.Billed.find({
                    district: district._id
                })
                    .where('remittanceDate')
                    .ne(null)
                    .where('dateTime')
                    .gte(moment(start)
                        .toDate())
                    .where('dateTime')
                    .lte(moment(end)
                        .toDate())
                    .exec(function(err, billed) {
                        cb(err, billed);
                    })
            },
            rejected: function(cb) {
                context.rollup.Rejected.find({
                    district: district._id
                })
                    .where('dateTime')
                    .gte(moment(start)
                        .toDate())
                    .where('dateTime')
                    .lte(moment(end)
                        .toDate())
                    .exec(function(err, rejected) {
                        cb(err, rejected);
                    })
            },
        };

        async.parallel(tasks, function(err, r) {
            //aggregate into amount billed, paid, denied, %paid
            var aggregate = {
                districtCode: district.districtCode,
                districtName: district.name,
                billed: _.reduce(r.billed, function(memo, bill) {
                    return memo + bill.amountBilled
                }, 0),
                paid: _.reduce(r.billed, function(memo, bill) {
                    return memo + bill.amountPaid;
                }, 0),
                denied: _.reduce(r.billed, function(memo, bill) {
                    return memo + (bill.amountBilled - bill.amountPaid);
                }),
                start: start,
                end: end,
                monthName: moment(start)
                    .format("MMM"),
                year: moment(start)
                    .format('YYYY')
            };

            aggregate.percentPaid = (aggregate.paid / aggregate.billed) * 100;

            cb(err, aggregate);
        });

    };


    async.waterfall([

            function(cb) {
                context.rollup.billingSummary.remove({}, function(err, numberRemoved) {
                    cb(err, numberRemoved)
                });
            },
            function(numberRemoved, cb) {

                //find all districts with ranged data,
                context.District.find()
                    .populate('rangedData')
                    .lean()
                    .exec(function(err, districts) {

                        var ittasks = _.chain(districts)
                            .map(function(district) {
                                //iterate by month over the 'year' range,
                                var iterator = moment(options.yearStart)
                                    .twix(moment(options.yearEnd))
                                    .iterate('months');

                                var tasks = [];
                                while (iterator.hasNext()) {
                                    var startOfIteration = iterator.next();
                                    var endOfIteration = moment(startOfIteration)
                                        .endOf('month');


                                    //for each month,
                                    tasks.push(function(cb) {
                                        //flatten district
                                        var flatDistrict = dataUtil.getByDateRangeSync(district, 'rangedData', startOfIteration, endOfIteration);
                                        //roll up data by district by month
                                        //callback will return an array of flats by district by month
                                        rollupFunction(flatDistrict, startOfIteration, endOfIteration, cb);
                                    });
                                }

                                return tasks;
                            })
                            .reduce(function(l, r) {
                                return l.concat(r);
                            }, [])
                            .value();
                        logger.silly(util.inspect(ittasks));


                        async.parallel(ittasks, function(err, r) {
                            cb(err, r);
                        });
                    });
            }
        ],
        function(err, flats) {
            //put reportable records in the reporting collection
            async.parallel(_.map(flats, function(flat) {
                return function(cb) {
                    context.rollup.billingSummary.save(flat, cb);
                };
            }), function(err, r) {
                logger.silly('calling back');
                cb(err, flats);
            });
        });
};
