var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../controllers/controllerContext'),
    middleware = require('./middleware/middleware');

var errorHandlingRouter= express.Router();

errorHandlingRouter.use(middleware.loggedIn);
errorHandlingRouter.use(middleware.localsShim);
errorHandlingRouter.get('/districtNotFound', controllerContext.ErrorHandling.districtNotFound); 

module.exports = errorHandlingRouter;
