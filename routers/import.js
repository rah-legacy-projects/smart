var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../controllers/controllerContext'),
    middleware = require('./middleware/middleware');

var importRouter = express.Router();
importRouter.use(middleware.loggedIn);
importRouter.use(middleware.localsShim);


importRouter.get('/import/district', controllerContext.Upload.getDistrictImport);
importRouter.get('/import/school', controllerContext.Upload.getSchoolImport);
importRouter.get('/import/mco', controllerContext.Upload.getMCOImport);
importRouter.get('/import/procedureCode', controllerContext.Upload.getProcedureCodeImport);
importRouter.get('/import/diagnosticCode', controllerContext.Upload.getDiagnosticCodeImport);
importRouter.get('/import/procedureCodeValue', controllerContext.Upload.getProcedureCodeValueImport);
importRouter.get('/import/student', controllerContext.Upload.getStudentImport);
importRouter.get('/import/service', controllerContext.Upload.getServiceImport);
importRouter.get('/import/gdsMedicaid', controllerContext.Upload.getGDSMedicaidImport);
importRouter.get('/import/gdsBCBST', controllerContext.Upload.getGDSBCBSTImport);
importRouter.get('/import/gdsUHC', controllerContext.Upload.getGDSUHCImport);
importRouter.get('/import/remittance', controllerContext.Upload.getRemittanceImport);
importRouter.get('/import/remittanceErrorCodes', controllerContext.Upload.getRemittanceError);
importRouter.get('/import/remittanceRecords', controllerContext.Upload.getRemittanceRecords);
importRouter.get('/export/gdsNewStudents', controllerContext.Upload.getGDSNewStudentExport);
importRouter.get('/export/gdsPreBilling', controllerContext.Upload.getGDSPreBillingExport);
importRouter.get('/export/billing', controllerContext.Upload.getBilling);

importRouter.get('/template', controllerContext.Template.get);
importRouter.get('/report', controllerContext.Import.getReport);
importRouter.get('/exportReport', controllerContext.Export.getReport);
importRouter.get('/billingZip', controllerContext.Export.getBillingZip);



module.exports = importRouter;
