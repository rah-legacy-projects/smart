var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../controllers/controllerContext'),
    middleware = require('./middleware/middleware');


var rootRouter = express.Router();

rootRouter.use(middleware.loggedIn);
rootRouter.get('/', controllerContext.Root.get);
module.exports = rootRouter;


