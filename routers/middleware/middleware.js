module.exports = {
    localsShim: require('./localsShim'),

    loggedIn: require('./loggedIn'),
    version: require('./version'),
    activityLog: require('./activityLog'),

    navigation: {
        district: require('./navigation/district')
    }
};
