var logger = require('winston');
    _ = require('lodash'),
module.exports = function(req, res, next) {
    var username = 'unknown';
    if(!!req.session.permissionsProfile && !!req.session.permissionsProfile.emailAddress){
        username = req.session.permissionsProfile.emailAddress;
    }
    logger.log('info', '', {
        method: req.method,
        url: req.originalUrl,
        user: username
    });
    next();
}
