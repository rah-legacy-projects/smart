var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    context = require('../../../models/context');
require('../../../util')();
module.exports = function(req, res, next) {

    if (req.method == 'GET') {
        //logger.silly('req: ' + util.inspect(req));
        logger.silly('\t' + req.baseUrl);
        logger.silly('\t' + req.originalUrl);
        //if navigation does not exist, create it
        if (!req.session.navigation) {
            req.session.navigation = [];
        }

        //the following accounts for changing districts
        //if the length of navigation is more than zero,
        if (req.session.navigation.length > 0) {
            //if path and nav[0] do not have the same base
            if (!new RegExp('^' + req.baseUrl)
                .test(req.session.navigation[0].link)) {
                req.session.navigation = [];
            }
        }

        //the following accounts for circular navigation
        //does this path start any of the urls?
        var ix = _.findIndex(req.session.navigation, function(navpoint) {
            logger.silly("\t\t reqpath: " + req.originalUrl + ', npl: ' + navpoint.link);
            return new RegExp('^' + req.originalUrl)
                .test(navpoint.link);
        });

        if (ix >= 0) {
            req.session.navigation.length = (ix);
        }



        var urlParts = _.filter(req.url.split('/'), function(part) {
            return part != '';
        });

        var tasks = [];


        if (/^\/student/.test(req.url)) {
            var studentId = urlParts[1];
            logger.silly('student Id: ' + studentId);
            tasks.push(function(cb) {
                context.Student.findOne({
                    _id: new context.ObjectId(studentId)
                })
                    .exec(function(err, student) {
                        if (!!err) {
                            logger.error('problem getting student for nav');
                            logger.error(err)
                        }

                        logger.silly(util.inspect(student));

                        req.session.navigation.push({
                            display: student.fullName,
                            link: req.originalUrl
                        });
                        cb();
                    });
            })
        } else if (/^\/schedule/.test(req.url)) {
            var scheduleId = urlParts[1],
                seviceId = null;
            if (scheduleId == 'new') {
                serviceId = urlParts[2];

                tasks.push(function(cb) {
                    context.Service.findOne({
                        _id: serviceId
                    })
                        .populate('student')
                        .exec(function(err, service) {
                            req.session.navigation.push({
                                display: ("Scheduling {service} for {student}")
                                    .format({
                                        service: service.service,
                                        student: service.student.fullName
                                    }),
                                link: req.originalUrl
                            });
                            cb();
                        });
                });
            } else {
                tasks.push(function(cb) {
                    context.Schedule.findOne({
                        _id: scheduleId
                    })
                        .populate('services')
                        .exec(function(err, schedule) {
                            context.Service.populate(schedule, {
                                path: 'services'
                            }, function(err, schedule) {
                                context.Student.populate(schedule, {
                                    path: 'services.student'
                                }, function(err, schedule) {
                                    req.session.navigation.push({
                                        display: ("Scheduling {service} for {student}")
                                            .format({
                                                service: schedule.services[0].service,
                                                student: schedule.services.length > 1 ? 'a group' : schedule.services[0].student.fullName
                                            }),
                                        link: req.originalUrl
                                    });
                                    cb();
                                });
                            });
                        });
                });
            }
        } else if (/^\/session/.test(req.url)) {
            if (urlParts[1] == 'new') {
                var scheduleId = urlParts[2];
                tasks.push(function(cb) {
                    var scheduleId = urlParts[1];
                    context.Schedule.findOne({
                        _id: scheduleId
                    })
                        .populate('services')
                        .exec(function(err, schedule) {
                            context.Service.populate(schedule, {
                                path: 'services'
                            }, function(err, schedule) {
                                context.Student.populate(schedule, {
                                    path: 'services.student'
                                }, function(err, schedule) {
                                    req.session.navigation.push({
                                        display: ("Performing {service} for {student}")
                                            .format({
                                                service: schedule.services[0].service,
                                                student: schedule.services.length > 1 ? 'a group' : schedule.services[0].student.fullName
                                            }),
                                        link: req.originalUrl
                                    });
                                    cb();
                                });
                            });
                        });
                });
            } else if (urlParts[2] == 'adHocSession') {
                /*var serviceId = urlParts[1];
                context.Service.findOne({
                    _id: serviceId
                })
                    .populate('student')
                    .exec(function(err, service) {
                        req.session.navigation.push({
                            display: ("Performing {service} for {student}")
                                .format({
                                    service: service.service,
                                    student: service.student.fullName
                                }),
                            link: req.originalUrl
                        });
                        cb();
                    });
                */
                tasks.push(function(cb) {
                    //no op.  Users shouldn't be able to get back to the ad hoc get.
                    cb();
                });
            } else {
                tasks.push(function(cb) {
                    var scheduleId = urlParts[1];
                    context.Schedule.findOne({
                        _id: scheduleId
                    })
                        .populate('services')
                        .exec(function(err, schedule) {
                            context.Service.populate(schedule, {
                                path: 'services'
                            }, function(err, schedule) {
                                context.Student.populate(schedule, {
                                    path: 'services.student'
                                }, function(err, schedule) {
                                    req.session.navigation.push({
                                        display: ("Performing {service} for {student}")
                                            .format({
                                                service: schedule.services[0].service,
                                                student: schedule.services.length > 1 ? 'a group' : schedule.services[0].student.fullName
                                            }),
                                        link: req.originalUrl
                                    });
                                    cb();
                                });
                            });
                        });
                });

            }
        } else {
            tasks.push(function(cb) {

                req.session.navigation.push({
                    display: req.district.name,
                    link: req.baseUrl
                });
                cb();
            });
        }



        //assign to locals
        async.series(tasks, function(err) {

            //hack: if two consecutive navpoints have the same base, skip the former
            var temp = [];
            _.each(req.session.navigation, function(navpoint, nix) {
                if (nix + 1 == req.session.navigation.length) {
                    temp.push(navpoint);
                } else {
                    var nextNavpoint = req.session.navigation[nix + 1];
                    
                    var navpointParts = _.filter(navpoint.link.split('/'), function(p){return p!== '';});
                    var nextNavpointParts = _.filter(nextNavpoint.link.split('/'), function(p){return p!=='';});

                    if(navpointParts.length == 1 || navpointParts[1] !== nextNavpointParts[1]) {
                        temp.push(navpoint);
                    }
                }
            });
            req.session.navigation = temp;


            res.locals.navigation = req.session.navigation;
            next();
        });
    } else {
        next();
    }
}
