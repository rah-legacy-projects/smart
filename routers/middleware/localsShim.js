var logger = require('winston');
    _ = require('lodash');

module.exports = function(req, res, next) {
    //hack: set the locals' district code to prevent redirect loops
    res.locals.districtCode = null;
    res.locals.districtName = null;
    res.locals.multipleDistrictAccess = req.session.permissionsProfile.districts.length > 1;

    //shim the navigation if there isn't any
    if(!res.locals.navigation){
        res.locals.navigation = [];
    }

    next();
}
