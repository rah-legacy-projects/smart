var logger = require('winston');
    _ = require('lodash'),
    packaging = require('../../package.json');
module.exports = function(req, res, next) {
    res.locals.version = packaging.version;
    next();
}
