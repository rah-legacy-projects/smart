var logger = require('winston');
    _ = require('lodash');

module.exports = function(req, res, next) {
    if (!!req.session.permissionsProfile) {
        res.locals.isAdministrator = !!req.session.permissionsProfile.areas && 
            !!req.session.permissionsProfile.areas.administration && 
            !!req.session.permissionsProfile.areas.administration.access && 
            _.contains(req.session.permissionsProfile.areas.administration.access, 'Administer');
        res.locals.userFullName = req.session.permissionsProfile.userName;
        next();
    } else {
        res.redirect('/login');
    }
}
