var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../controllers/controllerContext'),
    middleware = require('./middleware/middleware');

var reportingRouter = express.Router();

reportingRouter.get("/authorizationSummary", controllerContext.Reporting.getAuthorizationSummary);
reportingRouter.get("/billingDetail", controllerContext.Reporting.getBillingDetail);
reportingRouter.get("/billingSummary", controllerContext.Reporting.getBillingSummary);
reportingRouter.get("/mcoAuthorizationRequiringAttention", controllerContext.Reporting.getMcoAuthorizationRequiringAttention);
reportingRouter.get("/medicaidInformationRequiringAttention", controllerContext.Reporting.getMedicaidInformationRequiringAttention);
reportingRouter.get("/parentalConsentRequiringAttention", controllerContext.Reporting.getParentalConsentRequiringAttention);
reportingRouter.get("/prescriptionsRequiringAttention", controllerContext.Reporting.getPrescriptionsRequiringAttention);
reportingRouter.get("/print", controllerContext.Reporting.getPrint);
reportingRouter.get("/servicesBilledPerformance", controllerContext.Reporting.getServicesBilledPerformance);
reportingRouter.get("/therapistLicensing", controllerContext.Reporting.getTherapistLicensing);

module.exports = reportingRouter;
