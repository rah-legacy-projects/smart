var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../controllers/controllerContext'),
    middleware = require('./middleware/middleware');

var loggingRouter = express.Router();

loggingRouter.get("/", controllerContext.Logging.getLogs);
loggingRouter.post("/", controllerContext.Logging.postLogs);

module.exports = loggingRouter;
