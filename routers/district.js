var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../controllers/controllerContext'),
    middleware = require('./middleware/middleware');

var districtRouter = express.Router();

districtRouter.use(middleware.loggedIn);

districtRouter.param('studentId', function(req, res, next, studentId){
    logger.silly('student id: ' + studentId);
    next();
});

districtRouter.use(middleware.navigation.district);
districtRouter.get('/', controllerContext.Home.get);

//case load
districtRouter.post('/caseload/list', controllerContext.Caseload.listCaseload);

//student
districtRouter.get('/student/:studentId', controllerContext.Student.getStudentInformation);
districtRouter.post('/student/:studentId/basicInformation', controllerContext.Student.getBasicInformation);
districtRouter.post('/student/:studentId/iepServices', controllerContext.Student.getIepServices);
districtRouter.post('/student/:studentId/sessionHistory', controllerContext.Student.getSessionHistory);
districtRouter.post('/student/:studentId/additionalInfo', controllerContext.Student.getAdditionalInfo);
districtRouter.post('/student/:studentId/getProgressNotes', controllerContext.Student.getProgressNotes);
districtRouter.post('/student/:studentId/saveServiceRange', controllerContext.Student.saveServiceRange);
districtRouter.post('/student/:studentId/saveParentalConsent', controllerContext.Student.saveParentalConsent);
districtRouter.post('/student/:studentId/saveProgressNote', controllerContext.Student.saveProgressNote);
districtRouter.post('/student/:studentId/savePrescription', controllerContext.Student.savePrescription);
districtRouter.post('/student/:studentId/saveStudentMCOInfo', controllerContext.Student.saveStudentMCOInfo);
districtRouter.post('/student/:studentId/removeParentalConsent/:parentalConsentId', controllerContext.Student.removeParentalConsent);
districtRouter.post('/student/:studentId/removePrescription/:prescriptionId', controllerContext.Student.removePrescription);
districtRouter.post('/student/:studentId/removeStudentMCOInfo/:studentMCOInfoId', controllerContext.Student.removeStudentMCOInfo);
districtRouter.post('/student/:studentId/removeProgressNote/:progressNoteId', controllerContext.Student.removeProgressNote);

//schedule
districtRouter.get('/schedule/new/:serviceId', controllerContext.Schedule.getAddSchedule);
districtRouter.post('/schedule/save', controllerContext.Schedule.saveSeries);
districtRouter.post('/schedule/scheduleInfo', controllerContext.Schedule.scheduleInfo);
districtRouter.post('/schedule/upcoming', controllerContext.Schedule.upcoming);
districtRouter.post('/schedule/getServices', controllerContext.Schedule.getServicesByService);
districtRouter.post('/schedule/unfinished', controllerContext.Schedule.unfinished);
districtRouter.post('/schedule/delete/:scheduleId', controllerContext.Schedule.deleteSchedule);

//session
districtRouter.get('/session/new/:scheduleId', controllerContext.Session.getNewSession);
districtRouter.get('/session/:scheduleId', controllerContext.Session.getExistingSession);
districtRouter.get('/session/:serviceId/adHocSession', controllerContext.Session.adHocSession);
districtRouter.post('/session/new/:scheduleId', controllerContext.Session.getSession);
districtRouter.post('/session/:scheduleId', controllerContext.Session.getSession);
districtRouter.post('/session/:scheduleId/save', controllerContext.Session.saveSession);

//procedure codes
districtRouter.post('/procedureCodes/list', controllerContext.ProcedureCode.listProcedureCodes);

//services
districtRouter.post('/service/:studentId/getServices', controllerContext.Service.getServicesByStudent);

module.exports = districtRouter;
