var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../controllers/controllerContext'),
    middleware = require('./middleware/middleware');

var userAdministrationRouter = express.Router();

userAdministrationRouter.use(middleware.loggedIn);
userAdministrationRouter.use(middleware.localsShim);

//user admin
userAdministrationRouter.get('/users', controllerContext.UserAdministration.getUsers);
userAdministrationRouter.get('/user/:id', controllerContext.UserAdministration.getUser);
userAdministrationRouter.get('/newuser', controllerContext.UserAdministration.getNewUser);
userAdministrationRouter.post('/user/:id', controllerContext.UserAdministration.singleUser);
userAdministrationRouter.post('/newuser', controllerContext.UserAdministration.singleUser);
userAdministrationRouter.post('/user', controllerContext.UserAdministration.postUser);

//mco quals
userAdministrationRouter.get('/:userId/mcoQualifications', controllerContext.MCOQualificationAdministration.getMCOQualifications);
userAdministrationRouter.get('/:userId/mcoQualification/:id', controllerContext.MCOQualificationAdministration.getMCOQualification);
userAdministrationRouter.get('/:userId/new/mcoQualification', controllerContext.MCOQualificationAdministration.getNewMCOQualification);
userAdministrationRouter.post('/:userId/mcoQualifications', controllerContext.MCOQualificationAdministration.listMCOQualifications);
userAdministrationRouter.post('/:userId/mcoQualification/:id?', controllerContext.MCOQualificationAdministration.singleMCOQualification);
userAdministrationRouter.post('/mcoQualification', controllerContext.MCOQualificationAdministration.postMCOQualification);
userAdministrationRouter.post('/revokeMCOQualification', controllerContext.MCOQualificationAdministration.postRemoveMCOQualification);

//therapist quals
userAdministrationRouter.get('/:userId/therapistQualifications', controllerContext.TherapistQualificationAdministration.getTherapistQualifications);
userAdministrationRouter.get('/:userId/therapistQualification/:id', controllerContext.TherapistQualificationAdministration.getTherapistQualification);
userAdministrationRouter.get('/:userId/new/therapistQualification', controllerContext.TherapistQualificationAdministration.getNewTherapistQualification);
userAdministrationRouter.post('/:userId/therapistQualifications', controllerContext.TherapistQualificationAdministration.listTherapistQualifications);
userAdministrationRouter.post('/:userId/therapistQualification/:id?', controllerContext.TherapistQualificationAdministration.singleTherapistQualification);
userAdministrationRouter.post('/therapistQualification', controllerContext.TherapistQualificationAdministration.postTherapistQualification);
userAdministrationRouter.post('/revokeTherapistQualificationAdministration', controllerContext.TherapistQualificationAdministration.postRemoveTherapistQualification);

//permissions
userAdministrationRouter.get('/:userId/permissions', controllerContext.PermissionAdministration.getUserPermissions);
userAdministrationRouter.get('/:userId/permissions/new', controllerContext.PermissionAdministration.getNewUserPermission);
userAdministrationRouter.get('/:userId/permissions/school/:schoolId', controllerContext.PermissionAdministration.getEditUserPermission);
userAdministrationRouter.get('/:userId/permissions/district/:districtId', controllerContext.PermissionAdministration.getEditUserPermission);
userAdministrationRouter.post('/userPermission', controllerContext.PermissionAdministration.postUserPermission);
userAdministrationRouter.post('/revokePermission', controllerContext.PermissionAdministration.postRemovePermission);

module.exports = userAdministrationRouter;
