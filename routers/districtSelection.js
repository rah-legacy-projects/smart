var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../controllers/controllerContext'),
    middleware = require('./middleware/middleware');

var districtSelectionRouter = express.Router();
districtSelectionRouter.use(middleware.loggedIn);
districtSelectionRouter.use(middleware.localsShim);
districtSelectionRouter.get('/', controllerContext.DistrictSelection.get);

module.exports = districtSelectionRouter;
