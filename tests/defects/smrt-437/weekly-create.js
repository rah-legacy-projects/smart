var mongoose = require('mongoose'),
    testUtil = require('../../testUtil'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    mongodb = require('../../config')
    .mongodb,
    context = require('../../../models/context'),
    logger = require('../../config')
    .logger;
module.exports = exports = {
    setUp: function(cb) {
        this.startMoment = moment(new Date(2014, 7, 5));
        this.endMoment = moment(new Date(2014, 7, 27));
        this.connection = mongoose.connect(mongodb.url, function(err) {
            mongoose.connection.db.dropDatabase(function(err, result) {
                cb();
            });
        });

    },
    tearDown: function(cb) {
        mongoose.disconnect();
        cb();

    },
    'ends on date': {
        // {{{ ends on date
        'smrt-437 inclusive': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]', {
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 1,
                    repeatBy: 'tuesday',
                    ends: {
                        never: false,
                        occurrences: null,
                        on: self.endMoment.toDate()
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(4, series.length);
                _.each(series, function(s, i) {
                    //assert members of the series are all on tuesday
                    test.equal(2, moment(s.dateTime)
                        .weekday());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(1, weeks);
                    } else {
                        //make sure the first occurrence is on after the start moment
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || self.startMoment.isSame(thisMoment), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        'smrt-437 exclusive': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]', {
                startDateTime: moment(self.startMoment).add(1, 'days').toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 1,
                    repeatBy: 'tuesday',
                    ends: {
                        never: false,
                        occurrences: null,
                        on: self.endMoment.toDate()
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(3, series.length);
                _.each(series, function(s, i) {
                    //assert members of the series are all on tuesday
                    test.equal(2, moment(s.dateTime)
                        .weekday());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(1, weeks);
                    } else {
                        //make sure the first occurrence is on after the start moment
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || self.startMoment.isSame(thisMoment), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        'smrt-437 different day of week': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]', {
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 1,
                    repeatBy: 'wednesday',
                    ends: {
                        never: false,
                        occurrences: null,
                        on: self.endMoment.toDate()
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(4, series.length);
                _.each(series, function(s, i) {
                    //assert members of the series are all on tuesday
                    test.equal(3, moment(s.dateTime)
                        .weekday());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(1, weeks);
                    } else {
                        //make sure the first occurrence is on after the start moment
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || self.startMoment.isSame(thisMoment), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        //}}}
    },

};
