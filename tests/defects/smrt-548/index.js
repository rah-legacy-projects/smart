var mongoose = require('mongoose'),
    testUtil = require('../../testUtil'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    mongodb = require('../../config')
    .mongodb,
    moment = require('moment'),
    context = require('../../../models/context'),
    logger = require('../../config')
    .logger,
    billable = require('../../../billing/billable'),
    converter = require('../../../billing/converter'),
    path = require('path'),
    fs = require('fs');
module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        this.connection = mongoose.connect(mongodb.url, function(err) {
            //destroy the existing test database
            //done for safety
            mongoose.connection.db.dropDatabase(function(err, result) {
                //like the billing tests, run the imports in setup
                testUtil.importDirectory(path.join(__dirname, 'data'), function(err) {
                    cb(err);
                });
            });
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'multiple districts, students with same id': function(test) {
        context.Student.find().populate('rangedData').exec(function(err, students){
            test.ok(students);
            test.ifError(err);
            test.equal(students.length, 2);
            test.equal(students[0].studentId, students[1].studentId);
            test.notEqual(students[0].rangedData[0].school, students[1].rangedData[0].school);
            test.done();
        });
    },
};
