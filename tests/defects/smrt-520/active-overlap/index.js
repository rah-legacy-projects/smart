var mongoose = require('mongoose'),
    testUtil = require('../../../testUtil'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    mongodb = require('../../../config')
    .mongodb,
    moment = require('moment'),
    context = require('../../../../models/context'),
    logger = require('../../../config')
    .logger,
    billable = require('../../../../billing/billable');
path = require('path');
module.exports = exports = {
    setUp: function(cb) {
        logger.info(__filename);
        this.connection = mongoose.connect(mongodb.url, function(err) {
            //destroy the existing test database
            //done for safety
            mongoose.connection.db.dropDatabase(function(err, result) {
                //like the billing tests, run the imports in setup
                testUtil.importDirectory(path.join(__dirname, 'data'), function(err) {
                    cb(err);
                });
            });
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'SMRT-520': function(test) {
        async.waterfall([

            function(cb) {
                context.GDS.find()
                    .populate('rangedData')
                    .populate('student')
                    .exec(function(err, gds) {

                        cb(err, gds);
                    });
            },
            function(gds, cb) {
                context.GDS.populate(gds, {
                    path: 'rangedData.mco',
                    model: 'MCO'
                }, cb);
            }
        ], function(err, gds) {
            var temp = _.map(gds, function(g) {
                return {
                    name: g.student.fullName,
                    studentId: g.student.studentId,
                    ranges: _.map(g.rangedData, function(r) {
                        return {
                            start: moment(r.startDate).format('MM-DD-YYYY'),
                            end: moment(r.endDate).format('MM-DD-YYYY'),
                            active: r.rangeActive,
                            mco: r.mco.name
                        };
                    })
                }
            });

            logger.silly(util.inspect(temp, null, 10));
            test.done();
        });
    },
};
