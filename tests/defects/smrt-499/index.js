var mongoose = require('mongoose'),
    testUtil = require('../../testUtil'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    mongodb = require('../../config')
    .mongodb,
    moment = require('moment'),
    context = require('../../../models/context'),
    logger = require('../../config')
    .logger,
    billable = require('../../../billing/billable');
path = require('path');
module.exports = exports = {
    setUp: function(cb) {
        logger.info(__filename);
        this.connection = mongoose.connect(mongodb.url, function(err) {
            //destroy the existing test database
            //done for safety
            mongoose.connection.db.dropDatabase(function(err, result) {
                //like the billing tests, run the imports in setup
                testUtil.importDirectory(path.join(__dirname, 'data'), function(err) {
                    cb(err);
                });
            });
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'SMRT-499 - GDS Import Breakage': function(test) {
        context.GDS.find({}).exec(function(err, gdses){
            logger.silly('#gds: ' + gdses.length);
            test.done(); 
        });
    },
};
