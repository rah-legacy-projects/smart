var mongoose = require('mongoose'),
    testUtil = require('../../testUtil'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    mongodb = require('../../config')
    .mongodb,
    context = require('../../../models/context'),
    logger = require('../../config')
    .logger;
module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        this.connection = mongoose.connect(mongodb.url, function(err) {
            //destroy the existing test database
            //done for safety
            mongoose.connection.db.dropDatabase(function(err, result) {
                //like the billing tests, run the imports in setup
                testUtil.importDirectory('./data', function(err) {
                    cb(err);
                });
            });
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'Verify Service Dates': function(test) {
        async.waterfall([

            function(cb) {
                context.Student.findOne({
                    studentId: '5465913'
                })
                    .populate('rangedData')
                    .lean()
                    .exec(function(err, student) {
                        test.ok(student);
                        var flatStudent = dataUtil.getCurrentSync(student, 'rangedData');
                        cb(!!student ? null : 'Could not find student', flatStudent);
                    });
            },
            function(student, cb) {
                logger.silly('student: ' + student._id);
                context.Service.find({
                    student: new context.ObjectId(student._id)
                })
                    .populate('rangedData')
                    .exec(function(err, services) {
                        cb(err, services);
                    });
            }
        ], function(err, service) {
            test.ifError(err);
            if (!err) {
                test.ok(service);
                test.ok(service.length == 1);
                service = service[0];
                test.ok(service.rangedData);
                if (!!service.rangedData) {
                    test.equal(service.rangedData.length, 1);
                    var activeRange = _.find(service.rangedData, function(dr) {
                        return dr.rangeActive;
                    });
                    test.equal(moment(activeRange.startDate)
                        .format('YYYY-MM-DD'), '2014-06-15');
                    test.equal(moment(activeRange.endDate)
                        .format('YYYY-MM-DD'), '2015-06-14');

                }
            }
            test.done();

        });
    }
}
