module.exports = {
    setUp: function(cb) {
        cb();
    },
    tearDown: function(cb) {
        cb();
    },
    mcoAuthorizationRequiringAttention: require("./mcoAuthorizationRequiringAttention"),
    medicaidInformationRequiringAttention: require("./medicaidInformationRequiringAttention"),
    parentalConsentRequiringAttention: require("./parentalConsentRequiringAttention"),
    prescriptionsRequiringAttention: require('./prescriptionsRequiringAttention'),
};
