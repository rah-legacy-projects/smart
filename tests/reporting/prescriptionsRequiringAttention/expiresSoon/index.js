var mongoose = require('mongoose'),
    testUtil = require('../../../testUtil'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    mongodb = require('../../../config')
    .mongodb,
    mongoose = require('mongoose'),
    moment = require('moment'),
    context = require('../../../../models/context'),
    logger = require('../../../config')
    .logger,
    billable = require('../../../../billing/billable'),
    reporting = require('../../../../utilities/reporting');
path = require('path');
module.exports = exports = {
    setUp: function(cb) {
        logger.info(__filename);
        this.connection = mongoose.connect(mongodb.url, function(err) {
            //destroy the existing test database
            //done for safety
            mongoose.connection.db.dropDatabase(function(err, result) {
                //like the billing tests, run the imports in setup
                testUtil.importDirectory(path.join(__dirname, 'data'), function(err) {
                    cb(err);
                });
            });
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'Rx Requires Attention': function(test) {
        reporting.prescriptionsRequiringAttention({
                from: moment('2014-12-01')
            },
            function(err) {
                test.ifError(err);

                mongoose.connection.collection('rpt_prescriptionsrequiringattention')
                    .find({}, function(err, rollups) {
                        rollups.toArray(function(err, rollups) {
                            logger.silly(util.inspect(rollups));
                            test.equal(rollups.length, 1);
                            test.done();

                        });
                    });

            });
    },
};
