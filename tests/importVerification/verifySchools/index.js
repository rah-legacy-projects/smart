var mongoose = require('mongoose'),
    testUtil = require('../../testUtil'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    mongodb = require('../../config')
    .mongodb,
    context = require('../../../models/context'),
    logger = require('../../config')
    .logger;
module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        this.connection = mongoose.connect(mongodb.url, function(err) {
            //destroy the existing test database
            //done for safety
            mongoose.connection.db.dropDatabase(function(err, result) {
                //like the billing tests, run the imports in setup
                testUtil.importDirectory('./data', function(err) {
                    cb(err);
                });
            });
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'Verify Schools': function(test) {
        context.School.find()
            .exec(function(err, schools) {
                test.ifError(err);
                test.equal(schools.length, 1, "schools length: " + schools.length);
                test.done();
            });
    },
    };
