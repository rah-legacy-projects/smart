var mongoose = require('mongoose'),
    testUtil = require('../testUtil'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    mongodb = require('../config')
    .mongodb,
    context = require('../../models/context'),
    logger = require('../config')
    .logger;
module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        cb();
    },
    tearDown: function(cb) {
        cb();
    },
    verifyDistricts: require("./verifyDistricts"),
    verifyGDS: require("./verifyGDS"),
    verifyMCOInfos: require("./verifyMCOInfos"),
    verifyMCOQualifications: require("./verifyMCOQualifications"),
    verifyMCOs: require("./verifyMCOs"),
    verifyParentalConsents: require("./verifyParentalConsents"),
    verifyPrescriptions: require("./verifyPrescriptions"),
    verifyProcedureCodes: require("./verifyProcedureCodes"),
    verifySchools: require("./verifySchools"),
    verifyServices: require("./verifyServices"),
    verifySessions: require("./verifySessions"),
    verifyStudents: require("./verifyStudents"),
    verifyStudentStartAndEndDate: require("./verifyStudentStartAndEndDate"),
    verifyTherapistQualifications: require("./verifyTherapistQualifications"),
    verifyUsers: require("./verifyUsers"),
};
