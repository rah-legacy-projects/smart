var mongoose = require('mongoose'),
    testUtil = require('../../../testUtil'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    mongodb = require('../../../config')
    .mongodb,
    context = require('../../../../models/context'),
    logger = require('../../../config')
    .logger,
    moment = require('moment-timezone');
module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        this.connection = mongoose.connect(mongodb.url, function(err) {
            //destroy the existing test database
            //done for safety
            mongoose.connection.db.dropDatabase(function(err, result) {
                //like the billing tests, run the imports in setup
                testUtil.importDirectory(__dirname + '/data', function(err) {
                    cb(err);
                });
            });
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'Verify Student Start/End Date': function(test) {
        context.Student.find()
            .populate('rangedData')
            .exec(function(err, students) {
                test.ifError(err);
                test.equal(students.length, 1, 'students length: ' + students.length);
                if (students.length > 0) {
                    var student = students[0];
                    test.equal(student.rangedData.length, 2);
                    logger.silly('ranges: ' + util.inspect(student.rangedData));

                    var active = _.filter(student.rangedData, function(r) {
                            return r.rangeActive;
                        });

                    test.equal(active.length, 1);

                    if (active.length > 0) {
                        var range = active[0];
                        test.equal(moment(range.endDate)
                            .format('YYYYMMDD'), moment("01/01/2016")
                            .format('YYYYMMDD'));
                        test.equal(moment(range.startDate)
                            .format('YYYYMMDD'), moment("01/01/2014")
                            .format('YYYYMMDD'));
                    }

                }
                test.done();
            });

    },
};
