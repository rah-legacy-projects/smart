var mongoose = require('mongoose'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async');
module.exports = exports = {
    setUp: function(cb) {
        cb();
    },
    tearDown: function(cb) {
        cb();
    },

    after: require("./after"),
    before: require("./before"),
    both: require("./both")
}
