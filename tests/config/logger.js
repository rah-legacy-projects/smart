var logger = require('winston'),
    mongo = require('./mongodb');

    require('winston-mongodb').MongoDB;
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    //level: 'error',
    level: 'silly',
    colorize: true
});

logger.add(logger.transports.MongoDB, {
    level: 'silly',
    dbUri: mongo.logUrl,
    collection: 'logs',
    storeHost: true
});


process.on('uncaughtException', function(x){
    logger.error('UNCAUGHT: ' + x);
    logger.error(x.stack);
});

exports = module.exports = logger;
