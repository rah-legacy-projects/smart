var mongoose = require('mongoose'),
    testUtil = require('../testUtil'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    mongodb = require('../config')
    .mongodb,
    context = require('../../models/context'),
    logger = require('../config')
    .logger;
module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        this.connection = mongoose.connect(mongodb.url, function(err) {
            //destroy the existing test database
            //done for safety
            mongoose.connection.db.dropDatabase(function(err, result) {
                //like the billing tests, run the imports in setup
                testUtil.importDirectory('./data', function(err) {
                    cb(err);
                });
            });
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'Verification': function(test) {
        async.series([

            function(cb) {
                context.User.find()
                    .exec(function(err, users) {
                        test.ifError(err);
                        test.equal(users.length, 1, "users length: " + users.length);
                        cb();
                    });
            },
            function(cb) {
                context.District.find()
                    .exec(function(err, districts) {
                        test.ifError(err);
                        test.equal(districts.length, 1, "districts length: " + districts.length);

                        cb();
                    });
            },
            function(cb) {
                context.School.find()
                    .exec(function(err, schools) {
                        test.ifError(err);
                        test.equal(schools.length, 1, "schools length: " + schools.length);
                        cb();
                    });
            },
            function(cb) {
                context.MCO.find()
                    .exec(function(err, mcos) {
                        test.ifError(err);
                        test.equal(mcos.length, 1, "mcos length: " + mcos.length);
                        cb();
                    });
            },
            function(cb) {
                context.ProcedureCode.find()
                    .exec(function(err, procedureCodes) {
                        test.ifError(err);
                        test.equal(procedureCodes.length, 1, 'procedure codes length: ' + procedureCodes.length);
                        cb();
                    });
            },
            function(cb) {
                context.Student.find()
                    .exec(function(err, students) {
                        test.ifError(err);
                        test.equal(students.length, 1, 'students length: ' + students.length);
                        cb();
                    });
            },
            function(cb) {
                context.Service.find()
                    .exec(function(err, services) {
                        test.ifError(err);
                        test.equal(services.length, 1, 'services length: ' + services.length);
                        cb();
                    });
            },
            function(cb) {
                context.ParentalConsent.find()
                    .exec(function(err, parentalConsents) {
                        test.ifError(err);
                        test.equal(parentalConsents.length, 1, 'parental consents length: ' + parentalConsents.length);
                        cb();
                    });
            },
            function(cb) {
                context.ranged.StudentMCOInformation.find()
                    .exec(function(err, mcoInfos) {
                        test.ifError(err);
                        test.equal(mcoInfos.length, 1, 'mco infos length: ' + mcoInfos.length);
                        cb();
                    });
            },
            function(cb) {
                context.Prescription.find()
                    .exec(function(err, rx) {
                        test.ifError(err);
                        test.equal(rx.length, 1, 'rx length: ' + rx.length);
                        cb();
                    });
            },
            function(cb) {
                context.Service.findOne()
                    .populate('rangedData')
                    .exec(function(err, service) {
                        test.ifError(err);
                        test.ok(service);
                        test.ok(service.rangedData[0].diagnosticCode);
                        cb();
                    });
            },
            function(cb) {
                context.GDS.find()
                    .exec(function(err, gds) {
                        test.ifError(err);
                        test.equal(gds.length, 1, 'gds length: ' + gds.length);
                        cb();
                    });
            },
            function(cb) {
                context.Session.find()
                    .exec(function(err, sessions) {
                        test.ifError(err);
                        test.equal(sessions.length, 1, 'sessions length: ' + sessions.length);
                        cb();
                    });
            }
        ], function(err) {
            test.done();
        })
    }
}
