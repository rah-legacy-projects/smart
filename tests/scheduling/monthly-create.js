var mongoose = require('mongoose'),
    testUtil = require('../testUtil'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    mongodb = require('../config')
    .mongodb,
    context = require('../../models/context'),
    logger = require('../config')
    .logger;

module.exports = exports = {
    setUp: function(cb) {

        this.startMoment = moment("2014-07-23T14:30"); //july 23, 2014 (a wednesday) at 2:30 pm
        this.weekOfMonth = Math.ceil(this.startMoment.date() / 7);
        this.dayOfWeek = this.startMoment.weekday();

        this.connection = mongoose.connect(mongodb.url, function(err) {
            mongoose.connection.db.dropDatabase(function(err, result) {
                cb();
            });
        });
    },
    tearDown: function(cb) {
        mongoose.disconnect();
        cb();

    },
    'occurrences': {
        //{{{ occurrences
        '10 occurrences, contiguous months, day of week': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]', {
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'monthly',
                    repeatEvery: 1,
                    repeatBy: 'day of week',
                    ends: {
                        never: false,
                        occurrences: 10,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 10);
                _.each(series, function(s, i) {
                    //assert members of the series are all on first  monday
                    test.equal(self.dayOfWeek, moment(s.dateTime)
                        .weekday());
                    test.equal(self.weekOfMonth, Math.ceil(self.startMoment.date() / 7));

                    //assert the initial date and this date are i months apart
                    test.equal(self.startMoment.month(), moment(s.dateTime).add(-1*((i)%12), 'months')
                        .month(), '[i='+i+'] ' + moment(s.dateTime).month() + ' - ' + ((i)%12));

                    //assert this date and the last are a month apart by startOf def
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime)
                            .startOf('month');
                        var thisMoment = moment(s.dateTime)
                            .startOf('month');
                        var months = thisMoment.diff(prevMoment, 'months');
                        test.equal(1, months);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || (self.startMoment.isSame(thisMoment, 'year') && self.startMoment.isSame(thisMoment, 'month') && self.startMoment.isSame(thisMoment, 'day')), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        '10 occurrences, every other month, day of week': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]', {
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'monthly',
                    repeatEvery: 2,
                    repeatBy: 'day of week',
                    ends: {
                        never: false,
                        occurrences: 10,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 10);
                _.each(series, function(s, i) {
                    //assert members of the series are all on first  monday
                    test.equal(self.dayOfWeek, moment(s.dateTime)
                        .weekday());
                    test.equal(self.weekOfMonth, Math.ceil(self.startMoment.date() / 7));

                    //assert the initial date and this date are i months apart
                    test.equal(self.startMoment.month(), moment(s.dateTime).add(-1*((i*2)%12), 'months')
                        .month(), '[i='+i+'] ' + moment(s.dateTime).month() + ' - ' + ((i*2)%12));


                    //assert this date and the last are a month apart by startOf def
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime)
                            .startOf('month');
                        var thisMoment = moment(s.dateTime)
                            .startOf('month');
                        var months = thisMoment.diff(prevMoment, 'months');
                        test.equal(2, months);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || (self.startMoment.isSame(thisMoment, 'year') && self.startMoment.isSame(thisMoment, 'month') && self.startMoment.isSame(thisMoment, 'day')), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        '10 occurrences, contiguous months, day of month': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]', {
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'monthly',
                    repeatEvery: 1,
                    repeatBy: 'day of month',
                    ends: {
                        never: false,
                        occurrences: 10,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 10);
                _.each(series, function(s, i) {
                    //assert members of the series are all on the same day
                    test.equal(self.startMoment.date(), moment(s.dateTime)
                        .date());

                    //assert this date and the last are a month apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var months = thisMoment.diff(prevMoment, 'months');
                        test.equal(1, months);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || (self.startMoment.isSame(thisMoment, 'year') && self.startMoment.isSame(thisMoment, 'month') && self.startMoment.isSame(thisMoment, 'day')), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        '10 occurrences, every other month, day of month': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]', {
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'monthly',
                    repeatEvery: 2,
                    repeatBy: 'day of month',
                    ends: {
                        never: false,
                        occurrences: 10,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 10);
                _.each(series, function(s, i) {
                    //assert members of the series are all on first  m
                    test.equal(self.startMoment.date(), moment(s.dateTime)
                        .date());
                    //assert this date and the last are a month apart by startOf def
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var months = thisMoment.diff(prevMoment, 'months');
                        test.equal(2, months);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || (self.startMoment.isSame(thisMoment, 'year') && self.startMoment.isSame(thisMoment, 'month') && self.startMoment.isSame(thisMoment, 'day')), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        //}}}
    },
    'ends on date': {
        // {{{ ends on date
        '20 months later, contiguous months, day of week': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]', {
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'monthly',
                    repeatEvery: 1,
                    repeatBy: 'day of week',
                    ends: {
                        never: false,
                        occurrences: null,
                        on: moment(self.startMoment)
                            .add('months', 20)
                            .toDate()
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 20);
                _.each(series, function(s, i) {
                    //assert members of the series are all on first  monday
                    test.equal(self.dayOfWeek, moment(s.dateTime)
                        .weekday());
                    test.equal(self.weekOfMonth, Math.ceil(self.startMoment.date() / 7));

                    //assert the initial date and this date are i months apart
                    test.equal(self.startMoment.month(), moment(s.dateTime).add(-1*((i)%12), 'months')
                        .month(), '[i='+i+'] ' + moment(s.dateTime).month() + ' - ' + ((i)%12));


                    //assert this date and the last are a month apart by startOf def
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime)
                            .startOf('month');
                        var thisMoment = moment(s.dateTime)
                            .startOf('month');
                        var months = thisMoment.diff(prevMoment, 'months');
                        test.equal(1, months);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || (self.startMoment.isSame(thisMoment, 'year') && self.startMoment.isSame(thisMoment, 'month') && self.startMoment.isSame(thisMoment, 'day')), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        '20 months later, every other month, day of week': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]', {
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'monthly',
                    repeatEvery: 2,
                    repeatBy: 'day of week',
                    ends: {
                        never: false,
                        occurrences: null,
                        on: moment(self.startMoment)
                            .add('months', 20)
                            .toDate()
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 10); //20 months / every other
                _.each(series, function(s, i) {
                    //assert members of the series are all on first  monday
                    test.equal(self.dayOfWeek, moment(s.dateTime)
                        .weekday());
                    test.equal(self.weekOfMonth, Math.ceil(self.startMoment.date() / 7));

                    //assert the initial date and this date are i months apart
                    test.equal(self.startMoment.month(), moment(s.dateTime).add(-1*((i*2)%12), 'months')
                        .month(), '[i='+i+'] ' + moment(s.dateTime).month() + ' - ' + ((i*2)%12));


                    //assert this date and the last are a month apart by startOf def
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime)
                            .startOf('month');
                        var thisMoment = moment(s.dateTime)
                            .startOf('month');
                        var months = thisMoment.diff(prevMoment, 'months');
                        test.equal(2, months);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || (self.startMoment.isSame(thisMoment, 'year') && self.startMoment.isSame(thisMoment, 'month') && self.startMoment.isSame(thisMoment, 'day')), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        '20 months later, contiguous months, day of month': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]', {
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'monthly',
                    repeatEvery: 1,
                    repeatBy: 'day of month',
                    ends: {
                        never: false,
                        occurrences: 10,
                        on: moment(self.startMoment)
                            .add('months', 20)
                            .toDate()
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 10);
                _.each(series, function(s, i) {
                    //assert members of the series are all on the 6th
                    test.equal(self.startMoment.date(), moment(s.dateTime)
                        .date());

                    //assert this date and the last are a month apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var months = thisMoment.diff(prevMoment, 'months');
                        test.equal(1, months);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || (self.startMoment.isSame(thisMoment, 'year') && self.startMoment.isSame(thisMoment, 'month') && self.startMoment.isSame(thisMoment, 'day')), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        '20 months later, every other month, day of month': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]', {
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'monthly',
                    repeatEvery: 2,
                    repeatBy: 'day of month',
                    ends: {
                        never: false,
                        occurrences: 10,
                        on: moment(self.startMoment)
                            .add('months', 20)
                            .toDate()
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 10);
                _.each(series, function(s, i) {
                    //assert members of the series are all on first  m
                    test.equal(self.startMoment.date(), moment(s.dateTime)
                        .date());
                    //assert this date and the last are a month apart by startOf def
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var months = thisMoment.diff(prevMoment, 'months');
                        test.equal(2, months);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || (self.startMoment.isSame(thisMoment, 'year') && self.startMoment.isSame(thisMoment, 'month') && self.startMoment.isSame(thisMoment, 'day')), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        //}}} 
    },
    'forever': {
        //{{{ forever :( 
        'contiguous months, day of week': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]', {
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'monthly',
                    repeatEvery: 1,
                    repeatBy: 'day of week',
                    ends: {
                        never: true,
                        occurrences: null,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.ok(series.length > 20);
                _.each(series, function(s, i) {
                    //assert members of the series are all on first  monday
                    test.equal(self.dayOfWeek, moment(s.dateTime)
                        .weekday());
                    test.equal(self.weekOfMonth, Math.ceil(self.startMoment.date() / 7));

                    //assert the initial date and this date are i months apart
                    test.equal(self.startMoment.month(), moment(s.dateTime).add(-1*((i)%12), 'months')
                        .month(), '[i='+i+'] ' + moment(s.dateTime).month() + ' - ' + ((i)%12));

                    //assert this date and the last are a month apart by startOf def
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime)
                            .startOf('month');
                        var thisMoment = moment(s.dateTime)
                            .startOf('month');
                        var months = thisMoment.diff(prevMoment, 'months');
                        test.equal(1, months);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || (self.startMoment.isSame(thisMoment, 'year') && self.startMoment.isSame(thisMoment, 'month') && self.startMoment.isSame(thisMoment, 'day')), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        'every other month, day of week': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]', {
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'monthly',
                    repeatEvery: 2,
                    repeatBy: 'day of week',
                    ends: {
                        never: true,
                        occurrences: null,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.ok(series.length > 10); //20 months / every other
                _.each(series, function(s, i) {
                    //assert members of the series are all on first  monday
                    test.equal(self.dayOfWeek, moment(s.dateTime)
                        .weekday());
                    test.equal(self.weekOfMonth, Math.ceil(self.startMoment.date() / 7));

                    //assert the initial date and this date are i months apart
                    test.equal(self.startMoment.month(), moment(s.dateTime).add(-1*((i*2)%12), 'months')
                        .month(), '[i='+i+'] ' + moment(s.dateTime).month() + ' - ' + ((i*2)%12));

                    //assert this date and the last are a month apart by startOf def
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime)
                            .startOf('month');
                        var thisMoment = moment(s.dateTime)
                            .startOf('month');
                        var months = thisMoment.diff(prevMoment, 'months');
                        test.equal(2, months);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || (self.startMoment.isSame(thisMoment, 'year') && self.startMoment.isSame(thisMoment, 'month') && self.startMoment.isSame(thisMoment, 'day')), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        'contiguous months, day of month': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]', {
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'monthly',
                    repeatEvery: 1,
                    repeatBy: 'day of month',
                    ends: {
                        never: true,
                        occurrences: null,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.ok(series.length > 10);
                _.each(series, function(s, i) {
                    //assert members of the series are all on the 6th
                    test.equal(self.startMoment.date(), moment(s.dateTime)
                        .date());

                    //assert this date and the last are a month apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var months = thisMoment.diff(prevMoment, 'months');
                        test.equal(1, months);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || (self.startMoment.isSame(thisMoment, 'year') && self.startMoment.isSame(thisMoment, 'month') && self.startMoment.isSame(thisMoment, 'day')), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        'every other month, day of month': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]', {
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'monthly',
                    repeatEvery: 2,
                    repeatBy: 'day of month',
                    ends: {
                        never: true,
                        occurrences: null,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.ok(series.length > 10);
                _.each(series, function(s, i) {
                    //assert members of the series are all on first  m
                    test.equal(self.startMoment.date(), moment(s.dateTime)
                        .date());
                    //assert this date and the last are a month apart by startOf def
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var months = thisMoment.diff(prevMoment, 'months');
                        test.equal(2, months);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || (self.startMoment.isSame(thisMoment, 'year') && self.startMoment.isSame(thisMoment, 'month') && self.startMoment.isSame(thisMoment, 'day')), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        //}}}
    }
};
