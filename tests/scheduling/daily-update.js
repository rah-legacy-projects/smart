var mongoose = require('mongoose'),
    testUtil = require('../testUtil'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    mongodb = require('../config')
    .mongodb,
    context = require('../../models/context'),
    logger = require('../config').logger;


module.exports = exports = {
    setUp: function(cb) {
        process.on('uncaughtException', function(err) {
            logger.error(util.inspect(err));
            logger.error(err.stack);
        });

        logger.info('start');
        this.startMoment = moment(new Date(2014, 0, 6))//jan 6, 2014 (a monday)
        this.connection = mongoose.connect(mongodb.url, function(err) {
            mongoose.connection.db.dropDatabase(function(err, result) {
                cb();
            });
        });
    },
    tearDown: function(cb) {
        mongoose.disconnect();
        cb();

    },
    'daily series: 10 occurrences, last 5 moved to another time and place': function(test) {
        var self = this;
        context.ScheduleSeries.createSeries('[unit test]', {
            startDateTime: self.startMoment.toDate(),
            location: "here",
            duration: 30,
            repeats: {
                repeatType: 'daily',
                repeatEvery: null,
                repeatBy: null,
                ends: {
                    never: false,
                    occurrences: 10,
                    on: null
                }
            }
        }, function(err, sa) {
            test.ifError(err);
            test.ok(sa);
            test.equal(10, sa.length);
            context.ScheduleSeries.findOne({
                _id: sa[0].scheduleSeries
            })
                .exec(function(err, series) {
                    test.ifError(err, 'problem getting series');
                    test.ok(series);
                    series.updateSeries('[unit test]', {
                        originatingSchedule: sa[5]._id,
                        updateType: 'forward',
                        newSchedule: {
                            startDateTime: moment(self.startMoment)
                                .add(5, 'days')
                                .add(4, 'hours')
                                .toDate(),
                            location: "over there",
                            duration: 30,
                        }
                    }, function(err, supdate) {
                        test.ifError(err);
                        test.ok(supdate);

                        var tasks = {
                            checkupdated: function(tcb) {
                                test.equal(5, supdate.length);
                                test.notEqual(sa[0].scheduleSeries, supdate[0].scheduleSeries);
                                //assert every new one is 'over there' and 4 hours off of the startmoment
                                _.each(supdate, function(su, sui) {
                                    test.equal('over there', su.location);
                                    test.equal(4, moment(su.dateTime)
                                        .diff(moment(sa[sui + 5].dateTime), 'hours'));
                                });
                                tcb(null, true);
                            },
                            checkoriginal: function(tcb) {
                                context.Schedule.find({
                                    scheduleSeries: sa[0].scheduleSeries
                                })
                                    .exec(function(err, ss) {
                                        test.equal(5, ss.length);
                                        test.notEqual(sa[0].scheduleSeries, supdate[0].scheduleSeries);
                                        //assert every new one is 'over there' and 4 hours off of the startmoment
                                        _.each(ss, function(sse, ssei) {
                                            test.equal('here', sse.location);
                                            test.equal(0, moment(sse.dateTime)
                                                .diff(moment(sa[ssei].dateTime), 'hours'));
                                        });
                                        tcb(null, true);

                                    });
                            }
                        };

                        async.series(tasks, function(err, r) {
                            test.done();
                        });
                    })
                });
        });
    },
    'daily series: 10 occurrences, last 5 moved to weekly on tuesdays': function(test) {
        var self = this;
        context.ScheduleSeries.createSeries('[unit test]', {
            startDateTime: self.startMoment.toDate(),
            location: "here",
            duration: 30,
            repeats: {
                repeatType: 'daily',
                repeatEvery: null,
                repeatBy: null,
                ends: {
                    never: false,
                    occurrences: 10,
                    on: null
                }
            }
        }, function(err, sa) {
            test.ifError(err);
            test.ok(sa);
            test.equal(10, sa.length);
            context.ScheduleSeries.findOne({
                _id: sa[0].scheduleSeries
            })
                .exec(function(err, series) {
                    test.ifError(err, 'problem getting series');
                    test.ok(series);
                    series.updateSeries('[unit test]', {
                        originatingSchedule: sa[5]._id,
                        updateType: 'forward',
                        newSchedule: {
                            startDateTime: moment(self.startMoment)
                                .add(5, 'days')
                                .add(4, 'hours')
                                .toDate(),
                            location: "over there",
                            duration: 30,
                            repeats:{
                                repeatType: 'weekly',
                                repeatEvery: 1,
                                repeatBy: 'Tuesday'
                            }
                        }
                    }, function(err, supdate) {
                        test.ifError(err);
                        test.ok(supdate);

                        var tasks = {
                            checkupdated: function(tcb) {
                                test.equal(5, supdate.length);
                                test.notEqual(sa[0].scheduleSeries, supdate[0].scheduleSeries);
                                //assert every new one is 'over there' and 4 hours off of the startmoment
                                _.each(supdate, function(su, sui) {
                                    test.equal('over there', su.location);
                                    //todo: check time
                                    test.equal(2, moment(su.dateTime).day());
                                });
                                tcb(null, true);
                            },
                            checkoriginal: function(tcb) {
                                context.Schedule.find({
                                    scheduleSeries: sa[0].scheduleSeries
                                })
                                    .exec(function(err, ss) {
                                        test.equal(5, ss.length);
                                        test.notEqual(sa[0].scheduleSeries, supdate[0].scheduleSeries);
                                        //assert every new one is 'over there' and 4 hours off of the startmoment
                                        _.each(ss, function(sse, ssei) {
                                            test.equal('here', sse.location);
                                            test.equal(0, moment(sse.dateTime)
                                                .diff(moment(sa[ssei].dateTime), 'hours'));
                                        });
                                        tcb(null, true);

                                    });
                            }
                        };

                        async.series(tasks, function(err, r) {
                            test.done();
                        });
                    })
                });
        });
    }
}
