var mongoose = require('mongoose'),
    testUtil = require('../testUtil'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    mongodb = require('../config')
    .mongodb,
    context = require('../../models/context'),
    logger = require('../config').logger;



module.exports = exports = {
    setUp: function(cb) {
        this.startMoment = moment(new Date(2014, 0, 6)) //jan 6, 2014 (a monday)
this.connection = mongoose.connect(mongodb.url, function(err) {
            mongoose.connection.db.dropDatabase(function(err, result) {
                cb();
            });
        });
    },
    tearDown: function(cb) {
        mongoose.disconnect();
        cb();

    },
    'occurrences': {
        //{{{ occurrences
        '10 occurrences, contiguous years': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]', {
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'yearly',
                    repeatEvery: 1,
                    repeatBy: null,
                    ends: {
                        never: false,
                        occurrences: 10,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 10);
                _.each(series, function(s, i) {
                    //assert members of the series are all on same day of year
                    test.equal(self.startMoment.dayOfYear(), moment(s.dateTime)
                        .dayOfYear());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var years = thisMoment.diff(prevMoment, 'years');
                        test.equal(1, years);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || (self.startMoment.isSame(thisMoment, 'year') && self.startMoment.isSame(thisMoment, 'month') && self.startMoment.isSame(thisMoment, 'day')), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        '10 occurrences, every other year': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'yearly',
                    repeatEvery: 2,
                    repeatBy: null,
                    ends: {
                        never: false,
                        occurrences: 10,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(10, series.length);
                _.each(series, function(s, i) {
                    //assert members of the series are all on same day of year
                    test.equal(self.startMoment.dayOfYear(), moment(s.dateTime)
                        .dayOfYear());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var years = thisMoment.diff(prevMoment, 'years');
                        test.equal(2, years);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || (self.startMoment.isSame(thisMoment, 'year') && self.startMoment.isSame(thisMoment, 'month') && self.startMoment.isSame(thisMoment, 'day')), 'first occurrence is not after today');

                    }
                });
                test.done();
            });
        },
        //}}}
    },
    'ends on date': {
        // {{{ ends on date
        '20 years after, contiguous years': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'yearly',
                    repeatEvery: 1,
                    repeatBy: null,
                    ends: {
                        never: false,
                        occurrences: null,
                        on: moment(self.startMoment).add(20, 'years').toDate()
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 20);
                _.each(series, function(s, i) {
                    //assert members of the series are all on same day of year
                    test.equal(self.startMoment.dayOfYear(), moment(s.dateTime)
                        .dayOfYear());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var years = thisMoment.diff(prevMoment, 'years');
                        test.equal(1, years);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || (self.startMoment.isSame(thisMoment, 'year') && self.startMoment.isSame(thisMoment, 'month') && self.startMoment.isSame(thisMoment, 'day')), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        '20 years after, every other year': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'yearly',
                    repeatEvery: 2,
                    repeatBy: null,
                    ends: {
                        never: false,
                        occurrences: null,
                        on: moment(self.startMoment).add(20, 'years').toDate()
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(10, series.length);
                _.each(series, function(s, i) {
                    //assert members of the series are all on same day of year
                    test.equal(self.startMoment.dayOfYear(), moment(s.dateTime)
                        .dayOfYear());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var years = thisMoment.diff(prevMoment, 'years');
                        test.equal(2, years);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || (self.startMoment.isSame(thisMoment, 'year') && self.startMoment.isSame(thisMoment, 'month') && self.startMoment.isSame(thisMoment, 'day')), 'first occurrence is not after today');

                    }
                });
                test.done();
            });
        },
        //}}}
    },
    'forever': {

        
        //{{{ forever :(
        'contiguous years': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'yearly',
                    repeatEvery: 1,
                    repeatBy: null,
                    ends: {
                        never: true,
                        occurrences: null,
                        on: null                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.ok(series.length > 20);
                _.each(series, function(s, i) {
                    //assert members of the series are all on same day of year
                    test.equal(self.startMoment.dayOfYear(), moment(s.dateTime)
                        .dayOfYear());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var years = thisMoment.diff(prevMoment, 'years');
                        test.equal(1, years);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || (self.startMoment.isSame(thisMoment, 'year') && self.startMoment.isSame(thisMoment, 'month') && self.startMoment.isSame(thisMoment, 'day')), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        'every other year': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'yearly',
                    repeatEvery: 2,
                    repeatBy: null,
                    ends: {
                        never: true,
                        occurrences: null,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.ok(10 < series.length);
                _.each(series, function(s, i) {
                    //assert members of the series are all on same day of year
                    test.equal(self.startMoment.dayOfYear(), moment(s.dateTime)
                        .dayOfYear());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var years = thisMoment.diff(prevMoment, 'years');
                        test.equal(2, years);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment) || (self.startMoment.isSame(thisMoment, 'year') && self.startMoment.isSame(thisMoment, 'month') && self.startMoment.isSame(thisMoment, 'day')), 'first occurrence is not after today');

                    }
                });
                test.done();
            });
        },
        //}}}
    }
};
