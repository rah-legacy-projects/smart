var mongoose = require('mongoose'),
    testUtil = require('../testUtil'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    mongodb = require('../config')
    .mongodb,
    context = require('../../models/context'),
    logger = require('../config').logger;
module.exports = exports = {
    setUp: function(cb) {
        this.startMoment = moment(new Date(2014, 0, 6)) //jan 6, 2014 (a monday)
        this.connection = mongoose.connect(mongodb.url, function(err) {
            mongoose.connection.db.dropDatabase(function(err, result) {
                cb();
            });
        });

    },
    tearDown: function(cb) {
        mongoose.disconnect();
        cb();

    },
    'occurrences': {
        //{{{ occurrences
        '10 occurrences, contiguous weeks, tuesdays': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]', {
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 1,
                    repeatBy: 'tuesday',
                    ends: {
                        never: false,
                        occurrences: 10,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 10);
                _.each(series, function(s, i) {
                    //assert members of the series are all on tuesday
                    test.equal(2, moment(s.dateTime)
                        .weekday());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(1, weeks);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        '10 occurrences, every other week, tuesdays': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 2,
                    repeatBy: 'tuesday',
                    ends: {
                        never: false,
                        occurrences: 10,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 10);
                _.each(series, function(s, i) {
                    //assert members of the series are all on tuesday
                    test.equal(2, moment(s.dateTime)
                        .weekday());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(2, weeks);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        '10 occurrences, contiguous weeks, sundays': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 1,
                    repeatBy: 'sunday',
                    ends: {
                        never: false,
                        occurrences: 10,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 10);
                _.each(series, function(s, i) {
                    //assert members of the series are all on tuesday
                    test.equal(0, moment(s.dateTime)
                        .weekday());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(1, weeks);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        '10 occurrences, every other week, sundays': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 2,
                    repeatBy: 'sunday',
                    ends: {
                        never: false,
                        occurrences: 10,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 10);
                _.each(series, function(s, i) {
                    //assert members of the series are all on sunday
                    test.equal(0, moment(s.dateTime)
                        .weekday());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(2, weeks);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        '10 occurrences, every other week, tuesday thursday': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 2,
                    repeatBy: 'tuesday,thursday',
                    ends: {
                        never: false,
                        occurrences: 10,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 20); //two days * 10 occurrences
                _.each(series, function(s, i) {
                    //assert members of the series are all on sunday
                    if (i % 2 == 0) {
                        //even indices will fall on tuesday
                        test.equal(2, moment(s.dateTime)
                            .weekday());

                    } else {
                        //odd indices will fall on thursday
                        test.equal(4, moment(s.dateTime)
                            .weekday());
                    }
                    if (i == 0) {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment), 'first occurrence is not after today');
                    }

                    if (i >= 2) {
                        //make sure the tuesdays and thursdays are two weeks apart
                        var prevMoment = moment(series[i - 2].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(2, weeks);
                    }
                });
                test.done();
            });
        },
        '10 occurrences, every week, tuesday thursday': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 1,
                    repeatBy: 'tuesday,thursday',
                    ends: {
                        never: false,
                        occurrences: 10,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 20); //two days * 10 occurrences
                _.each(series, function(s, i) {
                    //assert members of the series are all on sunday
                    if (i % 2 == 0) {
                        //even indices will fall on tuesday
                        test.equal(2, moment(s.dateTime)
                            .weekday());

                    } else {
                        //odd indices will fall on thursday
                        test.equal(4, moment(s.dateTime)
                            .weekday());
                    }
                    if (i == 0) {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment), 'first occurrence is not after today');
                    }

                    if (i >= 2) {
                        //make sure the tuesdays and thursdays are two weeks apart
                        var prevMoment = moment(series[i - 2].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(1, weeks);
                    }
                });
                test.done();
            });
        },

        //}}}
    },
    'ends on date': {
        // {{{ ends on date
        '20 weeks later, contiguous weeks, tuesdays': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 1,
                    repeatBy: 'tuesday',
                    ends: {
                        never: false,
                        occurrences: null,
                        on: moment(self.startMoment)
                            .add('weeks', 20)
                            .toDate()
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(20, series.length);
                _.each(series, function(s, i) {
                    //assert members of the series are all on tuesday
                    test.equal(2, moment(s.dateTime)
                        .weekday());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(1, weeks);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        '20 weeks later, every other week, tuesdays': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 2,
                    repeatBy: 'tuesday',
                    ends: {
                        never: false,
                        occurrences: null,
                        on: moment(self.startMoment)
                            .add('weeks', 20)
                            .toDate()
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 10); //20 weeks / every other (2) weeks = 10 occurrences
                _.each(series, function(s, i) {
                    //assert members of the series are all on tuesday
                    test.equal(2, moment(s.dateTime)
                        .weekday());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(2, weeks);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        '20 weeks later, contiguous weeks, sundays': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 1,
                    repeatBy: 'sunday',
                    ends: {
                        never: false,
                        occurrences: null,
                        on: moment(self.startMoment)
                            .add('weeks', 20)
                            .toDate()
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 20);
                _.each(series, function(s, i) {
                    //assert members of the series are all on tuesday
                    test.equal(0, moment(s.dateTime)
                        .weekday());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(1, weeks);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        '20 weeks later, every other week, sundays': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 2,
                    repeatBy: 'sunday',
                    ends: {
                        never: false,
                        occurrences: 10,
                        on: moment(self.startMoment)
                            .add('weeks', 20)
                            .toDate()
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 10);
                _.each(series, function(s, i) {
                    //assert members of the series are all on sunday
                    test.equal(0, moment(s.dateTime)
                        .weekday());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(2, weeks);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        '20 weeks later, every other week, tuesday thursday': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 2,
                    repeatBy: 'tuesday,thursday',
                    ends: {
                        never: false,
                        occurrences: 10,
                        on: moment(self.startMoment)
                            .add('weeks', 20)
                            .toDate()
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 20); //two days * (20 occurrences/every other)
                _.each(series, function(s, i) {
                    //assert members of the series are all on sunday
                    if (i % 2 == 0) {
                        //even indices will fall on tuesday
                        test.equal(2, moment(s.dateTime)
                            .weekday());

                    } else {
                        //odd indices will fall on thursday
                        test.equal(4, moment(s.dateTime)
                            .weekday());
                    }
                    if (i == 0) {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment), 'first occurrence is not after today');
                    }

                    if (i >= 2) {
                        //make sure the tuesdays and thursdays are two weeks apart
                        var prevMoment = moment(series[i - 2].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(2, weeks);
                    }
                });
                test.done();
            });
        },
        '20 weeks later, every week, tuesday thursday': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 1,
                    repeatBy: 'tuesday,thursday',
                    ends: {
                        never: false,
                        occurrences: null,
                        on: moment(self.startMoment)
                            .add('weeks', 20)
                            .toDate()
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.equal(series.length, 40); //two days * 20 weeks (contiguous)
                _.each(series, function(s, i) {
                    //assert members of the series are all on sunday
                    if (i % 2 == 0) {
                        //even indices will fall on tuesday
                        test.equal(2, moment(s.dateTime)
                            .weekday());

                    } else {
                        //odd indices will fall on thursday
                        test.equal(4, moment(s.dateTime)
                            .weekday());
                    }
                    if (i == 0) {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment), 'first occurrence is not after today');
                    }

                    if (i >= 2) {
                        //make sure the tuesdays and thursdays are two weeks apart
                        var prevMoment = moment(series[i - 2].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(1, weeks);
                    }
                });
                test.done();
            });
        },
        //}}}
    },
    'forever': {
        //{{{ forever :(
        'contiguous weeks, tuesdays': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 1,
                    repeatBy: 'tuesday',
                    ends: {
                        never: true,
                        occurrences: null,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.ok(series.length > 20);
                _.each(series, function(s, i) {
                    //assert members of the series are all on tuesday
                    test.equal(2, moment(s.dateTime)
                        .weekday());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(1, weeks);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        'every other week, tuesdays': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 2,
                    repeatBy: 'tuesday',
                    ends: {
                        never: true,
                        occurrences: null,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.ok(series.length > 20);
                _.each(series, function(s, i) {
                    //assert members of the series are all on tuesday
                    test.equal(2, moment(s.dateTime)
                        .weekday());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(2, weeks);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        'contiguous weeks, sundays': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 1,
                    repeatBy: 'sunday',
                    ends: {
                        never: true,
                        occurrences: null,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.ok(series.length > 20);
                _.each(series, function(s, i) {
                    //assert members of the series are all on tuesday
                    test.equal(0, moment(s.dateTime)
                        .weekday());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(1, weeks);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        'every other week, sundays': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 2,
                    repeatBy: 'sunday',
                    ends: {
                        never: true,
                        occurrences: null,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.ok(series.length > 20);
                _.each(series, function(s, i) {
                    //assert members of the series are all on sunday
                    test.equal(0, moment(s.dateTime)
                        .weekday());
                    //assert this date and the last are only a week apart
                    if (i > 0) {
                        var prevMoment = moment(series[i - 1].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(2, weeks);
                    } else {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment), 'first occurrence is not after today');
                    }
                });
                test.done();
            });
        },
        'every other week, tuesday thursday': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 2,
                    repeatBy: 'tuesday,thursday',
                    ends: {
                        never: true,
                        occurrences: null,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.ok(series.length > 20); //two days * (20 occurrences/every other)
                _.each(series, function(s, i) {
                    //assert members of the series are all on sunday
                    if (i % 2 == 0) {
                        //even indices will fall on tuesday
                        test.equal(2, moment(s.dateTime)
                            .weekday());

                    } else {
                        //odd indices will fall on thursday
                        test.equal(4, moment(s.dateTime)
                            .weekday());
                    }
                    if (i == 0) {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment), 'first occurrence is not after today');
                    }

                    if (i >= 2) {
                        //make sure the tuesdays and thursdays are two weeks apart
                        var prevMoment = moment(series[i - 2].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(2, weeks);
                    }
                });
                test.done();
            });
        },
        'every week, tuesday thursday': function(test) {
            var self = this;
            context.ScheduleSeries.createSeries('[unit test]',{
                startDateTime: self.startMoment.toDate(),
                location: "here",
                duration: 30,
                repeats: {
                    repeatType: 'weekly',
                    repeatEvery: 1,
                    repeatBy: 'tuesday,thursday',
                    ends: {
                        never: true,
                        occurrences: null,
                        on: null
                    }
                }
            }, function(err, series) {
                test.ifError(err);
                test.notEqual(null, series);
                test.ok(series.length > 40); //two days * 20 weeks (contiguous)
                _.each(series, function(s, i) {
                    //assert members of the series are all on sunday
                    if (i % 2 == 0) {
                        //even indices will fall on tuesday
                        test.equal(2, moment(s.dateTime)
                            .weekday());

                    } else {
                        //odd indices will fall on thursday
                        test.equal(4, moment(s.dateTime)
                            .weekday());
                    }
                    if (i == 0) {
                        //make sure the first occurrence is after today
                        var thisMoment = moment(s.dateTime);
                        test.equal(true, self.startMoment.isBefore(thisMoment), 'first occurrence is not after today');
                    }

                    if (i >= 2) {
                        //make sure the tuesdays and thursdays are two weeks apart
                        var prevMoment = moment(series[i - 2].dateTime);
                        var thisMoment = moment(s.dateTime);
                        var weeks = thisMoment.diff(prevMoment, 'weeks');
                        test.equal(1, weeks);
                    }
                });
                test.done();
            });
        },
        //}}}
    }
};
