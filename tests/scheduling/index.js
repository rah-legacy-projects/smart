module.exports = {
    "daily-create": require("./daily-create"),
    "daily-update": require("./daily-update"),
    "index": require("./index"),
    "monthly-create": require("./monthly-create"),
    "weekly-create": require("./weekly-create"),
    "yearly-create": require("./yearly-create")
}
