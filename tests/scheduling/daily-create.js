var mongoose = require('mongoose'),
    testUtil = require('../testUtil'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    mongodb = require('../config')
    .mongodb,
    context = require('../../models/context'),
    logger = require('../config').logger;


module.exports = exports = {
    setUp: function(cb) {
        process.on('uncaughtException', function(err) {
            logger.error(util.inspect(err));
            logger.error(util.inspect(err.stack));
        });
        logger.info('start');
        this.startMoment = moment(new Date(2014, 0, 6))//jan 6, 2014 (a monday)
        this.connection = mongoose.connect(mongodb.url, function(err) {
            mongoose.connection.db.dropDatabase(function(err, result) {
                cb();
            });
        });

    },
    tearDown: function(cb) {
        mongoose.disconnect();
        cb();

    },
    'make daily series: 10 occurrences': function(test) {
        var self = this;
        logger.silly('hello');
        context.ScheduleSeries.createSeries('[unit test]', {
            startDateTime: self.startMoment.toDate(),
            location: "here",
            duration: 30,
            repeats: {
                repeatType: 'daily',
                repeatEvery: null,
                repeatBy: null,
                ends: {
                    never: false,
                    occurrences: 10,
                    on: null
                }
            }
        }, function(err, series) {
            logger.silly('derp');
            test.ifError(err);
            test.notEqual(null, series);
            test.equal(series.length, 10);
            test.done();
        });
    },
    'make daily series: ends on date 20 days from now': function(test) {
        var self = this;
        context.ScheduleSeries.createSeries('[unit test]',{
            startDateTime: self.startMoment.toDate(),
            location: "here",
            duration: 30,
            repeats: {
                repeatType: 'daily',
                repeatEvery: null,
                repeatBy: null,
                ends: {
                    never: false,
                    occurrences: null,
                    on: moment(self.startMoment)
                        .add('days', 20)
                        .toDate()
                }
            }
        }, function(err, series) {
            test.ifError(err);
            test.notEqual(null, series);
            test.equal(series.length, 20);
            test.done();
        });
    },
    'make daily series: forever': function(test) {
        var self= this;
        context.ScheduleSeries.createSeries('[unit test]',{
            startDateTime: self.startMoment.toDate(),
            location: "here",
            duration: 30,
            repeats: {
                repeatType: 'daily',
                repeatEvery: null,
                repeatBy: null,
                ends: {
                    never: true,
                    occurrences: null,
                    on: null
                }
            }
        }, function(err, series) {
            test.ifError(err);
            test.notEqual(null, series);
            //make sure the last one is in 2025?

            test.done();
        });
    }

};
