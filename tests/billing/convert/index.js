module.exports = {
    "base": require("./base"),
    "index.js": require("./index.js"),
    "many": require("./many"),
    "many-singlestudent": require("./many-singlestudent"),
    "single": require("./single")
};
