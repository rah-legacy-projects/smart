var mongoose = require('mongoose'),
    testUtil = require('../../../testUtil'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    mongodb = require('../../../config')
    .mongodb,
    moment = require('moment'),
    context = require('../../../../models/context'),
    logger = require('../../../config')
    .logger,
    billable = require('../../../../billing/billable'),
    converter = require('../../../../billing/converter'),
    path = require('path'),
    fs = require('fs');
module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        this.connection = mongoose.connect(mongodb.url, function(err) {
            //destroy the existing test database
            //done for safety
            mongoose.connection.db.dropDatabase(function(err, result) {
                //like the billing tests, run the imports in setup
                testUtil.importDirectory(path.join(__dirname, 'data'), function(err) {
                    cb(err);
                });
            });
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'single session convert': function(test) {
        billable({
            startDate: moment('2014-01-01')
                .toDate(),
            endDate: moment('2014-12-31')
                .toDate()
        }, function(err, flats) {
            test.ifError(err);
            //make sure there are one rejection and no flats
            test.equal(flats.length, 32);
            if (flats.length > 0) {
                test.ok(_.all(flats, function(flat){
                    return flat.errors.length == 0;
                }));
            }

            //call into the converter with the converted flat
            converter(null, flats, function(err, billingFileContents) {
                fs.writeFile('./bill.txt', billingFileContents, function(err) {
                    test.ifError(err);
                    test.done();
                });
            });
        });
    },
};
