var mongoose = require('mongoose'),
    testUtil = require('../../testUtil'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    mongodb = require('../../config')
    .mongodb,
    moment = require('moment'),
    context = require('../../../models/context'),
    logger = require('../../config')
    .logger,
    billable = require('../../../billing/billable');
path = require('path');
module.exports = exports = {
    setUp: function(cb) {
        logger.info(__filename);
        this.connection = mongoose.connect(mongodb.url, function(err) {
            //destroy the existing test database
            //done for safety
            mongoose.connection.db.dropDatabase(function(err, result) {
                //like the billing tests, run the imports in setup
                testUtil.importDirectory(path.join(__dirname, 'data'), function(err) {
                    cb(err);
                });
            });
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'Student Expired': function(test) {

        async.series([
            function(cb) {
                //change the district range to expire in the past
                context.ranged.Student.find()
                    .exec(function(err, ranges) {
                        logger.silly('ranges found: ' + ranges.length);
                        var activeRanges = _.filter(ranges, function(r){
                            return r.rangeActive;
                        });
                        logger.silly('_active_ ranges found: ' + activeRanges.length);
                        activeRanges[0].startDate = moment('2013-10-10').toDate();
                        activeRanges[0].endDate = moment('2013-11-11').toDate();
                        activeRanges[0].save(cb);
                    });
            }
        ], function(err) {

            billable({
                startDate: moment('2014-01-01')
                    .toDate(),
                endDate: moment('2014-12-31')
                    .toDate()
            }, function(err, flats, rejections) {
                test.ifError(err);
                test.notEqual(flats[0].errors.length, 0);
                logger.silly('flats: ' + flats.length);

                logger.silly('flat errors: ' + util.inspect(flats[0].errors));
                testUtil.singleFlatError(test, flats, 'studentExpired');
                test.done();
            });
        });
    },
};
