var mongoose = require('mongoose'),
    testUtil = require('../../testUtil'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    mongodb = require('../../config')
    .mongodb,
    moment = require('moment'),
    context = require('../../../models/context'),
    logger = require('../../config')
    .logger,
    billing = require('../../../billing');
path = require('path'),
fs = require('fs');
module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        this.connection = mongoose.connect(mongodb.url, function(err) {
            //destroy the existing test database
            //done for safety
            mongoose.connection.db.dropDatabase(function(err, result) {
                //like the billing tests, run the imports in setup
                testUtil.importDirectory(path.join(__dirname, 'data'), function(err) {
                    cb(err);
                });
            });
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'Failure Rollup': function(test) {
        billing.billable({
            startDate: moment('2014-01-01')
                .toDate(),
            endDate: moment('2014-12-31')
                .toDate()
        }, function(err, flats) {
            test.ifError(err);
            //make sure there are one rejection and no flats
            test.equal(flats.length, 1);

            //call into the converter with the converted flat
            billing.converter(null, flats, function(err, billingFileContents) {
                billing.rollup.markAsBilled(flats, function(err, flats) {
                    billing.rollup.success(flats, function(err, flats) {
                        billing.rollup.rejection(flats, function(err, flats) {
                            //for each flat, check if billed and attendees are billed
                            var checks = [];
                            _.each(flats, function(flat) {
                                test.equal(flat.errors.length, 1);
                                checks.push(function(cb) {
                                    context.Session.findOne({
                                        _id: flat.sessionId
                                    })
                                        .populate('attendees')
                                        .exec(function(err, session) {

                                            test.ifError(session.billedDate);
                                            _.each(session.attendees, function(a) {
                                                test.ifError(a.billedDate);
                                            });
                                            cb(err);
                                        });
                                });

                                checks.push(function(cb) {
                                    logger.silly('attendee id: ' + flat.attendeeId);
                                    context.rollup.Rejected.find({
                                        attendee: context.ObjectId(flat.attendeeId)
                                    })
                                        .exec(function(err, rejected) {
                                            if (err) {
                                                logger.error(err);
                                            }
                                            test.equal(rejected.length, 1);
                                            test.equal(rejected[0].attendee.toString(), flat.attendeeId);
                                            test.ok(rejected[0].absent)
                                            cb(err);
                                        });
                                });
                            });
                            async.parallel(checks, function(err) {
                                test.done();
                            });
                        });
                    });
                });
            });
        });
    },
};
