var logger = require('winston'),
    util = require('util'),
    fs = require('fs'),
    path = require('path'),
    async = require('async'),
    _ = require('lodash'),
    mongoose = require("mongoose"),
    context = require('../models/context'),
    spreadsheetImport = require('spreadsheet-import'),
    xlutil = spreadsheetImport.util,
    importDefs = require('../importDefinitions'),
    moment = require('moment-timezone');

var importOrder = [{
    sheetName: "User Import",
    typeName: "user"
}, {
    sheetName: "District Import",
    typeName: "district"
}, {
    sheetName: "School Import",
    typeName: "school"
}, {
    sheetName: "MCO Import",
    typeName: "mco"
}, {
    sheetName: "Procedure Code Import",
    typeName: 'procedureCode'
}, {
    sheetName: "Diagnostic Code Import",
    typeName: 'diagnosticCode'
}, {
    sheetName: "Procedure Code Value Import",
    typeName: 'procedureCodeValue'
}, {
    sheetName: "Student Import",
    typeName: 'student'
}, {
    sheetName: "IEP Service Import",
    typeName: 'service'
}, {
    sheetName: "Parental Consent Import",
    typeName: 'parentalConsent'
}, {
    sheetName: "MCO Information Import",
    typeName: 'mcoInformation'
}, {
    sheetName: "Prescription Import",
    typeName: 'prescription'
}, {
    sheetName: "Student Diagnostic Code Import",
    typeName: 'studentDiagnosticCode'
}, {
    sheetName: "GDS Test Import",
    typeName: 'gdsTest'
}, {
    sheetName: "Session Import",
    typeName: 'session'
}, {
    sheetName: 'MCO Qualification Import',
    typeName: 'mcoQualification'
}, {
    sheetName: 'Therapist Qualification Import',
    typeName: 'therapistQualification'
}, {
    sheetName: 'GDS BCBST Import',
    typeName: 'gdsBCBST'
}, {
    sheetName: 'GDS UHC Import',
    typeName: 'gdsUHC'
}, {
    sheetName: 'GDS Medicaid Import',
    typeName: 'gdsMedicaid'
}, {
    sheetName: 'Error Codes Import',
    typeName: 'errorCodes'
}, {
    sheetName: 'Remittance Records Import',
    typeName: 'remittanceRecords'
}];


var utilities = {

    imports: function(file, type, cb) {
        var importXLSX = new spreadsheetImport.import(null, null);
        importXLSX.on('sheet warning', function(e) {});

        importXLSX.on('sheet info', function(e) {});

        importXLSX.on('row validation error', function(e) {
            logger.error(util.inspect(e));
        });

        importXLSX.on('sheet validation error', function(e) {
            logger.error(util.inspect(e));
        });

        importXLSX.on('sheet progress', function(e) {});

        importXLSX.on('row progress', function(e) {});

        importXLSX.on('workbook complete', function(e) {});



        logger.silly('import type: ' + type);
        importXLSX.process((new importDefs[type]('[scripted import]', 'America/Chicago'))
            .sheet, 'scripted-import', file, function(err, sheets) {
                cb(err, importXLSX.importData[type]);
            });
    },
    commits: function(data, type, cb) {
        var commitXLSX = new spreadsheetImport.commit();
        commitXLSX.on('commit row progress', function(e) {});

        commitXLSX.on('commit error', function(e) {});

        commitXLSX.process((new(importDefs[type])('[scripted import]', 'America/Chicago')),
            data,
            'scripted-import', function(err, thing) {
                cb(null);
            });

    },
    commitsSession: function(data, type, cb) {
        var sessionTasks = [];
        //group sessions by session identifier

        _.chain(data)
            .groupBy('sessionIdentifier')
            .forIn(function(value, key) {
                sessionTasks.push(function(cb) {
                    //create a schedule at the time
                    /*
                    var startDateTime = moment('1/1/1900')
                        .add(value[0].date, 'days')
                        .add((value[0].time * 24.0), 'hours');
                    */

                    var startDateTime = moment(value[0].date);

                    logger.silly('start date time: ' + startDateTime.format('MM/DD/YYYY hh:mm:ss'));


                    //{{{ tasks for organizer, proc code and services
                    var tasks = {
                        organizer: function(cb) {
                            var nameParts = value[0].organizer.split(' ');
                            context.User.findOne({
                                firstName: new RegExp(nameParts[0], 'i'),
                                lastName: new RegExp(nameParts[1], 'i')
                            })
                                .exec(function(err, organizer) {
                                    cb(err, organizer);
                                });
                        },
                        procedureCode: function(cb) {
                            context.District.findOne({
                                districtCode: value[0].districtCode
                            })
                                .exec(function(err, district) {
                                    logger.silly('%s has id %s and proc code %s', value[0].districtCode,
                                                 district.id,
                                                 value[0].procedureCode);
                                    context.ProcedureCode.findOne({
                                        district: district.id,
                                        procedureCode: value[0].procedureCode
                                    })
                                        .exec(function(err, procedureCode) {
                                            cb(err, procedureCode);
                                        });

                                });
                        },
                        services: function(cb) {
                            //identify students
                            var studentTasks = [];
                            _.each(value, function(importedStudent) {
                                studentTasks.push(function(cb) {
                                    context.Student.find({
                                        studentId: importedStudent.studentId
                                    })
                                        .populate('mcoInformation')
                                        .populate('rangedData')
                                        .exec(function(err, students) {
                                            async.waterfall([

                                                function(cb) {
                                                    context.School.populate(students, {
                                                        path: 'rangedData.school',
                                                        model: 'School'
                                                    }, cb);
                                                },
                                                function(students, cb) {
                                                    context.School.populate(students, {
                                                        path: 'rangedData.school.district',
                                                        model: 'District'
                                                    }, cb);
                                                }
                                            ], function(err, students) {
                                                var student = _.find(students, function(student) {
                                                    return _.any(student.rangedData, function(range) {
                                                        return range.school.district.districtCode == importedStudent.districtCode;
                                                    });
                                                });

                                                cb(err, student);
                                            });

                                        });
                                });
                            });

                            async.parallel(studentTasks, function(err, students) {
                                //find services for those ids + service type
                                //this assumes one service per type per student
                                var serviceTasks = [];
                                _.each(students, function(student) {
                                    serviceTasks.push(function(cb) {
                                        //grab the service from the values
                                        var v = _.find(value, function(val) {
                                            return val.studentId == student.studentId;
                                        });
                                        context.Service.findOne({
                                            service: v.specialty,
                                            student: student.id
                                        })
                                            .exec(function(err, service) {
                                                cb(err, {
                                                    service: service,
                                                    importValue: v
                                                });
                                            });

                                    });
                                });

                                async.parallel(serviceTasks, function(err, services) {
                                    cb(err, services);
                                });
                            });
                        }
                    };
                    //}}}  

                    async.parallel(tasks, function(err, r) {
                        context.ScheduleSeries.createSeries('[unit test]', {
                            startDateTime: startDateTime.toDate(),
                            location: value[0].location,
                            duration: value[0].duration,
                            organizer: r.organizer._id,
                            services: _.map(r.services, function(service) {
                                return service.service.id;
                            }),
                            repeats: {
                                repeatType: 'one time',
                                ends: {
                                    never: false
                                }
                            }
                        }, function(err, series) {
                            var schedule = series[0];
                            var session = new context.Session({
                                provider: r.organizer._id,
                                serviceType: value[0].specialty,
                                procedureCode: !!r.procedureCode ? r.procedureCode.id : null,
                                sessionNotes: value[0].sessionNote,
                                attendees: []
                            });
                            if (/yes/i.test(value[0].finalize)) {
                                session.finalizingUser = r.organizer._id;
                            }

                            var attendeeSaves = [];

                            _.each(r.services, function(v) {
                                attendeeSaves.push(function(cb) {
                                    var attendee = new context.Attendee({
                                        service: v.importValue.specialty,
                                        absent: /yes/.test(v.importValue.absent),
                                        absenceReason: v.importValue.absenceReason,
                                        individualNote: v.importValue.individualNote,
                                        service: v.service._id,
                                        diagnosticCode: v.service.diagnosticCode
                                    });

                                    attendee.save(function(err, attendee) {
                                        if (!!err) {
                                            logger.error(err);
                                        }
                                        cb(err, attendee._id);
                                    })
                                });
                            });

                            async.parallel(attendeeSaves, function(err, attendeeIds) {
                                logger.silly('attendees: ' + util.inspect(attendeeIds));
                                session.attendees = attendeeIds;
                                session.createdBy = r.organizer.emailAddress;
                                session.modifiedBy = r.organizer.emailAddress;
                                session.save(function(err, session) {
                                    if (!!err) {
                                        logger.error(err);
                                    }
                                    schedule.session = session._id;
                                    schedule.save(function(err) {
                                        cb(err);
                                    });
                                });
                            });
                        });
                    });
                });
            });

        async.parallel(sessionTasks, function(err) {
            if (!!err) {
                logger.error(err);
            }
            cb(err);
        });
    },
    seedAreas: function(cb) {
        context.Area.findOne({
            name: /administration/i
        })
            .exec(function(err, admin) {
                if (!admin) {
                    admin = new context.Area();
                    admin.name = 'administration';
                    admin.createdBy = '[autoseed]';
                    admin.modifiedBy = '[autoseed]';
                    admin.save(function(err, saved) {
                        cb(err, saved);
                    });
                } else {
                    logger.silly('administration area already made.  skipping.');
                    cb(null, null);
                }

            });
    }
};
utilities.importDirectory = function(directory, cb) {

    var options = {
        excludeBase: false
    };
    if (_.isString(directory)) {
        logger.silly(' --- directory was a string');
        options.directory = directory;
    } else {
        logger.silly(' --- directory was a hash');
        _.defaults(directory, options);
        options = directory;
        logger.silly('opts: ' + util.inspect(options));
    }

    logger.silly(util.inspect(options));

    async.series([

            function(cb) {
                utilities.seedAreas(cb);
            },
            function(cb) {
                var testSpecificFiles = [],
                    baseFiles = [];
                if (!options.excludeBase) {
                    baseFiles = _.map(fs.readdirSync(path.join(__dirname, 'baseData')), function(f) {
                        return {
                            name: path.join(__dirname, 'baseData', f),
                            basename: path.basename(f)
                        };
                    });

                } else {

                }
                if (fs.existsSync(options.directory)) {
                    testSpecificFiles = _.map(fs.readdirSync(options.directory), function(f) {
                        return {
                            name: path.join(options.directory, f),
                            basename: path.basename(f)
                        };
                    });
                }


                //basefiles - test specifics
                //test specifics
                var files = _.filter(baseFiles, function(bf) {
                    return !_.any(testSpecificFiles, function(tsf) {
                        return bf.basename == tsf.basename;
                    });
                });
                files = files.concat(testSpecificFiles);

                files = _.sortBy(files, function(f) {
                    return f.basename;
                });

                files = _.pluck(files, 'name');

                logger.silly('importing files: ' + util.inspect(files));
                //fs.readdir(directory, function(err, files) {
                var mappingTasks = [];
                _.each(files, function(fileName, ix) {
                    //fileName = path.join(directory, fileName);
                    if (!(/xlsx$/.test(fileName))) {
                        logger.warn(fileName + ' is not importable');
                    } else {
                        mappingTasks.push(function(cb) {
                            xlutil.parse(fileName, function(err, obj) {
                                var index = _.findIndex(importOrder, function(order) {
                                    return _.any(obj.worksheets, function(sheet) {
                                        return order.sheetName == sheet.name;
                                    });
                                });
                                logger.silly('file: ' + fileName + ', ix: ' + index);

                                cb(null, {
                                    index: index,
                                    fileName: fileName
                                });
                            });
                        });
                    }
                });

                async.parallel(mappingTasks, function(err, mappings) {
                    var importTasks = [];
                    _.chain(mappings)
                        .sortBy('index')
                        .each(function(mapping) {
                            importTasks.push(function(cb) {
                                logger.silly(util.inspect(mapping));
                                utilities.imports(mapping.fileName, importOrder[mapping.index].typeName, function(err, data) {
                                    if (importOrder[mapping.index].typeName == 'session') {
                                        utilities.commitsSession(data.data,
                                            importOrder[mapping.index].typeName,
                                            function(err) {
                                                cb(err);
                                            });
                                    } else {
                                        utilities.commits(data,
                                            importOrder[mapping.index].typeName,
                                            function(err) {
                                                if (!!err) {
                                                    logger.error(err);
                                                }
                                                cb(err);
                                            });
                                    }
                                });
                            });
                        });
                    async.series(importTasks, function(err) {
                        cb(err);
                    });
                });
                //});
            }
        ],
        function(err) {
            cb(err);
        });
};

utilities.singleFlatError = function(test, flats, expectedError) {
    //make sure there are one rejection and no flats
    test.ok(flats);
    if (!!flats) {
        test.equal(flats.length, 1);
        if (flats.length > 0) {
            test.ok(flats[0].errors);
            if (!!flats[0].errors) {
                //test.equal(flats[0].errors.length, 1);
                if (flats[0].errors.length > 0) {
                    //ensure the rejection is for the absence
                    test.equal(flats[0].errors[0].rejectionType, expectedError);
                }
            }
        }
    }
};


module.exports = utilities;
