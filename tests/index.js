module.exports = exports = {
    setUp: function(cb) {
        cb();
    },
    tearDown: function(cb) {
        cb();
    },
    scheduling: require('./scheduling'),
    importVerification: require('./importVerification'),
    billing: require('./billing'),
    reporting: require('./reporting'),
    defects: require('./defects')
};
