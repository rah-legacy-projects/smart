var mongoose = require('mongoose'),
    testUtil = require('../testUtil'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async'),
    mongodb = require('../config')
    .mongodb,
    context = require('../../models/context'),
    logger = require('../config')
    .logger;
module.exports = exports = {
    setUp: function(cb) {
        logger.info('start');
        this.connection = mongoose.connect(mongodb.url, function(err) {
            //destroy the existing test database
            //done for safety
            mongoose.connection.db.dropDatabase(function(err, result) {
                //like the billing tests, run the imports in setup
                testUtil.importDirectory('./data', function(err) {
                    cb(err);
                });
            });
        });
    },
    tearDown: function(cb) {
        //destroy the existing test database
        mongoose.disconnect();
        cb();
    },
    'Verify Data Loaded': function(test) {
        async.series([
            function(cb) {
                context.User.find()
                    .exec(function(err, users) {
                        test.ifError(err);
                        test.equal(users.length, 1, "users length: " + users.length);
                        cb(null);
                    });
            },
            function(cb) {
                context.District.find()
                    .exec(function(err, districts) {
                        test.ifError(err);
                        test.equal(districts.length, 1, "districts length: " + districts.length);
                        cb(null);
                    });
            },
            function(cb) {
                context.School.find()
                    .exec(function(err, schools) {
                        test.ifError(err);
                        test.equal(schools.length, 1, "schools length: " + schools.length);
                        cb(null);
                    });
            },
            function(cb) {
                context.MCO.find()
                    .exec(function(err, mcos) {
                        test.ifError(err);
                        test.equal(mcos.length, 1, "mcos length: " + mcos.length);
                        cb(null);
                    });
            },
            function(cb) {
                context.ProcedureCode.find()
                    .exec(function(err, procedureCodes) {
                        test.ifError(err);
                        test.equal(procedureCodes.length, 1, 'procedure codes length: ' + procedureCodes.length);
                        cb(null);
                    });
            },
            function(cb) {
                context.Student.find()
                    .exec(function(err, students) {
                        test.ifError(err);
                        test.equal(students.length, 1, 'students length: ' + students.length);
                        cb(null);
                    });
            },
            function(cb) {
                context.Service.find()
                    .exec(function(err, services) {
                        test.ifError(err);
                        test.equal(services.length, 1, 'services length: ' + services.length);
                        cb(null);
                    });
            }
        ], function(err) {
            test.done();
        });
    },
    'Verify Service Dates': function(test){
        context.Service.findOne().populate('rangedData').exec(function(err, service){
            test.ifError(err);
            test.ok(service);
            test.equal(service.rangedData.length, 2);
            var activeRange = _.find(service.rangedData, function(dr){
                return dr.rangeActive;
            });
            test.equal(moment(activeRange.startDate).format('YYYY-MM-DD'), '2014-06-15');
            test.equal(moment(activeRange.endDate).format('YYYY-MM-DD'), '2015-06-14');
            test.done();
        });
    }
}
