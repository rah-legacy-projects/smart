var userModel = require('../models/user'),
    util = require('util'),
    logger = require('winston'),
    context = require('../models/context'),
    async = require('async'),
    _ = require('underscore');

module.exports.seedUsers = function(cb) {

    var makeUser = function(email, first, last, cb) {
        userModel.findOne({
            emailAddress: email
        }, function(err, user) {
            if (!!err) {
                logger.error('error when seeding %s: %s', email, util.inspect(err));
                cb(err, null);
            } else if (!user) {
                user = new userModel({
                    emailAddress: email,
                    password: 'test',
                    active: true,
                    lastName: last,
                    firstName: first,
                    createdBy: '[autoseed]',
                    modifiedBy: '[autoseed]'
                });
                user.save(function(userSaveErr) {
                    if (!!userSaveErr) {
                        logger.error('error after save: %s', util.inspect(userSaveErr));
                        cb(userSaveErr, null);
                    } else {
                        logger.debug(email + ' seeded');
                        cb(null, user);
                    }
                });
            } else {
                logger.debug(email + ' already seeded');
                cb(null, user);
            }
        });
    };

    async.parallel({
        ross: function(cb) {
            makeUser('ross@treasurylane.com', 'ross', 'hinkley', cb);
        },
        leigh: function(cb) {
            makeUser('leigh@treasurylane.com', 'leigh', 'rogers', cb);
        },
        richard: function(cb) {
            makeUser('richard@treasurylane.com', 'richard', 'brown', cb);
        },
        paul: function(cb) {
            makeUser('paul@treasurylane.com', 'paul', 'oppegard', cb);
        },
        jean: function(cb) {
            makeUser('jean.lauzon@beaconanalytics.com', 'Jean', 'Lauzon', cb);
        },
    }, function(err, r) {
        cb(err, r);
    });
}

module.exports.seedAdministratorArea = function(cb) {
    context.Area.findOne({
        name: /administration/i
    })
        .exec(function(err, admin) {
            if (!admin) {
                admin = new context.Area();
                admin.name = 'administration';
                admin.createdBy = '[autoseed]';
                admin.modifiedBy = '[autoseed]';
                admin.save(function(err, saved) {
                    cb(err, saved);
                });
            } else {
                logger.silly('administration area already made.  skipping.');
            }

        });
};

module.exports.obviousAdministrators = function(cb) {
    tasks = {
        users: function(callback) {
            context.User.find({
                emailAddress: /treasurylane.com$/i
            })
                .exec(function(err, tlsUsers) {
                    logger.silly('found ' + tlsUsers.length + ' tls users');
                    callback(err, tlsUsers);
                });
        },
        admin: function(callback) {
            context.Area.findOne({
                name: /administration/i
            })
                .exec(function(err, area) {
                    logger.silly('area: ' + util.inspect(area));
                    callback(err, area);
                });
        }
    }

    async.parallel(tasks, function(err, r) {
        var saves = [];
        _.each(r.users, function(user) {
            r.admin.setAccess(user, ['Administer']);
            saves.push(function(icb) {
                logger.silly(user.fullName + ' is an obvious administrator');
                user.modifiedBy = '[autoseed]';

                user.save(function(err) {
                    icb(err);
                })
            });
        });
        async.parallel(saves, function(err, r2) {
            cb(err, r2);
        });
    });
};
