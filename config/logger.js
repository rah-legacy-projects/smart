var logger = require('winston'),
    mongo = require('./mongodb');

    require('winston-mongodb').MongoDB;
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    level: 'silly',
    colorize: true
});

logger.add(logger.transports.MongoDB, {
    level: 'silly',
    dbUri: mongo.logUrl(),
    collection: 'logs',
    storeHost: true
});

logger.add(logger.transports.File,{
    level: 'silly',
    filename: 'log-test.txt', 
    timestamp:true,
    colorize:false
});

process.on('uncaughtException', function(x){
    logger.error('UNCAUGHT: ' + x);
    logger.error(x.stack);
});

exports = module.exports = logger;
