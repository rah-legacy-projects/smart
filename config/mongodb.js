var x = {
    url: function(){
        if(process.env.NODE_ENV == 'staging'){
            return 'mongodb://smart:d2C1mu6i587wceYnJIHp@localhost/smart?authSource=admin'
        }else{
            return 'mongodb://localhost/smart'
        }
    },
    logUrl: function(){
        if(process.env.NODE_ENV == 'staging'){
            return 'mongodb://smart:d2C1mu6i587wceYnJIHp@localhost/smart-log?authSource=admin'
        }else{
            return 'mongodb://localhost/smart-log'
        }
    }
};

exports = module.exports = x;
