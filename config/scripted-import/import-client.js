var socket = require('socket.io'),
    io = require('socket.io/node_modules/socket.io-client'),
    ss = require('socket.io-stream'),
    path = require('path'),
    fs = require('fs'),
    logger = require('winston'),
    util = require('util'),
    async = require('async');

//setup

var f = {
    upload: function(filePath, cb) {
        var uploadSocket = io.connect('http://localhost:7450/upload');
        var uploadToken = null;
        uploadSocket.on('upload setToken', function(token) {
            logger.silly('upload token: ' + token);
            uploadToken = token;
        });

        uploadSocket.on('stream-end', function(args) {
            logger.silly('stream end!');
            uploadSocket.disconnect();
            uploadSocket.removeAllListeners('upload setToken');
            uploadSocket.removeAllListeners('stream-end');
            cb(null, uploadToken);
        });
        var stream = ss.createStream();
        ss(uploadSocket)
            .emit('upload', stream, {
                name: path.basename(filePath),
                size: fs.statSync(filePath)
                    .size
            });

        var filestream = fs.createReadStream(filePath);
        filestream.on('end', function() {
            logger.debug('ended file stream');
        });
        filestream.pipe(stream);
    },
    import: function(token, type, cb) {
        logger.silly('token: %s, type: %s', token, type);
        var importSocket = io.connect('http://localhost:7450/import');
        importSocket.on('row validation error', function(rowEvent) {
            logger.warn('%s, col %s, row %s: %s', rowEvent.sheetName, rowEvent.columnName, rowEvent.row, rowEvent.message);
        });
        importSocket.on('sheet validation error', function(sheetEvent) {
            logger.warn('%s: %s', sheetEvent.sheetName, sheetEvent.message);
        });
        importSocket.on('import finish', function(finis) {
            importSocket.disconnect();
            importSocket.removeAllListeners('sheet validation error');
            importSocket.removeAllListeners('row validation error');
            importSocket.removeAllListeners('import finish');
            cb(null);
        });

        importSocket.emit('import start', type, token);
    },
    commit: function(token, type, cb) {
        var commitSocket = io.connect('http://localhost:7450/commit');
        commitSocket.on('commit finish', function(finish) {
            commitSocket.disconnect();
            commitSocket.removeAllListeners('commit finish');
            cb(null);
        });

        commitSocket.emit('commit start', type, token);
    }
};

module.exports.push = function(filePath, type, cb) {
    var basename = path.basename(filePath);
    logger.silly('about to throw up ' + basename + ' of ' + type);
    f.upload(filePath, function(err, token) {
        logger.info('%s (%s) uploaded', basename, type);
        f.import(token, type, function(err) {
            logger.info('%s (%s) imported', basename, type);
            f.commit(token, type, function(err) {
                logger.info('%s (%s) committed', basename, type);
                cb(null);
            });
        });
    });
}

//push('./data/district-import.xlsx', 'district', function(err) {
//    logger.silly('push complete');
//});
