var logger = require('winston'),
    util = require('util'),
    async = require('async'),
    _ = require('lodash'),
    moment = require('moment'),
    mongoose = require("mongoose"),
    context = require('../../models/context'),
    spreadsheetImport = require('spreadsheet-import');

mongoose.connect('mongodb://localhost/smart');
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    level: 'silly',
    colorize: true
});

var processor = function(file, type, cb) {
    var importXLSX = new spreadsheetImport.import(null, null);
    importXLSX.on('sheet warning', function(e) {
        logger.debug(e.message);
    });

    importXLSX.on('sheet info', function(e) {
        logger.debug(e.message);
    });

    importXLSX.on('row validation error', function(e) {
        logger.debug(e.message);
    });

    importXLSX.on('sheet validation error', function(e) {
        logger.debug(e.message);
    });

    importXLSX.on('sheet progress', function(e) {
        logger.debug("sheet " + e.current + '/' + e.total);
    });

    importXLSX.on('row progress', function(e) {
        logger.debug('row ' + e.current + '/' + e.total);
    });

    importXLSX.on('workbook complete', function(e) {});

    var commitXLSX = new spreadsheetImport.commit();
    commitXLSX.on('commit row progress', function(e) {
        logger.debug('row ' + e.current + '/' + e.total);
    });

    commitXLSX.on('commit error', function(e) {
        logger.error(util.inspect(e));
    });

    importXLSX.process(new(require('../../importDefinitions/' + type))('[scripted import]', 
                                'Tue Aug 05 2014 00:00:00 GMT-0400 (EDT)',
                                'Tue Aug 05 2014 23:59:59 GMT-0400 (EDT)')
        .sheet, 'scripted-import', file, function(err, sheets) {
            logger.debug('%s finished importing, committing', type);
            commitXLSX.process(new(require('../../importDefinitions/' + type))('[scripted import]', 
                                'Tue Aug 05 2014 00:00:00 GMT-0400 (EDT)',
                                'Tue Aug 05 2014 23:59:59 GMT-0400 (EDT)'),


                importXLSX.importData[type],
                'scripted-import', function(err, thing) {
                    logger.debug('commit complete for %s', type);
                    cb(null);
                });
        });
};

process.on('uncaughtException', function(err) {
    logger.error('UNCAUGHT: ' + err);
    logger.error('UNCAUGHT: ' + err.stack);
    //mongoose.disconnect();
});

var prereqData = function(cb) {
    context.Area.findOne({
        name: /administration/i
    })
        .exec(function(err, admin) {
            if (!admin) {
                admin = new context.Area();
                admin.name = 'administration';
                admin.save(function(err, saved) {
                    cb(err, saved);
                });
            } else {
                logger.silly('administration area already made.  skipping.');
                cb(err, admin);
            }
        });
};

var dataDir = "./data/";

var tasks = [

    function(cb) {
        prereqData(cb);
    },
    function(cb) {
        processor(dataDir + '00 User Import.xlsx', 'user', cb);
    },
    function(cb) {
        processor(dataDir + '01 District Import.xlsx', 'district', cb);
    },
    /*
    function(cb) {
        processor(dataDir+'02 School Import.xlsx', 'school', cb);
    },
    function(cb) {
        processor(dataDir+'03 MCO Import.xlsx', 'mco', cb);
    },
    function(cb) {
        processor(dataDir+'04 Procedure Code Import.xlsx', 'procedureCode', cb);
    },
    function(cb) {
        processor(dataDir+'05 Diagnosis Code Import.xlsx', 'diagnosticCode', cb);
    },
    function(cb) {
        processor(dataDir+'06 Procedure Code Value Import.xlsx', 'procedureCodeValue', cb);
    },
    function(cb) {
        processor(dataDir+'07 student import.xlsx', 'student', cb);
    },
    function(cb) {
        processor(dataDir+'08 service report.xlsx', 'service', cb);
    }*/
];

async.series(tasks, function(err, r) {
    if (!!err) {
        logger.error(err);
    }
    logger.info('import completed.');
    //give auditing a chance to catch up
    logger.info('cleaning up...');
    setTimeout(function() {
        mongoose.disconnect();
        logger.info('done.');
    }, 5000);
});
