var logger = require('winston'),
    util = require('util'),
    async = require('async'),
    _ = require('lodash'),
    moment = require('moment'),
    mongoose = require("mongoose"),
    context = require('../../models/context'),
    spreadsheetImport = require('spreadsheet-import'),
    path = require('path'),
    testUtil = require('../../tests/testUtil'),
    config = require('../../config'),
    logger = config.logger;

logger.silly('node env: ' + process.env.NODE_ENV);

mongoose.connect(config.mongodb.url());

//mongoose.connect('mongodb://localhost/smart');
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    level: 'silly',
    colorize: true
});

process.on('uncaughtException', function(err) {
    logger.error('UNCAUGHT: ' + err);
    logger.error('UNCAUGHT: ' + err.stack);
    //mongoose.disconnect();
});

logger.silly('about to import directory');
testUtil.importDirectory(path.join(__dirname, 'data'), function(err) {
    if(!!err){
        logger.error(err);
    }
    mongoose.disconnect(function(err){
        logger.silly('done');
    });
});
