var _ = require('lodash'),
    async = require('async'),
    moment = require('moment-timezone'),
    mongoose = require('mongoose');
config = require('../../config')
logger = config.logger;
mongoose.connect(config.mongodb.url());
var context = {
    rollup: {
        auth: mongoose.connection.collection('rpt_authorizationsummary'),
        billingDetail: mongoose.connection.collection('rpt_billingdetail'),
        billingSummary: mongoose.connection.collection('rpt_billingsummary'),
        mcoAuth: mongoose.connection.collection('rpt_mcopriorauthorizationrequringattention'),
        gds: mongoose.connection.collection('rpt_medicaidinformationrequringattention'),
        parentalConsent: mongoose.connection.collection('rpt_parentalconsentsrequringattention'),
        rx: mongoose.connection.collection('rpt_prescriptionsrequiringattention'),
        servicesBilledPerformance: mongoose.connection.collection('rpt_servicesbilledperformance'),
        therapistLicensing: mongoose.connection.collection('rpt_therapistlicensing'),
        summaryClaimingBilledPerformance: mongoose.connection.collection('rpt_summaryclaimingbilledperformance')
    }
};

var dataTasks = {
    auth: function(cb) {
        var data = [];
        data.push({
            studentId: '000001',
            studentName: 'Sue Smith',
            mcoAuthsAboutToExpire: 2,
            mcoClosestExpiration: moment('2014-9-15')
                .tz('America/Chicago')
                .format('MM/DD/YYYY'),
            parentalConsentsAboutToExpire: 1,
            parentalConsentClosestExpiration: moment('2014-9-15')
                .tz('America/Chicago')
                .format('MM/DD/YYYY'),
            prescriptionsAboutToExpire: 1,
            prescriptionClosestExpiration: moment('2014-9-15')
                .tz('America/Chicago')
                .format('MM/DD/YYYY'),

            //there should only ever be one medicaid info
            medicaidInfoClosestExpiration: moment('2014-9-15')
                .tz('America/Chicago')
                .format('MM/DD/YYYY'),

            districtName: 'Valley Springs',
            districtCode: 'VS1'
        });

        data.push({
            studentId: '000002',
            studentName: 'Pete Jones',
            mcoAuthsAboutToExpire: 0,
            mcoClosestExpiration: null,
            parentalConsentsAboutToExpire: 1,
            parentalConsentClosestExpiration: moment('2014-9-15')
                .tz('America/Chicago')
                .format('MM/DD/YYYY'),
            prescriptionsAboutToExpire: 0,
            prescriptionClosestExpiration: null,

            //there should only ever be one medicaid info
            medicaidInfoClosestExpiration: null,

            districtName: 'Valley Springs',
            districtCode: 'VS1'
        });

        async.parallel(_.map(data, function(d) {
            return function(cb) {
                context.rollup.auth.save(d, cb);
            };
        }), cb);
    },
    billingDetail: function(cb) {
        var data = [];
        data.push({
            studentName: 'Sue Smith',
            therapist: 'Joe Watkins',
            districtCode: 'VS1',
            districtName: 'Valley Springs',
            therapy: 'Phsical Therapy',
            amountBilled: 52.88,
            amountPaid: 50.00,
            amountDenied: 2.88,
            percentPaid: (50.00 / 52 / 88) * 100
        });

        data.push({
            studentName: 'Pete Jones',
            therapist: 'Joe Watkins',
            districtCode: 'VS1',
            districtName: 'Valley Springs',
            therapy: 'Phsical Therapy',
            amountBilled: 35.00,
            amountPaid: 0,
            amountDenied: 35.00,
            percentPaid: 0
        });
        async.parallel(_.map(data, function(d) {
            return function(cb) {
                context.rollup.billingDetail.save(d, cb);
            };
        }), cb);
    },
    billingSummary: function(cb) {
        data = [];
        data.push({
            districtCode: 'VS1',
            districtName: 'Valley Springs',
            billed: 300,
            paid: 200,
            denied: 100,
            start: moment('2014-8-1')
                .tz('America/Chicago')
                .format('MM/DD/YYYY'),
            end: moment('2014-8-1')
                .tz('America/Chicago')
                .endOf('month')
                .format('MM/DD/YYYY'),
            monthName: moment('2014-8-1')
                .tz('America/Chicago')
                .format('MMM'),
            month: 8,
            year: moment('2014-8-1')
                .tz('America/Chicago')
                .format('YYYY'),
        });
        data.push({
            districtCode: 'VS1',
            districtName: 'Valley Springs',
            billed: 300,
            paid: 200,
            denied: 100,
            start: moment('2014-8-1')
                .tz('America/Chicago')
                .format('MM/DD/YYYY'),
            end: moment('2014-8-1')
                .tz('America/Chicago')
                .endOf('month')
                .format('MM/DD/YYYY'),
            monthName: moment('2014-8-1')
                .tz('America/Chicago')
                .format('MMM'),
            month: 8,
            year: moment('2014-8-1')
                .tz('America/Chicago')
                .format('YYYY'),
        });
        data.push({
            districtCode: 'VS1',
            districtName: 'Valley Springs',
            billed: 300,
            paid: 200,
            denied: 100,
            start: moment('2014-8-1')
                .tz('America/Chicago')
                .format('MM/DD/YYYY'),
            end: moment('2014-8-1')
                .tz('America/Chicago')
                .endOf('month')
                .format('MM/DD/YYYY'),
            monthName: moment('2014-8-1')
                .tz('America/Chicago')
                .format('MMM'),
            month: 8,
            year: moment('2014-8-1')
                .tz('America/Chicago')
                .format('YYYY'),
        });
        data.push({
            districtCode: 'VS1',
            districtName: 'Valley Springs',
            billed: 500,
            paid: 200,
            denied: 100,
            start: moment('2014-7-1')
                .tz('America/Chicago')
                .format('MM/DD/YYYY'),
            end: moment('2014-7-1')
                .tz('America/Chicago')
                .endOf('month')
                .format('MM/DD/YYYY'),
            monthName: moment('2014-7-1')
                .tz('America/Chicago')
                .format('MMM'),
            month: 7,
            year: moment('2014-7-1')
                .tz('America/Chicago')
                .format('YYYY'),
        });
        async.parallel(_.map(data, function(d) {
            return function(cb) {
                context.rollup.billingSummary.save(d, cb);
            };
        }), cb);
    },
    mcoAuth: function(cb) {
        var data = [];
        data.push({
            internalStudentId: mongoose.Schema.Types.ObjectId(),
            districtCode: 'VS1',
            districtName: 'Valley Springs',
            studentId: '000001',
            studentName: 'Sue Smith',
            specialty: 'Physical Therapy',
            numberOfSessions: 9,
            approvalToTreat: true,
            endDate: moment('2014-9-15').tz('America/Chicago')
                .endOf('month')
                .format('MM/DD/YYYY'),
            mcoName: 'BCBST',
            created: moment()
                .toDate()
        });
        data.push({
            internalStudentId: mongoose.Schema.Types.ObjectId(),
            districtCode: 'VS1',
            districtName: 'Valley Springs',
            studentId: '000002',
            studentName: 'Pete Jones',
            specialty: 'Physical Therapy',
            numberOfSessions: 7,
            approvalToTreat: true,
            endDate: moment('2014-9-15').tz('America/Chicago')
                .endOf('month')
                .format('MM/DD/YYYY'),
            mcoName: 'UHC',
            created: moment()
                .toDate()
        });
        async.parallel(_.map(data, function(d) {
            return function(cb) {
                context.rollup.mcoAuth.save(d, cb);
            };
        }), cb);
    },
    gds: function(cb) {
        var data = [];
        data.push({
            districtCode: 'VS1',
            districtName: 'Valley Springs',
            studentId: '000001',
            studentName: 'Sue Smith',
            endDate: moment('2014-9-15').tz('America/Chicago')
                .toDate(),
            internalStudentId: mongoose.Schema.Types.ObjectId(),
            created: moment()
                .toDate()
        });
        data.push({
            districtCode: 'VS1',
            districtName: 'Valley Springs',
            studentId: '000002',
            studentName: 'Pete Jones',
            endDate: moment('2014-9-15').tz('America/Chicago')
                .endOf('month')
                .format('MM/DD/YYYY'),
            internalStudentId: mongoose.Schema.Types.ObjectId(),
            created: moment()
                .toDate()
        });
        async.parallel(_.map(data, function(d) {
            return function(cb) {
                context.rollup.gds.save(d, cb);
            };
        }), cb);
    },
    parentalConsent: function(cb) {
        var data = [];
        data.push({
            districtCode: 'VS1',
            districtName: 'Valley Springs',
            studentId: '000001',
            studentName: 'Sue Smith',
            toBill: moment('2013-9-15')
                .endOf('month')
                .format('MM/DD/YYYY'),
            toTreat: moment('2013-9-15')
                .endOf('month')
                .format('MM/DD/YYYY'),
            endDate: moment('2014-9-15')
                .endOf('month')
                .format('MM/DD/YYYY'),
            internalStudentId: mongoose.Schema.Types.ObjectId(),
            created: moment()
        });
        data.push({
            districtCode: 'VS1',
            districtName: 'Valley Springs',
            studentId: '000002',
            studentName: 'Pete Jones',
            toBill: moment('2013-9-15')
                .endOf('month')
                .format('MM/DD/YYYY'),
            toTreat: moment('2013-9-15')
                .endOf('month')
                .format('MM/DD/YYYY'),
            endDate: moment('2014-9-15')
                .endOf('month')
                .format('MM/DD/YYYY'),
            internalStudentId: mongoose.Schema.Types.ObjectId(),
            created: moment()
        });
        async.parallel(_.map(data, function(d) {
            return function(cb) {
                context.rollup.parentalConsent.save(d, cb);
            };
        }), cb);
    },
    rx: function(cb) {
        var data = [];
        data.push({
            districtCode: 'VS1',
            districtName: 'Valley Springs',
            studentId: '000001',
            studentName: 'Sue Smith',
            endDate: moment('2014-9-15')
                .endOf('month')
                .format('MM/DD/YYYY'),
            serviceType: 'Physical Therapy',
            doctorName: 'John Chrisberg',
            created: moment()
                .tz('America/Chicago'),
            internalStudentId: mongoose.Schema.Types.ObjectId()
        });
        data.push({
            districtCode: 'VS1',
            districtName: 'Valley Springs',
            studentId: '000002',
            studentName: 'Pete Jones',
            endDate: moment('2014-9-15')
                .endOf('month')
                .format('MM/DD/YYYY'),
            serviceType: 'Speech Therapy',
            doctorName: 'Alice Wynott',
            created: moment()
                .tz('America/Chicago')
                .format('MM/DD/YYYY'),
            internalStudentId: mongoose.Schema.Types.ObjectId()
        });
        async.parallel(_.map(data, function(d) {
            return function(cb) {
                context.rollup.rx.save(d, cb);
            };
        }), cb);
    },
    servicesBilledPerformance: function(cb) {
        var data = [];
        var aid = mongoose.Schema.Types.ObjectId();
        data.push({
            districtCode: 'VS1',
            districtName: 'Valley Springs',
            therapist: 'Alice Wynott',
            therapistId: aid,
            therapy: 'Physical Therapy',
            monthName: moment('2014-7-1')
                .format('MMM'),
            month: 7,
            year: moment('2014-7-1')
                .format('YYYY'),
            billed: 4,
            unbilled: 22,
            billedYearToDate: 4,
            unbilledYearToDate: 22
        });

        data.push({
            districtCode: 'VS1',
            districtName: 'Valley Springs',
            therapist: 'Alice Wynott',
            therapistId: aid,
            therapy: 'Occupational Therapy',
            monthName: moment('2014-8-1')
                .format('MMM'),
            month: 7,
            year: moment('2014-7-1')
                .format('YYYY'),
            billed: 6,
            unbilled: 12,
            billedYearToDate: 10,
            unbilledYearToDate: 34
        });

        data.push({
            districtCode: 'VS1',
            districtName: 'Valley Springs',
            therapist: 'Alice Wynott',
            therapistId: aid,
            therapy: 'Speech Therapy',
            monthName: moment('2014-7-1')
                .format('MMM'),
            month: 7,
            year: moment('2014-7-1')
                .format('YYYY'),
            billed: 15,
            unbilled: 4,
            billedYearToDate: 25,
            unbilledYearToDate: 38
        });

        data.push({
            districtCode: 'VS1',
            districtName: 'Valley Springs',
            therapist: 'Joe Watkins',
            therapy: 'Physical Therapy',
            therapistId: mongoose.Schema.Types.ObjectId(),
            monthName: moment('2014-7-1').tz('America/Chicago')
                .endOf('month')
                .format('MMM'),
            month: 7,
            year: moment('2014-7-1').tz('America/Chicago')
                .endOf('month')
                .format('YYYY'),
            billed: 34,
            unbilled: 4,
            billedYearToDate: 34,
            unbilledYearToDate: 4
        });
        async.parallel(_.map(data, function(d) {
            return function(cb) {
                context.rollup.servicesBilledPerformance.save(d, cb);
            };
        }), cb);
    },
    therapistLicensing: function(cb) {
        var data = [];
        data.push({
            name: 'Alice Wynott',
            emailAddress: 'awynott@valleysprings.edu',
            hasNpiNumber: true,
            npiNumber: '981525A',
            hasStateLicense: false,
            districtCode: 'VS1',
            districtName: 'Valley Springs',
            created: moment()
                .toDate()
        });
        data.push({
            name: 'Joe Watkins',
            emailAddress: 'jwatkins2@valleysprings.edu',
            hasNpiNumber: false,
            npiNumber: null,
            hasStateLicense: true,
            districtCode: 'VS1',
            districtName: 'Valley Springs',
            created: moment()
                .toDate()
        });
        async.parallel(_.map(data, function(d) {
            return function(cb) {
                context.rollup.therapistLicensing.save(d, cb);
            };
        }), cb);
    },
    summaryClaimingBilledPerformance: function(cb){
        var data = [];
        data.push({
            monthNumber: '01',
            month: 'Jan',
            year: '2014',
            sessionsWithIep: 3,
            sessionsWithIepBilled: 2,
            sessionsMedicaidEligible: 3,
            sessionsMedicaidEligibleBilled: 2
        });
        data.push({
            monthNumber: '02',
            month: 'Feb',
            year: '2014',
            sessionsWithIep: 23,
            sessionsWithIepBilled: 16,
            sessionsMedicaidEligible: 20,
            sessionsMedicaidEligibleBilled: 16
        });
        data.push({
            monthNumber: '03',
            month: 'Mar',
            year: '2014',
            sessionsWithIep: 44,
            sessionsWithIepBilled: 30,
            sessionsMedicaidEligible: 42,
            sessionsMedicaidEligibleBilled: 30
        });
    
        async.parallel(_.map(data, function(d) {
            return function(cb) {
                context.rollup.summaryClaimingBilledPerformance.save(d, cb);
            };
        }), cb);
    }


};

var scrubberTasks = {
    auth: function(cb) {
        context.rollup.auth.remove({}, cb);
    },
    billingDetail: function(cb) {
        context.rollup.billingDetail.remove({}, cb);
    },
    billingSummary: function(cb) {
        context.rollup.billingSummary.remove({}, cb);
    },
    mcoAuth: function(cb) {
        context.rollup.mcoAuth.remove({}, cb);
    },
    gds: function(cb) {
        context.rollup.gds.remove({}, cb);
    },
    parentalConsent: function(cb) {
        context.rollup.parentalConsent.remove({}, cb);
    },
    rx: function(cb) {
        context.rollup.rx.remove({}, cb);
    },
    servicesBilledPerformance: function(cb) {
        context.rollup.servicesBilledPerformance.remove({}, cb);
    },
    therapistLicensing: function(cb) {
        context.rollup.therapistLicensing.remove({}, cb);
    },
    summaryClaimingBilledPerformance: function(cb){
        context.rollup.summaryClaimingBilledPerformance.remove({}, cb);
    }


};

var tasks = {
    scrub: function(cb) {
        logger.debug('scrubbing...');
        async.parallel(scrubberTasks, cb);
    },
    populate: function(cb) {
        logger.debug('populating..');
        async.parallel(dataTasks, cb);
    }
};

async.series(tasks, function(err) {
    mongoose.disconnect();
    logger.silly('done');
});
