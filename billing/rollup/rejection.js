var _ = require('lodash')
async = require('async'),
moment = require('moment'),
context = require('../../models/context');

module.exports = function(flats, cb) {
    var rolluptasks = [];

    var rejects = _.filter(flats, function(flat) {
        return flat.errors.length > 0;
    });

    _.each(rejects, function(flat) {
        rolluptasks.push(function(cb) {
            var rejected = new context.rollup.Rejected();
            rejected.attendee = flat.attendeeId;
            rejected.session = flat.sessionId;
            rejected.therapist = flat.modifiedByUser._id;
            rejected.billingRun = flat.runId;
            if (!!flat.procedureCode) {
                rejected.procedureCode = flat.procedureCode._id;
                rejected.procedureCodeRange = flat.procedureCode._rangeId;
                rejected.procedureCodeCode = flat.procedureCode.procedureCode;
                if (!!flat.procedureCode.procedureCodeValue) {
                    rejected.procedureCodeValue = flat.procedureCode.procedureCodeValue._id;
                    rejected.procedureCodeValueRange = flat.procedureCode.procedureCodeValue._rangeId;
                    rejected.procedureCodeValue = flat.procedureCode.procedureCodeValue.value;
                }
            }

            if (!!flat.diagnosticCode) {
                rejected.diagnosticCode = flat.diagnosticCode._id;
                rejected.diagnosticCodeRange = flat.diagnosticCode._rangeId;
                rejected.diagnosticCodeDescription = flat.diagnosticCode.description;
            }

            if (!!flat.medicaidInfo) {
                if (!!flat.medicaidInfo.mco) {
                    rejected.studentMCO = flat.medicaidInfo.mco._id;
                    rejected.studentMCORange = flat.medicaidInfo.mco._rangeId;
                    rejected.studentMCOName = flat.medicaidInfo.mco.name;
                }
            }

            rejected.school = flat.school._id;
            rejected.schoolRange = flat.school._rangeId;
            rejected.district = flat.district._id;
            rejected.districtRange = flat.district._rangeId;
            rejected.schedule = flat.scheduleId;

            rejected.studentName = flat.student.firstName + ' ' + flat.student.lastName;
            rejected.dateTime = flat.dateTime;
            rejected.therapistName = flat.modifiedByUser.firstName + ' ' + flat.modifiedByUser.lastName;
            rejected.possibleIEPUnits = flat.iepUnits;
            rejected.billedIEPUnits = flat.iepBillableUnits;
            rejected.schoolCode = flat.school.schoolCode;
            rejected.schoolName = flat.school.name;
            rejected.districtCode = flat.school.districtCode;
            rejected.districtName = flat.district.name;
            rejected.studentId = flat.student.studentId;

            rejected.therapy = flat.serviceType;

            //mark what rejection steps the reject failed on
            _.each(flat.errors, function(error) {
                rejected[error.rejectionType] = true;
            });

            rejected.save(cb);
        });
    });

    async.parallel(rolluptasks, function(err) {
        if (err) {
            logger.error(err);
        }
        cb(err, flats);
    });
};
