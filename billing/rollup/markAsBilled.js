var _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    context = require('../../models/context'),
    logger = require('winston');

module.exports = function(flats, cb) {
    var billtasks = [];

    var success = _.filter(flats, function(flat) {
        return flat.errors.length == 0;
    });

    //add marking attendee as billed tasks
    _.each(success, function(flat) {
        billtasks.push(function(cb) {
            context.Attendee.findOne({
                _id: flat.attendeeId
            })
                .exec(function(err, attendee) {
                    attendee.billedDate = moment()
                        .toDate();
                    attendee.iepBilledUnits = flat.iepBilledUnits;
                    attendee.save(cb);
                });
        });
    });

    //add marking session as billed tasks
    //extract session ids
    var sessionIds = _.chain(success)
        .map(function(flat) {
            return flat.sessionId;
        })
        .uniq()
        .value();
    _.each(sessionIds, function(sessionId) {
        billtasks.push(function(cb) {
            context.Session.findOne({
                _id: sessionId
            })
                .exec(function(err, session) {
                    session.billedDate = moment()
                        .toDate();
                    session.save(cb);
                });
        });
    });

    //execute the tasks
    async.parallel(billtasks, function(err) {
        logger.info(success.length + ' attendees in ' + sessionIds.length + ' marked as billed.');
        cb(err, flats);
    });
};
