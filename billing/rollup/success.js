var _ = require('lodash')
async = require('async'),
moment = require('moment'),
logger = require('winston'),
context = require('../../models/context');

module.exports = function(flats, cb) {
    var rolluptasks = [];

    var success = _.filter(flats, function(flat) {
        return flat.errors.length == 0;
    });

    _.each(success, function(flat) {
        rolluptasks.push(function(cb) {
            var billed = new context.rollup.Billed();
            billed.billingRun = flat.runId;
            billed.billingGroup = flat.groupId;
            billed.attendee = flat.attendeeId;
            billed.session = flat.sessionId;
            billed.procedureCode = flat.procedureCode._id;
            billed.procedureCodeRange = flat.procedureCode._rangeId;
            billed.procedureCodeValue = flat.procedureCode.procedureCodeValue._id;
            billed.procedureCodeValueRange = flat.procedureCode.procedureCodeValue._rangeId;
            billed.diagnosticCode = flat.diagnosticCode._id;
            billed.diagnosticCodeRange = flat.diagnosticCode._rangeId;
            billed.school = flat.school._id;
            billed.schoolRange = flat.school._rangeId;
            billed.district = flat.district._id;
            billed.districtRange = flat.district._rangeId;
            billed.schedule = flat.scheduleId;
            billed.studentMCO = flat.medicaidInfo.mco._id;
            billed.studentMCORange = flat.medicaidInfo.mco._rangeId;
            billed.therapist = flat.finalizingUser._id;

            billed.studentName = flat.student.firstName + ' ' + flat.student.lastName;
            billed.studentId = flat.student.studentId;
            billed.dateTime = flat.dateTime;
            billed.therapistName = flat.finalizingUser.firstName + ' ' + flat.finalizingUser.lastName;
            billed.diagnosticCodeDescription = flat.diagnosticCode.description;
            billed.possibleIEPUnits = flat.iepUnits;
            billed.billedIEPUnits = flat.iepBillableUnits;
            billed.procedureCodeCode = flat.procedureCode.procedureCode;
            billed.procedureCodeValueValue = flat.procedureCode.procedureCodeValue.value;
            billed.schoolCode = flat.school.schoolCode;
            billed.schoolName = flat.school.name;
            billed.districtCode = flat.district.districtCode;
            billed.districtName = flat.district.name;
            billed.studentMCOName = flat.medicaidInfo.mco.name;

            billed.therapy = flat.serviceType;

            billed.save(cb);
        });
    });

    async.parallel(rolluptasks, function(err, saved) {
        if (err) {
            logger.error(err);
        }
        cb(err, flats);
    });
};
