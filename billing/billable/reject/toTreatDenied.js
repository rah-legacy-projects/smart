var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');

module.exports = function(flats, cb) {
    logger.debug('reject to treat denied');
    //rejection step: reject students with no to treat date
    var noConsents = _.filter(flats, function(flat) {
        return
            !flat.student ||
                !flat.student.parentalConsent ||
            flat.student.parentalConsent.toTreatDenied;
    });

    //add rejections for those students
    _.each(noConsents, function(noConsent) {
        makeRejection("toTreatDenied", noConsent, "Student is denied treatment.");
    });

    cb(null, flats);
}
