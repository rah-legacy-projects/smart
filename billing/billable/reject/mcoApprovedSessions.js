var _ = require('lodash'),
    logger = require('winston'),
    context = require('../../../models/context'),
    makeRejection = require('../makeRejection'),
    moment = require('moment-timezone'),
    _ = require('lodash'),
    async = require('async'),

    populate = require('../manipulate/populate'),
    flatten = require('../manipulate/flatten');


module.exports = function(flats, cb) {
    logger.debug('rejecting students beyond MCO approval');
    if (flats.length == 0) {
        //special case: if flats are empty, skip reject step
        cb(null, flats);
        return;
    }
    var retval = _.filter(flats, function(flat){
        return !flat.student || flat.errors.length > 0;
    });

    var flatsWithStudents = _.filter(flats, function(flat){
        return !!flat.student && flat.errors.length == 0;
    });

    if(flatsWithStudents.length ==0){
        cb(null, retval);
        return;
    }
    //group by internal student ID
    _.forIn(_.groupBy(flatsWithStudents, function(flat) {
        return flat.student.internalStudendId;
    }), function(unbilled, key) {
        var tasks = {
            billedFlats: function(cb) {
                async.waterfall([

                    function(cb) {
                        context.Service.find({
                            student: key
                        })
                            .exec(function(err, services) {
                                cb(err, _.pluck(services, '_id'));
                            });
                    },
                    function(serviceIds, cb) {
                        //get the billed sessions with billed attendees for the grouped-by service ID
                        context.Session
                            .find()
                            .where('billedDate')
                            .ne(null)
                            .elemMatch('attendees', {
                                service: {
                                    $in: serviceIds
                                },
                                billedDate: {
                                    $ne: null
                                }
                            })
                            .exec(function(err, sessions) {
                                cb(err, _.pluck(sessions, '_id'));
                            });
                    },
                    function(sessionIds, cb) {
                        //get the schedules for those sessions with populated sessions
                        context.Schedule.find()
                            .where('session')
                            .in(sessionIds)
                            .populate('session')
                            .exec(function(err, schedules) {
                                cb(err, schedules);
                            });
                    },
                    function(schedules, cb) {
                        //borrow billing population and flattening
                        populate(schedules, [], cb);
                    },
                    flatten,
                    function(billedFlats, billedRejects, cb) {
                        //filter to the student
                        cb(null, _.filter(billedFlats, function(flat) {
                            return flat.student.internalStudentId == key;
                        }), billedRejects);
                    }
                ], function(err, billedFlats, billedRejects) {
                    cb(err, billedFlats);
                });
            },

        }
        async.parallel(tasks, function(err, r) {
            //hack: add no mco infos to retval
            _.chain(unbilled)
                .filter(function(ub) {
                    return !ub.student || !ub.student.mcoInformation;
                })
                .each(function(ub) {
                    retval.push(ub);
                });
            //group unbilled by mco info
            _.forIn(_.chain(unbilled)
                .filter(function(ub) {
                    return !!ub.student && !!ub.student.mcoInformation;
                })
                .groupBy(function(ub) {
                    return ub.student.mcoInformation._id;
                })
                .value(), function(unbilled, key) {
                    //unbilled by mco info (which is also by specialty)
                    //get billed by mco info
                    var billed = _.filter(r.billedFlats, function(bf) {
                        return bf.student.mcoInformation._id == key;
                    });

                    //sort unbilled
                    unbilled = _.sortBy(unbilled, function(ub) {
                        return moment(ub.dateTime)
                            .format("YYYYMMDDHHmm");
                    });

                    //if the mco information for these flats has # of sessions specified
                    if (!!unbilled[0].student.mcoInformation.numberOfSessions) {
                        //if billed + unbilled > mco info approved sessions,
                        //reverse the unbilled
                        unbilled = unbilled.reverse();

                        //figure out how many to reject
                        var reject = (billed.length + unbilled.length) - unbilled[0].student.mcoInformation.numberOfSessions;
                        if (reject < 0) {
                            //if there are more sessions allowed than the billed + unbilled, reset rejections to zero
                            reject = 0;
                        }

                        _.each(unbilled, function(ub, i) {
                            if (reject > 0) {
                                reject--;
                                makeRejection('mcoApprovedSessions', ub, 'Service exceeded number of MCO approved sessions');
                            }
                            retval.push(ub);
                        });
                    } else {
                        logger.info('student has blanket approval from mco, adding all billable');
                        retval = retval.concat(unbilled);
                    }
                });

            //call back

            cb(null, retval);
        });
    });
};
