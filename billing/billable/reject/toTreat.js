var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    logger.debug('reject to treat missing');
    //rejection step: reject students with no to treat date
    var noConsents = _.filter(flats, function(flat) {
        return !flat.student ||
            !flat.student.parentalConsent ||
            !flat.student.parentalConsent.toTreat;
    });

    //add rejections for those students
    _.each(noConsents, function(noConsent) {
        makeRejection("toTreat", noConsent, "Student did not have a parental consent to treat.");
    });

    cb(null, flats);
}
