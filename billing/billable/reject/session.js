var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');

module.exports = function(schedules, includeReasons, cb) {
    logger.debug('reject schedules with no sessions');
    //rejection step: filter out flats with no session
    var rejected = _.filter(schedules, function(schedule) {
        return !schedule.session;
    });

    _.each(rejected, function(x) {
        //makeRejection('session', x, "flat did not have a session.");
        logger.info('schedule ' + x._id + ' did not have session.  Skipping.');
    });

    cb(null, _.filter(schedules, function(schedule) {
        return !!schedule.session;
    }), includeReasons);
};
