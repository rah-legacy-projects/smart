var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    logger.debug('rejecting absent students');
    //rejection step: reject students that are absent
    _.chain(flats)
        .filter(function(flat) {
            return flat.absent;
        })
        .each(function(flat) {
            makeRejection('absent', flat, "Attendee was absent.");
        });

    cb(null, flats);

};
