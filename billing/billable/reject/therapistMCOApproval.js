var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    logger.debug('reject therapist not approved');
    _.chain(flats)
        .filter(function(flat) {
            return !flat.finalizingUser || !_.any(flat.finalizingUser.mcoQualifications, function(qual) {
                return !flat.medicaidInfo || !flat.medicaidInfo.mco || qual.mco.toString() == flat.medicaidInfo.mco._id;
            });
        })
        .each(function(flat) {
            makeRejection('therapistMCOApproval', flat, 'Therapist does not have approval from the MCO.');
        });

    cb(null, flats);
}
