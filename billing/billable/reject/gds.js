var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    logger.debug('rejecting no gds info');
    _.chain(flats)
        .filter(function(flat) {
            return !flat.medicaidInfo;
        })
        .each(function(flat) {
            makeRejection('GDS', flat, 'No data found for GDS student.');
        });

    cb(null, flats);
};
