var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    //rejection step: reject students that are absent
    logger.debug('rejecting students without diagnostic codes');
    _.chain(flats)
        .filter(function(flat) {
            return !flat.diagnosticCode;
        })
        .each(function(flat) {
            makeRejection('diagnosticCode', flat, "Diagnostic Code Missing.");
        });

        cb(null,flats);
};
