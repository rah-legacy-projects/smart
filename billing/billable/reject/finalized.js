var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    logger.debug('rejecting finalized');
    //rejection step: filter sessions that are not finalized
    //fail unfinalized
    _.chain(flats)
        .filter(function(flat) {
            return !flat.finalizingUser;
        })
        .each(function(flat) {
            makeRejection('finalized', flat, "Session was not finalized");
        });

    cb(null, flats);
};
