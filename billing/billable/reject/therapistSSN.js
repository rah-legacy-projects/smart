var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    logger.debug('reject therapist with no SSN');
    _.chain(flats)
        .filter(function(flat) {
            return !flat.finalizingUser || !flat.finalizingUser.ssn ||
                /^\s*$/.test(flat.finalizingUser.ssn);
        })
        .each(function(flat) {
            makeRejection('therapistSSN', flat, 'Therapist does not have an social security number.');
        });

    cb(null, flats);
}
