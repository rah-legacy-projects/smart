var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    logger.debug('rejecting expired students');
    _.chain(flats)
        .filter(function(flat) {
            return !flat.student;
        })
        .each(function(flat) {
            makeRejection('studentExpired', flat, "Student expired.");
        });

    cb(null, flats);

};
