var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    logger.debug('reject to bill denied');
    //rejection step: reject students with to bill denied
    var noConsents = _.filter(flats, function(flat) {
        return 
        !flat.student ||
            !flat.student.parentalConsent ||
        flat.student.parentalConsent.toBillMedicaidDenied;
    });

    //add rejections for those students
    _.each(noConsents, function(noConsent) {
        makeRejection("toBillDenied", noConsent, "Student is denied to bill Medicaid.");
    });

    cb(null, flats);
}
