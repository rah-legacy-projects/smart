var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    logger.debug('reject not mco approved');
    _.chain(flats)
        .filter(function(flat) {
            return 
                !flat.medicaidInfo ||
                !flat.medicaidInfo.mco ||
                !flat.medicaidInfo.mco.address1 ||
                !flat.medicaidInfo.mco.city ||
                !flat.medicaidInfo.mco.state ||
                !flat.medicaidInfo.mco.zip ||
                /^\s*$/.test(flat.medicaidInfo.mco.address1) ||
                /^\s*$/.test(flat.medicaidInfo.mco.city) ||
                /^\s*$/.test(flat.medicaidInfo.mco.state) ||
                /^\s*$/.test(flat.medicaidInfo.mco.zip);

        })
        .each(function(flat) {
            makeRejection('studentMCOAddress', flat, 'Student MCO missing address data');
        });

    cb(null, flats);
};
