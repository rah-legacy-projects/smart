var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    logger.debug('rejecting billed');
    //rejection step: reject attendees with billed date
    var billedAttendees = _.filter(flats, function(flat) {
        return !!flat.billedDate;
    });
    _.each(billedAttendees, function(billed) {
        makeRejection("billed", billed, "Attendee was already billed.");
    });
    cb(null, flats);
}
