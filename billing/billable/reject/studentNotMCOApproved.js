var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    logger.debug('reject not mco approved');
    _.chain(flats)
        .filter(function(flat) {
            return 
                !flat.student ||
                !flat.student.mcoInformation ||
                !flat.student.mcoInformation.approvalToTreat;
        })
        .each(function(flat) {
            makeRejection('studentNotMCOApproved', flat, 'Student has a disapproval to treat.');
        });

    cb(null, flats);
};
