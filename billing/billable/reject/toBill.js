var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    logger.debug('reject no to bill');
    //rejection step: reject students with no to bill date
    var noConsents = _.filter(flats, function(flat) {
        return 
        !flat.student ||
            !flat.student.parentalConsent || 
        !flat.student.parentalConsent.toBillMedicaid;
    });

    //add rejections for those students
    _.each(noConsents, function(noConsent) {
        makeRejection("toBill", noConsent, "Student did not have a parental consent to bill medicaid.");
    });

    cb(null, flats);
}
