var _ = require('lodash'),
    logger = require('winston'),
    context = require('../../../models/context'),
    dataUtil = require('../../../models/dataUtil'),
    moment = require('moment-timezone'),
    _ = require('lodash'),
    async = require('async'),
    makeRejection = require('../makeRejection');

module.exports = function(flats, cb) {
    logger.debug('rejecting sessions beyond IEP');
    if (flats.length == 0) {
        //special case: if flats are empty, skip reject step
        cb(null, flats);
        return;
    }
    var retval = _.filter(flats, function(flat) {
        return flat.errors.length > 0;
    });

    var flatsWithNoErrors = _.filter(flats, function(flat) {
        return flat.errors.length == 0;
    });

    if (flatsWithNoErrors.length == 0) {
        logger.debug('no flats without error.  Skipping sessions beyond IEP.');
        cb(null, retval);
        return;
    }


    var taskbuilder = [];

    _.forIn(_.groupBy(flatsWithNoErrors, function(flat) {
        return flat.serviceId;
    }), function(unbilled, key) {
        taskbuilder.push(function(cb) {
            var tasks = {
                billedSchedules: function(cb) {
                    async.waterfall([

                        function(cb) {
                            //get the billed sessions with billed attendees for the grouped-by service ID
                            context.Session
                                .find()
                                .where('billedDate')
                                .ne(null)
                                .elemMatch('attendees', {
                                    service: key,
                                    billedDate: {
                                        $ne: null
                                    }
                                })
                                .exec(function(err, sessions) {
                                    cb(err, _.pluck(sessions, '_id'));
                                });
                        },
                        function(sessionIds, cb) {
                            //get the schedules for those sessions with populated sessions
                            context.Schedule.find()
                                .where('session')
                                .in(sessionIds)
                                .populate('session')
                                .exec(function(err, schedules) {
                                    cb(err, schedules);
                                });
                        }
                    ], function(err, schedules) {
                        cb(err, schedules);
                    });
                },
                service: function(cb) {
                    //get the service with ranged data for the grouped-by service ID
                    context.Service.findOne({
                        _id: key
                    })
                        .populate('rangedData')
                        .lean()
                        .exec(function(err, service) {
                            cb(err, service);
                        });
                }
            }

            async.parallel(tasks, function(err, r) {
                if (!!err) {
                    logger.error(err);
                }
                //group the like-services by range
                var groupedByServiceRange = _.groupBy(unbilled, function(v) {
                    var flatService = dataUtil.getByDateRangeSync(r.service, 'rangedData', v.dateTime, v.dateTime);
                    return flatService._rangeId;
                })

                _.forIn(groupedByServiceRange, function(value, key) {
                    //organize unbilled by date
                    value = _.sortBy(value, function(v) {
                        return moment(v.dateTime)
                            .format("YYYYMMDDHHmm");
                    });

                    //get first and last unbilled session
                    var first = _.first(value);
                    var last = _.last(value);

                    //flatten the service by the first's date (they should all be the same)
                    var flatService = dataUtil.getByDateRangeSync(r.service, 'rangedData', first.dateTime, first.dateTime);

                    //get the time to iterate by
                    var iterateUnit = null;
                    if (/mon/i.test(flatService.frequency)) {
                        iterateUnit = 'month';
                    } else if (/w/i.test(flatService.frequency)) {
                        iterateUnit = 'week';
                    } else if (/a|y/i.test(flatService.frequency)) {
                        iterateUnit = 'year'
                    }

                    //prepare a date iterator
                    var iterator = moment(first.dateTime)
                        .startOf(iterateUnit)
                        .twix(moment(last.dateTime)
                            .endOf(iterateUnit))
                        .iterate(iterateUnit + 's');

                    var init = true;

                    //while times through <1 or the iterator can continue,
                    while (init || iterator.hasNext()) {
                        //iterate
                        var startOfIteration = iterator.next();

                        init = false;

                        //determine the end of the iteration
                        var endOfIteration = moment(startOfIteration)
                            .add(1, iterateUnit)
                            .subtract(1, 'second');


                        //get billed sessions after service start and after iterator and before the end of the iteration and before the service end
                        var iterationBilled = _.filter(r.billedSchedules, function(schedule) {
                            return (moment(schedule.dateTime)
                                    .isAfter(moment(startOfIteration)) || moment(schedule.dateTime)
                                    .isSame(moment(startOfIteration))) &&
                                (moment(schedule.dateTime)
                                    .isAfter(moment(flatService.startDate)) || moment(schedule.dateTime)
                                    .isSame(moment(flatService.startDate))) &&
                                (moment(schedule.dateTime)
                                    .isBefore(moment(endOfIteration)) || moment(schedule.dateTime)
                                    .isSame(moment(endOfIteration))) &&
                                (moment(schedule.dateTime)
                                    .isBefore(moment(flatService.endDate)) || moment(schedule.dateTime)
                                    .isSame(moment(flatService.endDate)));
                        });

                        //get unbilled flats after service start, after iterator start, before iteration end, before service end
                        var iterationUnbilled = _.filter(value, function(flat) {
                            return (moment(flat.dateTime)
                                    .isAfter(moment(startOfIteration)) || moment(flat.dateTime)
                                    .isSame(moment(startOfIteration))) &&
                                (moment(flat.dateTime)
                                    .isAfter(moment(flatService.startDate)) || moment(flat.dateTime)
                                    .isSame(moment(flatService.startDate))) &&
                                (moment(flat.dateTime)
                                    .isBefore(moment(endOfIteration)) || moment(flat.dateTime)
                                    .isSame(moment(endOfIteration))) &&
                                (moment(flat.dateTime)
                                    .isBefore(moment(flatService.endDate)) || moment(flat.dateTime)
                                    .isBefore(moment(flatService.endDate)));

                        });

                        //get the iteration billed units

                        var iterationBilledUnits = _.reduce(iterationBilled, function(collection, ib) {

                            var attendee = _.find(ib.session.attendees, function(a) {
                                return a.serviceId.toString() == r.service._id.toString();
                            });

                            return collection + attendee.iepBilledUnits;
                        }, 0);

                        //get the iteration unbilled units
                        var iterationUnbilledUnits = _.reduce(iterationUnbilled, function(collection, iu) {
                            return collection + iu.iepUnits;
                        }, 0);


                        //figure out how many units to reject
                        var reject = (iterationBilledUnits + iterationUnbilledUnits) - flatService.numberOfSessions;
                        //number of sessions can be greater than the billed + unbilled
                        //if reject < 0, set to 0
                        if(reject < 0){
                            reject = 0;
                        }
                        //starting at the late end,
                        _.chain(iterationUnbilled)
                            .reverse()
                            .each(function(unbilled) {
                                if (unbilled.iepUnits - reject <= 0) {
                                    //if there are more rejectable units than available,
                                    //deduct the units from rejectable
                                    reject = reject - unbilled.iepUnits;
                                    //set that flat's billable units to zero
                                    unbilled.iepBillableUnits = 0;
                                    //mark as rejected
                                    makeRejection('iepApprovedSessions', unbilled, 'Service exceeded number of IEP approved units');
                                    retval.push(unbilled);
                                } else {
                                    //if the billable units are bigger than the rejectable units,
                                    //deduct the rejectable units from the billable units
                                    //do NOT reject this record!
                                    unbilled.iepBillableUnits = unbilled.iepUnits - reject;
                                    //set rejectable units to zero
                                    reject = 0;
                                    retval.push(unbilled);
                                }
                            });
                        if (reject > 0) {
                            logger.warn("incongruity detected: more rejections than rejectables found");
                        }
                    }
                });

                //call back
                cb(null, retval);
            });
        });
    });

    async.series(taskbuilder, function(err) {
        cb(err, retval);
    });

};
