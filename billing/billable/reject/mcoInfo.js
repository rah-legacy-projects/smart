var _ = require('lodash'),
    makeRejection = require('../makeRejection'),
    logger = require('winston');
module.exports = function(flats, cb) {
    logger.debug('reject students with no valid MCO info');
    //rejection step: reject students with no valid mco infos
    //filter students with no mcos
    var noMCOs = _.filter(flats, function(flat) {
        return !flat.student || 
        !flat.student.mcoInformation;
    });

    //add rejections for those students
    _.each(noMCOs, function(noMCO) {
        makeRejection("mcoInfo", noMCO, "Student did not have any current MCO approvals.");
    });

    cb(null, flats);
}
