var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    logger.debug('reject therapist with no NPI');
    _.chain(flats)
        .filter(function(flat) {
            return !flat.finalizingUser ||
                !flat.finalizingUser.therapistQualification ||
                !flat.finalizingUser.therapistQualification.npiNumber ||
                /^\s*$/.test(flat.finalizingUser.therapistQualification.npiNumber);
        })
        .each(function(flat) {
            makeRejection('therapistNPI', flat, 'Therapist does not have an NPI number.');
        });

    cb(null, flats);
}
