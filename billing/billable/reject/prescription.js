var _ = require('lodash'),
    makeRejection = require('../makeRejection'),
    logger = require('winston');
module.exports = function(flats, cb) {
    //rejection step: reject students with no valid prescription
    //filter students with no prescription
    logger.debug('reject no current rx');
    var noRx = _.filter(flats, function(flat) {
        return !flat.student || !flat.student.prescriptions || flat.student.prescriptions.length == 0;
    });

    //add rejections for those students
    _.each(noRx, function(noRx) {
        makeRejection("prescription", noRx, "Student did not have any current prescriptions.");
    });

    cb(null, flats);
}
