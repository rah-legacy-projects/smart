var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    logger.debug('rejecting expired schools');
    _.chain(flats)
        .filter(function(flat) {
            return !flat.school;
        })
        .each(function(flat) {
            makeRejection('schoolExpired', flat, "School expired.");
        });

    cb(null, flats);

};
