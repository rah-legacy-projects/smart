var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    _.chain(flats)
        .filter(function(flat) {
            return
                !flat.medicaidInfo ||
                !flat.medicaidInfo.mco ||
                !flat.medicaidInfo.mco.healthPlanId ||
                /^\s*$/.test(flat.medicaidInfo.mco.healthPlanId)
        })
        .each(function(flat) {
            makeRejection('studentMCOHealthPlanId', flat, 'Student MCO missing health plan ID');
        });

    cb(null, flats);
};
