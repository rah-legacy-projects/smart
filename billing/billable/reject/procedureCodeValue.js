var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    logger.debug('reject procedure codes with no value');
    //rejection step: filter sessions without a selected procedure code
    _.chain(flats)
        .filter(function(flat) {
            return !flat.procedureCode || !flat.procedureCode.procedureCodeValue;
        })
        .each(function(flat) {
            makeRejection('procedureCodeValue', flat, "Session did not have a procedure code with a valid procedure code value.");
        });

    cb(null,flats);
};
