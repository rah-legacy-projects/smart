var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    logger.debug('reject no proecedure code');
    //rejection step: filter sessions without a selected procedure code
    _.chain(flats)
        .filter(function(flat) {
            return !flat.procedureCode;
        })
        .each(function(flat) {
            makeRejection('procedureCode', flat, "Session did not have a procedure code selected.");
        });

    cb(null, flats);
};
