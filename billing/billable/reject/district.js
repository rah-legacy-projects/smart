var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    logger.debug('rejecting expired schools');
    _.chain(flats)
        .filter(function(flat) {
            return !flat.district;
        })
        .each(function(flat) {
            makeRejection('districtExpired', flat, "District expired.");
        });

    cb(null, flats);

};
