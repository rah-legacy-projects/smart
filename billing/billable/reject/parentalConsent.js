var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    logger.debug('reject no parental consent');
    //rejection step: reject students with no parental consent
    var noConsents = _.filter(flats, function(flat) { 
        return !flat.student || !flat.student.parentalConsent;
    });

    //add rejections for those students
    _.each(noConsents, function(noConsent) {
        makeRejection("parentalConsent", noConsent, "Student did not have a parental consent.");
    });

    cb(null, flats);
}
