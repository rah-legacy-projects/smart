var _ = require('lodash'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    logger.debug('rejecting no gds info');
    _.chain(flats)
        .filter(function(flat) {
            return !flat.medicaidInfo || !flat.medicaidInfo.sSN || /^\s*$/.test(flat.medicaidInfo.sSN);
        })
        .each(function(flat) {
            makeRejection('gdsSSN', flat, 'Student SSN missing on GDS data.');
        });

    cb(null, flats);
};
