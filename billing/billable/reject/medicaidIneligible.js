var _ = require('lodash'),
    makeRejection = require('../makeRejection'),
    logger = require('winston');
module.exports = function(flats, cb) {
    logger.debug('reject students not medicaid eligible');
    //rejection step: reject students who are not eligible for medicaid
    _.chain(flats)
        .filter(function(flat) {
            return !flat.medicaidInfo || !flat.medicaidInfo.mcaidFlag || /^[^y]$/i.test(flat.medicaidInfo.mcaidFlag);
        })
        .each(function(flat) {
            makeRejection('medicaidIneligible', flat, "Student was not eligible for medicaid per GDS.");
        });

    cb(null, flats);
}
