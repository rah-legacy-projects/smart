var _ = require('lodash'),
    makeRejection = require('../makeRejection'),
    logger = require('winston');
module.exports = function(flats, cb) {
    //rejection step: reject students with no valid prescription for the service in the flat
    logger.debug('reject no rx for service');
    var kept = [];
    _.each(flats, function(flat) {
        if (!!flat.student) {
            var rxForService = _.find(flat.student.prescriptions, function(rx) {
                return rx.serviceType == flat.iepService;
            });
            if (!!rxForService) {
                kept.push(flat);
            } else {
                makeRejection('prescriptionMatch', flat, 'No prescriptions found for prescribed service.');
                kept.push(flat);
            }
        } else {
            makeRejection('prescriptionMatch', flat, 'No prescriptions found for prescribed service.');
            kept.push(flat);
        }
    });

    cb(null, kept);
}
