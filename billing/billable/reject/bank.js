var _ = require('lodash'),
    makeRejection = require('../makeRejection'),
    logger = require('winston'),
    context = require('../../../models/context');
module.exports = function(flats, rejections, cb) {
    //group the remaining flats by service id
    var t = [];
    if (flats.length == 0) {
        //edge case: everything prior to now has been rejected
        cb(null, flats, rejections);
    } else {
        _.chain(flats)
            .groupBy('serviceId')
            .forIn(function(value, serviceId) {
                t.push(function(cb) {
                    //nb: services should all have the same start/end
                    var start = moment(value[0].serviceStartDate),
                        end = moment(value[0].serviceEndDate);

                    //figure out time based on service duration, number of sessions, frequency, and start/end date
                    var multiplier = 0;
                    var timespan = start.twix(end, true);
                    if (/mon/i.test(value[0].serviceFrequency)) {
                        //monthly figure out the difference in months
                        multiplier = timespan.countInner('months');
                    } else if (/w/i.test(value[0].serviceFrequency)) {
                        //weekly, figure out difference in weeks
                        multiplier = timespan.countInner('weeks');
                    } else if (/a|y/i.test(value[0].serviceFrequency)) {
                        //annually, figure out difference in years
                        multiplier = timespan.countInner('years');
                    }
                    var timeBank = value[0].serviceDuration * value[0].serviceNumberOfSessions * multiplier;

                    //find billed sessions for the service
                    var billedSchedulesFromServices = [

                        function(cb) {
                            context.Session.find({
                                isBilled: true
                            })
                                .where('attendees')
                                .elemMatch({
                                    serviceId: serviceId
                                })
                                .exec(function(err, sessions) {
                                    cb(err, sessions);
                                });
                        },
                        function(sessions, cb) {
                            var sessionIds = _.pluck(sessions, '_id');
                            context.Schedules.find()
                                .where('session')
                                .in(sessionIds)
                                .populate('session')
                                .exec(function(err, schedules) {
                                    cb(err, schedules);
                                });
                        }
                    ];

                    async.waterfall(billedSchedulesFromServices, function(err, bsfs) {
                        //determine used time
                        var usedTime = _.reduce(bsfs, function(r, b) {
                            return r + b.duration;
                        });
                        //deduct the used time from the time bank
                        timeBank -= usedTime;

                        //figure out what sessions get included based on duration
                        var accepted = [];
                        _.each(value, function(s) {
                            if (timeBank - s.duration < 0) {
                                rejections.push(makeRejection("time bank", flat, "Session was outside of the time allotted"));
                            } else {
                                accepted.push(s);
                                timeBank -= s.duration;
                            }
                        });
                        cb(null, accepted);
                    });
                });


                async.parallel(t, function(err, acceptedArrays) {
                    //collapse the accepted values into a single array
                    var accepted = _.reduceRight(acceptedArrays, function(a, b) {
                        return a.concat(b);
                    }, []);
                    cb(null, accepted, rejections);
                });
            });
    }
}
