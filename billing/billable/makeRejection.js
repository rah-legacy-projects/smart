var logger = require('winston');
module.exports = function(rejectionType, flat, message) {
    logger.debug('[REJECTED] %s: %s', rejectionType, message);
    flat.errors.push({
        rejectionType: rejectionType,
        message: message
    });
}
