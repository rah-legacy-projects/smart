var _ = require('lodash'),
    context = require('../../../models/context'),
    async = require('async'),
    dataUtil = require('../../../models/dataUtil'),
    moment = require('moment-timezone'),
    logger = require('winston');
module.exports = function(schedules, includeReasons, cb) {
    //flatten records down to single line record
    var flats = [];
    _.each(schedules, function(schedule) {
        _.each(schedule.session.attendees, function(attendee) {
            var runId = context.ObjectId();
            var r = {
                //schedule info
                scheduleId: schedule._id,
                dateTime: schedule.dateTime,
                location: schedule.location,
                duration: schedule.duration,
                organizer: schedule.organizer,

                //session info
                sessionId: schedule.session._id,
                sessionNotes: schedule.session.sessionNotes,
                serviceType: schedule.session.serviceType,
                finalizingUser: schedule.session.finalizingUser,
                sessionProvider: schedule.session.provider,

                //attendee info
                billedDate: attendee.billedDate,
                provider: attendee.provider,
                absent: attendee.absent,
                absenceReason: attendee.absenceReason,
                individualNote: attendee.individualNote,
                iepBilledUnits: attendee.iepBilledUnits,
                attendeeId: attendee._id,
                runId: runId,
                modifiedByEmail: schedule.session.modifiedBy
            }

            r.includeReasons = _.map(includeReasons[schedule._id], function(includeReason){
                return includeReason.includeReason.reason;
            });

            if (!!schedule.session && !!schedule.session.procedureCode) {
                r.procedureCode = dataUtil.getByDateRangeSync(schedule.session.procedureCode, 'rangedData', schedule.dateTime, schedule.dateTime)
            } else {
                r.procedureCode = null;
            }

            //set up session provider's theraquals
            if (!!r.finalizingUser && !!r.finalizingUser.therapistQualifications && r.finalizingUser.therapistQualifications.length > 0) {
                var temp = _.chain(r.finalizingUser.therapistQualifications)
                    .map(function(theraqual) {
                        //get mco infos that are valid for the schedule
                        var s = moment(schedule.dateTime)
                            .twix(moment(schedule.dateTime)
                                .add(schedule.duration, 'minutes'));
                        var m = moment(theraqual.startDate)
                            .twix(moment(theraqual.endDate));
                        if (s.overlaps(m)) {
                            return theraqual;
                        }
                        return null;
                    })
                    .filter(function(theraqual) {
                        return theraqual != null;
                    })
                    .sortBy(function(theraqual) {
                        return moment(theraqual.startDate);
                    })
                    .reverse()
                    .value();

                if (temp.length > 0) {
                    r.finalizingUser.therapistQualification = temp[0];
                } else {
                    r.finalizingUser.therapistQualification = null;
                }
            }

            if (!!attendee.diagnosticCode) {
                r.diagnosticCode = dataUtil.getByDateRangeSync(attendee.diagnosticCode, 'rangedData', schedule.dateTime, schedule.dateTime);
            }
            //attendee service info
            var attendeeService = dataUtil.getByDateRangeSync(attendee.service, 'rangedData', schedule.dateTime, schedule.dateTime);
            r.caseManager = attendeeService.caseManager;
            r.iepService = attendeeService.service;
            r.therapist = attendeeService.therapist;
            r.serviceFrequency = attendeeService.frequency;
            r.serviceDuration = attendeeService.duration;
            r.serviceNumberOfSessions = attendeeService.numberOfSessions;
            r.serviceId = attendeeService._id;
            r.serviceStartDate = attendeeService.startDate;
            r.serviceEndDate = attendeeService.endDate;
            r.serviceDiagnosticCode = null;
            if (!!attendeeService.diagnosticCode) {
                r.serviceDiagnosticCode = dataUtil.getByDateRangeSync(attendeeService.diagnosticCode, 'rangedData', schedule.dateTime, schedule.dateTime);
            }

            //hack: include unflat student name and id on the flat
            //hack: abuse that no students can cross districts
            r.unflat = {
                studentName: attendee.service.student.firstName + ' ' + attendee.service.student.lastName,
                studentId: attendee.service.student.studentId,
                districtCode: attendee.service.student.rangedData[0].school.district.districtCode,
                districtName: attendee.service.student.rangedData[0].school.district.rangedData[0].name
            };

            //attendee service student info
            var student = dataUtil.getByDateRangeSync(attendee.service.student, 'rangedData', schedule.dateTime, schedule.dateTime);
            if (!!student) {
                var school = dataUtil.getByDateRangeSync(student.school, 'rangedData', schedule.dateTime, schedule.dateTime);
                var district = null;
                if (!!school) {
                    district = dataUtil.getByDateRangeSync(school.district, 'rangedData', schedule.dateTime, schedule.dateTime);
                }

                if (!!student) {
                    r.student = {
                        internalStudentId: student._id,
                        firstName: student.firstName,
                        lastName: student.lastName,
                        ssn: student.ssn,
                        stateId: student.stateId,
                        studentId: student.studentId,
                        medicaidId: student.medicaidId,
                        status: student.status
                    };
                    r.unflat.studentName = r.student.firstName + ' ' + r.student.lastName;

                    r.student.prescriptions = null;
                    if (!!student.prescriptions) {
                        r.student.prescriptions = _.chain(student.prescriptions)
                            .map(function(prescription) {
                                //get prescriptions that are valid for the schedule
                                var s = moment(schedule.dateTime)
                                    .twix(moment(schedule.dateTime)
                                        .add(schedule.duration, 'minutes'));
                                var rx = moment(prescription.startDate)
                                    .twix(moment(prescription.endDate));
                                if (s.overlaps(rx)) {
                                    return prescription;
                                }
                                return null;
                            })
                            .filter(function(prescription) {
                                return prescription != null;
                            })
                            .value();
                    } else {}

                    r.student.mcoInformation = null;
                    if (!!student.mcoInformation) {

                        var temp = _.chain(student.mcoInformation)
                            .map(function(mcoInfo) {
                                //get mco infos that are valid for the schedule
                                var s = moment(schedule.dateTime)
                                    .twix(moment(schedule.dateTime)
                                        .add(schedule.duration, 'minutes'));
                                var m = moment(mcoInfo.startDate)
                                    .twix(moment(mcoInfo.endDate));
                                if (s.overlaps(m)) {
                                    return mcoInfo;
                                }
                                return null;
                            })
                            .filter(function(mcoInfo) {
                                return mcoInfo != null;
                            })
                            .sortBy(function(mcoInfo) {
                                return moment(mcoInfo.startDate);
                            })
                            .filter(function(mcoInfo) {
                                return mcoInfo.specialty == r.serviceType;
                            })
                            .reverse()
                            .value();

                        if (temp.length > 0) {
                            r.student.mcoInformation = temp[0];
                        } else {
                            r.student.mcoInformation = null;
                        }
                    }
                    r.student.parentalConsent = null;
                    if (!!student.parentalConsents && student.parentalConsents.length > 0) {
                        r.student.parentalConsent = _.chain(student.parentalConsents)
                            .filter(function(parentalConsent) {
                                //get the closest parental consent to the schedule that is before the schedule
                                var scheduleMoment = moment(schedule.dateTime),
                                    toTreatMoment = moment(parentalConsent.toTreat);
                                toBillMoment = moment(parentalConsent.toBill);

                                return scheduleMoment.isAfter(toTreatMoment) || scheduleMoment.isAfter(toBillMoment);
                            })
                            .sortBy(function(parentalConsent) {
                                return moment(parentalConsent.toTreat)
                                    .format('YYYY-MM-DD');
                            })
                            .reverse()
                            .value()[0]
                    }
                }

                if (!!school) {
                    r.school = {
                        name: school.name,
                        number: school.schoolNumber
                    };
                }

                if (!!district) {
                    r.district = district;
                    r.unflat.districtCode = district.districtCode;
                    r.unflat.districtName = district.name;
                }
            }

            r.errors = [];

            flats.push(r);
        });
    });
    cb(null, flats);
};
