var _ = require('lodash'),
    logger = require('winston'),
    context = require('../../../models/context'),
    dataUtil = require('../../../models/dataUtil'),
    async = require('async'),
    _ = require('lodash'),
    moment = require('moment-timezone');

require('twix');
module.exports = function(flats, cb) {
    //tack in last modified information into the flats
    var modifiedByEmails = _.map(flats, function(flat) {
        return flat.modifiedByEmail;
    });

    modifiedByEmails = _.compact(modifiedByEmails);
    context.User.find()
        .where('emailAddress')
        .in(modifiedByEmails)
        .populate('therapistQualifications')
        .lean()
        .exec(function(err, users) {
            _.each(flats, function(flat) {
                flat.modifiedByUser = _.find(users, function(user) {
                    return flat.modifiedByEmail == user.emailAddress;
                });

                if (!!flat.modifiedByUser) {
                    var temp = _.chain(flat.modifiedByUser.therapistQualifications)
                        .map(function(theraqual) {
                            //get mco infos that are valid for the schedule
                            var s = moment(flat.dateTime)
                                .twix(moment(flat.dateTime)
                                    .add(flat.duration, 'minutes'));
                            var m = moment(theraqual.startDate)
                                .twix(moment(theraqual.endDate));
                            if (s.overlaps(m)) {
                                return theraqual;
                            }
                            return null;
                        })
                        .filter(function(theraqual) {
                            return theraqual != null;
                        })
                        .sortBy(function(theraqual) {
                            return moment(theraqual.startDate);
                        })
                        .reverse()
                        .value();

                    if (temp.length > 0) {
                        flat.modifiedByUser.therapistQualification = temp[0];
                    } else {
                        flat.modifiedByUser.therapistQualification = null;
                    }
                }else{
                    flat.modifiedByUser = {};
                    flat.modifiedByUser.firstName = 'Unknown';
                    flat.modifiedByUser.lastName = 'Unknown';
                }
            });
            cb(err, flats);
        });
};
