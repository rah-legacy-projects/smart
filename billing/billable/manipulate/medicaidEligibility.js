var _ = require('lodash'),
    logger = require('winston'),
    context = require('../../../models/context'),
    dataUtil = require('../../../models/dataUtil'),
    async = require('async');
module.exports = function(flats, cb) {
    //tack in GDS eligibility information to flats
    var studentIds = _.map(flats, function(flat) {
        if (!!flat.student) {
            return flat.student.internalStudentId
        }
        return null;
    });

    studentIds = _.compact(studentIds);
    context.GDS.find()
        .where('student')
        .in(studentIds)
        .populate('rangedData')
        .exec(function(err, medicaidInfos) {
            async.waterfall([

                function(cb) {
                    context.GDS.populate(medicaidInfos, {
                        path: 'rangedData.mco',
                        model: 'MCO'
                    }, cb);
                },
                function(medicaidInfos, cb) {
                    context.GDS.populate(medicaidInfos, {
                        path: 'rangedData.mco.rangedData',
                        model: 'MCORange'
                    }, cb);
                }
            ], function(err, medicaidInfos) {
                _.each(medicaidInfos, function(medicaidInfo) {
                    var mobj = medicaidInfo.toObject();
                    filtered = _.filter(flats, function(flat) {
                        return flat.student.internalStudentId == mobj.student.toString();
                    });
                    _.each(filtered, function(flat) {

                        var temp = dataUtil.getByDateRangeSync(mobj, 'rangedData', flat.dateTime, flat.dateTime);
                        if (!temp) {
                            //... do nothing?
                        } else if (!!temp.mco) {
                            temp.mco = dataUtil.getByDateRangeSync(temp.mco, 'rangedData', flat.dateTime, flat.dateTime);
                        }
                        flat.medicaidInfo = temp;
                    });
                });
                cb(err, flats);
            });
        });
}
