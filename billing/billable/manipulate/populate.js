var mongoose = require("mongoose"),
    context = require('../../../models/context'),
    util = require("util"),
    path = require("path"),
    fs = require('fs'),
    async = require('async'),
    _ = require('lodash'),
    moment = require('moment'),
    logger = require('winston');
require('twix');

module.exports = function(schedules, includeReasons, cb) {
    //populate downstream data
    var population = [

        function(cb) {
            //get the schedule session
            context.Schedule.populate(schedules, {
                path: 'session',
                model: 'Session'
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            context.Schedule.populate(schedules, {
                path: 'session.attendees',
                model: 'Attendee'
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            //get the session provider
            context.Schedule.populate(schedules, {
                path: 'session.provider',
                model: 'User'
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            //get the session provider's mco qualifications
            context.Schedule.populate(schedules, {
                path: 'session.provider.mcoQualifications',
                model: 'MCOQualification'
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            //get the session provider's therapist qualifications
            context.Schedule.populate(schedules, {
                path: 'session.provider.therapistQualifications',
                model: 'TherapistQualification'
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            //get the attendee service
            context.Schedule.populate(schedules, {
                path: 'session.attendees.service',
                model: 'Service'
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            //get the attendee diagnostic code
            context.Schedule.populate(schedules, {
                    path: 'session.attendees.diagnosticCode',
                    model: 'DiagnosticCode'
                },
                function(err, schedules) {
                    if (!!err) {
                        logger.error(err);
                    }
                    cb(err, schedules);
                });
        },
        function(schedules, cb) {
            //get the attendee service range
            context.Schedule.populate(schedules, {
                path: 'session.attendees.service.rangedData',
                model: 'ServiceRange',
                match: {
                    rangeActive: true
                }
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            //get the attendee diagnostic code ranges
            context.Schedule.populate(schedules, {
                    path: 'session.attendees.diagnosticCode.rangedData',
                    model: 'DiagnosticCodeRange',
                    match: {
                        rangeActive: true
                    }
                },
                function(err, schedules) {
                    if (!!err) {
                        logger.error(err);
                    }
                    cb(err, schedules);
                });
        },
        function(schedules, cb) {
            //get the attendee service student
            context.Schedule.populate(schedules, {
                path: 'session.attendees.service.student',
                model: 'Student'
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            //get the attendee service student range data
            context.Schedule.populate(schedules, {
                path: 'session.attendees.service.student.rangedData',
                model: 'StudentRange',
                match: {
                    rangeActive: true
                }
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            //get the attendee service student's parental consents
            context.Schedule.populate(schedules, {
                path: 'session.attendees.service.student.parentalConsents',
                model: 'ParentalConsent',
                match: {
                    rangeActive: true
                }
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            //get the attendee service student's precriptions
            context.Schedule.populate(schedules, {
                path: 'session.attendees.service.student.prescriptions',
                model: 'Prescription',
                match: {
                    rangeActive: true
                }
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            //get the attendee service student's mco info
            context.Schedule.populate(schedules, {
                path: 'session.attendees.service.student.mcoInformation',
                model: 'StudentMCOInformationRange',
                match: {
                    rangeActive: true
                }
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            //get the session organizer
            context.Schedule.populate(schedules, {
                path: 'session.organizer',
                model: 'User'
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            context.Schedule.populate(schedules, {
                path: 'session.organizer.mcoQualifications',
                model: 'MCOQualification'
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            //get the session finalizing user
            context.Schedule.populate(schedules, {
                path: 'session.finalizingUser',
                model: 'User'
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            //get the session finalizing user's mco quals
            context.Schedule.populate(schedules, {
                path: 'session.finalizingUser.mcoQualifications',
                model: 'MCOQualification'
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            //get the session finalizing user's therapist quals
            context.Schedule.populate(schedules, {
                path: 'session.finalizingUser.therapistQualifications',
                model: 'TherapistQualification'
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            //get the session procedure code
            context.Schedule.populate(schedules, {
                path: 'session.procedureCode',
                model: 'ProcedureCode'
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            //get the session procedure code ranges
            context.Schedule.populate(schedules, {
                path: 'session.procedureCode.rangedData',
                model: 'ProcedureCodeRange',
                match: {
                    rangeActive: true
                }
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            context.Schedule.populate(schedules, {
                path: 'session.attendees.service.student.rangedData.school',
                model: 'School',
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            context.Schedule.populate(schedules, {
                path: 'session.attendees.service.student.rangedData.school.rangedData',
                model: 'SchoolRange',
                match: {
                    rangeActive: true
                }
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            context.Schedule.populate(schedules, {
                path: 'session.attendees.service.student.rangedData.school.district',
                model: 'District',
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            context.Schedule.populate(schedules, {
                path: 'session.attendees.service.student.rangedData.school.district.rangedData',
                model: 'DistrictRange',
                match: {
                    rangeActive: true
                }
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            context.Schedule.populate(schedules, {
                path: 'session.attendees.service.rangedData.diagnosticCode',
                model: 'DiagnosticCode',
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        },
        function(schedules, cb) {
            context.Schedule.populate(schedules, {
                path: 'session.attendees.service.rangedData.diagnosticCode.rangedData',
                model: 'DiagnosticCodeRange',
                match: {
                    rangeActive: true
                }
            }, function(err, schedules) {
                if (!!err) {
                    logger.error(err);
                }
                cb(err, schedules);
            });
        }
    ];

    async.waterfall(population, function(err, schedules) {
        //lean the schedules
        cb(err, _.map(schedules, function(schedule) {
            return schedule.toObject();
        }), includeReasons);
    });
};
