var _ = require('lodash'),
    context = require('../../../models/context'),
    async = require('async'),
    logger = require('winston');
module.exports = function(flats, rejections, cb) {
    logger.warn('todo: merge shorter sessions?');
    //identify sessions that are shorter than service duration
    var shorter = _.filter(flats, function(flat) {
        return flat.serviceDuration > flat.duration;
    });

    var flats = _.filter(flats, function(flat) {
        return flat.serviceDuration <= flat.duration;
    });

    //group shorter flats by service ID
    _.forIn(_.groupBy(shorter, function(d) {
        return d.serviceId;
    }), function(value, key) {
        //group those service flats by procedure code ID
        _.forIn(_.groupBy(value, function(d) {
            return d.procedureCode._id;
        }), function(value, key) {

            //reduce temporally consecutive sessions that are less than the service duration
            var reduced = _.reduce(value, function(result, val) {
                if (result.length == 0) {
                    result.push(val);
                } else {
                    var last = _.last(result);
                    if (last.duration + val.duration <= last.serviceDuration) {
                        last.duration += val.duration;
                    } else {
                        result.push(val);
                    }
                }
                return result;
            }, []);

            _.each(reduced, function(r) {
                flats.push(r);
            });
        });

    });


    cb(null, flats, rejections);
};
