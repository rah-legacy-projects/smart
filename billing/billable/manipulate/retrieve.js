var mongoose = require("mongoose"),
    context = require('../../../models/context'),
    logger = require('winston'),
    async = require('async'),
    changed = require('./changed'),
    _ = require('lodash');
require('twix');

module.exports = function(range, cb) {
    var tasks = [];
    if (!!range.schedulesOnly) {
        tasks = [

            function(cb) {
                changed.schedules(range, cb);
            }
        ];
    } else {
        tasks = [

            function(cb) {
                changed.diagnosticCode(range, cb);
            },
            function(cb) {
                changed.gds(range, cb);
            },
            function(cb) {
                changed.mcoApprovals(range, cb);
            },
            function(cb) {
                changed.parentalConsents(range, cb);
            },
            function(cb) {
                changed.prescriptions(range, cb);
            },
            function(cb) {
                changed.procedureCode(range, cb);
            },
            function(cb) {
                changed.procedureCodeValue(range, cb);
            },
            function(cb) {
                changed.schedules(range, cb);
            },
            function(cb) {
                changed.services(range, cb);
            }
        ];
    };

    async.parallel(tasks, function(err, results) {
        if (!!err) {
            logger.error("error in retrieval: " + util.inspect(err));
        }
        //reduce results to single set
        var flatArray = _.reduceRight(results, function(a, b) {
            a = a || [];
            b = b || [];
            return a.concat(b);
        }, []);

        var includeReasons = _.chain(flatArray)
            .map(function(f) {
                return {
                    id: f.id,
                    includeReason: f.includeReason
                };
            })
            .groupBy(function(a) {
                return a.id;
            })
            .value();

        //unique based on schedule id
        flatArray = _.uniq(flatArray, function(a) {
            return a._id.toString();
        });

        //filter deleted schedules
        flatArray = _.filter(flatArray, function(a) {
            return !a.deleted;
        });

        //sort on datetime
        flatArray = _.sortBy(flatArray, function(a) {
            return a.dateTime;
        })

        cb(err, flatArray, includeReasons);
    });
};
