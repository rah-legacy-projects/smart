var _ = require('lodash'),
    context = require('../../../models/context'),
    async = require('async'),
    logger = require('winston'),
    makeRejection = require('../makeRejection');
module.exports = function(flats, cb) {
    //fix up diagnostic codes
    var fixups = [];
    _.chain(flats)
        .filter(function(flat) {
            return !flat.diagnosticCode //|| flat.diagnosticCode._id != flat.serviceDiagnosticCode._id;
        })
        .each(function(flat) {
            fixups.push(function(cb) {
                context.Session.findOne({
                    _id: flat.sessionId
                }).populate('attendees')
                    .exec(function(err, session) {
                        var attendee = _.find(session.attendees, function(a) {
                            return a.service.toString() == flat.serviceId.toString();
                        });

                        if (!!flat.serviceDiagnosticCode && !attendee.diagnosticCode) {
                            attendee.diagnosticCode = flat.serviceDiagnosticCode._id;
                            flat.diagnosticCode = flat.serviceDiagnosticCode;
                            var temp = _.filter(session.attendees, function(a) {
                                return a.service.toString() != flat.serviceId.toString();
                            });
                            temp.push(attendee);


                            session.attendees = temp;
                            session.save(function(err) {
                                cb(err, session);
                            });
                        }else{
                            cb(null, session);
                        }
                    });
            });
        });

    //note: upon fixup completion, always use flat.serviceDiagnosticCode
    async.parallel(fixups, function(err, sessions) {
        cb(err, flats);
    });
}
