var _ = require('lodash'),
    context = require('../../../models/context'),
    async = require('async'),
    logger = require('winston');
module.exports = function(flats, cb) {
    //for each flat,
    _.each(flats, function(flat) {
        //start everything with one unit
        flat.iepUnits = 1;
        //deduct the first unit from the duration
        var miniBank = flat.duration-flat.serviceDuration;;
        //while there is still time,
        while (miniBank > 0) {
            //add units
            flat.iepUnits++;
            //deduct the service duration
            miniBank -= flat.serviceDuration;
        }
    });
    cb(null, flats);
};
