var _ = require('lodash'),
    context = require('../../../models/context'),
    async = require('async'),
    logger = require('winston');
module.exports = function(flats, rejections, cb) {
    //identify what sessions are longer than the service for that student and split them

    //pull the longer than service flats
    var longer = _.filter(flats, function(flat) {
        return flat.serviceDuration < flat.duration;
    });

    //filter the longer than service flats out
    flats = _.filter(flats, function(flat) {
        return flat.serviceDuration >= flat.duration;
    });

    //for each flat longer than service,
    _.each(longer, function(flat) {
        var miniBank = flat.duration;
        //while there is still time,
        while (miniBank >= 0) {
            //clone the flat
            var newFlat = _.clone(flat);
            if (miniBank - flat.serviceDuration < 0) {
                //if time remaining is less than the service duration, use the time remaining
                newFlat.duration = miniBank;
            } else {
                //otherwise, use the service duration
                newFlat.duration = flat.serviceDuration;
            }
            //deduct time
            minibank -= flat.serviceDuration;
            //add the split flat to flats
            flats.push(newFlat);
        }
    });
    cb(null, flats, rejections);
};
