var _ = require('lodash'),
    context = require('../../../../models/context'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston')
module.exports = function(range, cb) {
    var tasks = [

        function(cb) {
            //identify procedure codes that have been changed in the range 
            context.ProcedureCode
                .find()
                .where('modified')
                .gte(moment(range.startDate)
                    .toDate())
                .where('modified')
                .lte(moment(range.endDate)
                    .toDate())
                .exec(function(err, procedureCodes) {
                    cb(err, _.map(procedureCodes, function(pc) {
                        return pc.id;
                    }));
                });
        },
        function(pcids, cb) {
            //get the services those diagnostic codes are used for
            context.Session.find()
                .where('procedureCode')
                .in(pcids)
                .exec(function(err, sessions) {
                    cb(err, _.map(sessions, function(s) {
                        return s.id;
                    }));
                });
        },
        function(sessionIds, cb) {
            //get the schedules for those services
            //the schedules must be before the start date of range (prevents overlap)
            context.Schedule.find()
                .where('session')
                .in(sessionIds)
                .where('dateTime')
                .lte(moment(range.startDate)
                    .toDate())
                .exec(function(err, schedules) {

                    _.each(schedules, function(schedule) {
                        schedule.includeReason = {
                            priority: 1,
                            reason: 'Procedure code modified'
                        };
                    });
                    cb(err, schedules);
                });
        }
    ];

    async.waterfall(tasks, function(err, schedules) {
        if (!!err) {
            logger.error("error in schedule retrieval for procedure code: " + util.inspect(err));
        }
        logger.debug('retrieved schedules for touched procedure codes');
        cb(err, schedules);
    });
}
