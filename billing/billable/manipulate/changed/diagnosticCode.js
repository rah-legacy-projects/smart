var _ = require('lodash'),
    context = require('../../../../models/context'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston');
module.exports = function(range, cb) {
    var tasks = [

        function(cb) {
            //identify diagnostic codes that have been changed in the range 
            context.DiagnosticCode
                .find()
                .where('modified')
                .gte(moment(range.startDate)
                    .toDate())
                .where('modified')
                .lte(moment(range.endDate)
                    .toDate())
                .exec(function(err, diagnosticCodes) {
                    cb(err, _.map(diagnosticCodes, function(dc) {
                        return dc.id;
                    }));
                });
        },
        function(dcids, cb) {
            //get the services those diagnostic codes are used for
            context.Service.find()
                .where('diagnosticCode')
                .in(dcids)
                .exec(function(err, services) {
                    cb(err, _.map(services, function(s) {
                        return s.id;
                    }));
                });
        },
        function(serviceIds, cb) {
            //get attendees for those service IDs
            context.Attendee.find()
                .where('service')
                .in(serviceIds)
                .exec(function(err, attendees) {
                    cb(err, _.map(attendees, function(s) {
                        return s.id;
                    }));
                });
        },
        function(attendeeIds, cb) {
            //get the sessions those service IDs are in
            context.Session.find()
                .elemMatch('attendees', {
                    $in: attendeeIds
                })
                .exec(function(err, sessions) {
                    cb(err, _.map(sessions, function(s) {
                        return s.id;
                    }));
                });
        },
        function(sessionIds, cb) {
            //get the schedules for those sessions
            //the schedules must be before the start date of range (prevents overlap)
            context.Schedule.find()
                .where('session')
                .in(sessionIds)
                .where('dateTime')
                .lte(moment(range.startDate)
                    .toDate())
                .exec(function(err, schedules) {

                    _.each(schedules, function(schedule) {
                        schedule.includeReason = {
                            priority: 1,
                            reason: 'Diagnostic code data modified'
                        };
                    });

                    cb(err, schedules);
                });
        }
    ];

    async.waterfall(tasks, function(err, schedules) {
        if (!!err) {
            logger.error("error in schedule retrieval by dc: " + util.inspect(err));
        }
        logger.debug('retrieved schedules for touched diagnostic codes');
        cb(err, schedules);
    });
}
