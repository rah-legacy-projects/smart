var _ = require('lodash'),
    context = require('../../../../models/context'),
    moment = require('moment'),
    async = require('async'),
    logger = require('winston');
module.exports = function(range, cb) {
    var tasks = [

        function(cb) {
            //identify gds infos that have been changed in the range 
            context.GDS
                .find()
                .where('modified')
                .gte(moment(range.startDate)
                    .toDate())
                .where('modified')
                .lte(moment(range.endDate)
                    .toDate())
                .exec(function(err, gds) {
                    if (!!err) {
                        logger.error("wf1 error in schedule retrieval for gds: " + util.inspect(err));
                    }
                    cb(err, _.compact(_.map(gds, function(g) {
                        return g.student;
                    })));
                });
        },
        function(studentIds, cb) {
            context.Service.find()
                .where('student')
                .in(studentIds)
                .exec(function(err, services) {
                    if (!!err) {
                        logger.error("wf2 error in schedule retrieval for gds: " + util.inspect(err));
                    }
                    cb(err, _.map(services, function(service) {
                        return service._id;
                    }));
                });
        },
        function(serviceIds, cb) {
            //get attendees for those service IDs
            context.Attendee.find()
                .where('service')
                .in(serviceIds)
                .exec(function(err, attendees) {
                    cb(err, _.map(attendees, function(s) {
                        return s.id;
                    }));
                });
        },
        function(attendeeIds, cb) {
            //get the sessions those service IDs are in
            context.Session.find()
                .elemMatch('attendees', {
                    $in: attendeeIds
                })
                .exec(function(err, sessions) {
                    cb(err, _.map(sessions, function(s) {
                        return s.id;
                    }));
                });
        },
        function(sessionIds, cb) {
            //get the schedules for those services
            //the schedules must be before the start date of range (prevents overlap)
            context.Schedule.find()
                .where('session')
                .in(sessionIds)
                .where('dateTime')
                .lte(moment(range.startDate)
                    .toDate())
                .exec(function(err, schedules) {
                    if (!!err) {
                        logger.error("wf4 error in schedule retrieval for gds: " + util.inspect(err));
                    }
                    _.each(schedules, function(schedule) {
                        schedule.includeReason = {
                            priority: 1,
                            reason: 'GDS data modified'
                        };
                    });
                    cb(err, schedules);
                });
        }
    ];

    async.waterfall(tasks, function(err, schedules) {
        if (!!err) {
            logger.error("error in schedule retrieval for gds: " + util.inspect(err));
        }
        logger.debug('retrieved schedules for touched GDS');
        cb(err, schedules);
    });
}
