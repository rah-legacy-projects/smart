var mongoose = require("mongoose"),
    context = require('../../../../models/context'),
    moment = require('moment'),
    _ = require('lodash'),
    logger = require('winston');
require('twix');

module.exports = function(range, cb) {
    //find schedules within the range
    context.Schedule.find({
        $and: [{
            dateTime: {
                $gte: range.startDate
            }
        }, {
            dateTime: {
                $lte: range.endDate
            }
        }]
    })
        .exec(function(err, schedules) {
            if (!!err) {
                logger.error("error in schedule retrieval: " + util.inspect(err));
            }
            _.each(schedules, function(schedule) {
                schedule.includeReason = {
                    priority: 0,
                    reason: 'Schedule in range'
                };
            });
            cb(err, schedules);
        });
};
