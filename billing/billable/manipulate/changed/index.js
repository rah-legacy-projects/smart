module.exports = {
    diagnosticCode: require("./diagnosticCode"),
    gds: require("./gds"),
    index: require("./index"),
    mcoApprovals: require("./mcoApprovals"),
    parentalConsents: require("./parentalConsents"),
    prescriptions: require("./prescriptions"),
    procedureCode: require("./procedureCode"),
    procedureCodeValue: require("./procedureCodeValue"),
    schedules: require("./schedules"),
    services: require("./services")
}
