var _ = require('lodash'),
    context = require('../../../../models/context'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston');
module.exports = function(range, cb) {
    var tasks = [

        function(cb) {
            //identify prescriptions that have been changed in the range 
            context.Prescription
                .find()
                .where('modified')
                .gte(moment(range.startDate)
                    .toDate())
                .where('modified')
                .lte(moment(range.endDate)
                    .toDate())
                .exec(function(err, rxs) {
                    cb(err, _.map(rxs, function(rx) {
                        return rx.id;
                    }));
                });
        },
        function(rxids, cb) {
            context.Student
                .find()
                .elemMatch('prescriptions', {
                    $in: rxids
                })
                .exec(function(err, students) {
                    cb(err, _.map(students, function(student) {
                        return student.id;
                    }));
                });
        },
        function(studentIds, cb) {
            context.Service.find()
                .where('student')
                .in(studentIds)
                .exec(function(err, services) {
                    cb(err, _.map(services, function(service) {
                        return service.id;
                    }));
                });
        },
        function(serviceIds, cb) {
            //get attendees for those service IDs
            context.Attendee.find()
                .where('service')
                .in(serviceIds)
                .exec(function(err, attendees) {
                    cb(err, _.map(attendees, function(s) {
                        return s.id;
                    }));
                });
        },
        function(attendeeIds, cb) {
            //get the sessions those service IDs are in
            context.Session.find()
                .elemMatch('attendees', {
                    $in: attendeeIds
                })
                .exec(function(err, sessions) {
                    cb(err, _.map(sessions, function(s) {
                        return s.id;
                    }));
                });
        },
        function(sessionIds, cb) {
            //get the schedules for those sessions
            //the schedules must be before the start date of range (prevents overlap)
            context.Schedule.find()
                .where('session')
                .in(sessionIds)
                .where('dateTime')
                .lte(moment(range.startDate)
                    .toDate())
                .exec(function(err, schedules) {
                    _.each(schedules, function(schedule) {
                        schedule.includeReason = {
                            priority: 1,
                            reason: 'Prescription data modified'
                        };
                    });
                    cb(err, schedules);
                });
        }
    ];

    async.waterfall(tasks, function(err, schedules) {
        if (!!err) {
            logger.error("error in schedule retrieval for prescription: " + util.inspect(err));
        }
        logger.debug('retrieved schedules for touched prescriptions');
        cb(err, schedules);
    });
}
