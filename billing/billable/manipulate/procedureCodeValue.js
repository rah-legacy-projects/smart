var _ = require('lodash'),
    logger = require('winston'),
    context = require('../../../models/context'),
    dataUtil = require('../../../models/dataUtil'),
    async = require('async');
module.exports = function(flats, cb) {
    //tack in procedure code values to flats
    var procedureCodeIds = _.map(_.filter(flats, function(flat) {
        return !!flat.procedureCode;
    }), function(flat) {
        return flat.procedureCode._id;
    });

    context.ProcedureCodeValue.find()
        .where('procedureCode')
        .in(procedureCodeIds)
        .populate('rangedData')
        .lean()
        .exec(function(err, procedureCodeValues) {
            //for each flat,
            _.each(flats, function(flat) {
                if (!!flat.procedureCode && !!flat.medicaidInfo) {
                    //get the appropriate procedure code value and clone it
                    var pcv = _.chain(procedureCodeValues)
                        .find(function(procedureCodeValue) {
                            return procedureCodeValue.procedureCode.toString() == flat.procedureCode._id 
                                && flat.medicaidInfo.mco._id == procedureCodeValue.mco.toString();
                        })
                        .value();
                    if (!!pcv) {
                        pcv = _.clone(pcv)
                        //assign procedure code value for the session time to the procedure code
                        flat.procedureCode.procedureCodeValue = dataUtil.getByDateRangeSync(pcv, 'rangedData', flat.dateTime, flat.dateTime);
                    }
                }
            });

            cb(err, flats);
        });
}
