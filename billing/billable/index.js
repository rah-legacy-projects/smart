var reject = require('./reject'),
    manipulate = require('./manipulate'),
    async = require('async'),
    _ = require('lodash'),
    logger = require('winston');
module.exports = function(range, cb) {
    var tasks = [

        function(cb) {
            //retrieve for range with empty rejection set
            manipulate.retrieve(range, cb);
        },
        reject.session,
        manipulate.populate,
        manipulate.flatten,
        function(flats, cb) {
            cb(null, flats);
        },
        reject.student,
        reject.school,
        reject.district,
        manipulate.medicaidEligibility,
        manipulate.diagnosticCode,
        manipulate.procedureCodeValue,
        manipulate.iepUnits,
        manipulate.lastModifiedBy,

        reject.procedureCode,

        reject.finalized,
        reject.billed,

        reject.gds,
        reject.gdsSSN,
        reject.medicaidIneligible,
        reject.procedureCodeValue,

        reject.therapistNPI,
        reject.therapistSSN,
        reject.therapistMCOApproval,
        reject.absent,
        reject.diagnosticCode,

        reject.parentalConsent,
        reject.toTreat,
        reject.toTreatDenied,
        reject.toBill,
        reject.toBillDenied,

        reject.mcoInfo,
        reject.studentMCOAddress,
        reject.studentMCOHealthPlanId,
        reject.studentNotMCOApproved,

        reject.prescription,
        reject.prescriptionMatch,
        function(flats, cb) {
            cb(null, flats);
        },
        reject.iepApprovedSessions,
        function(flats, cb) {
            cb(null, flats);
        },
        reject.mcoApprovedSessions,
        function(flats, cb) {
            cb(null, flats);
        },
    ];
    async.waterfall(tasks, function(err, flats) {
        logger.debug('finished deriving billables');
        logger.debug('total flats: ' + flats.length);
        logger.debug('billable flats: ' + _.filter(flats, function(flat) {
                return flat.errors.length == 0;
            })
            .length)
        if (!!err) {
            logger.error(err);
        }
        cb(err, flats);
    });
}
