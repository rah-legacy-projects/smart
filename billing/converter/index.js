 var _ = require('lodash'),
     logger = require('winston'),
     context = require('../../models/context'),
     _ = require('lodash'),
     moment = require('moment-timezone'),
     components = require('./components');

 module.exports = function(options, billable, cb) {

     options = options || {};

     options = _.defaults(options, {
         timezone: 'America/Chicago'
     });

     var groupIds = {};

     var adjustedNow = moment.tz(moment(), options.timezone);

     //ensure all billables are billable
     var originalBillableLength = billable.length;
     billable = _.filter(billable, function(b) {
         return !b.errors || b.errors.length == 0;
     });
     if (billable.length !== originalBillableLength) {
         logger.warn('check and make sure you are only passing in billable sessions to convert');
     }

     var billparts = [];

     //add the control header
     billparts.push(components.interchangeControlHeader({
         interchangeDate: adjustedNow.format('YYMMDD'),
         interchangeTime: adjustedNow.format('HHmm')
     }));

     //add functional group header
     billparts.push(components.functionalGroupHeader({
         date: adjustedNow.format('YYYYMMDD'),
         time: adjustedNow.format('HHmm')
     }));

     //add transaction set header
     billparts.push(components.transactionSetHeader({
         transactionSetControlNumber: 1,
     }));

     //begin hierarchical transaction
     billparts.push(components.beginningOfHierarchicalTransaction({
         date: adjustedNow.format('YYYYMMDD'),
         time: adjustedNow.format('HHmm')
     }));

     //add submitter name
     billparts.push(components.submitterName());

     //add submitter edi contact
     billparts.push(components.submitterEDIContactName());

     //add reciever name
     //todo: backfill this with information from jean's email & number 7
     billparts.push(components.receiverName());

     var hl = 1;

     //group billable records by provider
     _.forIn(_.groupBy(billable, function(b) {
         return b.finalizingUser._id;
     }), function(groupedByProvider, providerId) {

         //add HL record for the provider
         billparts.push(components.billingProviderHierarchicalLevel({
             hierarchicalIdNumber: hl++
         }));

         //add billing provider name,
         billparts.push(components.billingProviderName({
             nameLastOrOrganizationName: groupedByProvider[0].finalizingUser.lastName,
             nameFirst: groupedByProvider[0].finalizingUser.firstName,
             nameMiddle: groupedByProvider[0].finalizingUser.middleName || '',
             nameSuffix: '',
             identificationCode: groupedByProvider[0].finalizingUser.therapistQualification.npiNumber,
         }));
         //billing provider address,
         //hack: use the student's district for the billing provider
         //hack: student and provider's should be the same
         //note: should this be using billing info address?
         billparts.push(components.billingProviderAddress({
             addressInformation: groupedByProvider[0].district.address1,
             addressInformation2: groupedByProvider[0].district.address2
         }));

         //billing provider city state zip,
         billparts.push(components.billingProviderCityStateZIP({
             cityName: groupedByProvider[0].district.city,
             stateCode: groupedByProvider[0].district.state,
             postalCode: groupedByProvider[0].district.zip
         }));

         //billing provider tax id,
         billparts.push(components.billingProviderTaxIdentification({
             referenceIdentification: groupedByProvider[0].finalizingUser.ssn.replace(/\-/g, '')
         }));

         //pay-to name,
         billparts.push(components.payToAddressName({
             nameLastOrOrganizationName: groupedByProvider[0].district.name,
             nameFirst: '',
             nameMiddle: '',
             nameSuffix: '',
             identificationCode: groupedByProvider[0].district.npiNumber
         }));

         //pay-to address,
         billparts.push(components.payToAddress({
             addressInformation: groupedByProvider[0].district.address1,
             addressInformation2: groupedByProvider[0].district.address2
         }));

         //pay-to city state zip
         billparts.push(components.payToAddressCityStateZIP({
             cityName: groupedByProvider[0].district.city,
             stateCode: groupedByProvider[0].district.state,
             postalCode: groupedByProvider[0].district.zip
         }));

         //group provider-grouped records by student
         _.forIn(_.groupBy(groupedByProvider, function(b) {
             return b.student.internalStudentId;
         }), function(groupedByStudent, studentId) {
             //group student-grouped records by diagnostic code
             var groupedByDiagnosticCode = _.groupBy(groupedByStudent, function(b) {
                 return b.diagnosticCode.diagnosticCode;
             });

             for (var i = 0; i < _.keys(groupedByDiagnosticCode)
                 .length; i += 12) {
                 var diagnosticCodeKeys = _.keys(groupedByDiagnosticCode)
                     .slice(i, i + 12);
                 //create the billing group
                 var groupId = context.ObjectId()
                     .toString()
                     .slice(0, 8);
                 if (!!groupIds[groupId]) {
                     groupIds[groupId].incrementer++;
                 } else {
                     groupIds[groupId] = {incrementer:0};
                 }
                 //subscriber hl
                 billparts.push(components.subscriberHierarchicalLevel({
                     hierarchicalIdNumber: hl++
                 }));

                 //add subscriber info
                 billparts.push(components.subscriberInformation({}));

                 //add subscriber name
                 billparts.push(components.subscriberName({
                     nameLastOrOrganizationName: groupedByStudent[0].medicaidInfo.lname,
                     nameFirst: groupedByStudent[0].medicaidInfo.fname,
                     nameMiddle: groupedByStudent[0].medicaidInfo.mname,
                     //hack: is the medicaid number the plan number?
                     identificationCode: groupedByStudent[0].medicaidInfo.planBeneNum
                 }));

                 //add subscriber address
                 billparts.push(components.subscriberAddress({
                     addressInformation: groupedByStudent[0].medicaidInfo.address1,
                     addressInformation2: groupedByStudent[0].medicaidInfo.address2
                 }));

                 //add subscriber citystatezup
                 billparts.push(components.subscriberCityStateZip({
                     cityName: groupedByStudent[0].medicaidInfo.city,
                     stateCode: groupedByStudent[0].medicaidInfo.state,
                     postalCode: groupedByStudent[0].medicaidInfo.zip
                 }));

                 //add demographic information
                 billparts.push(components.subscriberDemographicInformation({
                     dateTimePeriod: moment(groupedByStudent[0].medicaidInfo.dOB)
                         .format('YYYYMMDD'),
                     genderCode: groupedByStudent[0].medicaidInfo.gender
                 }));

                 //add subscriber secondary information
                 billparts.push(components.subscriberSecondaryInformation({
                     referenceIdentification: groupedByStudent[0].medicaidInfo.sSN.replace(/\-/g, '')
                 }));

                 //add payer name
                 billparts.push(components.payerName({
                     nameLastOrOrganizationName: groupedByStudent[0].medicaidInfo.mco.name,
                     //hack: what is the id code for MCOs?
                     identificationCode: groupedByStudent[0].medicaidInfo.mco.healthPlanId
                 }));

                 //add payer address
                 billparts.push(components.payerAddress({
                     addressInformation: groupedByStudent[0].medicaidInfo.mco.address1,
                     addressInformation2: groupedByStudent[0].medicaidInfo.mco.address2,
                 }));

                 //add payer city state zip
                 billparts.push(components.payerCityStateZIP({
                     cityName: groupedByStudent[0].medicaidInfo.mco.city,
                     stateCode: groupedByStudent[0].medicaidInfo.mco.state,
                     postalCode: groupedByStudent[0].medicaidInfo.mco.zip
                 }));

                 //add up to 12 diagnostic codes
                 var dcopts = {};
                 var dcrollup = {};
                 _.each(diagnosticCodeKeys, function(key, i) {
                     dcopts['codeListQualifierCode' + (i + 1)] = 'BK';
                     var periodlessDC = key.replace(/\./g, '');
                     dcopts['industryCode' + (i + 1)] = periodlessDC;
                     dcrollup[periodlessDC] = (i + 1);
                 });

                 var claimTotal = _.reduce(_.filter(groupedByStudent, function(g) {
                     return _.any(_.keys(dcrollup), function(k) {
                         return k == g.diagnosticCode.diagnosticCode.replace(/\./g, '');
                     });
                 }), function(sum, v) {
                     return sum + (v.iepBillableUnits * v.procedureCode.procedureCodeValue.value);
                 }, 0);

                 //add claim totals
                 billparts.push(components.claimInformation({
                     //hack: use the timestamp part of the group ID
                     //hack: this must be done because 837p only allows for 20 characters in the echo ID
                     claimSubmitterIdentifier: groupId + groupIds[groupId].incrementer,
                     monetaryAmount: claimTotal
                 }));

                 //add epsdt referral
                 billparts.push(components.epsdtReferral());


                 //add the diagnostic code parts
                 billparts.push(components.healthCareDiagnosisCode(dcopts));

                 _.each(_.filter(groupedByStudent, function(g) {
                     return _.any(_.keys(dcrollup), function(k) {
                         return k == g.diagnosticCode.diagnosticCode.replace(/\./g, '');
                     });
                 }), function(flat, index) {
                     //give the flat the group ID
                     flat.groupId = groupId + groupIds[groupId].incrementer;
                     //service line
                     billparts.push(components.serviceLineNumber({
                         assignedNumber: index + 1,
                     }));

                     //professional service
                     billparts.push(components.professionalService({
                         serviceIdQualifier: 'HC',
                         procedureCode: flat.procedureCode.procedureCode,
                         procedureModifier1: null,
                         procedureModifier2: null,
                         procedureModifier3: null,
                         procedureModifier4: null,
                         description: null,
                         //hack: all procedure code values are for fifteen minute units?
                         monetaryAmount: flat.procedureCode.procedureCodeValue.value * flat.iepBillableUnits,
                         unitMeasurementCode: 'UN',
                         //hack: all units are in 15 minute increments...?
                         quantity: flat.iepBillableUnits,
                         facilityCodeValue: null,
                         diagnosisCodePointer1: dcrollup[flat.diagnosticCode.diagnosticCode.replace(/\./g, '')],
                         diagnosisCodePointer2: null,
                         diagnosisCodePointer3: null,
                         diagnosisCodePointer4: null,
                         emergencyIndicator: null,
                         epsdtIndicator: null,
                         familyPlanningIndicator: null,
                         copayStatusCode: null
                     }));


                     //date
                     billparts.push(components.serviceDate({
                         dateTimePeriod: ('{start}-{end}')
                             .format({
                                 start: moment(flat.dateTime)
                                     .format('YYYYMMDD'),
                                 end: moment(flat.dateTime)
                                     .format('YYYYMMDD')
                             })
                     }));


                     //line item control number
                     billparts.push(components.lineItemControlNumber({
                         referenceIdentification: index + 1,
                     }));
                 });

             }
         });
     });

     //transaction set trailer
     var partsSoFar = billparts.length;
     billparts.push(components.transactionSetTrailer({
         numberOfIncludedSegments: partsSoFar,
         trasactionSetControlNumber: 1,
     }));

     //functional group trailer
     billparts.push(components.functionalGroupTrailer({
         numberOfTransactionSetsIncluded: 1,
         groupControlNumber: 1,
     }));

     //interchange control trailer
     billparts.push(components.interchangeControlTrailer({
         numberOfIncludedFunctionalGroups: 1,
         interchangeControlNumber: '000000001',
     }));

     //join the parts together
     var fileContents = billparts.join('\n');
     fileContents = fileContents.toUpperCase();

     //call back
     logger.debug('about to return billing file contents.');
     cb(null, fileContents);
 };
