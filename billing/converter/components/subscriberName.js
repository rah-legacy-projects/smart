var logger = require('winston'),
    _ = require('lodash'),
    commonBilling = require('./common');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        entityIdentifierCode: 'IL',
        entityTypeQualifier: '1',
        nameLastOrOrganizationName: '[student last from gds]',
        nameFirst: '[student first from gds]',
        nameMiddle: '[student middle from gds]',
        nameSuffix: '',
        identificationCodeQualifier: 'MI',
        identificationCode: '[plan number from gds]',
    });
    return commonBilling.nm1(options);
};
