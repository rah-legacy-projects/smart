var logger = require('winston'),
    _ = require('lodash'),
    commonBilling = require('./common');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        entityIdentifierCode: '85',
        entityTypeQualifier: '1',
        nameLastOrOrganizationName: '[therapist last]',
        nameFirst: '[therapist first]',
        nameMiddle: '[therapist middle]',
        nameSuffix: '[therapist suffix]',
        identificationCodeQualifier: 'XX',
        identificationCode: '[therapist npi]',
    });

    return commonBilling.nm1(options);

};
