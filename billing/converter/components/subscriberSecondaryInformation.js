var logger = require('winston'),
    _ = require('lodash');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        referenceIdentificationQualifier: 'SY',
        referenceIdentification: null,
    });

    return ('REF*{referenceIdentificationQualifier}' +
            '*{referenceIdentification}~')
        .format(options);
};
