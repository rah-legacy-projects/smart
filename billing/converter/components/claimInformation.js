var logger = require('winston'),
    _ = require('lodash');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        claimSubmitterIdentifier: '[pt id from gds]',
        monetaryAmount: '[total billed amount]',
        clm03: '',
        clm04: '',
        facilityCodeValue: '03',
        facilityCodeQualifier: 'B',
        claimFrequencyTypeCode: '1',
        providerOrSupplierSignatureIndicator: 'Y',
        assignmentOrPlanParticipationCode: 'C',
        benefitsAssignmentCertificationIndicator: 'N',
        releaseOfInformationCode: 'Y',
        patientSignatureSourceCode: 'P',
        relatedCausesInformation: null,
        relatedCausesCode: null,
        stateCode: null,
        countryCode: null,
        specialProgramCode: null,
        delayReasonCode: null,
    });

    return ('CLM*{claimSubmitterIdentifier}' +
            '*{monetaryAmount}' +
            '*{clm03}'+
            '*{clm04}'+
            '*{facilityCodeValue}:{facilityCodeQualifier}:{claimFrequencyTypeCode}' +
            '*{providerOrSupplierSignatureIndicator}' +
            '*{assignmentOrPlanParticipationCode}' +
            '*{benefitsAssignmentCertificationIndicator}' +
            '*{releaseOfInformationCode}' +
            '*{patientSignatureSourceCode}~')
        /*
            '*{relatedCausesInformation}' +
            '*{relatedCausesCode}' +
            '*{stateCode}' +
            '*{countryCode}' +
            '*{specialProgramCode}' +
            '*{delayReasonCode}~')
            */
        .format(options);
};
