var logger = require('winston'),
    _ = require('lodash');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        hierarchicalIdNumber: '{billing provider record number}',
        hierarchicalLevelCode: null,
        hierarchicalChildCode: '20',
        moreRecords: '1',
    });

    return ('HL*{hierarchicalIdNumber}' +
            '*{hierarchicalLevelCode}' +
            '*{hierarchicalChildCode}' +
            '*{moreRecords}~')
        .format(options);
};
