var logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    commonBilling = require('./common');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        entityIdentifierCode: '41',
        entityTypeQualifier: '2',
        nameLastOrOrganizationName: 'Beacon Analytics',
        nameFirst: null,
        nameMiddle: null,
        identificationCodeQualifier: '46',
        identificationCode: '26-3083779',
    });

    return commonBilling.nm1(options);
};
