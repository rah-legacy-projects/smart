var logger = require('winston'),
    _ = require('lodash'),
    commonBilling = require('./common');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        addressInformation: '[addr 1 from gds]',
        addressInformation2: '[addr2 from gds]',
    });
    return commonBilling.n3(options);
};
