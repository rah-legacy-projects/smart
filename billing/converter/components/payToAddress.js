var logger = require('winston'),
    _ = require('lodash'),
    commonBilling = require('./common');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        addressInformation1: '[line 1 - school district]',
        addressInformation2: '[line 2 - school district]',
    });
    return commonBilling.n3(options);
};
