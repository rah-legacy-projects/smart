var logger = require('winston'),
    moment = require('moment'),
    _ = require('lodash');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        functionalIdentifierCode: 'HC',
        applicationSendersCode: 'APP SENDER CODE',
        applicationCode: 'APP RCVR CODE',
        date: moment()
            .format("YYYYMMDD"),
        time: moment()
            .format("HHMM"),
        groupControlNumber: '1',
        responsibleAgencyCode: 'X',
        version: '005010X222A1',
    });

    return ('GS*{functionalIdentifierCode}' +
            '*{applicationSendersCode}' +
            '*{applicationCode}' +
            '*{date}' +
            '*{time}' +
            '*{groupControlNumber}' +
            '*{responsibleAgencyCode}' +
            '*{version}~')
        .format(options);
};
