var logger = require('winston'),
    _ = require('lodash');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        healthCareCodeInformation: null,
        codeListQualifierCode1: 'BK',industryCode1: '[periodless dc]', healthCareCodeInformation1: null,
        codeListQualifierCode2: null,industryCode2: null, healthCareCodeInformation2: null,
        codeListQualifierCode3: null,industryCode3: null, healthCareCodeInformation3: null,
        codeListQualifierCode4: null,industryCode4: null, healthCareCodeInformation4: null,
        codeListQualifierCode5: null,industryCode5: null, healthCareCodeInformation5: null,
        codeListQualifierCode6: null,industryCode6: null, healthCareCodeInformation6: null,
        codeListQualifierCode7: null,industryCode7: null, healthCareCodeInformation7: null,
        codeListQualifierCode8: null,industryCode8: null, healthCareCodeInformation8: null,
        codeListQualifierCode9: null,industryCode9: null, healthCareCodeInformation9: null,
        codeListQualifierCode10: null,industryCode10: null, healthCareCodeInformation10: null,
        codeListQualifierCode11: null,industryCode11: null, healthCareCodeInformation11: null,
        codeListQualifierCode12: null,industryCode12: null, healthCareCodeInformation12: null,
    });

    var formatstr = 'HI';
    for(var i=1; i<=12; i++){
        if(!!options['codeListQualifierCode'+i] && !!options['industryCode'+i]){
            formatstr += '*{codeListQualifierCode'+i+'}:{industryCode'+i+'}';
            if(!!options['healthCareCodeInformation'+i]){
                formatstr +='*{healthCareCodeInformation'+i+'}';
            }
        }
    }
    formatstr += '~';

    return formatstr.format(options);
};
