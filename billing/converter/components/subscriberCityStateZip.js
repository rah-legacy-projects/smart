var logger = require('winston'),
    _ = require('lodash'),
    commonBilling = require('./common');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        cityName: '[city]',
        stateCode: '[state]',
        postalCode: '[zip+4]',
        countryCode: null,
        countrySubdivisionCode: null,
    });
    return commonBilling.n4(options);
};
