var logger = require('winston'),
    _ = require('lodash');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        dateTimePeriodFormatQualifier: 'D8',
        dateTimePeriod: null,
        genderCode: null,
    });

    return ('DMG*{dateTimePeriodFormatQualifier}' +
            '*{dateTimePeriod}' +
            '*{genderCode}~')
        .format(options);
};
