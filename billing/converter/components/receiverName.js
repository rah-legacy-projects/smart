var logger = require('winston'),
    _ = require('lodash'),
    commonBilling = require('./common');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        entityIdentifierCode: '40',
        entityTypeQualifier: '2',
        nameLastOrOrganizationName: 'Proximed',
        identificationCodeQualifier: '46',
        identificationCode: '[todo: proximed ein]',
    });

    return commonBilling.nm1(options);
};
