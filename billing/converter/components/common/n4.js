var logger = require('winston'),
    _ = require('lodash');

require('../../../../util')();

module.exports = function(options) {
    options = options || {};
    /*
    options
        cityName
        stateCode
        postalCode
        countryCode
        countrySubdivisionCode
    */
    var formatstr = 'N4*{cityName}' +
        '*{stateCode}' +
        '*{postalCode}';
    if (!!options.countryCode && !(/^\s*$/.test(options.countryCode))) {
        formatstr += '*{countryCode}';
    }
    if (!!options.countrySubdivisionCode && !(/^\s*$/.test(options.countrySubdivisionCode))) {
        formatstr = '*{countrySubdivisionCode}';
    }
    formatstr += '~';
    return formatstr.format(options);
};
