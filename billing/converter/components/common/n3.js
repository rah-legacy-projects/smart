var logger = require('winston'),
    _ = require('lodash');

require('../../../../util')();

module.exports = function(options) {
    options = options || {};
    /*
    options:
        addressInformation
        addressInformation2
    */

   var formatstr = 'N3*{addressInformation}';
   if(!!options.addressInformation2 && !(/^\s*$/.test(options.addressInformation2))){
       formatstr += '*{addressInformation2}';
   }
   formatstr += '~';
    return formatstr.format(options);
};
