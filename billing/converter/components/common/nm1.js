var logger = require('winston'),
    _ = require('lodash');

require('../../../../util')();

module.exports = function(options) {
    options = options || {};
    /*options:
        entityIdentifierCode 
        entityTypeQualifier
        nameLastOrOrganizationName
        nameFirst
        nameMiddle
        --optional name suffix
        identificationCodeQualifier
        identificationCode
    */

  _.defaults(options, {
        nameFirst: null,
        nameMiddle: null,
        nameSuffix: null,
        nm106: null
    });  

    var formatstr = 'NM1*{entityIdentifierCode}' +
        '*{entityTypeQualifier}' +
        '*{nameLastOrOrganizationName}';
    //always include namefirst/last/suffix
    formatstr += '*{nameFirst}' +
            '*{nameMiddle}' +
            '*{nameSuffix}' +
            '*{nm106}';

    formatstr += '*{identificationCodeQualifier}' +
        '*{identificationCode}~';

    return formatstr.format(options);

};
