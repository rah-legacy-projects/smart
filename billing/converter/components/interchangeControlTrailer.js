var logger = require('winston'),
    _ = require('lodash');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        numberOfIncludedFunctionalGroups: null,
        interchangeControlNumber: null,
    });

    return ('IEA*{numberOfIncludedFunctionalGroups}' +
            '*{interchangeControlNumber}~')
        .format(options);
};
