var logger = require('winston'),
    _ = require('lodash');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        numberOfTransactionSetsIncluded: '[count of transaction set]',
        groupControlNumber: '[must match group number in header gs]',
    });

    return ('GE*{numberOfTransactionSetsIncluded}' +
            '*{groupControlNumber}~')
        .format(options);
};
