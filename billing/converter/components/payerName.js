var logger = require('winston'),
    _ = require('lodash'),
    commonBilling = require('./common');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        entityIdentifierCode: 'PR',
        entityTypeQualifier: '2',
        nameLastOrOrganizationName: '[mco name]',
        identificationCodeQualifier: 'PI',
        identificationCode: '[mco id #]',
    });
    return commonBilling.nm1(options);
};
