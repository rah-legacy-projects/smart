var logger = require('winston'),
    _ = require('lodash');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        codeCategory: 'ZZ',
        certificationConditionCodeAppliesIndicator: 'N',
        conditionIndicator1: 'NU',
        conditionIndicator2: null,
        conditionIndicator3: null,
    });

    var formatstr = 'CRC*{codeCategory}' +
        '*{certificationConditionCodeAppliesIndicator}' +
        '*{conditionIndicator1}';
    if (!!options.conditionIndicator2) {
        formatstr += '*{conditionIndicator2}';
    }
    if (!!options.conditionIndicator3) {
        formatstr += '*{conditionIndicator3}';
    }
    formatstr += '~';
    return formatstr
        .format(options);
};
