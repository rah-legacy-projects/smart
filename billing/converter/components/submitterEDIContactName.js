var logger = require('winston'),
    _ = require('lodash');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        contactFunctionCode: 'IC',
        name: 'BEACON ANALYTICS',
        communicationNumberQualifier1: 'TE',
        communicationNumber1: '8555101101',
        communicationNumberQualifier2: null,
        communicationNumber2: null,
        communicationNumberQualifier2: null,
        communicationNumber2: null,
        communicationNumberQualifier3: null,
        communicationNumber3: null,
    });

    var formatstr = 'PER*{contactFunctionCode}' +
            '*{name}' +
            '*{communicationNumberQualifier1}' +
            '*{communicationNumber1}';

    if(!!options.communicationNumberQualifier2){
            formatstr += '*{communicationNumberQualifier2}' +
            '*{communicationNumber2}' +
            '*{communicationNumberQualifier2}';
    }
    if(!!options.communicationNumberQualifier3){
            formatstr += '*{communicationNumber2}' +
            '*{communicationNumberQualifier3}' +
            '*{communicationNumber3}';
    }
    formatstr += '~';

    return formatstr.format(options);
};
