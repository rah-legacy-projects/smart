var logger = require('winston'),
    _ = require('lodash');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        referenceIdentificationQualifier: '6R',
        referenceIdentification: '[line number matching service line number]',
    });

    return ('REF*{referenceIdentificationQualifier}' +
            '*{referenceIdentification}~')
        .format(options);
};
