var logger = require('winston'),
    _ = require('lodash');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        numberOfIncludedSegments: '[number of segments ~]',
        trasactionSetControlNumber: '[has to match transaction set header]',
    });

    return ('SE*{numberOfIncludedSegments}' +
            '*{trasactionSetControlNumber}~')
        .format(options);
};
