var logger = require('winston'),
    _ = require('lodash');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        dateTimeQualifier: '472',
        dateTimePeriodFormatQualifier: 'RD8',
        dateTimePeriod: '[start-end]',
    });

    return ('DTP*{dateTimeQualifier}' +
            '*{dateTimePeriodFormatQualifier}' +
            '*{dateTimePeriod}~')
        .format(options);
};
