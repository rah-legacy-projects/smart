var logger = require('winston'),
    _ = require('lodash');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        payerResponsibilitySequenceNumberCode: 'P',
        individualRelationshipCode: '18',
        referenceIdentification: null,
        name: null,
        insuranceTypeCode: null,
        claimFilingIndicatorCode: 'MC',
        sbr06:null,
        sbr07: null,
        sbr08: null
    });

    return ('SBR*{payerResponsibilitySequenceNumberCode}' +
            '*{individualRelationshipCode}' +
            '*{referenceIdentification}' +
            '*{name}' +
            '*{insuranceTypeCode}' +
            '*{sbr06}'+
            '*{sbr07}'+
            '*{sbr08}'+
            '*{claimFilingIndicatorCode}~')
        .format(options);
};
