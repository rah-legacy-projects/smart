var logger = require('winston'),
    _ = require('lodash');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        serviceIdQualifier: 'HC',
        procedureCode: null,
        procedureModifier1: null,
        procedureModifier2: null,
        procedureModifier3: null,
        procedureModifier4: null,
        description: null,
        monetaryAmount: null,
        unitMeasurementCode: 'UN',
        quantity: null,
        facilityCodeValue: null,
        diagnosisCodePointer1: null,
        diagnosisCodePointer2: null,
        diagnosisCodePointer3: null,
        diagnosisCodePointer4: null,
        emergencyIndicator: null,
        epsdtIndicator: null,
        familyPlanningIndicator: null,
        copayStatusCode: null,
        sv1106: null
    });

    var formatstr = 'SV1*{serviceIdQualifier}' +
        ':{procedureCode}';
    if(!!options.procedureModifier1 && !(/^\s*$/.test(options.procedureModifier2))){
        formatstr += ':{procedureModifier1}';
    }
    if (!!options.procedureModifier2 && !(/^\s*$/.test(options.procedureModifier2))) {
        formatstr += ':{procedureModifier2}';
    }
    if (!!options.procedureModifier3 && !(/^\s*$/.test(options.procedureModifier3))) {
        formatstr += ':{procedureModifier3}';
    }
    if (!!options.procedureModifier4 && !(/^\s*$/.test(options.procedureModifier4))) {
        formatstr += ':{procedureModifier4}';
    }
    if (!!options.description && !(/^\s*$/.test(options.description))) {
        formatstr += '*{description}';
    }
    formatstr += '*{monetaryAmount}' +
        '*{unitMeasurementCode}' +
        '*{quantity}';

    //always include facility code value
        formatstr += '*{facilityCodeValue}';
        formatstr += '*{sv1106}';

    if (!!options.diagnosisCodePointer1 || !!options.diagnosisCodePointer2 || !!options.diagnosisCodePointer3 || !!options.diagnosisCodePointer4) {
        formatstr += '*';
        if (!!options.diagnosisCodePointer1) {
            formatstr += '{diagnosisCodePointer1}';
        }
        if (!!options.diagnosisCodePointer2) {
            formatstr += ':{diagnosisCodePointer2}';
        }
        if (!!options.diagnosisCodePointer3) {
            formatstr += ':{diagnosisCodePointer3}';
        }
        if (!!options.diagnosisCodePointer4) {
            formatstr += ':{diagnosisCodePointer4}';
        }
    }


    if (!!options.emergencyIndicator && !(/^\s*$/.test(options.emergencyIndicator))) {
        formatstr+= '*{emergencyIndicator}';
    }

    if (!!options.epsdtIndicator && !(/^\s*$/.test(options.epsdtIndicator))) {
        formatstr += '*{epsdtIndicator}';
    }

    if (!!options.familyPlanningIndicator && !(/^\s*$/.test(options.familyPlanningIndicator))) {
        formatstr+= '*{familyPlanningIndicator}';
    }

    formatstr+= '*{copayStatusCode}~';
    
    return formatstr.format(options);
};
