var logger = require('winston'),
    moment = require('moment'),
    _ = require('lodash');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        hierarchicalStructureCode: '0019',
        transactionSetPurposeCode: '00',
        referenceIdentification: '1',
        date: moment()
            .format('YYYYMMDD'),
        time: moment()
            .format('HHMM'),
        transactionTypeCode: 'CH',
    });

    return ('BHT*{hierarchicalStructureCode}' +
            '*{transactionSetPurposeCode}' +
            '*{referenceIdentification}' +
            '*{date}' +
            '*{time}' +
            '*{transactionTypeCode}~')
        .format(options);
};
