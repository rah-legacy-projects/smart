var logger = require('winston'),
    _ = require('lodash'),
    moment = require('moment');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        authorizationQualifier: '00',
        authorizationInformation: null,
        securityQualifier: '00',
        securityInformation: null,
        interchangeIdQualfier: 'ZZ',
        interchangeRecieverId: 'CAPARIO',
        interchangeSenderId: 'BEACON ANLTCS',
        interchangeDate: moment()
            .format('YYMMDD'),
        interchangeTime: moment()
            .format('HHMM'),
        repititionSeparator: 'U',
        interchangeControlNumber: '00501',
        interchangeNumber: '000000001',
        acknowledgementRequested: '0',
        interchangeUsageIndicator: 'P',
        componentSeparator: ':',
    });

    return ('ISA*{authorizationQualifier}' +
            '*{authorizationInformation}' +
            '*{securityQualifier}' +
            '*{securityInformation}' +
            '*{interchangeIdQualfier}' +
            '*{interchangeSenderId}' +
            '*{interchangeIdQualfier}' +
            '*{interchangeRecieverId}' +
            '*{interchangeDate}' +
            '*{interchangeTime}' +
            '*{repititionSeparator}' +
            '*{interchangeControlNumber}' +
            '*{interchangeNumber}' +
            '*{acknowledgementRequested}' +
            '*{interchangeUsageIndicator}' +
            '*{componentSeparator}~')
        .format(options);
};
