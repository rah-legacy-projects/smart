var logger = require('winston'),
    _ = require('lodash');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        assignedNumber: '[index]',
    });

    return ('LX*{assignedNumber}~')
        .format(options);
};
