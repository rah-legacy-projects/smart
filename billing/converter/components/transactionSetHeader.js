var logger = require('winston'),
    util = require('util'),
    _ = require('lodash');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        transactionSetIdentifierCode: '837',
        transactionSetControlNumber: '[must match footer]',
        implentationConventionReference: '005010X222A1',
    });

    return ('ST*{transactionSetIdentifierCode}' +
            '*{transactionSetControlNumber}' +
            '*{implentationConventionReference}~')
        .format(options);
};
