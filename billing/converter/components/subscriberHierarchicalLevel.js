var logger = require('winston'),
    _ = require('lodash');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        hierarchicalIdNumber: '[subscriber hl record number]',
        hierarchicalParentIdNumber: '1',
        hierarchicalLevelCode: '22',
        hierarchicalChildCode: '0',
    });

    return ('HL*{hierarchicalIdNumber}' +
            '*{hierarchicalParentIdNumber}' +
            '*{hierarchicalLevelCode}' +
            '*{hierarchicalChildCode}~')
        .format(options);
};
