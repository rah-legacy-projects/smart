var logger = require('winston'),
    _ = require('lodash'),
    commonBilling = require('./common');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        entityIdentifierCode: '87',
        entityTypeQualifier: '2',
    });
    return ('NM1*{entityIdentifierCode}*{entityTypeQualifier}~').format(options);
};
