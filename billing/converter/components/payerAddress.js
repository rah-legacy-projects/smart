var logger = require('winston'),
    _ = require('lodash'),
    commonBilling = require('./common');

require('../../../util')();

module.exports = function(options) {
    options = options || {};
    _.defaults(options, {
        addressInformation: null,
        addressInformation2: null,
    });
    return commonBilling.n3(options);
};
