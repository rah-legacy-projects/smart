var moment = require('moment'),
    logger = require('winston');
module.exports = function() {
    if (!String.prototype.format) {
        String.prototype.format = function() {
            var args = arguments;
            var used = [];

            if (Object.prototype.toString.call(arguments[0])
                .match(/^\[object (.*)\]$/)[1] === "Object") {
                var formatted = this.replace(/{(\w+)}/g, function(match, memberName) {
                    used.push(memberName);
                    if (typeof args[0][memberName] == 'undefined') {
                        //logger.warn(memberName + ' not found in the parameter list');
                    }
                    return typeof args[0][memberName] != 'undefined' ? (args[0][memberName] || '') : match;
                });

                for (var member in args[0]) {
                    if (used.indexOf(member) === -1) {
                        //logger.warn(member + ' not used by format string');
                    }
                }
                return formatted;
            } else {
                var formatted = this.replace(/{(\d+)}/g, function(match, number) {
                    used.push(parseInt(number, 10));
                    if (typeof args[number] == 'undefined') {
                        //logger.warn(number + ' not found in the parameter list');
                    }
                    return typeof args[number] != 'undefined' ? args[number] : match;
                });
                for (var i = 0; i < args.length; i++) {
                    if (used.indexOf(i) === -1) {
                        //logger.warn(i + ' not used by format string');
                    }
                }
                return formatted;
            }
        };
    }

    //hack: this is a shim until moment #1595 gets roundTo
    //hacK: https://github.com/moment/moment/pull/1595
    moment.fn.roundNext15Min = function() {
        var q = Math.floor(this.minutes() / 15);
        if (this.minutes() % 15 != 0)
            q++;
        if (q == 4) {
            this.add('hours', 1);
            q = 0;
        }
        this.minutes(q * 15);
        this.seconds(0);
        return this;
    }

};
