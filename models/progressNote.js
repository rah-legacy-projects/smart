var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    audit = require('mongoose-audit'),
    plugins = require('aio-mongoose-plugins'),
    ObjectId = Schema.Types.ObjectId;

var progressNoteSchema = new Schema({
    note: {
        type: String,
        required: true
    },
    specialty: {
        type:String,
        required:true
    },
    notedBy: {
        type: ObjectId,
        required: true,
        ref: 'User'
    },
    addedDate:{
        type:Date,
        required:true
    }
}, {});

progressNoteSchema.plugin(audit.auditPlugin, 'ProgressNote', {});
progressNoteSchema.plugin(plugins.CreatedAndModified);
progressNoteSchema.plugin(plugins.ActiveAndDeleted);
var progressNoteModel = mongoose.model('ProgressNote', progressNoteSchema);

var student = module.exports = exports = progressNoteModel;
