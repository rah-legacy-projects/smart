var mongoose = require('mongoose'),
    audit = require('mongoose-audit'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    logger = require('winston'),
    dataUtil = require('./dataUtil'),
    plugins = require('aio-mongoose-plugins');

var schoolSchema = new Schema({
    schoolCode: {
        type: String,
        required: true
    },
    schoolNumber: {
        type: String,
        required: true
    },
    district: {
        type: ObjectId,
        ref: 'District'
    },
    rangedData: [{
        type: ObjectId,
        ref: 'SchoolRange'
    }]
}, {});

schoolSchema.plugin(audit.auditPlugin, 'School', {});
schoolSchema.plugin(plugins.AclSubject, {
    key: function() {
        return 'school:' + this._id;
    }
});
schoolSchema.plugin(plugins.CreatedAndModified);

schoolSchema.statics.getCurrentById = function(schoolId, cb) {
    this.findOne({
        _id: schoolId
    })
        .populate('rangedData')
        .lean()
        .exec(function(err, school) {
            dataUtil.getCurrent(school, 'rangedData', function(err, flatSchool) {
                //hack: err is set if the obj is not current
                //hack: shouldp probably have better error/warn handling

                if (!!err) {
                    logger.warn('school code %s is not current.', school.schoolCode);
                }
                cb(null, flatSchool);
            });
        });
};

schoolSchema.statics.getByDateById = function(date, schoolId, cb) {
    this.findOne({
        _id: schoolId
    })
        .populate('rangedData')
        .lean()
        .exec(function(err, school) {
            dataUtil.getByDateRange(school, 'rangedData', date, date, function(err, flatSchool) {
                //hack: err is set if the obj is not current
                //hack: shouldp probably have better error/warn handling

                if (!!err) {
                    logger.warn('school code %s is not current.', school.schoolCode);
                }
                cb(null, flatSchool);
            });
        });
};

schoolSchema.methods.getCurrentSync = function(){
    var self= this;
    if(!self.rangedData){
        return null;
    }

    return dataUtil.getCurrentSync({
        schoolCode: self.schoolCode,
        schoolNumber: self.schoolNumber,
        district: self.district,
        active: self.active,
        rangedData: _.map(self.rangedData, function(r){
            return {
                name: r.name,
                npiNumber: r.npiNumber,
                startDate: r.startDate,
                endDate: r.endDate,
                rangeActive: r.rangeActive
            };
        })
    }, 'rangedData')
};
var schoolModel = mongoose.model('School', schoolSchema);
var school = module.exports = exports = schoolModel;
