var mongoose = require('mongoose'),
    plugins = require('aio-mongoose-plugins'),
    audit = require('mongoose-audit'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;

var mcoSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    district: {
        type: ObjectId,
        ref: 'District'
    },
    rangedData: [{type:ObjectId, ref: 'MCORange'}]
}, {});

mcoSchema.plugin(audit.auditPlugin, 'MCO', {});

mcoSchema.plugin(plugins.CreatedAndModified);
var mcoModel = mongoose.model('MCO', mcoSchema);
var mco = module.exports = exports = mcoModel;
