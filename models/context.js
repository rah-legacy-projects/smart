module.exports = {
    Area: require('./area'),
    Attendee: require('./attendee'),
    DiagnosticCode: require('./diagnosticCode'),
    District: require('./district'),
    ForgotPassword: require('./forgotPassword'),
    GDS: require('./gds'),
    GDSTennCare: require('./gdsTennCare'),
    MCO: require('./mco'),
    MCOQualification: require('./mcoQualification'),
    ParentalConsent: require('./parentalConsent'),
    Permissions: require('./permissions'),
    Prescription: require('./prescription'),
    ProcedureCode: require('./procedureCode'),
    ProcedureCodeValue: require('./procedureCodeValue'),
    ProgressNote: require('./progressNote'),
    RemittanceError: require('./remittanceError'),
    Schedule: require('./schedule'),
    ScheduleSeries: require('./scheduleSeries'),
    School: require('./school'),
    Service: require('./service'),
    Session: require('./session'),
    Student: require('./student'),
    TherapistQualification: require('./therapistQualification'),
    TransportLog: require('./transportLog'),
    User: require('./user'),

    ranged:{
        DiagnosticCode: require('./ranged/diagnosticCode'),
        District: require('./ranged/district'),
        GDS: require('./ranged/gds'),
        GDSTennCare: require('./ranged/gdsTennCare'),
        MCO: require('./ranged/mco'),
        ProcedureCode: require('./ranged/procedureCode'),
        ProcedureCodeValue: require('./ranged/procedureCodeValue'),
        School: require('./ranged/school'),
        Service: require('./ranged/service'),
        Student: require('./ranged/student'),
        StudentMCOInformation: require('./ranged/studentMCOInformation')
    },

    rollup:{
        Billed: require('./rollup/billed'),
        Rejected: require('./rollup/rejected')
    },

    ObjectId: require('mongoose').Types.ObjectId
};
