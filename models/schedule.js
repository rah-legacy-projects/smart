var mongoose = require('mongoose'),
    plugins = require('aio-mongoose-plugins'),
    audit = require('mongoose-audit'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;

var scheduleSchema = new Schema({
    dateTime:{type:Date, required:true},
    location:{type:String, required:false},
    duration:{type:Number, required:true},
    scheduleSeries: {type:ObjectId, ref:'ScheduleSeries'},
    services: [{type:ObjectId, ref: 'Service'}],
    organizer: {type:ObjectId, ref: 'User'},
    session:{type:ObjectId, ref:'Session'}
}, {});

scheduleSchema.plugin(audit.auditPlugin, 'Schedule', {});
scheduleSchema.plugin(plugins.ActiveAndDeleted);
scheduleSchema.plugin(plugins.CreatedAndModified);
var scheduleModel = mongoose.model('Schedule', scheduleSchema);
var schedule = module.exports = exports = scheduleModel;
