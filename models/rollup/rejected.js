var mongoose = require('mongoose'),
    plugins = require('aio-mongoose-plugins'),
    audit = require('mongoose-audit'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;

var rejectedSchema = new Schema({
    
    billingRun:{
        type:ObjectId,
        required:true
        //not a reference to anything, logically groups touched billed rollups together
    },
    
    //{{{ track IDs used for billing
    attendee: {
        type: ObjectId,
        ref: 'Attendee'
    },
    session: {
        type: ObjectId,
        ref: 'Session'
    },
    procedureCode: {
        type: ObjectId,
        ref: 'ProcedureCode'
    },
    procedureCodeRange: {
        type: ObjectId,
        ref: 'ProcedureCodeRange'
    },
    procedureCodeValue: {
        type: ObjectId,
        ref: 'ProcedureCodeValue'
    },
    procedureCodeValueRange: {
        type: ObjectId,
        ref: 'ProcedureCodeValueRange'
    },
    diagnosticCode: {
        type: ObjectId,
        ref: 'DiagnosticCode'
    },
    diagnosticCodeRange: {
        type: ObjectId,
        ref: 'DiagnosticCodeRange'
    },
    school: {
        type: ObjectId,
        ref: 'School'
    },
    schoolRange: {
        type: ObjectId,
        ref: 'SchoolRange'
    },
    district: {
        type: ObjectId,
        ref: 'District'
    },
    districtRange: {
        type: ObjectId,
        ref: 'DistrictRange'
    },
    schedule: {
        type: ObjectId,
        ref: 'Schedule'
    },
    studentMCO: {
        type: ObjectId,
        ref: 'MCO'
    },
    studentMCORange: {
        type: ObjectId,
        ref: 'MCORange'
    },
    therapist: {
        type: ObjectId,
        ref: 'User'
    },
     //}}}

    //{{{ flat-derived data used for reporting
    studentName: {
        type: String,
        required: true
    },
    dateTime: {
        type: Date,
        required: true
    },
    therapistName: {
        type: String,
        required: true
    },
    diagnosticCodeDescription: {
        type: String,
        required: false
    },
    possibleIEPUnits: {
        type: Number,
        required: false
    },
    rejectedIEPUnits: {
        type: Number,
        required: false
    },
    procedureCodeValue: {
        type: Number,
        required: false
    },
    schoolCode:{
        type:String,
        required:false
    },
    schoolName: {
        type: String,
        required: false
    },
    districtCode:{
        type:String,
        required:false
    },
    districtName: {
        type: String,
        required: false
    },
    studentMCOName: {
        type: String,
        required: false
    },
    therapy: {
        type: String,
        required:false
    },
    studentId:{
        type:String,
        required:true
    },
    procedureCodeCode:{
        type:String,
        required:true
    },

    //}}}

    //{{{ failure tracking
    absent: {
        type: Boolean,
        required: true,
        default: false
    },
    billed: {
        type: Boolean,
        required: true,
        default: false
    },
    diagnosticCode: {
        type: Boolean,
        required: true,
        default: false
    },
    finalized: {
        type: Boolean,
        required: true,
        default: false
    },
    gds: {
        type: Boolean,
        required: true,
        default: false
    },
    gdsSSN: {
        type: Boolean,
        required: true,
        default: false
    },
    iepApprovedSessions: {
        type: Boolean,
        required: true,
        default: false
    },
    index: {
        type: Boolean,
        required: true,
        default: false
    },
    mcoApprovedSessions: {
        type: Boolean,
        required: true,
        default: false
    },
    mcoInfo: {
        type: Boolean,
        required: true,
        default: false
    },
    medicaidIneligible: {
        type: Boolean,
        required: true,
        default: false
    },
    notApproved: {
        type: Boolean,
        required: true,
        default: false
    },
    parentalConsent: {
        type: Boolean,
        required: true,
        default: false
    },
    prescription: {
        type: Boolean,
        required: true,
        default: false
    },
    prescriptionMatch: {
        type: Boolean,
        required: true,
        default: false
    },
    procedureCode: {
        type: Boolean,
        required: true,
        default: false
    },
    procedureCodeValue: {
        type: Boolean,
        required: true,
        default: false
    },
    session: {
        type: Boolean,
        required: true,
        default: false
    },
    studentMCOAddress: {
        type: Boolean,
        required: true,
        default: false
    },
    studentMCOHealthPlanId: {
        type: Boolean,
        required: true,
        default: false
    },
    studentNotMCOApproved: {
        type: Boolean,
        required: true,
        default: false
    },
    therapistMCOApproval: {
        type: Boolean,
        required: true,
        default: false
    },
    therapistNPI: {
        type: Boolean,
        required: true,
        default: false
    },
    therapistSSN: {
        type: Boolean,
        required: true,
        default: false
    },
    toBillDenied: {
        type: Boolean,
        required: true,
        default: false
    },
    toBill: {
        type: Boolean,
        required: true,
        default: false
    },
    toTreatDenied: {
        type: Boolean,
        required: true,
        default: false
    },
    toTreat: {
        type: Boolean,
        required: true,
        default: false
    }
    //}}}
}, {});

rejectedSchema.plugin(audit.auditPlugin, 'Rejected', {});

rejectedSchema.plugin(plugins.CreatedAndModified);
var rejectedModel = mongoose.model('Rejected', rejectedSchema);
var rejected = module.exports = exports = rejectedModel;
