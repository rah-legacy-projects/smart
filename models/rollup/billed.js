var mongoose = require('mongoose'),
    plugins = require('aio-mongoose-plugins'),
    audit = require('mongoose-audit'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;

var billedSchema = new Schema({

    billingRun:{
        type:ObjectId,
        required:true
        //not a reference to anything, logically groups touched billed rollups together
    },

    //{{{ track IDs used for billing
    billingGroup:{
        type: String,
        required:true
        //not a reference to anything.  This is the echo identifier sent to Capario.
    },
    attendee: {
        type: ObjectId,
        ref: 'Attendee',
        required:true
    },
    session: {
        type: ObjectId,
        ref: 'Session'
    },
    procedureCode: {
        type: ObjectId,
        ref: 'ProcedureCode'
    },
    procedureCodeRange: {
        type: ObjectId,
        ref: 'ProcedureCodeRange'
    },
    procedureCodeValue:{
        type:ObjectId,
        ref:'ProcedureCodeValue'
    },
    procedureCodeValueRange:{
        type:ObjectId,
        ref:'ProcedureCodeValueRange'
    },
    diagnosticCode: {
        type: ObjectId,
        ref: 'DiagnosticCode'
    },
    diagnosticCodeRange: {
        type: ObjectId,
        ref: 'DiagnosticCodeRange'
    },
    school: {
        type: ObjectId,
        ref: 'School'
    },
    schoolRange: {
        type: ObjectId,
        ref: 'SchoolRange'
    },
    district: {
        type: ObjectId,
        ref: 'District'
    },
    districtRange: {
        type: ObjectId,
        ref: 'DistrictRange'
    },
    schedule: {
        type: ObjectId,
        ref: 'Schedule'
    },
    studentMCO: {
        type: ObjectId,
        ref: 'MCO'
    },
    studentMCORange: {
        type: ObjectId,
        ref: 'MCORange'
    },
    therapist: {
        type:ObjectId,
        ref: 'User'
    },
    //}}}

    //{{{ flat-derived data used for reporting
    studentName: {
        type: String,
        required: true
    },
    dateTime: {
        type: Date,
        required: true
    },
    therapistName: {
        type: String,
        required: true
    },
    diagnosticCodeDescription: {
        type: String,
        required: true
    },
    possibleIEPUnits: {
        type: Number,
        required: true
    },
    billedIEPUnits: {
        type:Number,
        required:true
    },
    procedureCodeValueValue: {
        type: Number,
        required: true
    },
    schoolCode: {
        type:String,
        required:false
    },
    schoolName: {
        type: String,
        required: true
    },
    districtCode: {
        type:String,
        required:true
    },
    districtName: {
        type: String,
        required: true
    },
    studentMCOName: {
        type: String,
        required: true
    },
    therapy: {
        type: String,
        required:true
    },
    studentId:{
        type:String,
        required:true
    },
    procedureCodeCode:{
        type:String,
        required:true
    },
    //}}}

//{{{ remittance data
    amountBilled:{
        type:Number,
        required:false
    },
    amountAllowed: {
        type:Number,
        required:false
    },
    deduction: {
        type:Number,
        required:false
    },
    amountPaid:{
        type:Number,
        required:false,
    },
    remittanceDate:{
        type: Date,
        required:false
    },
    remittanceType:{
        type:String,
        required:false
    },
    remittanceErrors:[{
        type:ObjectId,
        ref: 'RemittanceError'
    }]
    //}}}

}, {});

billedSchema.plugin(audit.auditPlugin, 'Billed', {});

billedSchema.plugin(plugins.CreatedAndModified);
var billedModel = mongoose.model('Billed', billedSchema);
var billed = module.exports = exports = billedModel;
