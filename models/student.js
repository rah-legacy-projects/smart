var mongoose = require('mongoose'),
    plugins = require('aio-mongoose-plugins'),
    dataUtil = require('./dataUtil'),
    Schema = mongoose.Schema,
    audit = require('mongoose-audit'),
    async = require('async'),
    ObjectId = Schema.Types.ObjectId,
    school = require('./school'),
    schoolRange = require('./ranged/school'),
    logger = require('winston');

var studentSchema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    middleName: {
        type: String,
        required: false
    },
    lastName: {
        type: String,
        required: true
    },
    countyStudentId: {
        type: String,
        required: false
    },
    gender: {
        type: String,
        required: false
    },
    dateOfBirth: {
        type: Date,
        required: true
    },
    race: {
        type: String,
        required: false
    },
    ethnicity: {
        type: String,
        required: false
    },
    ssn: {
        type: String,
        required: false
    },

    //items from student file not covered in ERD

    stateId: {
        type: String,
        required: false
    },
    studentId: {
        type: String,
        required: true
    },


    parentalConsents: [{
        type: ObjectId,
        ref: 'ParentalConsent'
    }],
    progressNotes: [{
        type: ObjectId,
        ref: 'ProgressNote'
    }],
    prescriptions: [{
        type: ObjectId,
        ref: 'Prescription'
    }],
    mcoInformation: [{
        type: ObjectId,
        ref: 'StudentMCOInformationRange'
    }],

    rangedData: [{
        type: ObjectId,
        ref: 'StudentRange'
    }]

}, {});

studentSchema.virtual('fullName')
    .get(function() {
        return this.firstName + ' ' + this.lastName;
    });

studentSchema.plugin(audit.auditPlugin, 'Student', {});
studentSchema.plugin(plugins.CreatedAndModified);

studentSchema.statics.getCurrentById = function(studentId, cb) {
    this.findOne({
        _id: studentId
    })
        .populate('rangedData')
        .lean()
        .exec(function(err, student) {
            async.waterfall([

                    function(cb) {
                        var flatStudent = dataUtil.getCurrentSync(student, 'rangedData');
                        school.getCurrentById(flatStudent.school, function(err, flatSchool) {
                            cb(null, flatStudent, flatSchool);
                        });
                    },
                    function(flatStudent, flatSchool, cb) {
                        flatStudent.school = flatSchool;
                        cb(null, flatStudent);
                    }
                ],
                function(err, flatStudent) {
                    cb(err, flatStudent);
                });
        });
};

studentSchema.statics.getByDateById = function(date, studentId, cb) {
    this.findOne({
        _id: studentId
    })
        .populate('rangedData')
        .lean()
        .exec(function(err, student) {
            async.waterfall([

                    function(cb) {
                        var flatStudent = dataUtil.getByDateRangeSync(student, 'rangedData', date, date);
                        if (!!flatStudent) {
                            school.getByDateById(date, flatStudent.school, function(err, flatSchool) {
                                cb(null, flatStudent, flatSchool);
                            });
                        } else {
                            cb(null, null, null);
                        }
                    },
                    function(flatStudent, flatSchool, cb) {
                        if (!!flatStudent) {
                            flatStudent.school = flatSchool;
                        }
                        cb(null, flatStudent);
                    }
                ],
                function(err, flatStudent) {
                    cb(err, flatStudent);
                });
        });
};


studentSchema.statics.getCurrentByStudentId = function(studentId, cb) {
    this.findOne({
        studentId: new RegExp(studentId, 'i')
    })
        .populate('rangedData')
        .lean()
        .exec(function(err, student) {
            if (!student) {
                logger.silly('could not find student ' + studentId);
                cb('could not find student ' + studentId);
                return;
            }
            async.waterfall([

                    function(cb) {
                        //logger.silly('student: ' + util.inspect(student));
                        var flatStudent = dataUtil.getCurrentSync(student, 'rangedData');
                        school.getCurrentById(flatStudent.school, function(err, flatSchool) {
                            cb(null, flatStudent, flatSchool);
                        });
                    },
                    function(flatStudent, flatSchool, cb) {
                        flatStudent.school = flatSchool;
                        cb(null, flatStudent);
                    }
                ],
                function(err, flatStudent) {
                    cb(err, flatStudent);
                });
        });
};

var studentModel = mongoose.model('Student', studentSchema);

var student = module.exports = exports = studentModel;
