var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    logger = require('winston'),
    plugins = require('aio-mongoose-plugins');
    audit = require('mongoose-audit'),
    util = require('util');

var therapistQualificationSchema = new Schema({
        npiNumber: {
            type: String,
            required: false
        },
        startDate: {
            type: Date,
            required: true
        },
        endDate: {
            type: Date,
            required: true
        },
        providerType: {
            type: String,
            required: false
        },
        licensed: {
            type: Boolean,
            required: true
        }, 
        deleted:{
            type:Boolean,
            required:true,
            default:false
        }

    //tbd?
}, {});

therapistQualificationSchema.plugin(audit.auditPlugin, 'TherapistQualification', {});
therapistQualificationSchema.plugin(plugins.CreatedAndModified);

var tqm = mongoose.model('TherapistQualification', therapistQualificationSchema);


var tq = module.exports = exports = tqm;
