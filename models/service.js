var mongoose = require('mongoose'),
    audit = require('mongoose-audit'),
    plugins = require('aio-mongoose-plugins'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId

var serviceSchema = new Schema({
    student: {
        type: ObjectId,
        required: true,
        ref: 'Student'
    },
    school: {
        type: String,
        required: true,
    },
    caseManager: {
        type: ObjectId,
        required: true,
        ref: 'User'
    },
    service: {
        type: String,
        required: true
    },
    therapist: {
        type: ObjectId,
        required: true,
        ref: 'User'
    },
    rangedData: [{
        type: ObjectId,
        ref: 'ServiceRange'
    }]
}, {});

serviceSchema.plugin(audit.auditPlugin, 'Service', {});

serviceSchema.plugin(plugins.CreatedAndModified);
var serviceModel = mongoose.model('Service', serviceSchema);
var service = module.exports = exports = serviceModel;
