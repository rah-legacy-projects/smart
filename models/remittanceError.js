var mongoose = require('mongoose'),
    audit = require('mongoose-audit'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    moment = require('moment'),
    dataUtil = require('./dataUtil'),
    plugins = require('aio-mongoose-plugins');

var remittanceErrorSchema = new Schema({
    code: {
        type: String,
        required: true
    },
    description: {
        type:String,
        required:true
    }
}, {});

remittanceErrorSchema.plugin(audit.auditPlugin, 'RemittanceError', {});
remittanceErrorSchema.plugin(plugins.CreatedAndModified);

var remittanceErrorModel = mongoose.model('RemittanceError', remittanceErrorSchema);
var remittanceError = module.exports = exports = remittanceErrorModel;
