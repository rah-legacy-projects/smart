var mongoose = require('mongoose'),
    audit = require('mongoose-audit'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins');

var gdsSchema = new Schema({
    student: {
        type: ObjectId,
        ref: 'Student',
        required:true
    },
    rangedData: [{
        type: ObjectId,
        ref: 'GDSRange'
    }]
});

gdsSchema.plugin(audit.auditPlugin, 'GDS', {});
gdsSchema.plugin(plugins.CreatedAndModified);
var gdsModel = mongoose.model('GDS', gdsSchema);
var gds = module.exports = exports = gdsModel;
