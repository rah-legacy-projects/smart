var mongoose = require('mongoose'),
    audit = require('mongoose-audit'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins');

var gdsTennCareSchema = new Schema({
    student: {
        type: ObjectId,
        ref: 'Student'
    },
    rangedData: [{
        type: ObjectId,
        ref: 'GDSTennCareRange'
    }]
});

gdsTennCareSchema.plugin(audit.auditPlugin, 'GDSTennCare', {});
gdsTennCareSchema.plugin(plugins.CreatedAndModified);
var gdsTennCareModel = mongoose.model('GDSTennCare', gdsTennCareSchema);
var gdsTennCare = module.exports = exports = gdsTennCareModel;
