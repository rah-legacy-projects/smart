var mongoose = require('mongoose'),
    plugins = require('aio-mongoose-plugins'),
    audit = require('mongoose-audit'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;

var procedureCodeValueSchema = new Schema({
    mco: {
        type: ObjectId,
        ref: 'MCO'
    },
    procedureCode: {
        type: ObjectId,
        ref: 'ProcedureCode'
    },
    district: {
        type: ObjectId,
        ref: 'District'
    },
    rangedData: [{
        type: ObjectId,
        ref: 'ProcedureCodeValueRange'
    }]
}, {});

procedureCodeValueSchema.plugin(audit.auditPlugin, 'procedureCodeValue', {});
procedureCodeValueSchema.plugin(plugins.CreatedAndModified);

var procedureCodeValueModel = mongoose.model('procedureCodeValue', procedureCodeValueSchema);
var procedureCodeValue = module.exports = exports = procedureCodeValueModel;
