var mongoose = require('mongoose'),
    audit = require('mongoose-audit'),
    plugins = require('aio-mongoose-plugins'),
    Schema = mongoose.Schema,
    logger = require('winston'),
    ObjectId = Schema.Types.ObjectId;

var forgotPasswordSchema = new Schema({
    user: {
        type: ObjectId,
        required: true,
        ref: 'User'
    },
    claimed: {
        type: Boolean,
        required: true,
        default: false
    },
    expiry: {
        type: Date,
        required: true,
        default: (new Date())
            .setMinutes((new Date())
                .getMinutes() + 30)
    }
}, {});

var forgotPasswordModel = mongoose.model('ForgotPassword', forgotPasswordSchema);

forgotPasswordSchema.plugin(plugins.CreatedAndModified);
var fpm = module.exports = exports = forgotPasswordModel;
