var mongoose = require('mongoose'),
    audit = require('mongoose-audit'),
    plugins = require('aio-mongoose-plugins'),
    splice = require('mongoose-date-splice'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId

var diagnosticCodeRangeSchema = new Schema({
    description: {
        type: String,
        required: true
    },
    therapy: {
        type: String,
        required: true
    }
}, {});

diagnosticCodeRangeSchema.plugin(splice.plugin, {});
diagnosticCodeRangeSchema.plugin(audit.auditPlugin, 'DiagnosticCodeRange', {});
diagnosticCodeRangeSchema.plugin(plugins.CreatedAndModified);
var diagnosticCodeRangeModel = mongoose.model('DiagnosticCodeRange', diagnosticCodeRangeSchema);
var diagnosticCodeRange = module.exports = exports = diagnosticCodeRangeModel;
