var mongoose = require('mongoose'),
    plugins = require('aio-mongoose-plugins'),
    Schema = mongoose.Schema,
    audit = require('mongoose-audit'),
    splice = require('mongoose-date-splice'),
    ObjectId = Schema.Types.ObjectId;

var studentRangeSchema = new Schema({
    school: {
        type: ObjectId,
        ref: 'School'
    },
    billingCode: {
        type: String,
        required: false
    },
    primaryDisability: {
        type: String,
        required: false
    },
    primaryDisabilityCode: {
        type: String,
        required: false
    },
    secondaryDisability: {
        type: String,
        required: false
    },
    secondaryDisabilityCode: {
        type: String,
        required: false
    },
    grade: {
        type: String,
        required: false
    },
    age: {
        type: String,
        required: false
    },
    primaryOption: {
        type: String,
        required: false
    },
    secondaryOption: {
        type: String,
        required: false
    },
    wpprDate: {
        type: Date,
        required: false
    },
    elgibilityDate: {
        type: Date,
        required: false
    },
    numOfIEPs: {
        type: Number,
        required: false
    },
    daysUntilEligible: {
        type: Number,
        required: false
    },
    daysUntilIEP: {
        type: Number,
        required: false
    },
    statusOfService: {
        type: String,
        required: false
    },
    studentType: {
        type: String,
        required: false
    },
    reasonLessFullService: {
        type: String,
        required: false
    },
    eligible: {
        type: Boolean,
        required: false
    },
    instrPercent: {
        type: Number,
        required: false
    },
    relPercent: {
        type: Number,
        required: false
    },
    totPercent: {
        type: Number,
        required: false
    },
    transportation: {
        type: String,
        required: false
    },
    status: {
        type: String,
        required: false
    },
    dateAdded: {
        type: Date,
        required: false
    },
    cenContract: {
        type: String,
        required: false
    },
    cenSeparate: {
        type: String,
        required: false
    },
    cenCorrectional: {
        type: String,
        required: false
    },
    cenMaterialsOnly: {
        type: String,
        required: false
    },
    nextSchool: {
        type: String,
        required: false
    },
    nextSchoolCode: {
        type: String,
        required: false
    },
    area: {
        type: String,
        required: false
    },
    eisEnrollmentYear: {
        type: String,
        required: false
    },
    last504PlanDate: {
        type: Date,
        required: false
    },
    teidsLastIFSPDate: {
        type: Date,
        required: false
    },
    assignedSchoolCode: {
        type: String,
        required: false
    },
    studentType: {
        type: String,
        required: false
    },
    medicaidId: {
        type: String,
        required: false
    },
    medicaidStartDate: {
        type: Date,
        required: false
    },
    medicaidEndDate: {
        type: Date,
        required: false
    },
    exitReason: {
        type: String,
        required: false
    },
    exitReasonCode: {
        type: String,
        required: false
    },
    iepEndDate: {
        type: Date,
        required: false
    },
    lastIEPDate: {
        type: Date,
        required: false
    },
    eligibilityDate: {
        type: Date,
        requried: false
    },
    caseManager: {
        type: String,
        required: false
    },
    dateExited: {
        type: Date,
        required: false
    },
    lastGradPlanDate: {
        type: Date,
        required: false
    },

}, {});

studentRangeSchema.plugin(splice.plugin, {});
studentRangeSchema.plugin(audit.auditPlugin, 'StudentRange', {});
studentRangeSchema.plugin(plugins.CreatedAndModified);

var studentRangeModel = mongoose.model('StudentRange', studentRangeSchema);

var student = module.exports = exports = studentRangeModel;
