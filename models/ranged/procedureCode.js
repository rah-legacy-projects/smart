var mongoose = require('mongoose'),
    audit = require('mongoose-audit'),
    plugins = require('aio-mongoose-plugins'),
    splice = require('mongoose-date-splice'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;

var procedureCodeRangeSchema = new Schema({
    serviceType: {
        type: String,
        required: true
    },
    modifier: {
        type: String,
        required: false
    },
    description: {
        type: String,
        required: true
    }
}, {});

procedureCodeRangeSchema.plugin(splice.plugin, {});
procedureCodeRangeSchema.plugin(audit.auditPlugin, 'ProcedureCodeRange', {});
procedureCodeRangeSchema.plugin(plugins.CreatedAndModified);
var procedureCodeRangeModel = mongoose.model('ProcedureCodeRange', procedureCodeRangeSchema);
var procedureCodeRange = module.exports = exports = procedureCodeRangeModel;
