var mongoose = require('mongoose'),
    audit = require('mongoose-audit'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    logger = require('winston'),
    splice = require('mongoose-date-splice'),
    plugins = require('aio-mongoose-plugins');

var schoolRangeSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    address1: {
        type: String,
        required: true
    },
    address2: {
        type: String,
        required: false
    },
    city: {
        type: String,
        required: true
    },
    state: {
        type: String,
        required: true
    },
    zip: {
        type: String,
        required: true
    },
    telephone: {
        type: String,
        required: false
    }
}, {});

schoolRangeSchema.plugin(splice.plugin, {});
schoolRangeSchema.plugin(audit.auditPlugin, 'SchoolRange', {});
schoolRangeSchema.plugin(plugins.CreatedAndModified);
var schoolRangeModel = mongoose.model('SchoolRange', schoolRangeSchema);
var schoolRange = module.exports = exports = schoolRangeModel;
