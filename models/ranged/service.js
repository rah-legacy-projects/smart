var mongoose = require('mongoose'),
    audit = require('mongoose-audit'),
    splice = require('mongoose-date-splice'),
    plugins = require('aio-mongoose-plugins'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId

var serviceRangeSchema = new Schema({
    serviceType: {
        type: String,
        required: true
    },
    location: {
        type: String,
        required: true
    },
    servingSchool: {
        type: String,
        required: false
    },
    consult: {
        type: String,
        required: true
    },
    esy: {
        type: String,
        required: true
    },
    ssn: {
        type: String,
        required: false
    },
    dob: {
        type: Date,
        required: false
    },
    frequency: {
        type: String,
        required: true
    },
    duration: {
        type: Number,
        required: true
    },
    numberOfSessions: {
        type: Number,
        required: true
    },
    diagnosticCode: {
        type: ObjectId,
        ref: 'DiagnosticCode'
    },
    groupTherapyOrdered:{
        type: Boolean,
        default: false
    }
}, {});

serviceRangeSchema.virtual('frequencyAndDuration')
    .get(function() {
        return this.numberOfSessions + ' sessions/' + this.frequency + ' of ' + this.duration + 'min';
    });

serviceRangeSchema.plugin(audit.auditPlugin, 'ServiceRange', {});
serviceRangeSchema.plugin(splice.plugin, {});
serviceRangeSchema.plugin(plugins.CreatedAndModified);
var serviceRangeModel = mongoose.model('ServiceRange', serviceRangeSchema);
var serviceRange = module.exports = exports = serviceRangeModel;
