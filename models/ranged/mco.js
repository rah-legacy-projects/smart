var mongoose = require('mongoose'),
    plugins = require('aio-mongoose-plugins'),
    splice = require('mongoose-date-splice'),
    audit = require('mongoose-audit'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;

var mcoRangeSchema = new Schema({
    shortName: {
        type: String,
        required: true
    },
    healthPlanId: {
        type: String,
        required: false
    },
    address1: {
        type: String,
        required: false
    },
    address2: {
        type: String,
        required: false
    },
    city: {
        type: String,
        required: false
    },
    state: {
        type: String,
        required: false
    },
    zip: {
        type: String,
        required: false
    }
}, {});

mcoRangeSchema.plugin(splice.plugin, {});
mcoRangeSchema.plugin(audit.auditPlugin, 'MCORange', {});

mcoRangeSchema.plugin(plugins.CreatedAndModified);
var mcoRangeModel = mongoose.model('MCORange', mcoRangeSchema);
var mcoRange = module.exports = exports = mcoRangeModel;
