var mongoose = require('mongoose'),
    plugins = require('aio-mongoose-plugins'),
    splice = require('mongoose-date-splice'),
    audit = require('mongoose-audit'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;

var procedureCodeValueRangeSchema = new Schema({
    serviceType: {
        type: String,
        required: true
    },
    modifier: {
        type: String,
        required: false
    },
    value: {
        type: String,
        required: true
    }
}, {});

procedureCodeValueRangeSchema.plugin(splice.plugin, {});
procedureCodeValueRangeSchema.plugin(audit.auditPlugin, 'ProcedureCodeValueRange', {});
procedureCodeValueRangeSchema.plugin(plugins.CreatedAndModified);

var procedureCodeValueRangeModel = mongoose.model('ProcedureCodeValueRange', procedureCodeValueRangeSchema);
var procedureCodeValueRange = module.exports = exports = procedureCodeValueRangeModel;
