var mongoose = require('mongoose'),
    audit = require('mongoose-audit'),
    splice = require('mongoose-date-splice'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins');

var gdsTennCareRangeSchema = new Schema({
    pageNum: {
        type: String,
        required: false
    },
    dataSource: {
        type: String,
        required: false
    },
    accountNbr_In: {
        type: String,
        required: false
    },
    recipID1_In: {
        type: String,
        required: false
    },
    sSN_Inp: {
        type: String,
        required: false
    },
    lName1_In: {
        type: String,
        required: false
    },
    fName1_In: {
        type: String,
        required: false
    },
    mI1_In: {
        type: String,
        required: false
    },
    dOB_In: {
        type: String,
        required: false
    },
    gender_In: {
        type: String,
        required: false
    },
    custDefined_In: {
        type: String,
        required: false
    },
    beg_In: {
        type: String,
        required: false
    },
    end_In: {
        type: String,
        required: false
    },
    los_In: {
        type: String,
        required: false
    },
    errorMsg: {
        type: String,
        required: false
    },
    statusFlag: {
        type: String,
        required: false
    },
    mcaidFlag: {
        type: String,
        required: false
    },
    mCRA_Flag: {
        type: String,
        required: false
    },
    mCRB_Flag: {
        type: String,
        required: false
    },
    hMO_Flag: {
        type: String,
        required: false
    },
    gDS_Ref_No: {
        type: String,
        required: false
    },
    searchCode: {
        type: String,
        required: false
    },
    split_Serial_Num: {
        type: String,
        required: false
    },
    mcaidNum: {
        type: String,
        required: false
    },
    sSN: {
        type: String,
        required: false
    },
    lname: {
        type: String,
        required: false
    },
    fname: {
        type: String,
        required: false
    },
    mname: {
        type: String,
        required: false
    },
    dOB: {
        type: String,
        required: false
    },
    gender: {
        type: String,
        required: false
    },
    hICNum: {
        type: String,
        required: false
    },
    address1: {
        type: String,
        required: false
    },
    address2: {
        type: String,
        required: false
    },
    city: {
        type: String,
        required: false
    },
    state: {
        type: String,
        required: false
    },
    zip: {
        type: String,
        required: false
    },
    mcaidDays_1: {
        type: String,
        required: false
    },
    begDate_1: {
        type: String,
        required: false
    },
    endDate_1: {
        type: String,
        required: false
    },
    status_1: {
        type: String,
        required: false
    },
    coverage_1: {
        type: String,
        required: false
    },
    serviceType_1: {
        type: String,
        required: false
    },
    insuranceType_1: {
        type: String,
        required: false
    },
    plan_Desc_1: {
        type: String,
        required: false
    },
    mcaidDays_2: {
        type: String,
        required: false
    },
    begDate_2: {
        type: String,
        required: false
    },
    endDate_2: {
        type: String,
        required: false
    },
    status_2: {
        type: String,
        required: false
    },
    coverage_2: {
        type: String,
        required: false
    },
    serviceType_2: {
        type: String,
        required: false
    },
    insuranceType_2: {
        type: String,
        required: false
    },
    plan_Desc_2: {
        type: String,
        required: false
    },
    mcaidDays_3: {
        type: String,
        required: false
    },
    begDate_3: {
        type: String,
        required: false
    },
    endDate_3: {
        type: String,
        required: false
    },
    status_3: {
        type: String,
        required: false
    },
    coverage_3: {
        type: String,
        required: false
    },
    serviceType_3: {
        type: String,
        required: false
    },
    insuranceType_3: {
        type: String,
        required: false
    },
    plan_Desc_3: {
        type: String,
        required: false
    },
    mcaidDays_4: {
        type: String,
        required: false
    },
    begDate_4: {
        type: String,
        required: false
    },
    endDate_4: {
        type: String,
        required: false
    },
    status_4: {
        type: String,
        required: false
    },
    coverage_4: {
        type: String,
        required: false
    },
    serviceType_4: {
        type: String,
        required: false
    },
    insuranceType_4: {
        type: String,
        required: false
    },
    plan_Desc_4: {
        type: String,
        required: false
    },
    mcaidDays_5: {
        type: String,
        required: false
    },
    begDate_5: {
        type: String,
        required: false
    },
    endDate_5: {
        type: String,
        required: false
    },
    status_5: {
        type: String,
        required: false
    },
    coverage_5: {
        type: String,
        required: false
    },
    serviceType_5: {
        type: String,
        required: false
    },
    insuranceType_5: {
        type: String,
        required: false
    },
    plan_Desc_5: {
        type: String,
        required: false
    },
    mcaidDays_6: {
        type: String,
        required: false
    },
    begDate_6: {
        type: String,
        required: false
    },
    endDate_6: {
        type: String,
        required: false
    },
    status_6: {
        type: String,
        required: false
    },
    coverage_6: {
        type: String,
        required: false
    },
    serviceType_6: {
        type: String,
        required: false
    },
    insuranceType_6: {
        type: String,
        required: false
    },
    plan_Desc_6: {
        type: String,
        required: false
    },
    partADays_1: {
        type: String,
        required: false
    },
    partABeg_1: {
        type: String,
        required: false
    },
    partAEnd_1: {
        type: String,
        required: false
    },
    partAStatus_1: {
        type: String,
        required: false
    },
    partACovLevel_1: {
        type: String,
        required: false
    },
    partADays_2: {
        type: String,
        required: false
    },
    partABeg_2: {
        type: String,
        required: false
    },
    partAEnd_2: {
        type: String,
        required: false
    },
    partAStatus_2: {
        type: String,
        required: false
    },
    partACovLevel_2: {
        type: String,
        required: false
    },
    partBDays_1: {
        type: String,
        required: false
    },
    partBBeg_1: {
        type: String,
        required: false
    },
    partBEnd_1: {
        type: String,
        required: false
    },
    partBStatus_1: {
        type: String,
        required: false
    },
    partBCovLevel_1: {
        type: String,
        required: false
    },
    partBDays_2: {
        type: String,
        required: false
    },
    partBBeg_2: {
        type: String,
        required: false
    },
    partBEnd_2: {
        type: String,
        required: false
    },
    partBStatus_2: {
        type: String,
        required: false
    },
    partBCovLevel_2: {
        type: String,
        required: false
    },
    mcaidDays: {
        type: String,
        required: false
    },
    partADays: {
        type: String,
        required: false
    },
    mcaid_PartADays: {
        type: String,
        required: false
    },
    partBDays: {
        type: String,
        required: false
    },
    mcaid_PartBDays: {
        type: String,
        required: false
    },
    partA_PartBDays: {
        type: String,
        required: false
    },
    mcaid_PartA_PartBDays: {
        type: String,
        required: false
    },
    otherPayerBegDate_1: {
        type: String,
        required: false
    },
    otherPayerEndDate_1: {
        type: String,
        required: false
    },
    otherPayerAdd_Date_1: {
        type: String,
        required: false
    },
    otherPayerInfo_1: {
        type: String,
        required: false
    },
    otherPayerCoverage_1: {
        type: String,
        required: false
    },
    otherPayerServiceType_1: {
        type: String,
        required: false
    },
    otherPayerInsuranceType_1: {
        type: String,
        required: false
    },
    otherPayerPlan_Desc_1: {
        type: String,
        required: false
    },
    otherPayerMessage1_1: {
        type: String,
        required: false
    },
    otherPayerMessage2_1: {
        type: String,
        required: false
    },
    otherPayerIdentifierCode_1: {
        type: String,
        required: false
    },
    otherPayer_Name_1: {
        type: String,
        required: false
    },
    otherPayer_RefID_1: {
        type: String,
        required: false
    },
    otherPayer_Tel_1: {
        type: String,
        required: false
    },
    otherPayer_address1_1: {
        type: String,
        required: false
    },
    otherPayer_address2_1: {
        type: String,
        required: false
    },
    otherPayer_City_1: {
        type: String,
        required: false
    },
    otherPayer_State_1: {
        type: String,
        required: false
    },
    otherPayer_Zipcode_1: {
        type: String,
        required: false
    },
    otherPayerBegDate_2: {
        type: String,
        required: false
    },
    otherPayerEndDate_2: {
        type: String,
        required: false
    },
    otherPayerAdd_Date_2: {
        type: String,
        required: false
    },
    otherPayerInfo_2: {
        type: String,
        required: false
    },
    otherPayerCoverage_2: {
        type: String,
        required: false
    },
    otherPayerServiceType_2: {
        type: String,
        required: false
    },
    otherPayerInsuranceType_2: {
        type: String,
        required: false
    },
    otherPayerPlan_Desc_2: {
        type: String,
        required: false
    },
    otherPayerMessage1_2: {
        type: String,
        required: false
    },
    otherPayerMessage2_2: {
        type: String,
        required: false
    },
    otherPayerIdentifierCode_2: {
        type: String,
        required: false
    },
    otherPayer_Name_2: {
        type: String,
        required: false
    },
    otherPayer_RefID_2: {
        type: String,
        required: false
    },
    otherPayer_Tel_2: {
        type: String,
        required: false
    },
    otherPayer_address1_2: {
        type: String,
        required: false
    },
    otherPayer_address2_2: {
        type: String,
        required: false
    },
    otherPayer_City_2: {
        type: String,
        required: false
    },
    otherPayer_State_2: {
        type: String,
        required: false
    },
    otherPayer_Zipcode_2: {
        type: String,
        required: false
    },
    otherBenefitBegDate_1: {
        type: String,
        required: false
    },
    otherBenefitEndDate_1: {
        type: String,
        required: false
    },
    otherBenefitAdd_Date_1: {
        type: String,
        required: false
    },
    otherBenefitInfo_1: {
        type: String,
        required: false
    },
    otherBenefitCoverage_1: {
        type: String,
        required: false
    },
    otherBenefitServiceType_1: {
        type: String,
        required: false
    },
    otherBenefitInsuranceType_1: {
        type: String,
        required: false
    },
    otherBenefitPlan_Desc_1: {
        type: String,
        required: false
    },
    otherBenefitMessage1_1: {
        type: String,
        required: false
    },
    otherBenefitTimePeriodQualifier_1: {
        type: String,
        required: false
    },
    otherBenefitAmount_1: {
        type: String,
        required: false
    },
    otherBenefitQuantity_1: {
        type: String,
        required: false
    },
    otherBenefitNetworkFlag_1: {
        type: String,
        required: false
    },
    otherBenefitBegDate_2: {
        type: String,
        required: false
    },
    otherBenefitEndDate_2: {
        type: String,
        required: false
    },
    otherBenefitAdd_Date_2: {
        type: String,
        required: false
    },
    otherBenefitInfo_2: {
        type: String,
        required: false
    },
    otherBenefitCoverage_2: {
        type: String,
        required: false
    },
    otherBenefitServiceType_2: {
        type: String,
        required: false
    },
    otherBenefitInsuranceType_2: {
        type: String,
        required: false
    },
    otherBenefitPlan_Desc_2: {
        type: String,
        required: false
    },
    otherBenefitMessage1_2: {
        type: String,
        required: false
    },
    otherBenefitTimePeriodQualifier_2: {
        type: String,
        required: false
    },
    otherBenefitAmount_2: {
        type: String,
        required: false
    },
    otherBenefitQuantity_2: {
        type: String,
        required: false
    },
    otherBenefitNetworkFlag_2: {
        type: String,
        required: false
    },
    otherBenefitBegDate_3: {
        type: String,
        required: false
    },
    otherBenefitEndDate_3: {
        type: String,
        required: false
    },
    otherBenefitAdd_Date_3: {
        type: String,
        required: false
    },
    otherBenefitInfo_3: {
        type: String,
        required: false
    },
    otherBenefitCoverage_3: {
        type: String,
        required: false
    },
    otherBenefitServiceType_3: {
        type: String,
        required: false
    },
    otherBenefitInsuranceType_3: {
        type: String,
        required: false
    },
    otherBenefitPlan_Desc_3: {
        type: String,
        required: false
    },
    otherBenefitMessage1_3: {
        type: String,
        required: false
    },
    otherBenefitTimePeriodQualifier_3: {
        type: String,
        required: false
    },
    otherBenefitAmount_3: {
        type: String,
        required: false
    },
    otherBenefitQuantity_3: {
        type: String,
        required: false
    },
    otherBenefitNetworkFlag_3: {
        type: String,
        required: false
    },
    transactionCnts: {
        type: String,
        required: false
    },
    validationNotes: {
        type: String,
        required: false
    },
    validationScore: {
        type: String,
        required: false
    }
});

gdsTennCareRangeSchema.plugin(splice.plugin, {});
gdsTennCareRangeSchema.plugin(plugins.CreatedAndModified);
gdsTennCareRangeSchema.plugin(audit.auditPlugin, 'GDSTennCareRange', {});
var gdsTennCareRangeModel = mongoose.model('GDSTennCareRange', gdsTennCareRangeSchema);
var gdsTennCareRange = module.exports = exports = gdsTennCareRangeModel;
