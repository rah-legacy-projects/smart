var mongoose = require('mongoose'),
    audit = require('mongoose-audit'),
    splice = require('mongoose-date-splice'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins');

var districtRangeSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    address1: {
        type: String,
        required: true
    },
    address2: {
        type: String,
        required: false
    },
    city: {
        type: String,
        required: true
    },
    state: {
        type: String,
        required: true
    },
    zip: {
        type: String,
        required: true
    },
    telephone: {
        type: String,
        required: true
    },
    claimingStartDate: {
        type: Date,
        required: false
    },
    healthPlanId: {
        type: String,
        required: false
    },
    billingInfo: {
        name: {
            type: String,
            required: false
        },
        address1: {
            type: String,
            required: false
        },
        address2: {
            type: String,
            required: false
        },
        city: {
            type: String,
            required: false
        },
        state: {
            type: String,
            required: false
        },
        zip: {
            type: String,
            required: false
        },
        taxId: {
            type: String,
            required: false
        },
        billerId: {
            type: String,
            required: false
        },
        telephone: {
            type: String,
            required: false
        }
    },
    npiNumber: {
        type: String,
        required: true
    }
}, {});

districtRangeSchema.plugin(splice.plugin, {});
districtRangeSchema.plugin(audit.auditPlugin, 'DistrictRange', {});
districtRangeSchema.plugin(plugins.CreatedAndModified);
var districtRangeModel = mongoose.model('DistrictRange', districtRangeSchema);
var districtRange = module.exports = exports = districtRangeModel;
