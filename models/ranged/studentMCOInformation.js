var mongoose = require('mongoose'),
    audit = require('mongoose-audit'),
    splice = require('mongoose-date-splice'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins');

var studentMCOInformationRangeSchema = new Schema({
    approvalToTreat: {type:Boolean, required:true},
    specialty: {type: String, required:true},
    numberOfSessions:{type:Number, required:false}
}, {});

studentMCOInformationRangeSchema.plugin(splice.plugin, {});
studentMCOInformationRangeSchema.plugin(audit.auditPlugin, 'StudentMCOInformationRange', {});
studentMCOInformationRangeSchema.plugin(plugins.CreatedAndModified);
var studentMCOInformationRangeModel = mongoose.model('StudentMCOInformationRange', studentMCOInformationRangeSchema);
var studentMCOInformationRange = module.exports = exports = studentMCOInformationRangeModel;
