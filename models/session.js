var mongoose = require('mongoose'),
    plugins = require('aio-mongoose-plugins'),
    audit = require('mongoose-audit'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId

var sessionSchema = new Schema({
    provider: {
        type: ObjectId,
        ref: 'User'
    },
    serviceType: {
        type: String,
        required: true
    },
    procedureCode: {
        type: ObjectId,
        ref: 'ProcedureCode'
    },
    sessionNotes: {
        type: String,
        required: false
    },
    attendees: [{
        type: ObjectId,
        ref: 'Attendee'
    }],
    finalizingUser: {
        type: ObjectId,
        ref: 'User',
        default: null
    },
    billedDate: {
        type: Date,
        required: false
    },

    finalizingDate: {
        type: Date,
        required: false
    },

    procedureCodeRangeAtFinalizing: {
        type: ObjectId,
        ref: 'ProcedureCodeRange',
        required: false
    }

}, {});

sessionSchema.plugin(plugins.CreatedAndModified);
sessionSchema.plugin(plugins.ActiveAndDeleted);
sessionSchema.plugin(audit.auditPlugin, 'Session', {});
var sessionModel = mongoose.model('Session', sessionSchema);
var session = module.exports = exports = sessionModel;
