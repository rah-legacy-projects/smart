var mongoose = require('mongoose'),
    audit = require('mongoose-audit'),
    plugins = require('aio-mongoose-plugins'),
    Schema = mongoose.Schema,
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    logger = require('winston'),
    district = require('./district'),
    dataUtil = require('./dataUtil'),
    ObjectId = Schema.Types.ObjectId;

var procedureCodeSchema = new Schema({

    procedureCode: {
        type: String,
        required: true
    },
    district: {
        type: ObjectId,
        ref: 'District'
    },
    rangedData: [{
        type: ObjectId,
        ref: 'ProcedureCodeRange'
    }]
}, {});

procedureCodeSchema.plugin(audit.auditPlugin, 'ProcedureCode', {});

procedureCodeSchema.plugin(plugins.CreatedAndModified);

procedureCodeSchema.statics.getCurrentById = function(procedureCodeId, cb) {
    var self = this;
    var query = null;
    if (_.isArray(procedureCodeId)) {
        query = this.find({})
            .in('_id', procedureCodeId);
    } else {
        query = self.findOne({
            _id: procedureCodeId
        });
    }

    query
        .populate('rangedData')
        .lean()
        .exec(function(err, procedureCode) {

            var tasks = [];
            if (_.isArray(procedureCode)) {
                _.each(procedureCode, function(pc) {
                    tasks.push(function(cb) {
                        dataUtil.getCurrent(pc, 'rangedData', function(err, flatProcedureCode) {
                            cb(err, flatProcedureCode);
                        });
                    });
                });
            } else {
                tasks.push(function(cb) {
                    dataUtil.getCurrent(pc, 'rangedData', function(err, flatProcedureCode) {
                        cb(err, flatProcedureCode);
                    });
                });
            }

            async.parallel(tasks, function(err, flatProcedureCodes) {
                cb(err, _.compact(flatProcedureCodes));
            });

        });
};

procedureCodeSchema.statics.getByDateById = function(date, procedureCodeId, cb) {
    var self = this;
    var query = null;
    if (_.isArray(procedureCodeId)) {
        query = this.find({})
            .in('_id', procedureCodeId);
    } else {
        query = self.findOne({
            _id: procedureCodeId
        });
    }

    query
        .populate('rangedData')
        .lean()
        .exec(function(err, procedureCode) {

            var tasks = [];
            if (_.isArray(procedureCode)) {
                _.each(procedureCode, function(pc) {
                    tasks.push(function(cb) {
                        dataUtil.getByDateRange(pc, 'rangedData', date, date, function(err, flatProcedureCode) {
                            cb(err, flatProcedureCode);
                        });
                    });
                });
            } else {
                tasks.push(function(cb) {
                    dataUtil.getByDateRange(pc, 'rangedData',date, date,  function(err, flatProcedureCode) {
                        cb(err, flatProcedureCode);
                    });
                });
            }

            async.parallel(tasks, function(err, flatProcedureCodes) {
                cb(err, _.compact(flatProcedureCodes));
            });

        });
};


procedureCodeSchema.statics.getByServiceType = function(date, serviceType, districtId, cb) {
    var self = this;
    //hack: find district to get code
    district.getCurrentById(districtId, function(err, district) {
        //get the procedure codes by district
        //populate the ranged data
        //go lean to get JS objects only
        self.find({
            district: districtId
        })
            .lean()
            .exec(function(err, procedureCodes) {
                self.getByDateById(date, _.map(procedureCodes, function(pc) {
                    return pc._id;
                }), function(err, flattenedProcedureCodes) {

                    //filter out the proc codes to just the service type
                    flattenedProcedureCodes = _.filter(flattenedProcedureCodes, function(fpc) {
                        return fpc.serviceType === serviceType;
                    });

                    cb(err, flattenedProcedureCodes)
                });

            });
    });
};

var procedureCodeModel = mongoose.model('ProcedureCode', procedureCodeSchema);
var procedureCode = module.exports = exports = procedureCodeModel;
