var moment = require('moment'),
    logger = require('winston'),
    async = require('async'),
    _ = require('lodash');
require('twix');

module.exports.getByDateRange = function(object, rangeName, startDate, endDate, cb) {
    //todo: make sure object has rangename
    //nb: object must be lean
    //todo: check for leanness of object

    //filter to current ranges
    var currentRanges = _.filter(object[rangeName], function(objectRange) {
        if (!objectRange.rangeActive) {
            return false;
        }
        var objStart = moment(objectRange.startDate),
            objEnd = moment(objectRange.endDate),
            rangeStart = moment(startDate),
            rangeEnd = moment(endDate);

        objRange = objStart.twix(objEnd);
        rangeRange = rangeStart.twix(rangeEnd);

        var retval =
            //dates are the same,
            (objStart.isSame(rangeStart) && objEnd.isSame(rangeEnd)) ||
            objRange.overlaps(rangeRange);
        return retval;
    });

    var flat = _.clone(object);
    delete flat[rangeName];

    if (currentRanges.length == 0) {
        cb('object is not current', null);
        return;
    }

    //use the topmost range
    //clone the topmost range
    //remove the identifier as not to stomp the parent record's ID
    var currentRange = _.clone(currentRanges[0]);
    var rangeId = currentRange._id;

    delete currentRange._id;
    if (currentRanges.length > 1) {
        logger.warn('object has multiple current ranges, using the first');
    }

    //merge that data into the flattened object
    _.merge(flat, currentRange);

    //add the range id back
    flat._rangeId = rangeId;


    //flat should now contain all current data
    cb(null, flat);
};
module.exports.getCurrent = function(object, rangeName, cb) {
    module.exports.getByDateRange(object, rangeName, moment(), moment(), cb);
};

module.exports.getByDateRangeSync = function(object, rangeName, startDate, endDate) {
    //todo: make sure object has rangename
    //nb: object must be lean
    //todo: check for leanness of object

    //filter to current ranges
    var currentRanges = _.filter(object[rangeName], function(objectRange) {
        if (!objectRange.rangeActive) {
            return false;
        }
        var objStart = moment(objectRange.startDate),
            objEnd = moment(objectRange.endDate),
            rangeStart = moment(startDate),
            rangeEnd = moment(endDate);

        objRange = objStart.twix(objEnd);
        rangeRange = rangeStart.twix(rangeEnd);


        //dates are the same,
        var retval = (objStart.isSame(rangeStart) && objEnd.isSame(rangeEnd)) ||
            objRange.overlaps(rangeRange);
        return retval;
    });


    var flat = _.clone(object);
    delete flat[rangeName];

    if (currentRanges.length == 0) {
        return null;
    }

    //use the topmost range
    //clone the topmost range
    //remove the identifier as not to stomp the parent record's ID
    var currentRange = _.clone(currentRanges[0]);
    var rangeId = currentRange._id;
    delete currentRange._id;
    if (currentRanges.length > 1) {
        logger.warn('object has multiple current ranges, using the first');
    }

    //add the range ID back into the flat
    flat._rangeId = rangeId;

    //merge that data into the flattened object
    _.merge(flat, currentRange);


    //flat should now contain all current data
    return flat;
};
module.exports.getCurrentSync = function(object, rangeName) {
    return module.exports.getByDateRangeSync(object, rangeName, moment(), moment());
};

module.exports.dateSplice = require('mongoose-date-splice')
    .splice;

module.exports.dateSpliceSave = function(who, object, rangesToSave, rangeName, cb) {
    //nb: object must be mongoose object
    //todo: ensure object and ranges are mongoose objects
    //todo: ensure object[rangeName] exists
    //save the changes
    var saveTasks = [];
    _.each(rangesToSave, function(rangeToSave) {
        saveTasks.push(function(cb) {
            if (!rangeToSave._id || !rangeToSave.createdBy) {
                rangeToSave.createdBy = who;
            }
            rangeToSave.modifiedBy = who;
            rangeToSave.save(function(err, result) {
                cb(err, result);
            });
        });
    });
    async.parallel(saveTasks, function(err, r) {
        if (!!err) {
            logger.error(err);
        }
        //reconcile the ranged data with saved ranges
        object[rangeName] = _.filter(object[rangeName], function(rangedData) {
            return _.filter(rangesToSave, function(saveRangedData) {
                    return rangedData.id === saveRangedData.id;
                })
                .length == 0;
        });

        object[rangeName] = object[rangeName].concat(r);
        object.modifiedBy = who;
        object.save(function(err, savedObject) {
            cb(err, savedObject);
        });
    });
};
