var mongoose = require('mongoose'),
    plugins = require('aio-mongoose-plugins'),
    audit = require('mongoose-audit'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    Schedule = require('./schedule'),
    async = require('async'),
    logger = require('winston'),
    _ = require('lodash'),
    util = require('util'),
    moment = require('moment');

var scheduleSeriesSchema = new Schema({
    startDateTime: {
        type: Date,
        required: true
    },
    location: {
        type: String,
        required: false
    },
    duration: {
        type: Number,
        required: true
    },
    services: [{
        type: ObjectId,
        ref: 'Services'
    }],
    organizer: {
        type: ObjectId,
        ref: 'User'
    },

    repeats: {
        repeatType: {
            type: String,
            required: true
        },
        repeatEvery: {
            type: Number,
            required: false
        },
        repeatBy: {
            type: String,
            required: false
        },
        ends: {
            never: {
                type: Boolean,
                required: true
            },
            occurrences: {
                type: Number,
                required: false
            },
            on: {
                type: Date,
                required: false
            }
        }
    }
}, {});

scheduleSeriesSchema.statics.createSeries = function(who, seriesObject, cb) {
    //create a new schedule series
    var series = new this(seriesObject);
    //save
    series.createdBy = who;
    series.modifiedBy = who;
    series.save(function(err, savedSeries) {
        if (!!err) {
            logger.error('problem creating schedule series');
            logger.error(err);
            cb(err, null);
            return;
        }

        if (/empty/i.test(seriesObject.repeats.repeatType)) {
            cb(err, series);
        } else {
            //abuse the update method to create the series' schedules
            savedSeries.updateSeries(who, {
                updateType: 'all'
            }, cb);
        }
    });
};

scheduleSeriesSchema.methods.updateSeries = function(who, options, cb) {
    var self = this;
    if (!options) {
        logger.error('scheduler: no options');
        cb('no options entered.', null);
        return;
    } else if (!options.updateType) {
        logger.error('scheduler: no update type');
        cb('update type not specified.', null);
        return;
    } else if (!options.newSchedule && /forward/i.test(options.updateType)) {
        logger.error('scheduler: no new schedule for forward scheduling');
        cb('new schedule not specified.', null);
        return;
    }

    //options format:
    //{
    //  originatingSchedule: ObjectId [only used for this one + and this one only]
    //  updateType: ['all'|'forward'|'one'],
    //  newSchedule: schemaObject
    //}

    if (/one/i.test(options.updateType)) {
        if (!options.originatingSchedule) {
            logger.error('originating schedule missing for one update');
            cb('forward updates need an originating schedule', null);
            return;
        }
        //this one only
        Schedule.findOne({
            _id: options.originatingSchedule
        })
            .exec(function(err, os) {
                var osdate = os.dateTime;
                //create a new schedule series with this' members, using originating schedule's date as the start
                //note: create will call update with 'all', which is ultimately what creates the schedules in a series
                var scheduleArgument = {
                    startDateTime: osdate,
                    location: self.location,
                    duration: self.duration,
                    services: self.services,
                    organizer: self.organizer,
                    repeats: {
                        repeatType: self.repeats.repeatType,
                        repeatEvery: null,
                        repeatBy: null,
                        ends: {
                            never: false,
                        }
                    }
                };

                //merge in changes from options.newSchedule
                _.merge(scheduleArgument, options.newSchedule);
                var services = scheduleArgument.services;
                if (!!options.newSchedule && !!options.newSchedule.services) {
                    services = options.newSchedule.services;
                }
                logger.silly('merged arg: ' + util.inspect(scheduleArgument));

                //edge case: one should only update this one as it's own series
                scheduleArgument.repeats.repeatType = 'empty';

                scheduleSeriesModel.createSeries(who, scheduleArgument, function(err, emptySeries) {
                    if (!!err) {
                        logger.error('error in creating one schedule: ' + err);
                    }

                    //assign the schedule to the empty series
                    os.scheduleSeries = emptySeries._id;
                    os.location = scheduleArgument.location;
                    os.duration = scheduleArgument.duration;
                    os.dateTime = scheduleArgument.startDateTime;
                    os.services = services;

                    logger.silly('about to save: ' + util.inspect(os));

                    os.save(function(err, savedschedule) {
                        cb(err, savedschedule);
                    });
                });
            });
    } else if (/forward/i.test(options.updateType)) {
        if (!options.originatingSchedule) {
            cb('forward updates need an originating schedule', null);
            return;
        }
        //this one and forward
        Schedule.findOne({
            _id: options.originatingSchedule
        })
            .exec(function(err, os) {
                var osdate = os.dateTime;
                //strike 'this' (originating schedule) and every other member of the series after
                Schedule.find({
                    scheduleSeries: self._id
                })
                    .where('dateTime')
                    .gte(osdate)
                    .remove(function(err, numberRemoved) {
                        if (err) {
                            logger.error(util.inspect(err));
                            cb(err, null);
                            return;
                        }
                        //create a new schedule series with this' members, using originating schedule's date as the start

                        //hack: if occurrences is not null, use the removed count to determine occurrences
                        //hack: this method is far simpler than trying to hack around the edge cases
                        var occurrences = null;
                        if (!!self.repeats.ends.occurrences) {
                            occurrences = self.repeats.ends.occurrences - (self.repeats.ends.occurrences - numberRemoved);
                        }
                        //note: create will call update with 'all', which is ultimately what creates the schedules in a series
                        var scheduleArgument = {
                            startDateTime: osdate,
                            location: self.location,
                            duration: self.duration,
                            services: self.services,
                            organizer: self.organizer,
                            repeats: {
                                repeatType: self.repeats.repeatType,
                                repeatEvery: self.repeats.repeatEvery,
                                repeatBy: self.repeats.repeatBy,
                                ends: {
                                    never: self.repeats.ends.never,
                                    occurrences: occurrences,
                                    on: self.repeats.ends.on
                                }
                            }
                        };

                        //merge in changes from options.newSchedule
                        _.merge(scheduleArgument, options.newSchedule);

                        scheduleSeriesModel.createSeries(who, scheduleArgument, function(err, r) {
                            if (!!err) {
                                logger.error('error in creating forward schedule: ' + err);
                            }
                            cb(err, r);
                        });
                    });
            });

    } else if (/all/i.test(options.updateType)) {
        //remove existing schedules (if any)
        Schedule.remove({
            scheduleSeries: self._id
        })
            .exec(function(err) {
                //{{{ all occurrences//{{{
                var tasks = [];
                //create schedules based on repeats referencing the newly-minted scheduled series
                if (/one time/i.test(self.repeats.repeatType)) {
                    tasks.push(function(callback) {
                        var s = new Schedule({
                            dateTime: moment(self.startDateTime)
                                .toDate(),
                            location: self.location,
                            duration: self.duration,
                            organizer: self.organizer,
                            services: self.services,
                            scheduleSeries: self._id
                        });
                        s.save(function(err, saved) {
                            callback(err, saved);
                        });
                    });
                }
                if (/daily/i.test(self.repeats.repeatType)) {
                    var days = 0;
                    //daily
                    if (!!self.repeats.ends.occurrences) {
                        //from the start date, create a schedule one per day for the occurrence number
                        days = self.repeats.ends.occurrences;
                    } else if (!!self.repeats.ends.on) {
                        //figure out the difference between beginning and end in days using moment
                        var startMoment = moment(self.startDateTime);
                        var endMoment = moment(self.repeats.ends.on);
                        days = (endMoment.diff(startMoment, 'days'));
                    } else if (self.repeats.ends.never) {
                        var startMoment = moment(self.startDateTime);
                        var endMoment = moment({
                            year: 2025,
                            month: 1,
                            day: 1
                        });
                        days = endMoment.diff(startMoment, 'days');
                    }


                    for (var i = 0; i < days; i++) {
                        (function(iterator) {
                            tasks.push(function(callback) {
                                var date = moment(self.startDateTime)
                                    .add('days', iterator)
                                    .toDate();
                                var s = new Schedule({
                                    dateTime: date,
                                    location: self.location,
                                    duration: self.duration,
                                    organizer: self.organizer,
                                    services: self.services,

                                    scheduleSeries: self._id
                                });
                                s.save(function(err, savedSchedule) {
                                    callback(err, savedSchedule);
                                });
                            });
                        })(i);
                    }
                }
                if (/weekly/i.test(self.repeats.repeatType)) {
                    var repeatEvery = 1;
                    var daysOfWeek = [];
                    var occurrences = 1;

                    //repeat every n weeks
                    if (!!self.repeats.repeatEvery) {
                        repeatEvery = self.repeats.repeatEvery;
                    }

                    //on given days
                    if (!!self.repeats.repeatBy) {
                        //if the days are defined, use defined days
                        daysOfWeek = self.repeats.repeatBy.split(',');
                    } else {
                        //use the start date's day
                        daysOfWeek.push(moment(self.startDateTime)
                            .isoWeekday());
                    }

                    //get the earliest day of week
                    var earliestDay = _.reduce(daysOfWeek, function(memo, dow) {
                        var dayofweek = moment()
                            .day(dow)
                            .weekday();
                        if (dayofweek < memo) {
                            memo = dayofweek;
                            return memo;
                        }
                    }, 8);

                    //weekly
                    if (!!self.repeats.ends.occurrences) {
                        //straight up assign occurrences
                        occurrences = self.repeats.ends.occurrences;
                    } else if (!!self.repeats.ends.on) {
                        //determine the number of weeks
                        var startMoment = moment(self.startDateTime);
                        var endMoment = moment(self.repeats.ends.on);
                        var weeks = endMoment.diff(startMoment, 'weeks');

                        //if the start moment is before or the same as the earliest day, add a week
                        if(startMoment.isBefore(moment(startMoment).day(earliestDay)) 
                            || startMoment.isSame(moment(startMoment).day(earliestDay))){
                            weeks++;
                        }

                        logger.silly('weeks found: ' + weeks)

                        //divide by repeat every to get occurrences
                        occurrences = weeks / repeatEvery;
                    } else if (self.repeats.ends.never) {
                        var startMoment = moment(self.startDateTime);
                        var endMoment = moment({
                            year: 2025,
                            month: 1,
                            day: 1
                        });
                        var weeks = endMoment.diff(startMoment, 'weeks');
                        occurrences = weeks / repeatEvery;
                    }

                    var offset = 0;
                    

                    //if that is < this series' day of week, add an offsetting week
                    if (earliestDay < moment(self.startDateTime)
                        .weekday()) {
                        offset = 1;
                    }


                    //cycle through [every n weeks] * [occurrences]
                    for (var i = 0; i < (repeatEvery * occurrences); i++) {
                        if (i % repeatEvery == 0) {
                            //for each day of week, add a schedule for that day
                            _.each(daysOfWeek, function(day) {
                                //use moment to add i+offset weeks and set to the appropriate day
                                //hack: assumes locale is the same for everywhere (0 = sunday)
                                //hack: day() uses isoWeekday (0-based) notation or dayname
                                var newdate = moment(self.startDateTime)
                                    .add('weeks', i + offset)
                                    .day(day);

                                logger.silly('\t\t occurrence %s: %s', i, newdate.format('MM-DD-YYYY'));
                                tasks.push(function(callback) {
                                    var s = new Schedule({
                                        dateTime: newdate.toDate(),
                                        location: self.location,
                                        duration: self.duration,
                                        organizer: self.organizer,
                                        services: self.services,

                                        scheduleSeries: self._id
                                    });
                                    s.save(function(err, savedSchedule) {
                                        callback(err, savedSchedule);
                                    });
                                });
                            });
                        }
                    }
                }
                if (/monthly/i.test(self.repeats.repeatType)) {
                    var repeatEvery = 1;
                    var occurrences = 1;

                    //repeat every n months
                    if (!!self.repeats.repeatEvery) {
                        repeatEvery = self.repeats.repeatEvery;
                    }



                    //monthly
                    if (!!self.repeats.ends.occurrences) {
                        //straight up assign occurrences
                        occurrences = self.repeats.ends.occurrences;
                    } else if (!!self.repeats.ends.on) {
                        //determine the number of months
                        var startMoment = moment(self.startDateTime);
                        var endMoment = moment(self.repeats.ends.on);
                        var months = endMoment.diff(startMoment, 'months');
                        //divide by repeat every to get occurrences
                        occurrences = months / repeatEvery;
                    } else if (self.repeats.ends.never) {
                        var startMoment = moment(self.startDateTime);
                        var endMoment = moment({
                            year: 2025,
                            month: 1,
                            day: 1
                        });
                        var weeks = endMoment.diff(startMoment, 'months');
                        occurrences = weeks / repeatEvery;
                    }
                    //cycle through [every n months] * [occurrences]
                    for (var i = 0; i < (repeatEvery * occurrences); i++) {
                        if (i % repeatEvery == 0) {
                            (function(iterator) {
                                tasks.push(function(callback) {
                                    var newdate = moment(self.startDateTime)
                                        .add('months', iterator);
                                    if (!!self.repeats.repeatBy) {
                                        if (/day of week/i.test(self.repeats.repeatBy)) {
                                            //day of week in #week in month
                                            var startMoment = moment(self.startDateTime);
                                            var dayOfWeek = startMoment.day();

                                            //that day of week falls on...
                                            //hack: determine the days that day of week falls on
                                            var possible = [];
                                            for(var d=1; d<= newdate.endOf('month').date(); d++){
                                                var temp = moment(newdate).startOf('month').date(d);
                                                if(dayOfWeek == temp.day()){
                                                    possible.push(d);
                                                }
                                            }
                                            //hack: moment does not have a "week of month", use ceiling of day of month divided by seven
                                            var weekOfMonth = Math.ceil(startMoment.date() / 7);
                                            newdate.date(possible[weekOfMonth-1]);
                                        } else if (/day of month/i.test(self.repeats.repeatBy)) {
                                            //day of month
                                            //hack: setting the day of month after adding a month should be a no-op... right?
                                            newdate.date(moment(self.startDateTime)
                                                .date());
                                        }
                                    }
                                    var s = new Schedule({
                                        dateTime: newdate.toDate(),
                                        location: self.location,
                                        duration: self.duration,
                                        organizer: self.organizer,
                                        services: self.services,

                                        scheduleSeries: self._id
                                    });
                                    s.save(function(err, savedSchedule) {
                                        callback(err, savedSchedule);
                                    });
                                });
                            })(i);
                        }
                    }

                }
                if (/yearly/i.test(self.repeats.repeatType)) {
                    var repeatEvery = 1;
                    var occurrences = 1;

                    //repeat every n years
                    if (!!self.repeats.repeatEvery) {
                        repeatEvery = self.repeats.repeatEvery;
                    }



                    //annually
                    if (!!self.repeats.ends.occurrences) {
                        //straight up assign occurrences
                        occurrences = self.repeats.ends.occurrences;
                    } else if (!!self.repeats.ends.on) {
                        //determine the number of years
                        var startMoment = moment(self.startDateTime);
                        var endMoment = moment(self.repeats.ends.on);
                        var years = endMoment.diff(startMoment, 'years');
                        //divide by repeat every to get occurrences
                        occurrences = years / repeatEvery;
                    } else if (self.repeats.ends.never) {
                        var startMoment = moment(self.startDateTime);
                        var endMoment = moment({
                            year: 2100,
                            month: 1,
                            day: 1
                        });
                        var weeks = endMoment.diff(startMoment, 'years');
                        occurrences = weeks / repeatEvery;
                    }
                    //cycle through [every n months] * [occurrences]
                    for (var i = 0; i < (repeatEvery * occurrences); i++) {
                        if (i % repeatEvery == 0) {
                            (function(iterator) {
                                tasks.push(function(callback) {
                                    var ff = moment(self.startDateTime)
                                        .add(iterator, 'years');
                                    var s = new Schedule({
                                        dateTime: ff.toDate(),
                                        location: self.location,
                                        duration: self.duration,
                                        organizer: self.organizer,
                                        services: self.services,

                                        scheduleSeries: self._id
                                    });
                                    s.save(function(err, savedSchedule) {
                                        callback(err, savedSchedule);
                                    });
                                });
                            })(i);
                        }
                    }
                }
                logger.silly('about to save all schedules');
                //save those schedules
                async.parallel(tasks, function(err, r) {
                    //callback
                    if (!!err)
                        logger.error(util.inspect(err));
                    logger.silly('tasks complete for scheduling, calling back');
                    cb(err, r);
                });

                // }}} //}}}
            });
    } else {
        cb('unrecognized update type', null);
        return;
    }
};


scheduleSeriesSchema.plugin(plugins.CreatedAndModified);
scheduleSeriesSchema.plugin(audit.auditPlugin, 'ScheduleSeries', {});
var scheduleSeriesModel = mongoose.model('ScheduleSeries', scheduleSeriesSchema);
var scheduleSeries = module.exports = exports = scheduleSeriesModel;
