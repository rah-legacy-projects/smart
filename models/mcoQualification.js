var mongoose = require('mongoose'),
    plugins = require('aio-mongoose-plugins'),
    audit = require('mongoose-audit'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId

var mcoQualificationSchema = new Schema({
    mco: {
        type: ObjectId,
        required: true,
        ref: 'MCO'
    },
    startDate: {
        type: Date,
        required: true
    },
    endDate: {
        type: Date,
        required: true
    },
    deleted: {
        type: Boolean,
        required: true,
        default: false
    }

    //tbd?
}, {});

mcoQualificationSchema.plugin(audit.auditPlugin, 'MCOQualification', {});
mcoQualificationSchema.plugin(plugins.CreatedAndModified);
var mcoQualificationModel = mongoose.model('MCOQualification', mcoQualificationSchema);
var mcoq = module.exports = exports = mcoQualificationModel;
