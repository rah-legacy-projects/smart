var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    logger = require('winston'),
    plugins = require('aio-mongoose-plugins'),
    districtModel = require('./district'),
    areaModel = require('./area'),
    dataUtil = require('./dataUtil'),
    audit = require('mongoose-audit'),
    util = require('util'),
    _ = require('lodash'),
    async = require('async');

var userSchema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    middleName: {
        type: String,
        required: false
    },
    lastName: {
        type: String,
        required: true
    },
    emailAddress: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    ssn: {
        type: String,
        required: false
    },
    dateOfBirth: {
        type: Date,
        required: false
    },
    gender: {
        type: String,
        required: false
    },
    active: {
        type: Boolean,
        required: true
    },
    mcoQualifications: [{
        type: ObjectId,
        ref: 'MCOQualification'
    }],
    therapistQualifications: [{
        type: ObjectId,
        ref: 'TherapistQualification'
    }]
    //tbd?
}, {});

userSchema.virtual('fullName')
    .get(function() {
        return this.firstName + " " + this.lastName;
    });

userSchema.plugin(audit.auditPlugin, 'User', {});
userSchema.plugin(plugins.AclObject);
userSchema.plugin(plugins.CreatedAndModified);

userSchema.methods.permissionsProfile = function(cb) {
    var self = this;
    var tasks = {
        districts: function(cb) {
            districtModel
                .find()
                .populate('rangedData')
                .exec(function(err, districts) {
                    cb(err, districts);
                });
        },
        areas: function(cb) {
            areaModel.find()
                .exec(function(err, areas) {
                    cb(err, areas);
                });
        }
    };

    async.parallel(tasks, function(err, r) {
        var permissions = {
            userId: self._id,
            emailAddress: self.emailAddress,
            userName: self.fullName,
            districts: {},
            areas: {}
        };

        _.each(r.areas, function(area) {
            permissions.areas[area.name] = {
                access: area.getAccess(self)
            }
        });

        _.each(r.districts, function(district) {
            var leanDistrict = {
                _id: district._id,
                districtCode: district.districtCode,
                active: district.active,
                rangedData: _.map(district.rangedData, function(dr) {
                    return {
                        _id: dr._id,
                        name: dr.name,
                        startDate: dr.startDate,
                        endDate: dr.endDate,
                        rangeActive: dr.rangeActive
                    }
                })
            };
            var flatDistrict = dataUtil.getCurrentSync(leanDistrict, 'rangedData');
            flatDistrict.access = district.getAccess(self);
            if (flatDistrict.access.length > 0 || _.contains(permissions.areas.administration.access, 'Administer')) {
                permissions.districts[flatDistrict.districtCode] = flatDistrict;
            }
        });

        cb(err, permissions);
    });
};

var userModel = mongoose.model('User', userSchema);


var user = module.exports = exports = userModel;
