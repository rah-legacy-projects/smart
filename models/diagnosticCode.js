var mongoose = require('mongoose'),
    audit = require('mongoose-audit'),
    plugins = require('aio-mongoose-plugins'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId

var diagnosticCodeSchema = new Schema({
    diagnosticCode: {
        type: String,
        required: true
    },
    district: {
        type: ObjectId,
        ref: 'District'
    },
    rangedData: [{
        type: ObjectId,
        ref: 'DiagnosticCodeRange'
    }]
}, {});

diagnosticCodeSchema.plugin(audit.auditPlugin, 'DiagnosticCode', {});
diagnosticCodeSchema.plugin(plugins.CreatedAndModified);
var diagnosticCodeModel = mongoose.model('DiagnosticCode', diagnosticCodeSchema);
var diagnosticCode = module.exports = exports = diagnosticCodeModel;
