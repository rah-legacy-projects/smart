var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    audit = require('mongoose-audit'),
    plugins = require('aio-mongoose-plugins'),
    splice = require('mongoose-date-splice'),
    ObjectId = Schema.Types.ObjectId;

var prescriptionSchema = new Schema({
    serviceType: {type:String, required:true},
    doctorName: {type:String, required:true},
}, {});

prescriptionSchema.plugin(splice.plugin, {});
prescriptionSchema.plugin(audit.auditPlugin, 'Prescription', {});
prescriptionSchema.plugin(plugins.CreatedAndModified);
var prescriptionModel = mongoose.model('Prescription', prescriptionSchema);

var student = module.exports = exports = prescriptionModel;
