var mongoose = require('mongoose'),
    plugins = require('aio-mongoose-plugins'),
    Schema = mongoose.Schema,
    audit = require('mongoose-audit'),
    splice = require('mongoose-date-splice'),
    ObjectId = Schema.Types.ObjectId;

var parentalConsentSchema = new Schema({
        toShareIEP: {
            type: Date,
            required: false
        },
        toShareIEPDenied: {
            type: Boolean,
            required: true,
            default: false
        },

        toBillMedicaid: {
            type: Date,
            required: false
        },
        toBillMedicaidDenied: {
            type: Boolean,
            required: true,
            default: false
        },

        toTreat: {
            type: Date,
            required: false
        },
        toTreatDenied: {
            type: Boolean,
            required: true,
            default:false
        },
        rangeActive: {
            type:Boolean,
            required:true,
            default:true
        }
}, {});


//parentalConsentSchema.plugin(splice.plugin, {});
parentalConsentSchema.plugin(audit.auditPlugin, 'ParentalConsent', {});
parentalConsentSchema.plugin(plugins.CreatedAndModified);

var parentalConsentModel = mongoose.model('ParentalConsent', parentalConsentSchema);

var parentalConsent = module.exports = exports = parentalConsentModel
