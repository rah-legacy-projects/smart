var mongoose = require('mongoose'),
    audit = require('mongoose-audit'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    moment = require('moment'),
    dataUtil = require('./dataUtil'),
    plugins = require('aio-mongoose-plugins');

var districtSchema = new Schema({
    districtCode: {
        type: String,
        required: true
    },
    active: {
        type: Boolean,
        required: true,
        default: true
    },
    rangedData: [{
        type: ObjectId,
        ref: 'DistrictRange'
    }]
}, {});

districtSchema.plugin(audit.auditPlugin, 'District', {});
districtSchema.plugin(plugins.AclSubject, {
    key: function() {
        return 'District:' + this._id;
    }
});
districtSchema.plugin(plugins.CreatedAndModified);

districtSchema.methods.getCurrentSync = function(){
    var self= this;
    if(!self.rangedData){
        return null;
    }

    return dataUtil.getCurrentSync({
        districtCode: self.districtCode,
        active: self.active,
        rangedData: _.map(self.rangedData, function(r){
            return {
                name: r.name,
                npiNumber: r.npiNumber,
                startDate: r.startDate,
                endDate: r.endDate,
                rangeActive: r.rangeActive
            };
        })
    }, 'rangedData')
};

districtSchema.statics.getCurrentById = function(districtId, cb) {
    this.findOne({
        _id: districtId
    })
        .populate('rangedData')
        .lean()
        .exec(function(err, district) {
            dataUtil.getCurrent(district, 'rangedData', function(err, flatDistrict) {
                cb(err, flatDistrict);
            });
        });
};

districtSchema.statics.getCurrentByShortCode = function(shortCode, cb) {
    this.findOne({
        districtCode: shortCode
    })
        .populate('rangedData')
        .lean()
        .exec(function(err, district) {
            if(!district){
                cb(err, null);
            }else{
            dataUtil.getCurrent(district, 'rangedData', function(err, flatDistrict) {
                cb(err, flatDistrict);
            });}
        });
};
var districtModel = mongoose.model('District', districtSchema);
var district = module.exports = exports = districtModel;
