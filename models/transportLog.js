var mongoose = require('mongoose'),
    plugins = require('aio-mongoose-plugins'),
    audit = require('mongoose-audit'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId

var transportLogSchema = new Schema({
    date: {
        type: Date,
        required: true
    },
    schedule: {
        type: String,
        required: true
    },
    notes: {
        type: String,
        required: false
    }
    //tbd?
}, {});

transportLogSchema.plugin(plugins.CreatedAndModified);
transportLogSchema.plugin(audit.auditPlugin, 'TransportLog', {});
var transportLogModel = mongoose.model('TransportLog', transportLogSchema);
var transportLog = module.exports = exports = transportLogModel;
