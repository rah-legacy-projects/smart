var mongoose = require('mongoose'),
    plugins = require('aio-mongoose-plugins'),
    audit = require('mongoose-audit'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId

var attendeeSchema = new Schema({
        service: {
            type: ObjectId,
            ref: 'Service'
        },
        absent: {
            type: Boolean,
            required: true
        },
        absenceReason: {
            type: String,
            required: false
        },
        individualNote: {
            type: String,
            required: false
        },
        diagnosticCode: {
            type: ObjectId,
            ref: 'DiagnosticCode'
        },
        billedDate: {
            type: Date,
            required: false
        },
        iepBilledUnits:{
            type:Number,
            required:false
        },

        diagnosticCodeRangeAtFinalizing: {
            type: ObjectId,
            ref: 'DiagnosticCodeRange'
        }
}, {});

attendeeSchema.plugin(plugins.CreatedAndModified);
attendeeSchema.plugin(plugins.ActiveAndDeleted);
attendeeSchema.plugin(audit.auditPlugin, 'Attendee', {});
var attendeeModel = mongoose.model('Attendee', attendeeSchema);
var attendee = module.exports = exports = attendeeModel;
