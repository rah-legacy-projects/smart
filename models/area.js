var mongoose = require('mongoose'),
    audit = require('mongoose-audit'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins');

var areaSchema = new Schema({
    name: {
        type: String,
        required: true
    },
}, {});

areaSchema.plugin(audit.auditPlugin, 'Area', {});
areaSchema.plugin(plugins.AclSubject, {key:function(){return 'Area:'+this._id;}});
areaSchema.plugin(plugins.CreatedAndModified);
var areaModel = mongoose.model('Area', areaSchema);
var area = module.exports = exports = areaModel;
