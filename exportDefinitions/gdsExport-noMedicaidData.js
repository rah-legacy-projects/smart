var logger = require('winston'),
    EventEmitter = require('eventemitter2'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    context = require('../models/context');

//parameters:
//startDate
//endDate
//
var GdsExport = function() {
    var self = this;
    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'GDS Export',
        className: 'gdsExport',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'State Abbreviation',
            propertyName: 'stateAbbreviation',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for State Abbreviation needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for State Abbreviation]'
            }]
        }, {
            name: 'Patient Account Number',
            propertyName: 'patientAccountNumber',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Patient Account Number needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Patient Account Number]'
            }]
        }, {
            name: 'Medicaid Recipient ID',
            propertyName: 'medicaidRecipientID',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Medicaid Recipient ID needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Medicaid Recipient ID]'
            }]
        }, {
            name: 'Plan Name',
            propertyName: 'planName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Plan Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Plan Name]'
            }]
        }, {
            name: 'Patient Insurance Policy Number',
            propertyName: 'patientInsurancePolicyNumber',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Patient Insurance Policy Number needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Patient Insurance Policy Number]'
            }]
        }, {
            name: 'Last Name',
            propertyName: 'lastName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Last Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Last Name]'
            }]
        }, {
            name: 'First Name',
            propertyName: 'firstName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for First Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for First Name]'
            }]
        }, {
            name: 'Middle Name',
            propertyName: 'middleName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Middle Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Middle Name]'
            }]
        }, {
            name: 'SSN',
            propertyName: 'sSN',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for SSN needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for SSN]'
            }]
        }, {
            name: 'Date of Birth',
            propertyName: 'dateOfBirth',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Date of Birth needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Date of Birth]'
            }]
        }, {
            name: 'Gender',
            propertyName: 'gender',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Gender needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Gender]'
            }]
        }, {
            name: 'Admit Date',
            propertyName: 'admitDate',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Admit Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Admit Date]'
            }]
        }, {
            name: 'Discharge Date',
            propertyName: 'dischargeDate',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Discharge Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Discharge Date]'
            }]
        }, {
            name: 'Customer Defined Field',
            propertyName: 'customerDefinedField',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Customer Defined Field needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Customer Defined Field]'
            }]
        }, {
            name: 'Patient Address',
            propertyName: 'patientAddress',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Patient Address needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Patient Address]'
            }]
        }, {
            name: 'Patient Mailing Address',
            propertyName: 'patientMailingAddress',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Patient Mailing Address needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Patient Mailing Address]'
            }]
        }, {
            name: 'Patient Phone Number',
            propertyName: 'patientPhoneNumber',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Patient Phone Number needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Patient Phone Number]'
            }]
        }, {
            name: 'Guarantor Full Name',
            propertyName: 'guarantorFullName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Guarantor Full Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Guarantor Full Name]'
            }]
        }, {
            name: 'Provider NPI Number',
            propertyName: 'providerNPINumber',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Provider NPI Number needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Provider NPI Number]'
            }]
        }, {
            name: 'Provider Medicare Provider ID',
            propertyName: 'providerMedicareProviderID',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Provider Medicare Provider ID needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Provider Medicare Provider ID]'
            }]
        }]
    };
    self.commit = function(value, rowIndex, cb) {
        throw "GDS Export is for export only";
        cb(null, false);
    };
    self.exportData = function(parameters, callback) {
        logger.silly('export parameters: ' + util.inspect(parameters));


        //get the gds data
        context.Student.find({
            status: /a/i,
            gdsData: null
        })
            .populate('school')
            .exec(function(err, students) {
                logger.silly('students without gds data: ' + students.length);
                //pull in student district info
                context.Student.populate(students, {
                    path: 'school.district',
                    model: context.District
                }, function(err, studentsPopulated) {
                    //make found students of the export format
                    var exportStudents = _.map(studentsPopulated, function(student) {
                        logger.silly('%s: %s to %s', student.fullName, parameters.startDate, parameters.endDate);
                        return {
                            //hack: hardcode to tn? use student addr?
                            stateAbbreviation: 'TN',
                            patientAccountNumber: student.studentId,
                            medicaidRecipientID: '',
                            planName: '',
                            patientInsurancePolicyNumber: '',
                            lastName: student.lastName,
                            firstName: student.firstName,
                            middleName: '',
                            sSN: student.ssn,
                            dateOfBirth: student.dateOfBirth,
                            gender: student.gender,
                            admitDate: parameters.startDate,
                            dischargeDate: parameters.endDate,
                            customerDefinedField: student.id,
                            patientAddress: '',
                            patientMailingAddress: '',
                            patientPhoneNumber: '',
                            guarantorFullName: '',
                            providerNPINumber: student.school.district.npiNumber,
                            providerMedicareProviderID: ''
                        };
                    });

                    callback(null, exportStudents);
                });
            });
    };
    return self;
};
GdsExport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = GdsExport;
