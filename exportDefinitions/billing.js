var logger = require('winston');

var BillingExport = function() {
    var self = this;
    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'Billing Export',
        className: 'billing',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'Student Name',
            propertyName: 'studentName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Student Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Student Name]'
            }]
        }, {
            name: 'Student ID',
            propertyName: 'studentId',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Student ID needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Student ID]'
            }]
        }, {
            name: 'District Name',
            propertyName: 'districtName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for District Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for District Name]'
            }]
        }, {
            name: 'District Code',
            propertyName: 'districtCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for District ID needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for District ID]'
            }]
        }, {
            name: 'Therapist Name',
            propertyName: 'therapistName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Therapist Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Therapist Name]'
            }]
        }, {
            name: 'Therapist NPI',
            propertyName: 'therapistNPI',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Therapist NPI needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Therapist NPI]'
            }]
        }, {
            name: 'Session Date',
            propertyName: 'sessionDate',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Session Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Session Date]'
            }]
        }, {
            name: 'Session Time',
            propertyName: 'sessionTime',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Session Time needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Session Time]'
            }]
        }, {
            name: 'Therapy',
            propertyName: 'therapy',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Therapy needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Therapy]'
            }]
        }, {
            name: 'Billing Errors',
            propertyName: 'billingErrors',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Errors needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Errors]'
            }]
        }, {
            name: 'Echo ID',
            propertyName: 'echoId',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for echo id needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for echo id]'
            }]
        },]
    };

    self.exportData = function(parameters, callback){
        throw new Error( "this export must be overridden");
    };
self.sheet.rowValidators = [{
        validator: function(value, cb) {
            cb(null, true);
        },
        errorMessage: 'not used'
    }];


    return self;
};
BillingExport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = BillingExport;
