module.exports = {
    discard: require("./discard"),
    errorCodes: require("./errorCodes"),
    noMatch: require("./noMatch"),
    remittanceRecords: require("./remittanceRecords")
}
