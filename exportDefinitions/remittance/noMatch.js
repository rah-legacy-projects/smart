var logger = require('winston');

var NoMatchImport = function() {
    var self = this;
    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'No Match Import',
        className: 'noMatch',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'Page',
            propertyName: 'page',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Page needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Page]'
            }]
        }, {
            name: 'Line',
            propertyName: 'line',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Line needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Line]'
            }]
        }, {
            name: 'Text',
            propertyName: 'text',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Text needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Text]'
            }]
        }, ]
    };
    self.commit = function(value, rowIndex, cb) {
        record.page = value.page;
        record.line = value.line;
        record.text = value.text;
        cb(null, true);
    };
    self.sheet.rowValidators = [{
        validator: function(value, cb) {
            cb(null, true);
        },
        errorMessage: '[error messages for noMatch needed]'
    }];
    return self;
};
NoMatchImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = NoMatchImport;
