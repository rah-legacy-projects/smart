var logger = require('winston');

var RemittanceRecordsImport = function() {
    var self = this;
    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'Remittance Records Import',
        className: 'remittanceRecords',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'Page',
            propertyName: 'page',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Page needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Page]'
            }]
        }, {
            name: 'Recipient Number',
            propertyName: 'recipientNumber',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Recipient Number needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Recipient Number]'
            }]
        }, {
            name: 'Recipient Last Name',
            propertyName: 'recipientLastName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Recipient Last Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Recipient Last Name]'
            }]
        }, {
            name: 'Recipient Last Initial',
            propertyName: 'recipientLastInitial',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Recipient Last Initial needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Recipient Last Initial]'
            }]
        }, {
            name: 'Recipient Middle Initial',
            propertyName: 'recipientMiddleInitial',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Recipient Middle Initial needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Recipient Middle Initial]'
            }]
        }, {
            name: 'Start Date',
            propertyName: 'startDate',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Start Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Start Date]'
            }]
        }, {
            name: 'End Date',
            propertyName: 'endDate',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for End Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for End Date]'
            }]
        }, {
            name: 'Units',
            propertyName: 'units',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Units needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Units]'
            }]
        }, {
            name: 'Procedure Code',
            propertyName: 'procedureCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Procedure Code needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Procedure Code]'
            }]
        }, {
            name: 'Echo ID',
            propertyName: 'echoId',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for echo identifier needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for echo identifier]'
            }]
        }, {
            name: 'Procedure',
            propertyName: 'procedure',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Procedure needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Procedure]'
            }]
        }, {
            name: 'Length',
            propertyName: 'length',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Length needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Length]'
            }]
        }, {
            name: 'Amount Billed',
            propertyName: 'amountBilled',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Amount Billed needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Amount Billed]'
            }]
        }, {
            name: 'Amount Allowed',
            propertyName: 'amountAllowed',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Amount Allowed needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Amount Allowed]'
            }]
        }, {
            name: 'Deductions',
            propertyName: 'deductions',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Deductions needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Deductions]'
            }]
        }, {
            name: 'Amount Paid',
            propertyName: 'amountPaid',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Amount Paid needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Amount Paid]'
            }]
        }, {
            name: 'Control Number',
            propertyName: 'controlNumber',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Control Number needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Control Number]'
            }]
        }, {
            name: 'Error Number',
            propertyName: 'errorNumber',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Error Number needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Error Number]'
            }]
        }, ]
    };
    self.commit = function(value, rowIndex, cb) {
        record.recipientNumber = value.recipientNumber;
        record.recipientLastName = value.recipientLastName;
        record.recipientFirstInitial = value.recipientFirstInitial;
        record.recipientMiddleInitial = value.recipientMiddleInitial;
        record.startDate = value.startDate;
        record.endDate = value.endDate;
        record.units = value.units;
        record.procedureCode = value.procedureCode;
        record.procedureCodeModifier = value.procedureCodeModifier;
        record.procedure = value.procedure;
        record.length = value.length;
        record.amountBilled = value.amountBilled;
        record.amountAllowed = value.amountAllowed;
        record.deductions = value.deductions;
        record.amountPaid = value.amountPaid;
        record.controlNumber = value.controlNumber;
        record.errorNumber = value.errorNumber;
        cb(null, true);
    };
    self.sheet.rowValidators = [{
        validator: function(value, cb) {
            cb(null, true);
        },
        errorMessage: '[error messages for remittanceRecords needed]'
    }];
    return self;
};
RemittanceRecordsImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = RemittanceRecordsImport;
