var logger = require('winston');

var ErrorCodesImport = function() {
    var self = this;
    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'Error Codes Import',
        className: 'errorCodes',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'Error Code',
            propertyName: 'errorCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Error Code needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Error Code]'
            }]
        }, {
            name: 'Error Description',
            propertyName: 'errorDescription',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Error Description needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Error Description]'
            }]
        }, ]
    };
    self.commit = function(value, rowIndex, cb) {
        record.errorCode = value.errorCode;
        record.errorDescription = value.errorDescription;
        cb(null, true);
    };
    self.sheet.rowValidators = [{
        validator: function(value, cb) {
            cb(null, true);
        },
        errorMessage: '[error messages for errorCodes needed]'
    }];
    return self;
};
ErrorCodesImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = ErrorCodesImport;
