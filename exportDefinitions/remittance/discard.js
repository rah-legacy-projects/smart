var logger = require('winston');

var DiscardImport = function() {
    var self = this;
    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'Discard Import',
        className: 'discard',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'Page',
            propertyName: 'page',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Page needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Page]'
            }]
        }, {
            name: 'Line',
            propertyName: 'line',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Line needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Line]'
            }]
        }, {
            name: 'Match Type',
            propertyName: 'matchType',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Match Type needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Match Type]'
            }]
        }, {
            name: 'Text',
            propertyName: 'linetext',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Text needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Text]'
            }]
        }, ]
    };
    self.commit = function(value, rowIndex, cb) {
        record.page = value.page;
        record.line = value.line;
        record.matchType = value.matchType;
        record.text = value.text;
        cb(null, true);
    };
    self.sheet.rowValidators = [{
        validator: function(value, cb) {
            cb(null, true);
        },
        errorMessage: '[error messages for discard needed]'
    }];
    return self;
};
DiscardImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = DiscardImport;
