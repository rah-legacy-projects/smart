var uuid = require('uuid'),
    redis = require('redis'),
    util = require('util'),
    fs = require('fs'),
    path = require('path'),
    logger = require('winston'),
    async = require('async'),
    _ = require('underscore'),
    context = require('../models/context'),
    dataUtil = require('../models/dataUtil');

module.exports.getUserPermissions = function(req, res) {
    async.parallel({
        schools: function(callback) {
            context.School.find({})
                .exec(function(err, schools) {
                    callback(err, schools);
                });
        },
        districts: function(callback) {
            context.District.find({})
                .exec(function(err, districts) {
                    callback(err, districts);
                });
        },
        user: function(callback) {
            context.User.findOne({
                _id: new context.ObjectId(req.params.userId)
            })
                .exec(function(err, user) {
                    callback(err, user);
                });

        },
        flatDistricts: function(callback) {
            context.District.find({})
                .populate('rangedData')
                .lean()
                .exec(function(err, districts) {
                    callback(err,
                        _.map(districts, function(district) {
                            return dataUtil.getCurrentSync(district, 'rangedData');
                        }));
                });
        },
        flatSchools: function(callback) {
            context.School.find({})
                .populate('rangedData')
                .lean()
                .exec(function(err, schools) {
                    callback(err, _.map(schools, function(school) {
                        return dataUtil.getCurrentSync(school, 'rangedData');
                    }));
                });
        }
    }, function(err, r) {
        var permissions = {
            districts: []
        };
        //build the permissions object
        _.each(r.districts, function(district) {
            var d = {
                schools: [],
                name: (_.find(r.flatDistricts, function(d) {
                    return d._id == district.id;
                })).name,
                districtCode: (_.find(r.flatDistricts, function(d) {
                    return d._id == district.id;
                })).districtCode,
                _id: district._id
            };
            _.each(_.filter(r.schools, function(s) {
                return s.district == district.id;
            }), function(school) {
                var s = {
                    name: (_.find(r.flatSchools, function(s) {
                        return s._id == school.id;
                    }))
                        .name,
                    _id: school._id,
                    schoolPermissions: school.getAccess(r.user)
                };
                d.schools.push(s);
            });
            d.districtPermissions = district.getAccess(r.user);
            permissions.districts.push(d);
        });

        //render out the permissions and user to the view
        res.render('admin/permissions/permissions', {
            data: {
                permissions: permissions,
                user: r.user
            }
        });
    });
};

module.exports.getNewUserPermission = function(req, res) {
    async.parallel({
        schools: function(callback) {
            context.School.find({})
                .populate('rangedData')
                .lean()
                .exec(function(err, schools) {
                    callback(err, _.map(schools, function(school) {
                        return dataUtil.getCurrentSync(school, 'rangedData');
                    }));
                });
        },
        districts: function(callback) {
            context.District.find({})
                .populate('rangedData')
                .lean()
                .exec(function(err, districts) {
                    callback(err, _.map(districts, function(district) {
                        return dataUtil.getCurrentSync(district, 'rangedData');
                    }));
                });
        },
        user: function(callback) {
            context.User.findOne({
                _id: new context.ObjectId(req.params.userId)
            })
                .exec(function(err, user) {
                    callback(err, user);
                });

        }
    }, function(err, r) {
        var locations = {
            districts: []
        };
        _.each(r.districts, function(d) {
            var d2 = {
                name: d.name,
                districtCode: d.districtCode,
                _id: d._id
            };
            //d2.schools = _.filter(r.schools, function(s) {
            //    return s.district == d.id;
            //});
            locations.districts.push(d2);

        });
        res.render('admin/permissions/add', {
            data: {
                derp: 'herp',
                locations: locations,
                user: r.user,
                possiblePermissions: context.Permissions
            }
        });
    });
};
module.exports.getEditUserPermission = function(req, res) {
    //req.params.userId
    //req.params.districtId
    //req.params.schoolId

    var tasks = {};
    if (!!req.params.districtId) {
        tasks.district = function(callback) {
            context.District.findOne({
                _id: new context.ObjectId(req.params.districtId)
            }).populate('rangedData')
                .exec(callback);
        };
    } else if (!!req.params.schoolId) {
        tasks.school = function(callback) {
            context.School.findOne({
                _id: new context.ObjectId(req.params.schoolId)
            })
                .exec(callback);
        };
    }
    tasks.user = function(callback) {
        context.User.findOne({
            _id: new context.ObjectId(req.params.userId)
        })
            .exec(function(err, user) {
                callback(err, user);
            });
    }

    async.parallel(tasks, function(err, r) {
        var retval = {
            data: {}
        }
        retval.data.userId = r.user._id;
        if (!!r.district) {
            retval.data.permissions = r.district.getAccess(r.user);
            retval.data.district = {
                _id: r.district._id,
                name: r.district.getCurrentSync().name,
                districtCode: r.district.districtCode
            };
        } else if (!!r.school) {
            retval.data.permissions = r.school.getAccess(r.user);
            retval.data.school = {
                _id: r.school._id,
                name: r.school.name
            };
        }
        retval.data.possiblePermissions = context.Permissions;


        res.render('admin/permissions/edit', retval);

    });
};

module.exports.postUserPermission = function(req, res) {
    var tasks = {};
    if (!!req.body.districtId) {
        tasks.district = function(callback) {
            context.District.findOne({
                _id: new context.ObjectId(req.body.districtId)
            })
                .exec(callback);
        };
    } else if (!!req.body.schoolId) {
        tasks.school = function(callback) {
            context.School.findOne({
                _id: new context.ObjectId(req.body.schoolId)
            })
                .exec(callback);
        };
    }
    tasks.user = function(callback) {
        context.User.findOne({
            _id: new context.ObjectId(req.body.userId)
        })
            .exec(function(err, user) {
                callback(err, user);
            });
    };

    async.parallel(tasks, function(err, r) {
        if (!!err) {
            logger.error(util.inspect(err));
        }
        if (!!r.district) {
            r.district.setAccess(r.user, req.body.selectedPermissions);
            r.user.modifiedBy = req.session.permissionsProfile.emailAddress;
            r.user.save(function(err, saved) {
                res.send({
                    error: err
                });
            });
        } else if (!!r.school) {
            r.school.setAccess(r.user, req.body.selectedPermissions);
            r.user.modifiedBy = req.session.permissionsProfile.emailAddress;
            r.user.save(function(err, savedUser) {
                if (!!err) {
                    logger.error(util.inspect(err));
                }
                res.send({
                    error: err
                });
            });
        }
    });
};

module.exports.postRemovePermission = function(req, res) {
    var tasks = {};
    if (!!req.body.districtId) {
        tasks.district = function(callback) {
            context.District.findOne({
                _id: new context.ObjectId(req.body.districtId)
            })
                .exec(callback);
        };
    } else if (!!req.body.schoolId) {
        tasks.school = function(callback) {
            context.School.findOne({
                _id: new context.ObjectId(req.body.schoolId)
            })
                .exec(callback);
        };
    }
    tasks.user = function(callback) {
        context.User.findOne({
            _id: new context.ObjectId(req.body.userId)
        })
            .exec(function(err, user) {
                callback(err, user);
            });
    };


    async.parallel(tasks, function(err, r) {
        if (!!err) {
            logger.error(util.inspect(err));
        }
        if (!!r.district) {
            r.district.setAccess(r.user, []);
            r.user.modifiedBy = req.session.permissionsProfile.emailAddress;
            r.user.save(function(err, saved) {
                res.send({
                    error: err
                });
            });
        } else if (!!r.school) {
            r.school.setAccess(r.user, []);
            r.user.modifiedBy = req.session.permissionsProfile.emailAddress;
            r.user.save(function(err, savedUser) {
                if (!!err) {
                    logger.error(util.inspect(err));
                }
                res.send({
                    error: err
                });
            });
        }
    });
};
