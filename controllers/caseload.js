var log = require('winston'),
    redis = require('redis'),
    util = require('util'),
    fs = require('fs'),
    path = require('path'),
    logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    context = require('../models/context'),
    dataUtil = require('../models/dataUtil');

require('../util')();

module.exports.listCaseload = function(req, res) {
    var query = context.Service.find({});

    if (!!req.body.search.myStudentsOnly && new RegExp(req.body.search.myStudentsOnly, 'i')
        .test('yes')) {
        query = query.or([{
            'therapist': req.session.permissionsProfile.userId
        }, {
            'caseManager': req.session.permissionsProfile.userId
        }]);
    }

    query.populate('rangedData')
        .populate('student')
        .populate('therapist')
        .populate('caseManager')
        .exec(function(err, services) {
            context.Student.populate(services, {
                path: 'student.rangedData',
                model: 'StudentRange'
            }, function(err, services) {
                services = _.map(services, function(service) {
                    return service.toObject();
                });
                //hack: filter to only active students' services
                services = _.filter(services, function(s) {
                    var flatStudent = dataUtil.getCurrentSync(s.student, 'rangedData');
                    return flatStudent.status === 'A';
                });

                var students = [];
                _.each(services, function(service) {
                    students.push(function(cb) {
                        context.Student.getCurrentById(service.student, function(err, student) {
                            cb(err, student);
                        });
                    });
                });

                async.parallel(students, function(err, flatStudents) {

                    var schoolIds = _.uniq(_.map(flatStudents, function(flatStudent) {
                        return flatStudent.school;
                    }));

                    var groupedByStudent = _.groupBy(services, function(service) {
                        return service.student._id;
                    });

                    var schools = [];
                    _.each(schoolIds, function(sid) {
                        schools.push(function(cb) {
                            context.School.getCurrentById(sid, function(err, flatSchool) {
                                if (flatSchool.district.toString() !== req.district._id.toString()) {
                                    cb(err, null);
                                } else {
                                    cb(err, flatSchool);
                                }
                            });
                        });
                    });

                    async.parallel(schools, function(err, flatSchools) {

                        //hack: filter out schools that are not current
                        flatSchools = _.filter(flatSchools, function(fs) {
                            return fs !== null;
                        });

                        var things = [];
                        _.forIn(groupedByStudent, function(value, key) {

                            var flatStudent = _.find(flatStudents, function(fs) {
                                return fs._id == key;
                            });
                            var flatSchool = _.find(flatSchools, function(fs) {
                                logger.silly(fs._id.toString() + ' == ' + flatStudent.school._id.toString());
                                return fs._id.toString() == flatStudent.school._id.toString();
                            });

                            if (!!flatSchool) {
                                var thing = {
                                    studentId: key,
                                    studentFirstName: flatStudent.firstName,
                                    studentLastName: flatStudent.lastName,
                                    schoolName: flatSchool.name,
                                    districtStudentId: flatStudent.studentId,
                                    services: _.map(value, function(val) {
                                        if (!val) {
                                            logger.warn('val was null for service');
                                            return null;
                                        }
                                        var ranged = dataUtil.getCurrentSync(val, 'rangedData');
                                        if (!ranged) return null;
                                        return {
                                            serviceId: ranged._id,
                                            caseManager: ("{firstName} {lastName}")
                                                .format({
                                                    firstName: val.caseManager.firstName,
                                                    lastName: val.caseManager.lastName
                                                }),
                                            therapist: ("{firstName} {lastName}")
                                                .format({
                                                    firstName: val.therapist.firstName,
                                                    lastName: val.therapist.lastName
                                                }),
                                            service: val.service,
                                            frequencyAndDuration: ("{numberOfSessions} sessions/{frequency} of {duration} min")
                                                .format({
                                                    numberOfSessions: ranged.numberOfSessions,
                                                    frequency: ranged.frequency,
                                                    duration: ranged.duration
                                                }),
                                            startDate: ranged.startDate,
                                            endDate: ranged.endDate

                                        };
                                    })
                                };
                                thing.services = _.filter(thing.services, function(s) {
                                    return s !== null;
                                });
                                if (!!thing) {
                                    things.push(thing);
                                }
                            }
                        });

                        var thingLengthPreSearch = things.length;

                        //honor searching
                        if (!!req.body.search) {
                            for (var searchMember in req.body.search) {
                                things = _.filter(things, function(s) {
                                    if (searchMember === 'myStudentsOnly') {
                                        return true;
                                    } else if (!req.body.search[searchMember] || req.body.search[searchMember] === '') {
                                        return true;
                                    } else if (!s[searchMember]) {
                                        //return true;
                                        if (s.services.length > 0 && !!s.services[0][searchMember]) {
                                            return !!(_.find(s.services, function(service) {
                                                return (new RegExp(req.body.search[searchMember], 'i'))
                                                    .test(service[searchMember]);
                                            }));
                                        } else {
                                            return true;
                                        }

                                    } else if (s[searchMember] === '') {
                                        return true;
                                    } else {
                                        return (new RegExp(req.body.search[searchMember], 'i'))
                                            .test(s[searchMember]);
                                    }
                                });
                            };
                        }

                        //todo: there is probably a clever way to do sort and slice arrays out of mongo
                        //todo: multisort
                        //todo: ensure body parts exist
                        var filteredServices = _.sortBy(things, function(s) {
                            return s[req.body.orderByClauses[0].orderByName];
                        });

                        //hack: if descending, reverse
                        if (req.body.orderByClauses[0].orderByDirection === 'descending') {
                            filteredServices.reverse();
                        }

                        //slice off the requested amount
                        logger.silly('slice start: %s, slice end: %s', req.body.displayStart, (req.body.displayStart + req.body.displayLength) - 1);
                        var slicedServices = filteredServices.slice(req.body.displayStart, (req.body.displayStart + req.body.displayLength) - 1);

                        logger.silly('filtered sechedule length: ' + filteredServices.length);


                        res.send({
                            echo: req.body.echo,
                            filteredRecords: filteredServices.length,
                            totalRecords: thingLengthPreSearch,
                            recordSet: slicedServices
                        });

                    });
                });
            });
        });
};
