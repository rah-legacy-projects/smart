var log = require('winston'),
    uuid = require('uuid'),
    redis = require('redis'),
    util = require('util'),
    fs = require('fs'),
    path = require('path');

var localstore = redis.createClient();

module.exports.getReport = function(req, res) {
    log.debug('getting report: ' + req.query.token);

    //pull the excel info out of redis
    localstore.get(req.query.token, function(err, excelProcessData) {
        var _excelProcessData = JSON.parse(excelProcessData);

        var deftype = require('../importDefinitions/' + _excelProcessData.type);
        var prettyName = (new deftype())
            .sheet
            .sheetName + " - Import Report" + path.extname(_excelProcessData.outputFilePath);

        //download the document location (guidname) as the pretty name
        res.download(_excelProcessData.outputFilePath, prettyName, function(err) {
            if (err) {
                log.error('report download error: ' + util.inspect(err));
                //TODO: check res.headerSent
            }
            //delete the document
            //fs.unlink(docLocation);
        })
    });
};
