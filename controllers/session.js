var log = require('winston'),
    redis = require('redis'),
    util = require('util'),
    fs = require('fs'),
    path = require('path'),
    logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    context = require('../models/context'),
    dataUtil = require('../models/dataUtil');

require('../util')();
module.exports.getNewSession = function(req, res) {
    res.render('session/index', {
        scheduleId: req.params.scheduleId
    });
}

module.exports.getExistingSession = function(req, res) {
    context.Schedule.findOne({
        _id: req.params.scheduleId
    })
        .populate('session')
        .exec(function(err, schedule) {
            if (!!schedule.session && !!schedule.session.finalizingUser) {
                res.render('session/view', {
                    scheduleId: req.params.scheduleId
                })
            } else {
                logger.silly('getting existing session for edit');
                res.render('session/index', {
                    scheduleId: req.params.scheduleId
                });
            }
        });
};

module.exports.adHocSession = function(req, res) {
    //get the service and student
    //create a one-time schedule for the service
    var scheduleObject = {
        startDateTime: moment()
            .roundNext15Min()
            .toDate(),
        location: ' ',
        duration: 0,
        services: [new context.ObjectId(req.params.serviceId)],
        organizer: new context.ObjectId(req.session.permissionsProfile.userId),
        repeats: {
            repeatType: 'One Time',
            ends: {
                never: false
            }
        }
    };

    context.ScheduleSeries.createSeries(req.session.permissionsProfile.emailAddress, scheduleObject, function(err, r) {
        //redirect to session begin
        res.redirect('/' + req.district.districtCode + '/session/' + r[0]._id);
    });
};

module.exports.getSession = function(req, res) {
    logger.info('getting session for ' + req.params.scheduleId);
    if (!!req.params.scheduleId) {
        //new session for a schedule.  make it and push it back.
        context.Schedule.findOne({
            _id: req.params.scheduleId
        })
            .populate('services')
            .populate('organizer')
            .populate('session')
            .exec(function(err, schedule) {


                var populationTasks = {
                    scheduleServices: function(cb) {
                        context.Service.populate(schedule, {
                            model: 'ServiceRange',
                            path: 'services.rangedData'
                        }, function(err, schedpop) {
                            cb(err, schedpop);
                        });
                    },
                    student: function(cb) {
                        context.Service.populate(schedule, {
                            path: 'services.student',
                            model: context.Student
                        }, function(err, servpop) {
                            cb(err, servpop);
                        });
                    },
                    provider: function(cb) {
                        context.Session.populate(schedule, {
                            path: 'session.provider',
                            model: context.User
                        }, function(err, pop) {
                            cb(err, pop);
                        });
                    },
                    finalizedProcedureCodeRange: function(cb) {
                        context.Session.populate(schedule, {
                            path: 'session.procedureCodeRangeAtFinalizing',
                            model: 'ProcedureCodeRange'
                        }, function(err, pop) {
                            cb(err, pop);
                        });
                    },
                    attendeeService: function(cb) {

                        async.waterfall([
                            //{{{ populate session
                            function(cb) {
                                context.Session.populate(schedule, {
                                    path: 'session.attendees',
                                    model: context.Attendee
                                }, function(err, pop) {
                                    cb(err, pop);
                                });
                            },
                            function(schedule, cb) {
                                context.Session.populate(schedule, {
                                    path: 'session.attendees.service',
                                    model: context.Service
                                }, function(err, pop) {
                                    cb(err, pop);
                                })
                            },
                            function(schedule, cb) {
                                context.Service.populate(schedule, {
                                    path: 'session.attendees.service.student', //{{{//}}}
                                    model: context.Student
                                }, function(err, pop) {
                                    cb(err, pop);
                                })
                            },
                            function(schedule, cb) {
                                context.Service.populate(schedule, {
                                    path: 'session.attendees.service.rangedData',
                                    model: 'ServiceRange'
                                }, function(err, pop) {
                                    cb(err, pop);
                                })
                            },
                            function(schedule, cb) {
                                context.Service.populate(schedule, {
                                    path: 'session.attendees.service.rangedData.diagnosticCode',
                                    model: 'DiagnosticCode'
                                }, function(err, pop) {
                                    cb(err, pop);
                                })
                            },
                            function(schedule, cb) {
                                context.Service.populate(schedule, {
                                    path: 'session.attendees.service.rangedData.diagnosticCode.rangedData',
                                    model: 'DiagnosticCodeRange'
                                }, function(err, pop) {
                                    cb(err, pop);
                                })
                            },
                            function(schedule, cb) {
                                context.Service.populate(schedule, {
                                    path: 'session.attendees.diagnosticCode',
                                    model: 'DiagnosticCode'
                                }, function(err, pop) {
                                    cb(err, pop);
                                });
                            },
                            function(schedule, cb) {
                                context.Service.populate(schedule, {
                                    path: 'session.attendees.diagnosticCode.rangedData',
                                    model: 'DiagnosticCodeRange'
                                }, function(err, pop) {
                                    cb(err, pop);
                                });
                            },
                            //}}}
                            function(schedule, cb) {
                                context.Schedule.populate(schedule, {
                                    path: 'services.rangedData',
                                    model: 'ServiceRange'
                                }, function(err, pop) {
                                    cb(err, pop);
                                });
                            },
                            function(schedule, cb) {
                                context.Schedule.populate(schedule, {
                                    path: 'services.rangedData.diagnosticCode',
                                    model: 'DiagnosticCode'
                                }, function(err, pop) {
                                    cb(err, pop);
                                });
                            },
                            function(schedule, cb) {
                                context.Schedule.populate(schedule, {
                                    path: 'services.rangedData.diagnosticCode.rangedData',
                                    model: 'DiagnosticCodeRange'
                                }, function(err, pop) {
                                    cb(err, pop);
                                });
                            },
                            function(schedule, cb) {
                                context.Schedule.populate(schedule, {
                                    path: 'session.attendees.diagnosticCodeRangeAtFinalizing',
                                    model: 'DiagnosticCodeRange'
                                }, function(err, pop) {
                                    cb(err, pop);
                                });
                            },
                            function(schedule, cb) {
                                context.Schedule.populate(schedule, {
                                    path: 'session.procedureCode',
                                    model: 'ProcedureCode'
                                }, function(err, pop) {
                                    cb(err, pop);
                                })
                            },
                            function(schedule, cb) {
                                context.Schedule.populate(schedule, {
                                    path: 'procedureCodeRangeAtFinalizing',
                                    model: 'ProcedureCodeRange'
                                }, function(err, pop) {
                                    cb(err, pop);
                                });
                            },
                            function(schedule, cb) {
                                if (!!schedule.session) {
                                    logger.silly('modified by: ' + schedule.session.modifiedBy);
                                    context.User.findOne({
                                        emailAddress: schedule.session.modifiedBy
                                    })
                                        .exec(function(err, user) {
                                            logger.silly('found mod by? ' + !!user);
                                            cb(err, schedule, user);
                                        });
                                } else {
                                    cb(err, schedule, null);
                                }
                            }
                        ], function(err, schedule, user) {
                            cb(err, {
                                schedule: schedule,
                                modifiedByUser: user
                            });
                        });
                    },
                    finalizingUser: function(cb) {
                        context.User.populate(schedule, {
                            path: 'session.finalizingUser'
                        }, function(err, pop) {
                            cb(err, pop);
                        })
                    },
                    previousAndNext: function(cb) {
                        context.Schedule.find({
                            organizer: new context.ObjectId(req.session.permissionsProfile.userId),
                            deleted: false
                        })
                            .populate('session')
                            .sort('dateTime')
                            .exec(function(err, schedules) {
                                schedules = _.chain(schedules)
                                    .filter(function(schedule) {
                                        //only include schedules without a session, no finalizing user or the one called for
                                        return !schedule.session || !schedule.session.finalizingUser || schedule._id.toString() === req.params.scheduleId;
                                    })
                                    .sortBy(function(schedule) {
                                        return moment(schedule.dateTime)
                                            .unix() + "_" + moment(schedule.created)
                                            .unix()
                                    })
                                    .value();
                                //hack: previous and next scan the entire schedule for a given user, into the past and the future
                                //hack: this can get wildly expensive and will need to be fixed in a future revision
                                //get the index of the requested id
                                var indexOfRequested = _.findIndex(schedules, function(s) {
                                    return s.id == req.params.scheduleId;
                                });
                                logger.silly('index of requested: ' + indexOfRequested + ', sched length: ' + schedules.length);
                                var temp = {
                                    previous: indexOfRequested >= 0 && indexOfRequested > 0 ? schedules[indexOfRequested - 1]._id : null,
                                    next: indexOfRequested >= 0 && indexOfRequested < (schedules.length - 1) ? schedules[indexOfRequested + 1]._id : null
                                };
                                cb(err, temp);

                            });

                    }

                };
                async.parallel(populationTasks, function(err, r) {
                    logger.silly('sessions populated');
                    var kodata = {};
                    if (!!schedule.session) {
                        //if the session exists, use the session data

                        var procedureCodeDisplay = null;
                        logger.silly('finalized pc: ' + util.inspect(schedule.session.procedureCodeRangeAtFinalizing));
                        //hack: if the session has been finalized, use the procedure code range from finalization
                        //hack: note this also alters the behavior of the UI
                        if (!!schedule.session.procedureCodeRangeAtFinalizing) {

                            procedureCodeDisplay = {
                                procedureCode: schedule.session.procedureCode.procedureCode,
                                description: schedule.session.procedureCodeRangeAtFinalizing.description,
                                procedureCodeId: schedule.session.procedureCode._id
                            };
                        }

                        //prefer finalzing user, then last mod, then IEP provider
                        var provider = schedule.session.provider;
                        if (!!r.attendeeService.modifiedByUser) {
                            provider = r.attendeeService.modifiedByUser;
                        }
                        if (!!schedule.session.finalizingUser) {
                            provider = schedule.session.finalizingUser;
                        }

                        kodata = {
                            schedule: schedule, //hack: pull in the entire schedule
                            previous: r.previousAndNext.previous,
                            next: r.previousAndNext.next,
                            providerName: provider.fullName,
                            providerId: provider._id,
                            serviceType: schedule.session.serviceType,
                            procedureCodeId: !schedule.session.procedureCode ? null : schedule.session.procedureCode._id,
                            procedureCodeDisplay: procedureCodeDisplay,
                            sessionNotes: schedule.session.sessionNotes,
                            billed: !!schedule.session.billedDate || _.any(schedule.session.attendees, function(a) {
                                return !!a.billedDate;
                            }),
                            finalizingUser: schedule.session.finalizingUser,
                            attendees: _.map(schedule.session.attendees, function(a) {
                                var flatService = dataUtil.getByDateRangeSync(a.service.toObject(), 'rangedData', schedule.dateTime, schedule.dateTime);
                                //prefer the attendee diagnostic code
                                //todo: does this also require a billed check?
                                var wholeDiagnosticCode = null;
                                if (!!a.diagnosticCode) {
                                    logger.silly('\t\tDC for attendee proper');
                                    wholeDiagnosticCode = a.diagnosticCode.toObject();
                                } else {
                                    logger.silly('\t\tusing service dc');
                                    wholeDiagnosticCode = flatService.diagnosticCode;
                                }

                                var flatDiagnostic = null;
                                if (!!wholeDiagnosticCode) {
                                    //todo: in the future, the attendee diagnostic code range at finalizing should also determine the diagnostic code to collapse to
                                    if (!!a.diagnosticCodeRangeAtFinalizing) {
                                        flatDiagnostic = {
                                            diagnosticCode: wholeDiagnosticCode.diagnosticCode,
                                            description: a.diagnosticCodeRangeAtFinalizing.description,
                                            _id: wholeDiagnosticCode._id
                                        };
                                    } else {
                                        flatDiagnostic = dataUtil.getByDateRangeSync(wholeDiagnosticCode, 'rangedData', schedule.dateTime, schedule.dateTime);
                                    }
                                }

                                return {
                                    name: a.service.student.fullName,
                                    studentId: a.service.student.studentId,
                                    internalStudentId: a.service.student._id.toString(),
                                    dateOfBirth: moment(a.service.student.dateOfBirth)
                                        .format('M/D/YYYY'),
                                    absent: a.absent,
                                    absenceReason: a.absenceReason,
                                    individualNote: a.individualNote,
                                    serviceId: a.service._id,
                                    serviceType: a.service.service,
                                    diagnosticCode: (!!flatDiagnostic ? (flatDiagnostic.diagnosticCode + ' - ' + flatDiagnostic.description) : 'Not Selected'),
                                    diagnosticCodeId: (!!flatDiagnostic ? (flatDiagnostic._id) : null),
                                    frequencyAndDuration: ("{numberOfSessions} sessions/ {frequency} of {duration} min")
                                        .format({
                                            numberOfSessions: flatService.numberOfSessions,
                                            frequency: flatService.frequency,
                                            duration: flatService.duration
                                        })
                                };
                            }),
                        };
                    } else {
                        logger.silly('session does not exist');
                        kodata = {
                            schedule: schedule, //hack: this pulls in the entire schedule
                            previous: r.previousAndNext.previous,
                            next: r.previousAndNext.next,
                            providerName: schedule.organizer.fullName,
                            providerId: schedule.organizer._id,
                            serviceType: schedule.services[0].service,
                            procedureCodeId: null,
                            sessionNotes: null,
                            finalizingUser: null,
                            billed: false,
                            attendees: _.map(schedule.services, function(a) {

                                var flatService = dataUtil.getByDateRangeSync(a.toObject(), 'rangedData', schedule.dateTime, schedule.dateTime);

                                logger.silly('flatservice dc: ' + util.inspect(flatService.diagnosticCode));
                                var flatDiagnostic = null;
                                if (!!flatService.diagnosticCode) {
                                    flatDiagnostic = dataUtil.getByDateRangeSync(flatService.diagnosticCode, 'rangedData', schedule.dateTime, schedule.dateTime);
                                }
                                logger.silly('flat dc: ' + util.inspect(flatDiagnostic));

                                //flatService = dataUtil.getByDateRangeSync(a.toObject(), 'rangedData', schedule.dateTime, schedule.dateTime);
                                return {
                                    name: a.student.fullName,
                                    internalStudentId: a.student._id.toString(),
                                    studentId: a.student.studentId,
                                    dateOfBirth: moment(a.student.dateOfBirth)
                                        .format('M/D/YYYY'),
                                    absent: null,
                                    absenceReason: null,
                                    individualNote: null,
                                    serviceId: a._id,
                                    serviceType: a.service,
                                    diagnosticCode: (!!flatDiagnostic ? (flatDiagnostic.diagnosticCode + ' - ' + flatDiagnostic.description) : 'Not Selected'),
                                    diagnosticCodeId: (!!flatDiagnostic ? (flatDiagnostic._id) : null),
                                    frequencyAndDuration: ("{numberOfSessions} sessions/ {frequency} of {duration} min")
                                        .format({
                                            numberOfSessions: flatService.numberOfSessions,
                                            frequency: flatService.frequency,
                                            duration: flatService.duration
                                        })

                                };
                            }),
                        };
                    }

                    res.send({
                        err: null,
                        value: kodata
                    });
                });
            });
    }
};


module.exports.saveSession = function(req, res) {


    var populateSchedule = function(scheduleId, cb) {
        async.waterfall([

            function(cb) {
                context.Schedule.findOne({
                    _id: scheduleId
                })
                    .populate('session')
                    .populate('services')
                    .populate('scheduleSeries')
                    .exec(function(err, schedule) {
                        cb(err, schedule);
                    });
            },
            function(schedule, cb) {
                context.Schedule.populate(schedule, {
                    path: 'services.rangedData',
                    model: 'ServiceRange'
                }, function(err, schedule) {
                    cb(err, schedule);
                });
            },
            function(schedule, cb) {
                context.Schedule.populate(schedule, {
                    path: 'services.rangedData.diagnosticCode',
                    model: 'DiagnosticCode'
                }, function(err, schedule) {
                    cb(err, schedule);
                });
            },
            function(schedule, cb) {
                context.Schedule.populate(schedule, {
                    path: 'services.rangedData.diagnosticCode.rangedData',
                    model: 'DiagnosticCodeRange'
                }, function(err, schedule) {
                    cb(err, schedule);
                });
            },
            function(schedule, cb) {
                context.Schedule.populate(schedule, {
                    path: 'session.attendees',
                    model: 'Attendee'
                }, function(err, schedule) {
                    cb(err, schedule);
                });
            },
            function(schedule, cb) {
                context.Schedule.populate(schedule, {
                    path: 'session.attendees.diagnosticCode',
                    model: 'DiagnosticCode'
                }, function(err, schedule) {
                    cb(err, schedule);
                });
            },
            function(schedule, cb) {
                context.Schedule.populate(schedule, {
                    path: 'session.attendees.diagnosticCode.rangedData',
                    model: 'DiagnosticCodeRange'
                }, function(err, schedule) {
                    cb(err, schedule);
                });
            }
        ], function(err, schedule) {
            cb(err, schedule);
        });
    };


    var tasks = [

        function(cb) {
            populateSchedule(req.params.scheduleId, function(err, schedule) {
                cb(err, schedule);
            });
        },

        function(schedule, cb) {
            if (!schedule) {
                logger.debug('schedule was new, creating one');
                schedule = new context.Schedule();
                schedule.createdBy = req.session.permissionsProfile.emailAddress;
                //todo: need to include provider for ad hoc sessions
                //schedule.provider = 
            }

            var session = schedule.session;
            if (!session) {
                session = new context.Session();
                logger.debug('schedule session was new, creating one');
                session.createdBy = req.session.permissionsProfile.emailAddress;
            } else {
                logger.debug('session exists.  updating.');
            }
            cb(null, session, schedule);
        },
        function(session, schedule, cb) {
            var scheduleStudents = _.map(schedule.services, function(service) {
                return service.id.toString();
            });

            var sessionStudents = _.map(req.body.attendees, function(attendee) {
                return attendee.serviceId.toString();
            });
            var attendeeDifferences = _.xor(scheduleStudents, sessionStudents);

            if (moment(schedule.dateTime)
                .format('YYYY-MM-DD hh:mm:ss') !== moment(req.body.schedule.dateTime)
                .format('YYYY-MM-DD hh:mm:ss') ||
                schedule.duration.toString() !== req.body.schedule.duration ||
                schedule.location !== req.body.schedule.location ||
                attendeeDifferences.length > 0) {
                logger.silly('schedule and session were different');
                logger.silly(moment(schedule.dateTime)
                    .format('YYYY-MM-DD hh:mm:ss') + ' !== ' + moment(req.body.schedule.dateTime)
                    .format('YYYY-MM-DD hh:mm:ss'));
                logger.silly(schedule.duration + ' !== ' + req.body.schedule.duration);
                logger.silly(schedule.location + ' !== ' + req.body.schedule.location);
                logger.silly('attendee diff: ' + util.inspect(attendeeDifferences));

                logger.silly('updating the series');

                //pull together a schedule argument
                //should only need the time, location, duration and students
                //the update type should take care of the rest
                var scheduleArgument = {
                    updateType: 'one',
                    originatingSchedule: schedule._id,
                    newSchedule: {

                        startDateTime: moment(req.body.schedule.dateTime)
                            .toDate(),
                        duration: req.body.schedule.duration,
                        location: req.body.schedule.location,
                        repeats: {
                            ends: {
                                never: false
                            }
                        },
                        services: _.map(req.body.attendees, function(attendee) {
                            return attendee.serviceId;
                        })
                    }
                };
                logger.silly('schedule argument: ' + util.inspect(scheduleArgument));

                schedule.scheduleSeries.updateSeries(
                    req.session.permissionsProfile.emailAddress,
                    scheduleArgument,
                    function(err, s) {
                        logger.silly('schedule saved: ' + util.inspect(s));
                        populateSchedule(s._id, function(err, schedule) {
                            cb(err, session, schedule);
                        });
                    });

            } else {
                logger.silly('skipping schedule update: nothing has changed');
                cb(null, session, schedule);
            }
        },
        function(session, schedule, cb) {
            logger.silly('schedule services before proc code: ' + util.inspect(schedule.services));
            logger.silly('getting procedure code: ' + req.body.procedureCodeId);
            if (req.body.procedureCodeId == '') {
                logger.silly('procedure code id is empty string?');
                req.body.procedureCodeId = null;
            }
            context.ProcedureCode.findOne({
                _id: req.body.procedureCodeId
            })
                .populate('rangedData')
                .lean()
                .exec(function(err, procedureCode) {
                    if (!!procedureCode) {
                        var flatProcedureCode = dataUtil.getByDateRangeSync(procedureCode, 'rangedData', schedule.dateTime, schedule.dateTime);
                        cb(err, flatProcedureCode, session, schedule);
                    } else {
                        cb(err, null, session, schedule);
                    }
                });
        },
        function(flatProcedureCode, session, schedule, cb) {
            //fill in the session with the passed up values
            session.procedureCode = req.body.procedureCodeId == '' ? null : req.body.procedureCodeId;
            session.sessionNotes = req.body.sessionNotes;
            if (!session.provider) {
                //hack: set up the provider to be the current user if it's not set
                session.provider = req.session.permissionsProfile.userId;
            }
            session.serviceType = req.body.serviceType;
            session.procedureCode = req.body.procedureCodeId || null;

            logger.debug('finalizing value: ' + req.body.finalize);
            if (!!req.body.finalize && /true/i.test(req.body.finalize)) {
                session.finalizingUser = req.session.permissionsProfile.userId;
                session.procedureCodeRangeAtFinalizing = flatProcedureCode._rangeId;
            } else {
                //unfinalize
                logger.silly('unfinalizing user');
                session.finalizingUser = null;
                session.procedureCodeRangeAtFinalizing = null;
            }


            var attendeeSaves = [];
            _.each(req.body.attendees, function(attendee) {
                var originalAttendee = _.find(session.attendees, function(oa) {
                    if (!oa.serviceId) {
                        return false;
                    }
                    return oa.serviceId.toString() == attendee.serviceId;
                });


                var scheduleService = dataUtil.getByDateRangeSync(_.find(schedule.services, function(svc) {
                        logger.silly('\t\t\t ' + attendee.serviceId + ' == ' + svc.id.toString());
                        return attendee.serviceId == svc._id.toString();
                    })
                    .toObject(), 'rangedData', schedule.dateTime, schedule.dateTime);

                logger.silly('schedule service: ' + util.inspect(scheduleService));
                if (!!scheduleService.diagnosticCode) {
                    scheduleService.diagnosticCode = dataUtil.getByDateRangeSync(scheduleService.diagnosticCode, 'rangedData', schedule.dateTime, schedule.dateTime);
                }


                if (!originalAttendee) {
                    //create the new attendee record
                    attendeeSaves.push(function(cb) {
                        originalAttendee = new context.Attendee({
                            service: attendee.serviceId,
                            absent: attendee.absent,
                            absenceReason: attendee.absenceReason,
                            individualNote: attendee.individualNote,
                            //if the finalizing user is null, clear the attendee dc
                            //otherwise save the attendee dc coalesced with the attendee svc dc
                            diagnosticCode: !session.finalizingUser ? null : (scheduleService.diagnosticCode._id),
                            diagnosticCodeRangeAtFinalizing: !session.finalizingUser ? null : (scheduleService.diagnosticCode._rangeId)
                        });
                        if (req.body.finalize) {
                            diagnosticCodeRangeAtFinalizing = null;
                        } else {
                            diagnosticCodeRangeAtFinalizing = null;
                        }
                        originalAttendee.save(function(err, attendee) {
                            if (!!err) {
                                logger.error('problem saving attendee: ' + err);
                            }
                            cb(err, attendee);
                        });
                    });
                } else {
                    //update the existing record
                    attendeeSaves.push(function(cb) {
                        originalAttendee.absent = attendee.absent;
                        originalAttendee.absenceReason = attendee.absenceReason;
                        originalAttendee.individualNote = attendee.individualNote;
                        originalAttendee.diagnosticCode = !session.finalizingUser ? null : (scheduleService.diagnosticCode._id);
                        originalAttendee.diagnosticCodeRangeAtFinalizing = !session.finalizingUser ? null : (scheduleService.diagnosticCode._rangeId);
                        if (req.body.finalize) {
                            originalAttendee.diagnosticCodeRangeAtFinalizing = null;
                        } else {
                            originalAttendee.diagnosticCodeRangeAtFinalizing = null;
                        }
                        originalAttendee.save(function(err, attendee) {
                            if (!!err) {
                                logger.error('problem updating attendee: ' + err);
                            }
                            cb(err, attendee);
                        });
                    });
                }

            });

            async.parallel(attendeeSaves, function(err, r) {
                //assign that into the session
                session.attendees = r;
                //save the session
                session.modifiedBy = req.session.permissionsProfile.emailAddress;
                session.save(function(err, savedSession) {
                    if (!!err) {
                        logger.error('problem in session save: ' + err);
                    }
                    schedule.session = session._id;
                    schedule.modifiedBy = req.session.permissionsProfile.emailAddress;
                    schedule.save(function(err) {
                        if (!!err) {
                            logger.error('problem in schedule save: ' + err);
                        }
                        cb(err);

                    });
                });
            });
        }

    ];

    async.waterfall(tasks, function(err) {
        if (!!err) {
            logger.error(err);
        }
        res.send({
            err: err,
            value: {
                providerName: req.session.permissionsProfile.userName,
                providerId: req.session.permissionsProfile.userId
            }
        });
    });
}
