module.exports.get = function(req, res) {
    var permissionsProfile = req.session.permissionsProfile;

    var districtKeys = _.keys(permissionsProfile.districts);
    if (districtKeys.length > 1 ||
        (!!permissionsProfile.areas.administration &&
            !!permissionsProfile.areas.administration.access &&
            _.contains(permissionsProfile.areas.administration.access, 'Administer'))) {
        //if user has access to >1 district or is an administrator, force through district selection
        logger.silly('redirecting to district selection');
        res.redirect('/districtSelection');
    } else if (districtKeys.length == 0) {
        //if the user has no access, show user as locked
        logger.warn('login for %s failed - no district access');
        res.render('home/login', {
            error: 'User has been locked.  Please contact techinical support.'
        });
    } else {
        //otherwise, redirect to the district user has access to
        res.redirect('/' + districtKeys[0]);
    }
};
