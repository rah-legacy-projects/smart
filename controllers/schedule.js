var log = require('winston'),
    redis = require('redis'),
    util = require('util'),
    fs = require('fs'),
    path = require('path'),
    logger = require('winston'),
    _ = require('underscore'),
    dataUtil = require('../models/dataUtil'),
    async = require('async'),
    moment = require('moment'),
    context = require('../models/context');

require('../util')();

module.exports.upcoming = function(req, res) {

    context.Schedule.find({
        organizer: new context.ObjectId(req.session.permissionsProfile.userId),
        $and: [{
            dateTime: {
                $gte: moment(req.body.search['startDate'])
                    .toDate()
            }
        }, {
            dateTime: {
                $lte: moment(req.body.search['endDate'])
                    .endOf('day')
                    .toDate()
            }
        }],
        deleted: false
    })
        .populate('services')
        .populate('session')
        .exec(function(err, schedules) {
            //filter out the schedules that are finalized
            schedules = _.filter(schedules, function(schedule) {
                if (!!schedule.session) {
                    return schedule.session.finalizingUser == null;
                }
                return true;
            });
            context.Student.populate(schedules, {
                path: 'services.student',
            }, function(err, schedules) {
                context.ranged.Student.populate(schedules, {
                    path: 'services.student.rangedData'
                }, function(err, schedules) {

                    var districtScheduleMapping = [];
                    _.forEach(schedules, function(schedule) {
                        _.forEach(schedule.services, function(service) {
                            districtScheduleMapping.push(function(cb) {
                                //get the current student
                                var currentStudent = dataUtil.getCurrentSync(service.student.toObject(), 'rangedData');
                                //get the current school for that student
                                context.School.getCurrentById(currentStudent.school, function(err, school) {
                                    cb(err, {
                                        district: school.district,
                                        schedule: schedule.id
                                    });
                                });
                            });
                        });
                    });

                    async.parallel(districtScheduleMapping, function(err, r) {
                        schedules = _.filter(schedules, function(schedule) {
                            return _.filter(r, function(rr) {
                                    return rr.schedule == schedule.id &&
                                        rr.district == req.district._id.toString()
                                })
                                .length > 0;
                        });

                        //map schedules to a flattened viewmodel
                        var mappedSchedules = _.map(schedules, function(s) {
                                var temp = _.map(s.services, function(service) {
                                    return dataUtil.getCurrentSync(service.student.toObject(), 'rangedData');
                                });

                                if (s.deleted) {
                                    return null;
                                }
                                if (!!s.session && !!s.session.billedDate) {
                                    return null;
                                }
                                if (!!s.session && s.session.isFinalized) {
                                    return null;
                                }

                                return {
                                    finalizingUserId: !!s.session ? s.session.finalizingUser : null,
                                    isBilled: !!s.session ? !!s.session.billedDate : false,
                                    datetime: s.dateTime,
                                    location: s.location,
                                    duration: s.duration,
                                    service: s.services[0].service,
                                    scheduleId: s._id,
                                    sessionId: s.session,
                                    students: _.map(temp, function(s) {
                                        return {
                                            name: s.firstName + ' ' + s.lastName,
                                            internalStudentId: s._id
                                        }
                                    }),
                                    districtCode: req.district.districtCode //hack: include the district code on the table response
                                };
                            })
                            .filter(function(r) {
                                return r != null;
                            });


                        logger.silly('mapped schedules: ' + mappedSchedules.length);

                        //honor searching
                        if (!!req.body.search) {
                            for (var searchMember in req.body.search) {
                                mappedSchedules = _.filter(mappedSchedules, function(s) {
                                    if (!s[searchMember]) {
                                        if (/startdate/i.test(searchMember)) {
                                            var startMoment = moment(req.body.search[searchMember])
                                            var scheduleMoment = moment(s.datetime);
                                            return startMoment.isBefore(scheduleMoment);
                                        } else if (/endDate/i.test(searchMember)) {
                                            var startMoment = moment(req.body.search[searchMember])
                                                .add(1, 'days')
                                                .subtract(1, 'seconds');
                                            var scheduleMoment = moment(s.datetime);
                                            return startMoment.isAfter(scheduleMoment);
                                        }
                                    } else if (s[searchMember] === '') return true;
                                    return (new RegExp(req.body.search[searchMember], 'i'))
                                        .test(s[searchMember]);
                                });
                            };
                        }

                        //todo: there is probably a clever way to do sort and slice arrays out of mongo
                        //todo: multisort
                        //todo: ensure body parts exist
                        var filteredSchedules = _.sortBy(mappedSchedules, function(s) {
                            if (req.body.orderByClauses[0].orderByName == 'datetime') {
                                return moment(s[req.body.orderByClauses[0].orderByName])
                                    .unix() + "_" + moment(s.created)
                                    .unix();
                            } else {
                                return s[req.body.orderByClauses[0].orderByName];
                            }
                        });

                        //hack: if descending, reverse
                        if (req.body.orderByClauses[0].orderByDirection === 'descending') {
                            filteredSchedules.reverse();
                        }

                        //slice off the requested amount
                        var slicedSchedules = filteredSchedules.slice(req.body.displayStart, (req.body.displayStart + req.body.displayLength));


                        var temp = {
                            echo: req.body.echo,
                            filteredRecords: filteredSchedules.length,
                            totalRecords: schedules.length,
                            recordSet: slicedSchedules
                        };
                        res.send(temp);
                    });
                });
            });
        });
};

module.exports.unfinished = function(req, res) {
    context.Schedule.find({
        organizer: new context.ObjectId(req.session.permissionsProfile.userId),
        deleted: false
    })
        .populate('services')
        .populate('session')
        .exec(function(err, schedules) {
            var temp = _.chain(schedules)
                .sortBy(function(s) {
                    return moment(s.dateTime)
                        .format('YYYYMMDD');
                })
                .find(function(s) {
                    return ((!s.session) || (!s.session.finalizingUser) || (!!s.session.deleted)) &&
                        //moment(req.body.startDate).isAfter(moment(s.dateTime));
                        moment()
                        .startOf('day')
                        .isAfter(moment(s.dateTime));
                })
                .value();
            if (!!temp) {
                temp = temp.toObject();
            }

            var returner = {
                err: err,
                value: null
            };
            if (!!temp) {
                returner.value = temp;
            }

            res.send(returner);

        });
}

module.exports.getAddSchedule = function(req, res) {
    //req.params should have the service id
    //req.session has the user id

    res.render('schedule/index', {
        serviceId: req.params.serviceId,
        scheduleId: null
    });
};

module.exports.getModifySchedule = function(req, res) {
    throw 'modifying scheudles not implmented yet'
    res.render('schedule/index', {});
};

module.exports.scheduleInfo = function(req, res) {
    var tasks = {
        service: function(cb) {
            context.Service
                .findOne({
                    _id: req.body.serviceId
                })
                .populate('student')
                .populate('rangedData')
                .exec(function(err, sm) {

                    async.series([

                        function(cb) {
                            context.Service.populate(sm, {
                                path: 'rangedData.diagnosticCode',
                                model: 'DiagnosticCode'
                            }, function(err, pop) {
                                cb(err, pop);
                            });
                        },
                        function(cb) {
                            context.Service.populate(sm, {
                                path: 'rangedData.diagnosticCode.rangedData',
                                model: 'DiagnosticCodeRange'
                            }, function(err, pop) {
                                cb(err, pop);
                            });
                        }
                    ], function(err) {


                        var s = dataUtil.getCurrentSync(sm.toObject(), 'rangedData');

                        var flatDiagnostic = null;
                        if (!!s.diagnosticCode) {
                            flatDiagnostic = dataUtil.getCurrentSync(s.diagnosticCode, 'rangedData');
                        }
                        logger.silly('flat dc: ' + util.inspect(flatDiagnostic));



                        var ko = {
                            name: ("{firstName} {lastName}")
                                .format({
                                    firstName: s.student.firstName,
                                    lastName: s.student.lastName
                                }),
                            schoolName: s.school.name,
                            studentId: s.student.studentId,
                            id: s.student._id,
                            serviceType: s.service,
                            status: s.student.status,
                            diagnosticCode: (!!flatDiagnostic ? (flatDiagnostic.diagnosticCode + ' - ' + flatDiagnostic.description) : 'Not Selected'),
                            frequencyAndDuration: ("{numberOfSessions} sessions/ {frequency} of {duration} min")
                                .format({
                                    numberOfSessions: s.numberOfSessions,
                                    frequency: s.frequency,
                                    duration: s.duration
                                }),
                            serviceId: s._id
                        };
                        cb(err, ko);
                    });
                });
        },
        user: function(cb) {
            context.User.findOne({
                _id: req.session.permissionsProfile.userId
            })
                .exec(function(err, u) {
                    cb(err, u);
                });
        },
        schedule: function(cb) {
            if (!req.body.seriesId) {
                cb(null, null);
            } else {
                context.Schedule.findOne({
                    _id: req.body.scheduleId
                })
                    .exec(function(err, ss) {
                        context.Service.find({})
                            .where('_id')
                            .in(ss.services)
                            .populate('rangedData')
                            .populate('student')
                            .lean()
                            .exec(function(err, services) {
                                var flattenedSvcs = [];
                                _.each(services, function(service) {
                                    flattenedSvcs.push(
                                        dataUtil.getCurrentSync(service, 'rangedData'));
                                });


                                //todo: add diagnostic code to edit
                                //map services to a flattened viewmodel
                                var mappedServices = _.map(flattenedSvcs, function(s) {
                                    return {
                                        name: ("{firstName} {lastName}")
                                            .format({
                                                firstName: s.student.firstName,
                                                lastName: s.student.lastName
                                            }),
                                        schoolName: s.school.name,
                                        studentId: s.student.studentId,
                                        id: s.student._id,
                                        serviceType: s.service,
                                        status: s.student.status,
                                        frequencyAndDuration: ("{numberOfSessions} sessions/ {frequency} of {duration} min")
                                            .format({
                                                numberOfSessions: s.numberOfSessions,
                                                frequency: s.frequency,
                                                duration: s.duration
                                            }),
                                        serviceId: s._id
                                    };
                                });
                                cb(err, flattenedSvcs);
                            });
                    });
            }
        }
    };
    async.parallel(tasks, function(err, r) {
        //pull together data for the knockout model and return it
        var kodata = {
            providerName: r.user.fullName,
            providerId: r.user._id,
        };

        if (!!r.schedule) {
            kodata.scheduleId = r.schedule._id;
            kodata.seriesId = r.schedule.scheduleSeries;

            //hack: all services are the same?
            kodata.serviceName = r.schedule.services[0].serviceType;

            //if the schedule exists, map the scheduled students in the schedule
            kodata.services = r.schedule.services;
        } else {
            //otherwise, map the student in the found service (this is a request for a new service schedule)
            kodata.serviceName = r.service.serviceType;
            kodata.serviceId = r.service._id;

            kodata.services = [r.service];
        }

        res.send(kodata);
    });

};

module.exports.saveSeries = function(req, res) {
    var scheduleObject = req.body;
    //hack: convert array of service objects in to array of student strings
    scheduleObject.services = _.map(scheduleObject.services, function(serviceObject) {
        return serviceObject.serviceId;
    });

    //get the schedule series by series id
    if (!scheduleObject.seriesId) {
        //it's a new series, create it
        context.ScheduleSeries.createSeries(req.session.permissionsProfile.emailAddress, scheduleObject, function(err, r) {

            if (!!err) {
                logger.error(util.inspect(err));
                logger.error(err.stack);
            } else if (!!r) {}
            res.send({
                err: err,
                result: !!r
            }); //?
        });
    } else {
        //it's an existing series, update it
        context.ScheduleSeries.findOne({
            _id: scheduleObject.seriesId
        })
            .exec(function(err, seriesToUpdate) {
                //make an options object
                var options = {
                    newSchedule: scheduleObject,
                    updateType: 'todo',
                    originatingSchedule: schduleObject.scheduleId
                };
                seriesToUpdate.updateSeries(req.session.permissionsProfile.emailAddress, options, function(err, r) {
                    res.send({
                        err: err,
                        result: !!r
                    });
                });
            });
    }
};

module.exports.getServicesByService = function(req, res) {

    var serviceQuery = context.Service.find({
        //therapist: new context.ObjectId(req.session.userId),
    })
    if (!!req.body.service) {
        serviceQuery = serviceQuery.where('service')
            .equals(req.body.service);
    }


    serviceQuery
        .populate('student')
        .populate('therapist')
        .populate('caseManager')
        .populate('rangedData')
    //.lean()
    .exec(function(err, services) {
        if (!!err) {
            logger.error(err);
            logger.error(err.stack);
        }

        var tasks = [

            function(cb) {
                context.School.populate(services, {
                    path: 'student.rangedData',
                    model: 'StudentRange'
                }, cb);
            },
            function(cb) {
                context.School.populate(services, {
                    path: 'student.rangedData.school',
                    model: 'School'
                }, cb);
            },
            function(cb) {
                context.School.populate(services, {
                    path: 'student.rangedData.school.rangedData',
                    model: 'SchoolRange'
                }, cb);
            },
            function(cb) {
                context.Service.populate(services, {
                    path: 'rangedData.diagnosticCode',
                    model: 'DiagnosticCode'
                }, cb);
            },
            function(cb) {
                context.Service.populate(services, {
                    path: 'rangedData.diagnosticCode.rangedData',
                    model: 'DiagnosticCodeRange'
                }, cb);
            }
        ];

        async.series(tasks, function(err, p) {
            var flattenedSvcs = _.chain(services)
                .map(function(service) {

                    var date = moment(req.body.date);
                    var flatService = dataUtil.getByDateRangeSync(service.toObject(), 'rangedData', date, date);
                    if (!!flatService) {
                        var flatStudent = dataUtil.getByDateRangeSync(flatService.student, 'rangedData', date, date);
                        var flatSchool = dataUtil.getByDateRangeSync(flatStudent.school, 'rangedData', date, date);
                        var flatDiagnostic = null;
                        if (!!flatService.diagnosticCode) {
                            flatDiagnostic = dataUtil.getByDateRangeSync(flatService.diagnosticCode, 'rangedData', date, date);
                        }


                        if (flatSchool.district.toString() !== req.district._id.toString() || !(/a/i.test(flatStudent.status))) {
                            return null;
                        }

                        return {
                            name: ("{firstName} {lastName}")
                                .format({
                                    firstName: flatStudent.firstName,
                                    lastName: flatStudent.lastName
                                }),
                            schoolName: flatSchool.name,
                            dateOfBirth: moment(flatStudent.dateOfBirth)
                                .format('M/D/YYYY'),
                            studentId: flatStudent.studentId,
                            id: flatStudent._id,
                            serviceType: flatService.service,
                            status: flatStudent.status,
                            diagnosticCode: (!!flatDiagnostic ? (flatDiagnostic.diagnosticCode + ' - ' + flatDiagnostic.description) : 'Not Selected'),
                            frequencyAndDuration: ("{numberOfSessions} sessions/ {frequency} of {duration} min")
                                .format({
                                    numberOfSessions: flatService.numberOfSessions,
                                    frequency: flatService.frequency,
                                    duration: flatService.duration
                                }),
                            serviceId: flatService._id
                        };
                    } else {
                        return null;
                    }


                })
                .filter(function(s) {
                    return s !== null;
                })
                .value();

            res.send({
                err: err,
                value: flattenedSvcs
            });
        });
    });


}

module.exports.deleteSchedule = function(req, res) {
    context.Schedule.findOne({
        _id: new context.ObjectId(req.params.scheduleId)
    })
        .populate('session')
        .exec(function(err, schedule) {
            var tasks = [];
            if (!!schedule.session) {
                tasks.push(function(cb) {
                    schedule.session.deleted = true;
                    schedule.modifiedBy = req.session.permissionsProfile.emailAddress;
                    schedule.session.save(function(err) {
                        cb(err);
                    });
                });
            }
            tasks.push(function(cb) {
                schedule.deleted = true;
                schedule.modifiedBy = req.session.permissionsProfile.emailAddress;
                schedule.save(function(err) {
                    cb(err);
                });
            });

            async.parallel(tasks, function(err) {
                res.send({
                    err: err,
                    value: !err
                });
            });
        });
};
