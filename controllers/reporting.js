var logger = require('winston'),
    uuid = require('uuid'),
    redis = require('redis'),
    util = require('util'),
    fs = require('fs'),
    reporting = require('../utilities/reporting');

module.exports.getAuthorizationSummary = function(req, res) {
    logger.debug("reporting authorizationSummary");
    reporting.authorizationSummary(req.query, function(err, flats) {
        res.json(flats);
    })
};

module.exports.getBillingDetail = function(req, res) {
    logger.debug("reporting billingDetail");
    reporting.billingDetail(req.query, function(err, flats) {
        res.json(flats);
    })
};

module.exports.getBillingSummary = function(req, res) {
    logger.debug("reporting billingSummary");
    reporting.billingSummary(req.query, function(err, flats) {
        //logger.silly(util.inspect(flats));
        res.json(flats);
    })
};

module.exports.getIndex = function(req, res) {
    logger.debug("reporting index");
    reporting.index(req.query, function(err, flats) {
        res.json(flats);
    })
};

module.exports.getMcoAuthorizationRequiringAttention = function(req, res) {
    logger.debug("reporting mcoAuthorizationRequiringAttention");
    reporting.mcoAuthorizationRequiringAttention(req.query, function(err, flats) {
        res.json(flats);
    })
};

module.exports.getMedicaidInformationRequiringAttention = function(req, res) {
    logger.debug("reporting medicaidInformationRequiringAttention");
    reporting.medicaidInformationRequiringAttention(req.query, function(err, flats) {
        res.json(flats);
    })
};

module.exports.getParentalConsentRequiringAttention = function(req, res) {
    logger.debug("reporting parentalConsentRequiringAttention");
    reporting.parentalConsentRequiringAttention(req.query, function(err, flats) {
        res.json(flats);
    })
};

module.exports.getPrescriptionsRequiringAttention = function(req, res) {
    logger.debug("reporting prescriptionsRequiringAttention");
    reporting.prescriptionsRequiringAttention(req.query, function(err, flats) {
        res.json(flats);
    })
};

module.exports.getPrint = function(req, res) {
    logger.debug("reporting print");
    logger.silly('params: ' + util.inspect(req.query));
    reporting.print(req.query, function(err, flats) {
        res.json(flats);
    })
};

module.exports.getReportingDataUtil = function(req, res) {
    logger.debug("reporting reportingDataUtil");
    reporting.reportingDataUtil(req.query, function(err, flats) {
        res.json(flats);
    })
};

module.exports.getRunner = function(req, res) {
    logger.debug("reporting runner");
    reporting.runner(req.query, function(err, flats) {
        res.json(flats);
    })
};

module.exports.getServicesBilledPerformance = function(req, res) {
    logger.debug("reporting servicesBilledPerformance");
    reporting.servicesBilledPerformance(req.query, function(err, flats) {
        res.json(flats);
    })
};

module.exports.getTherapistLicensing = function(req, res) {
    logger.debug("reporting therapistLicensing");
    reporting.therapistLicensing(req.query, function(err, flats) {
        res.json(flats);
    })
};
