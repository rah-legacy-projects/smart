var log = require('winston'),
    uuid = require('uuid'),
    redis = require('redis'),
    util = require('util'),
    fs = require('fs'),
    path = require('path'),
    logger = require('winston'),
    context = require('../models/context'),
    _ = require('lodash');

module.exports.get = function(req, res) {
    log.debug('home');
    res.render('caseload/index', {
        userId: req.session.permissionsProfile.userId
    });
};

module.exports.getLogin = function(req, res) {
    log.debug('login');
    res.render('home/login', {
        error: null
    });
};
module.exports.postLogin = function(req, res) {
    log.debug('login post');

    context.User.findOne({
        emailAddress:new RegExp(req.body.email, 'i')
    })
        .exec(function(err, user) {
            if (!!err) {
                logger.error('error logging in for user %s: %s', req.body.email, util.inspect(err));
                res.render('home/login', {
                    error: 'A critical error has occured.  Please contact technical support.'
                });
            }
            if (!user) {
                logger.warn('login for %s failed - user not found.', req.body.email);
                res.render('home/login', {
                    error: 'User name not found.  Please try again.'
                });
            } else {
                if (!user.active) {
                    logger.warn('login for %s rejected: inactive', req.body.email);
                    res.render('home/login', {
                        error: 'User has been locked.  Please contact technical support.'
                    });
                } else if (user.password !== req.body.password) {
                    logger.warn('login for %s failed - password incorrect', req.body.email);
                    res.render('home/login', {
                        error: 'User name or password not valid.  Please try again.'
                    });
                } else {
                    user.permissionsProfile(function(err, permissionsProfile) {
                        req.session.permissionsProfile = permissionsProfile;

                        var districtKeys = _.keys(permissionsProfile.districts);
                        if (districtKeys.length > 1 ||
                            (!!permissionsProfile.areas.administration &&
                                !!permissionsProfile.areas.administration.access &&
                                _.contains(permissionsProfile.areas.administration.access, 'Administer'))) {
                            //if user has access to >1 district or is an administrator, force through district selection
                            logger.silly('redirecting to district selection');
                            res.redirect('/districtSelection');
                        } else if (districtKeys.length == 0) {
                            //if the user has no access, show user as locked
                            logger.warn('login for %s failed - no district access');
                            res.render('home/login', {
                                error: 'User has been locked.  Please contact techinical support.'
                            });
                        } else {
                            //otherwise, redirect to the district user has access to
                            res.redirect('/' + districtKeys[0]);
                        }
                    });
                }
            }
        });

};

module.exports.getLogout = function(req, res) {
    req.session.permissionsProfile = null;
    res.redirect('/');
};
