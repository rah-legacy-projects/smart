var log = require('winston'),
    uuid = require('uuid'),
    redis = require('redis'),
    util = require('util'),
    fs = require('fs'),
    path = require('path'),
    logger = require('winston'),
    async = require('async'),
    context = require('../models/context'),
    _ = require('lodash'),
    dataUtil = require('../models/dataUtil');

module.exports.getMCOQualifications = function(req, res) {
    res.render('admin/mcoQualifications/mcoQualifications', {
        userId: req.params.userId
    });
};

module.exports.listMCOQualifications = function(req, res) {
    async.parallel({
        mcos: function(callback) {
            context.MCO.find({
                //district: req.district._id
            })
                .populate('rangedData')
                .lean()
                .exec(function(err, mcos) {
                    if (!!err) {
                        logger.error(err);
                    }
                    var flatMCOs = _.map(mcos, function(mco) {
                        logger.silly(mco.name);
                        var flatMCO = dataUtil.getCurrentSync(mco, 'rangedData');
                        if (!flatMCO) {
                            return null;
                        }
                        return {
                            id: flatMCO._id,
                            name: flatMCO.name,
                            shortName: flatMCO.shortName
                        }
                    });
                    if (!!err) {
                        logger.error(err);
                    }
                    callback(err, flatMCOs);
                });
        },
        user: function(callback) {
            //get user
            context.User.findOne({
                _id: new context.ObjectId(req.params.userId)
            })
                .populate({
                    path: 'mcoQualifications',
                    match: {
                        deleted: false
                    }
                })
                .exec(function(err, user) {
                    if (!!err) {
                        logger.error(err);
                    }
                    callback(err, user);
                });
        }
    }, function(err, results) {
        if (!!err) {
            logger.error(err);
        }
        logger.silly('mco qual results: ' + util.inspect(results));
        res.send({
            err: err,
            value: results
        });
    });

};

module.exports.getMCOQualification = function(req, res) {
    log.debug('getMCOQualification');

    res.render('admin/mcoQualifications/mcoQualification', {
        userId: req.params.userId,
        mcoQualificationId: req.params.id
    });
};

module.exports.getNewMCOQualification = function(req, res) {
    log.debug('getNewMcoQualification');
    res.render('admin/mcoQualifications/mcoQualification', {
        userId: req.params.userId,
        mcoQualificationId: null
    });

};

module.exports.singleMCOQualification = function(req, res) {
    
    logger.silly(util.inspect(req.params));
    async.waterfall([

            function(cb) {
                //get user
                context.User.findOne({
                    _id: new context.ObjectId(req.params.userId)

                })
                    .exec(function(err, user) {
                        if(!!err){
                            logger.error(err);
                        }
                        user.permissionsProfile(function(err, permissions) {
                            if(!!err){
                                logger.error(err);
                            }
                            cb(err, user, permissions);
                        });
                    });
            },
            function(user, permissions, cb) {
                //get the districts for the user
                //identify districts
                var districtIds = [];
                _.forIn(permissions.districts, function(value, key) {
                    districtIds.push(value._id);
                });
                //get the active MCOs for those districts
                context.MCO.find()
                    .where('district')
                    .in(districtIds)
                    .populate('rangedData')
                    .lean()
                    .exec(function(err, mcos) {
                        if (!!err) {
                            logger.error(err);
                        }
                        var flatMCOs = _.map(mcos, function(mco) {
                            logger.silly(mco.name);
                            var flatMCO = dataUtil.getCurrentSync(mco, 'rangedData');
                            if (!flatMCO) {
                                return null;
                            }
                            return {
                                id: flatMCO._id,
                                name: flatMCO.name,
                                shortName: flatMCO.shortName
                            }
                        });
                        if (!!err) {
                            logger.error(err);
                        }
                        logger.silly('calling back from mcos');
                        cb(err, flatMCOs, user);
                    });

            },
            function(mcos, user, cb) {
                if (!req.params.id) {
                    cb(null, mcos, user, null);
                } else {
                    logger.silly('about to get mco qual');
                    context.MCOQualification.findOne({
                        _id: new context.ObjectId(req.params.id)
                    })
                        .exec(function(err, mcoQualification) {
                            cb(err, mcos, user, mcoQualification);
                        });
                }
            }

        ],
        function(err, mcos, user, mcoQualification) {
            //assemble the data
            res.send({
                err: err,
                value: {
                    user: user,
                    mcos: mcos,
                    mcoQualification: mcoQualification
                }
            });
        });
};

module.exports.postMCOQualification = function(req, res) {
    log.debug('postMCOQualification');

    var tasks = {
        mcoQualification: function(callback) {
            if (!req.body.mcoQualificationId) {
                callback(null, null);
            } else {
                context.MCOQualification.findOne({
                    _id: new context.ObjectId(req.body.mcoQualificationId)
                })
                    .exec(callback);
            }
        },
        user: function(callback) {
            context.User.findOne({
                _id: new context.ObjectId(req.body.userId)
            })
                .exec(callback);
        }
    };

    async.parallel(tasks, function(err, r) {
        var isNew = false;
        if (!r.mcoQualification) {
            r.mcoQualification = new context.MCOQualification();
            isNew = true;
            r.mcoQualification.createdBy = req.session.permissionsProfile.emailAddress;
        }

        r.mcoQualification.mco = req.body.mco;
        r.mcoQualification.startDate = req.body.startDate;
        r.mcoQualification.endDate = req.body.endDate;
        r.modifiedBy = req.session.permissionsProfile.emailAddress;
        r.mcoQualification.save(function(err, smcoq) {
            if (isNew) {
                r.user.mcoQualifications.push(smcoq.id);
                r.user.modifiedBy = req.session.permissionsProfile.emailAddress;
                r.user.save(function(err, savedUser) {
                    res.send({
                        error: err
                    });
                });
            } else {
                res.send({
                    error: err
                });
            }
        });
    });
};

module.exports.postRemoveMCOQualification = function(req, res) {
    context.MCOQualification.findOne({
        _id: new context.ObjectId(req.body.mcoQualificationId)
    })
        .exec(function(err, mcoQualification) {
            mcoQualification.deleted = true;
            mcoQualification.modifiedBy = req.session.permissionsProfile.emailAddress;
            mcoQualification.save(function(err, saved) {
                res.send({
                    error: err
                });
            });
        });
};
