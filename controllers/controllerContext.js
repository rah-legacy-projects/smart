module.exports = {
    ErrorHandling: require('./errorHandling'),

    ForgotPassword: require('./forgotPassword'),
    Home: require('./home'),
    DistrictSelection: require('./districtSelection'),
    
    Caseload: require('./caseload'),
    MCOQualificationAdministration: require('./mcoQualificationAdministration'),
    PermissionAdministration: require('./permissionAdministration'),
    ProcedureCode: require('./procedureCode'),
    Schedule: require('./schedule'),
    Service: require('./service'),
    Session: require('./session'),
    Student: require('./student'),
    TherapistQualificationAdministration: require('./therapistQualificationAdministration'),
    UserAdministration: require('./userAdministration'),

    Export: require('./export'),
    Import: require('./import'),
    Template: require('./template'),
    Upload: require('./upload'),

    Reporting: require('./reporting'),
    Logging: require('./logging'),
    Root: require('./root')
};
