var logger = require('winston'),
    util = require('util'),
    logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    context = require('../models/context');


module.exports.get = function(req, res) {
    var districtKeys = _.keys(req.session.permissionsProfile.districts);

    if (districtKeys.length > 1 ||
        (!!req.session.permissionsProfile.areas.administration &&
            !!req.session.permissionsProfile.areas.administration.access &&
            _.contains(req.session.permissionsProfile.areas.administration.access, 'Administer'))) {
        //if user has access to >1 district or is an administrator, force through district selection
        res.render('districtSelection/index', {
            permissionsProfile: req.session.permissionsProfile
        });
    } else if (districtKeys.length == 0) {
        //if the user has no access, show user as locked
        res.redirect('/error/permissionDenied');
    } else {
        //otherwise, redirect to the district user has access to
        res.redirect('/' + districtKeys[0]);
    }

};
