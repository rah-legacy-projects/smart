var log = require('winston'),
    uuid = require('uuid'),
    redis = require('redis'),
    util = require('util'),
    fs = require('fs'),
    path = require('path'),
    logger = require('winston'),
    async = require('async'),
    context = require('../models/context');

module.exports.getTherapistQualifications = function(req, res) {
    res.render('admin/therapistQualifications/therapistQualifications', {
        userId: req.params.userId
    });

};

module.exports.listTherapistQualifications = function(req, res) {
    async.parallel({
        user: function(callback) {
            //get user
            context.User.findOne({
                _id: new context.ObjectId(req.params.userId)
            })
                .populate({
                    path: 'therapistQualifications',
                    match: {
                        deleted: false
                    }
                })
                .exec(callback);
        }
    }, function(err, results) {
        //assemble the data
        res.send({
            err: err,
            value: results
        });
    });

};

module.exports.getTherapistQualification = function(req, res) {
    log.debug('getTherapistQualification');

    res.render('admin/therapistQualifications/therapistQualification', {
        userId: req.params.userId,
        therapistQualificationId: req.params.id
    });
};

module.exports.getNewTherapistQualification = function(req, res) {
    log.debug('getNewMcoQualification');
    res.render('admin/therapistQualifications/therapistQualification', {
        userId: req.params.userId,
        therapistQualificationId: null
    });
};

module.exports.singleTherapistQualification = function(req, res) {
    async.parallel({
        user: function(callback) {
            //get user
            context.User.findOne({
                _id: new context.ObjectId(req.params.userId)
            })
                .exec(callback);
        },
        therapistQualification: function(callback) {
            if (!req.params.id) {
                callback(null, null);
            } else {
                context.TherapistQualification.findOne({
                    _id: new context.ObjectId(req.params.id)
                })
                    .exec(callback);
            }
        }
    }, function(err, r) {
        //assemble the data
        res.send({
            error: err,
            value: {
                user: r.user,
                therapistQualification: r.therapistQualification
            }
        });

    });

};

module.exports.postTherapistQualification = function(req, res) {
    log.debug('postTherapistQualification');

    var tasks = {
        therapistQualification: function(callback) {
            if (!req.body.therapistQualificationId) {
                callback(null, null);
            } else {
                context.TherapistQualification.findOne({
                    _id: new context.ObjectId(req.body.therapistQualificationId)
                })
                    .exec(callback);
            }
        },
        user: function(callback) {
            context.User.findOne({
                _id: new context.ObjectId(req.body.userId)
            })
                .exec(callback);
        }
    };

    async.parallel(tasks, function(err, r) {
        var isNew = false;
        if (!r.therapistQualification) {
            r.therapistQualification = new context.TherapistQualification();
            r.createdBy = req.session.permissionsProfile.emailAddress;
            isNew = true;
        }

        r.therapistQualification.startDate = req.body.startDate;
        r.therapistQualification.endDate = req.body.endDate;
        r.therapistQualification.npiNumber = req.body.npiNumber;
        r.therapistQualification.providerType = req.body.providerType;
        r.therapistQualification.licensed = req.body.licensed;
        r.modifiedBy = req.session.permissionsProfile.emailAddress;
        r.therapistQualification.save(function(err, stherapistq) {
            if ( !! err) {
                logger.error(util.inspect(err));
            }
            if (isNew) {
                r.user.therapistQualifications.push(stherapistq.id);
                r.user.save(function(err, savedUser) {
                    res.send({
                        error: err
                    });
                });
            } else {
                res.send({
                    error: err
                });
            }
        });
    });
};

module.exports.postRemoveTherapistQualification = function(req, res) {
    context.TherapistQualification.findOne({
        _id: new context.ObjectId(req.body.therapistQualificationId)
    })
        .exec(function(err, therapistQualification) {
            therapistQualification.deleted = true;
            therapistQualification.modifiedBy = req.session.permissionsProfile.emailAddress;
            therapistQualification.save(function(err, saved) {
                res.send({
                    error: err
                });
            });
        });
};
