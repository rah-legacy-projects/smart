var log = require('winston'),
    redis = require('redis'),
    util = require('util'),
    fs = require('fs'),
    path = require('path'),
    logger = require('winston'),
    _ = require('underscore'),
    async = require('async'),
    context = require('../models/context');

module.exports.getServicesByStudent = function(req, res) {
    context.Service.find({
        student: new context.ObjectId(req.params.studentId)
    })
        .populate('student')
        .populate('therapist')
        .populate({
            path: 'rangedData',
            match: {
                rangeActive: true,
                $or: [{
                    beginDate: {
                        $gt: new Date()
                    }
                }, {
                    $and: [{
                        startDate: {
                            $lt: new Date()
                        }
                    }, {
                        endDate: {
                            $gt: new Date()
                        }
                    }]
                }]

            }
        })
        .exec(function(err, services) {
            if (!!err) {
                logger.error(err);
            }
            //map records with current data to client model consumable
            var mappedSpecialties = [];
            _.each(_.filter(services, function(service) {
                return service.rangedData.length > 0;
            }), function(service) {
                _.each(service.rangedData, function(ranged) {
                    mappedSpecialties.push({
                        service: service.service,
                        startDate: ranged.startDate,
                        endDate: ranged.endDate,
                        //frequencyAndDuration: ranged.frequencyAndDuration,
                        //therapistName: service.therapist.fullName,
                        //diagnosticCode: ranged.diagnosticCode
                    });
                });
            });

            res.send({
                err: null,
                value: mappedSpecialties});
        });


};
