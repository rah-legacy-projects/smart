var logger = require('winston'),
    uuid = require('uuid'),
    redis = require('redis'),
    util = require('util'),
    fs = require('fs');

module.exports.getDistrictImport = function(req, res) {
    logger.debug('upload page for district');
    res.render('import/index', {
        type: 'district',
        displayName: 'District'
    });
};
module.exports.getSchoolImport = function(req, res) {
    logger.debug('upload page for school');
    res.render('import/index', {
        type: 'school',
        displayName: 'School'
    });
};

module.exports.getRemittanceImport = function(req, res){
    logger.debug('upload for remittance');
    res.render('import/remittance', {});
};


module.exports.getMCOImport = function(req, res) {
    logger.debug('upload page for mco');
    res.render('import/index', {
        type: 'mco',
        displayName: 'MCO'
    });
}
module.exports.getProcedureCodeImport = function(req, res) {
    logger.debug('upload page for procedure code import');
    res.render('import/index', {
        type: 'procedureCode',
        displayName: 'Procedure Code'
    });
}
module.exports.getDiagnosticCodeImport = function(req, res) {
    logger.debug('upload page for diagnostic code import');
    res.render('import/index', {
        type: 'diagnosticCode',
        displayName: 'Diagnostic Code'
    });
};
module.exports.getProcedureCodeValueImport = function(req, res) {
    logger.debug('upload for pcv');
    res.render('import/index', {
        type: 'procedureCodeValue',
        displayName: 'Procedure Code Value'
    });
}
module.exports.getStudentImport = function(req, res) {
    logger.debug('upload for student');
    res.render('import/index', {
        type: 'student',
        displayName: 'Student'
    });
};
module.exports.getServiceImport = function(req, res) {
    logger.debug('upload for service');
    res.render('import/index', {
        type: 'service',
        displayName: 'IEP Service'
    });
};

module.exports.getGDSMedicaidImport = function(req, res) {
    logger.debug('upload for gds/medicaid');
    res.render('import/index', {
        type: 'gdsMedicaid',
        displayName: 'GDS Medicaid'
    });

};
module.exports.getGDSBCBSTImport = function(req, res) {
    logger.debug('upload for gds/bcbst');
    res.render('import/index', {
        type: 'gdsBCBST',
        displayName: 'GDS BCBST'
    });

};
module.exports.getGDSUHCImport = function(req, res) {
    logger.debug('upload for gds/uhc');
    res.render('import/index', {
        type: 'gdsUHC',
        displayName: 'GDS UHC'
    });

};


module.exports.getGDSNewStudentExport = function(req, res) {
    logger.debug('gds new student export');
    res.render('export/gdsNewStudents', {
        type: 'gdsExport-noMedicaidData',
        displayName: 'New Students'
    });
};
module.exports.getGDSPreBillingExport = function(req, res) {
    logger.debug('gds existing student/pre bill export');
    res.render('export/gdsPreBilling', {
        type: 'gdsExport',
        displayName: 'Pre Billing'
    });
};
module.exports.getBilling = function(req, res) {
    logger.debug('billing file');
    res.render('export/billing', {
        type: 'billing',
        displayName: 'Billing'
    });
};

module.exports.getRemittanceError = function(req, res) {
    logger.debug('upload for remit error');
    res.render('import/index', {
        type: 'errorCodes',
        displayName: 'Remittance Error Codes'
    });

};

module.exports.getRemittanceRecords = function(req, res) {
    logger.debug('upload for remit record');
    res.render('import/index', {
        type: 'remittanceRecords',
        displayName: 'Remittance Records'
    });

};
