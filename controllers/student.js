var log = require('winston'),
    redis = require('redis'),
    util = require('util'),
    fs = require('fs'),
    path = require('path'),
    logger = require('winston'),
    _ = require('underscore'),
    async = require('async'),
    dataUtil = require('../models/dataUtil');
context = require('../models/context');

module.exports.getStudentInformation = function(req, res) {
    context.Student.findOne({
        _id: req.params.studentId
    })
        .exec(function(err, student) {
            res.render('student/index', {
                internalStudentId: req.params.studentId,
                studentId: student.studentId,
                studentName: student.fullName
            });
        });
}

module.exports.getBasicInformation = function(req, res) {
    var tasks = {
        services: function(cb) {
            context.Service.find({
                student: req.params.studentId
            })
                .populate('caseManager')
                .populate('therapist')
                .exec(function(err, services) {
                    cb(err, services);
                });
        },
        student: function(cb) {
            context.Student.getCurrentById(req.params.studentId,
                function(err, student) {
                    cb(err, student);
                });
        }
    };

    async.parallel(tasks, function(err, r) {
        var kodata = {
            dateOfBirth: r.student.dateOfBirth,
            gender: r.student.gender,
            ethnicity: r.student.ethnicity,
            studentId: r.student.studentId,
            grade: r.student.grade,
            medicaidNumber: r.student.medicaidNumber,
            schoolName: r.student.school.name,
            parentalConsent: 'todo',
            consentStartDate: 'todo',
            consentRevoked: 'todo',
        };

        if (!!r.services && r.services.length > 0) {
            if (!!r.services[0].caseManager) {
                kodata.caseManagerName = r.services[0].caseManager.fullName;
            }
            //hack: unique team members by name/speciality pairs
            var team = _.chain(r.services)
                .map(function(s) {
                    return {
                        name: s.therapist.fullName,
                        specialty: s.service
                    };
                })
                .uniq(function(item, key, i) {
                    return item.name + "|" + item.specialty;
                });

            var caseManagers = _.chain(r.services)
                .map(function(s) {
                    return {
                        name: s.caseManager.fullName,
                        specialty: 'Case Manager'
                    };
                })
                .uniq(function(item, key, i) {
                    return item.name;
                });

            kodata.iepTeamMembers = team.value()
                .concat(caseManagers.value());
        }

        res.send({
            err: null,
            value: kodata
        });

    });
};

module.exports.getIepServices = function(req, res) {
    logger.debug('get iep services for %s', req.params.studentId);
    var tasks = {
        diagnosticCodeOptions: function(callback) {
            context.Student.getCurrentById(req.params.studentId, function(err, student) {
                context.School.getCurrentById(student.school, function(err, school) {
                    //diagnostic codes that are current or future
                    //select the ICD and start/end dates
                    context.DiagnosticCode.find({
                        district: school.district
                    })
                        .populate({
                            path: 'rangedData',
                            match: {
                                rangeActive: true,
                                $or: [{
                                    startDate: {
                                        $gt: new Date()
                                    }
                                }, {
                                    //smrt-344 todo: relax start date
                                    $and: [{
                                        startDate: {
                                            $lt: new Date()
                                        }
                                    }, {
                                        endDate: {
                                            $gt: new Date()
                                        }
                                    }]
                                }]

                            }
                        })
                        .exec(function(err, diagnosticCodes) {
                            if (!!err) {
                                logger.error('dc err: ' + err);
                            }
                            var options = [];
                            _.each(diagnosticCodes, function(diagnosticCode) {
                                _.each(diagnosticCode.rangedData, function(drange) {
                                    options.push({
                                        icd: diagnosticCode.diagnosticCode,
                                        therapy: drange.therapy,
                                        startDate: drange.startDate,
                                        diagnosticCodeId: diagnosticCode.id,
                                        endDate: drange.endDate,
                                        description: drange.description,
                                        groupTherapyOrdered: drange.groupTherapyOrdered
                                    });
                                });
                            });

                            callback(null, options);
                        });
                });
            });
        },
        services: function(cb) {
            context.Service.find({
                student: new context.ObjectId(req.params.studentId),
            })
                .populate('student')
                .populate('therapist')
                .populate({
                    path: 'rangedData',
                    match: {
                        rangeActive: true,
                        $or: [{
                            beginDate: {
                                $gt: new Date()
                            }
                        }, {
                            $and: [{
                                startDate: {
                                    $lt: new Date()
                                }
                            }, {
                                endDate: {
                                    $gt: new Date()
                                }
                            }]
                        }]

                    }
                })
                .exec(function(err, services) {
                    if (!!err) {
                        logger.error(err);
                    }
                    //map records with current data to client model consumable
                    var mappedServices = [];
                    _.each(_.filter(services, function(service) {
                        return service.rangedData.length > 0;
                    }), function(service) {
                        _.each(service.rangedData, function(ranged) {
                            mappedServices.push({
                                serviceRangeId: ranged._id,
                                service: service.service,
                                startDate: ranged.startDate,
                                endDate: ranged.endDate,
                                frequencyAndDuration: ranged.frequencyAndDuration,
                                therapistName: service.therapist.fullName,
                                diagnosticCode: ranged.diagnosticCode,
                                groupTherapyOrdered: ranged.groupTherapyOrdered
                            });
                        });
                    });
                    cb(err, mappedServices);
                });
        }
    };

    async.parallel(tasks, function(err, r) {
        res.send({
            err: err,
            value: {
                diagnosticCodeOptions: r.diagnosticCodeOptions,
                services: r.services
            }
        })
    });


};

module.exports.getSessionHistory = function(req, res) {

    var tasks = {
        data: function(cb) {
            async.waterfall([
                //{{{ find and populate history
                function(cb) {
                    context.Service.find({
                        student: req.params.studentId,
                    })
                        .populate('student')
                        .populate('therapist')
                        .exec(function(err, services) {
                            //pull out the service ids
                            var serviceIds = _.map(services, function(s) {
                                return s.id;
                            });
                            cb(err, serviceIds);
                        });
                },
                function(serviceIds, cb) {
                    //find attendees for those services
                    context.Attendee.find()
                        .where('service')
                        .in(serviceIds)
                        .exec(function(err, attendees) {
                            //pull out attendee ids
                            var attendeeIds = _.map(attendees, function(a) {
                                return a.id;
                            });
                            cb(err, attendeeIds);
                        });
                },
                function(attendeeIds, cb) {
                    //find the sessions for those attendees
                    context.Session.find()
                        .elemMatch('attendees', {
                            $in: attendeeIds
                        })
                        .exec(function(err, sessions) {
                            var sessionIds = _.map(sessions, function(s) {
                                return s.id;
                            });
                            cb(err, sessionIds);
                        });
                },
                function(sessionIds, cb) {
                    //find schedules for the sessions
                    context.Schedule.find({
                        deleted: false
                    })
                        .where('session')
                        .in(sessionIds)
                        .populate('session')
                        .exec(function(err, schedules) {
                            cb(err, schedules);
                        });
                },
                function(schedules, cb) {
                    context.Schedule.populate(schedules, {
                        path: 'session.attendees',
                        model: 'Attendee'
                    }, cb);
                },
                function(schedules, cb) {
                    //populate attendee dcs
                    context.Schedule.populate(schedules, {
                        path: 'session.attendees.diagnosticCode',
                        model: 'DiagnosticCode'
                    }, cb);
                },
                function(schedules, cb) {
                    //populate attendee dcs ranges
                    context.Schedule.populate(schedules, {
                        path: 'session.attendees.diagnosticCode.rangedData',
                        model: 'DiagnosticCodeRange'
                    }, cb);
                },
                function(schedules, cb) {
                    //populate attendee service
                    context.Schedule.populate(schedules, {
                        path: 'session.attendees.service',
                        model: 'Service'
                    }, cb);
                },
                function(schedules, cb) {
                    //populate attendee service
                    context.Schedule.populate(schedules, {
                        path: 'session.attendees.service.rangedData',
                        model: 'ServiceRange'
                    }, cb);
                },
                function(schedules, cb) {
                    //populate attendee service dc
                    context.Schedule.populate(schedules, {
                        path: 'session.attendees.service.rangedData.diagnosticCode',
                        model: 'DiagnosticCode'
                    }, cb);
                },
                function(schedules, cb) {
                    //populate attendee servuce dc range
                    context.Schedule.populate(schedules, {
                        path: 'session.attendees.service.rangedData.diagnosticCode.rangedData',
                        model: 'DiagnosticCodeRange'
                    }, cb);
                },
                function(schedules, cb) {
                    //populate attendee service student
                    context.Schedule.populate(schedules, {
                        path: 'session.attendees.service.student',
                        model: 'Student'
                    }, cb);
                },
                function(schedules, cb) {
                    //populate attendee service student range
                    context.Schedule.populate(schedules, {
                        path: 'session.attendees.service.student.rangedData',
                        model: 'StudentRange'
                    }, cb);
                },
                function(schedules, cb) {
                    //populate session proc code
                    context.Schedule.populate(schedules, {
                        path: 'session.procedureCode',
                        model: 'ProcedureCode'
                    }, cb);
                },
                function(schedules, cb) {
                    //populate session proc code range
                    context.Schedule.populate(schedules, {
                        path: 'session.procedureCode.rangedData',
                        model: 'ProcedureCodeRange'
                    }, cb);
                },
                function(schedules, cb) {
                    //populate provider
                    context.Schedule.populate(schedules, {
                        path: 'session.provider',
                        model: 'User',
                    }, cb);
                },
                function(schedules, cb) {
                    //populate session proc code range
                    context.Schedule.populate(schedules, {
                        path: 'session.procedureCodeRangeAtFinalizing',
                        model: 'ProcedureCodeRange'
                    }, cb);
                },
                function(schedules, cb) {
                    //populate session proc code range
                    context.Schedule.populate(schedules, {
                        path: 'session.attendees.diagnosticCodeRangeAtFinalizing',
                        model: 'ProcedureCodeRange'
                    }, cb);
                },
                function(schedules, cb) {
                    context.Schedule.populate(schedules, {
                        path: 'session.finalizingUser',
                        model: 'User'
                    }, cb);
                },
                function(schedules, cb) {
                    //extract modifiedBy for schedules
                    //identify anyone not set
                    //find users for those email addresses
                    var emailAddresses = _.map(schedules, function(schedule) {
                        if (!!schedule.session) {
                            return schedule.session.modifiedBy;
                        }
                        return schedule.modifiedBy;
                    });

                    context.User.find()
                        .where('emailAddress')
                        .in(emailAddresses)
                        .lean()
                        .exec(function(err, modByUsers) {

                            cb(err, modByUsers, schedules);
                        });

                }
                //}}}
            ], function(err, modByUsers, schedules) {
                logger.silly('schedule flattener');
                var kodata = _.map(schedules, function(schedule) {
                    schedule = schedule.toObject();
                    var session = schedule.session;
                    var attendee = _.filter(session.attendees, function(a) {
                        return a.service.student._id.toString() == req.params.studentId;
                    })[0];
                    var service = dataUtil.getByDateRangeSync(attendee.service, 'rangedData', schedule.dateTime, schedule.dateTime);

                    var flatDiagnostic = null;
                    if (!!attendee.diagnosticCode) {
                        flatDiagnostic = dataUtil.getByDateRangeSync(
                            attendee.diagnosticCode,
                            'rangedData',
                            schedule.dateTime,
                            schedule.dateTime);
                    } else if (!!service.diagnosticCode) {
                        flatDiagnostic = dataUtil.getByDateRangeSync(
                            service.diagnosticCode,
                            'rangedData',
                            schedule.dateTime,
                            schedule.dateTime);

                    }

                    var procedureCode = null;
                    if (!!session.procedureCode) {
                        procedureCode = dataUtil.getByDateRangeSync(
                            session.procedureCode,
                            'rangedData',
                            schedule.dateTime,
                            schedule.dateTime);
                    }

                    var procedureCodeDisplay = 'No Procedure Code';
                    if (!!procedureCode) {
                        procedureCodeDisplay = procedureCode.procedureCode + ' - ' + procedureCode.description;
                    }
                    if (!!session.procedureCodeRangeAtFinalizing) {
                        procedureCodeDisplay = procedureCode.procedureCode + ' - ' + session.procedureCodeRangeAtFinalizing.description;
                    }

                    var diagnosticCodeDisplay = 'No Diagnostic Code';
                    if (!!flatDiagnostic) {
                        diagnosticCodeDisplay = flatDiagnostic.diagnosticCode + ' - ' + flatDiagnostic.description;
                    }
                    if (!!attendee.diagnosticCodeRangeAtFinalizing) {
                        diagnosticCodeDisplay = flatDiagnostic.diagnosticCode + ' - ' + attendee.diagnosticCodeRangeAtFinalizing.description;
                    }

                    var modByUser = _.find(modByUsers, function(u) {
                        return u.emailAddress == (schedule.modifiedBy || schedule.session.modifiedBy);
                    });

                    //provider then last mod by then finalizer
                    var therapist = session.provider;
                    if (!!modByUser) {
                        therapist = modByUser;
                    }
                    if (!!session.finalizingUser) {
                        therapist = session.finalizingUser;
                    }

                    var retVal = {
                        therapistName: therapist.firstName + ' ' + therapist.lastName,
                        therapy: session.serviceType,
                        dateTime: schedule.dateTime,
                        duration: schedule.duration,
                        sessionNotes: session.sessionNotes,
                        individualNote: attendee.individualNote,
                        absent: attendee.absent,
                        absenceReason: attendee.absenceReason,
                        procedureCode: procedureCodeDisplay,
                        diagnosticCode: diagnosticCodeDisplay,
                        finalizingUserId: !session.finalizingUser ? null : session.finalizingUser._id,
                        isBilled: !!session.billedDate,
                        scheduleId: schedule._id,
                        isGroupSession: session.attendees.length > 1
                    };

                    return retVal;

                });

                cb(err, kodata);
            });
        }
    };

    async.parallel(tasks, function(err, r) {
        res.send({
            err: err,
            value: r.data
        });
    });
};

module.exports.getProgressNotes = function(req, res) {
    logger.debug('get progress notes for %s', req.params.studentId);
    context.Student.findOne({
        _id: req.params.studentId
    })
        .populate({
            path: 'progressNotes',
            match: {
                deleted: false
            }
        })
        .exec(function(err, student) {
            if (!student) {
                res.send({
                    err: 'Student not found.',
                    value: null
                });
                return;
            }
            context.Student.populate(student, {
                path: 'progressNotes.notedBy',
                model: context.User
            }, function(err, pop) {



                var mapped = _.map(student.progressNotes, function(pn) {
                    return {
                        progressNoteId: pn.id,
                        note: pn.note,
                        specialty: pn.specialty,
                        notedBy: pn.notedBy.fullName,
                        addedDate: pn.addedDate
                    };
                });

                res.send({
                    err: err,
                    value: mapped
                });
            });
        });
};

module.exports.getAdditionalInfo = function(req, res) {
    logger.debug('additional info request for ' + req.params.studentId);
    var tasks = {
        student: function(callback) {
            context.Student.findOne({
                _id: req.params.studentId
            })
                .populate({
                    path: 'prescriptions',
                    match: {
                        rangeActive: true
                    }
                })
                .populate({
                    path: 'mcoInformation',
                    match: {
                        rangeActive: true
                    }
                })
                .populate({
                    path: 'parentalConsents',
                    match: {
                        rangeActive: true
                    }
                })
                .exec(function(err, student) {
                    callback(err, student);
                });
        },
        gdsData: function(cb) {
            //retrieve gds data by student id
            //populate mco
            //populate mco range
            //collapse down to relevant info
            async.waterfall([

                function(cb) {
                    context.GDS
                        .findOne({
                            student: context.ObjectId(req.params.studentId)
                        })
                        .exec(function(err, gds) {
                            if (!!err) {
                                logger.error('problem getting gds for student: ' + err);
                            }
                            cb(err, gds);
                        });
                },
                function(gds, cb) {
                    context.GDS.populate(gds, {
                        path: 'rangedData',
                        model: 'GDSRange',
                        match: {
                            rangeActive: true
                        }
                    }, function(err, gds) {
                        cb(err, gds);
                    });
                },
                function(gds, cb) {
                    context.GDS.populate(gds, {
                        path: 'rangedData.mco',
                        model: 'MCO'
                    }, function(err, gds) {
                        cb(err, gds);
                    });
                },
                function(gds, cb) {
                    context.GDS.populate(gds, {
                        path: 'rangedData.mco.rangedData',
                        model: 'MCORange',
                        match: {
                            rangeActive: true
                        }
                    }, function(err, gds) {
                        if (!!gds) {
                            cb(err, gds.toObject());
                        } else {
                            cb(err, null);
                        }
                    });
                },
                function(gdsData, cb) {
                    //pull in the tenncare data
                    context.GDSTennCare
                        .findOne({
                            student: context.ObjectId(req.params.studentId)
                        })
                        .populate({
                            path: 'rangedData',
                            match: {
                                rangeActive: true
                            }
                        })
                        .exec(function(err, tcData) {
                            if (!!err) {
                                logger.error('problem getting gds for student: ' + err);
                            }
                            if (!!tcData) {
                                cb(err, gdsData, tcData.toObject());
                            } else {
                                cb(err, gdsData, null);
                            }
                        });
                },
                function(gdsData, tcData, cb) {
                    //for each active gds range,
                    //get the flat gds record for that range
                    //get the flat tc record for that range
                    //assign mcaid num


                    if (!!gdsData) {
                        var validGDSData = _.map(gdsData.rangedData, function(gdsRange) {
                            var flatGds = dataUtil.getByDateRangeSync(gdsData, 'rangedData', gdsRange.startDate, gdsRange.endDate);
                            if (!!gdsRange.mco) {
                                flatGds.mco = dataUtil.getByDateRangeSync(gdsRange.mco, 'rangedData', gdsRange.startDate, gdsRange.endDate);
                            }
                            var flatTc = null;
                            if (tcData) {
                                flatTc = dataUtil.getByDateRangeSync(tcData, 'rangedData', gdsRange.startDate, gdsRange.endDate);
                            }

                            return {
                                startDate: flatGds.startDate,
                                endDate: flatGds.endDate,
                                mcaidNum: !flatTc ? '' : flatTc.mcaidNum,
                                planBeneNum: flatGds.planBeneNum,
                                mcoName: !flatGds.mco ? '' : flatGds.mco.name
                            };
                        });


                        cb(null, validGDSData);
                    } else {
                        cb(null, null);
                    }
                }
            ], function(err, gdsdata) {
                if (!!err) {
                    logger.silly('error hydrating: ' + err);
                }
                logger.silly('gds info: ' + util.inspect(gdsdata));
                cb(err, gdsdata || {});
            });
        },
        school: function(callback) {
            context.Student.getCurrentById(req.params.studentId, function(err, student) {
                context.School.getCurrentById(student.school, function(err, school) {
                    callback(err, school);
                });
            });
        },
        district: function(callback) {
            context.Student.getCurrentById(req.params.studentId, function(err, student) {
                context.School.getCurrentById(student.school, function(err, school) {
                    context.District.getCurrentById(school.district, function(err, district) {
                        callback(err, school);
                    });
                });
            });
        },
        specialities: function(callback) {
            //hack: use distinct specialities from services + procedure codes
            //todo: lift specialities into their own enumeration
            var specialityTasks = {
                services: function(scb) {
                    context.Service.find()
                        .select('service')
                        .exec(function(err, services) {
                            var cbval = _.uniq(_.pluck(services, 'service'));
                            scb(err, cbval);
                        });
                },
                procedureCodes: function(scb) {
                    context.ProcedureCode.find()
                        .select('serviceType')
                        .exec(function(err, procedureCodes) {
                            scb(err, _.uniq(_.pluck(procedureCodes, 'serviceType')));
                        });
                }
            };

            async.parallel(specialityTasks, function(err, r) {
                var allSpecialities = [];
                allSpecialities.push.apply(allSpecialities, r.services);
                allSpecialities.push.apply(allSpecialities, r.procedureCodes);
                callback(err, _.uniq(allSpecialities));
            });
        }
    };

    async.parallel(tasks, function(err, r) {
        context.DiagnosticCode.find({
            district: r.district._id
        })
            .exec(function(err, diagnosticCodes) {
                var mappedDiagnosticCodes = _.map(diagnosticCodes, function(dc) {
                    return {
                        diagnosticCodeId: dc._id,
                        diagnosticCode: dc.diagnosticCode
                    };
                });
                var kodata = {
                    dateOfBirth: r.student.dateOfBirth,
                    gender: r.student.gender,
                    ethnicity: r.student.ethnicity,
                    studentId: r.student.studentId,
                    id: r.student._id,
                    parentalConsents: r.student.parentalConsents,
                    diagnosticCodes: r.student.diagnosticCodes,
                    diagnosticCodeOptions: mappedDiagnosticCodes,
                    specialities: r.specialities,
                    prescriptions: r.student.prescriptions,
                    studentMCOInfos: r.student.mcoInformation,
                    gdsData: r.gdsData
                    /*medicaidStartDate: r.gdsData.startDate,
                    medicaidEndDate: r.gdsData.endDate
                    medicaidNumber: r.gdsData.mcaidNum,
                    planNumber: r.gdsData.planBeneNum,
                    mcoName: !r.gdsData.mco ? null : r.gdsData.mco.name,*/

                };
                res.send({
                    err: err,
                    value: kodata
                });

            });

    });

}

module.exports.saveServiceRange = function(req, res) {
    logger.debug('saving service range for ' + req.params.studentId);
    logger.silly('group therapy ordered: ' + req.body.groupTherapyOrdered);

    context.ranged.Service.findOne({
        _id: req.body.serviceRangeId
    })
        .exec(function(err, serviceRange) {
            serviceRange.diagnosticCode = req.body.diagnosticCode;
            serviceRange.groupTherapyOrdered = req.body.groupTherapyOrdered;
            serviceRange.modifiedBy = req.session.permissionsProfile.emailAddress;
            serviceRange.save(function(err) {
                res.send({
                    err: err,
                    value: true
                });
            });
        });


};

module.exports.saveParentalConsent = function(req, res) {
    var tasks = {
        parentalConsent: function(callback) {
            context.ParentalConsent.findOne({
                _id: req.body.parentalConsentId
            })
                .exec(function(err, parentalConsent) {
                    var wasNew = false;
                    if (!parentalConsent) {
                        wasNew = true;
                        parentalConsent = new context.ParentalConsent();
                        parentalConsent.createdBy = req.session.permissionsProfile.emailAddress;
                    }

                    parentalConsent.toShareIEP = req.body.toShareIEP;
                    parentalConsent.toShareIEPDenied = req.body.toShareIEPDenied;
                    parentalConsent.toBillMedicaid = req.body.toBillMedicaid;
                    parentalConsent.toBillMedicaidDenied = req.body.toBillMedicaidDenied;
                    parentalConsent.toTreat = req.body.toTreat;
                    parentalConsent.toTreatDenied = req.body.toTreatDenied;
                    parentalConsent.modifiedBy = req.session.permissionsProfile.emailAddress;

                    parentalConsent.save(function(err, saved) {
                        saved.wasNew = wasNew;
                        callback(err, saved);
                    });
                });
        },
        student: function(callback) {
            context.Student.findOne({
                _id: req.params.studentId
            })
                .exec(function(err, student) {
                    callback(err, student);
                });
        }
    };

    async.parallel(tasks, function(err, r) {
        if (r.parentalConsent.wasNew) {
            r.student.parentalConsents.push(r.parentalConsent.id);
        }
        r.student.modifiedBy = req.session.permissionsProfile.emailAddress;

        r.student.save(function(err, savedStudent) {
            res.send({
                err: err,
                value: {
                    parentalConsentId: r.parentalConsent.id
                }
            });
        });

    });
};

module.exports.saveProgressNote = function(req, res) {
    var tasks = {
        student: function(callback) {
            context.Student.findOne({
                _id: req.params.studentId
            })
                .exec(function(err, student) {
                    if (!!err) {
                        logger.error(err);
                    }
                    callback(err, student);
                });
        },
        user: function(callback) {
            context.User.findOne({
                _id: req.session.permissionsProfile.userId
            })
                .exec(function(err, user) {
                    if (!!err) {
                        logger.error(err);
                    }
                    callback(err, user);
                });
        }
    };

    async.parallel(tasks, function(err, r) {
        context.ProgressNote.findOne({
            _id: req.body.progressNoteId
        })
            .exec(function(err, progressNote) {
                var isnew = false;
                if (!progressNote) {
                    progressNote = new context.ProgressNote();
                    progressNote.addedDate = Date.now();
                    isnew = true;
                    progressNote.createdBy = req.session.permissionsProfile.emailAddress;
                }
                progressNote.note = req.body.note;
                progressNote.student = r.student._id;
                progressNote.specialty = req.body.specialty;
                progressNote.notedBy = req.session.permissionsProfile.userId;
                progressNote.modifiedBy = req.session.permissionsProfile.emailAddress;

                progressNote.save(function(err) {
                    if (!!err) {
                        logger.error(err);
                    }


                    if (isnew) {
                        r.student.progressNotes.push(progressNote._id);
                    }
                    r.student.modifiedBy = req.session.permissionsProfile.emailAddress;
                    r.student.save(function(err, savedStudent) {
                        var savedProgNote = {
                            addedDate: progressNote.addedDate,
                            notedBy: r.user.fullName,
                            note: progressNote.note,
                            progressNoteId: progressNote.id
                        };
                        res.send({
                            err: err,
                            value: savedProgNote
                        });
                    });
                });
            });


    });
};

module.exports.savePrescription = function(req, res) {
    var tasks = {
        prescription: function(callback) {
            context.Prescription.findOne({
                _id: req.body.prescriptionId
            })
                .exec(function(err, prescription) {
                    var wasNew = false;
                    if (!prescription) {
                        wasNew = true;
                        prescription = new context.Prescription();
                        prescription.createdBy = req.session.permissionsProfile.emailAddress;
                    }
                    prescription.serviceType = req.body.serviceType;
                    prescription.doctorName = req.body.doctorName;
                    prescription.endDate = req.body.endDate;
                    prescription.startDate = req.body.startDate;
                    prescription.modifiedBy = req.session.permissionsProfile.emailAddress;
                    prescription.save(function(err, saved) {
                        saved.wasNew = wasNew;
                        callback(err, saved);
                    });
                });
        },
        student: function(callback) {
            context.Student.findOne({
                _id: req.params.studentId
            })
                .exec(function(err, student) {
                    callback(err, student);
                });
        }
    };

    async.parallel(tasks, function(err, r) {
        if (r.prescription.wasNew) {
            r.student.prescriptions.push(r.prescription.id);
        }

        r.student.modifiedBy = req.session.permissionsProfile.emailAddress;
        r.student.save(function(err, savedStudent) {
            res.send({
                err: err,
                value: {
                    prescriptionId: r.prescription.id
                }
            });
        });

    });
};

module.exports.saveStudentMCOInfo = function(req, res) {
    var tasks = {
        studentMcoInfo: function(callback) {
            context.ranged.StudentMCOInformation.findOne({
                _id: new context.ObjectId(req.body.studentMCOInfoId)
            })
                .exec(function(err, studentMcoInfo) {
                    var wasNew = false;
                    if (!studentMcoInfo) {
                        wasNew = true;
                        studentMcoInfo = new context.ranged.StudentMCOInformation();
                        studentMcoInfo.createdBy = req.session.permissionsProfile.emailAddress;
                    }

                    studentMcoInfo.specialty = req.body.specialty;
                    studentMcoInfo.numberOfSessions = req.body.numberOfSessions;
                    studentMcoInfo.approvalToTreat = req.body.approvalToTreat;
                    studentMcoInfo.startDate = req.body.startDate;
                    studentMcoInfo.endDate = req.body.endDate;

                    studentMcoInfo.modifiedBy = req.session.permissionsProfile.emailAddress;
                    studentMcoInfo.save(function(err, saved) {
                        saved.wasNew = wasNew;
                        callback(err, saved);
                    });
                });
        },
        student: function(callback) {
            context.Student.findOne({
                _id: req.params.studentId
            })
                .exec(function(err, student) {
                    callback(err, student);
                });
        }
    };

    async.parallel(tasks, function(err, r) {
        if (r.studentMcoInfo.wasNew) {
            r.student.mcoInformation.push(r.studentMcoInfo.id);
        }

        r.student.modifiedBy = req.session.permissionsProfile.emailAddress;
        r.student.save(function(err, savedStudent) {
            res.send({
                err: err,
                value: {
                    studentMcoInfoId: r.studentMcoInfo.id
                }
            });
        });

    });
};

module.exports.removeParentalConsent = function(req, res) {
    context.ParentalConsent.findOne({
        _id: req.params.parentalConsentId
    })
        .exec(function(err, parentalConsent) {
            parentalConsent.rangeActive = false;
            parentalConsent.modifiedBy = req.session.permissionsProfile.emailAddress;
            parentalConsent.save(function(err) {
                res.send({
                    err: err
                });
            });
        });
};

module.exports.removePrescription = function(req, res) {
    context.Prescription.findOne({
        _id: req.params.prescriptionId
    })
        .exec(function(err, prescription) {
            prescription.rangeActive = false;
            prescription.modifiedBy = req.session.permissionsProfile.emailAddress;
            prescription.save(function(err) {
                res.send({
                    err: err
                });
            });
        });
};

module.exports.removeStudentMCOInfo = function(req, res) {
    context.ranged.StudentMCOInformation.findOne({
        _id: req.params.studentMCOInfoId
    })
        .exec(function(err, smco) {
            smco.rangeActive = false;
            smco.modifiedBy = req.session.permissionsProfile.emailAddress;
            smco.save(function(err) {
                res.send({
                    err: err
                });
            });
        });
};

module.exports.removeProgressNote = function(req, res) {
    context.ProgressNote.findOne({
        _id: req.params.progressNoteId
    })
        .exec(function(err, pn) {
            pn.deleted = true;;
            pn.modifiedBy = req.session.permissionsProfile.emailAddress;
            pn.save(function(err) {
                res.send({
                    err: err
                });
            });
        });
};
