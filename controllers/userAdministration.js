var log = require('winston'),
    uuid = require('uuid'),
    redis = require('redis'),
    util = require('util'),
    fs = require('fs'),
    path = require('path'),
    logger = require('winston'),
    async = require('async'),
    context = require('../models/context'),
    _ = require('underscore');

var localstore = redis.createClient();

module.exports.getUsers = function(req, res) {
    log.debug('getUsers');
    //todo: need to cull the users collection into just users, and only relevant info for the table
    context.User.find({}, function(err, users) {
        res.render('admin/users/users', {
            users: users
        });
    });
};
module.exports.getUser = function(req, res) {
    res.render('admin/users/user', {
        userId: req.params.id
    });
};
module.exports.getNewUser = function(req, res) {
    res.render('admin/users/user', {
        userId: null
    });
}

module.exports.singleUser = function(req, res) {
    log.debug('getUser');

    var tasks = {
        user: function(callback) {
            if (!req.params.id) {
                var user = new context.User();
                callback(null, user);

            } else {

                context.User.findOne({
                    _id: (new context.ObjectId(req.params.id))
                })
                    .exec(function(err, user) {
                        callback(err, user);
                    });
            }

        },
        admin: function(callback) {
            context.Area.findOne({
                name: /administration/i
            })
                .exec(function(err, area) {
                    callback(err, area);
                });
        }
    }

    async.parallel(tasks, function(err, r) {
        var user = {
            firstName: r.user.firstName,
            lastName: r.user.lastName,
            middleName: r.user.middleName,
            emailAddress: r.user.emailAddress,
            id: r.user.id,
            isNew: !req.params.id,
            isAdministrator: false,
            active: r.user.active,
            ssn: r.user.ssn
        };


        if (_.contains(r.admin.getAccess(r.user), 'Administer')) {
            user.isAdministrator = true;
        }
        res.send({
            error: err,
            value: user
        });

    });

};

module.exports.postUser = function(req, res) {
    log.debug('postUser');

    var saveTasks = {
        user: function(callback) {
            context.User.findOne({
                _id: req.body.id
            })
                .exec(function(err, user) {
                    if (!!err) {
                        logger.error(util.inspect(err));
                    }
                    if (!user) {
                        user = new context.User();
                        user.createdBy = req.session.permissionsProfile.emailAddress;
                    }
                    user.firstName = req.body.firstName;
                    user.lastName = req.body.lastName;
                    user.emailAddress = req.body.emailAddress;
                    user.active = req.body.active;
                    user.ssn = req.body.cleanSSN;
                    if (!!req.body.newPassword && req.body.newPassword !== '') {
                        user.password = req.body.newPassword;
                    }
                    user.modifiedBy = req.session.permissionsProfile.emailAddress;
                    user.save(function(err) {
                        if (!!err) {
                            logger.error('error with user: ' + util.inspect(err));
                        }
                        callback(err, user);
                    });


                });
        },
        adminArea: function(callback) {
            context.Area.findOne({
                name: /administration/i
            })
                .exec(function(err, area) {
                    if (!!err) {
                        logger.error(err);
                    }
                    callback(err, area);
                });
        }
    };

    async.series(saveTasks, function(err, r) {
        if (/true/i.test(req.body.isAdministrator)) {
            r.adminArea.setAccess(r.user, ['Administer']);
            r.user.modifiedBy = req.session.permissionsProfile.emailAddress;
            r.user.save(function(err, saved) {
                res.send({
                    error: err,
                    value: saved
                });
            });
        } else {
            r.adminArea.setAccess(r.user, []);
            r.user.modifiedBy = req.session.permissionsProfile.emailAddress
            r.user.save(function(err, saved) {
                res.send({
                    error: err,
                    value: r.user
                });
            });
        }

    });


};
