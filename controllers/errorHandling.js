var logger = require('winston'),
    uuid = require('uuid'),
    redis = require('redis'),
    util = require('util'),
    fs = require('fs');

module.exports.districtNotFound = function(req, res) {
    res.render('errorHandling/districtNotFound', {
        notFoundDistrict: req.notFoundDistrict
    });

};

module.exports.getPermissionDenied = function(req,res){
    res.render('errorHandling/permissionDenied', {
        district: req.district
    });
};

module.exports.postPermissionDenied = function(req, res){
    //todo
};
