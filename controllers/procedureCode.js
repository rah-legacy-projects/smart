var log = require('winston'),
    uuid = require('uuid'),
    redis = require('redis'),
    util = require('util'),
    fs = require('fs'),
    path = require('path'),
    logger = require('winston'),
    async = require('async'),
    _ = require('underscore'),
    context = require('../models/context');

module.exports.listProcedureCodes = function(req, res) {


    var procedureCodeCallback = function(err, procedureCodes) {
        //flatten it down
        res.send({
            err: err,
            value: _.map(procedureCodes, function(pc) {
                return {
                    procedureCode: pc.procedureCode,
                    description: pc.description,
                    procedureCodeId: pc._id
                };
            })
        });
    };

    if ( !! req.body.scheduleId) {
        context.Schedule.findOne({
            _id: req.body.scheduleId
        })
            .populate('services')
            .exec(function(err, schedule) {

                //hack:using the top service, pull the procedure codes
                var servicename = schedule.services[0].service;
                //populate the school
                
                context.Student.getByDateById(req.body.date || schedule.dateTime, schedule.services[0].student, function(err, student){
                    if(!!student){
                    context.School.getByDateById(req.body.date || schedule.dateTime, student.school, function(err, school){
                        context.ProcedureCode.getByServiceType(req.body.date || schedule.dateTime, servicename, school.district, procedureCodeCallback);
                    });}else{
                        procedureCodeCallback();
                    }
                });
            });
    } else if ( !! req.body.serviceId) {
        throw ('do requests for proc code by service id happen?');
        context.Service.findOne({
            _id: req.body.serviceId
        })
            .exec(function(err, service) {
                context.Student.getCurrentById(service.student, function(err, student){
                    context.Schoo.getCurrentById(student.school, function(err, school){
                        context.ProcedureCode.getByServiceType(servicename, school.district, procedureCodeCallback);
                    });
                });
            });
    }
    /*else if ( !! req.body.sessionId) {
        context.Session.findOne({
            _id: req.body.sessionId
        })
            .exec(function(err, session) {
                context.ProcedureCode.getByServiceType(session.service, procedurecodeCallback);
            });
    } */
    else {
        //hack: pass in a cheater regex to get any current procedure code model
        logger.warn('districtless procedure codes are deprecated!');
        context.ProcedureCode.getByServiceType('.*', procedureCodeCallback);
    }
};
