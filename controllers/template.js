var log = require('winston'),
    uuid = require('uuid'),
    redis = require('redis'),
    util = require('util'),
    fs = require('fs');

var localstore = redis.createClient();
module.exports.get = function(req, res) {
    log.debug('getting template: ' + req.query.templateToken);

    //assemble the extension,
    var extension = req.query.extension;
    if (!(/^\./.test(extension))) {
        extension = "." + extension;
    }
    //and the prettyname
    var def = new require('../importDefinitions/' + req.query.typeName)();
    var prettyName =def.sheet.sheetName + extension;



    localstore.get(req.query.templateToken, function(err, docLocation) {;
        //download the document location (guidname) as the pretty name
        res.download(docLocation, prettyName, function(err) {
            if (err) {
                log.error('template download error: ' + util.inspect(err));
                //TODO: check res.headerSent
            }
            //delete the document
            fs.unlink(docLocation);
        })
    });
};
