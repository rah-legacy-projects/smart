var log = require('winston'),
    uuid = require('uuid'),
    redis = require('redis'),
    util = require('util'),
    fs = require('fs'),
    path = require('path'),
    logger = require('winston'),
    async = require('async'),
    context = require('../models/context');

var localstore = redis.createClient();

module.exports.get = function(req, res) {
    log.debug('get reset password');
    res.render('resetPassword/index', {
        success: false,
        error: null
    });

};

module.exports.post = function(req, res) {
    context.User.findOne({
        emailAddress: req.body.email,
        active: true
    })
        .exec(function(err, user) {
            if ( !! err) {
                res.render('resetPassword/index', {
                    error: "Error loading user.  Please try another.",
                    success: false
                });
            } else if (!user) {
                res.render('resetPassword/index', {
                    error: "User not found.",
                    success: false
                });
            } else {
                var forgotPassword = new context.ForgotPassword({
                    user: user._id,
                });
                forgotPassword.createdBy = user.emailAddress;
                forgotPassword.modifiedBy = user.emailAddress;
                forgotPassword.save(function(err, savedfpm) {
                    res.render('resetPassword/index', {
                        success: true,
                        slug: savedfpm._id
                    });
                });
            }
        });
};

module.exports.getConfirmation = function(req, res) {
    log.debug('get reset password confirmation');
    context.ForgotPassword.findOne({
        _id: new context.ObjectId(req.params.slug)
    })
        .exec(function(err, fpm) {
            if ( !! err) {
                logger.error(util.inspect(err));
                res.render('resetPassword/confirmation', {
                    data: {
                        error: 'Password reset could not be found.  PLease contact technical support.'
                    }
                });
            } else if (!fpm) {
                res.render('resetPassword/confirmation', {
                    data: {
                        error: 'Password reset could not be found.'
                    }
                });
            } else if (fpm.claimed) {
                res.render('resetPassword/confirmation', {
                    data: {
                        error: 'This password reset link has already been used.'
                    }
                });
            } else if (fpm.expiry < (Date.now())) {
                res.render('resetPassword/confirmation', {
                    data: {
                        error: 'This password reset link has expired.'
                    }
                });
            } else {
                res.render('resetPassword/confirmation', {
                    data: {
                        slug: req.params.slug
                    }
                });
            }
        });
};

module.exports.postConfirmation = function(req, res) {
    context.ForgotPassword.findOne({
        _id: new context.ObjectId(req.params.slug)
    })
        .populate('user')
        .exec(function(err, fpm) {
            fpm.user.password = req.body.newPassword;
            fpm.claimed = true;
            async.parallel({
                forgotPassword: function(cb) {
                    fpm.modifiedBy = fpm.user.emailAddress;
                    fpm.save(function(err, saved) {
                        if ( !! err) {
                            logger.error('error saving forgot password: ' + util.inspect(err));
                        }
                        cb(err, saved);
                    });
                },
                user: function(cb) {
                    fpm.modifiedBy = fpm.user.emailAddress;
                    fpm.user.save(function(err, saved) {
                        if ( !! err) {
                            logger.error('error saving user: ' + util.inspect(err));
                        }
                        cb(err, saved);
                    });
                }
            }, function(err, r) {
                res.send({});
            });
        });
};
