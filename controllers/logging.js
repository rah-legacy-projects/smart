var config = require('../config'),
    winston = require('winston'),
    util = require('util'),
    moment = require('moment'),
    _ = require('lodash'),
    logger = config.logger;

module.exports.getLogs = function(req, res) {
    res.render('logging/index', {});
};

module.exports.postLogs = function(req, res) {
    console.log('post logs');
    console.log(util.inspect(req.body.search));
    var options = {
        from: moment(req.body.search.start).startOf('day').toDate(),
        until: moment(req.body.search.end).endOf('day').toDate(),
        //limit: req.body.displayLength,
        //start: req.body.displayStart,
        order: 'asc'
    };
    winston.query(options, function(err, results) {
        if (err) {
            throw err;
        }


        var totalRecords = results.mongodb.length;
        var results = results.mongodb;
    
        if(!(/^\s*$/.test((req.body.search.level || '')))){
            results = _.filter(results, function(r){
                var result = (new RegExp(req.body.search.level, 'i')).test(r.level);
                return result;
            });
        }

        if(!(/^\s*$/.test((req.body.search.hostname || '')))){
            results = _.filter(results, function(r){
                var result = (new RegExp(req.body.search.hostname, 'i')).test(r.hostname);
                return result;
            });
        }

        if(!(/^\s*$/.test((req.body.search.message || '')))){
            results = _.filter(results, function(r){
                var result = (new RegExp(req.body.search.message, 'i')).test(r.message);
                return result;
            });
        }

        var sliced = results.slice(req.body.displayStart, (req.body.displayStart + req.body.displayLength) - 1);

        res.send({
            echo: req.body.echo,
            filteredRecords: sliced.length,
            totalRecords: totalRecords,
            recordSet: sliced
        });
    });
};
