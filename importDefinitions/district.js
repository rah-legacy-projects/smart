var logger = require('winston'),
    mongoose = require('mongoose'),
    context = require('../models/context'),
    async = require('async'),
    moment = require('moment-timezone'),
    _ = require('lodash'),
    util = require('util'),
    dataUtil = require('../models/dataUtil');

var DistrictImport = function(actor, clientTimezone) {
    var self = this;
    self.actor = actor || "[import]";
    self.clientTimezone = clientTimezone;
    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'District Import',
        className: 'district',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'DistCode',
            propertyName: 'districtCode',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for District Code needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: "[rules needed for district code]"
            }]
        }, {
            name: 'Name',
            propertyName: 'name',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Name]'
            }]
        }, {
            name: 'Address Line 1',
            propertyName: 'addressLine1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Address Line 1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Address Line 1]'
            }]
        }, {
            name: 'Address Line 2',
            propertyName: 'addressLine2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Address Line 2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Address Line 2]'
            }]
        }, {
            name: 'City',
            propertyName: 'city',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for City needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for City]'
            }]
        }, {
            name: 'State',
            propertyName: 'state',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for State needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for State]'
            }]
        }, {
            name: 'Zip',
            propertyName: 'zip',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Zip needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Zip]'
            }]
        }, {
            name: 'Telephone Number',
            propertyName: 'telephoneNumber',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Telephone Number needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Telephone Number]'
            }]
        }, {
            name: 'Health Plan ID',
            propertyName: 'healthPlanId',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Health Plan ID needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Health Plan ID]'
            }]
        }, {
            name: 'Start Date',
            propertyName: 'startDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Start Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))?$/,
                errorMessage: '[rules needed for Start Date]'
            }]
        }, {
            name: 'End Date',
            propertyName: 'endDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for End Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))$/,
                errorMessage: '[rules needed for End Date]'
            }]
        }, {
            name: 'Claiming Start Date',
            propertyName: 'claimingStartDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Claiming Start Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))$/,
                errorMessage: '[rules needed for Claiming Start Date]'
            }]
        }, {
            name: 'Billing Name',
            propertyName: 'billingName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Billing Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Billing Name]'
            }]
        }, {
            name: 'Billing Address 1',
            propertyName: 'billingAddress1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Billing Address 1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Billing Address 1]'
            }]
        }, {
            name: 'Billing Address 2',
            propertyName: 'billingAddress2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Billing Address 2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Billing Address 2]'
            }]
        }, {
            name: 'Billing City',
            propertyName: 'billingCity',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Billing City needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Billing City]'
            }]
        }, {
            name: 'Billing State',
            propertyName: 'billingState',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Billing State needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Billing State]'
            }]
        }, {
            name: 'Billing Zip',
            propertyName: 'billingZip',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Billing Zip needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Billing Zip]'
            }]
        }, {
            name: 'Billing Tax ID',
            propertyName: 'billingTaxId',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Billing Tax ID needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Billing Tax ID]'
            }]
        }, {
            name: 'Biller ID',
            propertyName: 'billingBillerId',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Billing Biller ID needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Billing Biller ID]'
            }]
        }, {
            name: 'Billing Phone',
            propertyName: 'billingPhone',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Billing Phone needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Billing Phone]'
            }]
        }, {
            name: 'NPI Number',
            propertyName: 'npiNumber',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for NPI Number needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\w+/,
                errorMessage: '[rules needed for NPI Number]'
            }]
        }]
    };

    self.diff = function(value, index, cb) {
        context.District.findOne({
            districtCode: value.districtCode
        })
            .populate('rangedData')
            .lean()
            .exec(function(err, district) {
                if (!!err) {
                    logger.error('district import error: %s', err);
                    self.emit('diff error', {});
                    cb(err, false);
                } else if (!district) {
                    cb(null, null);
                } else {

                    var startMoment = moment.tz(value.startDate, self.clientTimezone)
                        .startOf('day'),
                        endMoment = moment.tz(value.endDate, self.clientTimezone)
                        .endOf('day');

                    logger.silly('diff range: ' + startMoment.toString() + ' - ' + endMoment.toString());


                    dataUtil.getByDateRange(
                        district,
                        'rangedData',
                        startMoment,
                        endMoment,
                        function(err, flat) {
                            if (!flat) {
                                cb(err, null);
                            } else {
                                cb(err, {
                                    districtCode: flat.districtCode,
                                    name: flat.name,
                                    addressLine1: flat.address1,
                                    addressLine2: flat.address2,
                                    city: flat.city,
                                    state: flat.state,
                                    zip: flat.zip,
                                    telephoneNumber: flat.telephone,
                                    healthPlanId: flat.healthPlanId,
                                    startDate: flat.startDate,
                                    endDate: flat.endDate,
                                    claimingStartDate: flat.claimingStartDate,
                                    billingName: flat.billingInfo.name,
                                    billingAddress1: flat.billingInfo.address1,
                                    billingAddress2: flat.billingInfo.address2,
                                    billingCity: flat.billingInfo.city,
                                    billingState: flat.billingInfo.state,
                                    billingZip: flat.billingInfo.zip,
                                    billingTaxId: flat.billingInfo.taxId,
                                    billingBillerId: flat.billingInfo.billerId,
                                    billingPhone: flat.billingInfo.telephone,
                                    npiNumber: flat.npiNumber
                                });
                            }
                        });
                }
            });
    };


    self.commit = function(value, rowIndex, cb) {
        context.District.findOne({
            districtCode: value.districtCode
        })
            .populate('rangedData')
            .exec(function(err, district) {
                if (!!err) {
                    logger.error('district import error: %s', err);
                    self.emit('commit error', {});
                    cb(err, false);
                }
                if (!district) {
                    logger.debug('district ' + value.name + ' is new.  Creating.');
                    district = new context.District();
                    district.rangedData = [];
                } else {
                    logger.debug('district ' + value.name + ' is not new.  Modifying.');
                }
                district.districtCode = value.districtCode;

                districtRange = new context.ranged.District();
                districtRange.name = value.name;
                districtRange.address1 = value.addressLine1;
                districtRange.address2 = value.addressLine2;
                districtRange.city = value.city;
                districtRange.state = value.state;
                districtRange.zip = value.zip;
                districtRange.telephone = value.telephoneNumber;
                districtRange.healthPlanId = value.healthPlanId;
                districtRange.claimingStartDate = new Date(value.claimingStartDate);
                districtRange.npiNumber = value.npiNumber;

                logger.silly('adjusting moments to tz ' + self.clientTimezone);
                var startMoment = moment.tz(value.startDate, self.clientTimezone)
                    .startOf('day'),
                    endMoment = moment.tz(value.endDate, self.clientTimezone)
                    .endOf('day');

                logger.silly('end moment after client: ' + endMoment.date());
                logger.silly('district range: ' + startMoment.toString() + ' - ' + endMoment.toString())

                districtRange.startDate = startMoment.toDate();
                districtRange.endDate = endMoment.toDate();

                //billing info
                districtRange.billingInfo.name = value.billingName;
                districtRange.billingInfo.address1 = value.billingAddress1;
                districtRange.billingInfo.address2 = value.billingAddress2;
                districtRange.billingInfo.city = value.billingCity;
                districtRange.billingInfo.state = value.billingState;
                districtRange.billingInfo.zip = value.billingZip;
                districtRange.billingInfo.taxId = value.billingTaxId;
                districtRange.billingInfo.billerId = value.billingBillerId;
                districtRange.billingInfo.telephone = value.billingPhone;

                dataUtil.dateSplice(district.rangedData, districtRange, function(err, rangesToSave) {
                    if (!!err) {
                        logger.error(err);
                    }

                    dataUtil.dateSpliceSave(self.actor, district,
                        rangesToSave,
                        'rangedData',
                        function(err, savedDistrict) {
                            if (!!err) {
                                logger.error('error importing district: %s', util.inspect(err));
                                logger.debug('error rowdata info: %s', util.inspect(value));
                                logger.debug('error district: %s', util.inspect(district));
                                cb(err);
                            } else {
                                cb(null);
                            }
                        });
                });

            });
    };
    self.sheet.rowValidators = [{
        validator: function(value, callback) {
            var start = new Date(value.startDate);
            var end = new Date(value.endDate);
            if (!!start.getTime() && !!end.getTime()) {
                //both are valid
                callback(null, start < end && start <= Date.now() && end >= Date.now());
            } else if (!!start.getTime() && !end.getTime()) {
                //start is valid, end is blank
                callback(null, start <= Date.now());
            } else if (!start.getTime() && !!end.getTime()) {
                //end is valid, start is blank
                callback(null, end >= Date.now());
            } else if (!start.getTime() && !end.getTime()) {
                //item is always date-valid
                callback(null, true);
            } else {
                //this should not happen unless there is a rip in the space-time continuum
                logger.warn("dates are both valid and not valid in district import");
                callback(null, true);
            }
        },
        errorMessage: '[error messages for district needed]'
    }];
    return self;
};
DistrictImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = DistrictImport;
