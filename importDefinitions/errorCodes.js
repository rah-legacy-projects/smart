var logger = require('winston'),
    _ = require('lodash'),
    context = require('../models/context'),
    moment = require('moment'),
    dataUtil = require('../models/dataUtil'),
    async = require('async');

var ErrorCodesImport = function() {
    var self = this;
    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'Error Codes Import',
        className: 'errorCodes',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'Error Code',
            propertyName: 'errorCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Error Code needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Error Code]'
            }]
        }, {
            name: 'Error Description',
            propertyName: 'errorDescription',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Error Description needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Error Description]'
            }]
        }, ]
    };
    self.diff = function(value, index, cb) {
        context.RemittanceError.findOne({
            code: value.errorCode
        })
            .exec(function(err, record) {
                if (!!err) {
                    logger.error('remittance error import error: %s', err);
                    self.emit('diff error', {});
                    cb(err, false);
                } else if (!record) {
                    cb(null, null);
                } else {

                    cb(err, {
                        errorCode: record.code,
                        errorDescrption: record.description
                    });
                }
            });
    };
    self.commit = function(value, rowIndex, cb) {
        context.RemittanceError.findOne({
            code: value.errorCode
        })
            .exec(function(err, record) {
                if (!record) {
                    record = new context.RemittanceError();
                }
                record.code = value.errorCode;
                record.description = value.errorDescription;

                record.save(function(err) {
                    cb(err, true);
                });
            });
    };
    self.sheet.rowValidators = [{
        validator: function(value, cb) {
            cb(null, true);
        },
        errorMessage: '[error messages for errorCodes needed]'
    }];
    return self;
};
ErrorCodesImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = ErrorCodesImport;
