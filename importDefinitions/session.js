var logger = require('winston'),
    moment = require('moment-timezone');

var SessionImport = function(actor, clientTimezone) {
    var self = this;
    self.actor = actor || "[import]";
    self.clientTimezone = clientTimezone;
    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'Session Import',
        className: 'session',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'District Code',
            propertyName: 'districtCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for District Code needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for District Code]'
            }]
        }, {
            name: 'Student ID',
            propertyName: 'studentId',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Student ID needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Student ID]'
            }]
        }, {
            name: 'Specialty',
            propertyName: 'specialty',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Specialty needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Specialty]'
            }]
        }, {
            name: 'Session Identifier',
            propertyName: 'sessionIdentifier',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Session Identifier needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Session Identifier]'
            }]
        }, {
            name: 'Date',
            propertyName: 'date',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Date]'
            }]
        }, {
            name: 'Time',
            propertyName: 'time',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Time needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Time]'
            }]
        }, {
            name: 'Duration',
            propertyName: 'duration',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Duration needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Duration]'
            }]
        }, {
            name: 'Procedure Code',
            propertyName: 'procedureCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Procedure Code needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Procedure Code]'
            }]
        }, {
            name: 'Absent',
            propertyName: 'absent',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Absent needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Absent]'
            }]
        }, {
            name: 'Absence Reason',
            propertyName: 'absenceReason',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Absence Reason needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Absence Reason]'
            }]
        }, {
            name: 'Individual Note',
            propertyName: 'individualNote',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Individual Note needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Individual Note]'
            }]
        }, {
            name: 'Session Note',
            propertyName: 'sessionNote',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Session Note needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Session Note]'
            }]
        }, {
            name: 'Finalize',
            propertyName: 'finalize',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Finalize needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Finalize]'
            }]
        }, {
            name: 'Organizer',
            propertyName: 'organizer',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for organizer needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Organizer]'
            }]
        }]
    };
    self.commit = function(value, rowIndex, cb) {
        logger.warn('sessions have a special commit')
        cb(null, true);
    };
    self.sheet.rowValidators = [{
        validator: function(value, cb) {
            logger.warn('validator for session needed');
            cb(null, true);
        },
        errorMessage: '[error messages for session needed]'
    }];
    return self;
};
SessionImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = SessionImport;
