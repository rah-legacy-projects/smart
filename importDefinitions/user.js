var logger = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util'),
    moment = require('moment-timezone'),
    context = require('../models/context');

var UserImport = function(actor, clientTimezone) {
    var self = this;
    self.actor = actor || "[import]";
    self.clientTimezone = clientTimezone;

    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'User Import',
        className: 'user',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'Email Address',
            propertyName: 'emailAddress',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Email Address needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Email Address]'
            }]
        }, {
            name: 'First Name',
            propertyName: 'firstName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for First Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for First Name]'
            }]
        }, {
            name: 'Middle Name',
            propertyName: 'middleName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Middle Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Middle Name]'
            }]
        }, {
            name: 'Last Name',
            propertyName: 'lastName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Last Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Last Name]'
            }]
        }, {
            name: 'Password',
            propertyName: 'password',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Password needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Password]'
            }]
        }, {
            name: 'Is Administrator',
            propertyName: 'isAdministrator',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Is Administrator needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Is Administrator]'
            }]
        }, {
            name: 'SSN',
            propertyName: 'ssn',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for ssn needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for ssn]'
            }]
        }, ]
    };
    self.diff = function(value, rowIndex, cb) {
        //ignore users diff for now
        cb(null, null);
    };
    self.commit = function(value, rowIndex, cb) {
        context.User.findOne({
            emailAddress: value.emailAddress
        })
            .exec(function(err, record) {
                if (!record) {
                    record = new context.User();
                    record.createdBy = self.actor;
                }
                record.emailAddress = value.emailAddress;
                record.firstName = value.firstName;
                record.middleName = value.middleName;
                record.lastName = value.lastName;
                record.password = value.password;
                record.active = true;
                record.ssn = value.ssn;
                record.modifiedBy = self.actor;
                record.save(function(err) {
                    if (!!err) {
                        logger.error(err);
                    }
                    if (/yes/i.test(value.isAdministrator)) {
                        context.Area.findOne({
                            name: /administration/i
                        })
                            .exec(function(err, area) {
                                area.setAccess(record, ['Administer']);
                                record.save(function(err) {
                                    cb(null, true);
                                });
                            });
                    } else {
                        cb(null, true);
                    }
                });
            });
    };
    self.sheet.rowValidators = [{
        validator: function(value, cb) {
            cb(null, true);
        },
        errorMessage: '[error messages for user needed]'
    }];
    return self;
};
UserImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = UserImport;
