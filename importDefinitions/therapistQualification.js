var logger = require('winston'),
    mongoose = require('mongoose'),
    context = require('../models/context'),
    async = require('async'),
    moment = require('moment-timezone'),
    _ = require('lodash'),
    util = require('util'),
    dataUtil = require('../models/dataUtil');

var therapistQualificationImport = function(actor, clientTimezone) {
    var self = this;
    self.actor = actor || "[import]";
    self.clientTimezone = clientTimezone;
    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'Therapist Qualification Import',
        className: 'therapistQualification',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'District Code',
            propertyName: 'districtCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for DistrictCode needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for DistrictCode]'
            }]
        }, {
            name: 'Therapist Name',
            propertyName: 'therapistName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Therapist Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Therapist Name]'
            }]
        }, {
            name: 'NPI Number',
            propertyName: 'npiNumber',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for NPI number]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for npi number]'
            }]
        }, {
            name: 'Licensed',
            propertyName: 'licensed',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for NPI number]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for npi number]'
            }]
        }, {
            name: 'Start Date',
            propertyName: 'startDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Start Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Start Date]'
            }]
        }, {
            name: 'End Date',
            propertyName: 'endDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for End Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for End Date]'
            }]
        }, ]
    };

    self.commit = function(value, rowIndex, cb) {
        var tasks = {
            user: function(cb) {
                var nameParts = value.therapistName.split(' ');
                context.User.findOne({
                    firstName: new RegExp(nameParts[0], 'i'),
                    lastName: new RegExp(nameParts[1], 'i')
                })
                    .populate('therapistQualifications')
                    .exec(function(err, therapist) {
                        if (!!err) {
                            logger.error('error getting provider: ' + util.inspect(err));
                        }
                        cb(err, therapist);
                    });
            },
        };

        async.parallel(tasks, function(err, r) {
            var theraqual = new context.TherapistQualification();
            var startMoment = moment.tz(value.startDate, self.clientTimezone)
                .startOf('day'),
                endMoment = moment.tz(value.endDate, self.clientTimezone)
                .endOf('day');

            logger.silly('range: ' + startMoment.toString() + ' - ' + endMoment.toString())

            theraqual.startDate = startMoment.toDate();
            theraqual.endDate = endMoment.toDate();
            theraqual.npiNumber = value.npiNumber;
            theraqual.licensed = /yes/i.test(value.licensed);

            theraqual.save(function(err, theraqual) {
                r.user.therapistQualifications.push(theraqual);
                r.user.save(function(err) {
                    cb(err);
                });
            })
        });
    };

    self.sheet.rowValidators = [{
        validator: function(value, cb) {
            logger.silly('validator for therapistQualification needed');
            cb(null, true);
        },
        errorMessage: '[error messages for therapistQualification needed]'
    }];
    return self;
};
therapistQualificationImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = therapistQualificationImport;
