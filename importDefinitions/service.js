var logger = require('winston'),
    context = require('../models/context'),
    moment = require('moment-timezone'),
    _ = require('lodash'),
    dataUtil = require('../models/dataUtil'),
    async = require('async');

require('../util')();
var ServiceImport = function(actor, clientTimezone) {
    var self = this;
    self.actor = actor || "[import]";
    self.clientTimezone = clientTimezone;
    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'IEP Service Import',
        className: 'service',
        fuzzyHeader: {
            threshold: 5
        },
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'Student',
            propertyName: 'student',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for Student needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Student]'
            }]
        }, {
            name: 'State ID',
            propertyName: 'stateId',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for State ID needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for State ID]'
            }]
        }, {
            name: 'School',
            propertyName: 'school',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for School needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for School]'
            }]
        }, {
            name: 'Case Manager',
            propertyName: 'caseManager',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Case Manager needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Case Manager]'
            }]
        }, {
            name: 'Service Type',
            propertyName: 'serviceType',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for Service Type needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Service Type]'
            }]
        }, {
            name: 'Service',
            propertyName: 'service',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Service needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Service]'
            }]
        }, {
            name: 'Begin/End Date',
            propertyName: 'beginEndDate',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Begin/End Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /(\d{1,2}[\/\-]\d{1,2}[\/\-](?:\d{4}|\d{2}))\s*.\s*(\d{1,2}[\/\-]\d{1,2}[\/\-](?:\d{4}|\d{2}))/,
                errorMessage: '[cell rules needed for Begin/End Date]'
            }]
        }, {
            name: 'Frequency/Duration',
            propertyName: 'frequencyDuration',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Frequency/Duration needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /(\d+)\s*sessions\/(W|Y|MON|A)\s+of\s+(?:(\d+)\s*(min)|(\d*)(:?\.(\d+))?\s*(hr))/,
                errorMessage: '[rules needed for Frequency/Duration]'
            }]
        }, {
            name: 'Provider',
            propertyName: 'therapist',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Provider needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Provider]'
            }]
        }, {
            name: 'Location',
            propertyName: 'location',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Location needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Location]'
            }]
        }, {
            name: 'Serving School',
            propertyName: 'servingSchool',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Serving School needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Serving School]'
            }]
        }, {
            name: 'Consult',
            propertyName: 'consult',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Consult needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Consult]'
            }]
        }, {
            name: 'ESY',
            propertyName: 'esy',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for ESY needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for ESY]'
            }]
        }, {
            name: 'SSN',
            propertyName: 'ssn',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for SSN needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for SSN]'
            }]
        }, {
            name: 'DOB',
            propertyName: 'dob',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for DOB needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for DOB]'
            }]
        }, ]
    };

    self.diff = function(value, index, cb) {
        var dates = /(\d{1,2}[\/\-]\d{1,2}[\/\-](?:\d{4}|\d{2}))\s*.\s*(\d{1,2}[\/\-]\d{1,2}[\/\-](?:\d{4}|\d{2}))/.exec(value.beginEndDate);

        var startMoment = moment.tz(dates[1], self.clientTimezone)
            .startOf('day'),
            endMoment = moment.tz(dates[2], self.clientTimezone)
            .endOf('day');

        logger.silly('diff range: ' + startMoment.toString() + ' - ' + endMoment.toString());

        var tasks = {
            therapist: function(cb) {
                var nameParts = value.therapist.split(' ');
                context.User.findOne({
                    firstName: new RegExp(nameParts[0], 'i'),
                    lastName: new RegExp(nameParts[1], 'i')
                }, function(err, therapist) {
                    if (!!err) {
                        logger.error('error getting provider: ' + util.inspect(err));
                    }
                    cb(err, therapist);
                });
            },
            caseManager: function(cb) {
                var nameParts = value.caseManager.split(' ');
                context.User.findOne({
                    firstName: new RegExp(nameParts[0], 'i'),
                    lastName: new RegExp(nameParts[1], 'i')
                }, function(err, caseManager) {
                    if (!!err) {
                        logger.error('error getting case manager: ' + util.inspect(err));
                    }
                    cb(err, caseManager);
                });
            },
            student: function(cb) {
                context.Student.findOne({
                    studentId: new RegExp(value.stateId, 'i')
                })
                    .populate('rangedData')
                    .lean()
                    .exec(function(err, student) {
                        dataUtil.getByDateRange(student, 'rangedData',
                            startMoment,
                            endMoment,
                            function(err, flatStudent) {
                                cb(err, flatStudent);
                            });
                    });
            },
            school: function(cb) {
                context.Student.findOne({
                    studentId: new RegExp(value.stateId, 'i')
                })
                    .populate('rangedData')
                    .lean()
                    .exec(function(err, student) {
                        dataUtil.getByDateRange(student, 'rangedData',
                            startMoment,
                            endMoment,
                            function(err, flatStudent) {
                                context.School.findOne({
                                    _id: flatStudent.school
                                })
                                    .populate('rangedData')
                                    .lean()
                                    .exec(function(err, school) {
                                        dataUtil.getByDateRange(school, 'rangedData',
                                            startMoment,
                                            endMoment,
                                            function(err, flatSchool) {
                                                cb(err, flatSchool);
                                            });
                                    });
                            });
                    });
            },
            district: function(cb) {
                context.Student.findOne({
                    studentId: new RegExp(value.stateId, 'i')
                })
                    .populate('rangedData')
                    .lean()
                    .exec(function(err, student) {
                        dataUtil.getByDateRange(student, 'rangedData',
                            startMoment,
                            endMoment,

                            function(err, flatStudent) {
                                context.School.findOne({
                                    _id: flatStudent.school
                                })
                                    .exec(function(err, school) {
                                        context.District.findOne({
                                            _id: school.district
                                        })
                                            .populate('rangedData')
                                            .lean()
                                            .exec(function(err, district) {
                                                dataUtil.getByDateRange(district, 'rangedData',
                                                    startMoment,
                                                    endMoment,
                                                    function(err, flatDistrict) {
                                                        cb(err, flatDistrict);
                                                    });
                                            });
                                    });
                            });
                    });
            }

        };
        async.parallel(
            tasks,
            function(perr, results) {
                if (!!perr) {
                    logger.error(util.inspect(perr));
                }

                context.Service.findOne({
                    student: results.student._id,
                    caseManager: results.caseManager._id,
                    therapist: results.therapist._id,
                    service: value.service
                })
                    .populate('rangedData')
                    .lean()
                    .exec(function(err, service) {
                        if (!service) {
                            cb(err, null);
                        } else {
                            dataUtil.getByDateRange(
                                service,
                                'rangedData',
                                startMoment,
                                endMoment,
                                function(err, flat) {
                                    if (!flat) {
                                        cb(err, null);
                                    } else {
                                        cb(err, {
                                            student: results.student.firstName + ' ' + results.student.lastName,
                                            stateId: results.student.studentId,
                                            school: results.school.schoolCode,
                                            caseManager: results.caseManager.fullName,
                                            serviceType: flat.serviceType,
                                            service: flat.service,
                                            beginEndDate: ("{start} - {end}")
                                                .format({
                                                    start: startMoment
                                                        .format('MM/DD/YYYY'),
                                                    end: endMoment
                                                        .format('MM/DD/YYYY')
                                                }),
                                            frequencyDuration: ("{numSessions} sessions/{freq} of {duration} min")
                                                .format({
                                                    numSessions: flat.numberOfSessions,
                                                    freq: flat.frequency,
                                                    duration: flat.duration
                                                }),
                                            therapist: results.therapist.fullName,
                                            location: flat.location,
                                            servingSchool: flat.servingSchool,
                                            consult: flat.consult,
                                            esy: flat.esy,
                                            ssn: flat.ssn,
                                            dob: flat.dob
                                        });
                                    }
                                });
                        }
                    });
            });
    };



    self.commit = function(value, rowIndex, cb) {

        var tasks = {
            therapist: function(cb) {
                var nameParts = value.therapist.split(' ');
                context.User.findOne({
                    firstName: new RegExp(nameParts[0], 'i'),
                    lastName: new RegExp(nameParts[1], 'i')
                }, function(err, therapist) {
                    if (!!err) {
                        logger.error('error getting provider: ' + util.inspect(err));
                    }
                    cb(err, therapist);
                });
            },
            caseManager: function(cb) {
                var nameParts = value.caseManager.split(' ');
                context.User.findOne({
                    firstName: new RegExp(nameParts[0], 'i'),
                    lastName: new RegExp(nameParts[1], 'i')
                }, function(err, caseManager) {
                    if (!!err) {
                        logger.error('error getting case manager: ' + util.inspect(err));
                    }
                    cb(err, caseManager);
                });
            },
            student: function(cb) {
                context.Student.findOne({
                    studentId: new RegExp(value.stateId, 'i')
                })
                    .populate('rangedData')
                    .exec(function(err, student) {
                        if (!!err) {
                            logger.error('error getting student: ' + util.inspect(err));
                        }
                        cb(err, student);
                    });
            }
        };

        async.parallel(
            tasks,
            function(perr, results) {
                if (!!perr) {
                    logger.error(util.inspect(perr));
                }

                //get all of the services for a student
                context.Service.find({
                    student: results.student._id,
                    caseManager: results.caseManager._id,
                    therapist: results.therapist._id,
                    //service: value.service
                })
                    .populate('rangedData')
                    .exec(function(err, services) {

                        async.parallel({
                            serviceUpdate: function(cb) {

                                //{{{ service create/update
                                var service = _.find(services, function(svc) {
                                    return svc.service == value.service;
                                });

                                if (!!err) {
                                    logger.error(util.inspect(err));
                                }
                                if (!service) {
                                    service = new context.Service();
                                    service.rangedData = [];
                                }

                                service.student = results.student._id;
                                service.school = value.school;
                                service.caseManager = results.caseManager._id;
                                service.therapist = results.therapist._id;
                                service.service = value.service;

                                var serviceRange = new context.ranged.Service();
                                serviceRange.serviceType = value.serviceType;
                                serviceRange.location = value.location;
                                serviceRange.servingSchool = value.servingSchool;
                                serviceRange.consult = value.consult;
                                serviceRange.esy = value.esy;
                                serviceRange.ssn = value.ssn;
                                serviceRange.dob = (value.dob || '') == '' ? null : new Date(value.dob);





                                //begin and end dates
                                var dates = /(\d{1,2}[\/\-]\d{1,2}[\/\-](?:\d{4}|\d{2}))\s*.\s*(\d{1,2}[\/\-]\d{1,2}[\/\-](?:\d{4}|\d{2}))/.exec(value.beginEndDate);

                                var startMoment = moment(dates[1]),
                                    endMoment = moment(dates[2]);

                                //using the client-supplied moment, set the start date on the start of the day
                                startMoment = moment(self.clientStartOfDay)
                                    .year(startMoment.year())
                                    .month(startMoment.month())
                                    .date(startMoment.date());
                                endMoment = moment(self.clientEndOfDay)
                                    .year(endMoment.year())
                                    .month(endMoment.month())
                                    .date(endMoment.date());


                                logger.silly('range: ' + startMoment.toString() + ' - ' + endMoment.toString())


                                serviceRange.startDate = startMoment.toDate();
                                serviceRange.endDate = endMoment.toDate();

                                //frequency, duration
                                var fd = /(\d+)\s*sessions\/(W|Y|MON|A)\s+of\s+(?:(\d+)\s*(min)|(\d*)(:?\.(\d+))?\s*(hr))/
                                    .exec(value.frequencyDuration);

                                serviceRange.numberOfSessions = fd[1];
                                serviceRange.frequency = fd[2];
                                if (!!fd[4]) {
                                    //use the minute component as is
                                    serviceRange.duration = fd[3];
                                } else {
                                    //duration is hours
                                    if (fd[5] === '') {
                                        fd[5] = '0';
                                    }
                                    var hours = parseInt(fd[5], 10),
                                        fractionOfHour = parseFloat('0.' + fd[7], 10);
                                    serviceRange.duration = (hours * 60) + (fractionOfHour * 60);
                                }



                                dataUtil.dateSplice(service.rangedData, serviceRange, function(err, rangesToSave) {
                                    if (!!err) {
                                        logger.error(err);
                                        cb(err, false);
                                    } else {
                                        dataUtil.dateSpliceSave(self.actor, service, rangesToSave, 'rangedData', function(err, savedService) {
                                            if (!!err) {
                                                logger.error('error importing service: %s', util.inspect(err));
                                                logger.error(err.stack);
                                                logger.debug('error rowdata info: %s', util.inspect(value));
                                                logger.debug('error service: %s', util.inspect(service));
                                                cb(err);
                                            } else {
                                                cb(err, true);
                                            }
                                        });
                                    }
                                });
                                //}}}
                            },
                            studentUpdate: function(cb) {
                                /*
                                //get services that start today or before and end on or after today
                                var validServices = _.chain(services).map(function(svc){
                                    var svc.toObject()
                                }).filter(services, function(svc){
                                    var today = moment(),
                                        svc
                                });
                                */

                                var dates = /(\d{1,2}[\/\-]\d{1,2}[\/\-](?:\d{4}|\d{2}))\s*.\s*(\d{1,2}[\/\-]\d{1,2}[\/\-](?:\d{4}|\d{2}))/.exec(value.beginEndDate);

                                var startMoment = moment.tz(dates[1], self.clientTimezone)
                                    .startOf('day'),
                                    endMoment = moment.tz(dates[2], self.clientTimezone)
                                    .endOf('day'),
                                    today = moment.tz(moment()
                                        .format('YYYY-MM-DD'), self.clientTimezone);


                                //if the imported service starts before today, ends after today, and starts before the current student start,
                                if ((startMoment.isBefore(today) || startMoment.isSame(today)) &&
                                    (endMoment.isAfter(today) || endMoment.isSame(today))) {
                                    //get current (or as close to) student
                                    var studentRanges = _.chain(results.student.rangedData)
                                        .filter(function(r) {
                                            //students that are active,
                                            return r.rangeActive;
                                        })
                                        .filter(function(r) {
                                            //where end date is after today
                                            return moment(r.endDate)
                                                .isAfter(today) || moment(r.endDate)
                                                .isSame(today);
                                        })
                                        .filter(function(r) {
                                            //where start date is before today
                                            //note: not sure if this is valid.
                                            return moment(r.startDate)
                                                .isBefore(today) || moment(r.startDate)
                                                .isSame(today);
                                        })
                                        .sortBy(function(r) {
                                            return moment(r.startDate)
                                                .format('YYYYMMDD');
                                        })
                                        .reverse()
                                        .value();

                                    if (studentRanges.length > 0) {
                                        if (studentRanges.length > 1) {
                                            logger.warn('more than one active student range found.  Using closest start date.');
                                        }

                                        var studentRange = _.clone(studentRanges[0].toObject());


                                        if (moment(studentRange.startDate)
                                            .isAfter(startMoment)) {

                                            //if the closest student range starts after the service date in question, update it
                                            studentRange.startDate = startMoment.toDate();
                                            //leave the end date alone, use the iep end date from student import
                                            //splice the student
                                            //remove the (cloned) student range id
                                            delete studentRange._id;
                                            var temp = new context.ranged.Student(studentRange);

                                            dataUtil.dateSplice(results.student.rangedData, temp, function(err, rangesToSave) {
                                                if (!!err) {
                                                    logger.error(err);
                                                }

                                                logger.silly('ranges to save: ' + rangesToSave.length);
                                                logger.silly('peek: ' + util.inspect(_.map(rangesToSave, function(r) {
                                                    return {
                                                        start: r.startDate,
                                                        end: r.endDate,
                                                        active: r.rangeActive
                                                    };
                                                })));


                                                dataUtil.dateSpliceSave(self.actor, results.student,
                                                    rangesToSave,
                                                    'rangedData',
                                                    function(err, savedStudent) {
                                                        if (!!err) {
                                                            logger.error('error importing student: %s', util.inspect(err));
                                                            logger.debug('error rowdata info: %s', util.inspect(value));
                                                            logger.debug('error district: %s', util.inspect(savedStudent));
                                                            cb(err);
                                                        } else {
                                                            cb(null);
                                                        }
                                                    });
                                            });
                                        } else {
                                            //ignore the service
                                            cb(null, true);
                                        }
                                    } else {
                                        //ignore the service
                                        cb(null, true);
                                    }


                                } else {
                                    //ignore the service
                                    cb(null, true);
                                }
                            }
                        }, function(err, spliceResults) {
                            if (!!err) {
                                logger.error('service import error: ' + err);
                            }
                            cb(err, true);
                        });
                    });

            });
    };

    self.sheet.rowValidators = [{
        validator: function(value, cb) {

            var dates = /(\d{1,2}[\/\-]\d{1,2}[\/\-](?:\d{4}|\d{2}))\s*.\s*(\d{1,2}[\/\-]\d{1,2}[\/\-](?:\d{4}|\d{2}))/.exec(value.beginEndDate);

            var start = moment(dates[1], 'M/D/YYYY');
            var end = moment(dates[2], 'M/D/YYYY');
            if (start.isValid() && end.isValid()) {
                //both are valid
                cb(null, start.isBefore(end) && start.isBefore(moment()) && end.isAfter(moment()));
            } else {
                //this should not happen based on the cell rule, but for safety...a
                cb(null, false);
            }
        },
        errorMessage: '[error messages for service needed - service expired or future]'
    }, {
        validator: function(value, cb) {
            if (value.caseManager == /^\s*$/) {
                cb(null, false);
            } else {
                var tasks = {
                    admin: function(cb) {
                        context.Area.findOne({
                            name: /administration/i
                        })
                            .exec(function(err, area) {
                                cb(err, area);
                            });
                    },
                    district: function(cb) {
                        context.Student.getCurrentByStudentId(value.stateId, function(err, student) {
                            if (!student) {
                                cb('service import: no student');
                                return;
                            }
                            context.School.getCurrentById(student.school, function(err, school) {
                                if (!school) {
                                    cb('service import: no school');
                                    return;
                                }
                                context.District.findOne({
                                    _id: school.district
                                })
                                    .populate('rangedData')
                                    .exec(function(err, district) {
                                        if (!district) {
                                            cb('service import: no district');
                                            return;
                                        }
                                        if (!!err) {
                                            logger.error('uh... error: %s', err);
                                            cb(err, false);
                                        } else {
                                            cb(null, district);
                                        }
                                    });
                            });
                        });
                    },
                    caseManager: function(cb) {
                        var nameParts = value.caseManager.split(' ');
                        context.User.findOne({
                            firstName: new RegExp(nameParts[0], 'i'),
                            lastName: new RegExp(nameParts[1], 'i')
                        }, function(err, caseManager) {
                            cb(err, caseManager);
                        });
                    }
                };


                async.parallel(tasks, function(err, r) {
                    if (!!err) {
                        logger.error('uh... error: %s', err);
                        cb(err, false);
                    } else if (!r.caseManager) {
                        var names = value.caseManager.split(' ');
                        r.caseManager = new context.User({
                            firstName: names[0],
                            lastName: names[1],
                            emailAddress: '[temp created at import time]',
                            password: '[temp created at import time]',
                            active: false
                        });
                        r.caseManager.save(function(err) {
                            r.district.setAccess(r.caseManager, ['Edit']);
                            r.caseManager.save(function(err) {
                                cb(null, true);
                            });
                        });
                    } else {
                        if (_.contains(r.admin.getAccess(r.caseManager), 'Administer')) {
                            //if the user is an admin, ... do nothing?
                            cb(null, true);
                        } else if (!_.contains(r.district.getAccess(r.caseManager), 'Edit')) {
                            //if the user does not have edit permissions at the district, add them
                            r.district.setAccess(r.caseManager, ['Edit']);
                            r.caseManager.save(function(err) {
                                cb(null, true);
                            });
                        } else {
                            //the user should have permissions to edit or are an admin already... do nothing?
                            cb(null, true);
                        }

                    }
                });
            }
        },
        errorMessage: '[error message for service needed - case manager not found]'
    }, {
        validator: function(value, cb) {
            if (value.caseManager == /^\s*$/) {
                cb(null, false);
            } else {
                var tasks = {
                    admin: function(cb) {
                        context.Area.findOne({
                            name: /administration/i
                        })
                            .exec(function(err, area) {
                                cb(err, area);
                            });
                    },
                    district: function(cb) {
                        context.Student.getCurrentByStudentId(value.stateId, function(err, student) {
                            if (!student) {
                                cb('service import: no student', false);
                                return;
                            }
                            context.School.getCurrentById(student.school._id.toString(), function(err, school) {
                                if (!school) {
                                    cb('service import: no school');
                                    return;
                                }
                                context.District.findOne({
                                    _id: school.district
                                })
                                    .populate('rangedData')
                                    .exec(function(err, district) {
                                        if (!district) {
                                            cb('service import: no district');
                                            return;
                                        }
                                        if (!!err) {
                                            logger.error('uh... error: %s', err);
                                            cb(err, false);
                                        } else if (!district) {
                                            cb('service import: no district', true);
                                        } else {
                                            cb(null, district);
                                        }
                                    });
                            });
                        });
                    },
                    therapist: function(cb) {
                        var nameParts = value.therapist.split(' ');
                        context.User.findOne({
                            firstName: new RegExp(nameParts[0], 'i'),
                            lastName: new RegExp(nameParts[1], 'i')
                        }, function(err, therapist) {
                            cb(err, therapist);
                        });
                    }
                };


                async.parallel(tasks, function(err, r) {
                    if (!!err) {
                        logger.error('uh... error: %s', err);
                        cb(err, false);
                    } else if (!r.therapist) {
                        var names = value.therapist.split(' ');
                        r.therapist = new context.User({
                            firstName: names[0],
                            lastName: names[1],
                            emailAddress: '[temp created at import time]',
                            password: '[temp created at import time]',
                            active: false
                        });
                        r.therapist.save(function(err) {
                            //add district access
                            r.district.setAccess(r.therapist, ['Edit']);
                            r.therapist.save(function(err) {
                                cb(null, true);
                            });
                        });
                    } else {
                        if (_.contains(r.admin.getAccess(r.therapist), 'Administer')) {
                            //if the user is an admin, ... do nothing?
                            cb(null, true);
                        } else if (!_.contains(r.district.getAccess(r.therapist), 'Edit')) {
                            //if the user does not have edit permissions at the district, add them
                            r.district.setAccess(r.therapist, ['Edit']);
                            r.therapist.save(function(err) {
                                cb(null, true);
                            });
                        } else {
                            //the user should have permissions to edit or are an admin already... do nothing?
                            cb(null, true);
                        }

                    }
                });
            }
        },
        errorMessage: '[error message for service needed - provider not found]'
    }, {
        validator: function(value, cb) {
            //validate provider exists
            context.Student.findOne({
                studentId: new RegExp(value.stateId, "i")
            }, function(err, student) {
                if (!!err) {
                    logger.error('uh... error: %s', err);
                    cb(err, false);
                } else if (!student) {
                    logger.warn('student did not exist for service');
                    cb(null, false);
                } else {
                    cb(null, true);
                }
            });
        },
        errorMessage: '[error message for service needed - student not found]'
    }];
    return self;
};
ServiceImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = ServiceImport;
