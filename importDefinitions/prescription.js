var logger = require('winston'),
    mongoose = require('mongoose'),
    context = require('../models/context'),
    async = require('async'),
    moment = require('moment-timezone'),
    _ = require('lodash'),
    util = require('util'),
    dataUtil = require('../models/dataUtil');


var PrescriptionImport = function(actor, clientTimezone) {
    var self = this;
    self.actor = actor || "[import]";
    self.clientTimezone = clientTimezone;

    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'Prescription Import',
        className: 'prescription',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'Start Date',
            propertyName: 'startDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Start Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Start Date]'
            }]
        }, {
            name: 'Expiration Date',
            propertyName: 'expirationDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Expiration Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Expiration Date]'
            }]
        }, {
            name: 'Specialty',
            propertyName: 'specialty',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Specialty needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Specialty]'
            }]
        }, {
            name: 'Doctor Name',
            propertyName: 'doctorName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Doctor Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Doctor Name]'
            }]
        }, {
            name: 'Student ID',
            propertyName: 'studentId',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Student ID needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Student ID]'
            }]
        }, {
            name: 'District Code',
            propertyName: 'districtCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for District Code needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for District Code]'
            }]
        }, ]
    };
    self.commit = function(value, rowIndex, cb) {
        var tasks = [

            function(cb) {
                context.District.findOne({
                    districtCode: value.districtCode
                })
                    .lean()
                    .exec(function(err, district) {
                        cb(err, district);
                    });
            },
            function(district, cb) {
                context.Student.find({
                    studentId: value.studentId
                })
                    .populate('prescriptions')
                    .populate('rangedData')
                    .exec(function(err, students) {
                        async.waterfall([

                            function(cb) {
                                context.School.populate(students, {
                                    path: 'rangedData.school',
                                    model: 'School'
                                }, cb);
                            },
                            function(students, cb) {
                                context.School.populate(students, {
                                    path: 'rangedData.school.district',
                                    model: 'District'
                                }, cb);
                            }
                        ], function(err, students) {
                            var student = _.find(students, function(student) {
                                return _.any(student.rangedData, function(range) {
                                    return range.school.district.id.toString() == district._id.toString();
                                });
                            });

                            cb(err, student);
                        });

                    });
            }
        ];

        async.waterfall(tasks, function(err, student) {
            logger.silly('value range: ' + value.startDate + ' to ' + value.expirationDate);
            var record = new context.Prescription();

            var startMoment = moment.tz(value.startDate, self.clientTimezone)
                .startOf('day'),
                endMoment = moment.tz(value.expirationDate, self.clientTimezone)
                .endOf('day');

            logger.silly('range: ' + startMoment.toString() + ' - ' + endMoment.toString())

            record.startDate = startMoment.toDate();
            record.endDate = endMoment.toDate();
            record.serviceType = value.specialty;
            record.doctorName = value.doctorName;

            record.save(function(err, rx) {
                if (!!err) {
                    logger.error(err);
                }
                logger.silly('saved rx: ' + util.inspect(rx));
                student.prescriptions.push(rx);
                student.save(function(err) {
                    if (!!err) {
                        logger.error(err);
                    }
                    cb(err, true);
                });
            });
        });

    };
    self.sheet.rowValidators = [{
        validator: function(value, cb) {
            logger.warn('validator for prescription needed');
            cb(null, true);
        },
        errorMessage: '[error messages for prescription needed]'
    }];
    return self;
};
PrescriptionImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = PrescriptionImport;
