var logger = require('winston'),
    context = require('../models/context'),
    dataUtil = require('../models/dataUtil'),
    async = require('async'),
    moment = require('moment-timezone'),
    _ = require('lodash'),
    util = require('util');

var ProcedureCodeImport = function(actor, clientTimezone) {
    var self = this;
    self.actor = actor || "[import]";
    self.clientTimezone = clientTimezone;

    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'Procedure Code Import',
        className: 'procedureCode',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'Service Type',
            propertyName: 'serviceType',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for Service Type needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Service Type]'
            }]
        }, {
            name: 'Procedure Code',
            propertyName: 'procedureCode',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for Procedure Code needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Procedure Code]'
            }]
        }, {
            name: 'Modifier',
            propertyName: 'modifier',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Modifier needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Modifier]'
            }]
        }, {
            name: 'CPT Code Description',
            propertyName: 'cptCodeDescription',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for CPT Code Description needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for CPT Code Description]'
            }]
        }, {
            name: 'Start Date',
            propertyName: 'startDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Start Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))$/,
                errorMessage: '[rules needed for Start Date]'
            }]
        }, {
            name: 'End Date',
            propertyName: 'endDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for End Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))$/,
                errorMessage: '[rules needed for End Date]'
            }]
        }, {
            name: 'DistCode',
            propertyName: 'districtCode',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for DistCode needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\w+/,
                errorMessage: '[rules needed for DistCode]'
            }]
        }]
    };

    self.diff = function(value, index, cb) {
        context.District.findOne({
            districtCode: value.districtCode
        })
            .exec(function(err, district) {
                context.ProcedureCode.findOne({
                    procedureCode: value.procedureCode,
                    district: district.id
                })
                    .populate('rangedData')
                    .lean()
                    .exec(function(err, procedureCode) {
                        if (!!err) {
                            self.emit('diff error', {});
                            cb(err, false);
                        } else if (!procedureCode) {
                            cb(null, null);
                        } else {
                            var startMoment = moment.tz(value.startDate, self.clientTimezone)
                                .startOf('day'),
                                endMoment = moment.tz(value.endDate, self.clientTimezone)
                                .endOf('day');

                            logger.silly('diff range: ' + startMoment.toString() + ' - ' + endMoment.toString());
                            dataUtil.getByDateRange(
                                procedureCode,
                                'rangedData',
                                startMoment,
                                endMoment,
                                function(err, flat) {
                                    if (!flat) {
                                        cb(err, null);
                                    } else {
                                        cb(err, {
                                            serviceType: flat.serviceType,
                                            procedureCode: flat.procedureCode,
                                            modifier: flat.modifier,
                                            cptCodeDescription: flat.description,
                                            startDate: flat.startDate,
                                            endDate: flat.endDate,
                                            districtCode: district.districtCode,
                                        })
                                    }
                                });
                        }
                    });
            });
    };

    self.commit = function(value, rowIndex, cb) {

        context.District.findOne({
            districtCode: value.districtCode
        }, function(err, district) {
            context.ProcedureCode.findOne({
                procedureCode: value.procedureCode,
                district: district.id
            })
                .populate('rangedData')
                .exec(function(err, procedureCode) {
                    if (!!err) {
                        logger.error('uh... error: %s', err);
                        self.emit('commit error', {});
                        cb(err, false);
                    }
                    if (!procedureCode) {
                        logger.debug('procedure code ' + value.procedureCode + ' is new.  Creating.');
                        procedureCode = new context.ProcedureCode();
                        procedureCode.rangedData = [];
                    } else {
                        logger.debug('procedure code ' + value.procedureCode + ' is not new.  Modifying.');
                    }

                    procedureCode.procedureCode = value.procedureCode;
                    procedureCode.district = district.id;

                    var procedureCodeRange = new context.ranged.ProcedureCode();

                    procedureCodeRange.serviceType = value.serviceType;
                    procedureCodeRange.modifier = value.modifier;
                    procedureCodeRange.description = value.cptCodeDescription;

                    var startMoment = moment.tz(value.startDate, self.clientTimezone)
                        .startOf('day'),
                        endMoment = moment.tz(value.endDate, self.clientTimezone)
                        .endOf('day');

                    logger.silly('range: ' + startMoment.toString() + ' - ' + endMoment.toString())

                    procedureCodeRange.startDate = startMoment.toDate();
                    procedureCodeRange.endDate = endMoment.toDate();

                    dataUtil.dateSplice(procedureCode.rangedData,
                        procedureCodeRange,
                        function(err, rangesToSave) {
                            if (!!err) {
                                logger.error(err);
                            }
                            dataUtil.dateSpliceSave(self.actor, procedureCode,
                                rangesToSave,
                                'rangedData',
                                function(err, savedProcedureCode) {

                                    if (!!err) {
                                        logger.error('error importing procedurecode: %s', util.inspect(err));
                                        logger.debug('error rowdata info: %s', util.inspect(value));
                                        cb(err);
                                    } else {
                                        cb(null);
                                    }
                                });

                        });
                });
        });
    };
    self.sheet.rowValidators = [{
        validator: function(value, callback) {
            var start = new Date(value.startDate);
            var end = new Date(value.endDate);
            if (!!start.getTime() && !!end.getTime()) {
                //both are valid
                callback(null, start < end && start <= Date.now() && end >= Date.now());
            } else if (!!start.getTime() && !end.getTime()) {
                //start is valid, end is blank
                callback(null, start <= Date.now());
            } else if (!start.getTime() && !!end.getTime()) {
                //end is valid, start is blank
                callback(null, end >= Date.now());
            } else if (!start.getTime() && !end.getTime()) {
                //item is always date-valid
                callback(null, true);
            } else {
                //this should not happen unless there is a rip in the space-time continuum
                logger.warn("dates are both valid and not valid in procedure code import");
                callback(null, true);
            }
        },
        errorMessage: '[error messages for procedureCode needed]'
    }, {
        validator: function(value, callback) {
            //validate district exists
            context.District.findOne({
                districtCode: value.districtCode
            }, function(err, district) {
                if (!!err) {
                    logger.error('uh... error: %s', err);
                    callback(err, false);
                } else if (!district) {
                    logger.warn('district did not exist for procedure code');
                    callback(null, false);
                } else {
                    callback(null, true);
                }
            });
        },
        errorMessage: '[error message for procedure code needed - district not found]'
    }];
    return self;
};
ProcedureCodeImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = ProcedureCodeImport;
