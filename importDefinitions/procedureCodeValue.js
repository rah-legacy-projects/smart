var logger = require('winston'),
    mongoose = require('mongoose'),
    context = require('../models/context'),
    dataUtil = require('../models/dataUtil'),
    async = require('async'),
    moment = require('moment-timezone'),
    _ = require('lodash'),
    util = require('util');

var ProcedureCodeValueImport = function(actor, clientTimezone) {
    var self = this;
    self.actor = actor || "[import]";
    self.clientTimezone = clientTimezone;
    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'Procedure Code Value Import',
        className: 'procedureCodeValue',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'MCO',
            propertyName: 'mco',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for MCO needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for MCO]'
            }]
        }, {
            name: 'Service Type',
            propertyName: 'serviceType',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for Service Type needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Service Type]'
            }]
        }, {
            name: 'Procedure Code',
            propertyName: 'procedureCode',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for Procedure Code needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Procedure Code]'
            }]
        }, {
            name: 'Modifier',
            propertyName: 'modifier',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Modifier needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Modifier]'
            }]
        }, {
            name: 'Value',
            propertyName: 'value',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Value needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Value]'
            }]
        }, {
            name: 'Start Date',
            propertyName: 'startDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Start Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))$/,
                errorMessage: '[rules needed for Start Date]'
            }]
        }, {
            name: 'End Date',
            propertyName: 'endDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for End Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))$/,
                errorMessage: '[rules needed for End Date]'
            }]
        }, {
            name: 'DistCode',
            propertyName: 'districtCode',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for DistCode needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\w+/,
                errorMessage: '[rules needed for DistCode]'
            }]
        }]
    };


    self.diff = function(value, index, cb) {
        context.District.findOne({
            districtCode: value.districtCode
        }, function(err, district) {
            var tasks = {
                MCO: function(cb) {
                    context.MCO.findOne({
                        name: value.mco,
                        district: district.id
                    })
                        .populate('rangedData')
                        .lean()
                        .exec(function(err, mco) {
                            dataUtil.getByDateRange(mco, 'rangedData', moment(value.startDate)
                                .utc()
                                .add(self.timezoneOffset, 'minutes')
                                .toDate(),
                                moment(value.endDate)
                                .utc()
                                .add(self.timezoneOffset, 'minutes')
                                .toDate(),
                                function(err, flatMCO) {
                                    cb(err, flatMCO);
                                })
                        });
                },
                procedureCode: function(cb) {
                    context.ProcedureCode.findOne({
                        procedureCode: value.procedureCode,
                        district: district.id
                    })
                        .populate('rangedData')
                        .lean()
                        .exec(function(err, procedureCode) {
                            dataUtil.getByDateRange(procedureCode, 'rangedData',
                                moment(value.startDate)
                                .utc()
                                .add(self.timezoneOffset, 'minutes')
                                .toDate(),
                                moment(value.endDate)
                                .utc()
                                .add(self.timezoneOffset, 'minutes')
                                .toDate(),
                                function(err, flatProcCode) {
                                    cb(err, flatProcCode);
                                });
                        });
                },
            }

            async.parallel(tasks, function(err, r) {
                //try to find the procedure code value
                context.ProcedureCodeValue
                    .findOne({
                        mco: r.MCO._id,
                        procedureCode: r.procedureCode._id,
                        district: district._id
                    })
                    .populate('rangedData')
                    .lean()
                    .exec(function(err, procedureCodeValue) {
                        if (!!err) {
                            logger.error(err);
                            cb(err, null);
                        } else if (!procedureCodeValue) {
                            cb(err, null);
                        } else {
                    var startMoment = moment.tz(value.startDate, self.clientTimezone)
                        .startOf('day'),
                        endMoment = moment.tz(value.endDate, self.clientTimezone)
                        .endOf('day');

                            logger.silly('diff range: ' + startMoment.toString() + ' - ' + endMoment.toString());

                            dataUtil.getByDateRange(
                                procedureCodeValue,
                                'rangedData',
                                startMoment,
                                endMoment,
                                function(err, flat) {
                                    if (!flat) {
                                        cb(err, null);
                                    } else {
                                        cb(err, {
                                            mco: r.MCO.name,
                                            serviceType: flat.serviceType,
                                            procedureCode: r.procedureCode.procedureCode,
                                            modifier: flat.modifier,
                                            value: flat.value,
                                            startDate: flat.startDate,
                                            endDate: flat.endDate,
                                            districtCode: district.districtCode,
                                        });
                                    }
                                });
                        }
                    });
            });
        });
    };


    self.commit = function(value, rowIndex, cb) {

        context.District.findOne({
            districtCode: value.districtCode
        }, function(err, district) {
            var tasks = {
                MCO: function(cb) {
                    context.MCO.findOne({
                        name: value.mco,
                        district: district.id
                    }, function(err, mco) {
                        cb(null, mco);
                    });
                },
                procedureCode: function(cb) {
                    context.ProcedureCode.findOne({
                        procedureCode: value.procedureCode,
                        district: district.id
                    }, function(err, procedureCode) {
                        cb(null, procedureCode);
                    });
                },
            }

            async.parallel(tasks, function(err, r) {
                //try to find the procedure code value
                context.ProcedureCodeValue
                    .findOne({
                        mco: r.MCO.id,
                        procedureCode: r.procedureCode.id,
                        district: district.id
                    })
                    .populate('rangedData')
                    .exec(function(err, procedureCodeValue) {
                        if (!!err) {
                            logger.error('uh... error: %s', err);
                            self.emit('commit error', {});
                            cb(err, false);
                        }
                        if (!procedureCodeValue) {
                            logger.debug('procedure code value is new.  Creating.');
                            procedureCodeValue = new context.ProcedureCodeValue();
                        } else {
                            logger.debug('procedure code value is not new.  Modifying.');
                        }

                        procedureCodeValue.mco = r.MCO.id;
                        procedureCodeValue.procedureCode = r.procedureCode.id;
                        procedureCodeValue.district = district.id;

                        var procedureCodeValueRange = new context.ranged.ProcedureCodeValue();


                        procedureCodeValueRange.serviceType = value.serviceType;
                        procedureCodeValueRange.modifier = value.modifier;
                        procedureCodeValueRange.value = value.value;


var startMoment = moment.tz(value.startDate, self.clientTimezone)
                        .startOf('day'),
                        endMoment = moment.tz(value.endDate, self.clientTimezone)
                        .endOf('day');

                        logger.silly('range: ' + startMoment.toString() + ' - ' + endMoment.toString())

                        procedureCodeValueRange.startDate = startMoment.toDate();
                        procedureCodeValueRange.endDate = endMoment.toDate();

                        dataUtil.dateSplice(procedureCodeValue.rangedData, procedureCodeValueRange, function(err, rangesToSave) {
                            if (!!err) {
                                logger.error(err);
                            }
                            dataUtil.dateSpliceSave(self.actor, procedureCodeValue, rangesToSave, 'rangedData', function(err, savedProcedureCodeValue) {
                                if (!!err) {
                                    logger.error('error importing pcv: %s', util.inspect(err));
                                    logger.debug('error rowdata info: %s', util.inspect(value));
                                    logger.debug('error pcv: %s', util.inspect(procedureCodeValue));
                                    cb(err);
                                } else {
                                    cb(null);
                                }
                            });
                        });
                    });
            });

        });
    };
    self.sheet.rowValidators = [{
        validator: function(value, cb) {
            //validate district exists
            context.District.findOne({
                districtCode: value.districtCode
            }, function(err, district) {
                if (!!err) {
                    logger.error('uh... error: %s', err);
                    cb(err, false);
                } else if (!district) {
                    logger.warn('district did not exist for procedure code value');
                    //hack: call back with true, district existence is caught by the district validator
                    cb(null, true);
                } else {
                    //check the mco based on the found district
                    context.MCO.findOne({
                        name: value.mco,
                        district: district.id
                    }, function(err, mco) {
                        if (!!err) {
                            cb(err, false);
                        } else if (!mco) {
                            cb(null, false);
                        } else {
                            cb(null, true);
                        }
                    });

                }
            });

            //cb(null, true);
        },
        errorMessage: '[error messages for procedureCodeValue needed - could not find mco]'
    }, {
        validator: function(value, cb) {

            //check dates
            var start = new Date(value.startDate);
            var end = new Date(value.endDate);
            if (!!start.getTime() && !!end.getTime()) {
                //both are valid
                cb(null, start < end && start <= Date.now() && end >= Date.now());
            } else if (!!start.getTime() && !end.getTime()) {
                //start is valid, end is blank
                cb(null, start <= Date.now());
            } else if (!start.getTime() && !!end.getTime()) {
                //end is valid, start is blank
                cb(null, end >= Date.now());
            } else if (!start.getTime() && !end.getTime()) {
                //item is always date-valid
                cb(null, true);
            } else {
                //this should not happen unless there is a rip in the space-time continuum
                logger.warn("dates are both valid and not valid in procedure code value import");
                cb(null, true);
            }
        },
        errorMessage: '[error messages for procedureCodeValue needed - invalid dates]'
    }, {
        validator: function(value, cb) {

            //check procedure code
            context.ProcedureCode.findOne({
                procedureCode: value.procedureCode
            }, function(err, procedureCode) {
                if (!!err) {
                    cb(err, false);
                } else if (!procedureCode) {
                    cb(null, false);
                } else {
                    cb(null, true);
                }
            });

            //cb(null, true);
        },
        errorMessage: '[error messages for procedureCodeValue needed - invalid procedure code]'
    }, {
        validator: function(value, callback) {
            //validate district exists
            context.District.findOne({
                districtCode: value.districtCode
            }, function(err, district) {
                if (!!err) {
                    logger.error('uh... error: %s', err);
                    callback(err, false);
                } else if (!district) {
                    logger.warn('district did not exist for procedure code value');
                    callback(null, false);
                } else {
                    callback(null, true);
                }
            });
        },
        errorMessage: '[error message for procedure code value needed - district not found]'
    }];
    return self;
};
ProcedureCodeValueImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = ProcedureCodeValueImport;
