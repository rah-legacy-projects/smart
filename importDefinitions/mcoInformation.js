var logger = require('winston'),
    mongoose = require('mongoose'),
    context = require('../models/context'),
    async = require('async'),
    moment = require('moment-timezone'),
    _ = require('lodash'),
    util = require('util'),
    dataUtil = require('../models/dataUtil');

var McoInformationImport = function(actor, clientTimezone) {
    var self = this;
    self.actor = actor || "[import]";
    self.clientTimezone = clientTimezone;

    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'MCO Information Import',
        className: 'mcoInformation',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'Start Date',
            propertyName: 'startDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Start Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Start Date]'
            }]
        }, {
            name: 'End Date',
            propertyName: 'endDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for End Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for End Date]'
            }]
        }, {
            name: 'Specialty',
            propertyName: 'specialty',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Specialty needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Specialty]'
            }]
        }, {
            name: 'Number of Sessions',
            propertyName: 'numberOfSessions',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Number of Sessions needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Number of Sessions]'
            }]
        }, {
            name: 'MCO Approval',
            propertyName: 'mCOApproval',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for MCO Approval needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for MCO Approval]'
            }]
        }, {
            name: 'Student ID',
            propertyName: 'studentId',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Student ID needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Student ID]'
            }]
        }, {
            name: 'District Code',
            propertyName: 'districtCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for District Code needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for District Code]'
            }]
        }, ]
    };
    self.commit = function(value, rowIndex, cb) {
        var tasks = [

            function(cb) {
                context.District.findOne({
                    districtCode: value.districtCode
                })
                    .lean()
                    .exec(function(err, district) {
                        cb(err, district);
                    });
            },
            function(district, cb) {
                context.Student.find({
                    studentId: value.studentId
                })
                    .populate('mcoInformation')
                    .populate('rangedData')
                    .exec(function(err, students) {
                        async.waterfall([

                            function(cb) {
                                context.School.populate(students, {
                                    path: 'rangedData.school',
                                    model: 'School'
                                }, cb);
                            },
                            function(students, cb) {
                                context.School.populate(students, {
                                    path: 'rangedData.school.district',
                                    model: 'District'
                                }, cb);
                            }
                        ], function(err, students) {
                            var student = _.find(students, function(student) {
                                return _.any(student.rangedData, function(range) {
                                    return range.school.district.id.toString() == district._id;
                                });
                            });

                            cb(err, student);
                        });

                    });
            }
        ];

        async.waterfall(tasks, function(err, student) {
            var record = new context.ranged.StudentMCOInformation();

            var startMoment = moment.tz(value.startDate, self.clientTimezone)
                            .startOf('day'),
                            endMoment = moment.tz(value.endDate, self.clientTimezone)
                            .endOf('day');

            logger.silly('range: ' + startMoment.toString() + ' - ' + endMoment.toString())


            record.startDate = startMoment.toDate();
            record.endDate = endMoment.toDate();
            record.specialty = value.specialty;
            record.numberOfSessions = value.numberOfSessions;
            record.approvalToTreat = /yes/i.test(value.mCOApproval);
            if (!student.mcoInformation) {
                student.mcoInformation = [];
            }
            record.save(function(err, mcoInfo) {
                if (!!err) {
                    logger.error(err);
                }
                student.mcoInformation.push(mcoInfo._id);
                student.save(function(err) {
                    cb(err, true);
                });
            });
        });
    };
    self.sheet.rowValidators = [{
        validator: function(value, cb) {
            cb(null, true);
        },
        errorMessage: '[error messages for mcoInformation needed]'
    }];
    return self;
};
McoInformationImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = McoInformationImport;
