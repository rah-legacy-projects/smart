var logger = require('winston'),
    mongoose = require('mongoose'),
    context = require('../models/context'),
    dataUtil = require('../models/dataUtil'),
    moment = require('moment-timezone'),
    async = require('async'),
    _ = require('lodash'),
    util = require('util');

var StudentImport = function(actor, clientTimezone) {
    var self = this;
    self.actor = actor || "[import]";
    logger.silly('client timezone = ' + clientTimezone);
    self.clientTimezone = clientTimezone;
    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'Student Import',
        className: 'student',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'DistCode',
            propertyName: 'districtCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for DistCode needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for DistCode]'
            }]
        }, {
            name: 'DistName',
            propertyName: 'distName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for DistName needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for DistName]'
            }]
        }, {
            name: 'BillingCode',
            propertyName: 'billingCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for BillingCode needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for BillingCode]'
            }]
        }, {
            name: 'SchoolName',
            propertyName: 'schoolName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for SchoolName needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for SchoolName]'
            }]
        }, {
            name: 'SchoolCode',
            propertyName: 'schoolCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for SchoolCode needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for SchoolCode]'
            }]
        }, {
            name: 'Grade',
            propertyName: 'grade',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Grade needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Grade]'
            }]
        }, {
            name: 'PrimaryDisability',
            propertyName: 'primaryDisability',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PrimaryDisability needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PrimaryDisability]'
            }]
        }, {
            name: 'PrimaryDisabilityCode',
            propertyName: 'primaryDisabilityCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PrimaryDisabilityCode needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PrimaryDisabilityCode]'
            }]
        }, {
            name: 'SecondaryDisability',
            propertyName: 'secondaryDisability',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for SecondaryDisability needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for SecondaryDisability]'
            }]
        }, {
            name: 'SecondaryDisabilityCode',
            propertyName: 'secondaryDisabilityCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for SecondaryDisabilityCode needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for SecondaryDisabilityCode]'
            }]
        }, {
            name: 'Age',
            propertyName: 'age',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Age needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Age]'
            }]
        }, {
            name: 'Gender',
            propertyName: 'gender',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Gender needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Gender]'
            }]
        }, {
            name: 'StudentID',
            propertyName: 'studentId',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for StudentID needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for StudentID]'
            }]
        }, {
            name: 'StateID',
            propertyName: 'stateId',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for StateID needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for StateID]'
            }]
        }, {
            name: 'SSNumber',
            propertyName: 'sSNumber',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for SSNumber needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for SSNumber]'
            }]
        }, {
            name: 'StudentsFirstName',
            propertyName: 'studentFirstName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Student First Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Student Name]'
            }]
        }, {
            name: 'StudentsMiddleName',
            propertyName: 'studentMiddleName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Student Middle Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Student Name]'
            }]
        }, {
            name: 'StudentsLastName',
            propertyName: 'studentLastName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Student Last Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Student Name]'
            }]
        }, {
            name: 'DateOfBirth',
            propertyName: 'dateOfBirth',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for DateOfBirth needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))?$/,
                errorMessage: '[rules needed for DateOfBirth]'
            }]
        }, {
            name: 'PrimaryOption',
            propertyName: 'primaryOption',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PrimaryOption needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PrimaryOption]'
            }]
        }, {
            name: 'SecondaryOption',
            propertyName: 'secondaryOption',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for SecondaryOption needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for SecondaryOption]'
            }]
        }, {
            name: 'Race',
            propertyName: 'race',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Race needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Race]'
            }]
        }, {
            name: 'WPPRDate',
            propertyName: 'wPPRDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for WPPRDate needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))?$/,
                errorMessage: '[rules needed for WPPRDate]'
            }]
        }, {
            name: 'EligibilityDate',
            propertyName: 'eligibilityDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for EligibilityDate needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))?$/,
                errorMessage: '[rules needed for EligibilityDate]'
            }]
        }, {
            name: 'LastIEPDate',
            propertyName: 'lastIEPDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for LastIEPDate needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))?$/,
                errorMessage: '[rules needed for LastIEPDate]'
            }]
        }, {
            name: 'IEPEndDate',
            propertyName: 'iEPEndDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for IEPEndDate needed]',
                ordinal: 0
            }],
            validators: [{
                validator: function(value, cb) {
                    if (!/^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))$/.test(value)) {
                        cb(null, false);
                    } else {
                        var now = moment();
                        cb(null, moment(value)
                            .isAfter(now) || moment(value)
                            .isSame(now));
                    }
                },
                errorMessage: '[rules needed for IEPEndDate]'
            }]
        }, {
            name: 'CaseManager',
            propertyName: 'caseManager',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for CaseManager needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for CaseManager]'
            }]
        }, {
            name: 'MedicaidNumber',
            propertyName: 'medicaidNumber',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for MedicaidNumber needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for MedicaidNumber]'
            }]
        }, {
            name: 'DateExited',
            propertyName: 'dateExited',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for DateExited needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))?$/,
                errorMessage: '[rules needed for DateExited]'
            }]
        }, {
            name: 'ExitReason',
            propertyName: 'exitReason',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for ExitReason needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for ExitReason]'
            }]
        }, {
            name: 'ExitReasonCode',
            propertyName: 'exitReasonCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for ExitReasonCode needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for ExitReasonCode]'
            }]
        }, {
            name: 'NumOfIEPS',
            propertyName: 'numOfIEPs',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for NumOfIEPS needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\d*/,
                errorMessage: '[rules needed for NumOfIEPS]'
            }]
        }, {
            name: 'DaysUntilElig',
            propertyName: 'daysUntilEligible',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for DaysUntilEligible needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\d*/,
                errorMessage: '[rules needed for DaysUntilEligible]'
            }]
        }, {
            name: 'DaysUntilIEP',
            propertyName: 'daysUntilIEP',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for DaysUntilIEP needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\d*/,
                errorMessage: '[rules needed for DaysUntilIEP]'
            }]
        }, {
            name: 'StatusOfService',
            propertyName: 'statusOfService',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for StatusOfService needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for StatusOfService]'
            }]
        }, {
            name: 'StudentType',
            propertyName: 'studentType',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for StudentType needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for StatusOfService]'
            }]
        }, {
            name: 'ReasonLessFullService',
            propertyName: 'reasonLessFullService',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for ReasonLessFullService needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for ReasonLessFullService]'
            }]
        }, {
            name: 'Eligible',
            propertyName: 'eligible',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Eligible needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Eligible]'
            }]
        }, {
            name: 'InstrPercent',
            propertyName: 'instrPercent',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for InstrPercent needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for InstrPercent]'
            }]
        }, {
            name: 'RelPercent',
            propertyName: 'relPercent',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for RelPercent needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for RelPercent]'
            }]
        }, {
            name: 'TotPercent',
            propertyName: 'totPercent',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for TotPercent needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\d*/,
                errorMessage: '[rules needed for TotPercent]'
            }]
        }, {
            name: 'Transportation',
            propertyName: 'transportation',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Transportation needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Transportation]'
            }]
        }, {
            name: 'Status',
            propertyName: 'status',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Status needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Status]'
            }]
        }, {
            name: 'DateAdded',
            propertyName: 'dateAdded',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for DateAdded needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))?$/,
                errorMessage: '[rules needed for DateAdded]'
            }]
        }, {
            name: 'CenContract',
            propertyName: 'cenContract',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for CenContract needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for CenContract]'
            }]
        }, {
            name: 'CenSeparate',
            propertyName: 'cenSeparate',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for CenSeparate needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for CenSeparate]'
            }]
        }, {
            name: 'CenCorrectional',
            propertyName: 'cenCorrectional',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for CenCorrectional needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for CenCorrectional]'
            }]
        }, {
            name: 'CenMaterialsOnly',
            propertyName: 'cenMaterialsOnly',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for CenMaterialsOnly needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for CenMaterialsOnly]'
            }]
        }, {
            name: 'NextSchool',
            propertyName: 'nextSchool',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for NextSchool needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for NextSchool]'
            }]
        }, {
            name: 'NextSchoolCode',
            propertyName: 'nextSchoolCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for NextSchoolCode needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for NextSchoolCode]'
            }]
        }, {
            name: 'Area',
            propertyName: 'area',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Area needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Area]'
            }]
        }, {
            name: 'EISEnrollmentYear',
            propertyName: 'eISEnrollmentYear',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for EISEnrollmentYear needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for EISEnrollmentYear]'
            }]
        }, {
            name: 'Last504PlanDate',
            propertyName: 'last504PlanDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Last504PlanDate needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))?$/,
                errorMessage: '[rules needed for Last504PlanDate]'
            }]
        }, {
            name: 'LastGradPlanDate',
            propertyName: 'lastGradPlanDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for LastGradPlanDate needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))?$/,
                errorMessage: '[rules needed for LastGradPlanDate]'
            }]
        }, {
            name: 'TEIDSLastIFSPDate',
            propertyName: 'tEIDSLastIFSPDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for TEIDSLastIFSPDate needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))?$/,
                errorMessage: '[rules needed for TEIDSLastIFSPDate]'
            }]
        }, {
            name: 'AssignedSchoolCode',
            propertyName: 'assignedSchoolCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for AssignedSchoolCode needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for AssignedSchoolCode]'
            }]
        }]
    };

    self.diff = function(value, index, cb) {
        var startMoment = moment.tz(value.startDate, self.clientTimezone)
            .startOf('day'),
            endMoment = moment.tz(value.endDate, self.clientTimezone)
            .endOf('day');

        logger.silly('diff range: ' + startMoment.toString() + ' - ' + endMoment.toString());


        async.waterfall([

            function(cb) {
                context.Student.find({
                    studentId: value.studentId
                })
                    .populate('rangedData')
                    .exec(function(err, students) {
                        cb(err, students);

                    });
            },
            function(students, cb) {
                context.Student.populate(students, {
                    path: 'rangedData.school',
                    model: 'School'
                }, function(err, students) {
                    cb(err, students);
                });
            },
            function(students, cb) {
                context.Student.populate(students, {
                    path: 'rangedData.school.district',
                    model: 'District'
                }, function(err, students) {
                    cb(err, students);
                });
            },
            function(students, cb) {
                var startMoment = moment.tz(moment()
                        .format('YYYY-MM-DD'), self.clientTimezone),
                    endMoment = moment.tz(value.iEPEndDate, self.clientTimezone)
                    .endOf('day');
                students = _.compact(_.map(students, function(student) {
                    return dataUtil.getByDateRangeSync(student.toObject(), 'rangedData', startMoment, endMoment);
                }));
                var student = _.find(students, function(student) {
                    return student.school.district.districtCode == value.districtCode;
                });

                if (!!student) {
                    context.Student.findOne({
                        _id: student._id
                    })
                        .populate('rangedData')
                        .exec(function(err, student) {
                            cb(err, student);
                        });
                } else {
                    cb(null, null);
                }
            }
        ], function(err, student) {
            if (!!err) {
                logger.error('uh... error: %s', err);
                self.emit('commit error', {});
                cb(err, null);
            } else if (!student) {
                cb(err, null);
            } else {
                context.District.findOne({
                    districtCode: value.districtCode
                })
                    .populate('rangedData')
                    .lean()
                    .exec(function(err, district) {
                        dataUtil.getByDateRange(
                            district,
                            'rangedData',
                            startMoment,
                            endMoment,
                            function(err, flatDistrict) {
                                context.School.findOne({
                                    schoolNumber: value.schoolCode,
                                    district: district._id
                                })
                                    .populate('rangedData')
                                    .lean()
                                    .exec(function(err, school) {
                                        dataUtil.getByDateRange(
                                            school,
                                            'rangedData',
                                            startMoment,
                                            endMoment,
                                            function(err, flatSchool) {
                                                dataUtil.getByDateRange(
                                                    student,
                                                    'rangedData',
                                                    startMoment,
                                                    endMoment,
                                                    function(err, flat) {
                                                        if (!flat) {
                                                            cb(err, null);
                                                        } else {
                                                            cb(err, {
                                                                districtCode: district.districtCode,
                                                                distName: flatDistrict.name,
                                                                billingCode: flat.billingCode,
                                                                schoolName: flatSchool.name,
                                                                schoolCode: school.schoolCode,
                                                                grade: flat.grade,
                                                                primaryDisability: flat.primaryDisability,
                                                                primaryDisabilityCode: flat.primaryDisabilityCode,
                                                                secondaryDisability: flat.secondaryDisability,
                                                                secondaryDisabilityCode: flat.secondaryDisabilityCode,
                                                                age: flat.age,
                                                                gender: flat.gender,
                                                                studentId: flat.studentId,
                                                                stateId: flat.stateId,
                                                                sSNumber: flat.ssn,
                                                                studentFirstName: flat.firstName,
                                                                studentMiddleName: flat.middleName,
                                                                studentLastName: flat.lastName,
                                                                dateOfBirth: flat.dateOfBirth,
                                                                primaryOption: flat.primaryOption,
                                                                secondaryOption: flat.secondaryOption,
                                                                race: flat.race,
                                                                wPPRDate: flat.wpprDate,
                                                                eligibilityDate: flat.eligibilityDate,
                                                                lastIEPDate: flat.lastIEPDate,
                                                                iEPEndDate: flat.iepEndDate,
                                                                caseManager: flat.caseManager,
                                                                medicaidNumber: flat.medicaidId,
                                                                dateExited: flat.dateExited,
                                                                exitReason: flat.exitReason,
                                                                exitReasonCode: flat.exitReasonCode,
                                                                numOfIEPs: flat.numOfIEPs,
                                                                daysUntilEligible: flat.daysUntilEligible,
                                                                daysUntilIEP: flat.daysUntilIEP,
                                                                statusOfService: flat.statusOfService,
                                                                studentType: flat.studentType,
                                                                reasonLessFullService: flat.reasonLessFullService,
                                                                eligible: !!flat.eligible ? 'Yes' : 'No',
                                                                instrPercent: flat.instrPercent,
                                                                relPercent: flat.relPercent,
                                                                totPercent: flat.totPercent,
                                                                transportation: flat.transportation,
                                                                status: flat.status,
                                                                dateAdded: flat.dateAdded,
                                                                cenContract: flat.cenContract,
                                                                cenSeparate: flat.cenSeparate,
                                                                cenCorrectional: flat.cenCorrectional,
                                                                cenMaterialsOnly: flat.cenMaterialsOnly,
                                                                nextSchool: flat.nextSchool,
                                                                nextSchoolCode: flat.nextSchoolCode,
                                                                area: flat.area,
                                                                eISEnrollmentYear: flat.eisEnrollmentYear,
                                                                last504PlanDate: flat.last504PlanDate,
                                                                lastGradPlanDate: flat.lastGradPlanDate,
                                                                tEIDSLastIFSPDate: flat.teidsLastIFSPDate,
                                                                assignedSchoolCode: flat.assignedSchoolCode
                                                            });
                                                        }
                                                    });
                                            });
                                    });
                            });
                    });

            };
        });
    }

    self.commit = function(value, rowIndex, cb) {
        async.waterfall([

            function(cb) {
                context.Student.find({
                    studentId: value.studentId
                })
                    .populate('rangedData')
                    .exec(function(err, students) {
                        cb(err, students);

                    });
            },
            function(students, cb) {
                context.Student.populate(students, {
                    path: 'rangedData.school',
                    model: 'School'
                }, function(err, students) {
                    cb(err, students);
                });
            },
            function(students, cb) {
                context.Student.populate(students, {
                    path: 'rangedData.school.district',
                    model: 'District'
                }, function(err, students) {
                    cb(err, students);
                });
            },
            function(students, cb) {
                var startMoment = moment.tz(moment()
                        .format('YYYY-MM-DD'), self.clientTimezone),
                    endMoment = moment.tz(value.iEPEndDate, self.clientTimezone)
                    .endOf('day');
                students = _.compact(_.map(students, function(student) {
                    return dataUtil.getByDateRangeSync(student.toObject(), 'rangedData', startMoment, endMoment);
                }));
                var student = _.find(students, function(student) {
                    return student.school.district.districtCode == value.districtCode;
                });

                if (!!student) {
                    context.Student.findOne({
                        _id: student._id
                    })
                        .populate('rangedData')
                        .exec(function(err, student) {
                            cb(err, student);
                        });
                } else {
                    cb(null, null);
                }
            }
        ], function(err, student) {
            if (!!err) {
                logger.error('uh... error: %s', err);
                self.emit('commit error', {});
                cb(err, false);
            }
            if (!student) {
                logger.debug('student ' + value.studentLastName + ' is new.  Creating.');
                student = new context.Student();
                student.rangedData = [];
            } else {
                logger.debug('student ' + value.studentLastName + ' is not new.  Modifying.');
            }
            context.District.findOne({
                districtCode: value.districtCode
            })
                .exec(function(err, district) {
                    context.School.findOne({
                        schoolNumber: value.schoolCode,
                        district: district._id
                    }, function(err, school) {

                        //student.school = school.id;

                        student.gender = value.gender;
                        student.studentId = value.studentId;
                        student.stateId = value.stateId;
                        student.ssn = value.sSNumber;
                        //student.studentName = value.studentName;
                        student.firstName = value.studentFirstName;
                        student.middleName = value.studentMiddleName;
                        student.lastName = value.studentLastName;
                        student.dateOfBirth = value.dateOfBirth;
                        student.race = value.race;

                        var studentRange = new context.ranged.Student();
                        studentRange.school = school._id;


                        var startMoment = moment.tz(moment()
                                .format('YYYY-MM-DD'), self.clientTimezone),
                            endMoment = moment.tz(value.iEPEndDate, self.clientTimezone)
                            .endOf('day');

                        logger.silly('range: ' + startMoment.toString() + ' - ' + endMoment.toString())


                        studentRange.startDate = startMoment.toDate();
                        studentRange.endDate = endMoment.toDate();
                        logger.silly('studentRange: ' + studentRange.startDate + ' to ' + studentRange.endDate);
                        logger.silly('sr utc: ' + moment(studentRange.startDate)
                            .utc()
                            .format('YYYY-MM-DD HH:mm:SS a') + ' to ' + moment(studentRange.endDate)
                            .utc()
                            .format('YYYY-MM-DD HH:mm:SS a'));
                        studentRange.billingCode = value.billingCode;
                        studentRange.grade = value.grade;
                        studentRange.primaryDisability = value.primaryDisability;
                        studentRange.primaryDisabilityCode = value.primaryDisabilityCode;
                        studentRange.secondaryDisability = value.secondaryDisability;
                        studentRange.secondaryDisabilityCode = value.secondaryDisabilityCode;
                        studentRange.age = value.age;
                        studentRange.primaryOption = value.primaryOption;
                        studentRange.secondaryOption = value.secondaryOption;
                        studentRange.wpprDate = (value.wPPRDate || '') == '' ? null : new Date(value.wPPRDate);
                        studentRange.eligibilityDate = (value.eligibilityDate || '') == '' ? null : new Date(value.eligibilityDate);
                        studentRange.lastIEPDate = (value.lastIEPDate || '') == '' ? null : new Date(value.lastIEPDate);
                        studentRange.iepEndDate = (value.iEPEndDate || '') == '' ? null : new Date(value.iEPEndDate);
                        studentRange.caseManager = value.caseManager;
                        studentRange.medicaidId = value.medicaidNumber;
                        studentRange.dateExited = (value.dateExited || '') == '' ? null : new Date(value.dateExited);
                        studentRange.exitReason = value.exitReason;
                        studentRange.exitReasonCode = value.exitReasonCode;
                        studentRange.numOfIEPs = value.numOfIEPs;
                        studentRange.daysUntilEligible = value.daysUntilEligible;
                        studentRange.daysUntilIEP = value.daysUntilIEP;
                        studentRange.statusOfService = value.statusOfService;
                        studentRange.reasonLessFullService = value.reasonLessFullService;
                        studentRange.eligible = value.eligible;
                        studentRange.instrPercent = value.instrPercent;
                        studentRange.relPercent = value.relPercent;
                        studentRange.totPercent = value.totPercent;
                        studentRange.transportation = value.transportation;
                        studentRange.status = value.status;
                        studentRange.dateAdded = value.dateAdded;
                        studentRange.cenContract = value.cenContract;
                        studentRange.cenSeparate = value.cenSeparate;
                        studentRange.cenCorrectional = value.cenCorrectional;
                        studentRange.cenMaterialsOnly = value.cenMaterialsOnly;
                        studentRange.nextSchool = value.nextSchool;
                        studentRange.nextSchoolCode = value.nextSchoolCode;
                        studentRange.area = value.area;
                        studentRange.eisEnrollmentYear = value.eISEnrollmentYear;
                        studentRange.last504PlanDate = (value.last504PlanDate || '') == '' ? null : new Date(value.last504PlanDate);
                        studentRange.lastGradPlanDate = (value.lastGradPlanDate || '') == '' ? null : new Date(value.lastGradPlanDate);
                        studentRange.teidsLastIFSPDate = (value.tEIDSLastIFSPDate || '') == '' ? null : new Date(value.tEIDSLastIFSDate);
                        studentRange.assignedSchoolCode = value.assignedSchoolCode;
                        studentRange.studentType = value.studentType;



                        dataUtil.dateSplice(student.rangedData, studentRange, function(err, rangesToSave) {
                            if (!!err) {
                                logger.error(err);
                            }


                            dataUtil.dateSpliceSave(self.actor, student,
                                rangesToSave,
                                'rangedData',
                                function(err, savedStudent) {
                                    if (!!err) {
                                        logger.error('error importing student: %s', util.inspect(err));
                                        logger.debug('error rowdata info: %s', util.inspect(value));
                                        logger.debug('error district: %s', util.inspect(student));
                                        cb(err);
                                    } else {
                                        cb(null);
                                    }
                                });
                        });
                    });
                });

        });
    };
    self.sheet.rowValidators = [{
        validator: function(value, cb) {
            context.District.findOne({
                districtCode: value.districtCode
            }, function(err, district) {
                if (!!err) {
                    logger.error('uh... error: %s', err);
                    cb(err, false);
                } else if (!district) {
                    logger.warn('district did not exist for school');
                    //let the district validator catch it
                    cb(null, true);
                } else {

                    //check school and district
                    context.School.findOne({
                        schoolNumber: value.schoolCode,
                        district: district._id
                    })
                        .exec(function(err, school) {
                            if (!!err) {
                                logger.error('uh... error: %s', err);
                                cb(err, false);
                            }
                            if (!school) {
                                cb(null, false);
                            } else {
                                cb(null, true);
                            }
                        });
                }
            });
        },
        errorMessage: '[error messages for student needed - school validation failed]'
    }, {
        validator: function(value, callback) {
            //validate district exists
            context.District.findOne({
                districtCode: value.districtCode
            }, function(err, district) {
                if (!!err) {
                    logger.error('uh... error: %s', err);
                    callback(err, false);
                } else if (!district) {
                    logger.warn('district did not exist for school');
                    callback(null, false);
                } else {
                    callback(null, true);
                }
            });
        },
        errorMessage: '[error message for school needed - district not found]'
    }];
    return self;
};
StudentImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = StudentImport;
