var logger = require('winston'),
    mongoose = require('mongoose'),
    context = require('../models/context'),
    async = require('async'),
    moment = require('moment-timezone'),
    _ = require('lodash'),
    util = require('util'),
    dataUtil = require('../models/dataUtil');

var McoQualificationImport = function(actor, clientTimezone) {
    var self = this;
    self.actor = actor || "[import]";
    self.clientTimezone = clientTimezone;
    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'MCO Qualification Import',
        className: 'mcoQualification',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'District Code',
            propertyName: 'districtCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for DistrictCode needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for DistrictCode]'
            }]
        }, {
            name: 'Therapist Name',
            propertyName: 'therapistName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Therapist Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Therapist Name]'
            }]
        }, {
            name: 'MCO',
            propertyName: 'mco',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for MCO needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for MCO]'
            }]
        }, {
            name: 'Start Date',
            propertyName: 'startDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Start Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Start Date]'
            }]
        }, {
            name: 'End Date',
            propertyName: 'endDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for End Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for End Date]'
            }]
        }, ]
    };

    self.commit = function(value, rowIndex, cb) {
        var tasks = {
            user: function(cb) {
                var nameParts = value.therapistName.split(' ');
                context.User.findOne({
                    firstName: new RegExp(nameParts[0], 'i'),
                    lastName: new RegExp(nameParts[1], 'i')
                })
                    .populate('mcoQualifications')
                    .exec(function(err, therapist) {
                        if (!!err) {
                            logger.error('error getting provider: ' + util.inspect(err));
                        }
                        cb(err, therapist);
                    });
            },
            MCO: function(cb) {
                async.waterfall([

                    function(cb) {
                        context.District.findOne({
                            districtCode: value.districtCode
                        })
                            .exec(function(err, district) {
                                context.MCO.findOne({
                                    district: district._id,
                                    name: value.mco
                                })
                                    .exec(function(err, mco) {
                                        cb(err, mco);
                                    })
                            });
                    }
                ], function(err, mco) {
                    cb(err, mco);
                });
            }
        };

        async.parallel(tasks, function(err, r) {
            var mcoqual = _.find(r.user.mcoQualifications, function(qual) {
                return qual.mco == r.MCO.id;
            });

            var isnew = false;
            if (!mcoqual) {
                isnew = true;
                mcoqual = new context.MCOQualification();
            }

            var startMoment = moment.tz(value.startDate, self.clientTimezone)
                .startOf('day'),
                endMoment = moment.tz(value.endDate, self.clientTimezone)
                .endOf('day');



            logger.silly('range: ' + startMoment.toString() + ' - ' + endMoment.toString())

            mcoqual.mco = r.MCO.id;
            mcoqual.startDate = startMoment.toDate();
            mcoqual.endDate = endMoment.toDate();

            mcoqual.save(function(err, mcoqual) {
                if (isnew) {
                    r.user.mcoQualifications.push(mcoqual);
                    r.user.save(function(err) {
                        cb(err);
                    });
                } else {
                    cb(err);
                }
            })
        });
    };

    self.sheet.rowValidators = [{
        validator: function(value, cb) {
            logger.silly('validator for mcoQualification needed');
            cb(null, true);
        },
        errorMessage: '[error messages for mcoQualification needed]'
    }];
    return self;
};
McoQualificationImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = McoQualificationImport;
