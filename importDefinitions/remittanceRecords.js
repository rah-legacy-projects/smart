var logger = require('winston'),
    _ = require('lodash'),
    context = require('../models/context'),
    moment = require('moment'),
    dataUtil = require('../models/dataUtil'),
    async = require('async');


var RemittanceRecordsImport = function(actor, clientTimezone) {
    var self = this;
    self.actor = actor || '[unknown]';
    self.clientTimezone = clientTimezone;
    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'Remittance Records Import',
        className: 'remittanceRecords',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'Recipient Number',
            propertyName: 'recipientNumber',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Recipient Number needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Recipient Number]'
            }]
        }, {
            name: 'Recipient Last Name',
            propertyName: 'recipientLastName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Recipient Last Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Recipient Last Name]'
            }]
        }, {
            name: 'Recipient Last Initial',
            propertyName: 'recipientLastInitial',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Recipient Last Initial needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Recipient Last Initial]'
            }]
        }, {
            name: 'Recipient Middle Initial',
            propertyName: 'recipientMiddleInitial',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Recipient Middle Initial needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Recipient Middle Initial]'
            }]
        }, {
            name: 'Start Date',
            propertyName: 'startDate',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Start Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\d{8}/,
                errorMessage: '[rules needed for Start Date]'
            }]
        }, {
            name: 'End Date',
            propertyName: 'endDate',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for End Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\d{8}/,
                errorMessage: '[rules needed for End Date]'
            }]
        }, {
            name: 'Units',
            propertyName: 'units',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Units needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Units]'
            }]
        }, {
            name: 'Procedure Code',
            propertyName: 'procedureCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Procedure Code needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Procedure Code]'
            }]
        }, {
            name: 'Echo ID',
            propertyName: 'echoId',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for echo identifier needed]',
                ordinal: 0
            }],
            validators: [{
                validator: function(value, cb) {
                    logger.silly('echo id to check: ' + value);
                    if (/^[0-9a-fA-F]{8}\d+$/.test(value)) {
                        logger.silly('echo id matches identifier rule');
                        context.rollup.Billed.find({
                            billingGroup: new RegExp(value, 'i')
                        })
                            .exec(function(err, r) {
                                if (!!err) {
                                    logger.error(err);
                                }
                                logger.silly('result: ' + !!r);
                                logger.silly('result length: ' + r.length);
                                cb(err, !!r && r.length > 0);
                            });
                    } else {
                        logger.silly('echo id does not match identifier rule, looking for student id');
                        context.rollup.Billed.find({
                            studentId: value
                        })
                            .exec(function(err, r) {
                                cb(err, !!r && r.length > 0);
                            });
                    }
                },
                errorMessage: '[rules needed for echo identifier]'
            }]
        }, {
            name: 'Procedure',
            propertyName: 'procedure',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Procedure needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Procedure]'
            }]
        }, {
            name: 'Length',
            propertyName: 'length',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Length needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Length]'
            }]
        }, {
            name: 'Amount Billed',
            propertyName: 'amountBilled',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Amount Billed needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Amount Billed]'
            }]
        }, {
            name: 'Amount Allowed',
            propertyName: 'amountAllowed',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Amount Allowed needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Amount Allowed]'
            }]
        }, {
            name: 'Deductions',
            propertyName: 'deductions',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Deductions needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Deductions]'
            }]
        }, {
            name: 'Amount Paid',
            propertyName: 'amountPaid',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Amount Paid needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Amount Paid]'
            }]
        }, {
            name: 'Control Number',
            propertyName: 'controlNumber',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Control Number needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Control Number]'
            }]
        }, {
            name: 'Error Number',
            propertyName: 'errorNumber',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Error Number needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Error Number]'
            }]
        }, ]
    };
    self.commit = function(value, rowIndex, cb) {

        async.waterfall([


            function(cb) {
                if (/^[0-9a-fA-F]{8}\d+$/.test(value.echoId)) {
                    logger.silly('passing echo ID regex');
                    context.rollup.Billed.find({
                        billingGroup: new RegExp(value.echoId, 'i')
                    })
                        .exec(cb);
                } else {
                    context.rollup.Billed.find({
                        studentId: value.echoId
                    })
                        .exec(cb);
                }
            },
            function(billedRecords, cb) {
                logger.silly('about to sort ' + billedRecords.length + ' records');
                //order the records by date then by created
                billedRecords = _.sortBy(billedRecords, function(b) {
                    return moment(b.dateTime)
                        .format('YYYYMMDD') + '_' + moment(b.created)
                        .format('YYYYMMDDHHmmSS');
                });
                cb(null, billedRecords);
            },
            function(billedRecords, cb) {
                logger.silly('billed records: ' + billedRecords.length);
                var record = _.find(billedRecords, function(b) {
                    //if already billed, skip
                    if (!!b.amountBilled) {
                        logger.silly('record previously billed, skipping');
                        return false;
                    }

                    var valueStart = moment(value.startDate, 'YYYYMMDD');
                    var valueEnd = moment(value.endDate, 'YYYYMMDD');

                    logger.silly('units: ' + b.billedIEPUnits + ' == ' + value.units)
                    logger.silly('start date: ' + moment(b.dateTime)
                        .format('YYMMDD') + ' == ' + valueStart
                        .format('YYMMDD'));
                    logger.silly('end date: ' + moment(b.dateTime)
                        .format('YYMMDD') + ' == ' + valueEnd
                        .format('YYMMDD'));
                    logger.silly('procedure code: ' + b.procedureCodeCode + ' == ' + value.procedureCode);

                    var retval = b.billedIEPUnits == value.units &&
                        moment(b.dateTime)
                        .format('YYMMDD') == valueStart
                        .format('YYMMDD') &&
                        moment(b.dateTime)
                        .format('YYMMDD') == valueEnd
                        .format('YYMMDD') &&
                        b.procedureCodeCode == value.procedureCode;

                    return retval;

                });
                cb(null, record);

            },
            function(rollups, cb) {
                if (/^\s*$/.test(value.errorCodes)) {
                    cb(null, []);
                } else {
                    var errorCodes = _.map(_.compact(value.errorNumber.toString()
                        .split(',')), function(e) {
                        return e.replace(/\s/, '');
                    });
                    context.RemittanceError.find()
                        .where('code')
                        .in(errorCodes)
                        .exec(function(err, codes) {
                            cb(err, rollups, codes);
                        });
                }
            }
        ], function(err, record, codes) {

            record.amountBilled = value.amountBilled;
            record.amountAllowed = value.amountAllowed;
            record.deductions = value.deductions;
            record.amountPaid = value.amountPaid;
            record.controlNumber = value.controlNumber;
            _.each(codes, function(code) {
                record.remittanceErrors.push(code._id);
            });


            record.save(function(err) {

                cb(err, true);
            });
        });
    };
    self.sheet.rowValidators = [{
        validator: function(value, cb) {

            async.waterfall([

                function(cb) {
                    if (/^[0-9a-fA-F]{8}\d+$/.test(value.echoId)) {
                        logger.silly('passing echo ID regex');
                        context.rollup.Billed.find({
                            billingGroup: new RegExp(value.echoId, 'i')
                        })
                            .exec(cb);
                    } else {
                        context.rollup.Billed.find({
                            studentId: value.echoId
                        })
                            .exec(cb);
                    }
                },
                function(billedRecords, cb) {
                    logger.silly('about to sort ' + billedRecords.length + ' records');
                    //order the records by date then by created
                    billedRecords = _.sortBy(billedRecords, function(b) {
                        return moment(b.dateTime)
                            .format('YYYYMMDD') + '_' + moment(b.created)
                            .format('YYYYMMDDHHmmSS');
                    });
                    cb(null, billedRecords);
                },
                function(billedRecords, cb) {
                    logger.silly('billed records: ' + billedRecords.length);
                    var record = _.find(billedRecords, function(b) {
                        //if already billed, skip
                        if (!!b.amountBilled) {
                            logger.silly('record previously billed, skipping');
                            return false;
                        }

                        var valueStart = moment(value.startDate, 'YYYYMMDD');
                        var valueEnd = moment(value.endDate, 'YYYYMMDD');

                        logger.silly('units: ' + b.billedIEPUnits + ' == ' + value.units)
                        logger.silly('start date: ' + moment(b.dateTime)
                            .format('YYMMDD') + ' == ' + valueStart
                            .format('YYMMDD'));
                        logger.silly('end date: ' + moment(b.dateTime)
                            .format('YYMMDD') + ' == ' + valueEnd
                            .format('YYMMDD'));
                        logger.silly('procedure code: ' + b.procedureCodeCode + ' == ' + value.procedureCode);

                        var retval = b.billedIEPUnits == value.units &&
                            moment(b.dateTime)
                            .format('YYMMDD') == valueStart
                            .format('YYMMDD') &&
                            moment(b.dateTime)
                            .format('YYMMDD') == valueEnd
                            .format('YYMMDD') &&
                            b.procedureCodeCode == value.procedureCode;

                        return retval;

                    });
                    cb(null, record);
                }
            ], function(err, r) {
                logger.silly(value.echoId + ': ' + !!r);
                cb(err, !!r);
            });
        },
        errorMessage: '[error messages for remittanceRecords needed]'
    }, {
        validator: function(value, cb) {
            if (!value.errorNumber || /^\s*$/.test(value.errorNumber)) {
                cb(null, true);
                return;
            } else {
                var errorCodes = _.map(_.compact(value.errorNumber.toString()
                    .split(',')), function(e) {
                    return e.replace(/\s/, '');
                });
                context.RemittanceError.find()
                    .where('code')
                    .in(errorCodes)
                    .exec(function(err, codes) {
                        cb(err, codes.length === errorCodes.length);
                    });
            }
        },
        errormessage: '[some of the error codes could not be found]'
    }];
    return self;
};
RemittanceRecordsImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = RemittanceRecordsImport;
