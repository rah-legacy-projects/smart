var logger = require('winston'),
    mongoose = require('mongoose'),
    context = require('../models/context'),
    async = require('async'),
    moment = require('moment-timezone'),
    _ = require('lodash'),
    util = require('util'),
    dataUtil = require('../models/dataUtil');

var StudentDiagnosticCodeImport = function(actor, clientTimezone) {
    var self = this;
    self.actor = actor || "[import]";
    self.clientTimezone= clientTimezone;
    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'Student Diagnostic Code Import',
        className: 'studentDiagnosticCode',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'District Code',
            propertyName: 'districtCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for District Code needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for District Code]'
            }]
        }, {
            name: 'Student ID',
            propertyName: 'studentId',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Student ID needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Student ID]'
            }]
        }, {
            name: 'Specialty',
            propertyName: 'specialty',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Specialty needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Specialty]'
            }]
        }, {
            name: 'Diagnostic Code',
            propertyName: 'diagnosticCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Diagnostic Code needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Diagnostic Code]'
            }]
        }, ]
    };
    self.commit = function(value, rowIndex, cb) {
        var tasks = [

            function(cb) {
                context.District.findOne({
                    districtCode: value.districtCode
                })
                    .lean()
                    .exec(function(err, district) {
                        cb(err, district);
                    });
            },
            function(district, cb) {

                context.Student.find({
                    studentId: value.studentId
                })
                    .populate('rangedData')
                    .exec(function(err, students) {
                        async.waterfall([

                            function(cb) {
                                context.School.populate(students, {
                                    path: 'rangedData.school',
                                    model: 'School'
                                }, cb);
                            },
                            function(students, cb) {
                                context.School.populate(students, {
                                    path: 'rangedData.school.district',
                                    model: 'District'
                                }, cb);
                            }
                        ], function(err, students) {
                            var student = _.find(students, function(student) {
                                return _.any(student.rangedData, function(range) {
                                    return range.school.district.id.toString() == district._id;
                                });
                            });
                            cb(err, district, student);
                        });

                    });
            },
            function(district, student, cb) {
                context.DiagnosticCode.findOne({
                    district: district._id,
                    diagnosticCode: value.diagnosticCode
                })
                    .lean()
                    .exec(function(err, diagnosticCode) {
                        cb(err, district, student, diagnosticCode);
                    });
            },
            function(district, student, diagnosticCode, cb) {
                context.Service.findOne({
                    service: value.specialty,
                    student: student.id
                })
                    .populate('rangedData')
                    .exec(function(err, service) {
                        cb(err, diagnosticCode, service);
                    });
            }
        ];

        async.waterfall(tasks, function(err, diagnosticCode, service) {
            logger.silly('dc: %s, svc: %s', !!diagnosticCode, !!service);
            var saves = [];
            _.each(service.rangedData, function(serviceRange) {
                saves.push(function(cb) {
                    serviceRange.diagnosticCode = diagnosticCode._id;
                    serviceRange.save(function(err) {
                        cb(err);
                    })
                });
            });
            async.parallel(saves, function(err) {
                cb(err, true);
            });
        });
    };
    self.sheet.rowValidators = [{
        validator: function(value, cb) {




            var tasks = [

                function(cb) {
                    context.District.findOne({
                        districtCode: value.districtCode
                    })
                        .lean()
                        .exec(function(err, district) {
                            cb(err, district);
                        });
                },
                function(district, cb) {

                    context.Student.find({
                        studentId: value.studentId
                    })
                        .populate('rangedData')
                        .exec(function(err, students) {
                            async.waterfall([

                                function(cb) {
                                    context.School.populate(students, {
                                        path: 'rangedData.school',
                                        model: 'School'
                                    }, cb);
                                },
                                function(students, cb) {
                                    context.School.populate(students, {
                                        path: 'rangedData.school.district',
                                        model: 'District'
                                    }, cb);
                                }
                            ], function(err, students) {
                                var student = _.find(students, function(student) {
                                    return _.any(student.rangedData, function(range) {
                                        return range.school.district.id.toString() == district._id;
                                    });
                                });
                                cb(err, district, student);
                            });

                        });
                },
                function(district, student, cb) {
                    context.DiagnosticCode.findOne({
                        district: district._id,
                        diagnosticCode: value.diagnosticCode
                    })
                        .lean()
                        .exec(function(err, diagnosticCode) {
                            cb(err, district, student, diagnosticCode);
                        });
                },
                function(district, student, diagnosticCode, cb) {
                    context.Service.findOne({
                        service: value.specialty,
                        student: student.id
                    })
                        .populate('rangedData')
                        .exec(function(err, service) {
                            cb(err, diagnosticCode, service);
                        });
                }
            ];

            async.waterfall(tasks, function(err, diagnosticCode, service) {
                logger.silly('dc: %s, svc: %s', !!diagnosticCode, !!service);
                cb(err, !!diagnosticCode && !!service);
            });
        },
        errorMessage: '[error messages for studentDiagnosticCode needed]'
    }];
    return self;
};
StudentDiagnosticCodeImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = StudentDiagnosticCodeImport;
