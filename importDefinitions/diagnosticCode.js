var logger = require('winston'),
    mongoose = require('mongoose'),
    async = require('async'),
    moment = require('moment-timezone'),
    _ = require('lodash'),
    context = require('../models/context'),
    dataUtil = require('../models/dataUtil'),
    util = require('util');

var DiagnosticCodeImport = function(actor, clientTimezone) {
    var self = this;
    self.actor = actor || "[import]";
    self.clientTimezone = clientTimezone;

    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'Diagnostic Code Import',
        className: 'diagnosticCode',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'Service Type',
            propertyName: 'therapy',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for therapy needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for therapy]'
            }]
        }, {
            name: 'ICD Code',
            propertyName: 'icdCode',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for ICD Code needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for ICD Code]'
            }]
        }, {
            name: 'Description',
            propertyName: 'description',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Description needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Description]'
            }]
        }, {
            name: 'DistCode',
            propertyName: 'districtCode',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for DistCode needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\w+/,
                errorMessage: '[rules needed for DistCode]'
            }]
        }, {
            name: 'Start Date',
            propertyName: 'startDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Start Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))$/,
                errorMessage: '[rules needed for Start Date]'
            }]
        }, {
            name: 'End Date',
            propertyName: 'endDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for End Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))$/,
                errorMessage: '[rules needed for End Date]'
            }]
        }]
    };

    self.diff = function(value, index, cb) {
        //get the district
        context.District.findOne({
            districtCode: value.districtCode
        }, function(err, district) {
            //get the diagnostic code based on the icd and district
            context.DiagnosticCode.findOne({
                diagnosticCode: value.icdCode,
                district: district.id
            })
                .populate('rangedData')
                .lean()
                .exec(function(err, diagnosticCode) {
                    if (!!err) {
                        logger.error('uh... error: %s', err);
                        self.emit('commit error', {});
                        cb(err, null);
                        return;
                    }
                    if (!diagnosticCode) {
                        cb(null, null);
                    } else {
                        var startMoment = moment.tz(value.startDate, self.clientTimezone)
                            .startOf('day'),
                            endMoment = moment.tz(value.endDate, self.clientTimezone)
                            .endOf('day');

                        logger.silly('diff range: ' + startMoment.toString() + ' - ' + endMoment.toString());

                        dataUtil.getByDateRange(
                            diagnosticCode,
                            'rangedData',
                            startMoment,
                            endMoment,
                            function(err, flat) {
                                if (!flat) {
                                    cb(err, null);
                                } else {
                                    cb(err, {
                                        startDate: flat.startDate,
                                        endDate: flat.endDate,
                                        therapy: flat.therapy,
                                        icdCode: flat.diagnosticCode,
                                        description: flat.description,
                                        districtCode: district.districtCode
                                    });
                                }
                            });
                    }
                });
        });
    };

    self.commit = function(value, rowIndex, cb) {

        //get the district
        context.District.findOne({
            districtCode: value.districtCode
        }, function(err, district) {
            //get the diagnostic code based on the icd and district
            context.DiagnosticCode.findOne({
                diagnosticCode: value.icdCode,
                district: district.id
            })
                .populate('rangedData')
                .exec(function(err, diagnosticCode) {
                    if (!!err) {
                        logger.error('uh... error: %s', err);
                        self.emit('commit error', {});
                        cb(err, false);
                        return;
                    }
                    if (!diagnosticCode) {
                        logger.debug('diagnostic code ' + value.icdCode + ' is new.  Creating.');
                        diagnosticCode = new context.DiagnosticCode();
                        diagnosticCode.rangedData = [];
                    } else {
                        logger.debug('diagnostic code ' + value.icdCode + ' is not new.  Modifying.');
                    }

                    //get the related procedure code
                    //set up diagnostic code
                    diagnosticCode.diagnosticCode = value.icdCode;
                    diagnosticCode.district = district.id;

                    var diagnosticCodeRange = new context.ranged.DiagnosticCode();

                    diagnosticCodeRange.description = value.description;
                    diagnosticCodeRange.therapy = value.therapy;

                    var startMoment = moment.tz(value.startDate, self.clientTimezone)
                        .startOf('day'),
                        endMoment = moment.tz(value.endDate, self.clientTimezone)
                        .endOf('day');



                    logger.silly('range: ' + startMoment.toString() + ' - ' + endMoment.toString())

                    //set up start/end dates
                    diagnosticCodeRange.startDate = startMoment.toDate();
                    diagnosticCodeRange.endDate = endMoment.toDate();

                    dataUtil.dateSplice(diagnosticCode.rangedData,
                        diagnosticCodeRange,
                        function(err, rangesToSave) {
                            dataUtil.dateSpliceSave(self.actor, diagnosticCode,
                                rangesToSave,
                                'rangedData',
                                function(err, savedDiagnosticCode) {

                                    if (!!err) {
                                        logger.error('error importing diagnostic code: %s', util.inspect(err));
                                        logger.debug('error rowdata info: %s', util.inspect(value));
                                        logger.debug('error district: %s', util.inspect(diagnosticCode));
                                        cb(err);
                                    } else {
                                        cb(null);
                                    }
                                });
                        });
                });

        });
    };
    self.sheet.rowValidators = [{
        validator: function(value, callback) {
            //validate district exists
            context.District.findOne({
                districtCode: value.districtCode
            }, function(err, district) {
                if (!!err) {
                    logger.error('uh... error: %s', err);
                    callback(err, false);
                } else if (!district) {
                    logger.warn('district did not exist for diagnostic code');
                    callback(null, false);
                } else {
                    callback(null, true);
                }
            });
        },
        errorMessage: '[error message for diagnostic code needed - district not found]'
    }];
    return self;
};
DiagnosticCodeImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = DiagnosticCodeImport;
