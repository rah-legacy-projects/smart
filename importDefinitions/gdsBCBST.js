var logger = require('winston'),
    context = require('../models/context'),
    dataUtil = require('../models/dataUtil'),
    moment = require('moment-timezone'),
    _ = require('lodash'),
    async = require('async'),
    util = require('util');

var GdsBCBSTImport = function(actor, clientTimezone) {
    var self = this;
    self.clientTimezone = clientTimezone;
    self.actor = actor || "[import]";
    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'GDS BCBST Import',
        className: 'gdsBCBST',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'PageNum',
            propertyName: 'pageNum',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PageNum needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PageNum]'
            }]
        }, {
            name: 'DataSource',
            propertyName: 'dataSource',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for DataSource needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for DataSource]'
            }]
        }, {
            name: 'AccountNbr_In',
            propertyName: 'accountNbr_In',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for AccountNbr_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for AccountNbr_In]'
            }]
        }, {
            name: 'RecipID1_In',
            propertyName: 'recipID1_In',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for RecipID1_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for RecipID1_In]'
            }]
        }, {
            name: 'SSN_Inp',
            propertyName: 'sSN_Inp',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for SSN_Inp needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for SSN_Inp]'
            }]
        }, {
            name: 'LName1_In',
            propertyName: 'lName1_In',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for LName1_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for LName1_In]'
            }]
        }, {
            name: 'FName1_In',
            propertyName: 'fName1_In',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for FName1_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for FName1_In]'
            }]
        }, {
            name: 'MI1_In',
            propertyName: 'mI1_In',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for MI1_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for MI1_In]'
            }]
        }, {
            name: 'DOB_In',
            propertyName: 'dOB_In',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for DOB_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for DOB_In]'
            }]
        }, {
            name: 'Gender_In',
            propertyName: 'gender_In',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Gender_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Gender_In]'
            }]
        }, {
            name: 'CustDefined_In',
            propertyName: 'custDefined_In',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for CustDefined_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: function(value, callback) {
                    async.parallel({
                        byId: function(cb) {
                            context.Student.findOne({
                                _id: value
                            })
                                .exec(function(err, student) {
                                    if (!!err) {
                                        //logger.error(err);
                                    }
                                    //ignore if the value was not an objid
                                    cb(null, !!student);
                                })
                        },
                        byStudentId: function(cb) {
                            context.Student.findOne({
                                studentId: value
                            })
                                .exec(function(err, student) {
                                    if (!!err) {
                                        logger.error('by state id: ' + err);
                                    }
                                    cb(err, !!student);
                                });
                        }
                    }, function(err, r) {
                        callback(err, r.byId || r.byStudentId);
                    });
                },
                errorMessage: '[rules needed for CustDefined_In]'
            }]
        }, {
            name: 'Beg_In',
            propertyName: 'beg_In',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Beg_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/, //todo: validate as a date?
                errorMessage: '[rules needed for Beg_In]'
            }]
        }, {
            name: 'End_In',
            propertyName: 'end_In',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for End_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/, //todo: validate as a date?
                errorMessage: '[rules needed for End_In]'
            }]
        }, {
            name: 'Los_In',
            propertyName: 'los_In',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Los_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Los_In]'
            }]
        }, {
            name: 'ErrorMsg',
            propertyName: 'errorMsg',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for ErrorMsg needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for ErrorMsg]'
            }]
        }, {
            name: 'StatusFlag',
            propertyName: 'statusFlag',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for StatusFlag needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for StatusFlag]'
            }]
        }, {
            name: 'McaidFlag',
            propertyName: 'mcaidFlag',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for McaidFlag needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /[YNyn]/,
                errorMessage: '[rules needed for McaidFlag]'
            }]
        }, {
            name: 'MCRA_Flag',
            propertyName: 'mCRA_Flag',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for MCRA_Flag needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for MCRA_Flag]'
            }]
        }, {
            name: 'MCRB_Flag',
            propertyName: 'mCRB_Flag',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for MCRB_Flag needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for MCRB_Flag]'
            }]
        }, {
            name: 'OtherPayor_Flag',
            propertyName: 'otherPayor_Flag',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayor_Flag needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayor_Flag]'
            }]
        }, {
            name: 'GDS_Ref_No',
            propertyName: 'gDS_Ref_No',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for GDS_Ref_No needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for GDS_Ref_No]'
            }]
        }, {
            name: 'SearchCode',
            propertyName: 'searchCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for SearchCode needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for SearchCode]'
            }]
        }, {
            name: 'Split_Serial_Num',
            propertyName: 'split_Serial_Num',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Split_Serial_Num needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Split_Serial_Num]'
            }]
        }, {
            name: 'PlanBeneNum',
            propertyName: 'planBeneNum',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PlanBeneNum needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for PlanBeneNum]'
            }]
        }, {
            name: 'SSN',
            propertyName: 'sSN',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for SSN needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for SSN]'
            }]
        }, {
            name: 'Lname',
            propertyName: 'lname',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Lname needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Lname]'
            }]
        }, {
            name: 'Fname',
            propertyName: 'fname',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Fname needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Fname]'
            }]
        }, {
            name: 'Mname',
            propertyName: 'mname',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Mname needed]',
                ordinal: 0
            }],
            validators: [{
                //todo: should mname be "required"?
                validator: /.*/,
                errorMessage: '[rules needed for Mname]'
            }]
        }, {
            name: 'DOB',
            propertyName: 'dOB',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for DOB needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for DOB]'
            }]
        }, {
            name: 'Gender',
            propertyName: 'gender',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Gender needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Gender]'
            }]
        }, {
            name: 'HICNum',
            propertyName: 'hICNum',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for HICNum needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for HICNum]'
            }]
        }, {
            name: 'OtherID',
            propertyName: 'otherID',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherID needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherID]'
            }]
        }, {
            name: 'Address1',
            propertyName: 'address1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Address1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Address1]'
            }]
        }, {
            name: 'Address2',
            propertyName: 'address2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Address2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Address2]'
            }]
        }, {
            name: 'City',
            propertyName: 'city',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for City needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for City]'
            }]
        }, {
            name: 'State',
            propertyName: 'state',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for State needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for State]'
            }]
        }, {
            name: 'Zip',
            propertyName: 'zip',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Zip needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Zip]'
            }]
        }, {
            name: 'McaidDays_1',
            propertyName: 'mcaidDays_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for McaidDays_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for McaidDays_1]'
            }]
        }, {
            name: 'BegDate_1',
            propertyName: 'begDate_1',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for BegDate_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for BegDate_1]'
            }]
        }, {
            name: 'EndDate_1',
            propertyName: 'endDate_1',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for EndDate_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for EndDate_1]'
            }]
        }, {
            name: 'Add_Date_1',
            propertyName: 'add_Date_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Add_Date_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Add_Date_1]'
            }]
        }, {
            name: 'Status_1',
            propertyName: 'status_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Status_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:1 active coverage|6 inactive|\s*)$/i,
                errorMessage: '[rules needed for Status_1]'
            }]
        }, {
            name: 'Coverage_1',
            propertyName: 'coverage_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Coverage_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Coverage_1]'
            }]
        }, {
            name: 'ServiceType_1',
            propertyName: 'serviceType_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for ServiceType_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for ServiceType_1]'
            }]
        }, {
            name: 'InsuranceType_1',
            propertyName: 'insuranceType_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for InsuranceType_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for InsuranceType_1]'
            }]
        }, {
            name: 'Plan_Desc_1',
            propertyName: 'plan_Desc_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Plan_Desc_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Plan_Desc_1]'
            }]
        }, {
            name: 'Message1_1',
            propertyName: 'message1_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Message1_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Message1_1]'
            }]
        }, {
            name: 'Message2_1',
            propertyName: 'message2_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Message2_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Message2_1]'
            }]
        }, {
            name: 'McaidDays_2',
            propertyName: 'mcaidDays_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for McaidDays_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for McaidDays_2]'
            }]
        }, {
            name: 'BegDate_2',
            propertyName: 'begDate_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for BegDate_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for BegDate_2]'
            }]
        }, {
            name: 'EndDate_2',
            propertyName: 'endDate_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for EndDate_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for EndDate_2]'
            }]
        }, {
            name: 'Add_Date_2',
            propertyName: 'add_Date_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Add_Date_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Add_Date_2]'
            }]
        }, {
            name: 'Status_2',
            propertyName: 'status_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Status_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Status_2]'
            }]
        }, {
            name: 'Coverage_2',
            propertyName: 'coverage_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Coverage_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Coverage_2]'
            }]
        }, {
            name: 'ServiceType_2',
            propertyName: 'serviceType_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for ServiceType_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for ServiceType_2]'
            }]
        }, {
            name: 'InsuranceType_2',
            propertyName: 'insuranceType_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for InsuranceType_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for InsuranceType_2]'
            }]
        }, {
            name: 'Plan_Desc_2',
            propertyName: 'plan_Desc_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Plan_Desc_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Plan_Desc_2]'
            }]
        }, {
            name: 'Message1_2',
            propertyName: 'message1_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Message1_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Message1_2]'
            }]
        }, {
            name: 'Message2_2',
            propertyName: 'message2_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Message2_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Message2_2]'
            }]
        }, {
            name: 'McaidDays_3',
            propertyName: 'mcaidDays_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for McaidDays_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for McaidDays_3]'
            }]
        }, {
            name: 'BegDate_3',
            propertyName: 'begDate_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for BegDate_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for BegDate_3]'
            }]
        }, {
            name: 'EndDate_3',
            propertyName: 'endDate_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for EndDate_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for EndDate_3]'
            }]
        }, {
            name: 'Add_Date_3',
            propertyName: 'add_Date_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Add_Date_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Add_Date_3]'
            }]
        }, {
            name: 'Status_3',
            propertyName: 'status_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Status_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Status_3]'
            }]
        }, {
            name: 'Coverage_3',
            propertyName: 'coverage_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Coverage_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Coverage_3]'
            }]
        }, {
            name: 'ServiceType_3',
            propertyName: 'serviceType_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for ServiceType_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for ServiceType_3]'
            }]
        }, {
            name: 'InsuranceType_3',
            propertyName: 'insuranceType_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for InsuranceType_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for InsuranceType_3]'
            }]
        }, {
            name: 'Plan_Desc_3',
            propertyName: 'plan_Desc_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Plan_Desc_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Plan_Desc_3]'
            }]
        }, {
            name: 'Message1_3',
            propertyName: 'message1_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Message1_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Message1_3]'
            }]
        }, {
            name: 'Message2_3',
            propertyName: 'message2_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Message2_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Message2_3]'
            }]
        }, {
            name: 'PartADays_1',
            propertyName: 'partADays_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartADays_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartADays_1]'
            }]
        }, {
            name: 'PartA_Beg_1',
            propertyName: 'partA_Beg_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartA_Beg_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartA_Beg_1]'
            }]
        }, {
            name: 'PartA_End_1',
            propertyName: 'partA_End_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartA_End_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartA_End_1]'
            }]
        }, {
            name: 'PartA_AddDate_1',
            propertyName: 'partA_AddDate_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartA_AddDate_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartA_AddDate_1]'
            }]
        }, {
            name: 'PartA_Status_1',
            propertyName: 'partA_Status_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartA_Status_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartA_Status_1]'
            }]
        }, {
            name: 'PartA_CovLevel_1',
            propertyName: 'partA_CovLevel_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartA_CovLevel_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartA_CovLevel_1]'
            }]
        }, {
            name: 'PartA_Desc_1',
            propertyName: 'partA_Desc_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartA_Desc_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartA_Desc_1]'
            }]
        }, {
            name: 'PartBDays_1',
            propertyName: 'partBDays_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartBDays_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartBDays_1]'
            }]
        }, {
            name: 'PartB_Beg_1',
            propertyName: 'partB_Beg_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartB_Beg_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartB_Beg_1]'
            }]
        }, {
            name: 'PartB_End_1',
            propertyName: 'partB_End_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartB_End_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartB_End_1]'
            }]
        }, {
            name: 'PartBAdd_1',
            propertyName: 'partBAdd_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartBAdd_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartBAdd_1]'
            }]
        }, {
            name: 'PartB_Status_1',
            propertyName: 'partB_Status_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartB_Status_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartB_Status_1]'
            }]
        }, {
            name: 'PartB_CovLevel_1',
            propertyName: 'partB_CovLevel_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartB_CovLevel_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartB_CovLevel_1]'
            }]
        }, {
            name: 'PartB_Desc_1',
            propertyName: 'partB_Desc_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartB_Desc_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartB_Desc_1]'
            }]
        }, {
            name: 'McaidDays',
            propertyName: 'mcaidDays',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for McaidDays needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for McaidDays]'
            }]
        }, {
            name: 'PartADays',
            propertyName: 'partADays',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartADays needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartADays]'
            }]
        }, {
            name: 'Mcaid_PartADays',
            propertyName: 'mcaid_PartADays',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Mcaid_PartADays needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Mcaid_PartADays]'
            }]
        }, {
            name: 'PartBDays',
            propertyName: 'partBDays',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartBDays needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartBDays]'
            }]
        }, {
            name: 'Mcaid_PartBDays',
            propertyName: 'mcaid_PartBDays',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Mcaid_PartBDays needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Mcaid_PartBDays]'
            }]
        }, {
            name: 'PartA_PartBDays',
            propertyName: 'partA_PartBDays',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartA_PartBDays needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartA_PartBDays]'
            }]
        }, {
            name: 'Mcaid_PartA_PartBDays',
            propertyName: 'mcaid_PartA_PartBDays',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Mcaid_PartA_PartBDays needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Mcaid_PartA_PartBDays]'
            }]
        }, {
            name: 'OtherPayerBegDate_1',
            propertyName: 'otherPayerBegDate_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerBegDate_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerBegDate_1]'
            }]
        }, {
            name: 'OtherPayerEndDate_1',
            propertyName: 'otherPayerEndDate_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerEndDate_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerEndDate_1]'
            }]
        }, {
            name: 'OtherPayerAdd_Date_1',
            propertyName: 'otherPayerAdd_Date_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerAdd_Date_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerAdd_Date_1]'
            }]
        }, {
            name: 'OtherPayerInfo_1',
            propertyName: 'otherPayerInfo_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerInfo_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerInfo_1]'
            }]
        }, {
            name: 'OtherPayerCoverage_1',
            propertyName: 'otherPayerCoverage_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerCoverage_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerCoverage_1]'
            }]
        }, {
            name: 'OtherPayerServiceType_1',
            propertyName: 'otherPayerServiceType_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerServiceType_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerServiceType_1]'
            }]
        }, {
            name: 'OtherPayerInsuranceType_1',
            propertyName: 'otherPayerInsuranceType_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerInsuranceType_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerInsuranceType_1]'
            }]
        }, {
            name: 'OtherPayerPlan_Desc_1',
            propertyName: 'otherPayerPlan_Desc_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerPlan_Desc_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerPlan_Desc_1]'
            }]
        }, {
            //other payer message 1_1 is the NPI number
            name: 'OtherPayerMessage1_1',
            propertyName: 'otherPayerMessage1_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerMessage1_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerMessage1_1]'
            }]
        }, {
            name: 'OtherPayerMessage2_1',
            propertyName: 'otherPayerMessage2_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerMessage2_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerMessage2_1]'
            }]
        }, {
            name: 'OtherPayerIdentifierCode_1',
            propertyName: 'otherPayerIdentifierCode_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerIdentifierCode_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerIdentifierCode_1]'
            }]
        }, {
            //other payer name 1 is the PCP name
            name: 'OtherPayer_Name_1',
            propertyName: 'otherPayer_Name_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_Name_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for OtherPayer_Name_1]'
            }]
        }, {
            name: 'OtherPayer_RefID_1',
            propertyName: 'otherPayer_RefID_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_RefID_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_RefID_1]'
            }]
        }, {
            name: 'OtherPayer_Tel_1',
            propertyName: 'otherPayer_Tel_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_Tel_1 needed]',
                ordinal: 0
            }],
            validators: [{
                //todo: validate PCP phone as phone?
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_Tel_1]'
            }]
        }, {
            name: 'OtherPayer_address1_1',
            propertyName: 'otherPayer_address1_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_address1_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_address1_1]'
            }]
        }, {
            name: 'OtherPayer_address2_1',
            propertyName: 'otherPayer_address2_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_address2_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_address2_1]'
            }]
        }, {
            name: 'OtherPayer_City_1',
            propertyName: 'otherPayer_City_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_City_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_City_1]'
            }]
        }, {
            name: 'OtherPayer_State_1',
            propertyName: 'otherPayer_State_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_State_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_State_1]'
            }]
        }, {
            name: 'OtherPayer_Zipcode_1',
            propertyName: 'otherPayer_Zipcode_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_Zipcode_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_Zipcode_1]'
            }]
        }, {
            name: 'OtherPayerBegDate_2',
            propertyName: 'otherPayerBegDate_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerBegDate_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerBegDate_2]'
            }]
        }, {
            name: 'OtherPayerEndDate_2',
            propertyName: 'otherPayerEndDate_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerEndDate_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerEndDate_2]'
            }]
        }, {
            name: 'OtherPayerAdd_Date_2',
            propertyName: 'otherPayerAdd_Date_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerAdd_Date_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerAdd_Date_2]'
            }]
        }, {
            name: 'OtherPayerInfo_2',
            propertyName: 'otherPayerInfo_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerInfo_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerInfo_2]'
            }]
        }, {
            name: 'OtherPayerCoverage_2',
            propertyName: 'otherPayerCoverage_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerCoverage_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerCoverage_2]'
            }]
        }, {
            name: 'OtherPayerServiceType_2',
            propertyName: 'otherPayerServiceType_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerServiceType_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerServiceType_2]'
            }]
        }, {
            name: 'OtherPayerInsuranceType_2',
            propertyName: 'otherPayerInsuranceType_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerInsuranceType_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerInsuranceType_2]'
            }]
        }, {
            name: 'OtherPayerPlan_Desc_2',
            propertyName: 'otherPayerPlan_Desc_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerPlan_Desc_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerPlan_Desc_2]'
            }]
        }, {
            name: 'OtherPayerMessage1_2',
            propertyName: 'otherPayerMessage1_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerMessage1_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerMessage1_2]'
            }]
        }, {
            name: 'OtherPayerMessage2_2',
            propertyName: 'otherPayerMessage2_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerMessage2_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerMessage2_2]'
            }]
        }, {
            name: 'OtherPayerIdentifierCode_2',
            propertyName: 'otherPayerIdentifierCode_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerIdentifierCode_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerIdentifierCode_2]'
            }]
        }, {
            name: 'OtherPayer_Name_2',
            propertyName: 'otherPayer_Name_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_Name_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_Name_2]'
            }]
        }, {
            name: 'OtherPayer_RefID_2',
            propertyName: 'otherPayer_RefID_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_RefID_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_RefID_2]'
            }]
        }, {
            name: 'OtherPayer_Tel_2',
            propertyName: 'otherPayer_Tel_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_Tel_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_Tel_2]'
            }]
        }, {
            name: 'OtherPayer_address1_2',
            propertyName: 'otherPayer_address1_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_address1_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_address1_2]'
            }]
        }, {
            name: 'OtherPayer_address2_2',
            propertyName: 'otherPayer_address2_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_address2_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_address2_2]'
            }]
        }, {
            name: 'OtherPayer_City_2',
            propertyName: 'otherPayer_City_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_City_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_City_2]'
            }]
        }, {
            name: 'OtherPayer_State_2',
            propertyName: 'otherPayer_State_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_State_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_State_2]'
            }]
        }, {
            name: 'OtherPayer_Zipcode_2',
            propertyName: 'otherPayer_Zipcode_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_Zipcode_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_Zipcode_2]'
            }]
        }, {
            name: 'OtherPayerBegDate_3',
            propertyName: 'otherPayerBegDate_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerBegDate_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerBegDate_3]'
            }]
        }, {
            name: 'OtherPayerEndDate_3',
            propertyName: 'otherPayerEndDate_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerEndDate_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerEndDate_3]'
            }]
        }, {
            name: 'OtherPayerAdd_Date_3',
            propertyName: 'otherPayerAdd_Date_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerAdd_Date_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerAdd_Date_3]'
            }]
        }, {
            name: 'OtherPayerInfo_3',
            propertyName: 'otherPayerInfo_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerInfo_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerInfo_3]'
            }]
        }, {
            name: 'OtherPayerCoverage_3',
            propertyName: 'otherPayerCoverage_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerCoverage_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerCoverage_3]'
            }]
        }, {
            name: 'OtherPayerServiceType_3',
            propertyName: 'otherPayerServiceType_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerServiceType_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerServiceType_3]'
            }]
        }, {
            name: 'OtherPayerInsuranceType_3',
            propertyName: 'otherPayerInsuranceType_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerInsuranceType_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerInsuranceType_3]'
            }]
        }, {
            name: 'OtherPayerPlan_Desc_3',
            propertyName: 'otherPayerPlan_Desc_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerPlan_Desc_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerPlan_Desc_3]'
            }]
        }, {
            name: 'OtherPayerMessage1_3',
            propertyName: 'otherPayerMessage1_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerMessage1_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerMessage1_3]'
            }]
        }, {
            name: 'OtherPayerMessage2_3',
            propertyName: 'otherPayerMessage2_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerMessage2_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerMessage2_3]'
            }]
        }, {
            name: 'OtherPayerIdentifierCode_3',
            propertyName: 'otherPayerIdentifierCode_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerIdentifierCode_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerIdentifierCode_3]'
            }]
        }, {
            name: 'OtherPayer_Name_3',
            propertyName: 'otherPayer_Name_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_Name_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_Name_3]'
            }]
        }, {
            name: 'OtherPayer_RefID_3',
            propertyName: 'otherPayer_RefID_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_RefID_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_RefID_3]'
            }]
        }, {
            name: 'OtherPayer_Tel_3',
            propertyName: 'otherPayer_Tel_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_Tel_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_Tel_3]'
            }]
        }, {
            name: 'OtherPayer_address1_3',
            propertyName: 'otherPayer_address1_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_address1_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_address1_3]'
            }]
        }, {
            name: 'OtherPayer_address2_3',
            propertyName: 'otherPayer_address2_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_address2_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_address2_3]'
            }]
        }, {
            name: 'OtherPayer_City_3',
            propertyName: 'otherPayer_City_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_City_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_City_3]'
            }]
        }, {
            name: 'OtherPayer_State_3',
            propertyName: 'otherPayer_State_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_State_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_State_3]'
            }]
        }, {
            name: 'OtherPayer_Zipcode_3',
            propertyName: 'otherPayer_Zipcode_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_Zipcode_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_Zipcode_3]'
            }]
        }, {
            name: 'OtherBenefitBegDate_1',
            propertyName: 'otherBenefitBegDate_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitBegDate_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitBegDate_1]'
            }]
        }, {
            name: 'OtherBenefitEndDate_1',
            propertyName: 'otherBenefitEndDate_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitEndDate_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitEndDate_1]'
            }]
        }, {
            name: 'OtherBenefitAdd_Date_1',
            propertyName: 'otherBenefitAdd_Date_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAdd_Date_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAdd_Date_1]'
            }]
        }, {
            name: 'OtherBenefitInfo_1',
            propertyName: 'otherBenefitInfo_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInfo_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInfo_1]'
            }]
        }, {
            name: 'OtherBenefitCoverage_1',
            propertyName: 'otherBenefitCoverage_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitCoverage_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitCoverage_1]'
            }]
        }, {
            name: 'OtherBenefitServiceType_1',
            propertyName: 'otherBenefitServiceType_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitServiceType_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitServiceType_1]'
            }]
        }, {
            name: 'OtherBenefitInsuranceType_1',
            propertyName: 'otherBenefitInsuranceType_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInsuranceType_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInsuranceType_1]'
            }]
        }, {
            name: 'OtherBenefitPlan_Desc_1',
            propertyName: 'otherBenefitPlan_Desc_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitPlan_Desc_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitPlan_Desc_1]'
            }]
        }, {
            name: 'OtherBenefitMessage1_1',
            propertyName: 'otherBenefitMessage1_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitMessage1_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitMessage1_1]'
            }]
        }, {
            name: 'OtherBenefitTimePeriodQualifier_1',
            propertyName: 'otherBenefitTimePeriodQualifier_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitTimePeriodQualifier_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitTimePeriodQualifier_1]'
            }]
        }, {
            name: 'OtherBenefitAmount_1',
            propertyName: 'otherBenefitAmount_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAmount_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAmount_1]'
            }]
        }, {
            name: 'OtherBenefitQuantity_1',
            propertyName: 'otherBenefitQuantity_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitQuantity_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitQuantity_1]'
            }]
        }, {
            name: 'OtherBenefitNetworkFlag_1',
            propertyName: 'otherBenefitNetworkFlag_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitNetworkFlag_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitNetworkFlag_1]'
            }]
        }, {
            name: 'OtherBenefitBegDate_2',
            propertyName: 'otherBenefitBegDate_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitBegDate_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitBegDate_2]'
            }]
        }, {
            name: 'OtherBenefitEndDate_2',
            propertyName: 'otherBenefitEndDate_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitEndDate_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitEndDate_2]'
            }]
        }, {
            name: 'OtherBenefitAdd_Date_2',
            propertyName: 'otherBenefitAdd_Date_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAdd_Date_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAdd_Date_2]'
            }]
        }, {
            name: 'OtherBenefitInfo_2',
            propertyName: 'otherBenefitInfo_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInfo_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInfo_2]'
            }]
        }, {
            name: 'OtherBenefitCoverage_2',
            propertyName: 'otherBenefitCoverage_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitCoverage_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitCoverage_2]'
            }]
        }, {
            name: 'OtherBenefitServiceType_2',
            propertyName: 'otherBenefitServiceType_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitServiceType_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitServiceType_2]'
            }]
        }, {
            name: 'OtherBenefitInsuranceType_2',
            propertyName: 'otherBenefitInsuranceType_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInsuranceType_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInsuranceType_2]'
            }]
        }, {
            name: 'OtherBenefitPlan_Desc_2',
            propertyName: 'otherBenefitPlan_Desc_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitPlan_Desc_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitPlan_Desc_2]'
            }]
        }, {
            name: 'OtherBenefitMessage1_2',
            propertyName: 'otherBenefitMessage1_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitMessage1_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitMessage1_2]'
            }]
        }, {
            name: 'OtherBenefitTimePeriodQualifier_2',
            propertyName: 'otherBenefitTimePeriodQualifier_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitTimePeriodQualifier_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitTimePeriodQualifier_2]'
            }]
        }, {
            name: 'OtherBenefitAmount_2',
            propertyName: 'otherBenefitAmount_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAmount_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAmount_2]'
            }]
        }, {
            name: 'OtherBenefitQuantity_2',
            propertyName: 'otherBenefitQuantity_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitQuantity_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitQuantity_2]'
            }]
        }, {
            name: 'OtherBenefitNetworkFlag_2',
            propertyName: 'otherBenefitNetworkFlag_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitNetworkFlag_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitNetworkFlag_2]'
            }]
        }, {
            name: 'OtherBenefitBegDate_3',
            propertyName: 'otherBenefitBegDate_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitBegDate_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitBegDate_3]'
            }]
        }, {
            name: 'OtherBenefitEndDate_3',
            propertyName: 'otherBenefitEndDate_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitEndDate_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitEndDate_3]'
            }]
        }, {
            name: 'OtherBenefitAdd_Date_3',
            propertyName: 'otherBenefitAdd_Date_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAdd_Date_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAdd_Date_3]'
            }]
        }, {
            name: 'OtherBenefitInfo_3',
            propertyName: 'otherBenefitInfo_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInfo_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInfo_3]'
            }]
        }, {
            name: 'OtherBenefitCoverage_3',
            propertyName: 'otherBenefitCoverage_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitCoverage_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitCoverage_3]'
            }]
        }, {
            name: 'OtherBenefitServiceType_3',
            propertyName: 'otherBenefitServiceType_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitServiceType_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitServiceType_3]'
            }]
        }, {
            name: 'OtherBenefitInsuranceType_3',
            propertyName: 'otherBenefitInsuranceType_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInsuranceType_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInsuranceType_3]'
            }]
        }, {
            name: 'OtherBenefitPlan_Desc_3',
            propertyName: 'otherBenefitPlan_Desc_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitPlan_Desc_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitPlan_Desc_3]'
            }]
        }, {
            name: 'OtherBenefitMessage1_3',
            propertyName: 'otherBenefitMessage1_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitMessage1_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitMessage1_3]'
            }]
        }, {
            name: 'OtherBenefitTimePeriodQualifier_3',
            propertyName: 'otherBenefitTimePeriodQualifier_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitTimePeriodQualifier_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitTimePeriodQualifier_3]'
            }]
        }, {
            name: 'OtherBenefitAmount_3',
            propertyName: 'otherBenefitAmount_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAmount_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAmount_3]'
            }]
        }, {
            name: 'OtherBenefitQuantity_3',
            propertyName: 'otherBenefitQuantity_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitQuantity_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitQuantity_3]'
            }]
        }, {
            name: 'OtherBenefitNetworkFlag_3',
            propertyName: 'otherBenefitNetworkFlag_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitNetworkFlag_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitNetworkFlag_3]'
            }]
        }, {
            name: 'OtherBenefitBegDate_4',
            propertyName: 'otherBenefitBegDate_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitBegDate_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitBegDate_4]'
            }]
        }, {
            name: 'OtherBenefitEndDate_4',
            propertyName: 'otherBenefitEndDate_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitEndDate_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitEndDate_4]'
            }]
        }, {
            name: 'OtherBenefitAdd_Date_4',
            propertyName: 'otherBenefitAdd_Date_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAdd_Date_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAdd_Date_4]'
            }]
        }, {
            name: 'OtherBenefitInfo_4',
            propertyName: 'otherBenefitInfo_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInfo_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInfo_4]'
            }]
        }, {
            name: 'OtherBenefitCoverage_4',
            propertyName: 'otherBenefitCoverage_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitCoverage_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitCoverage_4]'
            }]
        }, {
            name: 'OtherBenefitServiceType_4',
            propertyName: 'otherBenefitServiceType_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitServiceType_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitServiceType_4]'
            }]
        }, {
            name: 'OtherBenefitInsuranceType_4',
            propertyName: 'otherBenefitInsuranceType_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInsuranceType_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInsuranceType_4]'
            }]
        }, {
            name: 'OtherBenefitPlan_Desc_4',
            propertyName: 'otherBenefitPlan_Desc_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitPlan_Desc_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitPlan_Desc_4]'
            }]
        }, {
            name: 'OtherBenefitMessage1_4',
            propertyName: 'otherBenefitMessage1_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitMessage1_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitMessage1_4]'
            }]
        }, {
            name: 'OtherBenefitTimePeriodQualifier_4',
            propertyName: 'otherBenefitTimePeriodQualifier_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitTimePeriodQualifier_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitTimePeriodQualifier_4]'
            }]
        }, {
            name: 'OtherBenefitAmount_4',
            propertyName: 'otherBenefitAmount_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAmount_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAmount_4]'
            }]
        }, {
            name: 'OtherBenefitQuantity_4',
            propertyName: 'otherBenefitQuantity_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitQuantity_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitQuantity_4]'
            }]
        }, {
            name: 'OtherBenefitNetworkFlag_4',
            propertyName: 'otherBenefitNetworkFlag_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitNetworkFlag_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitNetworkFlag_4]'
            }]
        }, {
            name: 'OtherBenefitBegDate_5',
            propertyName: 'otherBenefitBegDate_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitBegDate_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitBegDate_5]'
            }]
        }, {
            name: 'OtherBenefitEndDate_5',
            propertyName: 'otherBenefitEndDate_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitEndDate_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitEndDate_5]'
            }]
        }, {
            name: 'OtherBenefitAdd_Date_5',
            propertyName: 'otherBenefitAdd_Date_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAdd_Date_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAdd_Date_5]'
            }]
        }, {
            name: 'OtherBenefitInfo_5',
            propertyName: 'otherBenefitInfo_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInfo_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInfo_5]'
            }]
        }, {
            name: 'OtherBenefitCoverage_5',
            propertyName: 'otherBenefitCoverage_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitCoverage_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitCoverage_5]'
            }]
        }, {
            name: 'OtherBenefitServiceType_5',
            propertyName: 'otherBenefitServiceType_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitServiceType_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitServiceType_5]'
            }]
        }, {
            name: 'OtherBenefitInsuranceType_5',
            propertyName: 'otherBenefitInsuranceType_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInsuranceType_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInsuranceType_5]'
            }]
        }, {
            name: 'OtherBenefitPlan_Desc_5',
            propertyName: 'otherBenefitPlan_Desc_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitPlan_Desc_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitPlan_Desc_5]'
            }]
        }, {
            name: 'OtherBenefitMessage1_5',
            propertyName: 'otherBenefitMessage1_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitMessage1_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitMessage1_5]'
            }]
        }, {
            name: 'OtherBenefitTimePeriodQualifier_5',
            propertyName: 'otherBenefitTimePeriodQualifier_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitTimePeriodQualifier_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitTimePeriodQualifier_5]'
            }]
        }, {
            name: 'OtherBenefitAmount_5',
            propertyName: 'otherBenefitAmount_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAmount_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAmount_5]'
            }]
        }, {
            name: 'OtherBenefitQuantity_5',
            propertyName: 'otherBenefitQuantity_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitQuantity_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitQuantity_5]'
            }]
        }, {
            name: 'OtherBenefitNetworkFlag_5',
            propertyName: 'otherBenefitNetworkFlag_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitNetworkFlag_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitNetworkFlag_5]'
            }]
        }, {
            name: 'OtherBenefitBegDate_6',
            propertyName: 'otherBenefitBegDate_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitBegDate_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitBegDate_6]'
            }]
        }, {
            name: 'OtherBenefitEndDate_6',
            propertyName: 'otherBenefitEndDate_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitEndDate_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitEndDate_6]'
            }]
        }, {
            name: 'OtherBenefitAdd_Date_6',
            propertyName: 'otherBenefitAdd_Date_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAdd_Date_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAdd_Date_6]'
            }]
        }, {
            name: 'OtherBenefitInfo_6',
            propertyName: 'otherBenefitInfo_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInfo_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInfo_6]'
            }]
        }, {
            name: 'OtherBenefitCoverage_6',
            propertyName: 'otherBenefitCoverage_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitCoverage_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitCoverage_6]'
            }]
        }, {
            name: 'OtherBenefitServiceType_6',
            propertyName: 'otherBenefitServiceType_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitServiceType_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitServiceType_6]'
            }]
        }, {
            name: 'OtherBenefitInsuranceType_6',
            propertyName: 'otherBenefitInsuranceType_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInsuranceType_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInsuranceType_6]'
            }]
        }, {
            name: 'OtherBenefitPlan_Desc_6',
            propertyName: 'otherBenefitPlan_Desc_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitPlan_Desc_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitPlan_Desc_6]'
            }]
        }, {
            name: 'OtherBenefitMessage1_6',
            propertyName: 'otherBenefitMessage1_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitMessage1_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitMessage1_6]'
            }]
        }, {
            name: 'OtherBenefitTimePeriodQualifier_6',
            propertyName: 'otherBenefitTimePeriodQualifier_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitTimePeriodQualifier_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitTimePeriodQualifier_6]'
            }]
        }, {
            name: 'OtherBenefitAmount_6',
            propertyName: 'otherBenefitAmount_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAmount_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAmount_6]'
            }]
        }, {
            name: 'OtherBenefitQuantity_6',
            propertyName: 'otherBenefitQuantity_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitQuantity_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitQuantity_6]'
            }]
        }, {
            name: 'OtherBenefitNetworkFlag_6',
            propertyName: 'otherBenefitNetworkFlag_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitNetworkFlag_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitNetworkFlag_6]'
            }]
        }, {
            name: 'OtherBenefitBegDate_7',
            propertyName: 'otherBenefitBegDate_7',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitBegDate_7 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitBegDate_7]'
            }]
        }, {
            name: 'OtherBenefitEndDate_7',
            propertyName: 'otherBenefitEndDate_7',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitEndDate_7 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitEndDate_7]'
            }]
        }, {
            name: 'OtherBenefitAdd_Date_7',
            propertyName: 'otherBenefitAdd_Date_7',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAdd_Date_7 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAdd_Date_7]'
            }]
        }, {
            name: 'OtherBenefitInfo_7',
            propertyName: 'otherBenefitInfo_7',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInfo_7 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInfo_7]'
            }]
        }, {
            name: 'OtherBenefitCoverage_7',
            propertyName: 'otherBenefitCoverage_7',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitCoverage_7 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitCoverage_7]'
            }]
        }, {
            name: 'OtherBenefitServiceType_7',
            propertyName: 'otherBenefitServiceType_7',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitServiceType_7 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitServiceType_7]'
            }]
        }, {
            name: 'OtherBenefitInsuranceType_7',
            propertyName: 'otherBenefitInsuranceType_7',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInsuranceType_7 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInsuranceType_7]'
            }]
        }, {
            name: 'OtherBenefitPlan_Desc_7',
            propertyName: 'otherBenefitPlan_Desc_7',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitPlan_Desc_7 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitPlan_Desc_7]'
            }]
        }, {
            name: 'OtherBenefitMessage1_7',
            propertyName: 'otherBenefitMessage1_7',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitMessage1_7 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitMessage1_7]'
            }]
        }, {
            name: 'OtherBenefitTimePeriodQualifier_7',
            propertyName: 'otherBenefitTimePeriodQualifier_7',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitTimePeriodQualifier_7 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitTimePeriodQualifier_7]'
            }]
        }, {
            name: 'OtherBenefitAmount_7',
            propertyName: 'otherBenefitAmount_7',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAmount_7 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAmount_7]'
            }]
        }, {
            name: 'OtherBenefitQuantity_7',
            propertyName: 'otherBenefitQuantity_7',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitQuantity_7 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitQuantity_7]'
            }]
        }, {
            name: 'OtherBenefitNetworkFlag_7',
            propertyName: 'otherBenefitNetworkFlag_7',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitNetworkFlag_7 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitNetworkFlag_7]'
            }]
        }, {
            name: 'OtherBenefitBegDate_8',
            propertyName: 'otherBenefitBegDate_8',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitBegDate_8 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitBegDate_8]'
            }]
        }, {
            name: 'OtherBenefitEndDate_8',
            propertyName: 'otherBenefitEndDate_8',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitEndDate_8 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitEndDate_8]'
            }]
        }, {
            name: 'OtherBenefitAdd_Date_8',
            propertyName: 'otherBenefitAdd_Date_8',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAdd_Date_8 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAdd_Date_8]'
            }]
        }, {
            name: 'OtherBenefitInfo_8',
            propertyName: 'otherBenefitInfo_8',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInfo_8 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInfo_8]'
            }]
        }, {
            name: 'OtherBenefitCoverage_8',
            propertyName: 'otherBenefitCoverage_8',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitCoverage_8 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitCoverage_8]'
            }]
        }, {
            name: 'OtherBenefitServiceType_8',
            propertyName: 'otherBenefitServiceType_8',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitServiceType_8 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitServiceType_8]'
            }]
        }, {
            name: 'OtherBenefitInsuranceType_8',
            propertyName: 'otherBenefitInsuranceType_8',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInsuranceType_8 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInsuranceType_8]'
            }]
        }, {
            name: 'OtherBenefitPlan_Desc_8',
            propertyName: 'otherBenefitPlan_Desc_8',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitPlan_Desc_8 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitPlan_Desc_8]'
            }]
        }, {
            name: 'OtherBenefitMessage1_8',
            propertyName: 'otherBenefitMessage1_8',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitMessage1_8 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitMessage1_8]'
            }]
        }, {
            name: 'OtherBenefitTimePeriodQualifier_8',
            propertyName: 'otherBenefitTimePeriodQualifier_8',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitTimePeriodQualifier_8 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitTimePeriodQualifier_8]'
            }]
        }, {
            name: 'OtherBenefitAmount_8',
            propertyName: 'otherBenefitAmount_8',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAmount_8 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAmount_8]'
            }]
        }, {
            name: 'OtherBenefitQuantity_8',
            propertyName: 'otherBenefitQuantity_8',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitQuantity_8 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitQuantity_8]'
            }]
        }, {
            name: 'OtherBenefitNetworkFlag_8',
            propertyName: 'otherBenefitNetworkFlag_8',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitNetworkFlag_8 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitNetworkFlag_8]'
            }]
        }, {
            name: 'TransactionCnts',
            propertyName: 'transactionCnts',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for TransactionCnts needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for TransactionCnts]'
            }]
        }, {
            name: 'ValidationNotes',
            propertyName: 'validationNotes',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for ValidationNotes needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for ValidationNotes]'
            }]
        }, {
            name: 'ValidationScore',
            propertyName: 'validationScore',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for ValidationScore needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for ValidationScore]'
            }]
        }]
    };

    self.diff = function(value, index, cb) {
        context.GDS.findOne({
            student: value.custDefined_In
        })
            .populate('rangedData')
            .lean()
            .exec(function(err, gds) {
                if (!gds) {
                    cb(err, null);
                    return;
                }

var startMoment = moment.tz(value.begDate_1, self.clientTimezone)
                            .startOf('day'),
                            endMoment = moment.tz(value.endDate_1, self.clientTimezone)
                            .endOf('day');

                dataUtil.getByDateRange(
                    gds,
                    'rangedData',
                    startMoment,
                    endMoment,
                    function(err, flat) {
                        cb(err, {
                            pageNum: flat.pageNum,
                            dataSource: flat.dataSource,
                            accountNbr_In: flat.accountNbr_In,
                            recipID1_In: flat.recipID1_In,
                            sSN_Inp: flat.sSN_Inp,
                            lName1_In: flat.lName1_In,
                            fName1_In: flat.fName1_In,
                            mI1_In: flat.mI1_In,
                            dOB_In: flat.dOB_In,
                            gender_In: flat.gender_In,
                            custDefined_In: flat.custDefined_In,
                            beg_In: flat.beg_In,
                            end_In: flat.end_In,
                            los_In: flat.los_In,
                            errorMsg: flat.errorMsg,
                            statusFlag: flat.statusFlag,
                            mcaidFlag: flat.mcaidFlag,
                            mCRA_Flag: flat.mCRA_Flag,
                            mCRB_Flag: flat.mCRB_Flag,
                            otherPayor_Flag: flat.otherPayor_Flag,
                            gDS_Ref_No: flat.gDS_Ref_No,
                            searchCode: flat.searchCode,
                            split_Serial_Num: flat.split_Serial_Num,
                            planBeneNum: flat.planBeneNum,
                            sSN: flat.sSN,
                            lname: flat.lname,
                            fname: flat.fname,
                            mname: flat.mname,
                            dOB: flat.dOB,
                            gender: flat.gender,
                            hICNum: flat.hICNum,
                            otherID: flat.otherID,
                            address1: flat.address1,
                            address2: flat.address2,
                            city: flat.city,
                            state: flat.state,
                            zip: flat.zip,
                            mcaidDays_1: flat.mcaidDays_1,
                            begDate_1: flat.begDate_1,
                            endDate_1: flat.endDate_1,
                            add_Date_1: flat.add_Date_1,
                            status_1: flat.status_1,
                            coverage_1: flat.coverage_1,
                            serviceType_1: flat.serviceType_1,
                            insuranceType_1: flat.insuranceType_1,
                            plan_Desc_1: flat.plan_Desc_1,
                            message1_1: flat.message1_1,
                            message2_1: flat.message2_1,
                            mcaidDays_2: flat.mcaidDays_2,
                            begDate_2: flat.begDate_2,
                            endDate_2: flat.endDate_2,
                            add_Date_2: flat.add_Date_2,
                            status_2: flat.status_2,
                            coverage_2: flat.coverage_2,
                            serviceType_2: flat.serviceType_2,
                            insuranceType_2: flat.insuranceType_2,
                            plan_Desc_2: flat.plan_Desc_2,
                            message1_2: flat.message1_2,
                            message2_2: flat.message2_2,
                            mcaidDays_3: flat.mcaidDays_3,
                            begDate_3: flat.begDate_3,
                            endDate_3: flat.endDate_3,
                            add_Date_3: flat.add_Date_3,
                            status_3: flat.status_3,
                            coverage_3: flat.coverage_3,
                            serviceType_3: flat.serviceType_3,
                            insuranceType_3: flat.insuranceType_3,
                            plan_Desc_3: flat.plan_Desc_3,
                            message1_3: flat.message1_3,
                            message2_3: flat.message2_3,
                            partADays_1: flat.partADays_1,
                            partA_Beg_1: flat.partA_Beg_1,
                            partA_End_1: flat.partA_End_1,
                            partA_AddDate_1: flat.partA_AddDate_1,
                            partA_Status_1: flat.partA_Status_1,
                            partA_CovLevel_1: flat.partA_CovLevel_1,
                            partA_Desc_1: flat.partA_Desc_1,
                            partBDays_1: flat.partBDays_1,
                            partB_Beg_1: flat.partB_Beg_1,
                            partB_End_1: flat.partB_End_1,
                            partBAdd_1: flat.partBAdd_1,
                            partB_Status_1: flat.partB_Status_1,
                            partB_CovLevel_1: flat.partB_CovLevel_1,
                            partB_Desc_1: flat.partB_Desc_1,
                            mcaidDays: flat.mcaidDays,
                            partADays: flat.partADays,
                            mcaid_PartADays: flat.mcaid_PartADays,
                            partBDays: flat.partBDays,
                            mcaid_PartBDays: flat.mcaid_PartBDays,
                            partA_PartBDays: flat.partA_PartBDays,
                            mcaid_PartA_PartBDays: flat.mcaid_PartA_PartBDays,
                            otherPayerBegDate_1: flat.otherPayerBegDate_1,
                            otherPayerEndDate_1: flat.otherPayerEndDate_1,
                            otherPayerAdd_Date_1: flat.otherPayerAdd_Date_1,
                            otherPayerInfo_1: flat.otherPayerInfo_1,
                            otherPayerCoverage_1: flat.otherPayerCoverage_1,
                            otherPayerServiceType_1: flat.otherPayerServiceType_1,
                            otherPayerInsuranceType_1: flat.otherPayerInsuranceType_1,
                            otherPayerPlan_Desc_1: flat.otherPayerPlan_Desc_1,
                            otherPayerMessage1_1: flat.otherPayerMessage1_1,
                            otherPayerMessage2_1: flat.otherPayerMessage2_1,
                            otherPayerIdentifierCode_1: flat.otherPayerIdentifierCode_1,
                            otherPayer_Name_1: flat.otherPayer_Name_1,
                            otherPayer_RefID_1: flat.otherPayer_RefID_1,
                            otherPayer_Tel_1: flat.otherPayer_Tel_1,
                            otherPayer_address1_1: flat.otherPayer_address1_1,
                            otherPayer_address2_1: flat.otherPayer_address2_1,
                            otherPayer_City_1: flat.otherPayer_City_1,
                            otherPayer_State_1: flat.otherPayer_State_1,
                            otherPayer_Zipcode_1: flat.otherPayer_Zipcode_1,
                            otherPayerBegDate_2: flat.otherPayerBegDate_2,
                            otherPayerEndDate_2: flat.otherPayerEndDate_2,
                            otherPayerAdd_Date_2: flat.otherPayerAdd_Date_2,
                            otherPayerInfo_2: flat.otherPayerInfo_2,
                            otherPayerCoverage_2: flat.otherPayerCoverage_2,
                            otherPayerServiceType_2: flat.otherPayerServiceType_2,
                            otherPayerInsuranceType_2: flat.otherPayerInsuranceType_2,
                            otherPayerPlan_Desc_2: flat.otherPayerPlan_Desc_2,
                            otherPayerMessage1_2: flat.otherPayerMessage1_2,
                            otherPayerMessage2_2: flat.otherPayerMessage2_2,
                            otherPayerIdentifierCode_2: flat.otherPayerIdentifierCode_2,
                            otherPayer_Name_2: flat.otherPayer_Name_2,
                            otherPayer_RefID_2: flat.otherPayer_RefID_2,
                            otherPayer_Tel_2: flat.otherPayer_Tel_2,
                            otherPayer_address1_2: flat.otherPayer_address1_2,
                            otherPayer_address2_2: flat.otherPayer_address2_2,
                            otherPayer_City_2: flat.otherPayer_City_2,
                            otherPayer_State_2: flat.otherPayer_State_2,
                            otherPayer_Zipcode_2: flat.otherPayer_Zipcode_2,
                            otherPayerBegDate_3: flat.otherPayerBegDate_3,
                            otherPayerEndDate_3: flat.otherPayerEndDate_3,
                            otherPayerAdd_Date_3: flat.otherPayerAdd_Date_3,
                            otherPayerInfo_3: flat.otherPayerInfo_3,
                            otherPayerCoverage_3: flat.otherPayerCoverage_3,
                            otherPayerServiceType_3: flat.otherPayerServiceType_3,
                            otherPayerInsuranceType_3: flat.otherPayerInsuranceType_3,
                            otherPayerPlan_Desc_3: flat.otherPayerPlan_Desc_3,
                            otherPayerMessage1_3: flat.otherPayerMessage1_3,
                            otherPayerMessage2_3: flat.otherPayerMessage2_3,
                            otherPayerIdentifierCode_3: flat.otherPayerIdentifierCode_3,
                            otherPayer_Name_3: flat.otherPayer_Name_3,
                            otherPayer_RefID_3: flat.otherPayer_RefID_3,
                            otherPayer_Tel_3: flat.otherPayer_Tel_3,
                            otherPayer_address1_3: flat.otherPayer_address1_3,
                            otherPayer_address2_3: flat.otherPayer_address2_3,
                            otherPayer_City_3: flat.otherPayer_City_3,
                            otherPayer_State_3: flat.otherPayer_State_3,
                            otherPayer_Zipcode_3: flat.otherPayer_Zipcode_3,
                            otherBenefitBegDate_1: flat.otherBenefitBegDate_1,
                            otherBenefitEndDate_1: flat.otherBenefitEndDate_1,
                            otherBenefitAdd_Date_1: flat.otherBenefitAdd_Date_1,
                            otherBenefitInfo_1: flat.otherBenefitInfo_1,
                            otherBenefitCoverage_1: flat.otherBenefitCoverage_1,
                            otherBenefitServiceType_1: flat.otherBenefitServiceType_1,
                            otherBenefitInsuranceType_1: flat.otherBenefitInsuranceType_1,
                            otherBenefitPlan_Desc_1: flat.otherBenefitPlan_Desc_1,
                            otherBenefitMessage1_1: flat.otherBenefitMessage1_1,
                            otherBenefitTimePeriodQualifier_1: flat.otherBenefitTimePeriodQualifier_1,
                            otherBenefitAmount_1: flat.otherBenefitAmount_1,
                            otherBenefitQuantity_1: flat.otherBenefitQuantity_1,
                            otherBenefitNetworkFlag_1: flat.otherBenefitNetworkFlag_1,
                            otherBenefitBegDate_2: flat.otherBenefitBegDate_2,
                            otherBenefitEndDate_2: flat.otherBenefitEndDate_2,
                            otherBenefitAdd_Date_2: flat.otherBenefitAdd_Date_2,
                            otherBenefitInfo_2: flat.otherBenefitInfo_2,
                            otherBenefitCoverage_2: flat.otherBenefitCoverage_2,
                            otherBenefitServiceType_2: flat.otherBenefitServiceType_2,
                            otherBenefitInsuranceType_2: flat.otherBenefitInsuranceType_2,
                            otherBenefitPlan_Desc_2: flat.otherBenefitPlan_Desc_2,
                            otherBenefitMessage1_2: flat.otherBenefitMessage1_2,
                            otherBenefitTimePeriodQualifier_2: flat.otherBenefitTimePeriodQualifier_2,
                            otherBenefitAmount_2: flat.otherBenefitAmount_2,
                            otherBenefitQuantity_2: flat.otherBenefitQuantity_2,
                            otherBenefitNetworkFlag_2: flat.otherBenefitNetworkFlag_2,
                            otherBenefitBegDate_3: flat.otherBenefitBegDate_3,
                            otherBenefitEndDate_3: flat.otherBenefitEndDate_3,
                            otherBenefitAdd_Date_3: flat.otherBenefitAdd_Date_3,
                            otherBenefitInfo_3: flat.otherBenefitInfo_3,
                            otherBenefitCoverage_3: flat.otherBenefitCoverage_3,
                            otherBenefitServiceType_3: flat.otherBenefitServiceType_3,
                            otherBenefitInsuranceType_3: flat.otherBenefitInsuranceType_3,
                            otherBenefitPlan_Desc_3: flat.otherBenefitPlan_Desc_3,
                            otherBenefitMessage1_3: flat.otherBenefitMessage1_3,
                            otherBenefitTimePeriodQualifier_3: flat.otherBenefitTimePeriodQualifier_3,
                            otherBenefitAmount_3: flat.otherBenefitAmount_3,
                            otherBenefitQuantity_3: flat.otherBenefitQuantity_3,
                            otherBenefitNetworkFlag_3: flat.otherBenefitNetworkFlag_3,
                            otherBenefitBegDate_4: flat.otherBenefitBegDate_4,
                            otherBenefitEndDate_4: flat.otherBenefitEndDate_4,
                            otherBenefitAdd_Date_4: flat.otherBenefitAdd_Date_4,
                            otherBenefitInfo_4: flat.otherBenefitInfo_4,
                            otherBenefitCoverage_4: flat.otherBenefitCoverage_4,
                            otherBenefitServiceType_4: flat.otherBenefitServiceType_4,
                            otherBenefitInsuranceType_4: flat.otherBenefitInsuranceType_4,
                            otherBenefitPlan_Desc_4: flat.otherBenefitPlan_Desc_4,
                            otherBenefitMessage1_4: flat.otherBenefitMessage1_4,
                            otherBenefitTimePeriodQualifier_4: flat.otherBenefitTimePeriodQualifier_4,
                            otherBenefitAmount_4: flat.otherBenefitAmount_4,
                            otherBenefitQuantity_4: flat.otherBenefitQuantity_4,
                            otherBenefitNetworkFlag_4: flat.otherBenefitNetworkFlag_4,
                            otherBenefitBegDate_5: flat.otherBenefitBegDate_5,
                            otherBenefitEndDate_5: flat.otherBenefitEndDate_5,
                            otherBenefitAdd_Date_5: flat.otherBenefitAdd_Date_5,
                            otherBenefitInfo_5: flat.otherBenefitInfo_5,
                            otherBenefitCoverage_5: flat.otherBenefitCoverage_5,
                            otherBenefitServiceType_5: flat.otherBenefitServiceType_5,
                            otherBenefitInsuranceType_5: flat.otherBenefitInsuranceType_5,
                            otherBenefitPlan_Desc_5: flat.otherBenefitPlan_Desc_5,
                            otherBenefitMessage1_5: flat.otherBenefitMessage1_5,
                            otherBenefitTimePeriodQualifier_5: flat.otherBenefitTimePeriodQualifier_5,
                            otherBenefitAmount_5: flat.otherBenefitAmount_5,
                            otherBenefitQuantity_5: flat.otherBenefitQuantity_5,
                            otherBenefitNetworkFlag_5: flat.otherBenefitNetworkFlag_5,
                            otherBenefitBegDate_6: flat.otherBenefitBegDate_6,
                            otherBenefitEndDate_6: flat.otherBenefitEndDate_6,
                            otherBenefitAdd_Date_6: flat.otherBenefitAdd_Date_6,
                            otherBenefitInfo_6: flat.otherBenefitInfo_6,
                            otherBenefitCoverage_6: flat.otherBenefitCoverage_6,
                            otherBenefitServiceType_6: flat.otherBenefitServiceType_6,
                            otherBenefitInsuranceType_6: flat.otherBenefitInsuranceType_6,
                            otherBenefitPlan_Desc_6: flat.otherBenefitPlan_Desc_6,
                            otherBenefitMessage1_6: flat.otherBenefitMessage1_6,
                            otherBenefitTimePeriodQualifier_6: flat.otherBenefitTimePeriodQualifier_6,
                            otherBenefitAmount_6: flat.otherBenefitAmount_6,
                            otherBenefitQuantity_6: flat.otherBenefitQuantity_6,
                            otherBenefitNetworkFlag_6: flat.otherBenefitNetworkFlag_6,
                            otherBenefitBegDate_7: flat.otherBenefitBegDate_7,
                            otherBenefitEndDate_7: flat.otherBenefitEndDate_7,
                            otherBenefitAdd_Date_7: flat.otherBenefitAdd_Date_7,
                            otherBenefitInfo_7: flat.otherBenefitInfo_7,
                            otherBenefitCoverage_7: flat.otherBenefitCoverage_7,
                            otherBenefitServiceType_7: flat.otherBenefitServiceType_7,
                            otherBenefitInsuranceType_7: flat.otherBenefitInsuranceType_7,
                            otherBenefitPlan_Desc_7: flat.otherBenefitPlan_Desc_7,
                            otherBenefitMessage1_7: flat.otherBenefitMessage1_7,
                            otherBenefitTimePeriodQualifier_7: flat.otherBenefitTimePeriodQualifier_7,
                            otherBenefitAmount_7: flat.otherBenefitAmount_7,
                            otherBenefitQuantity_7: flat.otherBenefitQuantity_7,
                            otherBenefitNetworkFlag_7: flat.otherBenefitNetworkFlag_7,
                            otherBenefitBegDate_8: flat.otherBenefitBegDate_8,
                            otherBenefitEndDate_8: flat.otherBenefitEndDate_8,
                            otherBenefitAdd_Date_8: flat.otherBenefitAdd_Date_8,
                            otherBenefitInfo_8: flat.otherBenefitInfo_8,
                            otherBenefitCoverage_8: flat.otherBenefitCoverage_8,
                            otherBenefitServiceType_8: flat.otherBenefitServiceType_8,
                            otherBenefitInsuranceType_8: flat.otherBenefitInsuranceType_8,
                            otherBenefitPlan_Desc_8: flat.otherBenefitPlan_Desc_8,
                            otherBenefitMessage1_8: flat.otherBenefitMessage1_8,
                            otherBenefitTimePeriodQualifier_8: flat.otherBenefitTimePeriodQualifier_8,
                            otherBenefitAmount_8: flat.otherBenefitAmount_8,
                            otherBenefitQuantity_8: flat.otherBenefitQuantity_8,
                            otherBenefitNetworkFlag_8: flat.otherBenefitNetworkFlag_8,
                            transactionCnts: flat.transactionCnts,
                            validationNotes: flat.validationNotes,
                            validationScore: flat.validationScore
                        });
                    });
            });
    };

    self.commit = function(value, rowIndex, cb) {
        var tasks = [

            function(cb) {
                async.parallel({
                    byId: function(cb) {
                        context.Student.findOne({
                            _id: value.custDefined_In
                        })
                            .exec(function(err, student) {
                                cb(null, student);
                            })
                    },
                    byStudentId: function(cb) {
                        context.Student.findOne({
                            studentId: value.custDefined_In
                        })
                            .exec(function(err, student) {
                                cb(err, student);
                            });
                    }
                }, function(err, r) {
                    var student = (r.byId || r.byStudentId);
                    cb(err, student);
                })
            },
            function(student, cb) {
                //populate student parts
                async.waterfall([

                    function(cb) {
                        context.Student.populate(student, {
                            path: 'rangedData',
                            model: 'StudentRange'
                        }, cb);
                    },
                    function(student, cb) {
                        context.Student.populate(student, {
                            path: 'rangedData.school',
                            model: 'School'
                        }, cb);
                    },
                    function(student, cb) {
                        context.Student.populate(student, {
                            path: 'rangedData.school.district',
                            model: 'District'
                        }, cb);
                    }
                ], function(err, student) {
                    cb(err, student);
                })
            },
            function(student, cb) {
                student = student.toObject();
                var student = dataUtil.getByDateRangeSync(student, 'rangedData', value.beg_In, value.end_In);
                cb(null, student, student.school.district);
            },
            function(student, district, cb) {
                context.MCO.findOne({
                    district: district._id,
                    name: new RegExp(value.dataSource, 'i')
                })
                    .exec(function(err, mco) {
                        cb(err, student, mco);
                    });

            },
            function(student, mco, cb) {
                //get current gds data
                context.GDS.findOne({
                    student: student._id
                })
                    .populate('rangedData')
                    .exec(function(err, gds) {
                        cb(err, student, mco, gds);
                    })

            }
        ];

        async.waterfall(tasks, function(err, student, mco, gds) {
            if (!!err) {
                logger.error('gds import error: %s', err);
                self.emit('commit error', {});
            }
            if (!gds) {
                logger.debug('gds for ' + value.custDefined_In + ' is new. Creating');
                gds = new context.GDS();
                gds.rangedData = [];
            }

var startMoment = moment.tz(value.begDate_1, self.clientTimezone)
                            .startOf('day'),
                            endMoment = moment.tz(value.endDate_1, self.clientTimezone)
                            .endOf('day');

            gds.student = student._id;

            gds.custDefined_In = value.custDefined_In;
            var gdsRange = new context.ranged.GDS();
            gdsRange.startDate = startMoment.toDate();
            gdsRange.endDate = endMoment.toDate();

            if (/1 active coverage/i.test(value.status_1)) {
                gdsRange.rangeActive = true;
                logger.silly(value.custDefined_In + ' active coverage.');
            } else if (/6 inactive/i.test(value.status_1)) {
                gdsRange.rangeActive = false;
                logger.silly(value.custDefined_In + ' inactive coverage.');
            } else {
                gdsRange.rangeActive = false;
                logger.silly(value.custDefined_In + ' inactive coverage. (blank)');
            }

            //{{{ gds range data
            gdsRange.mco = mco.id;
            gdsRange.pageNum = value.pageNum;
            gdsRange.dataSource = value.dataSource;
            gdsRange.accountNbr_In = value.accountNbr_In;
            gdsRange.recipID1_In = value.recipID1_In;
            gdsRange.sSN_Inp = value.sSN_Inp;
            gdsRange.lName1_In = value.lName1_In;
            gdsRange.fName1_In = value.fName1_In;
            gdsRange.mI1_In = value.mI1_In;
            gdsRange.dOB_In = value.dOB_In;
            gdsRange.gender_In = value.gender_In;
            gdsRange.los_In = value.los_In;
            gdsRange.errorMsg = value.errorMsg;
            gdsRange.statusFlag = value.statusFlag;
            gdsRange.mcaidFlag = value.mcaidFlag;
            gdsRange.mCRA_Flag = value.mCRA_Flag;
            gdsRange.mCRB_Flag = value.mCRB_Flag;
            gdsRange.otherPayor_Flag = value.otherPayor_Flag;
            gdsRange.gDS_Ref_No = value.gDS_Ref_No;
            gdsRange.searchCode = value.searchCode;
            gdsRange.split_Serial_Num = value.split_Serial_Num;
            gdsRange.planBeneNum = value.planBeneNum;
            gdsRange.sSN = value.sSN;
            gdsRange.lname = value.lname;
            gdsRange.fname = value.fname;
            gdsRange.mname = value.mname;
            gdsRange.dOB = value.dOB;
            gdsRange.gender = value.gender;
            gdsRange.hICNum = value.hICNum;
            gdsRange.otherID = value.otherID;
            gdsRange.address1 = value.address1;
            gdsRange.address2 = value.address2;
            gdsRange.city = value.city;
            gdsRange.state = value.state;
            gdsRange.zip = value.zip;
            gdsRange.mcaidDays_1 = value.mcaidDays_1;
            gdsRange.begDate_1 = value.begDate_1;
            gdsRange.endDate_1 = value.endDate_1;
            gdsRange.add_Date_1 = value.add_Date_1;
            gdsRange.status_1 = value.status_1;
            gdsRange.coverage_1 = value.coverage_1;
            gdsRange.serviceType_1 = value.serviceType_1;
            gdsRange.insuranceType_1 = value.insuranceType_1;
            gdsRange.plan_Desc_1 = value.plan_Desc_1;
            gdsRange.message1_1 = value.message1_1;
            gdsRange.message2_1 = value.message2_1;
            gdsRange.mcaidDays_2 = value.mcaidDays_2;
            gdsRange.begDate_2 = value.begDate_2;
            gdsRange.endDate_2 = value.endDate_2;
            gdsRange.add_Date_2 = value.add_Date_2;
            gdsRange.status_2 = value.status_2;
            gdsRange.coverage_2 = value.coverage_2;
            gdsRange.serviceType_2 = value.serviceType_2;
            gdsRange.insuranceType_2 = value.insuranceType_2;
            gdsRange.plan_Desc_2 = value.plan_Desc_2;
            gdsRange.message1_2 = value.message1_2;
            gdsRange.message2_2 = value.message2_2;
            gdsRange.mcaidDays_3 = value.mcaidDays_3;
            gdsRange.begDate_3 = value.begDate_3;
            gdsRange.endDate_3 = value.endDate_3;
            gdsRange.add_Date_3 = value.add_Date_3;
            gdsRange.status_3 = value.status_3;
            gdsRange.coverage_3 = value.coverage_3;
            gdsRange.serviceType_3 = value.serviceType_3;
            gdsRange.insuranceType_3 = value.insuranceType_3;
            gdsRange.plan_Desc_3 = value.plan_Desc_3;
            gdsRange.message1_3 = value.message1_3;
            gdsRange.message2_3 = value.message2_3;
            gdsRange.partADays_1 = value.partADays_1;
            gdsRange.partA_Beg_1 = value.partA_Beg_1;
            gdsRange.partA_End_1 = value.partA_End_1;
            gdsRange.partA_AddDate_1 = value.partA_AddDate_1;
            gdsRange.partA_Status_1 = value.partA_Status_1;
            gdsRange.partA_CovLevel_1 = value.partA_CovLevel_1;
            gdsRange.partA_Desc_1 = value.partA_Desc_1;
            gdsRange.partBDays_1 = value.partBDays_1;
            gdsRange.partB_Beg_1 = value.partB_Beg_1;
            gdsRange.partB_End_1 = value.partB_End_1;
            gdsRange.partBAdd_1 = value.partBAdd_1;
            gdsRange.partB_Status_1 = value.partB_Status_1;
            gdsRange.partB_CovLevel_1 = value.partB_CovLevel_1;
            gdsRange.partB_Desc_1 = value.partB_Desc_1;
            gdsRange.mcaidDays = value.mcaidDays;
            gdsRange.partADays = value.partADays;
            gdsRange.mcaid_PartADays = value.mcaid_PartADays;
            gdsRange.partBDays = value.partBDays;
            gdsRange.mcaid_PartBDays = value.mcaid_PartBDays;
            gdsRange.partA_PartBDays = value.partA_PartBDays;
            gdsRange.mcaid_PartA_PartBDays = value.mcaid_PartA_PartBDays;
            gdsRange.otherPayerBegDate_1 = value.otherPayerBegDate_1;
            gdsRange.otherPayerEndDate_1 = value.otherPayerEndDate_1;
            gdsRange.otherPayerAdd_Date_1 = value.otherPayerAdd_Date_1;
            gdsRange.otherPayerInfo_1 = value.otherPayerInfo_1;
            gdsRange.otherPayerCoverage_1 = value.otherPayerCoverage_1;
            gdsRange.otherPayerServiceType_1 = value.otherPayerServiceType_1;
            gdsRange.otherPayerInsuranceType_1 = value.otherPayerInsuranceType_1;
            gdsRange.otherPayerPlan_Desc_1 = value.otherPayerPlan_Desc_1;
            gdsRange.otherPayerMessage1_1 = value.otherPayerMessage1_1;
            gdsRange.otherPayerMessage2_1 = value.otherPayerMessage2_1;
            gdsRange.otherPayerIdentifierCode_1 = value.otherPayerIdentifierCode_1;
            gdsRange.otherPayer_Name_1 = value.otherPayer_Name_1;
            gdsRange.otherPayer_RefID_1 = value.otherPayer_RefID_1;
            gdsRange.otherPayer_Tel_1 = value.otherPayer_Tel_1;
            gdsRange.otherPayer_address1_1 = value.otherPayer_address1_1;
            gdsRange.otherPayer_address2_1 = value.otherPayer_address2_1;
            gdsRange.otherPayer_City_1 = value.otherPayer_City_1;
            gdsRange.otherPayer_State_1 = value.otherPayer_State_1;
            gdsRange.otherPayer_Zipcode_1 = value.otherPayer_Zipcode_1;
            gdsRange.otherPayerBegDate_2 = value.otherPayerBegDate_2;
            gdsRange.otherPayerEndDate_2 = value.otherPayerEndDate_2;
            gdsRange.otherPayerAdd_Date_2 = value.otherPayerAdd_Date_2;
            gdsRange.otherPayerInfo_2 = value.otherPayerInfo_2;
            gdsRange.otherPayerCoverage_2 = value.otherPayerCoverage_2;
            gdsRange.otherPayerServiceType_2 = value.otherPayerServiceType_2;
            gdsRange.otherPayerInsuranceType_2 = value.otherPayerInsuranceType_2;
            gdsRange.otherPayerPlan_Desc_2 = value.otherPayerPlan_Desc_2;
            gdsRange.otherPayerMessage1_2 = value.otherPayerMessage1_2;
            gdsRange.otherPayerMessage2_2 = value.otherPayerMessage2_2;
            gdsRange.otherPayerIdentifierCode_2 = value.otherPayerIdentifierCode_2;
            gdsRange.otherPayer_Name_2 = value.otherPayer_Name_2;
            gdsRange.otherPayer_RefID_2 = value.otherPayer_RefID_2;
            gdsRange.otherPayer_Tel_2 = value.otherPayer_Tel_2;
            gdsRange.otherPayer_address1_2 = value.otherPayer_address1_2;
            gdsRange.otherPayer_address2_2 = value.otherPayer_address2_2;
            gdsRange.otherPayer_City_2 = value.otherPayer_City_2;
            gdsRange.otherPayer_State_2 = value.otherPayer_State_2;
            gdsRange.otherPayer_Zipcode_2 = value.otherPayer_Zipcode_2;
            gdsRange.otherPayerBegDate_3 = value.otherPayerBegDate_3;
            gdsRange.otherPayerEndDate_3 = value.otherPayerEndDate_3;
            gdsRange.otherPayerAdd_Date_3 = value.otherPayerAdd_Date_3;
            gdsRange.otherPayerInfo_3 = value.otherPayerInfo_3;
            gdsRange.otherPayerCoverage_3 = value.otherPayerCoverage_3;
            gdsRange.otherPayerServiceType_3 = value.otherPayerServiceType_3;
            gdsRange.otherPayerInsuranceType_3 = value.otherPayerInsuranceType_3;
            gdsRange.otherPayerPlan_Desc_3 = value.otherPayerPlan_Desc_3;
            gdsRange.otherPayerMessage1_3 = value.otherPayerMessage1_3;
            gdsRange.otherPayerMessage2_3 = value.otherPayerMessage2_3;
            gdsRange.otherPayerIdentifierCode_3 = value.otherPayerIdentifierCode_3;
            gdsRange.otherPayer_Name_3 = value.otherPayer_Name_3;
            gdsRange.otherPayer_RefID_3 = value.otherPayer_RefID_3;
            gdsRange.otherPayer_Tel_3 = value.otherPayer_Tel_3;
            gdsRange.otherPayer_address1_3 = value.otherPayer_address1_3;
            gdsRange.otherPayer_address2_3 = value.otherPayer_address2_3;
            gdsRange.otherPayer_City_3 = value.otherPayer_City_3;
            gdsRange.otherPayer_State_3 = value.otherPayer_State_3;
            gdsRange.otherPayer_Zipcode_3 = value.otherPayer_Zipcode_3;
            gdsRange.otherBenefitBegDate_1 = value.otherBenefitBegDate_1;
            gdsRange.otherBenefitEndDate_1 = value.otherBenefitEndDate_1;
            gdsRange.otherBenefitAdd_Date_1 = value.otherBenefitAdd_Date_1;
            gdsRange.otherBenefitInfo_1 = value.otherBenefitInfo_1;
            gdsRange.otherBenefitCoverage_1 = value.otherBenefitCoverage_1;
            gdsRange.otherBenefitServiceType_1 = value.otherBenefitServiceType_1;
            gdsRange.otherBenefitInsuranceType_1 = value.otherBenefitInsuranceType_1;
            gdsRange.otherBenefitPlan_Desc_1 = value.otherBenefitPlan_Desc_1;
            gdsRange.otherBenefitMessage1_1 = value.otherBenefitMessage1_1;
            gdsRange.otherBenefitTimePeriodQualifier_1 = value.otherBenefitTimePeriodQualifier_1;
            gdsRange.otherBenefitAmount_1 = value.otherBenefitAmount_1;
            gdsRange.otherBenefitQuantity_1 = value.otherBenefitQuantity_1;
            gdsRange.otherBenefitNetworkFlag_1 = value.otherBenefitNetworkFlag_1;
            gdsRange.otherBenefitBegDate_2 = value.otherBenefitBegDate_2;
            gdsRange.otherBenefitEndDate_2 = value.otherBenefitEndDate_2;
            gdsRange.otherBenefitAdd_Date_2 = value.otherBenefitAdd_Date_2;
            gdsRange.otherBenefitInfo_2 = value.otherBenefitInfo_2;
            gdsRange.otherBenefitCoverage_2 = value.otherBenefitCoverage_2;
            gdsRange.otherBenefitServiceType_2 = value.otherBenefitServiceType_2;
            gdsRange.otherBenefitInsuranceType_2 = value.otherBenefitInsuranceType_2;
            gdsRange.otherBenefitPlan_Desc_2 = value.otherBenefitPlan_Desc_2;
            gdsRange.otherBenefitMessage1_2 = value.otherBenefitMessage1_2;
            gdsRange.otherBenefitTimePeriodQualifier_2 = value.otherBenefitTimePeriodQualifier_2;
            gdsRange.otherBenefitAmount_2 = value.otherBenefitAmount_2;
            gdsRange.otherBenefitQuantity_2 = value.otherBenefitQuantity_2;
            gdsRange.otherBenefitNetworkFlag_2 = value.otherBenefitNetworkFlag_2;
            gdsRange.otherBenefitBegDate_3 = value.otherBenefitBegDate_3;
            gdsRange.otherBenefitEndDate_3 = value.otherBenefitEndDate_3;
            gdsRange.otherBenefitAdd_Date_3 = value.otherBenefitAdd_Date_3;
            gdsRange.otherBenefitInfo_3 = value.otherBenefitInfo_3;
            gdsRange.otherBenefitCoverage_3 = value.otherBenefitCoverage_3;
            gdsRange.otherBenefitServiceType_3 = value.otherBenefitServiceType_3;
            gdsRange.otherBenefitInsuranceType_3 = value.otherBenefitInsuranceType_3;
            gdsRange.otherBenefitPlan_Desc_3 = value.otherBenefitPlan_Desc_3;
            gdsRange.otherBenefitMessage1_3 = value.otherBenefitMessage1_3;
            gdsRange.otherBenefitTimePeriodQualifier_3 = value.otherBenefitTimePeriodQualifier_3;
            gdsRange.otherBenefitAmount_3 = value.otherBenefitAmount_3;
            gdsRange.otherBenefitQuantity_3 = value.otherBenefitQuantity_3;
            gdsRange.otherBenefitNetworkFlag_3 = value.otherBenefitNetworkFlag_3;
            gdsRange.otherBenefitBegDate_4 = value.otherBenefitBegDate_4;
            gdsRange.otherBenefitEndDate_4 = value.otherBenefitEndDate_4;
            gdsRange.otherBenefitAdd_Date_4 = value.otherBenefitAdd_Date_4;
            gdsRange.otherBenefitInfo_4 = value.otherBenefitInfo_4;
            gdsRange.otherBenefitCoverage_4 = value.otherBenefitCoverage_4;
            gdsRange.otherBenefitServiceType_4 = value.otherBenefitServiceType_4;
            gdsRange.otherBenefitInsuranceType_4 = value.otherBenefitInsuranceType_4;
            gdsRange.otherBenefitPlan_Desc_4 = value.otherBenefitPlan_Desc_4;
            gdsRange.otherBenefitMessage1_4 = value.otherBenefitMessage1_4;
            gdsRange.otherBenefitTimePeriodQualifier_4 = value.otherBenefitTimePeriodQualifier_4;
            gdsRange.otherBenefitAmount_4 = value.otherBenefitAmount_4;
            gdsRange.otherBenefitQuantity_4 = value.otherBenefitQuantity_4;
            gdsRange.otherBenefitNetworkFlag_4 = value.otherBenefitNetworkFlag_4;
            gdsRange.otherBenefitBegDate_5 = value.otherBenefitBegDate_5;
            gdsRange.otherBenefitEndDate_5 = value.otherBenefitEndDate_5;
            gdsRange.otherBenefitAdd_Date_5 = value.otherBenefitAdd_Date_5;
            gdsRange.otherBenefitInfo_5 = value.otherBenefitInfo_5;
            gdsRange.otherBenefitCoverage_5 = value.otherBenefitCoverage_5;
            gdsRange.otherBenefitServiceType_5 = value.otherBenefitServiceType_5;
            gdsRange.otherBenefitInsuranceType_5 = value.otherBenefitInsuranceType_5;
            gdsRange.otherBenefitPlan_Desc_5 = value.otherBenefitPlan_Desc_5;
            gdsRange.otherBenefitMessage1_5 = value.otherBenefitMessage1_5;
            gdsRange.otherBenefitTimePeriodQualifier_5 = value.otherBenefitTimePeriodQualifier_5;
            gdsRange.otherBenefitAmount_5 = value.otherBenefitAmount_5;
            gdsRange.otherBenefitQuantity_5 = value.otherBenefitQuantity_5;
            gdsRange.otherBenefitNetworkFlag_5 = value.otherBenefitNetworkFlag_5;
            gdsRange.otherBenefitBegDate_6 = value.otherBenefitBegDate_6;
            gdsRange.otherBenefitEndDate_6 = value.otherBenefitEndDate_6;
            gdsRange.otherBenefitAdd_Date_6 = value.otherBenefitAdd_Date_6;
            gdsRange.otherBenefitInfo_6 = value.otherBenefitInfo_6;
            gdsRange.otherBenefitCoverage_6 = value.otherBenefitCoverage_6;
            gdsRange.otherBenefitServiceType_6 = value.otherBenefitServiceType_6;
            gdsRange.otherBenefitInsuranceType_6 = value.otherBenefitInsuranceType_6;
            gdsRange.otherBenefitPlan_Desc_6 = value.otherBenefitPlan_Desc_6;
            gdsRange.otherBenefitMessage1_6 = value.otherBenefitMessage1_6;
            gdsRange.otherBenefitTimePeriodQualifier_6 = value.otherBenefitTimePeriodQualifier_6;
            gdsRange.otherBenefitAmount_6 = value.otherBenefitAmount_6;
            gdsRange.otherBenefitQuantity_6 = value.otherBenefitQuantity_6;
            gdsRange.otherBenefitNetworkFlag_6 = value.otherBenefitNetworkFlag_6;
            gdsRange.otherBenefitBegDate_7 = value.otherBenefitBegDate_7;
            gdsRange.otherBenefitEndDate_7 = value.otherBenefitEndDate_7;
            gdsRange.otherBenefitAdd_Date_7 = value.otherBenefitAdd_Date_7;
            gdsRange.otherBenefitInfo_7 = value.otherBenefitInfo_7;
            gdsRange.otherBenefitCoverage_7 = value.otherBenefitCoverage_7;
            gdsRange.otherBenefitServiceType_7 = value.otherBenefitServiceType_7;
            gdsRange.otherBenefitInsuranceType_7 = value.otherBenefitInsuranceType_7;
            gdsRange.otherBenefitPlan_Desc_7 = value.otherBenefitPlan_Desc_7;
            gdsRange.otherBenefitMessage1_7 = value.otherBenefitMessage1_7;
            gdsRange.otherBenefitTimePeriodQualifier_7 = value.otherBenefitTimePeriodQualifier_7;
            gdsRange.otherBenefitAmount_7 = value.otherBenefitAmount_7;
            gdsRange.otherBenefitQuantity_7 = value.otherBenefitQuantity_7;
            gdsRange.otherBenefitNetworkFlag_7 = value.otherBenefitNetworkFlag_7;
            gdsRange.otherBenefitBegDate_8 = value.otherBenefitBegDate_8;
            gdsRange.otherBenefitEndDate_8 = value.otherBenefitEndDate_8;
            gdsRange.otherBenefitAdd_Date_8 = value.otherBenefitAdd_Date_8;
            gdsRange.otherBenefitInfo_8 = value.otherBenefitInfo_8;
            gdsRange.otherBenefitCoverage_8 = value.otherBenefitCoverage_8;
            gdsRange.otherBenefitServiceType_8 = value.otherBenefitServiceType_8;
            gdsRange.otherBenefitInsuranceType_8 = value.otherBenefitInsuranceType_8;
            gdsRange.otherBenefitPlan_Desc_8 = value.otherBenefitPlan_Desc_8;
            gdsRange.otherBenefitMessage1_8 = value.otherBenefitMessage1_8;
            gdsRange.otherBenefitTimePeriodQualifier_8 = value.otherBenefitTimePeriodQualifier_8;
            gdsRange.otherBenefitAmount_8 = value.otherBenefitAmount_8;
            gdsRange.otherBenefitQuantity_8 = value.otherBenefitQuantity_8;
            gdsRange.otherBenefitNetworkFlag_8 = value.otherBenefitNetworkFlag_8;
            gdsRange.transactionCnts = value.transactionCnts;
            gdsRange.validationNotes = value.validationNotes;
            gdsRange.validationScore = value.validationScore;
            //}}}


            async.waterfall([

                function(cb) {
                    var mostRecentAndActiveRange = null;
                    if (!gdsRange.rangeActive) {
                        mostRecentAndActiveRange = _.chain(gds.rangedData)
                            .sortBy(function(r) {
                                return moment(r.startDate)
                                    .format('YYYYMMDD') + "_" + moment(r.endDate)
                                    .format('YYYYMMDD');
                            })
                            .reverse()
                            .find(function(r) {
                                logger.silly(r.mco + ' != ' + gdsRange.mco + ' && ' + r.rangeActive);
                                return !r.mco.equals(gdsRange.mco) && r.rangeActive;
                            })
                            .value();
                    }
                    cb(null, mostRecentAndActiveRange);
                },
                function(ar, cb) {
                    if (!!ar) {
                        logger.silly('ar: ' + util.inspect({
                            start: ar.startDate,
                            end: ar.endDate,
                            active: ar.rangeActive
                        }));

                        var temp = _.clone(ar.toObject());
                        delete temp._id;
                        temp = new context.ranged.GDS(temp);

                        logger.debug('there was another range that was in a different mco and active');
                        //splice in the import record
                        dataUtil.dateSplice(gds.rangedData, gdsRange, function(err, rangesToSave) {
                            dataUtil.dateSpliceSave(self.actor, gds, rangesToSave, 'rangedData', function(err, savedGDS) {

                                //clone ar into the import record
                                gdsRange = null;
                                gdsRange = temp;
                                logger.silly('gds from active range: ' + util.inspect({
                                    start: gdsRange.startDate,
                                    end: gdsRange.endDate,
                                    active: gdsRange.rangeActive
                                }));

                                //re-retrieve gds with ranged data
                                context.GDS.findOne({
                                    student: student._id
                                })
                                    .populate('rangedData')
                                    .exec(function(err, gds) {
                                        cb(err, gds);
                                    });
                            });
                        })

                    } else {
                        logger.debug('gds: no overlap');
                        //hack: abuse scope lifting
                        cb(null, gds);
                    }
                }
            ], function(err, gds) {
                logger.silly('gds range mco: ' + gdsRange.mco);

                dataUtil.dateSplice(gds.rangedData, gdsRange, function(err, rangesToSave) {
                    if (!!err) {
                        logger.error(err);
                    }
                    dataUtil.dateSpliceSave(self.actor, gds, rangesToSave, 'rangedData', function(err, savedDistrict) {
                        if (!!err) {
                            logger.error(util.inspect(err));
                        }
                        cb(err);
                    });
                });
            });
        });
    };

    self.sheet.rowValidators = [{

        validator: function(value, callback) {

            var tasks = [

                function(cb) {
                    logger.silly('looking for student: ' + value.custDefined_In);
                    async.parallel({
                        byId: function(cb) {
                            context.Student.findOne({
                                _id: value.custDefined_In
                            })
                                .exec(function(err, student) {
                                    cb(null, student);
                                })
                        },
                        byStudentId: function(cb) {
                            context.Student.findOne({
                                studentId: value.custDefined_In
                            })
                                .exec(function(err, student) {
                                    cb(err, student);
                                });
                        }
                    }, function(err, r) {
                        var student = (r.byId || r.byStudentId);
                        cb(err, student);
                    })
                },
                function(student, cb) {
                    //populate student parts
                    async.waterfall([

                        function(cb) {
                            context.Student.populate(student, {
                                path: 'rangedData',
                                model: 'StudentRange'
                            }, cb);
                        },
                    ], function(err, student) {
                        cb(err, student);
                    })
                }
            ];

            async.waterfall(tasks, function(err, student) {
                if (!!student) {
                    student = student.toObject();
                    var student = dataUtil.getByDateRangeSync(student, 'rangedData', value.beg_In, value.end_In);
                    callback(null, !!student);
                } else {
                    //should be caught by ID validator
                    callback(null, true);
                }
            });
        },
        errorMessage: 'student not valid for range'
    }, {
        validator: function(value, callback) {

            var tasks = [

                function(cb) {
                    async.parallel({
                        byId: function(cb) {
                            context.Student.findOne({
                                _id: value.custDefined_In
                            })
                                .exec(function(err, student) {
                                    cb(null, student);
                                })
                        },
                        byStudentId: function(cb) {
                            context.Student.findOne({
                                studentId: value.custDefined_In
                            })
                                .exec(function(err, student) {
                                    cb(err, student);
                                });
                        }
                    }, function(err, r) {
                        var student = (r.byId || r.byStudentId);
                        cb(err, student);
                    })
                },
                function(student, cb) {
                    //populate student parts
                    async.waterfall([

                        function(cb) {
                            context.Student.populate(student, {
                                path: 'rangedData',
                                model: 'StudentRange'
                            }, cb);
                        },
                        function(student, cb) {
                            context.Student.populate(student, {
                                path: 'rangedData.school',
                                model: 'School'
                            }, cb);
                        },
                        function(student, cb) {
                            context.Student.populate(student, {
                                path: 'rangedData.school.district',
                                model: 'District'
                            }, cb);
                        }
                    ], function(err, student) {
                        cb(err, student);
                    })
                },
                function(student, cb) {
                    if (!!student) {
                        student = student.toObject();
                        var student = dataUtil.getByDateRangeSync(student, 'rangedData', value.beg_In, value.end_In);
                        if (!!student) {
                            cb(null, student, student.school.district);
                        } else {
                            logger.error('student was not valid during BCBST import');
                            cb(null, null, null);
                        }
                    } else {
                        cb(null, null, null);
                    }

                },
                function(student, district, cb) {
                    //find the uhc instance for that district
                    if (!student || !district) {
                        cb(null, null, null);
                    } else {
                        context.MCO.findOne({
                            district: district._id,
                            //name: /uhc|unitedhealthcare|united health care/i
                            name: new RegExp(value.dataSource, 'i')
                        })
                            .exec(function(err, mco) {
                                cb(err, student, mco);
                            });
                    }
                }
            ];



            async.waterfall(tasks, function(err, student, mco) {
                callback(null, !!mco);
            });
        },
        errorMessage: 'MCO for BCBST does not exist.'
    }];
    return self;
};
GdsBCBSTImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = GdsBCBSTImport;
