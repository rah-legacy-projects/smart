var logger = require('winston'),
    context = require('../models/context'),
    _ = require('lodash'),
    async = require('async'),
    moment = require('moment-timezone'),
    dataUtil = require('../models/dataUtil'),
    util = require('util');

var GdsMedicaidImport = function(actor, clientTimezone) {
    var self = this;
    self.actor = actor || "[import]";
    self.clientTimezone = clientTimezone;
    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'GDS Medicaid Import',
        className: 'gdsMedicaid',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'PageNum',
            propertyName: 'pageNum',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PageNum needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PageNum]'
            }]
        }, {
            name: 'DataSource',
            propertyName: 'dataSource',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for DataSource needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for DataSource]'
            }]
        }, {
            name: 'AccountNbr_In',
            propertyName: 'accountNbr_In',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for AccountNbr_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for AccountNbr_In]'
            }]
        }, {
            name: 'RecipID1_In',
            propertyName: 'recipID1_In',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for RecipID1_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for RecipID1_In]'
            }]
        }, {
            name: 'SSN_Inp',
            propertyName: 'sSN_Inp',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for SSN_Inp needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for SSN_Inp]'
            }]
        }, {
            name: 'LName1_In',
            propertyName: 'lName1_In',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for LName1_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for LName1_In]'
            }]
        }, {
            name: 'FName1_In',
            propertyName: 'fName1_In',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for FName1_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for FName1_In]'
            }]
        }, {
            name: 'MI1_In',
            propertyName: 'mI1_In',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for MI1_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for MI1_In]'
            }]
        }, {
            name: 'DOB_In',
            propertyName: 'dOB_In',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for DOB_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for DOB_In]'
            }]
        }, {
            name: 'Gender_In',
            propertyName: 'gender_In',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Gender_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Gender_In]'
            }]
        }, {
            name: 'CustDefined_In',
            propertyName: 'custDefined_In',
            expectedType: 'string',
            isIdentity: true,
            isLocked: false,
            columnHelp: [{
                text: '[column help for CustDefined_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: function(value, callback) {
                    async.parallel({
                        byId: function(cb) {
                            context.Student.findOne({
                                _id: value
                            })
                                .exec(function(err, student) {
                                    if (!!err) {
                                        //logger.error(err);
                                    }
                                    //ignore if the value was not an objid
                                    cb(null, !!student);
                                })
                        },
                        byStudentId: function(cb) {
                            context.Student.findOne({
                                studentId: value
                            })
                                .exec(function(err, student) {
                                    if (!!err) {
                                        logger.error('by state id: ' + err);
                                    }
                                    cb(err, !!student);
                                });
                        }
                    }, function(err, r) {
                        callback(err, r.byId || r.byStudentId);
                    });
                },
                errorMessage: '[rules needed for CustDefined_In]'
            }]
        }, {
            name: 'Beg_In',
            propertyName: 'beg_In',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Beg_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Beg_In]'
            }]
        }, {
            name: 'End_In',
            propertyName: 'end_In',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for End_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for End_In]'
            }]
        }, {
            name: 'Los_In',
            propertyName: 'los_In',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Los_In needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Los_In]'
            }]
        }, {
            name: 'ErrorMsg',
            propertyName: 'errorMsg',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for ErrorMsg needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for ErrorMsg]'
            }]
        }, {
            name: 'StatusFlag',
            propertyName: 'statusFlag',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for StatusFlag needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for StatusFlag]'
            }]
        }, {
            name: 'McaidFlag',
            propertyName: 'mcaidFlag',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for McaidFlag needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /[YNyn]/,
                errorMessage: '[rules needed for McaidFlag]'
            }]
        }, {
            name: 'MCRA_Flag',
            propertyName: 'mCRA_Flag',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for MCRA_Flag needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for MCRA_Flag]'
            }]
        }, {
            name: 'MCRB_Flag',
            propertyName: 'mCRB_Flag',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for MCRB_Flag needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for MCRB_Flag]'
            }]
        }, {
            name: 'HMO_Flag',
            propertyName: 'hMO_Flag',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for HMO_Flag needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for HMO_Flag]'
            }]
        }, {
            name: 'GDS_Ref_No',
            propertyName: 'gDS_Ref_No',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for GDS_Ref_No needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for GDS_Ref_No]'
            }]
        }, {
            name: 'SearchCode',
            propertyName: 'searchCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for SearchCode needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for SearchCode]'
            }]
        }, {
            name: 'Split_Serial_Num',
            propertyName: 'split_Serial_Num',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Split_Serial_Num needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Split_Serial_Num]'
            }]
        }, {
            name: 'McaidNum',
            propertyName: 'mcaidNum',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for McaidNum needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for McaidNum]'
            }]
        }, {
            name: 'SSN',
            propertyName: 'sSN',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for SSN needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for SSN]'
            }]
        }, {
            name: 'Lname',
            propertyName: 'lname',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Lname needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Lname]'
            }]
        }, {
            name: 'Fname',
            propertyName: 'fname',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Fname needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Fname]'
            }]
        }, {
            name: 'Mname',
            propertyName: 'mname',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Mname needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Mname]'
            }]
        }, {
            name: 'DOB',
            propertyName: 'dOB',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for DOB needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for DOB]'
            }]
        }, {
            name: 'Gender',
            propertyName: 'gender',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Gender needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Gender]'
            }]
        }, {
            name: 'HICNum',
            propertyName: 'hICNum',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for HICNum needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for HICNum]'
            }]
        }, {
            name: 'Address1',
            propertyName: 'address1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Address1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Address1]'
            }]
        }, {
            name: 'Address2',
            propertyName: 'address2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Address2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Address2]'
            }]
        }, {
            name: 'City',
            propertyName: 'city',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for City needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for City]'
            }]
        }, {
            name: 'State',
            propertyName: 'state',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for State needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for State]'
            }]
        }, {
            name: 'Zip',
            propertyName: 'zip',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Zip needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Zip]'
            }]
        }, {
            name: 'McaidDays_1',
            propertyName: 'mcaidDays_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for McaidDays_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for McaidDays_1]'
            }]
        }, {
            name: 'BegDate_1',
            propertyName: 'begDate_1',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for BegDate_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for BegDate_1]'
            }]
        }, {
            name: 'EndDate_1',
            propertyName: 'endDate_1',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for EndDate_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for EndDate_1]'
            }]
        }, {
            name: 'Status_1',
            propertyName: 'status_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Status_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Status_1]'
            }]
        }, {
            name: 'Coverage_1',
            propertyName: 'coverage_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Coverage_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Coverage_1]'
            }]
        }, {
            name: 'ServiceType_1',
            propertyName: 'serviceType_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for ServiceType_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for ServiceType_1]'
            }]
        }, {
            name: 'InsuranceType_1',
            propertyName: 'insuranceType_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for InsuranceType_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for InsuranceType_1]'
            }]
        }, {
            name: 'Plan_Desc_1',
            propertyName: 'plan_Desc_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Plan_Desc_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Plan_Desc_1]'
            }]
        }, {
            name: 'McaidDays_2',
            propertyName: 'mcaidDays_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for McaidDays_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for McaidDays_2]'
            }]
        }, {
            name: 'BegDate_2',
            propertyName: 'begDate_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for BegDate_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for BegDate_2]'
            }]
        }, {
            name: 'EndDate_2',
            propertyName: 'endDate_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for EndDate_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for EndDate_2]'
            }]
        }, {
            name: 'Status_2',
            propertyName: 'status_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Status_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Status_2]'
            }]
        }, {
            name: 'Coverage_2',
            propertyName: 'coverage_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Coverage_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Coverage_2]'
            }]
        }, {
            name: 'ServiceType_2',
            propertyName: 'serviceType_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for ServiceType_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for ServiceType_2]'
            }]
        }, {
            name: 'InsuranceType_2',
            propertyName: 'insuranceType_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for InsuranceType_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for InsuranceType_2]'
            }]
        }, {
            name: 'Plan_Desc_2',
            propertyName: 'plan_Desc_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Plan_Desc_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Plan_Desc_2]'
            }]
        }, {
            name: 'McaidDays_3',
            propertyName: 'mcaidDays_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for McaidDays_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for McaidDays_3]'
            }]
        }, {
            name: 'BegDate_3',
            propertyName: 'begDate_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for BegDate_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for BegDate_3]'
            }]
        }, {
            name: 'EndDate_3',
            propertyName: 'endDate_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for EndDate_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for EndDate_3]'
            }]
        }, {
            name: 'Status_3',
            propertyName: 'status_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Status_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Status_3]'
            }]
        }, {
            name: 'Coverage_3',
            propertyName: 'coverage_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Coverage_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Coverage_3]'
            }]
        }, {
            name: 'ServiceType_3',
            propertyName: 'serviceType_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for ServiceType_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for ServiceType_3]'
            }]
        }, {
            name: 'InsuranceType_3',
            propertyName: 'insuranceType_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for InsuranceType_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for InsuranceType_3]'
            }]
        }, {
            name: 'Plan_Desc_3',
            propertyName: 'plan_Desc_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Plan_Desc_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Plan_Desc_3]'
            }]
        }, {
            name: 'McaidDays_4',
            propertyName: 'mcaidDays_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for McaidDays_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for McaidDays_4]'
            }]
        }, {
            name: 'BegDate_4',
            propertyName: 'begDate_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for BegDate_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for BegDate_4]'
            }]
        }, {
            name: 'EndDate_4',
            propertyName: 'endDate_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for EndDate_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for EndDate_4]'
            }]
        }, {
            name: 'Status_4',
            propertyName: 'status_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Status_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Status_4]'
            }]
        }, {
            name: 'Coverage_4',
            propertyName: 'coverage_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Coverage_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Coverage_4]'
            }]
        }, {
            name: 'ServiceType_4',
            propertyName: 'serviceType_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for ServiceType_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for ServiceType_4]'
            }]
        }, {
            name: 'InsuranceType_4',
            propertyName: 'insuranceType_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for InsuranceType_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for InsuranceType_4]'
            }]
        }, {
            name: 'Plan_Desc_4',
            propertyName: 'plan_Desc_4',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Plan_Desc_4 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Plan_Desc_4]'
            }]
        }, {
            name: 'McaidDays_5',
            propertyName: 'mcaidDays_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for McaidDays_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for McaidDays_5]'
            }]
        }, {
            name: 'BegDate_5',
            propertyName: 'begDate_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for BegDate_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for BegDate_5]'
            }]
        }, {
            name: 'EndDate_5',
            propertyName: 'endDate_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for EndDate_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for EndDate_5]'
            }]
        }, {
            name: 'Status_5',
            propertyName: 'status_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Status_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Status_5]'
            }]
        }, {
            name: 'Coverage_5',
            propertyName: 'coverage_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Coverage_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Coverage_5]'
            }]
        }, {
            name: 'ServiceType_5',
            propertyName: 'serviceType_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for ServiceType_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for ServiceType_5]'
            }]
        }, {
            name: 'InsuranceType_5',
            propertyName: 'insuranceType_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for InsuranceType_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for InsuranceType_5]'
            }]
        }, {
            name: 'Plan_Desc_5',
            propertyName: 'plan_Desc_5',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Plan_Desc_5 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Plan_Desc_5]'
            }]
        }, {
            name: 'McaidDays_6',
            propertyName: 'mcaidDays_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for McaidDays_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for McaidDays_6]'
            }]
        }, {
            name: 'BegDate_6',
            propertyName: 'begDate_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for BegDate_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for BegDate_6]'
            }]
        }, {
            name: 'EndDate_6',
            propertyName: 'endDate_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for EndDate_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for EndDate_6]'
            }]
        }, {
            name: 'Status_6',
            propertyName: 'status_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Status_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Status_6]'
            }]
        }, {
            name: 'Coverage_6',
            propertyName: 'coverage_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Coverage_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Coverage_6]'
            }]
        }, {
            name: 'ServiceType_6',
            propertyName: 'serviceType_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for ServiceType_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for ServiceType_6]'
            }]
        }, {
            name: 'InsuranceType_6',
            propertyName: 'insuranceType_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for InsuranceType_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for InsuranceType_6]'
            }]
        }, {
            name: 'Plan_Desc_6',
            propertyName: 'plan_Desc_6',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Plan_Desc_6 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Plan_Desc_6]'
            }]
        }, {
            name: 'PartADays_1',
            propertyName: 'partADays_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartADays_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartADays_1]'
            }]
        }, {
            name: 'PartABeg_1',
            propertyName: 'partABeg_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartABeg_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartABeg_1]'
            }]
        }, {
            name: 'PartAEnd_1',
            propertyName: 'partAEnd_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartAEnd_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartAEnd_1]'
            }]
        }, {
            name: 'PartAStatus_1',
            propertyName: 'partAStatus_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartAStatus_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartAStatus_1]'
            }]
        }, {
            name: 'PartACovLevel_1',
            propertyName: 'partACovLevel_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartACovLevel_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartACovLevel_1]'
            }]
        }, {
            name: 'PartADays_2',
            propertyName: 'partADays_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartADays_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartADays_2]'
            }]
        }, {
            name: 'PartABeg_2',
            propertyName: 'partABeg_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartABeg_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartABeg_2]'
            }]
        }, {
            name: 'PartAEnd_2',
            propertyName: 'partAEnd_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartAEnd_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartAEnd_2]'
            }]
        }, {
            name: 'PartAStatus_2',
            propertyName: 'partAStatus_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartAStatus_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartAStatus_2]'
            }]
        }, {
            name: 'PartACovLevel_2',
            propertyName: 'partACovLevel_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartACovLevel_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartACovLevel_2]'
            }]
        }, {
            name: 'PartBDays_1',
            propertyName: 'partBDays_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartBDays_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartBDays_1]'
            }]
        }, {
            name: 'PartBBeg_1',
            propertyName: 'partBBeg_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartBBeg_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartBBeg_1]'
            }]
        }, {
            name: 'PartBEnd_1',
            propertyName: 'partBEnd_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartBEnd_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartBEnd_1]'
            }]
        }, {
            name: 'PartBStatus_1',
            propertyName: 'partBStatus_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartBStatus_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartBStatus_1]'
            }]
        }, {
            name: 'PartBCovLevel_1',
            propertyName: 'partBCovLevel_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartBCovLevel_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartBCovLevel_1]'
            }]
        }, {
            name: 'PartBDays_2',
            propertyName: 'partBDays_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartBDays_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartBDays_2]'
            }]
        }, {
            name: 'PartBBeg_2',
            propertyName: 'partBBeg_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartBBeg_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartBBeg_2]'
            }]
        }, {
            name: 'PartBEnd_2',
            propertyName: 'partBEnd_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartBEnd_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartBEnd_2]'
            }]
        }, {
            name: 'PartBStatus_2',
            propertyName: 'partBStatus_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartBStatus_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartBStatus_2]'
            }]
        }, {
            name: 'PartBCovLevel_2',
            propertyName: 'partBCovLevel_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartBCovLevel_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartBCovLevel_2]'
            }]
        }, {
            name: 'McaidDays',
            propertyName: 'mcaidDays',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for McaidDays needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for McaidDays]'
            }]
        }, {
            name: 'PartADays',
            propertyName: 'partADays',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartADays needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartADays]'
            }]
        }, {
            name: 'Mcaid_PartADays',
            propertyName: 'mcaid_PartADays',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Mcaid_PartADays needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Mcaid_PartADays]'
            }]
        }, {
            name: 'PartBDays',
            propertyName: 'partBDays',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartBDays needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartBDays]'
            }]
        }, {
            name: 'Mcaid_PartBDays',
            propertyName: 'mcaid_PartBDays',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Mcaid_PartBDays needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Mcaid_PartBDays]'
            }]
        }, {
            name: 'PartA_PartBDays',
            propertyName: 'partA_PartBDays',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for PartA_PartBDays needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for PartA_PartBDays]'
            }]
        }, {
            name: 'Mcaid_PartA_PartBDays',
            propertyName: 'mcaid_PartA_PartBDays',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Mcaid_PartA_PartBDays needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Mcaid_PartA_PartBDays]'
            }]
        }, {
            name: 'OtherPayerBegDate_1',
            propertyName: 'otherPayerBegDate_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerBegDate_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerBegDate_1]'
            }]
        }, {
            name: 'OtherPayerEndDate_1',
            propertyName: 'otherPayerEndDate_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerEndDate_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerEndDate_1]'
            }]
        }, {
            name: 'OtherPayerAdd_Date_1',
            propertyName: 'otherPayerAdd_Date_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerAdd_Date_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerAdd_Date_1]'
            }]
        }, {
            name: 'OtherPayerInfo_1',
            propertyName: 'otherPayerInfo_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerInfo_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerInfo_1]'
            }]
        }, {
            name: 'OtherPayerCoverage_1',
            propertyName: 'otherPayerCoverage_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerCoverage_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerCoverage_1]'
            }]
        }, {
            name: 'OtherPayerServiceType_1',
            propertyName: 'otherPayerServiceType_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerServiceType_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerServiceType_1]'
            }]
        }, {
            name: 'OtherPayerInsuranceType_1',
            propertyName: 'otherPayerInsuranceType_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerInsuranceType_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerInsuranceType_1]'
            }]
        }, {
            name: 'OtherPayerPlan_Desc_1',
            propertyName: 'otherPayerPlan_Desc_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerPlan_Desc_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for OtherPayerPlan_Desc_1]'
            }]
        }, {
            name: 'OtherPayerMessage1_1',
            propertyName: 'otherPayerMessage1_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerMessage1_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerMessage1_1]'
            }]
        }, {
            name: 'OtherPayerMessage2_1',
            propertyName: 'otherPayerMessage2_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerMessage2_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerMessage2_1]'
            }]
        }, {
            name: 'OtherPayerIdentifierCode_1',
            propertyName: 'otherPayerIdentifierCode_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerIdentifierCode_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerIdentifierCode_1]'
            }]
        }, {
            name: 'OtherPayer_Name_1',
            propertyName: 'otherPayer_Name_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_Name_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_Name_1]'
            }]
        }, {
            name: 'OtherPayer_RefID_1',
            propertyName: 'otherPayer_RefID_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_RefID_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_RefID_1]'
            }]
        }, {
            name: 'OtherPayer_Tel_1',
            propertyName: 'otherPayer_Tel_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_Tel_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_Tel_1]'
            }]
        }, {
            name: 'OtherPayer_address1_1',
            propertyName: 'otherPayer_address1_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_address1_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_address1_1]'
            }]
        }, {
            name: 'OtherPayer_address2_1',
            propertyName: 'otherPayer_address2_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_address2_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_address2_1]'
            }]
        }, {
            name: 'OtherPayer_City_1',
            propertyName: 'otherPayer_City_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_City_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_City_1]'
            }]
        }, {
            name: 'OtherPayer_State_1',
            propertyName: 'otherPayer_State_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_State_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_State_1]'
            }]
        }, {
            name: 'OtherPayer_Zipcode_1',
            propertyName: 'otherPayer_Zipcode_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_Zipcode_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_Zipcode_1]'
            }]
        }, {
            name: 'OtherPayerBegDate_2',
            propertyName: 'otherPayerBegDate_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerBegDate_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerBegDate_2]'
            }]
        }, {
            name: 'OtherPayerEndDate_2',
            propertyName: 'otherPayerEndDate_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerEndDate_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerEndDate_2]'
            }]
        }, {
            name: 'OtherPayerAdd_Date_2',
            propertyName: 'otherPayerAdd_Date_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerAdd_Date_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerAdd_Date_2]'
            }]
        }, {
            name: 'OtherPayerInfo_2',
            propertyName: 'otherPayerInfo_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerInfo_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerInfo_2]'
            }]
        }, {
            name: 'OtherPayerCoverage_2',
            propertyName: 'otherPayerCoverage_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerCoverage_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerCoverage_2]'
            }]
        }, {
            name: 'OtherPayerServiceType_2',
            propertyName: 'otherPayerServiceType_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerServiceType_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerServiceType_2]'
            }]
        }, {
            name: 'OtherPayerInsuranceType_2',
            propertyName: 'otherPayerInsuranceType_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerInsuranceType_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerInsuranceType_2]'
            }]
        }, {
            name: 'OtherPayerPlan_Desc_2',
            propertyName: 'otherPayerPlan_Desc_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerPlan_Desc_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerPlan_Desc_2]'
            }]
        }, {
            name: 'OtherPayerMessage1_2',
            propertyName: 'otherPayerMessage1_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerMessage1_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerMessage1_2]'
            }]
        }, {
            name: 'OtherPayerMessage2_2',
            propertyName: 'otherPayerMessage2_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerMessage2_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerMessage2_2]'
            }]
        }, {
            name: 'OtherPayerIdentifierCode_2',
            propertyName: 'otherPayerIdentifierCode_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayerIdentifierCode_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayerIdentifierCode_2]'
            }]
        }, {
            name: 'OtherPayer_Name_2',
            propertyName: 'otherPayer_Name_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_Name_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_Name_2]'
            }]
        }, {
            name: 'OtherPayer_RefID_2',
            propertyName: 'otherPayer_RefID_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_RefID_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_RefID_2]'
            }]
        }, {
            name: 'OtherPayer_Tel_2',
            propertyName: 'otherPayer_Tel_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_Tel_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_Tel_2]'
            }]
        }, {
            name: 'OtherPayer_address1_2',
            propertyName: 'otherPayer_address1_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_address1_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_address1_2]'
            }]
        }, {
            name: 'OtherPayer_address2_2',
            propertyName: 'otherPayer_address2_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_address2_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_address2_2]'
            }]
        }, {
            name: 'OtherPayer_City_2',
            propertyName: 'otherPayer_City_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_City_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_City_2]'
            }]
        }, {
            name: 'OtherPayer_State_2',
            propertyName: 'otherPayer_State_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_State_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_State_2]'
            }]
        }, {
            name: 'OtherPayer_Zipcode_2',
            propertyName: 'otherPayer_Zipcode_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherPayer_Zipcode_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherPayer_Zipcode_2]'
            }]
        }, {
            name: 'OtherBenefitBegDate_1',
            propertyName: 'otherBenefitBegDate_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitBegDate_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitBegDate_1]'
            }]
        }, {
            name: 'OtherBenefitEndDate_1',
            propertyName: 'otherBenefitEndDate_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitEndDate_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitEndDate_1]'
            }]
        }, {
            name: 'OtherBenefitAdd_Date_1',
            propertyName: 'otherBenefitAdd_Date_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAdd_Date_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAdd_Date_1]'
            }]
        }, {
            name: 'OtherBenefitInfo_1',
            propertyName: 'otherBenefitInfo_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInfo_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInfo_1]'
            }]
        }, {
            name: 'OtherBenefitCoverage_1',
            propertyName: 'otherBenefitCoverage_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitCoverage_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitCoverage_1]'
            }]
        }, {
            name: 'OtherBenefitServiceType_1',
            propertyName: 'otherBenefitServiceType_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitServiceType_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitServiceType_1]'
            }]
        }, {
            name: 'OtherBenefitInsuranceType_1',
            propertyName: 'otherBenefitInsuranceType_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInsuranceType_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInsuranceType_1]'
            }]
        }, {
            name: 'OtherBenefitPlan_Desc_1',
            propertyName: 'otherBenefitPlan_Desc_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitPlan_Desc_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitPlan_Desc_1]'
            }]
        }, {
            name: 'OtherBenefitMessage1_1',
            propertyName: 'otherBenefitMessage1_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitMessage1_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitMessage1_1]'
            }]
        }, {
            name: 'OtherBenefitTimePeriodQualifier_1',
            propertyName: 'otherBenefitTimePeriodQualifier_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitTimePeriodQualifier_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitTimePeriodQualifier_1]'
            }]
        }, {
            name: 'OtherBenefitAmount_1',
            propertyName: 'otherBenefitAmount_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAmount_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAmount_1]'
            }]
        }, {
            name: 'OtherBenefitQuantity_1',
            propertyName: 'otherBenefitQuantity_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitQuantity_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitQuantity_1]'
            }]
        }, {
            name: 'OtherBenefitNetworkFlag_1',
            propertyName: 'otherBenefitNetworkFlag_1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitNetworkFlag_1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitNetworkFlag_1]'
            }]
        }, {
            name: 'OtherBenefitBegDate_2',
            propertyName: 'otherBenefitBegDate_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitBegDate_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitBegDate_2]'
            }]
        }, {
            name: 'OtherBenefitEndDate_2',
            propertyName: 'otherBenefitEndDate_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitEndDate_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitEndDate_2]'
            }]
        }, {
            name: 'OtherBenefitAdd_Date_2',
            propertyName: 'otherBenefitAdd_Date_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAdd_Date_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAdd_Date_2]'
            }]
        }, {
            name: 'OtherBenefitInfo_2',
            propertyName: 'otherBenefitInfo_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInfo_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInfo_2]'
            }]
        }, {
            name: 'OtherBenefitCoverage_2',
            propertyName: 'otherBenefitCoverage_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitCoverage_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitCoverage_2]'
            }]
        }, {
            name: 'OtherBenefitServiceType_2',
            propertyName: 'otherBenefitServiceType_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitServiceType_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitServiceType_2]'
            }]
        }, {
            name: 'OtherBenefitInsuranceType_2',
            propertyName: 'otherBenefitInsuranceType_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInsuranceType_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInsuranceType_2]'
            }]
        }, {
            name: 'OtherBenefitPlan_Desc_2',
            propertyName: 'otherBenefitPlan_Desc_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitPlan_Desc_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitPlan_Desc_2]'
            }]
        }, {
            name: 'OtherBenefitMessage1_2',
            propertyName: 'otherBenefitMessage1_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitMessage1_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitMessage1_2]'
            }]
        }, {
            name: 'OtherBenefitTimePeriodQualifier_2',
            propertyName: 'otherBenefitTimePeriodQualifier_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitTimePeriodQualifier_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitTimePeriodQualifier_2]'
            }]
        }, {
            name: 'OtherBenefitAmount_2',
            propertyName: 'otherBenefitAmount_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAmount_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAmount_2]'
            }]
        }, {
            name: 'OtherBenefitQuantity_2',
            propertyName: 'otherBenefitQuantity_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitQuantity_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitQuantity_2]'
            }]
        }, {
            name: 'OtherBenefitNetworkFlag_2',
            propertyName: 'otherBenefitNetworkFlag_2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitNetworkFlag_2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitNetworkFlag_2]'
            }]
        }, {
            name: 'OtherBenefitBegDate_3',
            propertyName: 'otherBenefitBegDate_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitBegDate_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitBegDate_3]'
            }]
        }, {
            name: 'OtherBenefitEndDate_3',
            propertyName: 'otherBenefitEndDate_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitEndDate_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitEndDate_3]'
            }]
        }, {
            name: 'OtherBenefitAdd_Date_3',
            propertyName: 'otherBenefitAdd_Date_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAdd_Date_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAdd_Date_3]'
            }]
        }, {
            name: 'OtherBenefitInfo_3',
            propertyName: 'otherBenefitInfo_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInfo_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInfo_3]'
            }]
        }, {
            name: 'OtherBenefitCoverage_3',
            propertyName: 'otherBenefitCoverage_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitCoverage_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitCoverage_3]'
            }]
        }, {
            name: 'OtherBenefitServiceType_3',
            propertyName: 'otherBenefitServiceType_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitServiceType_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitServiceType_3]'
            }]
        }, {
            name: 'OtherBenefitInsuranceType_3',
            propertyName: 'otherBenefitInsuranceType_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitInsuranceType_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitInsuranceType_3]'
            }]
        }, {
            name: 'OtherBenefitPlan_Desc_3',
            propertyName: 'otherBenefitPlan_Desc_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitPlan_Desc_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitPlan_Desc_3]'
            }]
        }, {
            name: 'OtherBenefitMessage1_3',
            propertyName: 'otherBenefitMessage1_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitMessage1_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitMessage1_3]'
            }]
        }, {
            name: 'OtherBenefitTimePeriodQualifier_3',
            propertyName: 'otherBenefitTimePeriodQualifier_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitTimePeriodQualifier_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitTimePeriodQualifier_3]'
            }]
        }, {
            name: 'OtherBenefitAmount_3',
            propertyName: 'otherBenefitAmount_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitAmount_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitAmount_3]'
            }]
        }, {
            name: 'OtherBenefitQuantity_3',
            propertyName: 'otherBenefitQuantity_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitQuantity_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitQuantity_3]'
            }]
        }, {
            name: 'OtherBenefitNetworkFlag_3',
            propertyName: 'otherBenefitNetworkFlag_3',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for OtherBenefitNetworkFlag_3 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for OtherBenefitNetworkFlag_3]'
            }]
        }, {
            name: 'TransactionCnts',
            propertyName: 'transactionCnts',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for TransactionCnts needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for TransactionCnts]'
            }]
        }, {
            name: 'ValidationNotes',
            propertyName: 'validationNotes',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for ValidationNotes needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for ValidationNotes]'
            }]
        }, {
            name: 'ValidationScore',
            propertyName: 'validationScore',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for ValidationScore needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for ValidationScore]'
            }]
        }, ]
    };

    self.diff = function(value, index, cb) {
        context.GDSTennCare.findOne({
            student: value.custDefined_In
        })
            .populate('rangedData')
            .lean()
            .exec(function(err, gds) {
                if (!gds) {
                    cb(err, null);
                    return;
                }

                var startMoment = moment.tz(value.begDate_1, self.clientTimezone)
                            .startOf('day'),
                            endMoment = moment.tz(value.endDate_1, self.clientTimezone)
                            .endOf('day');

                logger.silly('diff range: ' + startMoment.toString() + ' - ' + endMoment.toString());

                dataUtil.getByDateRange(
                    gds,
                    'rangedData',
                    startMoment,
                    endMoment,
                    function(err, flat) {
                        cb(err, {
                            //{{{ flat record
                            pageNum: flat.pageNum,
                            dataSource: flat.dataSource,
                            accountNbr_In: flat.accountNbr_In,
                            recipID1_In: flat.recipID1_In,
                            sSN_Inp: flat.sSN_Inp,
                            lName1_In: flat.lName1_In,
                            fName1_In: flat.fName1_In,
                            mI1_In: flat.mI1_In,
                            dOB_In: flat.dOB_In,
                            gender_In: flat.gender_In,
                            custDefined_In: flat.custDefined_In,
                            beg_In: flat.beg_In,
                            end_In: flat.end_In,
                            los_In: flat.los_In,
                            errorMsg: flat.errorMsg,
                            statusFlag: flat.statusFlag,
                            mcaidFlag: flat.mcaidFlag,
                            mCRA_Flag: flat.mCRA_Flag,
                            mCRB_Flag: flat.mCRB_Flag,
                            hMO_Flag: flat.hMO_Flag,
                            gDS_Ref_No: flat.gDS_Ref_No,
                            searchCode: flat.searchCode,
                            split_Serial_Num: flat.split_Serial_Num,
                            mcaidNum: flat.mcaidNum,
                            sSN: flat.sSN,
                            lname: flat.lname,
                            fname: flat.fname,
                            mname: flat.mname,
                            dOB: flat.dOB,
                            gender: flat.gender,
                            hICNum: flat.hICNum,
                            address1: flat.address1,
                            address2: flat.address2,
                            city: flat.city,
                            state: flat.state,
                            zip: flat.zip,
                            mcaidDays_1: flat.mcaidDays_1,
                            begDate_1: flat.begDate_1,
                            endDate_1: flat.endDate_1,
                            status_1: flat.status_1,
                            coverage_1: flat.coverage_1,
                            serviceType_1: flat.serviceType_1,
                            insuranceType_1: flat.insuranceType_1,
                            plan_Desc_1: flat.plan_Desc_1,
                            mcaidDays_2: flat.mcaidDays_2,
                            begDate_2: flat.begDate_2,
                            endDate_2: flat.endDate_2,
                            status_2: flat.status_2,
                            coverage_2: flat.coverage_2,
                            serviceType_2: flat.serviceType_2,
                            insuranceType_2: flat.insuranceType_2,
                            plan_Desc_2: flat.plan_Desc_2,
                            mcaidDays_3: flat.mcaidDays_3,
                            begDate_3: flat.begDate_3,
                            endDate_3: flat.endDate_3,
                            status_3: flat.status_3,
                            coverage_3: flat.coverage_3,
                            serviceType_3: flat.serviceType_3,
                            insuranceType_3: flat.insuranceType_3,
                            plan_Desc_3: flat.plan_Desc_3,
                            mcaidDays_4: flat.mcaidDays_4,
                            begDate_4: flat.begDate_4,
                            endDate_4: flat.endDate_4,
                            status_4: flat.status_4,
                            coverage_4: flat.coverage_4,
                            serviceType_4: flat.serviceType_4,
                            insuranceType_4: flat.insuranceType_4,
                            plan_Desc_4: flat.plan_Desc_4,
                            mcaidDays_5: flat.mcaidDays_5,
                            begDate_5: flat.begDate_5,
                            endDate_5: flat.endDate_5,
                            status_5: flat.status_5,
                            coverage_5: flat.coverage_5,
                            serviceType_5: flat.serviceType_5,
                            insuranceType_5: flat.insuranceType_5,
                            plan_Desc_5: flat.plan_Desc_5,
                            mcaidDays_6: flat.mcaidDays_6,
                            begDate_6: flat.begDate_6,
                            endDate_6: flat.endDate_6,
                            status_6: flat.status_6,
                            coverage_6: flat.coverage_6,
                            serviceType_6: flat.serviceType_6,
                            insuranceType_6: flat.insuranceType_6,
                            plan_Desc_6: flat.plan_Desc_6,
                            partADays_1: flat.partADays_1,
                            partABeg_1: flat.partABeg_1,
                            partAEnd_1: flat.partAEnd_1,
                            partAStatus_1: flat.partAStatus_1,
                            partACovLevel_1: flat.partACovLevel_1,
                            partADays_2: flat.partADays_2,
                            partABeg_2: flat.partABeg_2,
                            partAEnd_2: flat.partAEnd_2,
                            partAStatus_2: flat.partAStatus_2,
                            partACovLevel_2: flat.partACovLevel_2,
                            partBDays_1: flat.partBDays_1,
                            partBBeg_1: flat.partBBeg_1,
                            partBEnd_1: flat.partBEnd_1,
                            partBStatus_1: flat.partBStatus_1,
                            partBCovLevel_1: flat.partBCovLevel_1,
                            partBDays_2: flat.partBDays_2,
                            partBBeg_2: flat.partBBeg_2,
                            partBEnd_2: flat.partBEnd_2,
                            partBStatus_2: flat.partBStatus_2,
                            partBCovLevel_2: flat.partBCovLevel_2,
                            mcaidDays: flat.mcaidDays,
                            partADays: flat.partADays,
                            mcaid_PartADays: flat.mcaid_PartADays,
                            partBDays: flat.partBDays,
                            mcaid_PartBDays: flat.mcaid_PartBDays,
                            partA_PartBDays: flat.partA_PartBDays,
                            mcaid_PartA_PartBDays: flat.mcaid_PartA_PartBDays,
                            otherPayerBegDate_1: flat.otherPayerBegDate_1,
                            otherPayerEndDate_1: flat.otherPayerEndDate_1,
                            otherPayerAdd_Date_1: flat.otherPayerAdd_Date_1,
                            otherPayerInfo_1: flat.otherPayerInfo_1,
                            otherPayerCoverage_1: flat.otherPayerCoverage_1,
                            otherPayerServiceType_1: flat.otherPayerServiceType_1,
                            otherPayerInsuranceType_1: flat.otherPayerInsuranceType_1,
                            otherPayerPlan_Desc_1: flat.otherPayerPlan_Desc_1,
                            otherPayerMessage1_1: flat.otherPayerMessage1_1,
                            otherPayerMessage2_1: flat.otherPayerMessage2_1,
                            otherPayerIdentifierCode_1: flat.otherPayerIdentifierCode_1,
                            otherPayer_Name_1: flat.otherPayer_Name_1,
                            otherPayer_RefID_1: flat.otherPayer_RefID_1,
                            otherPayer_Tel_1: flat.otherPayer_Tel_1,
                            otherPayer_address1_1: flat.otherPayer_address1_1,
                            otherPayer_address2_1: flat.otherPayer_address2_1,
                            otherPayer_City_1: flat.otherPayer_City_1,
                            otherPayer_State_1: flat.otherPayer_State_1,
                            otherPayer_Zipcode_1: flat.otherPayer_Zipcode_1,
                            otherPayerBegDate_2: flat.otherPayerBegDate_2,
                            otherPayerEndDate_2: flat.otherPayerEndDate_2,
                            otherPayerAdd_Date_2: flat.otherPayerAdd_Date_2,
                            otherPayerInfo_2: flat.otherPayerInfo_2,
                            otherPayerCoverage_2: flat.otherPayerCoverage_2,
                            otherPayerServiceType_2: flat.otherPayerServiceType_2,
                            otherPayerInsuranceType_2: flat.otherPayerInsuranceType_2,
                            otherPayerPlan_Desc_2: flat.otherPayerPlan_Desc_2,
                            otherPayerMessage1_2: flat.otherPayerMessage1_2,
                            otherPayerMessage2_2: flat.otherPayerMessage2_2,
                            otherPayerIdentifierCode_2: flat.otherPayerIdentifierCode_2,
                            otherPayer_Name_2: flat.otherPayer_Name_2,
                            otherPayer_RefID_2: flat.otherPayer_RefID_2,
                            otherPayer_Tel_2: flat.otherPayer_Tel_2,
                            otherPayer_address1_2: flat.otherPayer_address1_2,
                            otherPayer_address2_2: flat.otherPayer_address2_2,
                            otherPayer_City_2: flat.otherPayer_City_2,
                            otherPayer_State_2: flat.otherPayer_State_2,
                            otherPayer_Zipcode_2: flat.otherPayer_Zipcode_2,
                            otherBenefitBegDate_1: flat.otherBenefitBegDate_1,
                            otherBenefitEndDate_1: flat.otherBenefitEndDate_1,
                            otherBenefitAdd_Date_1: flat.otherBenefitAdd_Date_1,
                            otherBenefitInfo_1: flat.otherBenefitInfo_1,
                            otherBenefitCoverage_1: flat.otherBenefitCoverage_1,
                            otherBenefitServiceType_1: flat.otherBenefitServiceType_1,
                            otherBenefitInsuranceType_1: flat.otherBenefitInsuranceType_1,
                            otherBenefitPlan_Desc_1: flat.otherBenefitPlan_Desc_1,
                            otherBenefitMessage1_1: flat.otherBenefitMessage1_1,
                            otherBenefitTimePeriodQualifier_1: flat.otherBenefitTimePeriodQualifier_1,
                            otherBenefitAmount_1: flat.otherBenefitAmount_1,
                            otherBenefitQuantity_1: flat.otherBenefitQuantity_1,
                            otherBenefitNetworkFlag_1: flat.otherBenefitNetworkFlag_1,
                            otherBenefitBegDate_2: flat.otherBenefitBegDate_2,
                            otherBenefitEndDate_2: flat.otherBenefitEndDate_2,
                            otherBenefitAdd_Date_2: flat.otherBenefitAdd_Date_2,
                            otherBenefitInfo_2: flat.otherBenefitInfo_2,
                            otherBenefitCoverage_2: flat.otherBenefitCoverage_2,
                            otherBenefitServiceType_2: flat.otherBenefitServiceType_2,
                            otherBenefitInsuranceType_2: flat.otherBenefitInsuranceType_2,
                            otherBenefitPlan_Desc_2: flat.otherBenefitPlan_Desc_2,
                            otherBenefitMessage1_2: flat.otherBenefitMessage1_2,
                            otherBenefitTimePeriodQualifier_2: flat.otherBenefitTimePeriodQualifier_2,
                            otherBenefitAmount_2: flat.otherBenefitAmount_2,
                            otherBenefitQuantity_2: flat.otherBenefitQuantity_2,
                            otherBenefitNetworkFlag_2: flat.otherBenefitNetworkFlag_2,
                            otherBenefitBegDate_3: flat.otherBenefitBegDate_3,
                            otherBenefitEndDate_3: flat.otherBenefitEndDate_3,
                            otherBenefitAdd_Date_3: flat.otherBenefitAdd_Date_3,
                            otherBenefitInfo_3: flat.otherBenefitInfo_3,
                            otherBenefitCoverage_3: flat.otherBenefitCoverage_3,
                            otherBenefitServiceType_3: flat.otherBenefitServiceType_3,
                            otherBenefitInsuranceType_3: flat.otherBenefitInsuranceType_3,
                            otherBenefitPlan_Desc_3: flat.otherBenefitPlan_Desc_3,
                            otherBenefitMessage1_3: flat.otherBenefitMessage1_3,
                            otherBenefitTimePeriodQualifier_3: flat.otherBenefitTimePeriodQualifier_3,
                            otherBenefitAmount_3: flat.otherBenefitAmount_3,
                            otherBenefitQuantity_3: flat.otherBenefitQuantity_3,
                            otherBenefitNetworkFlag_3: flat.otherBenefitNetworkFlag_3,
                            transactionCnts: flat.transactionCnts,
                            validationNotes: flat.validationNotes,
                            validationScore: flat.validationScore,
                            //}}}
                        });
                    });
            });
    }

    self.commit = function(value, rowIndex, cb) {

        async.parallel({
            byId: function(cb) {
                context.Student.findOne({
                    _id: value.custDefined_In
                })
                    .exec(function(err, student) {
                        cb(null, student);
                    })
            },
            byStudentId: function(cb) {
                context.Student.findOne({
                    studentId: value.custDefined_In
                })
                    .exec(function(err, student) {
                        cb(err, student);
                    });
            }
        }, function(err, r) {
            var student = (r.byId || r.byStudentId);
            context.GDSTennCare.findOne({
                student: student._id
            })
                .populate('rangedData')
                .exec(function(err, gds) {
                    if (!!err) {
                        logger.error('gds tenn care import error: %s', err);
                        self.emit('commit error', {});
                    }
                    if (!gds) {
                        logger.debug('gds tenncare for ' + value.custDefined_In + ' is new. Creating');
                        gds = new context.GDSTennCare();
                        gds.rangedData = [];
                    }

                    gds.student = student._id;

                    var gdsRange = new context.ranged.GDSTennCare();

                    var startMoment = moment.tz(value.begDate_1, self.clientTimezone)
                            .startOf('day'),
                            endMoment = moment.tz(value.endDate_1, self.clientTimezone)
                            .endOf('day');

                    logger.silly('range: ' + startMoment.toString() + ' - ' + endMoment.toString())


                    gdsRange.startDate = startMoment.toDate();
                    gdsRange.endDate = endMoment.toDate();
                    gdsRange.rangeActive = true;

                    gdsRange.pageNum = value.pageNum;
                    gdsRange.dataSource = value.dataSource;
                    gdsRange.accountNbr_In = value.accountNbr_In;
                    gdsRange.recipID1_In = value.recipID1_In;
                    gdsRange.sSN_Inp = value.sSN_Inp;
                    gdsRange.lName1_In = value.lName1_In;
                    gdsRange.fName1_In = value.fName1_In;
                    gdsRange.mI1_In = value.mI1_In;
                    gdsRange.dOB_In = value.dOB_In;
                    gdsRange.gender_In = value.gender_In;
                    gdsRange.los_In = value.los_In;
                    gdsRange.errorMsg = value.errorMsg;
                    gdsRange.statusFlag = value.statusFlag;
                    gdsRange.mcaidFlag = value.mcaidFlag;
                    gdsRange.mCRA_Flag = value.mCRA_Flag;
                    gdsRange.mCRB_Flag = value.mCRB_Flag;
                    gdsRange.hMO_Flag = value.hMO_Flag;
                    gdsRange.gDS_Ref_No = value.gDS_Ref_No;
                    gdsRange.searchCode = value.searchCode;
                    gdsRange.split_Serial_Num = value.split_Serial_Num;
                    gdsRange.mcaidNum = value.mcaidNum;
                    gdsRange.sSN = value.sSN;
                    gdsRange.lname = value.lname;
                    gdsRange.fname = value.fname;
                    gdsRange.mname = value.mname;
                    gdsRange.dOB = value.dOB;
                    gdsRange.gender = value.gender;
                    gdsRange.hICNum = value.hICNum;
                    gdsRange.address1 = value.subtractress1;
                    gdsRange.address2 = value.subtractress2;
                    gdsRange.city = value.city;
                    gdsRange.state = value.state;
                    gdsRange.zip = value.zip;
                    gdsRange.mcaidDays_1 = value.mcaidDays_1;
                    gdsRange.begDate_1 = value.begDate_1;
                    gdsRange.endDate_1 = value.endDate_1;
                    gdsRange.status_1 = value.status_1;
                    gdsRange.coverage_1 = value.coverage_1;
                    gdsRange.serviceType_1 = value.serviceType_1;
                    gdsRange.insuranceType_1 = value.insuranceType_1;
                    gdsRange.plan_Desc_1 = value.plan_Desc_1;
                    gdsRange.mcaidDays_2 = value.mcaidDays_2;
                    gdsRange.begDate_2 = value.begDate_2;
                    gdsRange.endDate_2 = value.endDate_2;
                    gdsRange.status_2 = value.status_2;
                    gdsRange.coverage_2 = value.coverage_2;
                    gdsRange.serviceType_2 = value.serviceType_2;
                    gdsRange.insuranceType_2 = value.insuranceType_2;
                    gdsRange.plan_Desc_2 = value.plan_Desc_2;
                    gdsRange.mcaidDays_3 = value.mcaidDays_3;
                    gdsRange.begDate_3 = value.begDate_3;
                    gdsRange.endDate_3 = value.endDate_3;
                    gdsRange.status_3 = value.status_3;
                    gdsRange.coverage_3 = value.coverage_3;
                    gdsRange.serviceType_3 = value.serviceType_3;
                    gdsRange.insuranceType_3 = value.insuranceType_3;
                    gdsRange.plan_Desc_3 = value.plan_Desc_3;
                    gdsRange.mcaidDays_4 = value.mcaidDays_4;
                    gdsRange.begDate_4 = value.begDate_4;
                    gdsRange.endDate_4 = value.endDate_4;
                    gdsRange.status_4 = value.status_4;
                    gdsRange.coverage_4 = value.coverage_4;
                    gdsRange.serviceType_4 = value.serviceType_4;
                    gdsRange.insuranceType_4 = value.insuranceType_4;
                    gdsRange.plan_Desc_4 = value.plan_Desc_4;
                    gdsRange.mcaidDays_5 = value.mcaidDays_5;
                    gdsRange.begDate_5 = value.begDate_5;
                    gdsRange.endDate_5 = value.endDate_5;
                    gdsRange.status_5 = value.status_5;
                    gdsRange.coverage_5 = value.coverage_5;
                    gdsRange.serviceType_5 = value.serviceType_5;
                    gdsRange.insuranceType_5 = value.insuranceType_5;
                    gdsRange.plan_Desc_5 = value.plan_Desc_5;
                    gdsRange.mcaidDays_6 = value.mcaidDays_6;
                    gdsRange.begDate_6 = value.begDate_6;
                    gdsRange.endDate_6 = value.endDate_6;
                    gdsRange.status_6 = value.status_6;
                    gdsRange.coverage_6 = value.coverage_6;
                    gdsRange.serviceType_6 = value.serviceType_6;
                    gdsRange.insuranceType_6 = value.insuranceType_6;
                    gdsRange.plan_Desc_6 = value.plan_Desc_6;
                    gdsRange.partADays_1 = value.partADays_1;
                    gdsRange.partABeg_1 = value.partABeg_1;
                    gdsRange.partAEnd_1 = value.partAEnd_1;
                    gdsRange.partAStatus_1 = value.partAStatus_1;
                    gdsRange.partACovLevel_1 = value.partACovLevel_1;
                    gdsRange.partADays_2 = value.partADays_2;
                    gdsRange.partABeg_2 = value.partABeg_2;
                    gdsRange.partAEnd_2 = value.partAEnd_2;
                    gdsRange.partAStatus_2 = value.partAStatus_2;
                    gdsRange.partACovLevel_2 = value.partACovLevel_2;
                    gdsRange.partBDays_1 = value.partBDays_1;
                    gdsRange.partBBeg_1 = value.partBBeg_1;
                    gdsRange.partBEnd_1 = value.partBEnd_1;
                    gdsRange.partBStatus_1 = value.partBStatus_1;
                    gdsRange.partBCovLevel_1 = value.partBCovLevel_1;
                    gdsRange.partBDays_2 = value.partBDays_2;
                    gdsRange.partBBeg_2 = value.partBBeg_2;
                    gdsRange.partBEnd_2 = value.partBEnd_2;
                    gdsRange.partBStatus_2 = value.partBStatus_2;
                    gdsRange.partBCovLevel_2 = value.partBCovLevel_2;
                    gdsRange.mcaidDays = value.mcaidDays;
                    gdsRange.partADays = value.partADays;
                    gdsRange.mcaid_PartADays = value.mcaid_PartADays;
                    gdsRange.partBDays = value.partBDays;
                    gdsRange.mcaid_PartBDays = value.mcaid_PartBDays;
                    gdsRange.partA_PartBDays = value.partA_PartBDays;
                    gdsRange.mcaid_PartA_PartBDays = value.mcaid_PartA_PartBDays;
                    gdsRange.otherPayerBegDate_1 = value.otherPayerBegDate_1;
                    gdsRange.otherPayerEndDate_1 = value.otherPayerEndDate_1;
                    gdsRange.otherPayerAdd_Date_1 = value.otherPayerAdd_Date_1;
                    gdsRange.otherPayerInfo_1 = value.otherPayerInfo_1;
                    gdsRange.otherPayerCoverage_1 = value.otherPayerCoverage_1;
                    gdsRange.otherPayerServiceType_1 = value.otherPayerServiceType_1;
                    gdsRange.otherPayerInsuranceType_1 = value.otherPayerInsuranceType_1;
                    gdsRange.otherPayerPlan_Desc_1 = value.otherPayerPlan_Desc_1;
                    gdsRange.otherPayerMessage1_1 = value.otherPayerMessage1_1;
                    gdsRange.otherPayerMessage2_1 = value.otherPayerMessage2_1;
                    gdsRange.otherPayerIdentifierCode_1 = value.otherPayerIdentifierCode_1;
                    gdsRange.otherPayer_Name_1 = value.otherPayer_Name_1;
                    gdsRange.otherPayer_RefID_1 = value.otherPayer_RefID_1;
                    gdsRange.otherPayer_Tel_1 = value.otherPayer_Tel_1;
                    gdsRange.otherPayer_address1_1 = value.otherPayer_address1_1;
                    gdsRange.otherPayer_address2_1 = value.otherPayer_address2_1;
                    gdsRange.otherPayer_City_1 = value.otherPayer_City_1;
                    gdsRange.otherPayer_State_1 = value.otherPayer_State_1;
                    gdsRange.otherPayer_Zipcode_1 = value.otherPayer_Zipcode_1;
                    gdsRange.otherPayerBegDate_2 = value.otherPayerBegDate_2;
                    gdsRange.otherPayerEndDate_2 = value.otherPayerEndDate_2;
                    gdsRange.otherPayerAdd_Date_2 = value.otherPayerAdd_Date_2;
                    gdsRange.otherPayerInfo_2 = value.otherPayerInfo_2;
                    gdsRange.otherPayerCoverage_2 = value.otherPayerCoverage_2;
                    gdsRange.otherPayerServiceType_2 = value.otherPayerServiceType_2;
                    gdsRange.otherPayerInsuranceType_2 = value.otherPayerInsuranceType_2;
                    gdsRange.otherPayerPlan_Desc_2 = value.otherPayerPlan_Desc_2;
                    gdsRange.otherPayerMessage1_2 = value.otherPayerMessage1_2;
                    gdsRange.otherPayerMessage2_2 = value.otherPayerMessage2_2;
                    gdsRange.otherPayerIdentifierCode_2 = value.otherPayerIdentifierCode_2;
                    gdsRange.otherPayer_Name_2 = value.otherPayer_Name_2;
                    gdsRange.otherPayer_RefID_2 = value.otherPayer_RefID_2;
                    gdsRange.otherPayer_Tel_2 = value.otherPayer_Tel_2;
                    gdsRange.otherPayer_address1_2 = value.otherPayer_address1_2;
                    gdsRange.otherPayer_address2_2 = value.otherPayer_address2_2;
                    gdsRange.otherPayer_City_2 = value.otherPayer_City_2;
                    gdsRange.otherPayer_State_2 = value.otherPayer_State_2;
                    gdsRange.otherPayer_Zipcode_2 = value.otherPayer_Zipcode_2;
                    gdsRange.otherBenefitBegDate_1 = value.otherBenefitBegDate_1;
                    gdsRange.otherBenefitEndDate_1 = value.otherBenefitEndDate_1;
                    gdsRange.otherBenefitAdd_Date_1 = value.otherBenefitAdd_Date_1;
                    gdsRange.otherBenefitInfo_1 = value.otherBenefitInfo_1;
                    gdsRange.otherBenefitCoverage_1 = value.otherBenefitCoverage_1;
                    gdsRange.otherBenefitServiceType_1 = value.otherBenefitServiceType_1;
                    gdsRange.otherBenefitInsuranceType_1 = value.otherBenefitInsuranceType_1;
                    gdsRange.otherBenefitPlan_Desc_1 = value.otherBenefitPlan_Desc_1;
                    gdsRange.otherBenefitMessage1_1 = value.otherBenefitMessage1_1;
                    gdsRange.otherBenefitTimePeriodQualifier_1 = value.otherBenefitTimePeriodQualifier_1;
                    gdsRange.otherBenefitAmount_1 = value.otherBenefitAmount_1;
                    gdsRange.otherBenefitQuantity_1 = value.otherBenefitQuantity_1;
                    gdsRange.otherBenefitNetworkFlag_1 = value.otherBenefitNetworkFlag_1;
                    gdsRange.otherBenefitBegDate_2 = value.otherBenefitBegDate_2;
                    gdsRange.otherBenefitEndDate_2 = value.otherBenefitEndDate_2;
                    gdsRange.otherBenefitAdd_Date_2 = value.otherBenefitAdd_Date_2;
                    gdsRange.otherBenefitInfo_2 = value.otherBenefitInfo_2;
                    gdsRange.otherBenefitCoverage_2 = value.otherBenefitCoverage_2;
                    gdsRange.otherBenefitServiceType_2 = value.otherBenefitServiceType_2;
                    gdsRange.otherBenefitInsuranceType_2 = value.otherBenefitInsuranceType_2;
                    gdsRange.otherBenefitPlan_Desc_2 = value.otherBenefitPlan_Desc_2;
                    gdsRange.otherBenefitMessage1_2 = value.otherBenefitMessage1_2;
                    gdsRange.otherBenefitTimePeriodQualifier_2 = value.otherBenefitTimePeriodQualifier_2;
                    gdsRange.otherBenefitAmount_2 = value.otherBenefitAmount_2;
                    gdsRange.otherBenefitQuantity_2 = value.otherBenefitQuantity_2;
                    gdsRange.otherBenefitNetworkFlag_2 = value.otherBenefitNetworkFlag_2;
                    gdsRange.otherBenefitBegDate_3 = value.otherBenefitBegDate_3;
                    gdsRange.otherBenefitEndDate_3 = value.otherBenefitEndDate_3;
                    gdsRange.otherBenefitAdd_Date_3 = value.otherBenefitAdd_Date_3;
                    gdsRange.otherBenefitInfo_3 = value.otherBenefitInfo_3;
                    gdsRange.otherBenefitCoverage_3 = value.otherBenefitCoverage_3;
                    gdsRange.otherBenefitServiceType_3 = value.otherBenefitServiceType_3;
                    gdsRange.otherBenefitInsuranceType_3 = value.otherBenefitInsuranceType_3;
                    gdsRange.otherBenefitPlan_Desc_3 = value.otherBenefitPlan_Desc_3;
                    gdsRange.otherBenefitMessage1_3 = value.otherBenefitMessage1_3;
                    gdsRange.otherBenefitTimePeriodQualifier_3 = value.otherBenefitTimePeriodQualifier_3;
                    gdsRange.otherBenefitAmount_3 = value.otherBenefitAmount_3;
                    gdsRange.otherBenefitQuantity_3 = value.otherBenefitQuantity_3;
                    gdsRange.otherBenefitNetworkFlag_3 = value.otherBenefitNetworkFlag_3;
                    gdsRange.transactionCnts = value.transactionCnts;
                    gdsRange.validationNotes = value.validationNotes;
                    gdsRange.validationScore = value.validationScore;

                    dataUtil.dateSplice(gds.rangedData, gdsRange, function(err, rangesToSave) {
                        if (!!err) {
                            logger.error(err);
                        }
                        dataUtil.dateSpliceSave(self.actor, gds, rangesToSave, 'rangedData', function(err, savedGDS) {
                            if (!!err) {
                                logger.error('error importing gds tenncar: ' + util.inspect(err));
                            }
                            cb(err);
                        });
                    });

                });
        });
    };
    self.sheet.rowValidators = [{
        validator: function(value, cb) {
            var start = moment(value.beg_In),
                end = moment(value.end_In);

            if (!end.isValid() || !start.isValid()) {
                cb(null, false);
            } else if (end.isBefore(start)) {
                cb(null, false);
            } else {
                cb(null, true);
            }
        },
        errorMessage: '[error messages for gdsMedicaid needed]'
    }];
    return self;
};
GdsMedicaidImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = GdsMedicaidImport;
