var logger = require('winston'),
    mongoose = require('mongoose'),
    context = require('../models/context'),
    dataUtil = require('../models/dataUtil'),
    _ = require('lodash'),
    moment = require('moment-timezone'),
    async = require('async'),
    util = require('util');

var McoImport = function(actor, clientTimezone) {
    var self = this;
    self.actor = actor || "[import]";
    self.clientTimezone = clientTimezone;

    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'MCO Import',
        className: 'mco',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'Name',
            propertyName: 'name',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for Name]'
            }]
        }, {
            name: 'Short Name',
            propertyName: 'shortName',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Short Name needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Short Name]'
            }]
        }, {
            name: 'Health Plan ID',
            propertyName: 'healthPlanId',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Health Plan ID needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Health Plan ID]'
            }]
        }, {
            name: 'Start Date',
            propertyName: 'startDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Start Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))$/,
                errorMessage: '[rules needed for Start Date]'
            }]
        }, {
            name: 'End Date',
            propertyName: 'endDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for End Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))$/,
                errorMessage: '[rules needed for End Date]'
            }]
        }, {
            name: 'DistCode',
            propertyName: 'districtCode',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for DistCode needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\w+/,
                errorMessage: '[rules needed for DistCode]'
            }]
        }, {
            name: 'Address',
            propertyName: 'address1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for address needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\w*/,
                errorMessage: '[rules needed for address]'
            }]
        }, {
            name: 'Address 2',
            propertyName: 'address2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for address 2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\w*/,
                errorMessage: '[rules needed for address 2]'
            }]
        }, {
            name: 'City',
            propertyName: 'city',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for city needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\w*/,
                errorMessage: '[rules needed for city]'
            }]
        }, {
            name: 'State',
            propertyName: 'state',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for state needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\w*/,
                errorMessage: '[rules needed for state]'
            }]
        }, {
            name: 'ZIP',
            propertyName: 'zip',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for zip needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\w*/,
                errorMessage: '[rules needed for zip]'
            }]
        }, ]
    };

    self.diff = function(value, index, cb) {
        context.District.findOne({
            districtCode: value.districtCode
        })
            .exec(function(err, district) {

                context.MCO.findOne({
                    name: value.name,
                    district: district.id
                })
                    .populate('rangedData')
                    .lean()
                    .exec(function(err, mco) {
                        if (!!err) {
                            self.emit('diff error', {});
                            cb(err, false);
                        } else if (!mco) {
                            cb(null, null);
                        } else {
                            var startMoment = moment.tz(value.startDate, self.clientTimezone)
                                .startOf('day'),
                                endMoment = moment.tz(value.endDate, self.clientTimezone)
                                .endOf('day');

                            logger.silly('diff range: ' + startMoment.toString() + ' - ' + endMoment.toString());

                            dataUtil.getByDateRange(
                                mco,
                                'rangedData',
                                startMoment,
                                endMoment,
                                function(err, flat) {
                                    if (!flat) {
                                        cb(err, null);
                                    } else {
                                        cb(err, {
                                            name: flat.name,
                                            shortName: flat.shortName,
                                            healthPlanId: flat.healthPlanId,
                                            startDate: flat.startDate,
                                            endDate: flat.endDate,
                                            districtCode: district.districtCode,
                                            address1: flat.address1,
                                            address2: flat.address2,
                                            city: flat.city,
                                            state: flat.state,
                                            zip: flat.zip
                                        })
                                    }
                                });
                        }
                    });
            });
    };

    self.commit = function(value, rowIndex, cb) {

        context.District.findOne({
            districtCode: value.districtCode
        })
            .exec(function(err, district) {

                context.MCO.findOne({
                    name: value.name,
                    district: district.id
                })
                    .populate('rangedData')
                    .exec(function(err, mco) {
                        if (!!err) {
                            logger.error('uh... error: %s', err);
                            self.emit('commit error', {});
                            cb(err, false);
                        }
                        if (!mco) {
                            logger.debug('mco ' + value.name + ' is new.  Creating.');
                            mco = new context.MCO();
                            mco.rangedData = [];
                        } else {
                            logger.debug('mco ' + value.name + ' is not new.  Modifying.');
                        }

                        mco.name = value.name;
                        mco.district = district.id;

                        var mcoRange = new context.ranged.MCO();

                        mcoRange.shortName = value.shortName;
                        mcoRange.healthPlanId = value.healthPlanId;
                        mcoRange.address1 = value.address1;
                        mcoRange.address2 = value.address2;
                        mcoRange.city = value.city;
                        mcoRange.state = value.state;
                        mcoRange.zip = value.zip;
                        
                        var startMoment = moment.tz(value.startDate, self.clientTimezone)
                            .startOf('day'),
                            endMoment = moment.tz(value.endDate, self.clientTimezone)
                            .endOf('day');

                        logger.silly('range: ' + startMoment.toString() + ' - ' + endMoment.toString())


                        mcoRange.startDate = startMoment.toDate();
                        mcoRange.endDate = endMoment.toDate();

                        dataUtil.dateSplice(mco.rangedData,
                            mcoRange,
                            function(err, rangesToSave) {
                                dataUtil.dateSpliceSave(self.actor, mco,
                                    rangesToSave,
                                    'rangedData',
                                    function(err, savedMCO) {
                                        if (!!err) {
                                            logger.error('error importing diagnostic code: %s', util.inspect(err));
                                            logger.debug('error rowdata info: %s', util.inspect(value));
                                            cb(err);
                                        } else {
                                            cb(null);
                                        }
                                    });
                            });
                    });
            });
    };
    self.sheet.rowValidators = [{
        validator: function(value, callback) {
            var start = new Date(value.startDate);
            var end = new Date(value.endDate);
            if (!!start.getTime() && !!end.getTime()) {
                //both are valid
                callback(null, start < end && start <= Date.now() && end >= Date.now());
            } else if (!!start.getTime() && !end.getTime()) {
                //start is valid, end is blank
                callback(null, start <= Date.now());
            } else if (!start.getTime() && !!end.getTime()) {
                //end is valid, start is blank
                callback(null, end >= Date.now());
            } else if (!start.getTime() && !end.getTime()) {
                //item is always date-valid
                callback(null, true);
            } else {
                //this should not happen unless there is a rip in the space-time continuum
                logger.warn("dates are both valid and not valid in mco import");
                callback(null, true);
            }
        },
        errorMessage: '[error messages for mco needed]'
    }, {
        validator: function(value, callback) {
            //validate district exists
            context.District.findOne({
                districtCode: value.districtCode
            }, function(err, district) {
                if (!!err) {
                    logger.error('uh... error: %s', err);
                    callback(err, false);
                } else if (!district) {
                    logger.warn('district did not exist for mco');
                    callback(null, false);
                } else {
                    callback(null, true);
                }
            });
        },
        errorMessage: '[error message for mco needed - district not found]'
    }];
    return self;
};
McoImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = McoImport;
