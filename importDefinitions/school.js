var logger = require('winston'),
    mongoose = require('mongoose'),
    context = require('../models/context'),
    dataUtil = require('../models/dataUtil'),
    async = require('async'),
    moment = require('moment-timezone'),
    _ = require('underscore'),
    util = require('util');


var SchoolImport = function(actor, clientTimezone) {
    var self = this;
    self.actor = actor || "[import]";
    self.clientTimezone = clientTimezone;

    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'School Import',
        className: 'school',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'School',
            propertyName: 'school',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for School needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.+/,
                errorMessage: '[rules needed for School]'
            }]
        }, {
            name: 'Address Line 1',
            propertyName: 'addressLine1',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Address Line 1 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\w+/,
                errorMessage: '[rules needed for Address Line 1]'
            }]
        }, {
            name: 'Address Line 2',
            propertyName: 'addressLine2',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Address Line 2 needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Address Line 2]'
            }]
        }, {
            name: 'City',
            propertyName: 'city',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for City needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\w+/,
                errorMessage: '[rules needed for City]'
            }]
        }, {
            name: 'St',
            propertyName: 'state',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for St needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\w+/,
                errorMessage: '[rules needed for St]'
            }]
        }, {
            name: 'Zip',
            propertyName: 'zip',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Zip needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\d+/,
                errorMessage: '[rules needed for Zip]'
            }]
        }, {
            name: 'School Code',
            propertyName: 'schoolCode',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for School Code needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\w+/,
                errorMessage: '[rules needed for School Code]'
            }]
        }, {
            name: 'School Number',
            propertyName: 'schoolNumber',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for School Number needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\w+/,
                errorMessage: '[rules needed for School Number]'
            }]
        }, {
            name: 'DistCode',
            propertyName: 'districtCode',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for DistCode needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\w+/,
                errorMessage: '[rules needed for DistCode]'
            }]
        }, {
            name: 'Start Date',
            propertyName: 'startDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Start Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))$/,
                errorMessage: '[rules needed for Start Date]'
            }]
        }, {
            name: 'End Date',
            propertyName: 'endDate',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for End Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))$/,
                errorMessage: '[rules needed for End Date]'
            }]
        }, ]
    };

    self.diff = function(value, index, cb) {
        context.District.findOne({
            districtCode: value.districtCode
        })
            .exec(function(err, district) {
                context.School.findOne({
                    schoolCode: value.schoolCode,
                    district: district.id
                })
                    .populate('rangedData')
                    .lean()
                    .exec(function(err, school) {
                        if (!!err) {
                            self.emit('diff error', {});
                            cb(err, false);
                        } else if (!school) {
                            cb(null, null);
                        } else {

                            var startMoment = moment.tz(value.startDate, self.clientTimezone)
                                .startOf('day'),
                                endMoment = moment.tz(value.endDate, self.clientTimezone)
                                .endOf('day');

                            logger.silly('diff range: ' + startMoment.toString() + ' - ' + endMoment.toString());

                            dataUtil.getByDateRange(
                                school,
                                'rangedData',
                                startMoment,
                                endMoment,
                                function(err, flat) {
                                    if (!flat) {
                                        cb(err, null);
                                    } else {
                                        cb(err, {
                                            school: flat.name,
                                            addressLine1: flat.address1,
                                            addressLine2: flat.address2,
                                            city: flat.city,
                                            state: flat.state,
                                            zip: flat.zip,
                                            schoolCode: flat.schoolCode,
                                            schoolNumber: flat.schoolNumber,
                                            startDate: flat.startDate,
                                            endDate: flat.endDate,
                                            districtCode: district.districtCode,
                                        })
                                    }
                                });
                        }
                    });
            });
    };

    self.commit = function(value, rowIndex, cb) {
        context.District.findOne({
            districtCode: value.districtCode
        }, function(err, district) {
            context.School.findOne({
                schoolCode: value.schoolCode,
                district: district.id
            })
                .populate('rangedData')
                .exec(function(err, school) {

                    if (!!err) {
                        logger.error('uh... error: %s', err);
                        cb(err, false);
                        return;
                    }

                    if (!school) {
                        school = new context.School();
                        school.rangedData = [];
                    } else {}
                    school.district = district.id;
                    school.schoolNumber = value.schoolNumber;
                    school.schoolCode = value.schoolCode;

                    var schoolRange = new context.ranged.School();

                    schoolRange.name = value.school;
                    schoolRange.address1 = value.addressLine1;
                    schoolRange.address2 = value.addressLine2;
                    schoolRange.city = value.city;
                    schoolRange.state = value.state;
                    schoolRange.zip = value.zip;

                    var startMoment = moment.tz(value.startDate, self.clientTimezone)
                        .startOf('day'),
                        endMoment = moment.tz(value.endDate, self.clientTimezone)
                        .endOf('day');

                    logger.silly('range: ' + startMoment.toString() + ' - ' + endMoment.toString())


                    schoolRange.startDate = startMoment.toDate();
                    schoolRange.endDate = endMoment.toDate();

                    dataUtil.dateSplice(school.rangedData, schoolRange, function(err, rangesToSave) {
                        if (!!err) {
                            logger.error(err);
                        }
                        dataUtil.dateSpliceSave(self.actor, school,
                            rangesToSave,
                            'rangedData',
                            function(err, savedSchool) {
                                if (!!err) {
                                    logger.error('error importing school: %s', util.inspect(err));
                                    logger.debug('error rowdata info: %s', util.inspect(value));
                                    logger.debug('error school: %s', util.inspect(school));
                                    cb(err);
                                } else {
                                    cb(null);
                                }
                            });
                    });
                });
        });
    };
    self.sheet.rowValidators = [{
        validator: function(value, callback) {
            var start = new Date(value.startDate);
            var end = new Date(value.endDate);
            if (!!start.getTime() && !!end.getTime()) {
                //both are valid
                callback(null, start < end && start <= Date.now() && end >= Date.now());
            } else if (!!start.getTime() && !end.getTime()) {
                //start is valid, end is blank
                callback(null, start <= Date.now());
            } else if (!start.getTime() && !!end.getTime()) {
                //end is valid, start is blank
                callback(null, end >= Date.now());
            } else if (!start.getTime() && !end.getTime()) {
                //item is always date-valid
                callback(null, true);
            } else {
                //this should not happen unless there is a rip in the space-time continuum
                logger.warn("dates are both valid and not valid in school import");
                callback(null, true);
            }
        },
        errorMessage: '[error messages for school needed]'
    }, {
        validator: function(value, callback) {
            //validate district exists
            context.District.findOne({
                districtCode: value.districtCode
            }, function(err, district) {
                if (!!err) {
                    logger.error('uh... error: %s', err);
                    callback(err, false);
                } else if (!district) {
                    logger.warn('district did not exist for school');
                    callback(null, false);
                } else {
                    callback(null, true);
                }
            });
        },
        errorMessage: '[error message for school needed - district not found]'
    }];
    return self;
};
SchoolImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = SchoolImport;
