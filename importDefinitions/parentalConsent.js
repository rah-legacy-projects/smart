var logger = require('winston'),
    moment = require('moment-timezone'),
    async = require('async'),
    _ = require('lodash'),
    context = require('../models/context');

var ParentalConsentImport = function(actor, clientTimezone) {
    var self = this;
    self.actor = actor || "[import]";
    self.clientTimezone = clientTimezone;

    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'Parental Consent Import',
        className: 'parentalConsent',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'To Share IEP',
            propertyName: 'toShareIEP',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for To Share IEP needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for To Share IEP]'
            }]
        }, {
            name: 'To Share IEP Denied',
            propertyName: 'toShareIEPDenied',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for To Share IEP Denied needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for To Share IEP Denied]'
            }]
        }, {
            name: 'To Bill Medicaid',
            propertyName: 'toBillMedicaid',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for To Bill Medicaid needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for To Bill Medicaid]'
            }]
        }, {
            name: 'To Bill Medicaid Denied',
            propertyName: 'toBillMedicaidDenied',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for To Bill Medicaid Denied needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for To Bill Medicaid Denied]'
            }]
        }, {
            name: 'To Treat',
            propertyName: 'toTreat',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for To Treat needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for To Treat]'
            }]
        }, {
            name: 'To Treat Denied',
            propertyName: 'toTreatDenied',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for To Treat Denied needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for To Treat Denied]'
            }]
        }, {
            name: 'Student ID',
            propertyName: 'studentId',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Student ID needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for Student ID]'
            }]
        }, {
            name: 'District Code',
            propertyName: 'districtCode',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for District Code needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /.*/,
                errorMessage: '[rules needed for District Code]'
            }]
        }, ]
    };
    self.commit = function(value, rowIndex, cb) {
        var tasks = [

            function(cb) {
                context.District.findOne({
                    districtCode: value.districtCode
                })
                    .lean()
                    .exec(function(err, district) {
                        cb(err, district);
                    });
            },
            function(district, cb) {
                context.Student.find({
                    studentId: value.studentId
                })
                    .populate('parentalConsents')
                    .populate('rangedData')
                    .exec(function(err, students) {
                        async.waterfall([

                            function(cb) {
                                context.School.populate(students, {
                                    path: 'rangedData.school',
                                    model: 'School'
                                }, cb);
                            },
                            function(students, cb) {
                                context.School.populate(students, {
                                    path: 'rangedData.school.district',
                                    model: 'District'
                                }, cb);
                            }
                        ], function(err, students) {
                            var student = _.find(students, function(student) {
                                return _.any(student.rangedData, function(range) {
                                    return range.school.district.id.toString() == district._id;
                                });
                            });

                            cb(err, student);
                        });

                    });
            }
        ];

        async.waterfall(tasks, function(err, student) {
            var record = new context.ParentalConsent();

            var toShareIEP = moment.tz(value.toShareIEP, self.clientTimezone)
                .startOf('day'),
                toTreat = moment.tz(value.toTreat, self.clientTimezone).startOf('day'),
                toBill = moment.tz(value.toBill, self.clientTimezone).startOf('day');


            record.toShareIEP = (value.toShareIEP == '' || value.toShareIEP == null) ? null : toShareIEP.toDate();
            record.toShareIEPDenied = /yes/i.test(value.toShareIEPDenied);
            record.toBillMedicaid = (value.toBillMedicaid == '' || value.toBillMedicaid == null) ? null : toBill.toDate();
            record.toBillMedicaidDenied = /yes/i.test(value.toBillMedicaidDenied);
            record.toTreat = ((value.toTreat || '') == '') ? null : toTreat.toDate();
            record.toTreatDenied = /yes/i.test(value.toTreatDenied);
            record.districtCode = value.districtCode;
            if (!student.parentalConsents) {
                student.parentalConsents = [];
            }


            record.save(function(err, parentalConsent) {
                if (!!err) {
                    logger.error(err);
                }
                student.parentalConsents.push(parentalConsent);
                student.save(function(err) {
                    if (!!err) logger.error(err);
                    cb(err, true);
                });
            });
        });
    };
    self.sheet.rowValidators = [{
        validator: function(value, cb) {
            logger.warn('validator for parentalConsent needed');
            cb(null, true);
        },
        errorMessage: '[error messages for parentalConsent needed]'
    }];
    return self;
};
ParentalConsentImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = ParentalConsentImport;
