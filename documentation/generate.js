var marked = require('marked'),
    ncp = require('ncp'),
    glob = require('glob'),
    util = require('util'),
    path = require('path'),
    fs = require('fs'),
    _ = require('lodash');

//copy everything md->output
ncp('./md', './output', function(err) {
    glob('./output/*.md', function(err, files) {
        _.each(files, function(file) {
            //read in the md file
            fs.readFile(file, 'utf8', function(err, data) {
                //render
                marked(data, function(err, renderedData) {
                    //get file name and path
                    var dirname = path.dirname(file),
                        filename = path.basename(file, '.md');
                    var htmlfile = path.join(dirname, filename + '.html');
                    //output html
                    fs.writeFile(htmlfile, renderedData, function(err) {
                        console.log('wrote ' + htmlfile);

                        //unlink md file
                        fs.unlinkSync(file);
                    });
                });
            });
        });
    });
});
