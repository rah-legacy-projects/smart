#Ranging

##Problem Description and a Brief History
Data in SMART has date-wise transience.  For example, District A is changing it's name to District B on October 29th, so the data would be reimported to reflect that change.  

One solution would be to simply create a new record.  However, when new record is created, it would need to have all of the references to the original district records.  For example. both District A and the newly-minted District B would have references to all of the same schools.  This solution quickly escalates in complexity as other items that are district-dependent would also need to be copied.  The problem is compounded further if overlapping, engulfing or contained date ranges are imported.

The district example is somewhat contrived, but demonstrative of the overarching issue: There are substantial referential integrity problems if one record is created per validity time span.

##Moving Transient Data
The silver lining in the SMART data is that there are a few pieces per datatype that are not transient.  For example, in the district, the district code cannot change.  A new district code would constitute a new district entirely.  Transient data can be placed in a child collection broken down by date, leaving the persistent data - the data which referential integrity is based upon - alone.  This solves the maintenance problem with referential integrity, but side-steps another problem: overlapping date ranges.  If the transient data overlaps, which one is actually valid?

##Enter Date Splicing
At import, the child data is loaded with the parent record.  The imported record range is spliced onto the existing child data so date continuity is maintained and there are no overlaps.  There are four types of overlap:
1. *Start Overlap.*  The imported record has an overlap on the start side.  In this case, the existing overlapping record is marked as inactive.  Next, a new record is made from the existing record's start date to the imported record's start date.  Finally, the imported record is created.
2. *End Overlap.*  Similar to the start overlap but on the end side of the imported record, the overlapping record is marked as inactive.  Then, a new record from the imported record's end to the existing record's end is created.  Finally, the imported record is created.
3. *Engulf.*  The imported record completely envelops the existing record.  (Note that the start and end dates can touch, e.g. if the transient data changes and needs to be saved.)  The enveloped record is marked as inactive and the imported record is created.
4. *Holepunch.*  The imported record resides completely inside an existing record.  The existing record is marked as inactive.  A new record based on the existing record is made from the existing record start to the imported record start.  Another record based on the existing record is created from the imported record end to the existing record end.  Finally, the imported record is created.

There are more complicated scenarios that can happen.  Any combination of the first three overlaps is possible.  The rules still apply and scale to meet those concerns.

##Tidying Up
Referential integrity has been addressed, as well as overlapping date ranges.  Now we have one valid record per date per ranged datatype, which means the last piece is flattening the data out for consumption.  This is done via utilities, and presents the data as though it were flat.

