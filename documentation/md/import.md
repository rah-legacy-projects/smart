# Imports
The base data for SMART can come from a series of data imports.  The data format is expected to be XLSX.  Currently, CSV and XLS (Excel 97-2003) are not supported.

##Import Definition
The selected import has a corresponding import definition.  This definition is responsible for supplying the validation rules, help information and identity rules.

##Upload
The selected file is uploaded using socket transport with vanilla POST fallbacks.  The socket transport was used as a logical extension of the socket event system when processing and committing records.  This also allows for easy client- and server-side progress tracking.  The file is saved to an upload directory and assigned a processing token.  The location is saved with the token as a key in semi-persistent storage (we are currently using [Redis](www.redis.io)).  The token is then sent back to the client.

##Import Processing
Once the upload is complete, the client currently automatically invokes import processing.  The client issues a processing request using the token from the Upload step.  The import processor will load the uploaded XLSX file, convert it into a machine-digestible format per the rules in the import definition, and start running through the processing rules.  For every row, independent of success, a progress event is raised and broadcast to the client.  These events are manifested in the progress bar.  For the rows that have validation issues, error events are raised and broadcast to the client for display in the messages area.  When the import process is finished, the validation data is saved to the same token-based semi-persistent storage.

##Report
This is optional.  The user can pull down an XLSX that will show the records that passed validation, the records that failed validation and why, as well as a help tab that provides further guidance over what the criteria are for valid data.  The help tab is derived from the import definition for the upload.

##Committing
This is optional.  The user can commit the valid records from [Import Processing](#import-processing).  For each record committed, a progress event is emitted to the client and manifested as the progress bar.  Each import definition has rules for how records are identified, created, and/or updated, as specfied in [Import Detail](./import-detail.html).

