#Billing Logic

##Determining Billable Sessions
1. Retrieve schedules where:
    1. The schedule time is in the supplied range,
    1. Services that have diagnostic codes that have been modified in the supplied range,
    1. Students who have GDS data modified in the supplied range,
    1. Students who have MCO approvals modified in the supplied range,
    1. Students who have parental consents modified in the supplied range, 
    1. Students who have prescriptions modified in the supplied range,
    1. Sessions that have procedure codes that have been modified in the supplied range,
    1. Sessions that have been modified in the supplied range.
1. Filter schedules with no sessions.
1. Filter sessions with no selected procedure code.
1. Fix diagnostic codes.  If the student diagnostic code has changed and has not been viewed, use the service's diagnostic code.  This was done so later, services still show with the correct diagnostic code as it was billed.  Note that this should be a no-op now as the diagnostic codes are written when the session is finalized.
1. Determine what sessions are longer than the IEP-prescribed sessions and add the IEP-sized units to the possible billable record.
1. Filter sessions that are _not_ finalized.
1. Filter sessions that are billed.
1. Filter attendees that do not have GDS data.
1. Filter attendees that do not have an SSN from GDS.
1. Filter students with no current Medicaid eligibility per GDS.
1. Filter students who are marked as ineligble for Medicaid in the data sent back from GDS.
1. Filter sessions that have no valid procedure code value for the selected procedure code.
1. Filter sessions where the therapist does not have a valid NPI number.
1. Filter sessions where the therapist does not have a valid social security number.
1. Filter sessions where the therapist does not have a contract with the student MCO per the GDS data.
1. Filter absentees.  The other students in the session are left alone.
1. Filter students without a valid diagnostic code.
1. Filter students with no parental consent.
1. Filter students with no explicit to treat date.
1. Filter students with explicit treatment denial.
1. Filter students with no explicit to bill date.
1. Filter students with explicit billing denial.
1. Filter students with no current MCO approvals.
1. Filter students whose MCO does not have an address.
1. Filter students whose MCO does not have a Health Plan ID.
1. Filter students with explicit MCO treatment disapprovals.
1. Filter students with no current prescriptions.
1. Filter students with no current prescription for the service provided.
1. Filter sessions that extend beyond the IEP-recorded amount for the period, including billed sessions.
1. Filter sessions that extend beyond the MCO-approved amount for the period, including billed sessions, if the MCO-approved amount is recorded.

Finally, all of the valid, billable sessions are available to create the billing file.

##Create Billing File
1. Create the interchange control header.
1. Create the functional group header.
1. Create the transaction set header.
1. Create the beginning of the hierarchical transaction.
1. Add the submitter name.
1. Add the submitter EDI contact name.
1. Add the receiver name.
1. Start tracking the HL record number, starting with 1.
1. For each billable record grouped by provider,
    1. Create a billing provider HL record.
    1. Increment HL count.
    1. Add the billing provider name.
    1. Add the billing provider address.  Currently uses the student's district.
    1. Add the billing provider city, state, ZIP.  Currently uses the student's district.
    1. Add the billing provider's tax identification.
    1. Add the pay-to address.  Currently uses the student's district.
    1. Add the pay-to name.  Currently uses the student's district.
    1. Add the pay-to address.  Currently uses the student's district.
    1. Add the pay-to city, state, ZIP.  Currently uses the student's district.
    1. For each billable record grouped by student in the records grouped by provider,
        1. Group the grouped by student records by diagnostic code.
        1. For each group of 12 (or less) diagnostic codes,
            1. Add the subscriber hierarchical level.
            1. Increment HL count.
            1. Add the subscriber info.
            1. Add the subscriber name as it came from GDS.
            1. Add the subscriber address as it came from GDS.
            1. Add the subscriber city, state, ZIP as it came from GDS.
            1. Add the subscriber demographic information as it came from GDS.
            1. Add the subscriber secondary information as it came from GDS.
            1. Add the student's payer name.
            1. Add the student payer's address.
            1. Add the student payer's city, state, zip.
            1. Determine and add the claim totals.
            1. Add the EPSDT referral.
            1. Add the diagnostic codes for this group.
            1. For each billable record in the grouped diagnostic codes,
                1. Add the service line number (index of the current record + 1).
                1. Add the professional service pointed to the correct diagnostic code.
                1. Add the service date.
                1. Add the line item control number.
1. Determine the number of parts written so far.
1. Add the transaction set trailer with the number of parts.
1. Add the functional group trailer.
1. Add the interchange control trailer.
1. Merge the parts together, joined by line.
1. Uppercase the billing file contents.

##Rollups
When the billable sessions are identified and the billing file is created, the billable attendees and the sessions those attendees are in can be committed as billed.  Once billed, two rollups are created, one for billed attendees and the other for rejected attendees.  Both contain data relevant to the session and service data for the student.  The rejection rollup contains extra fields for the billing steps those attendees did not pass.  This data can be used later to produce reports.  Additionally, the successfully billed attendee rollup can be updated later on with remittance data for further reporting.
