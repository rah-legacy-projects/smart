#Import Detail

While the basic moving parts of the import are the same, validation criteria and identifying information differs.  These are outlined for each import below.

##Important Note
A majority of the data is [ranged](./ranging.html).  This means that the start and end dates will cause the base record (consisting of the identifiers) to have date spliced child records.  Please read the ranging document before reading this document.

##District
###Identifiers (Required)
* District Code (DistCode)

###Required
* DistCode
* Name
* Address Line 1
* City
* State
* Zip
* Telephone Number
* Start Date
* End Date
* Claiming Start Date
* NPI Number

###Row Validations
* *Start/End Date.*  The End Date must be after the Start Date.

##School
###Identifiers (Required)
* District Code (DistCode)
* School Code

###Required
* School
* Address Line 1
* City
* State
* Zip
* School Code
* School Nummber
* DistCode
* Start Date
* End Date

###Row Validations
* *District.*  The district code must exist in SMART for the school to be imported.
* *Start/End Date.*  The End Date must be after the Start Date.

##MCO

###Identifiers (Required)
* District Code (DistCode)
* Name

###Required
* Name
* Start Date
* End Date
* DistCode

###Row Validations
* *District.*  The district code must exist in SMART for the MCO to be imported.
* *Start/End Date.*  The End Date must be after the Start Date.

##Procedure Code

###Identifiers (Required)
* District Code (DistCode)
* Procedure Code

###Required
* Service Type
* Procedure Code
* Start Date
* End Date

### Row Validations
* *District.*  The district code must exist in SMART for the procedure code to be imported.
* *Start/End Date.*  The End Date must be after the Start Date.

##Diagnostic Code

###Identifiers (Required)
* District Code (DistCode) 
* Procedure Code
* ICD Code (Diagnostic Code)

### Required
* Procedure Code
* ICD Code
* Description
* DistCode
* Start Date
* End Date

###Row Validations
* *District.*  The district code must exist in SMART for the diagnostic code to be imported.
* *Procedure Code.*  The corresponding procedure code must exist in SMART for the diagnostic code to be imported.
* *Start/End Date.*  The End Date must be after the Start Date.

##Procedure Code Value

###Identifiers (Required)
* District Code (DistCode)
* MCO
* Procedure Code

###Required
* MCO
* Service Type
* Procedure Code
* Start Date
* End Date
* DistCode

###Row Validations
* *District.*  The district code must exist in SMART for the procedure code value to be imported.
* *Procedure Code.*  The corresponding procedure code must exist in SMART for the procedure code value to be imported.
* *MCO.*  The MCO must existin in SMART for the procedure code value to be imported.
* *Start/End Date.*  The End Date must be after the Start Date.
