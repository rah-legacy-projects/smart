//set up table model 
var TableModel = function(parameters) {

    //parameters = {
    //    dataMethodPath: dataMethodPath,
    //    rowModel: rowModel,
    //    defaultSortBy: defaultSortBy,
    //    getServerParameterCallback: getServerParameterCallback,
    //    afterPageSelectCallback: afterPageSelectCallback,
    //    noItemsFoundDisplay: noItemsFoundDisplay,
    //    filters: [{filterName:, filterValue}]
    //};

    var self = this;
    self.parameters = parameters;
    self.getServerParameterCallback = self.parameters.getServerParameterCallback;
    self.rowModel = self.parameters.rowModel;
    self.dataMethodPath = self.parameters.dataMethodPath;
    self.echo = ko.observable(1);
    self.processing = ko.observable(false);
    self.firstBindComplete = ko.computed(function() {
        return self.echo() > 1;
    });
    self.selectedPage = ko.observable(1);
    self.totalRecords = ko.observable(0);
    self.filteredRecords = ko.observable(0);
    //how many to display/stats

    self.displayRecordOptions = ko.observableArray([
        '10', '25', '50', '100'
    ]);
    self._SelectedDisplayRecordOption = ko.observableArray(['10']);
    self.selectedDisplayRecordOption = ko.computed({
        read: function() {
            return self._SelectedDisplayRecordOption();
        },
        write: function(value) {
            self._SelectedDisplayRecordOption(value);
            if (self.firstBindComplete()) {
                //one of the side effects of options:/value: is that it persists the value option
                //to what it is bound to, which would cause the first bind action to happen twice
                //at class instantiation time.
                self.pageSelect(self.selectedPage());
            }
        },
        owner: self
    });

    self.from = ko.computed(function() {
        if (self.totalRecords() === 0)
            return 0;
        return ((self.selectedPage() * parseInt(self.selectedDisplayRecordOption(), 10)) - parseInt(self.selectedDisplayRecordOption(), 10)) + 1;
    });
    self.to = ko.computed(function() {
        var retval = (self.selectedPage() * parseInt(self.selectedDisplayRecordOption(), 10));
        if (retval > self.totalRecords()) {
            return self.totalRecords();
        }
        return retval;
    });

    //sorting
    self.sortBy = ko.observable(self.parameters.defaultSortBy);
    self.order = ko.observable('ascending');
    self.setSortBy = function(sortBy) {
        if (sortBy === self.sortBy()) {
            if (self.order() === 'ascending') {
                self.order('descending');
            } else {
                self.order('ascending');
            }
        } else {
            self.order('ascending');
        }
        self.sortBy(sortBy);
        self.pageSelect(self.selectedPage());
    };

    //search bar
    self._PreviousSearch = ko.observable({});
    self._Timer = null;
    self._Filters = ko.observable({});

    self.filters = ko.computed({
        read: function() {
            console.log('reading filters');
            return self._Filters();
        },
        write: function(arrayValue) {
            console.log('writing: ' + ko.toJSON(arrayValue));
            ko.utils.arrayForEach(arrayValue, function(v) {
                if (!self._Filters()[v.filterName]) {
                    console.log('\t adding ' + v.filterName);
                    self._Filters()[v.filterName] = ko.observable(v.filterValue);
                } else {
                    console.log('\t modifying ' + v.filterName);
                    self._Filters()[v.filterName](v.filterValue);
                }

                self._Filters()[v.filterName + 'Watcher'] = ko.computed(function() {
                    //don't remove this: the act of unwrapping will trip the computed on filter change
                    var t = self._Filters()[v.filterName]();
                    console.log('changing ' + v.filterName + ' to ' + self._Filters()[v.filterName]());
                    if ( !! self.pageSelect) {
                        self._Timer = window.setTimeout(function() {
                            console.log('watcher for ' + v.filterName + ' tripped');
                            self.pageSelect(self.selectedPage());
                        }, 100);
                    }
                });
            });
        }
    });

    if ( !! parameters.filters) {
        console.log('setting up filters');
        self.filters(parameters.filters);
    }



    /*
    self.search = ko.computed({
        read: function() {
            return self._Search();
        },
        write: function(value) {
            console.log('search val: ' + JSON.stringify(value));
            if (self._PreviousSearch() === null || !_.isEqual(self._PreviousSearch(), value)) {
                if (self._Timer !== null) {
                    window.clearTimeout(self._Timer);
                }
                self._PreviousSearch(_.clone(self._Search()));
                self._Search(value);
                self._Timer = window.setTimeout(function() {
                    self.pageSelect(self.selectedPage());
                }, 1250);
            }
        },
        owner: self
    });*/

    //data
    self.rows = ko.observableArray([]);

    self.visibleRows = ko.computed(function() {
        return self.rows();
    });

    //pagination
    self.pages = ko.computed(function() {
        var pages = [];
        for (var i = 1; i <= Math.ceil(self.filteredRecords() / self.selectedDisplayRecordOption()); i++) {
            pages.push(i);
        }

        return pages;
    });

    self.selectablePages = ko.computed(function() {
        var page = self.selectedPage() - 1;
        var displayPages = 5;
        var sliceStart = 0,
            sliceEnd = displayPages;

        if (self.pages()
            .length > 5 && page > (Math.floor(displayPages / 2))) {
            if (page + Math.ceil(displayPages / 2) > self.pages()
                .length) {
                sliceStart = self.pages()
                    .length - 5;
                sliceEnd = self.pages()
                    .length;
            } else {
                sliceStart = page - Math.floor(displayPages / 2);
                sliceEnd = page + Math.ceil(displayPages / 2);
            }
        }
        if (self.pages()
            .length < self.selectedPage()) {
            self.selectedPage(1);
        }
        return self.pages()
            .slice(sliceStart, sliceEnd);
    });

    self.first = function() {
        self.pageSelect(1);
    };
    self.last = function() {
        self.pageSelect(self.pages()
            .length);
    };
    self.next = function() {
        if (self.selectedPage() + 1 <= self.pages()
            .length) {
            self.pageSelect(self.selectedPage() + 1);
        }
    };
    self.previous = function() {
        console.log(self.selectedPage() + ' to ' + (self.selectedPage() - 1))
        if (self.selectedPage() - 1 > 0) {
            self.pageSelect(self.selectedPage() - 1);
        }
    };
    self.processing = ko.observable(false);
    self.pageSelect = function(page) {
        self.processing(true);
        self.selectedPage(page);

        //build search off of filters
        var search = ko.toJS(self._Filters());

        var dataMethodParameters = {
            echo: self.echo(),
            search: search,
            displayLength: parseInt(self.selectedDisplayRecordOption(), 10),
            displayStart: ((self.selectedPage() - 1) * parseInt(self.selectedDisplayRecordOption(), 10)),
            orderByClauses: [{
                orderByName: self.sortBy(),
                orderByDirection: self.order()
            }]
        };

        if (self.getServerParameterCallback !== null && self.getServerParameterCallback !== undefined) {
            var parameters = self.getServerParameterCallback();
            for (var parameter in parameters) {
                dataMethodParameters[parameter] = parameters[parameter];
            }
        }

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: self.dataMethodPath,
            data: JSON.stringify(dataMethodParameters),
            dataType: "json",
        })
            .done(function(data) {
                if (self.echo() === parseInt(data.echo, 10)) {
                    self.filteredRecords(data.filteredRecords);
                    self.totalRecords(data.totalRecords);

                    //remove all rows currently shown
                    self.rows.removeAll();
                    //add all rows returned by the data method
                    if (data.recordSet !== undefined && data.recordSet !== null) {
                        $.each(data.recordSet, function(index, row) {
                            self.rows.push(new self.rowModel(row));
                        });
                    }

                    //self.echo(self.echo() + 1);
                    self.echo(data.echo + 1);

                    if (self.parameters.afterPageSelectCallback !== undefined && self.parameters.afterPageSelectCallback !== null) {
                        self.parameters.afterPageSelectCallback();
                    }

                    self.processing(false);
                }
            })
            .fail(function(i, e) {
                self.processing(false);
                console.log('setup error: ' + e);
            });

    };
    self.noItemsFound = ko.computed(function() {
        return self.visibleRows()
            .length === 0;
    });
    if ( !! parameters.noItemsFoundDisplay && parameters.noItemsFoundDisplay != -1 && parameters.noItemsFoundDisplay.jquery !== undefined) {
        //if the item is a jquery selector, use the html innards
        self.noItemsHTML = ko.observable(parameters.noItemsFoundDisplay.html());
    } else {
        //otherwise, use jquery to get the html from whatever was passed
        //wrap in a div to fake out 'outerhtml' behavior
        self.noItemsHTML = ko.observable($('<div>')
            .append($(parameters.noItemsFoundDisplay))
            .html());
    }

    //finally, invoke the first page selection
    self.pageSelect(1);
};
