ko.validation.configure({
    registerExtenders: true,
    messagesOnModified: true,
    insertMessages: false,
    parseInputAttributes: true,
    messageTemplate: null
});

$(function() {
    //adds c-sharp-y style string formats
    if (!String.prototype.format) {
        String.prototype.format = function() {
            var args = arguments;
            return this.replace(/{(\d+)}/g, function(match, number) {
                return typeof args[number] != 'undefined' ? args[number] : match;
            });
        };
    }

    //"fix" IE8+ console log errors
    if (typeof console === "undefined" || typeof console.log === "undefined") {
        console = {};
        console.log = function() {};
    }

    //guarded named replacement
    if (!String.prototype.namedFormat) {
        String.prototype.namedFormat = function() {
            var args = arguments;
            var used = [];

            if (Object.prototype.toString.call(arguments[0])
                .match(/^\[object (.*)\]$/)[1] === "Object") {
                var formatted = this.replace(/{(\w+)}/g, function(match, memberName) {
                    used.push(memberName);
                    if (typeof args[0][memberName] == 'undefined') {
                        console.warn(memberName + ' not found in the parameter list');
                    }
                    return typeof args[0][memberName] != 'undefined' ? args[0][memberName] : match;
                });

                for (var member in args[0]) {
                    if (used.indexOf(member) === -1) {
                        console.warn(member + ' not used by format string');
                    }
                }
                return formatted;
            } else {
                var formatted = this.replace(/{(\d+)}/g, function(match, number) {
                    used.push(parseInt(number, 10));
                    if (typeof args[number] == 'undefined') {
                        console.warn(number + ' not found in the parameter list');
                    }
                    return typeof args[number] != 'undefined' ? args[number] : match;
                });
                for (var i = 0; i < args.length; i++) {
                    if (used.indexOf(i) === -1) {
                        console.warn(i + ' not used by format string');
                    }
                }
                return formatted;
            }
        };
    }

});
