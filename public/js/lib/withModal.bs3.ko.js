ko.bindingHandlers.withModal = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var value = valueAccessor(),

            //childViewModel = bindingContext.createChildContext(viewModel);
            childViewModel = bindingContext.createChildContext(
                bindingContext.$rawData,
                null,
                function(context) {
                    console.log(valueAccessor());
                    ko.utils.extend(context, valueAccessor().properties);
                    context.close = function() {
                        console.log('#1# close');
                        context.show(false);
                        if ( !! context.onClose) {
                            context.onClose();
                        }

                    };
                    context.action = function() {
                        console.log('#2# action');
                        if ( !! context.onAction) {
                            context.onAction();
                        }

                    };
                    ko.utils.toggleDomNodeCssClass(element, 'modal fade', true);
                    console.log('looking for ' + value.template);
                    ko.renderTemplate(value.template, context, null, element);
                    var showHide = ko.computed(function() {
                        console.log('showhide');
                        $(element)
                            .modal(context.show() ? 'show' : 'hide');
                    });

                });
        //ko.applyBindingsToDescendants(childViewModel, element);
        return {
            controlsDescendantBindings: true
        };
    }
};
