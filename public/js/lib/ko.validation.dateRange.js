ko.validation.rules['dateRange'] = {
    getValue: function(o){
        return (typeof o === 'function' ? o() : o);
    },
    validator: function(start, end){
        var startMoment = moment(this.getValue(start)), endMoment = moment(this.getValue(end.params));
        return startMoment.isBefore(endMoment) || startMoment.isSame(endMoment);
    },
    message: 'Start is not before or the same as end.'
};
