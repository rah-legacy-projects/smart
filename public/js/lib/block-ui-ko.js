//usage
//block: boolean -- simple on or off with default "Please Wait" message
//block: {on: boolean, message:'some message'} -- on/off with custom message
//block: {on: boolean, message:null} -- on off with no message

ko.bindingHandlers.block = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel) {},
    update: function(element, valueAccessor, allBindingsAccessor, viewModel) {
        var thing = ko.utils.unwrapObservable(valueAccessor());
        var value = false;
        if (typeof thing === 'boolean') {
            value = thing === true;
        } else {
            value = ko.utils.unwrapObservable(thing.on);
        }

        if (value) {
            if (typeof thing === 'boolean') {
                $(element)
                    .block();
            } else {
                $(element)
                    .block(thing);
            }
        } else {
            $(element)
                .unblock();
        }
    }
};
