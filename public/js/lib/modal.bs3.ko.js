ko.bindingHandlers.modal_old = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var value = valueAccessor(),
            childViewModel = bindingContext.createChildContext(viewModel);
        ko.utils.extend(childViewModel, value.properties);
        childViewModel.close = function() {
            childViewModel.show(false);
            if ( !! childViewModel.onClose) {
                childViewModel.onClose();
            }
        };

        childViewModel.action = function() {
            if ( !! childViewModel.onAction) {
                childViewModel.onAction();
            }
        };

        ko.utils.toggleDomNodeCssClass(element, 'modal fade', true);
        console.log('looking for ' + value.template);
        ko.renderTemplate(value.template, childViewModel, null, element);
        var showHide = ko.computed(function() {
            $(element)
                .modal(childViewModel.show() ? 'show' : 'hide');
        });
        return {
            controlsDescendantBindings: true
        };
    }
};
