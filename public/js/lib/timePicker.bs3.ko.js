ko.bindingHandlers.timePicker = {
    init: function(element, valueAccessor, allBindingsAccessor) {

        var minuteStep = valueAccessor().minuteStep || 1;

        $(element)
            .datetimepicker({
                format: valueAccessor()
                    .format,
                language: 'en',
                pickDate: false,
                minuteStepping: minuteStep
            })
             .on("dp.show", function(e){
                var observable = valueAccessor()
                    .value;

                if(!observable() || (!(/\w*/.test(observable())))){
                    var now = moment();
                    observable(now.format(valueAccessor().format));
                }
            })

            .on("change.dp", function(e) {
                var observable = valueAccessor()
                    .value;
                if ( !! e.timeStamp) {
                    var thisMoment = moment($(this)
                        .data("DateTimePicker")
                        .getDate()
                        ._d);

                        console.log('setting tp observable to ' + thisMoment.format(valueAccessor().format));
                    observable(thisMoment.format(valueAccessor()
                        .format));
                }
            });

        //set up the timepicker to reflect the selected time
        $(element)
            .data('DateTimePicker')
            .setDate(moment(valueAccessor()
                .value(), valueAccessor()
                .format));

    },
    update: function(element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        if ( !! value.value) {
            console.log('[update] ' + value.value());
            $(element)
                .data("DateTimePicker")
                .setDate(value.value());
        }
    }
};
