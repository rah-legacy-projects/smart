﻿
var StaticTableModel = function(parameters) {

    //parameters = {
    //    table: table,
    //    rowModel: rowModel,
    //    defaultSortBy: defaultSortBy,
    //    afterPageSelectCallback: afterPageSelectCallback,
    //    noItemsFoundDisplay: noItemsFoundDisplay
    //    filters: [{filterName:, filterValue}]
    //};

    var self = this;
    self.parameters = parameters;
    self.rowModel = self.parameters.rowModel;

    self.processing = ko.observable(false);
    self.firstBindComplete = ko.observable(false);
    self.selectedPage = ko.observable(1);
    //how many to display/stats
    self.displayRecordOptions = ko.observableArray(['10', '25', '50', '100']);
    self.selectedDisplayRecordOption = ko.observableArray(['10']);



    //sorting
    self.sortBy = ko.observable(parameters.defaultSortBy);
    self.order = ko.observable('ascending');
    self.setSortBy = function(sortBy) {
        if (sortBy === self.sortBy()) {
            if (self.order() === 'ascending') {
                self.order('descending');
            } else {
                self.order('ascending');
            }
        } else {
            self.order('ascending');
        }
        self.sortBy(sortBy);
        self.rows.sort(function(left, right) {
            if (self.order() === 'ascending') {
                return left[self.sortBy()]()
                    .toLowerCase() == right[self.sortBy()]()
                    .toLowerCase() ? 0 : (left[self.sortBy()]()
                        .toLowerCase() < right[self.sortBy()]()
                        .toLowerCase() ? -1 : 1);
            } else if (self.order() === 'descending') {
                return left[self.sortBy()]()
                    .toLowerCase() == right[self.sortBy()]()
                    .toLowerCase() ? 0 : (left[self.sortBy()]()
                        .toLowerCase() > right[self.sortBy()]()
                        .toLowerCase() ? -1 : 1);
            }
            console.log("Unknown order.");
        });
    };

    //search bar
    self.search = ko.observable('');

    //data
    if (self.rowModel === null || self.rowModel === undefined) {
        console.log("warning: rowmodel not defined");
        self.rows = ko.observableArray(parameters.table.Rows);
    } else {
        self.rows = ko.observableArray(ko.utils.arrayMap(parameters.table.rows, function(row) {
            return new self.rowModel(row);
        }));
    }

    self.totalRecords = ko.computed(function() {
        return self.rows()
            .length;
    });


    self.from = ko.computed(function() {
        if (self.totalRecords() === 0) return 0;
        return ((self.selectedPage() * parseInt(self.selectedDisplayRecordOption(), 10)) - parseInt(self.selectedDisplayRecordOption(), 10)) + 1;
    });
    self.to = ko.computed(function() {
        var retval = (self.selectedPage() * parseInt(self.selectedDisplayRecordOption(), 10));
        if (retval > self.totalRecords()) {
            return self.totalRecords();
        }
        return retval;
    });

    self._Filters = ko.observable({});

    self.filters = ko.computed({
        read: function() {
            return self._Filters();
        },
        write: function(arrayValue) {
            ko.utils.arrayForEach(arrayValue, function(v) {
                console.log('filter ' + v.filterName + ' is ' + v.filterValue);
                if (!self._Filters()[v.filterName]) {
                    console.log('\t new');
                    self._Filters()[v.filterName] = ko.observable(v.filterValue);
                } else {
                    console.log('\t exists');
                    self._Filters()[v.filterName](v.filterValue);
                }

                self._Filters()[v.filterName + 'Watcher'] = ko.computed(function() {
                    console.log('changing ' + v.filterName + ' to ' + self._Filters()[v.filterName]());
                    if ( !! self.pageSelect) {
                        self._Timer = window.setTimeout(function() {
                            self.pageSelect(self.selectedPage());
                        }, 100);
                    }
                });
            });
        }
    });

    if ( !! parameters.filters) {
        self.filters(parameters.filters);
    }

    self.filteredRows = ko.computed(function() {
        var returnVal = self.rows();
        ko.utils.arrayForEach(self.filters(), function(filter){
            returnVal = ko.utils.arrayFilter(returnVal, function(row){
                return (new RegExp(filter.value, i)).test(row[filter.fieldName]());
            });
        });
        return returnVal;
    });
    self.filteredRecords = ko.computed(function() {
        return self.filteredRows()
            .length;
    });

    self.visibleRows = ko.computed(function() {
        return self.filteredRows()
            .slice(self.from() - 1, self.to());
    });



    //pagination
    self.pages = ko.computed(function() {
        var pages = [];
        for (var i = 1; i <= Math.ceil(self.filteredRows()
            .length / self.selectedDisplayRecordOption()); i++) {
            pages.push(i);
        }

        return pages;
    });

    self.selectablePages = ko.computed(function() {
        var page = self.selectedPage() - 1;
        var displayPages = 5;
        var sliceStart = 0,
            sliceEnd = displayPages;

        if (page > (Math.floor(displayPages / 2))) {
            if (page + Math.ceil(displayPages / 2) > self.pages()
                .length) {
                sliceStart = self.pages()
                    .length - 5;
                sliceEnd = self.pages()
                    .length;
            } else {
                sliceStart = page - Math.floor(displayPages / 2);
                sliceEnd = page + Math.ceil(displayPages / 2);
            }
        }

        if (self.pages()
            .length < self.selectedPage()) {
            self.selectedPage(1);
        }
        return self.pages()
            .slice(sliceStart, sliceEnd);
    });

    self.first = function() {
        self.pageSelect(1);
    };
    self.last = function() {
        self.pageSelect(self.pages()
            .length);
    };
    self.next = function() {
        if (self.selectedPage() + 1 <= self.pages()
            .length) {
            self.pageSelect(self.selectedPage() + 1);
        }
    };
    self.previous = function() {
        if (self.selectedPage() - 1 > 1) {
            self.pageSelect(self.selectedPage() - 1);
        }
    };
    self.pageSelect = function(page) {
        self.selectedPage(page);
    };

    self.noItemsFound = ko.computed(function() {
        return self.visibleRows()
            .length === 0;
    });
    if ( !! parameters.noItemsFoundDisplay && parameters.noItemsFoundDisplay != -1 && parameters.noItemsFoundDisplay.jquery !== undefined) {
        //if the item is a jquery selector, use the html innards
        self.noItemsHTML = ko.observable(parameters.noItemsFoundDisplay.html());
    } else {
        //otherwise, use jquery to get the html from whatever was passed
        //wrap in a div to fake out 'outerhtml' behavior
        self.noItemsHTML = ko.observable($('<div>')
            .append($(parameters.noItemsFoundDisplay))
            .html());
    }

    setTimeout(function(){
        self.firstBindComplete(true);
    }, 500);

};

