﻿ko.dirtyFlag = function (root, isInitiallyDirty) {
    var result = function () { },
        _initialStateJSON = ko.observable(ko.toJSON(root)),
        _initialState = ko.observable(ko.toJS(root)),
        _isInitiallyDirty = ko.observable(isInitiallyDirty);

    result.isDirty = ko.computed(function () {
        return _isInitiallyDirty() || _initialStateJSON() !== ko.toJSON(root);
    });

    result.isMemberDirty = function (memberName) {
        if (_initialState()[memberName] != ko.toJS(root)[memberName]) { 
            return true;
        }
        return false;
    };


    result.makeDirtyFunction = function (memberName) {result["is" + memberName + "Dirty"] = ko.computed(function () {return (_initialState()[memberName] !== ko.toJS(root)[memberName]);});}

    for (var memberName in ko.toJS(root)) {
        //make sure the member isn't a function
        if (Object.prototype.toString.call(ko.toJS(root)[memberName]) !== "[object Function]") {
            //make a dirty member for the member name
            //rah: note that it does in fact have to be done this way and not in a single pass due to how variable scoping works in javascript.
            result.makeDirtyFunction(memberName);
        }
    } 

    result.reset = function () {
        _initialStateJSON(ko.toJSON(root));
        _initialState(ko.toJS(root));
        _isInitiallyDirty(false);
    };

    result.clean = function (newRoot) {
        _initialStateJSON(ko.toJSON(newRoot));
        _initialState(ko.toJS(newRoot));
        _isInitiallyDirty(false);
    };

    return result;
};
 
