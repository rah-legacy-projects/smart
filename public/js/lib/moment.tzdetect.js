//borrowed and adapted from https://github.com/Canop/tzdetect.js
var tzdetect = {
    names: moment.tz.names(),
    preferredNames: [
        'America/New_York', //EST
        'America/Chicago', //CST
        'America/Denver', //MST
        'America/Los_Angeles' //PST
    ],
    matches: function(m){
        var results = [], now = moment(), makeKey = function(id){
            return [0, 4, 8, -5*12, 4-5*12, 8-5*12, 4-2*12, 8-2*12].map(function(months){
                var t = moment(now).add(months, 'months');
                if(id){
                    t.tz(id);
                }
                return t.format('DDHHmm');
            }).join(' ');
        }, locKey = makeKey(m);

        tzdetect.preferredNames.forEach(function(id){
            if(makeKey(id) === locKey){
                results.push(id);
            }
        });

        tzdetect.names.forEach(function(id){
            if(makeKey(id) === locKey){
                results.push(id);
            };
        });
        return results;
    }
};
