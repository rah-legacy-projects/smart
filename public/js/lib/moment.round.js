//hack: this is a shim until moment #1595 gets roundTo
//hacK: https://github.com/moment/moment/pull/1595
moment.fn.roundNext15Min = function() {
    var q = Math.floor(this.minutes() / 15);
    if (this.minutes() % 15 != 0)
        q++;
    if (q == 4) {
        this.add('hours', 1);
        q = 0;
    }
    this.minutes(q * 15);
    this.seconds(0);
    return this;
}
