var Log = function(data) {
    var self = this;
    self.message = ko.observable(data.message);
    self.timestamp = ko.observable(moment(data.timestamp));
    self.level = ko.observable(data.level);
    self.hostname = ko.observable(data.hostname);
    self.meta = ko.observable(ko.toJSON(data.meta));
    console.log(self.meta());
};

var LogWrapperModel = function() {
    var self = this;
    self.logListRef = ko.observable(new TableModel({
        dataMethodPath: '/logging',
        rowModel: Log,
        defaultSortBy: 'timestamp',
        filters: [{
            filterName: 'message',
            filterValue: null
        }, {
            filterName: 'start',
            filterValue: moment().subtract(30, 'days'),
        }, {
            filterName: 'end',
            filterValue: moment()
        }, {
            filterName: 'level',
            filterValue: null
        }, {
            filterName: 'hostname',
            filterValue: null,
        }]
    }));
};
