//{{{ student mco info 
var StudentMCOInfo = function(districtCode, data) {
    var self = this;
    self.districtCode = ko.observable(districtCode);
    if (!data) {
        data = {};
    }
    self.studentMCOInfoId = ko.observable(data._id);
    self.specialty = ko.observable(data.specialty);
    self.numberOfSessions = ko.observable(data.numberOfSessions);
    self.approvalToTreat = ko.observable(data.approvalToTreat || true);
    self.startDate = ko.observable(data.startDate);
    self.endDate = ko.observable(data.endDate);
    self.createdDate = ko.observable(data.created);

    self.specialty.extend({
        required: true
    });
    self.numberOfSessions.extend({
        number: true
    });
    self.startDate.extend({
        required: true
    });
    self.endDate.extend({
        required: true
    });
    self.approvalToTreat.extend({
        required: true
    });


    self.sortDate = ko.computed(function() {
        return ("{start}_{end}_{created}")
            .namedFormat({
                start: moment(self.startDate())
                    .valueOf(),
                end: moment(self.endDate())
                    .valueOf(),
                created: moment(self.createdDate())
                    .valueOf()
            });
    });

    self.startDateDisplay = ko.computed(function() {
        if (moment(self.startDate())
            .isValid() && !!self.startDate()) {
            return moment(self.startDate())
                .format('M/D/YYYY');
        }
        return '';

    });
    self.endDateDisplay = ko.computed(function() {
        if (moment(self.endDate())
            .isValid() && !!self.endDate()) {
            return moment(self.endDate())
                .format('M/D/YYYY');
        }
        return '';
    });

    self.specialtyDisplay = ko.computed(function() {
        if (!self.specialty()) {
            return "Select one...";
        } else {
            return self.specialty();
        }
    });

    self.approvalToTreatDisplay = ko.computed(function() {
        if (self.approvalToTreat()) {
            return 'Yes'
        }
        return 'No';
    });
    self.numberOfSessionsDisplay = ko.computed(function() {
        if ((self.numberOfSessions() || '') == '') {
            return 'N/A';
        }
        return self.numberOfSessions();
    });


    self.startDate.extend({
        dateRange: {
            params: self.endDate
        }
    });
    self.dateRangeValid = ko.computed(function() {
        return moment(self.startDate())
            .isBefore(moment(self.endDate())) ||
            moment(self.startDate())
            .isSame(moment(self.endDate()));
    });

    self.errors = ko.validation.group(self);
};
//}}}

//{{{ diagnostic code model/ options 
var DiagnosticCodeOptionModel = function(districtCode, data) {
    var self = this;
    self.districtCode = ko.observable(districtCode);
    self.diagnosticCodeId = ko.observable(data.diagnosticCodeId);
    self.description = ko.observable(data.description);
    self.icd = ko.observable(data.icd);
    self.therapy = ko.observable(data.therapy);
    self.startDate = ko.observable(data.startDate);
    self.endDate = ko.observable(data.endDate);
};

//}}}

//{{{ GDS data list format
var GDSDataModel = function(data) {
    var self = this;
    self.medicaidStartDate = ko.observable(moment(data.startDate)
        .format('M/D/YYYY'));
    self.medicaidEndDate = ko.observable(moment(data.endDate)
        .format('M/D/YYYY'));
    self.medicaidNumber = ko.observable(data.mcaidNum);
    self.planNumber = ko.observable(data.planBeneNum);
    self.mcoName = ko.observable(data.mcoName);
};
//}}}

var ParentalConsentModel = function(districtCode, data) {
    var self = this;
    self.districtCode = ko.observable(districtCode);
    if (!data) {
        data = {};
    }
    self.parentalConsentId = ko.observable(data._id);
    self.toShareIEP = ko.observable(data.toShareIEP);
    self.toShareIEPDenied = ko.observable(data.toShareIEPDenied || false);
    self.toBillMedicaid = ko.observable(data.toBillMedicaid);
    self.toBillMedicaidDenied = ko.observable(data.toBillMedicaidDenied || false);
    self.toTreat = ko.observable(data.toTreat);
    self.toTreatDenied = ko.observable(data.toTreatDenied || false);
    self.created = ko.observable(data.created);

    self.toShareIEPDisplay = ko.computed(function() {
        if (!!self.toShareIEP()) {
            return moment(self.toShareIEP())
                .format('M/D/YYYY');
        }
        return '';
    });
    self.toBillMedicaidDisplay = ko.computed(function() {
        if (!!self.toBillMedicaid()) {
            return moment(self.toBillMedicaid())
                .format('M/D/YYYY');
        }
        return '';
    });
    self.toTreatDisplay = ko.computed(function() {
        if (!!self.toTreat()) {
            return moment(self.toTreat())
                .format('M/D/YYYY');
        }
        return '';
    });

    self.toShareIEPListDisplay = ko.computed(function() {
        if (self.toShareIEPDisplay() == '') return 'Not Entered';
        return self.toShareIEPDisplay();
    });
    self.toBillMedicaidListDisplay = ko.computed(function() {
        if (self.toBillMedicaidDisplay() == '') return 'Not Entered';
        return self.toBillMedicaidDisplay();
    });
    self.toTreatListDisplay = ko.computed(function() {
        if (self.toTreatDisplay() == '') return 'Not Entered';
        return self.toTreatDisplay();
    });

    self.toShareIEPDeniedDisplay = ko.computed(function() {
        if (self.toShareIEPDenied()) {
            return 'Yes';
        }
        return 'No';
    });
    self.toBillMedicaidDeniedDisplay = ko.computed(function() {
        if (self.toBillMedicaidDenied()) {
            return 'Yes';
        }
        return 'No';
    });
    self.toTreatDeniedDisplay = ko.computed(function() {
        if (self.toTreatDenied()) {
            return 'Yes';
        }
        return 'No';
    });

    self.errors = ko.validation.group(self);


};

var PrescriptionModel = function(districtCode, data) {
    var self = this;
    self.districtCode = ko.observable(districtCode);
    if (!data) {
        data = {};
    }
    self.serviceType = ko.observable(data.serviceType);
    self.doctorName = ko.observable(data.doctorName);
    self.prescriptionId = ko.observable(data._id);
    self.startDate = ko.observable(data.startDate);
    self.endDate = ko.observable(data.endDate);
    self.createdDate = ko.observable(data.create);

    self.endDateDisplay = ko.computed(function() {
        if (!!self.endDate()) {
            return moment(self.endDate())
                .format('M/D/YYYY');
        }
        return '';
    });
    self.startDateDisplay = ko.computed(function() {
        if (!!self.startDate()) {
            return moment(self.startDate())
                .format('M/D/YYYY');
        }
        return '';
    });

    self.serviceTypeDisplay = ko.computed(function() {
        if (!self.serviceType()) {
            return "Select one...";
        } else {
            return self.serviceType();
        }
    });

    self.serviceType.extend({
        required: true
    });
    self.doctorName.extend({
        required: true
    });
    self.startDate.extend({
        required: true
    });
    self.endDate.extend({
        required: true
    });

    self.startDate.extend({
        dateRange: {
            params: self.endDate
        }
    });
    self.dateRangeValid = ko.computed(function() {
        return moment(self.startDate())
            .isBefore(moment(self.endDate())) ||
            moment(self.startDate())
            .isSame(moment(self.endDate()));
    });


    self.errors = ko.validation.group(self);
};

var AdditionalInformationModel = function(wrapper, data) {
    var self = this;
    self.districtCode = ko.observable(wrapper.districtCode());

    //repeat of demographics
    self.dateOfBirth = ko.observable(data.dateOfBirth);
    self.dateOfBirthDisplay = ko.computed(function() {
        return moment(self.dateOfBirth())
            .format('M/D/YYYY');
    });
    self.gender = ko.observable(data.gender);
    self.ethnicity = ko.observable(data.ethnicity);
    self.studentId = ko.observable(data.studentId);
    self.id = ko.observable(data.id);

    //medicaid info
    //hack: sort the gds data by date desc on the client
    data.gdsData = _.chain(data.gdsData)
        .sortBy(function(g) {
            return moment(g.startDate)
                .format('YYYYMMDD') + "_" + moment(g.endDate)
                .format('YYYYMMDD');
        })
        .reverse()
        .value();
    self.gdsData = ko.observableArray(ko.utils.arrayMap(data.gdsData, function(g) {
        return new GDSDataModel(g);
    }));

    //parental consents
    self.parentalConsents = ko.observableArray(ko.utils.arrayMap(data.parentalConsents, function(pc) {
        return new ParentalConsentModel(self.districtCode(), pc);
    }));

    self.sortedParentalConsents = ko.computed(function() {
        return _.sortBy(self.parentalConsents(), function(pc) {
                var dates = [!!pc.toShareIEP() ? moment(pc.toShareIEP()) : null, !!pc.toBillMedicaid() ? moment(pc.toBillMedicaid()) : null, !!pc.toTreat() ? moment(pc.toTreat()) : null]

                dates = _.filter(dates, function(d) {
                    return !!d;
                });

                dates = _.sortBy(dates, function(d) {
                    return d.format('YYYYMMDD');
                });

                if (dates.length == 0) {
                    return moment(pc.created());
                } else {
                    return dates[dates.length - 1];
                }
            })
            .reverse();
    });

    //diagnostic code options
    self.diagnosticCodeOptions = ko.observableArray(ko.utils.arrayMap(data.diagnosticCodeOptions, function(dco) {
        return new DiagnosticCodeOptionModel(self.districtCode(), dco);
    }));

    //therapy options
    //self.therapyOptions = ko.observableArray(data.specialities);

    //prescriptions
    self.prescriptions = ko.observableArray(ko.utils.arrayMap(data.prescriptions, function(p) {
        return new PrescriptionModel(self.districtCode(), p);
    }));
    self.sortedPrescriptions = ko.computed(function() {
        //hack: string sort by start, then by end, then by created using strings
        return _.sortBy(self.prescriptions(), function(rx) {
                return ("{start}_{end}_{created}")
                    .namedFormat({
                        start: moment(rx.startDate())
                            .valueOf(),
                        end: moment(rx.endDate())
                            .valueOf(),
                        created: moment(rx.createdDate())
                            .valueOf()
                    });
            })
            .reverse();
    });

    //mco information
    self.studentMCOInfos = ko.observableArray(ko.utils.arrayMap(data.studentMCOInfos, function(mcoas) {
        return new StudentMCOInfo(self.districtCode(), mcoas);
    }));

    self.sortedStudentMCOInfos = ko.computed(function() {
        return _.sortBy(self.studentMCOInfos(), function(mco) {
                return mco.sortDate();
            })
            .reverse();
    });




    self.saveParentalConsent = function(consent, cb) {
        $.ajax({
            url: '/' + self.districtCode() + '/student/' + self.id() + '/saveParentalConsent',
            type: 'POST',
            data: consent
        })
            .done(function(response) {
                if (!!cb) {
                    cb(response);
                }
            });


    };

    self.savePrescription = function(rx, cb) {
        $.ajax({
            url: '/' + self.districtCode() + '/student/' + self.id() + '/savePrescription',
            type: 'POST',
            data: rx
        })
            .done(function(response) {
                if (!!cb) {
                    cb(response);
                }
            });
    };

    self.saveStudentMCOInfo = function(mcoInfo, cb) {
        $.ajax({
            url: '/' + self.districtCode() + '/student/' + self.id() + '/saveStudentMCOInfo',
            type: 'POST',
            data: mcoInfo
        })
            .done(function(response) {
                if (!!cb) {
                    cb(response);
                }
            });
    };


    self.removeParentalConsent = function(consent) {
        AreYouSure.show('Remove this parental consent?', function() {
            $.ajax({
                url: '/' + self.districtCode() + '/student/' + self.id() + '/removeParentalConsent/' + consent.parentalConsentId(),
                type: 'POST'
            })
                .done(function(response) {
                    self.parentalConsents(
                        ko.utils.arrayFilter(self.parentalConsents(), function(pc) {
                            return pc.parentalConsentId() !== consent.parentalConsentId();
                        }));
                });
        });
    };

    self.removePrescription = function(rx) {
        AreYouSure.show('Remove this prescription?', function() {

            $.ajax({
                url: '/' + self.districtCode() + '/student/' + self.id() + '/removePrescription/' + rx.prescriptionId(),
                type: 'POST'
            })
                .done(function(response) {
                    self.prescriptions(
                        ko.utils.arrayFilter(self.prescriptions(), function(p) {
                            return p.prescriptionId() !== rx.prescriptionId();
                        }));
                });
        });
    };

    self.removeStudentMCOInfo = function(mcoInfo) {
        AreYouSure.show("Remove MCO information?", function() {
            $.ajax({
                url: '/' + self.districtCode() + '/student/' + self.id() + '/removeStudentMCOInfo/' + mcoInfo.studentMCOInfoId(),
                type: 'POST'
            })
                .done(function(response) {
                    self.studentMCOInfos(
                        ko.utils.arrayFilter(self.studentMCOInfos(), function(p) {
                            return p.studentMCOInfoId() !== mcoInfo.studentMCOInfoId();
                        }));
                });
        });
    };


    //{{{ internal parental consent  mod al model
    var InternalParentalConsentModalModel = function() {
            var modal = this;
            modal.showErrors = ko.observable(false);
            modal.onClose = function() {
                //alert('close');
                if (!!modal.originalData()) {
                    //was not new data.  restore to original values
                    _.forIn(modal.originalData(), function(value, member) {
                        if (!ko.isComputed(modal.originalData()[member]) && ko.isObservable(modal.originalData()[member])) {
                            modal.data()[member](value());
                        }
                    });
                }
            };
            modal.onAction = function() {
                //check validity
                if (modal.data()
                    .errors()
                    .length > 0) {
                    modal.showErrors(true);
                } else {
                    //save!

                    self.saveParentalConsent(ko.toJS(modal.data), function(response) {
                        modal.data()
                            .parentalConsentId(response.value.parentalConsentId);
                        if (!modal.originalData()) {
                            self.parentalConsents
                                .push(modal.data());
                        }
                        modal.show(false);
                    });
                }
            };

            modal.show = ko.observable(false);

            modal.originalData = ko.observable();
            modal.data = ko.observable(new ParentalConsentModel(self.districtCode(), null));
            modal.open = function(mapping) {
                modal.originalData(null);
                if (!!mapping) {
                    modal.originalData(new ParentalConsentModel(self.districtCode(), ko.toJS(mapping)));
                    modal.data(mapping);
                } else {
                    modal.data(new ParentalConsentModel(self.districtCode(), null));
                }
                modal.show(true);
            };

            return modal;
        }
        //}}}

    //{{{ internal student mco info modal model
    var InternalStudentMCOInfoModalModel = function() {
            var modal = this;
            modal.showErrors = ko.observable(false);
            modal.onClose = function() {
                //alert('close');
                if (!!modal.originalData()) {
                    //was not new data.  restore to original valuesa
                    _.forIn(modal.originalData(), function(value, member) {
                        if (!ko.isComputed(modal.originalData()[member]) && ko.isObservable(modal.originalData()[member])) {
                            modal.data()[member](value());
                        }
                    });
                }
            };
            modal.onAction = function() {
                //check validity
                if (modal.data()
                    .errors()
                    .length > 0) {
                    modal.showErrors(true);
                } else {
                    //save!
                    self.saveStudentMCOInfo(ko.toJS(modal.data), function(response) {
                        modal.data()
                            .studentMCOInfoId(response.value.studentMcoInfoId);
                        if (!modal.originalData()) {
                            self.studentMCOInfos
                                .push(modal.data());
                        }
                        modal.show(false);
                    });
                }
            };

            modal.show = ko.observable(false);

            modal.originalData = ko.observable();
            modal.data = ko.observable(new StudentMCOInfo(self.districtCode(), null));
            modal.open = function(mapping) {
                modal.originalData(null);
                if (!!mapping) {
                    modal.originalData(new StudentMCOInfo(self.districtCode(), ko.toJS(mapping)));
                    modal.data(mapping);
                } else {
                    modal.data(new StudentMCOInfo(self.districtCode(), null));
                }
                modal.show(true);
            };

            modal.therapyOptions = ko.computed(function() {
                var options = ko.utils.arrayFilter(wrapper.specialtyOptions(), function(specialty) {
                    console.log(specialty.service() + ": ");

                    var dataStart = moment(modal.data()
                            .startDate()),
                        dataEnd = moment(modal.data()
                            .endDate()),
                        specialtyStart = moment(specialty.startDate()),
                        specialtyEnd = moment(specialty.endDate());

                    if (dataStart.isAfter(specialtyStart) && dataEnd.isBefore(specialtyEnd)) {
                        //inside
                        console.log(specialty.service() + ": inside");
                        return true;
                    } else if (dataStart.isBefore(specialtyStart) && dataEnd.isAfter(specialtyStart)) {
                        //data overlaps specialty on start side
                        console.log(specialty.service() + ": start");
                        return true;
                    } else if (dataStart.isAfter(specialtyStart) && dataStart.isBefore(specialtyEnd)) {
                        //data overlaps specialty on end side
                        console.log(specialty.service() + ": end");
                        return true;
                    } else {
                        //data doesn't touch
                        console.log(specialty.service() + ": notouch");
                        return false;
                    }


                });

                return options;
            });

            return modal;
        }
        //}}} 


    //{{{  internal prescription  modal model
    var InternalPrescriptionModalModel = function() {
            var modal = this;
            modal.showErrors = ko.observable(false);
            modal.onClose = function() {
                modal.showErrors(false);
                //alert('close');
                if (!!modal.originalData()) {
                    //was not new data.  restore to original valuesa
                    _.forIn(modal.originalData(), function(value, member) {
                        if (!ko.isComputed(modal.originalData()[member]) && ko.isObservable(modal.originalData()[member])) {
                            modal.data()[member](value());
                        }
                    });
                }
            };
            modal.onAction = function() {
                modal.showErrors(false);
                //check validity
                if (modal.data()
                    .errors()
                    .length > 0) {
                    modal.showErrors(true);
                } else {
                    //save!
                    self.savePrescription(ko.toJS(modal.data), function(response) {
                        modal.data()
                            .prescriptionId(response.value.prescriptionId);
                        if (!modal.originalData()) {
                            self.prescriptions
                                .push(modal.data());
                        }
                        modal.show(false);
                    });
                }
            };

            modal.show = ko.observable(false);

            modal.originalData = ko.observable();
            modal.data = ko.observable(new PrescriptionModel(self.districtCode(), null));
            modal.open = function(mapping) {
                modal.originalData(null);
                if (!!mapping) {
                    modal.originalData(new PrescriptionModel(self.districtCode(), ko.toJS(mapping)));
                    modal.data(mapping);
                } else {
                    modal.data(new PrescriptionModel(self.districtCode(), null));
                }
                modal.show(true);
            };

            modal.therapyOptions = ko.computed(function() {
                var options = ko.utils.arrayFilter(wrapper.specialtyOptions(), function(specialty) {
                    console.log(specialty.service() + ": ");

                    var dataStart = moment(modal.data()
                            .startDate()),
                        dataEnd = moment(modal.data()
                            .endDate()),
                        specialtyStart = moment(specialty.startDate()),
                        specialtyEnd = moment(specialty.endDate());

                    if (dataStart.isAfter(specialtyStart) && dataEnd.isBefore(specialtyEnd)) {
                        //inside
                        console.log(specialty.service() + ": inside");
                        return true;
                    } else if (dataStart.isBefore(specialtyStart) && dataEnd.isAfter(specialtyStart)) {
                        //data overlaps specialty on start side
                        console.log(specialty.service() + ": start");
                        return true;
                    } else if (dataStart.isAfter(specialtyStart) && dataStart.isBefore(specialtyEnd)) {
                        //data overlaps specialty on end side
                        console.log(specialty.service() + ": end");
                        return true;
                    } else {
                        //data doesn't touch
                        console.log(specialty.service() + ": notouch");
                        return false;
                    }


                });

                return options;
            });

            return modal;
        }
        //}}}


    self.parentalConsentModal = new InternalParentalConsentModalModel();
    self.prescriptionModal = new InternalPrescriptionModalModel();
    self.studentMCOInfoModal = new InternalStudentMCOInfoModalModel();

    self.removeDiagnosticCode = function(diagnosticCode) {
        //filter out this diagnostic code
        var filtered = _.filter(self.diagnosticCodes(), function(dc) {
            return dc.id() !== diagnosticCode.id();
        });
        self.diagnosticCodes(filtered);
        self.saveDiagnosticCodes();
    }
};

var AdditionalInfoWrapper = function(districtCode, studentId) {
    var self = this;
    self.districtCode = ko.observable(districtCode);
    self.additionalInformation = ko.observable();
    self.specialtyOptions = ko.observableArray([]);
    var tasks = {
        additionalInfo: function(cb) {
            $.ajax({
                url: '/' + self.districtCode() + '/student/' + studentId + '/additionalInfo',
                type: 'POST'
            })
                .done(function(response) {
                    self.additionalInformation(new AdditionalInformationModel(self, response.value));
                    cb();
                });
        },
        specialtyOptions: function(cb) {
            $.ajax({
                url: '/' + self.districtCode() + '/service/' + studentId + '/getServices',
                type: 'POST'
            })
                .done(function(response) {
                    self.specialtyOptions(ko.utils.arrayMap(response.value, function(p) {
                        return new SpecialtyOptionModel(self.districtCode(), p);
                    }));
                    cb(null);
                });
        }
    }
    async.parallel(tasks, function(err, r) {});

};



var IEPServiceModel = function(districtCode, wrapper, data) {
    var self = this;
    self.districtCode = ko.observable(districtCode);
    if (!data) {
        data = {};
    }
    self.serviceRangeId = ko.observable(data.serviceRangeId);
    self.service = ko.observable(data.service);
    self.startDate = ko.observable(data.startDate);
    self.endDate = ko.observable(data.endDate);
    self.groupTherapyOrdered = ko.observable(data.groupTherapyOrdered);
    self.dateSpan = ko.computed(function() {
        var startMoment = moment(self.startDate());
        var endMoment = moment(self.endDate());
        return startMoment.format('M/D/YYYY') + ' - ' + endMoment.format('M/D/YYYY');
    });

    self.editable = ko.computed(function() {
        var startMoment = moment(self.startDate()),
            endMoment = moment(self.endDate()),
            now = moment();
        return (startMoment.isBefore(now) && endMoment.isAfter(now)) || startMoment.isAfter(now);
    });

    self.frequencyAndDuration = ko.observable(data.frequencyAndDuration);
    self.providerName = ko.observable(data.therapistName);
    self.diagnosticCode = ko.observable(data.diagnosticCode);

    self.diagnosticCodeDisplay = ko.computed(function() {
        var dc = self.diagnosticCode();
        var dc = ko.utils.arrayFirst(wrapper.diagnosticCodeOptions(), function(o) {
            return o.diagnosticCodeId() === dc;
        });
        if (!dc) return 'Not selected';
        return dc.icd() + ' - ' + dc.description();
    });

    self.diagnosticCode.extend({
        required: true
    });

    self.errors = ko.validation.group(self);
};

var IEPServiceModelWrapper = function(districtCode, studentId) {
    var self = this;
    self.districtCode = ko.observable(districtCode);
    self.iepServices = ko.observableArray([]);

    self.iepServicesDisplay = ko.computed(function() {
        var temp = ko.toJS(self.iepServices());


    });

    self.diagnosticCodeOptions = ko.observableArray([]);

    self.saveServiceRange = function(dc, cb) {
        console.log(dc.groupTherapyOrdered);
        $.ajax({
            url: '/' + self.districtCode() + '/student/' + studentId + '/saveServiceRange',
            type: 'POST',
            data: dc
        })
            .done(function(response) {
                if (!!cb) {
                    cb(response);
                }
            });

    };

    $.ajax({
        url: '/' + self.districtCode() + '/student/' + studentId + '/iepServices',
        type: 'POST'
    })
        .done(function(response) {
            self.diagnosticCodeOptions(ko.utils.arrayMap(response.value.diagnosticCodeOptions,
                function(option) {
                    return new DiagnosticCodeOptionModel(self.districtCode(), option);
                }));

            //hack: either IEP services should be sorted out of mongo,
            //hack: or this should be graduated into an observable.
            //get IEP services in date descending, then group by same start date and therapy
            var temp = _.chain(response.value.services)
                .sortBy(function(s) {
                    return moment(s.startDate)
                        .format('YYYY-MM-DD') + '|' + moment(s.endDate)
                        .format('YYYY-MM-DD');
                })
                .reverse()
                .groupBy(function(s) {
                    return moment(s.startDate)
                        .format('YYYY-MM-DD');
                })
                .value();

            //order each grouping by service, alpha ascending
            _.forIn(temp, function(value, key) {
                temp[key] = _.sortBy(value, function(v) {
                    return v.service;
                });
            });

            var array = [];
            _.forIn(temp, function(value, key) {
                array = array.concat(value);
            });

            self.iepServices(ko.utils.arrayMap(array, function(a) {
                return new IEPServiceModel(self.districtCode(), self, a);
            }));
        });

    //{{{ internal diagnostic code modal model
    var InternalServiceModalModel = function() {
            var modal = this;
            modal.showErrors = ko.observable(false);
            modal.onClose = function() {
                //alert('close');
                if (!!modal.originalData()) {
                    //was not new data.  restore to original values
                    _.forIn(modal.originalData(), function(value, member) {
                        if (!ko.isComputed(modal.originalData()[member]) && ko.isObservable(modal.originalData()[member])) {
                            modal.data()[member](value());
                        }
                    });
                }
            };
            modal.onAction = function() {
                //check validity
                if (modal.data()
                    .errors()
                    .length > 0) {
                    modal.showErrors(true);
                } else {
                    //save!
                    self.saveServiceRange(ko.toJS(modal.data), function() {
                        modal.show(false);
                    });
                }
            };

            modal.show = ko.observable(false);

            modal.originalData = ko.observable();
            modal.data = ko.observable(new IEPServiceModel(self.districtCode(), self, null));
            modal.open = function(mapping) {
                modal.originalData(null);
                if (!!mapping) {
                    modal.originalData(new IEPServiceModel(self.districtCode(), self, ko.toJS(mapping)));
                    modal.data(mapping);
                }
                modal.show(true);
            };

            modal.options = ko.computed(function() {
                if (!!modal.data()) {
                    //filter options down to current options for the selection
                    var filtered = ko.utils.arrayFilter(self.diagnosticCodeOptions(), function(option) {
                        var dataStart = moment(modal.data()
                                .startDate()),
                            dataEnd = moment(modal.data()
                                .endDate()),
                            optionStart = moment(option.startDate()),
                            optionEnd = moment(option.endDate());

                        var dataRange = dataStart.twix(dataEnd);
                        var optionRange = optionStart.twix(optionEnd);

                        if (option.therapy() === modal.data()
                            .service() &&
                            dataRange.overlaps(optionRange)) {
                            return true;
                        }
                        return false;
                    });

                    //make ICDs unique
                    var retval = [];
                    var grouped = _.groupBy(filtered, function(option) {
                        return option.icd();
                    });
                    _.each(_.keys(grouped), function(key) {
                        retval.push(new DiagnosticCodeOptionModel(self.districtCode(), {
                            icd: key,
                            therapy: grouped[key][0].therapy(),
                            description: grouped[key][0].description(),
                            diagnosticCodeId: grouped[key][0].diagnosticCodeId(),
                            startDate: grouped[key][0].startDate(),
                            endDate: grouped[key][0].endDate()
                        }));
                    });
                    return retval;
                }
                return [];
            });

            return modal;
        }
        //}}} 
    self.serviceModal = new InternalServiceModalModel();
};

var IEPTeamMemberModel = function(districtCode, data) {
    var self = this;
    self.districtCode = ko.observable(districtCode);
    self.name = ko.observable(data.name);
    self.specialty = ko.observable(data.specialty);
};

var BasicInformationModel = function(districtCode, studentId) {
    var self = this;
    self.districtCode = ko.observable(districtCode);
    self.dateOfBirth = ko.observable();
    self.dateOfBirthDisplay = ko.computed(function() {
        return moment(self.dateOfBirth())
            .format('M/D/YYYY');
    });
    self.gender = ko.observable();
    self.ethnicity = ko.observable();
    self.studentId = ko.observable();
    self.medicaidNumber = ko.observable();
    self.parentalConsent = ko.observable();
    self.consentStartDate = ko.observable();
    self.consentRevoked = ko.observable();
    self.caseManagerName = ko.observable();
    self.schoolName = ko.observable();
    self.grade = ko.observable();

    self.iepTeamMembers = ko.observableArray();

    $.ajax({
        url: '/' + self.districtCode() + '/student/' + studentId + '/basicInformation',
        type: 'POST'
    })
        .done(function(response) {
            var data = response.value;
            self.dateOfBirth(data.dateOfBirth);
            self.gender(data.gender);
            self.ethnicity(data.ethnicity);
            self.studentId(data.studentId);
            self.medicaidNumber(data.medicaidNumber);
            self.parentalConsent(data.parentalConsent);
            self.consentStartDate = ko.observable(data.consentStartDate);
            self.consentRevoked(data.consentRevoked);
            self.caseManagerName(data.caseManagerName);
            self.schoolName(data.schoolName);
            self.grade(data.grade);
            self.iepTeamMembers(ko.utils.arrayMap(data.iepTeamMembers, function(m) {
                return new IEPTeamMemberModel(self.districtCode(), m);
            }));
        });
};


var StudentContainerModel = function(districtCode, internalStudentId, studentId, studentName) {
    var self = this;
    self.studentName = ko.observable(studentName);
    self.studentId = ko.observable(studentId);
    self.districtCode = ko.observable(districtCode);
    self.selected = ko.observable('Basic Information');
    self.basicInformation = ko.observable(new BasicInformationModel(self.districtCode(), internalStudentId));
    self.iepServices = ko.observableArray([]);
    self.additionalInformationWrapper = ko.observable(new AdditionalInfoWrapper(self.districtCode(), internalStudentId));
    self.iepServicesWrapper = ko.observable(new IEPServiceModelWrapper(self.districtCode(), internalStudentId));
    self.studentHistory = ko.observable(new StudentHistoryWrapperModel(self.districtCode(), internalStudentId));
    self.loading = ko.observable(true);

};
