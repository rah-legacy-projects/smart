ko.validation.configure({
    registerExtenders: true,
    messagesOnModified: true,
    insertMessages: false,
    parseInputAttributes: true,
    messageTemplate: null
});

var PermissionsEdit = function(data) {
    console.log(data);
    var self = this;
    console.log(data.possiblePermissions);
    self.possiblePermissions = ko.observableArray(data.possiblePermissions);
    self.selectedPermissions = ko.observableArray(data.permissions);
    self.userId = ko.observable(data.userId);
    if ( !! data.district) {
        self.name = ko.observable(data.district.name);
        self.districtCode = ko.observable(data.district.districtCode);
        self.districtId = ko.observable(data.district._id);
        self.selectedLocationType = ko.observable('district');
    } else if ( !! data.school) {
        self.name = ko.observable(data.school.name);
        self.schoolId = ko.observable(data.school._id);
        self.selectedLocationType = ko.observable('school');
    }

    //validations
    self.selectedPermissions.extend({
        required: true
    });

    self.errors = ko.validation.group(self);
    self.showSubmitError = ko.observable(false);
    self.submit = function() {
        self.showSubmitError(false);
        if (self.errors()
            .length > 0) {
            self.showSubmitError(true);
        } else {
            var submission = {
                userId: self.userId(),
                selectedPermissions: self.selectedPermissions()
            };

            if (!!self.schoolId) {
                submission.schoolId = self.schoolId();
            } else if (!!self.districtId) {
                submission.districtId = self.districtId();
            }

            //console.log(submission);

            $.ajax({
                url: '/admin/userPermission',
                type: 'POST',
                data: submission
            })
                .done(function(response) {
                    console.log('done!');
                    window.location = '/admin/' + self.userId() + '/permissions';
                });
        }
    };

};
