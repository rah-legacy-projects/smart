var ExportBillingModel = function(data) {
    var self = this;
    //default values: first and last of current month
    if (!data) {
        data = {
            startDate: moment()
                .startOf('month'),
            endDate: moment()
                .endOf('month')
        };
    }
    self.startDateEdit = ko.observable(data.startDate);
    self.endDateEdit = ko.observable(data.endDate);

    self.startDate = ko.computed(function() {
        var start = self.startDateEdit();
        var sod = moment(start)
            .startOf('day');
        console.log(sod.toString());
        return sod;
    });
    self.endDate = ko.computed(function() {
        var end = self.endDateEdit();
        var eod = moment(end)
            .endOf('day');
        console.log(eod.toString());
        return eod;
    });




    self.startOfDay = ko.observable(moment()
        .startOf('day')
        .toDate());
    self.endOfDay = ko.observable(moment()
        .endOf('day')
        .toDate());

    self.isaDate = ko.observable(moment()
        .format('YYMMDD'));
    self.gsDate = ko.observable(moment()
        .format('YYYYMMDD'));
    self.bhtDate = ko.observable(moment()
        .format('YYYYMMDD'));
    self.time = ko.observable(moment()
        .format('HHmm'));

    self.timezone = ko.observable(tzdetect.matches()[0]);

    console.log('the timezone here is: ' + self.timezone()); 

};
