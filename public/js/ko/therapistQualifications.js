var TherapistQualificationEditModel = function(data) {
    var self = this;
    self.userId = ko.observable(data.user._id);
    data.therapistQualification = data.therapistQualification || {};
    self.therapistQualificationId = ko.observable(data.therapistQualification._id);
    self.startDate = ko.observable(moment(data.therapistQualification.startDate).format("M/D/YYYY"));
    self.endDate = ko.observable(moment(data.therapistQualification.endDate).format("M/D/YYYY"));
    self.npiNumber = ko.observable(data.therapistQualification.npiNumber);
    self.providerType = ko.observable(data.therapistQualification.providerType);
    self.licensed = ko.observable(data.therapistQualification.licensed);
    self.name = ko.observable(data.user.firstName + " " + data.user.lastName);

    //validations
    self.startDate.extend({
        date: true,
        required: true
    });
    self.endDate.extend({
        date: true,
        required: true
    });
    self.npiNumber.extend({required:true});
    self.providerType.extend({required:true});
    self.licensed.extend({required:true});

    self.errors = ko.validation.group(self);
    self.showSubmitError = ko.observable(false);
    self.submit = function() {
        self.showSubmitError(false);
        if (self.errors()
            .length > 0) {
            self.showSubmitError(true);
        } else {

            console.log(ko.toJSON(self));

            $.ajax({
                url: '/admin/therapistQualification',
                type: 'POST',
                data: JSON.parse(ko.toJSON(self))
            })
                .done(function(response) {
                    console.log('done!');
                    //self.id(response.updatedUser.id);
                    window.location = '/admin/' + self.userId() + '/therapistQualifications';
                });
        }
    };
};

var TherapistQualificationsViewModel = function(data) {
    var self = this;
    self.startDate = ko.observable(moment(data.startDate).format('M/D/YYYY'));
    self.endDate = ko.observable(moment(data.endDate).format('M/D/YYYY'));
    self.id = ko.observable(data._id);
    self.deleted= ko.observable(false);
    self.remove = function(){
        $.ajax({
            url:'/admin/revokeTherapistQualification',
            type:'POST',
            data:{therapistQualificationId: self.id()}
        }).done(function(response){
            self.deleted(true);
        });
    };

}

var TherapistQualificationsViewContainer = function(data) {
    var self = this;
    console.log(JSON.stringify(data));
    self.name = ko.observable(data.user.firstName + ' ' + data.user.lastName);
    self.userId = ko.observable(data.user._id);
    self.therapistQualificationsRaw = ko.observableArray(ko.utils.arrayMap(data.user.therapistQualifications, function(t) {
        return new TherapistQualificationsViewModel(t);
    }));
    self.therapistQualifications = ko.computed(function(){
        return ko.utils.arrayFilter(self.therapistQualificationsRaw(), function(t){
            return !t.deleted();
        });
    });

    console.log(self.therapistQualificationsRaw().length);
    console.log(self.therapistQualifications().length);

};
