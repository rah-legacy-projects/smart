var ProcedureCodeModel = function(data) {
    var self = this;
    self.procedureCode = ko.observable(data.procedureCode);
    self.description = ko.observable(data.description);
    self.procedureCodeId = ko.observable(data.procedureCodeId);

    self.display = ko.computed(function() {
        return ("{procedureCode} - {description}")
            .namedFormat({
                procedureCode: self.procedureCode(),
                description: self.description()
            });
    });
};


var AttendeeModel = function(data) {
    var self = this;
    self.name = ko.observable(data.name);
    self.studentId = ko.observable(data.studentId);
    self.internalStudentId = ko.observable(data.internalStudentId);
    self.dateOfBirth = ko.observable(data.dateOfBirth);
    self.absent = ko.observable(data.absent || false);
    self.absenceReason = ko.observable(data.absenceReason);
    self.individualNote = ko.observable(data.individualNote);
    self.serviceId = ko.observable(data.serviceId);
    self.serviceType = ko.observable(data.serviceType);
    self.frequencyAndDuration = ko.observable(data.frequencyAndDuration);
    self.diagnosticCode = ko.observable(data.diagnosticCode);
    self.diagnosticCodeId = ko.observable(data.diagnosticCodeId);

    self.absentMonitor = ko.computed(function() {
        if (!self.absent()) {
            self.absenceReason('');
        }
    });

    self.selectedProgress = ko.observable('Select One...');

    self.dirty = new ko.dirtyFlag(self, false);
    self.isActuallyDirty = ko.computed(function() {
        var x = self.dirty.isMemberDirty('individualNote') ||
            self.dirty.isMemberDirty('absent') ||
            self.dirty.isMemberDirty('absenceReason');
        console.log('attendee dirty: ' + x);
        return x;
    });

    self.resetDirty = function() {
        self.dirty.reset();
    };


    //validations
    /*self.absenceReason.extend({
        required: {
            onlyIf: function() {
                return self.absent();
            }
        },
        minLength: 5
    });*/
    self.errors = ko.validation.group(self);
};
var SessionNotesModel = function(districtCode, data, parent) {
    var self = this;
    self.districtCode = ko.observable(districtCode);
    if (!data) data = {};

    self.providerName = ko.observable(data.providerName);
    console.log('#### provider id: ' + data.providerId);
    self.providerId = ko.observable(data.providerId);
    self.serviceType = ko.observable(data.serviceType);
    self.procedureCodeId = ko.observable(data.procedureCodeId);
    self.sessionNotes = ko.observable(data.sessionNotes);
    self.previous = ko.observable(data.previous);
    self.next = ko.observable(data.next);

    self.finalize = ko.observable(!!data.finalizingUser);
    self.readOnly = ko.observable(!!data.finalizingUser);
    self.billed = ko.observable(data.billed);

    self.attendees = ko.observableArray(ko.utils.arrayMap(data.attendees, function(a) {
        return new AttendeeModel(a);
    }));



    self.dirty = new ko.dirtyFlag(self, false);
    self.actuallyDirty = ko.computed(function() {
        var x = self.dirty.isMemberDirty('sessionNotes') ||
            self.dirty.isMemberDirty('finalize') ||
            self.dirty.isMemberDirty('procedureCodeId') ||
            self.dirty.isMemberDirty('attendees') ||
            _.any(self.attendees(), function(a) {
                return a.isActuallyDirty();
            });
        console.log('session model actually dirty: ' + x);
        return x;
    });
    self.resetDirty = function() {
        self.dirty.reset();
        _.each(self.attendees(), function(a) {
            a.resetDirty();
        });
    };

    self.showSchedule = ko.observable(false);

    //use scheduling's schedule model
    self.schedule = ko.observable(new ScheduleModel(data.schedule, {
        validateEnd: false,
        validateRepeatBy: false
    }));

    console.log('procedure code display: ' + JSON.stringify(data.procedureCodeDisplay));
    if (!!data.procedureCodeDisplay) {
        self.finalizedProcedureCodeDisplay = ko.observable(new ProcedureCodeModel(data.procedureCodeDisplay));
    } else {
        self.finalizedProcedureCodeDisplay = ko.observable(null);
    }

    //make a display for the selected procedure code
    self.selectedProcedureCode = ko.computed(function() {
        var x = self.procedureCodeId();
        var p = _.find(parent.procedureCodeOptions(), function(pco) {
            return pco.procedureCodeId() === self.procedureCodeId();
        });
        if (!!self.finalizedProcedureCodeDisplay()) {
            console.log('using procedure code display.')
            return self.finalizedProcedureCodeDisplay()
                .display();
        } else if (!p) {
            console.log('not p');
            return 'Please Select One';
        } else {
            console.log('using p display');
            return p.display();
        }
    });


    //validations
    /*self.procedureCodeId.extend({
        required: true,
        onlyIf: parent.procedureCodeOptions().length > 0
    });*/
    self.sessionNotes.extend({
        required: {
            params: true,
            onlyIf: function() {
                return self.finalize();
            }
        }
    })
        .extend({
            minLength: {
                params: 25,
                onlyIf: function() {
                    console.log('minlength onlyif')
                    return self.finalize();
                }
            }
        });

    self.procedureCodeId.extend({
        required: {
            params: true,
            onlyIf: function() {
                return self.finalize();
            }
        }
    });

    self.errors = ko.validation.group(self);

    self.allAttendeesHaveDiagnosticCodes = ko.computed(function() {
        var x = _.all(self.attendees(), function(a) {
            return a.diagnosticCode() !== 'Not Selected';
        });
        return x;
    });

    self.attendeesValid = ko.computed(function() {
        var x = _.all(self.attendees(), function(a) {
            return true;
        });
        console.log('\t\t\t attendee length: ' + self.attendees().length);
        return x && self.attendees()
            .length > 0;
    });

    self.isValid = ko.computed(function() {
        self.attendeesValid();
        console.log('#errors: ' + self.errors()
            .length);

        console.log('attendees valid: ' + self.attendeesValid());
        if (self.errors()
            .length > 0) return false;

        if (self.schedule()
            .errors()
            .length > 0) return false;

        if (_.filter(self.attendees(), function(a) {
                return a.errors()
                    .length > 0;
            })
            .length > 0) return false;

        if (!self.allAttendeesHaveDiagnosticCodes() && self.finalize()) {
            return false;
        }

        if (!self.attendeesValid()) {
            console.log('attendees not valid!!');
            return false;
        }

        return true;
    });

    //{{{ student modal model
    var InternalStudentModalModel = function() {
        var modal = this;
        modal.onClose = function() {
            modal.selectedAttendees([]);
            modal.selectedPossibleAttendees([]);
        };

        modal.onAction = function() {

            modal.selectedAttendees([]);
            modal.selectedPossibleAttendees([]);


            //extract ids
            var selectedIds = _.map(modal.selectedStudents(), function(s) {
                return s.studentId();
            });
            var attendeeIds = _.map(self.attendees(), function(s) {
                return s.studentId();
            });
            var notUsedIds = _.map(modal.possibleStudentsFiltered(), function(s) {
                return s.studentId();
            });

            //get the new attendees
            var newAttendees = _.difference(selectedIds, attendeeIds);

            //add 
            _.each(newAttendees, function(newAttendeeId) {
                var newAttendee = _.find(modal.selectedStudents(), function(s) {
                    return s.studentId() == newAttendeeId;
                })
                self.attendees.push(new AttendeeModel(ko.toJS(newAttendee)));
            });

            //get removed attendees
            var removedAttendees = _.intersection(notUsedIds, attendeeIds);
            var actualAttendees = _.filter(self.attendees(), function(attendee) {
                return !_.contains(removedAttendees, attendee.studentId());
            });
            self.attendees(actualAttendees);




            //self.attendees(ko.utils.arrayMap(ko.toJS(modal.selectedStudents), function(ss) {
            //    return new AttendeeModel(ss);
            //}));
            modal.show(false);
        };

        modal.show = ko.observable(false);
        modal.selectedStudents = ko.observableArray([]);
        modal.possibleStudents = ko.observableArray([]);
        modal.possibleStudentsFiltered = ko.computed(function() {
            var selectedIds = _.map(modal.selectedStudents(), function(ss) {
                return ss.serviceId();
            });
            return _.filter(modal.possibleStudents(), function(ps) {
                return !_.contains(selectedIds, ps.serviceId());
            });
        });


        modal.selectedPossibleAttendees = ko.observable([]);
        modal.selectedAttendees = ko.observable([]);
        modal.togglePossibleAttendeeSelection = function(id) {
            var unwrapped = modal.selectedPossibleAttendees();
            var i = unwrapped.indexOf(id);
            if (i != -1) {
                unwrapped.splice(i, 1);
            } else {
                unwrapped.push(id);
            }
            modal.selectedPossibleAttendees(unwrapped);
        };
        modal.toggleAttendeeSelection = function(id) {
            if (modal.selectedStudents()
                .length == 1) {
                return;
            }
            var unwrapped = modal.selectedAttendees();
            var i = unwrapped.indexOf(id);
            if (i != -1) {
                unwrapped.splice(i, 1);
            } else {
                unwrapped.push(id);
            }
            modal.selectedAttendees(unwrapped);
        };

        modal.movePossible = function() {
            //get the selected service ids
            //filter possibles to those ids
            //add those to selected
            var selected = _.filter(ko.toJS(modal.possibleStudents()), function(ps) {
                var ids = modal.selectedPossibleAttendees();
                return ids.indexOf(ps.serviceId) > -1;
            });

            _.each(selected, function(s) {
                modal.selectedStudents.push(new AttendeeModel(s));
            });
            modal.selectedPossibleAttendees([]);
        };
        modal.removeAttendees = function() {
            if (modal.selectedAttendees()
                .length == modal.selectedStudents()
                .length) {
                return;
            }

            var ids = modal.selectedAttendees();
            var notSelected = _.filter(ko.toJS(modal.selectedStudents()), function(ps) {
                return ids.indexOf(ps.serviceId) === -1;
            });
            modal.selectedStudents([]);
            _.each(notSelected, function(ns) {
                modal.selectedStudents.push(new AttendeeModel(ns));
            });
            modal.selectedAttendees([]);
        };

        modal.loading = ko.observable(true);
        modal.open = function(root) {
            //show the modal
            modal.show(true);
            modal.loading(true);
            //get possible students
            var service = null;
            if (self.attendees()
                .length > 0) {
                service = self.attendees()[0].serviceType();
            }else{
                service = !!root.service ? root.service() : null
            }

            var postdata = {
                serviceIds: _.map(self.attendees(), function(s) {
                    return s.serviceId();
                }),
                service: service,
                date: root.sessionNotes()
                    .schedule()
                    .dateTime()
            };
            $.ajax({
                url: '/' + self.districtCode() + '/schedule/getServices',
                type: 'POST',
                data: postdata
            })
                .done(function(response) {


                    //make possible students
                    modal.possibleStudents(ko.utils.arrayMap(response.value, function(v) {
                        return new AttendeeModel(v);
                    }));

                    //clone current students into data
                    modal.selectedStudents(ko.utils.arrayMap(ko.toJS(self.attendees), function(s) {
                        return new AttendeeModel(_.clone(s));
                    }));

                    modal.loading(false);
                });
        };

        return modal;
    };
    //}}}
    self.attendeesModal = new InternalStudentModalModel();

};

var SessionNotesWrapperModel = function(districtCode, scheduleId) {
    var self = this;
    self.loading = ko.observable(true);
    self.districtCode = ko.observable(districtCode);
    var tasks = {
        sessionNotes: function(callback) {
            $.ajax({
                url: '/' + self.districtCode() + '/session/' + scheduleId,
                type: 'POST'
            })
                .done(function(response) {
                    if (!!response.err) {
                        console.error(response.err);
                    }
                    console.log('session notes responded');
                    callback(response.err, response.value);
                });
        },
        procedureCodes: function(callback) {
            $.ajax({
                url: '/' + self.districtCode() + '/procedureCodes/list',
                type: 'POST',
                data: {
                    scheduleId: scheduleId,
                }
            })
                .done(function(response) {
                    if (!!response.err) {
                        console.error(response.err);
                    }
                    console.log('proc codes responded');
                    callback(response.err, response.value);
                });
        }
    };

    self.procedureCodeOptions = ko.observableArray([]);
    self.sessionNotes = ko.observable(new SessionNotesModel(self.districtCode(), null, self));
    self.showSuccess = ko.observable(false);
    self.updater = ko.computed(function() {
        ko.toJSON(self.sessionNotes());
        self.showSuccess(false);
    });


    self.locked = ko.computed(function() {
        console.log('locked: ' + self.showSuccess() + " && " + self.sessionNotes()
            .finalize());
        return self.showSuccess() && self.sessionNotes()
            .finalize();
    });

    self.resetDirty = function() {
        self.dirty.reset();
        self.sessionNotes()
            .resetDirty();
    };

    self.showErrors = ko.observable(false);
    console.log('about to parallel it up');
    async.parallel(tasks, function(err, r) {
        console.log('tasks complete');
        console.error(err);
        self.procedureCodeOptions(ko.utils.arrayMap(r.procedureCodes, function(pc) {
            return new ProcedureCodeModel(pc);
        }));
        //note: session notes must be made last as they depend on self having procedure codes
        self.sessionNotes(new SessionNotesModel(self.districtCode(), r.sessionNotes, self));
        console.log('session notes made');

        self.dirty = new ko.dirtyFlag(self, false);
        self.actuallyDirty = ko.computed(function() {
            var x = self.sessionNotes()
                .actuallyDirty();
            console.log('wrapper dirty: ' + x);
            return x;
        });
        self.resetDirty();
        console.log('done loading.');
        self.loading(false);
    });

    self.procedureCodeOptionsWatcher = ko.computed(function() {
        console.log('procedure code options watcher tripped');
        var date = self.sessionNotes()
            .schedule()
            .dateTime();
        $.ajax({
            url: '/' + self.districtCode() + '/procedureCodes/list',
            type: 'POST',
            data: {
                scheduleId: scheduleId,
                date: date
            }
        })
            .done(function(response) {
                self.procedureCodeOptions(ko.utils.arrayMap(response.value, function(pc) {
                    return new ProcedureCodeModel(pc);
                }));

                if (_.any(self.procedureCodeOptions(), function(opt) {
                    return self.sessionNotes()
                        .procedureCodeId() == opt.procedureCodeId()
                })) {
                    self.sessionNotes()
                        .procedureCodeId(null);
                }
            });
    });

    self.attendeesWatcher = ko.computed(function() {
        var date = self.sessionNotes()
            .schedule()
            .dateTime();
        var service = null;
        if (self.sessionNotes()
            .attendees.peek()
            .length > 0) {
            if (!self.service) {
                console.log('making service observable');
                self.service = ko.observable(self.sessionNotes()
                    .attendees.peek()[0].serviceType());
            }
        }

        var postdata = {
            serviceIds: _.map(self.sessionNotes()
                .attendees.peek(), function(s) {
                    return s.serviceId();
                }),
            service: !!self.service ? self.service() : null,
            date: date
        };
        $.ajax({
            url: '/' + self.districtCode() + '/schedule/getServices',
            type: 'POST',
            data: postdata
        })
            .done(function(response) {
                //filter out attendees that are no longer valid
                var possibleStudents = ko.utils.arrayMap(response.value, function(v) {
                    return new AttendeeModel(v);
                });
                console.log('possible students: ' + possibleStudents.length);

                var filteredStudents = _.filter(self.sessionNotes()
                    .attendees.peek(), function(a) {
                        return _.any(possibleStudents, function(p) {
                            return a.serviceId() == p.serviceId();
                        });
                    });
                console.log('attendees: ' + self.sessionNotes()
                    .attendees.peek()
                    .length);
                console.log('filtered students: ' + filteredStudents.length);

                self.sessionNotes()
                    .attendees(filteredStudents);

            });

    });



    self.saving = ko.observable(false);
    self.save = function(cb) {
        if (self.sessionNotes()
            .readOnly()) {
            cb(null, true);
            return;
        }
        self.showSuccess(false);
        if (self.saving()) {
            return;
        }
        if (self.locked()) {
            return;
        }

        if (!self.actuallyDirty()) {
            console.log('nothing changed. skipping save');
            if (!!cb) {
                cb(null, true);
            }
            return;
        }

        self.saving(true);
        console.log('save called, valid: ' + self.sessionNotes()
            .isValid());
        if (!self.sessionNotes()
            .isValid()) {
            self.showErrors(true);
            self.saving(false);

            if (!self.sessionNotes()
                .schedule()
                .isValid()) {
                self.sessionNotes()
                    .showSchedule(true);
            }

        } else {
            self.showErrors(false);
            self.resetDirty();
            console.log('saving to: /session/' + self.sessionNotes()
                .schedule()
                .scheduleId() + '/save');
            var savedata = JSON.parse(ko.toJSON(self.sessionNotes));
            $.ajax({
                url: '/' + self.districtCode() + '/session/' + self.sessionNotes()
                    .schedule()
                    .scheduleId() + '/save',
                type: 'POST',
                data: savedata
            })
                .done(function(response) {
                    console.log(JSON.stringify(response));
                    console.log('save success');
                    self.sessionNotes()
                        .providerId(response.value.providerId);
                    self.sessionNotes()
                        .providerName(response.value.providerName);

                    self.showSuccess(true)
                    if (self.sessionNotes()
                        .finalize()) {
                        location.reload();
                    } else if (!!cb) {
                        console.log('cb');
                        self.saving(false);
                        cb(response.err, response.value);
                    } else {
                        console.log('not cb');
                        self.saving(false);
                    }
                })
        }

    };

    self.unfinalize = function() {
        //hack: set read-only false
        self.sessionNotes()
            .readOnly(false);
        self.sessionNotes()
            .finalize(false);
        self.save(function(response) {
            window.location.reload();
        });
    };

    //navigation
    self.previous = function() {
        self.save(function(response) {
            window.location = '/' + self.districtCode() + '/session/' + self.sessionNotes()
                .previous();
        });
    };

    self.next = function() {
        //todo
        self.save(function(response) {
            console.log('going to ' + '/' + self.districtCode() + '/session/' + self.sessionNotes());

            window.location = '/' + self.districtCode() + '/session/' + self.sessionNotes()
                .next();
        });
    };




}
