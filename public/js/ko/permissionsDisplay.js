var SchoolPermissionsDisplay = function(data) {
    var self = this;
    self.schoolId = ko.observable(data._id);
    self.name = ko.observable(data.name);
    self.schoolPermissions = ko.observableArray(data.schoolPermissions);
    self.schoolPermissionsDisplay = ko.computed(function() {
        if (self.schoolPermissions()
            .length == 0) {
            return '';
        }
        return "Permissions: " + self.schoolPermissions()
            .join(', ');
    });
    self.deleted = ko.observable(false);
    self.show = ko.computed(function() {
        console.log('school perms at ' + self.name() + ': ' + self.schoolPermissions()
            .length);
        return self.schoolPermissions()
            .length > 0 && !self.deleted();
    });

    self.revoke = function(userId) {
        $.ajax({
            url: "/admin/revokePermission",
            type: 'POST',
            data: {
                schoolId: self.schoolId(),
                userId: userId
            }
        })
            .done(function(result) {
                //hack: this is cheating - should actually filter itself from the parent
                self.deleted(true);
            });
    };
    return self;
};
var DistrictPermissionsDisplay = function(data) {
    var self = this;
    self.districtId = ko.observable(data._id);
    self.name = ko.observable(data.name);
    self.districtCode = ko.observable(data.districtCode);
    self.districtPermissions = ko.observableArray(data.districtPermissions);
    self.districtPermissionsDisplay = ko.computed(function() {
        if (self.districtPermissions()
            .length == 0) {
            return '';
        }
        return "Permissions: " + self.districtPermissions()
            .join(', ');
    });
    self.schools = ko.observableArray(ko.utils.arrayMap(data.schools, function(s) {
        return new SchoolPermissionsDisplay(s);
    }));
    self.shownSchools = ko.computed(function() {
        return ko.utils.arrayFilter(self.schools(), function(s) {
            return s.show();
        });
    });

    self.deleted = ko.observable(false);
    self.show = ko.computed(function() {
        var t = (self.shownSchools()
            .length > 0 || self.districtPermissions()
            .length > 0);

        console.log('shown schools: ' + (self.shownSchools()
            .length > 0));
        console.log('dist perms: ' + (self.districtPermissions()
            .length > 0));
        console.log('deld: ' + (!self.deleted()));

        console.log('show district ' + self.name() + ': ' + t);
        return t;
    });

    self.revoke = function(userId) {
        $.ajax({
            url: "/admin/revokePermission",
            type: 'POST',
            data: {
                districtId: self.districtId(),
                userId: userId
            }
        })
            .done(function(result) {
                //hack: this is cheating - should actually filter itself from the parent
                self.deleted(true);
                self.districtPermissions([]);
            });
    };
};

var PermissionsDisplay = function(data) {
    var self = this;

    console.log(data);
    self.name = ko.observable(data.user.first + ' ' + data.user.last);
    self.userId = ko.observable(data.user._id);
    self.userName = ko.observable(data.user.firstName + " " + data.user.lastName);
    self.districts = ko.observableArray(ko.utils.arrayMap(data.permissions.districts, function(d) {
        return new DistrictPermissionsDisplay(d);
    }));

    self.shownDistricts = ko.computed(function() {
        return ko.utils.arrayFilter(self.districts(), function(d) {
            return d.show();
        });
    });
};
