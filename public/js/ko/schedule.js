var EndsModel = function(data, validate) {
    console.log('validate: ' + validate);
    var self = this;
    if (!data) {
        //use empty
        data = {};
    }
    self.never = ko.observable(data.never);
    self.occurrences = ko.observable(data.occurrences);
    self.endDate = ko.observable(data.endDate);
    self.endType = ko.observable('');

    self.endType.extend({
        required: {
            onlyIf: function() {
                return validate;
            },
            message: 'check end type'
        }
    });
    self.occurrences.extend({
        required: {
            onlyIf: function() {
                return self.endType() === 'Occurrences' && validate;
            }
        },
        pattern: {
            onlyIf: function() {
                console.log('checking regex: ' + self.endType() + ' and ' + validate);
                return self.endType() === 'Occurrences' && validate;
            },
            params: /^\d+$/,
            message: 'check occurrences, it does not match the regex'
        }
    });
    self.endDate.extend({
        required: {
            onlyIf: function() {
                return validate && self.endType() === 'On';
            },
            message: 'check end date'
        }
    });

    self.errors = ko.validation.group(self);

    self.dump = ko.computed(function() {
        console.log(ko.toJSON(self.errors()));
        return ko.toJSON(self.errors());
    });
};

var ScheduleModel = function(data, validationOptions) {

    var self = this;
    if (!data) {
        //use empty
        data = {};
    }

    self.scheduleId = ko.observable(data._id);
    self.dateTime = ko.observable(moment(data.dateTime).roundNext15Min()
        .toDate());
    self.date = ko.computed({
        read: function() {
            return moment(self.dateTime())
                .format('M/D/YYYY');
        },
        write: function(value) {
            var valueMoment = moment(value);
            var selfMoment = moment(self.dateTime());
            selfMoment.month(valueMoment.month())
                .date(valueMoment.date())
                .year(valueMoment.year());
            self.dateTime(selfMoment.toDate());
        }
    });

    //hack: put the day of month/day of week of month display in schedule
    self.dayOfMonthDisplay = ko.computed(function() {
        return moment(self.dateTime())
            .format('Do [of the month]');
    });
    self.dayOfWeekInMonthDisplay = ko.computed(function() {
        var selfMoment = moment(self.dateTime());
        var week = Math.ceil(selfMoment.date() / 7);
        if (week == 1) week = '1st';
        else if (week == 2) week = '2nd';
        else if (week == 3) week = '3rd';
        else week = '4th';

        return week + ' ' + selfMoment.format('dddd [of the month]');
    });

    self.startTime = ko.computed({
        read: function() {
            return moment(self.dateTime())
                .format('h:mm a');
        },
        write: function(value) {
            var valueMoment = moment(value, 'h:mm a');
            var selfMoment = moment(self.dateTime());
            selfMoment.hour(valueMoment.hour())
                .minute(valueMoment.minute());
            self.dateTime(selfMoment.toDate());
        }
    });
    self.duration = ko.observable(data.duration);
    self.location = ko.observable(data.location);
    console.log('validation options validate end: ' + validationOptions.validateEnd);
    self.ends = ko.observable(new EndsModel(data.ends, validationOptions.validateEnd));
    self.repeatBy = ko.observableArray(data.repeatBy);
    //todo: repeat every

    self.repeatbyModify = function(item, singleOnly) {
        singleOnly = singleOnly || false;
        var rba = self.repeatBy();
        var i = rba.indexOf(item);

        if (singleOnly) {
            rba = [];
            rba.push(item);
            self.repeatBy(rba);
        } else {
            //if the repeat by has the item, remove it; otherwise, add it
            if (i != -1) {
                rba.splice(i, 1);
            } else {
                rba.push(item);
            }
            self.repeatBy(rba);
        }
    };

    //week computeds
    self.hasMonday = ko.computed(function() {
        return self.repeatBy()
            .indexOf('monday') != -1;
    });
    self.hasTuesday = ko.computed(function() {
        return self.repeatBy()
            .indexOf('tuesday') != -1;
    });
    self.hasWednesday = ko.computed(function() {
        return self.repeatBy()
            .indexOf('wednesday') != -1;
    });
    self.hasThursday = ko.computed(function() {
        return self.repeatBy()
            .indexOf('thursday') != -1;
    });
    self.hasFriday = ko.computed(function() {
        return self.repeatBy()
            .indexOf('friday') != -1;
    });
    self.hasSaturday = ko.computed(function() {
        return self.repeatBy()
            .indexOf('saturday') != -1;
    });
    self.hasSunday = ko.computed(function() {
        return self.repeatBy()
            .indexOf('sunday') != -1;
    });

    //month computeds
    self.hasDayOfMonth = ko.computed(function() {
        return self.repeatBy()
            .indexOf('day of month') != -1;
    });
    self.hasDayOfWeek = ko.computed(function() {
        return self.repeatBy()
            .indexOf('day of week') != -1;
    });

    //validations
    self.duration.extend({
        required: true
    }).extend({min: 15});

    self.ends()
        .endType.extend({
            required: {
                onlyIf: function() {
                    return !!validationOptions.validateEnd;
                }
            }
        });
    self.repeatBy.extend({
        minLength: {
            onlyIf: function() {
                return !!validationOptions.validateRepeatBy;
            },
            params: 1
        }
    });

    self.errors = ko.validation.group(self);

    self.isValid = ko.computed(function() {
        console.log(validationOptions.validateRepeatBy);
        console.log(ko.toJSON(self.repeatBy));
        console.log(ko.toJSON(self.errors));
        console.log('\t' + 'schedule validity: ' + (self.errors()
            .length == 0));
        console.log('\t' + 'end validity: ' + (validationOptions.validateEnd && self.ends()
            .errors()
            .length == 0));
        if (self.errors()
            .length > 0) return false;
        if (validationOptions.validateEnd && self.ends()
            .errors()
            .length > 0) return false;
        return true;
    });
};

var StudentModel = function(data) {
    var self = this;
    self.name = ko.observable(data.name);
    self.studentId = ko.observable(data.studentId);
    self.serviceType = ko.observable(data.serviceType);
    self.frequencyAndDuration = ko.observable(data.frequencyAndDuration);
    self.serviceId = ko.observable(data.serviceId);
    self.diagnosticCode = ko.observable(data.diagnosticCode);

};

var ScheduleContainerModel = function(districtCode, data) {
    var self = this;
    if (!data) {
        //treat as empty
        data = {};
    }
    self.providerName = ko.observable(data.providerName);
    self.providerId = ko.observable(data.providerId);
    self.serviceName = ko.observable(data.serviceName);
    self.seriesId = ko.observable(data.seriesId);
    self.scheduleId = ko.observable(data.scheduleId);
    self.districtCode = ko.observable(districtCode);

    self.students = ko.observableArray(ko.utils.arrayMap(data.services, function(s) {
        return new StudentModel(s);
    }));

    var InternalStudentModalModel = function() {
        var modal = this;
        modal.onClose = function() {
            modal.selectedAttendees([]);
            modal.selectedPossibleAttendees([]);
        };

        modal.onAction = function() {
            
            modal.selectedAttendees([]);
            modal.selectedPossibleAttendees([]);
            self.students(ko.utils.arrayMap(ko.toJS(modal.selectedStudents), function(ss){return new StudentModel(ss);}));
            modal.show(false);
        };

        modal.show = ko.observable(false);
        modal.selectedStudents = ko.observableArray([]);
        modal.possibleStudents = ko.observableArray([]);
        modal.possibleStudentsFiltered = ko.computed(function() {
            var selectedIds = _.map(modal.selectedStudents(), function(ss) {
                return ss.serviceId();
            });
            return _.filter(modal.possibleStudents(), function(ps) {
                return !_.contains(selectedIds, ps.serviceId());
            });
        });


        modal.selectedPossibleAttendees = ko.observable([]);
        modal.selectedAttendees = ko.observable([]);
        modal.togglePossibleAttendeeSelection = function(id) {
            var unwrapped = modal.selectedPossibleAttendees();
            var i = unwrapped.indexOf(id);
            if (i != -1) {
                unwrapped.splice(i, 1);
            } else {
                unwrapped.push(id);
            }
            modal.selectedPossibleAttendees(unwrapped);
        };
        modal.toggleAttendeeSelection = function(id) {
            if(modal.selectedStudents().length == 1){
                return;
            }
            var unwrapped = modal.selectedAttendees();
            var i = unwrapped.indexOf(id);
            if (i != -1) {
                unwrapped.splice(i, 1);
            } else {
                unwrapped.push(id);
            }
            modal.selectedAttendees(unwrapped);
        };

        modal.movePossible = function() {
            //get the selected service ids
            //filter possibles to those ids
            //add those to selected
            var selected = _.filter(ko.toJS(modal.possibleStudents()), function(ps) {
                var ids = modal.selectedPossibleAttendees();
                return ids.indexOf(ps.serviceId) > -1;
            });

            _.each(selected, function(s){
                modal.selectedStudents.push(new StudentModel(s));
            });
            modal.selectedPossibleAttendees([]);
        };
        modal.removeAttendees = function(){
            if(modal.selectedAttendees().length == modal.selectedStudents().length){
                return;
            }
            var ids= modal.selectedAttendees();
            var notSelected = _.filter(ko.toJS(modal.selectedStudents()), function(ps){
                return ids.indexOf(ps.serviceId) === -1;
            });
            modal.selectedStudents([]);
            _.each(notSelected, function(ns){
                modal.selectedStudents.push(new StudentModel(ns));
            });
            modal.selectedAttendees([]);
        };

        modal.loading = ko.observable(true);
        modal.open = function() {
            //show the modal
            modal.show(true);
            modal.loading(true);
            //get possible students
            var service = null;
            if (self.students()
                .length > 0) {
                service = self.students()[0].serviceType();
            }
            var data = {
                serviceIds: _.map(self.students(), function(s) {
                    return s.serviceId();
                }),
                service: service
            };
            $.ajax({
                url: '/'+self.districtCode()+'/schedule/getServices',
                type: 'POST',
                data: data
            })
                .done(function(response) {
                    //make possible students
                    modal.possibleStudents(ko.utils.arrayMap(response.value, function(v) {
                        return new StudentModel(v);
                    }));

                    //clone current students into data
                    modal.selectedStudents(ko.utils.arrayMap(ko.toJS(self.students), function(s) {
                        return new StudentModel(_.clone(s));
                    }));

                    modal.loading(false);
                });
        };

        return modal;
    };

    self.attendeesModal = new InternalStudentModalModel();

    //navigation
    self.selected = ko.observable(data.repeatType);
    if (!data.repeatType) {
        self.selected('One Time');
    }

    //student management
    self.addStudent = function() {
        //show overlay with my caseload students and filter
        //on ok, add selected
        alert('todo');
    };
    self.removeStudent = function(id) {
        self.students(
            _.filter(self.students(), function(s) {
                return s.studentId !== id;
            })
        );
    };

    //schedule instances
    self.oneTime = ko.observable(new ScheduleModel(null, {
        validateEnd: false,
        validateRepeatBy: false
    }));
    self.daily = ko.observable(new ScheduleModel(null, {
        validateEnd: true,
        validateRepeatBy: false
    }));
    self.weekly = ko.observable(new ScheduleModel(null, {
        validateEnd: true,
        validateRepeatBy: true
    }));
    self.monthly = ko.observable(new ScheduleModel(null, {
        validateEnd: true,
        validateRepeatBy: true
    }));
    self.yearly = ko.observable(new ScheduleModel(null, {
        validateEnd: true,
        validateRepeatBy: false
    }));

    if (/one time/i.test(self.selected())) {
        self.oneTime(new ScheduleModel(data, {
            validateEnd: false,
            validateRepeatBy: false
        }));
    } else if (/daily/i.test(self.selected())) {
        self.daily(new ScheduleModel(data, {
            validateEnd: true,
            validateRepeatBy: false
        }));
    } else if (/weekly/i.test(self.selected())) {
        self.weekly(new ScheduleModel(data, {
            validateEnd: true,
            validateRepeatBy: true
        }));
    } else if (/monthly/i.test(self.selected())) {
        self.monthly(new ScheduleModel(data, {
            validateEnd: true,
            validateRepeatBy: true
        }));
    } else if (/yearly/i.test(self.selected())) {
        self.yearly(new ScheduleModel(data, {
            validateEnd: true,
            validateRepeatBy: false
        }));
    }

    //validations
    self.students.extend({
        minLength: 1
    });
    self.errors = ko.validation.group(self);
    self.showErrors = ko.observable(false);

    //saving
    self.save = function() {
        var scheduleMember = '';

        //establish what's being done
        if (/one time/i.test(self.selected())) {
            //schedule.repeatType = 'One Time';
            scheduleMember = 'oneTime';
        } else if (/daily/i.test(self.selected())) {
            //schedule.repeatType = 'Daily';
            scheduleMember = 'daily';
        } else if (/weekly/i.test(self.selected())) {
            //schedule.repeatType = 'Weekly';
            scheduleMember = 'weekly';
        } else if (/monthly/i.test(self.selected())) {
            //schedule.repeatType = 'Monthly';
            scheduleMember = 'monthly';
        } else if (/yearly/i.test(self.selected())) {
            //schedule.repeatType = 'Yearly';
            scheduleMember = 'yearly';
        }

        //get the validity
        console.log('wrapper validity: ' + (self.errors()
            .length == 0));
        console.log(scheduleMember + ' validity: ' + self[scheduleMember]()
            .isValid());
        var valid = self.errors()
            .length == 0;
        valid = valid && self[scheduleMember]()
            .isValid();


        if (valid) {
            var scheduleInfo = ko.toJS(self[scheduleMember]);
            scheduleInfo.repeatType = self.selected();
            scheduleInfo.startDateTime = scheduleInfo.dateTime;
            scheduleInfo.organizer = self.providerId(),
            scheduleInfo.services = ko.toJS(self.students);
            scheduleInfo.seriesId = self.seriesId();
            scheduleInfo.scheduleId = self.scheduleId();
            scheduleInfo.repeats = {
                repeatType: self.selected(),
                //repeatEvery: scheduleInfo.repeatEvery,
                repeatBy: scheduleInfo.repeatBy
            };

            if ( !! scheduleInfo.ends) {
                scheduleInfo.repeats.ends = {
                    never: scheduleInfo.ends.never || false,
                    occurrences: scheduleInfo.ends.occurrences,
                    on: scheduleInfo.ends.endDate
                };
            }



            $.ajax({
                url: '/'+self.districtCode()+'/schedule/save',
                type: 'POST',
                data: scheduleInfo
            })
                .done(function(response) {
                    //hack: redirect to home for now
                    window.location = '/'+self.districtCode();
                });
        } else {
            self.showErrors(true);
            //alert('bad things');
        }
    };
};
