ko.validation.configure({
    registerExtenders: true,
    messagesOnModified: true,
    insertMessages: false,
    parseInputAttributes: true,
    messageTemplate: null
});

var TherapistQualificationsModel = function(data) {
    var self = this;
    if ( !! data) {
        data = {
            npiNumber: null,
        };
    }
    self.npiNumber = ko.observable(data.npiNumber);
    self.startDate = ko.observable(data.startDate);
    self.endDate = ko.observable(data.endDate);
    self.requiresSupervisorApproval = ko.observable(data.requiresSupervisorApproval);
    self.approvalStartDate = ko.observable(data.approvalStartDate);
    self.approvalEndDate = ko.observable(data.approvalEndDate);
    self.claimingStartDate = ko.observable(data.claimingStartDate);
    self.providerType = ko.observable(data.providerType);
    self.levelOfEducation = ko.observable(data.levelOfEducation);
    self.licensed = ko.observable(data.licensed);
    self.medicaidNumber = ko.observable(data.medicaidNumber);
};
var MCOQualificationsModel = function(data) {
    var self = this;
    if(!data){
        data = {};
        data.mco = null;
        data.startDate = null;
        data.endDate = null;
        data.userId = data.userId;
    }
    //self.userId = ko.observable(user.Id);
    self.id = ko.observable(data._id);
    self.mco = ko.observable(data.mco);
    self.startDate = ko.observable(data.startDate);
    self.endDate = ko.observable(data.endDate);
};

var UserModel = function(data) {
    var self = this;
    self.firstName = ko.observable(data.firstName);
    self.middleName = ko.observable(data.middleName);
    self.lastName = ko.observable(data.lastName);
    self.emailAddress = ko.observable(data.emailAddress)
        .extend({
            email: true,
            required: true
        });
    self.active = ko.observable( !! data.active);
    console.log('admin: ' + data.isAdministrator);
    self.isAdministrator = ko.observable(data.isAdministrator);
    self.id = ko.observable(data.id || data._id);
    console.log( !! data.isNew);
    self.isNew = ko.observable( !! data.isNew);

    self.ssn = ko.observable(data.ssn);
    self.cleanSSN = ko.computed(function(){
        var m = /^(?:(\d{3}).?(\d{2}).?(\d{4}))?$/.exec(self.ssn())
        if(!!m && m[0] !== ''){
            console.log('match!');
            return m[1].toString() + '-' + m[2].toString() + '-' + m[3].toString();
        }
        return '';
    });

    self.ssn.extend({
        //pattern: {params: /^(?:\d{3}.?\d{3}.?\d{4})?$/, message:''}
        pattern: '^(?:\\d{3}.?\\d{2}.?\\d{4})?$'
    });

    self.newPassword = ko.observable('')
        .extend({
            required: {
                onlyIf: function() {
                    return self.isNew();
                }
            }
        });
    self.newPasswordVerify = ko.observable('')
        .extend({
            equal: self.newPassword,
            required: {
                onlyIf: function() {
                    return self.newPassword() !== '';
                }
            }
        });

    //set up full name
    self.name = ko.computed(function() {
        var cleanFirst = '',
            cleanLast = '';
        if ( !! self.firstName()) {
            cleanFirst = self.firstName();
        }
        if ( !! self.lastName()) {
            cleanLast = self.lastName();
        }
        if (cleanFirst == '' && cleanLast == '') {
            return '[not entered]';
        }
        return cleanFirst + ' ' + cleanLast;
    });

    //set up validations
    self.firstName.extend({
        minLength: 2,
        required: true
    });
    self.lastName.extend({
        required: true
    });
    //self.emailAddress.extend({required:true});
    self.active.extend({
        required: true
    });

    self.errors = ko.validation.group(self);
    self.showSubmitError = ko.observable(false);
    self.submit = function() {
        self.showSubmitError(false);
        if (self.errors()
            .length > 0) {
            self.showSubmitError(true);
        } else {
            $.ajax({
                url: '/admin/user',
                type: 'POST',
                data: JSON.parse(ko.toJSON(self))
            })
                .done(function(response) {
                    console.log('done!');
                    self.id(response.value.id);
                    window.location = '/admin/users';
                });
        }
    };

    //navigation
    self.tab = ko.observable('User Information');

    //MCO qualifications
    self.mcoQualifications = ko.observableArray(ko.utils.arrayMap(data.mcoQualifications, function(m) {
        return new MCOQualificationsModel(m);
    }));

    self.mcos = ko.observable([{
        name: 'mco 1',
        id: '12345'
    }, {
        name: 'mco 2',
        id: '54321'
    }]);

    self.addMCOQualification = function(){
        self.mcoQualifications.push(new MCOQualificationsModel(null));
    };
};


var UsersModel = function(data) {
    var self = this;
    self.users = ko.observableArray(ko.utils.arrayMap(data, function(u) {
        return new UserModel(u);
    }));
}
