var MessageModel = function(message) {
    "use strict";
    var self = this;
    self.message = ko.observable(message.message);
    self.exception = ko.observable(message.exception);
};

//data contains: 
//  type name
var ImportModel = function(data, parent) {
    var self = new EventEmitter();
    self.processing = ko.observable(false);
    self.processingComplete = ko.observable(false);
    self.messages = ko.observableArray([]);

    self.currentPage = ko.observable(0);
    self.totalPages = ko.observable(0);
    self.validRecordCount = ko.observable(0);

    self.outputFormats = ko.observableArray([
        ".pdf"
    ]);
    self.selectedFormat = ko.observable(".pdf");
    self.process = function() {
        self.processing(true);
        self.processingComplete(false);
        importSocket.emit('import start', parent.typeName(), parent.token(),
            moment()
            .startOf('day')
            .toDate(), moment()
            .endOf('day')
            .toDate());
    };

    self.getImportReport = function() {
        $("#downloadFrame")
            .attr("src", "/report?token=" + parent.token() + "&v=" + new Date()
                .getTime());

    };

    self.discardImport = function() {
        $('#getTemplateContainer')
            .block({
                message: null
            });

        //todo: socket out to cancel - scorch redis cache for upload token
    };

    self.pageProgress = ko.computed(function() {
        if (self.totalPages() === 0)
            return 0;
        return Math.ceil(self.currentPage() / self.totalPages() * 100);
    });

    //set up socket.io event handling
    var importSocket = io.connect('/remittance');
    importSocket.on('error', function(err) {
        //todo: handle connection error
    });

    importSocket.on('import message', function(message) {
        parent.messages.push(new MessageModel(message));
    });

    importSocket.on('import pageEvent', function(pageEvent){
        //parent.messages.push(new MessageModel(pageEvent));
        self.currentPage(pageEvent.page);
        self.totalPages(pageEvent.totalPages);
    });

    importSocket.on('import finish', function(finish) {
        self.processing(false);
        self.processingComplete(true);

    });

    return self;
};

var UploadModel = function(data, parent) {
    var self = new EventEmitter();

    self.fileData = ko.observable();
    self.currentData = ko.observable(0);
    self.totalData = ko.observable(0);

    self.uploading = ko.observable(false);
    self.uploadComplete = ko.observable(false);

    var uploadSocket = io.connect('/upload');

    uploadSocket.on('upload setToken', function(uploadToken) {
        parent.token(uploadToken);
    });

    self.fileUpload = function() {
        //ensure it's an excel file
        if (!!self.fileData() && !self.fileData()
            .name.match(/(\.|\/)(pdf)$/i)) {
            alert("Only PDF (*.pdf) files are accepted.");
            return false;
        }

        //prevent changing file, reupload until complete
        self.emit('upload start');
        self.uploadComplete(false);
        self.uploading(true);
        var stream = ss.createStream();
        self.currentData(0);
        //if(!!self.fileData()){
        //    self.totalData(0);
        //}else{
        self.totalData(self.fileData()
            .size);
        //}
        ss(uploadSocket)
            .emit('upload', stream, {
                name: self.fileData()
                    .name,
                size: self.fileData()
                    .size
            });
        var blobstream = ss.createBlobReadStream(self.fileData());

        var size = 0;
        blobstream.on('data', function(chunk) {
            size += chunk.length;
            self.currentData(self.currentData() + chunk.length);
            if (self.uploadProgress() === 100) {
                setTimeout(function() {
                    self.uploadComplete(true);
                    self.emit('upload complete');
                    self.uploading(false);
                }, 1000);
            }
        });
        blobstream.pipe(stream);

    };

    self.uploadProgress = ko.computed(function() {
        if (self.totalData() === 0)
            return 0;
        var progress = Math.floor(self.currentData() / self.totalData() * 100);
        return progress;
    });

    self.currentDataDisplay = ko.computed(function() {
        var value = self.currentData();
        var measure = "bytes";
        if (value > 1000) {
            value = (value / 1000.0)
                .toFixed(2);
            measure = "kb";
        }
        if (value > 1000) {
            value = (value / 1000.0)
                .toFixed(2);
            measure = "mb";
        }
        return value + " " + measure;
    });

    self.totalDataDisplay = ko.computed(function() {
        var value = self.totalData();
        var measure = "bytes";
        if (value > 1000) {
            value = (value / 1000.0)
                .toFixed(2);
            measure = "kb";
        }
        if (value > 1000) {
            value = (value / 1000.0)
                .toFixed(2);
            measure = "mb";
        }
        return value + " " + measure;
    });

    self.fileNameDisplay = ko.computed(function() {
        if (self.fileData() === null || self.fileData() === undefined) {
            return "that file";
        }
        return self.fileData()
            .name;
    });

    self.reset = function() {
        console.log('resetting');
        self.fileData(null);
        self.currentData(0);
        self.totalData(0);
    };

    return self;
};

var PdfProcessingModel = function(data) {
    var self = this;
    self.token = ko.observable();
    self.selected = ko.observable('import');

    self.importModel = ko.observable(new ImportModel(data, self));
    self.uploadModel = ko.observable(new UploadModel(data, self));
    self.typeName = ko.observable(data.typeName);
    self.displayName = ko.observable(data.displayName);

    self.blockOverride = ko.observable(false);
    self.blocked = ko.computed(function() {
        return !!self.importModel()
            .processing() || !!self.uploadModel()
            .uploading() || self.blockOverride();
    });

    self.messages = ko.observableArray([]);
    self.throttledMessages = ko.computed(function() {
        return self.messages();
    })
        .extend({
            throttle: 500
        });

    //hack: set up a computed to enforce scroll hacking
    self.messageScrollhack = ko.computed(function() {
        var x = self.messages()
            .length;
        $(".scrollHack")
            .get(0)
            .scrollTop = $(".scrollHack")
            .parent()
            .get(0)
            .scrollHeight;
    });

    //excel processing model will...
    //1) upload the selected file (UploadModel)
    //2) process the selected file (ImportModel)
    //3) produce the import report (ImportModel)
    self.uploadAndProcess = function() {
        self.blockOverride(true);
        self.uploadModel()
            .fileUpload();
    }

    self.uploadModel()
        .addListener('upload start', function() {
            //todo: add message?
        });

    self.uploadModel()
        .addListener('upload complete', function() {
            self.importModel()
                .process();
        });

    self.importModel()
        .addListener('import start', function() {
            //todo: add message?
        });
    self.importModel()
        .addListener('import complete', function() {
            //todo: add message?
        });

    return self;
};
