ko.validation.configure({
    registerExtenders: true,
    messagesOnModified: true,
    insertMessages: false,
    parseInputAttributes: true,
    messageTemplate: null
});

var ResetPassword = function(data) {
    var self = this;
    self.error = ko.observable(data.error);
    self.hasError =ko.observable(!!data.error);
    self.slug = ko.observable(data.slug);

    self.newPassword = ko.observable('')
        .extend({
            required: true
        });
    self.newPasswordVerify = ko.observable('')
        .extend({
            equal: self.newPassword,
            required: true
        });

    self.done = ko.observable(false);
    self.errors = ko.validation.group(self);
    self.showSubmitError = ko.observable(false);
    self.submit = function() {
        self.showSubmitError(false);
        if (self.errors()
            .length > 0) {
            self.showSubmitError(true);
        } else {
            $.ajax({
                url: '/resetPassword/'+self.slug()+'/claim',
                type: 'POST',
                data: JSON.parse(ko.toJSON(self))
            })
                .done(function(response) {
                    console.log('done!');
                    self.done(true);
                });
        }
    };


};
