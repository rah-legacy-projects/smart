var ExportPreBillingStudentModel = function(data) {
    var self = this;
    //default values: first and last of current month
    if (!data) data = {
        startDate: moment().date(1), 
        endDate: moment().date(moment().daysInMonth())
    };
    self.startDate = ko.observable(data.startDate);
    self.endDate = ko.observable(data.endDate);
};
