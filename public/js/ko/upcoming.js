var UpcomingStudentModel = function(districtCode, data) {
    var self = this;
    self.districtCode = ko.observable(districtCode);
    self.name = ko.observable(data.name);
    self.internalStudentId = ko.observable(data.internalStudentId);
}
var UpcomingModel = function(data) {
    var self = this;
    self.districtCode = ko.observable(data.districtCode);
    self.datetime = ko.observable(data.datetime);
    self.location = ko.observable(data.location);
    self.duration = ko.observable(data.duration);
    self.service = ko.observable(data.service);
    self.scheduleId = ko.observable(data.scheduleId);
    self.sessionId = ko.observable(data.sessionId);
    self.students = ko.observableArray(ko.utils.arrayMap(data.students, function(student) {
        return new UpcomingStudentModel(self.districtCode(), student);
    }));
    self.isFinalized = ko.observable(!!data.finalizingUserId);
    self.isBilled = ko.observable(!!data.billed);
    self.studentDisplay = ko.computed(function() {
        if (self.students()
            .length > 1) {
            return 'Group Session (' + self.students()
                .length + ')';
        } else {
            console.log('student name: ' + self.students()[0].name());
            return self.students()[0].name();
        }
    });

    self.notesEntered = ko.computed(function() {
        if (!!self.sessionId()) {
            return "Yes";
        } else {
            return "No";
        }
    });

    self.showGroupSession = ko.observable(false);



    self.appointmentDescription = ko.computed(function() {
        var message = '';
        if (self.duration() !== 0) {
            if (/\w*/.test(self.location())) {
                message = '{service} on {date} at {time} for {duration} minutes';
            } else {
                message = '{service} on {date} at {time} in {location} for {duration} minutes';
            }
        } else {
            message = '{service} on {date} at {time} - ad hoc session incomplete'
        }
        return message
            .namedFormat({
                service: self.service(),
                date: moment(self.datetime())
                    .format('M/D/YYYY'),
                time: moment(self.datetime())
                    .format('h:mm a'),
                location: self.location(),
                duration: self.duration()
            });
    });

    self.notesMessage = ko.computed(function() {
        if (!self.sessionId()) {
            return "No notes for this session.";
        } else {
            return "Notes entered.";
        }
    });

    self.buttonText = ko.computed(function() {
        if (!self.sessionId()) {
            return "Begin Session";
        } else if (self.isFinalized() || self.isBilled()) {
            return "View Session";
        } else {
            return "Modify Session";
        }
    });

    self.delete = function(tableRef) {
        AreYouSure.show("Are you certain you want to delete this session?", function() {
            //hack: pass in table page observable for refresh
            console.log('deleting ' + self.scheduleId());
            $.ajax({
                url: '/' + self.districtCode() + '/schedule/delete/' + self.scheduleId(),
                type: 'POST'
            })
                .done(function(response) {
                    console.log('deleted');
                    //tablePageObservable(tablePageObservable());
                    //tablePageObservable.valueHasMutated();
                    tableRef.pageSelect(tableRef.selectedPage());
                });
        });
    };
};
