ko.validation.configure({
    registerExtenders: true,
    messagesOnModified: true,
    insertMessages: false,
    parseInputAttributes: true,
    messageTemplate: null
});

var MCOModel = function(data) {
    var self = this;
    self.id = ko.observable(data.id);
    self.name = ko.observable(data.name);
    self.shortName = ko.observable(data.shortName);
};

var MCOQualificationEditModel = function(data) {
    var self = this;
    self.userId = ko.observable(data.user._id);
    if (!data.mcoQualification) {
        data.mcoQualification = {
            startDate: null,
            endDate: null,
            _id: null
        };
    }
    self.mcoQualificationId = ko.observable(data.mcoQualification._id);
    self.startDate = ko.observable(data.mcoQualification.startDate);
    self.endDate = ko.observable(data.mcoQualification.endDate);
    self.name = ko.observable(data.user.firstName + " " + data.user.lastName);

    self.mcos = ko.observableArray(ko.utils.arrayMap(data.mcos, function(m) {
        return new MCOModel(m);
    }));

    self.endDateDisplay = ko.computed(function() {
        if (!!self.endDate()) {
            return moment(self.endDate())
                .format('M/D/YYYY');
        }
        return '';
    });
    self.startDateDisplay = ko.computed(function() {
        if (!!self.startDate()) {
            return moment(self.startDate())
                .format('M/D/YYYY');
        }
        return '';
    });

    self.startDate.extend({
        dateRange: {
            params: self.endDate
        }
    });
    self.dateRangeValid = ko.computed(function() {
        return moment(self.startDate())
            .isBefore(moment(self.endDate())) ||
            moment(self.startDate())
            .isSame(moment(self.endDate()));
    });


    self.mco = ko.observable();
    if (!!data.mcoQualification) {
        self.mco = ko.observable(data.mcoQualification.mco);

    }

    self.mcoDisplay = ko.computed(function(){
        var m = ko.utils.arrayFirst(self.mcos(), function(mco){
            return mco.id() == self.mco();
        });
        if(!m){
            return 'Choose one...'
        }
        return m.name();
    });

    //validations
    self.startDate.extend({
        date: true,
        required: true
    });
    self.endDate.extend({
        date: true,
        required: true
    });
    self.mco.extend({
        required: true
    });

    self.errors = ko.validation.group(self);
    self.showSubmitError = ko.observable(false);
    self.submit = function() {
        self.showSubmitError(false);
        if (self.errors()
            .length > 0) {
            self.showSubmitError(true);
        } else {

            console.log(ko.toJSON(self));

            $.ajax({
                url: '/admin/mcoQualification',
                type: 'POST',
                data: JSON.parse(ko.toJSON(self))
            })
                .done(function(response) {
                    console.log('done!');
                    //self.id(response.updatedUser.id);
                    window.location = '/admin/' + self.userId() + '/mcoQualifications';
                });
        }
    };
};

var MCOQualificationsViewModel = function(data, mcos, userId) {
    var self = this;
    self.startDate = ko.observable(moment(data.startDate)
        .format("M/D/YYYY"));
    self.endDate = ko.observable(moment(data.endDate)
        .format("M/D/YYYY"));

    self.id = ko.observable(data._id);
    self.mco = ko.observable(ko.utils.arrayFirst(mcos, function(m) {
            return m.id == data.mco;
        })
        .name);
    self.deleted = ko.observable(false);
    self.remove = function() {
        $.ajax({
            url: '/admin/revokeMCOQualification',
            type: 'POST',
            data: {
                mcoQualificationId: self.id()
            }
        })
            .done(function(response) {
                self.deleted(true);
            });
    };
}

var MCOQualificationsViewContainer = function(data) {
    var self = this;
    self.name = ko.observable(data.user.firstName + ' ' + data.user.lastName);
    self.userId = ko.observable(data.user._id);
    self.mcoQualificationsRaw = ko.observableArray(ko.utils.arrayMap(data.user.mcoQualifications, function(m) {
        return new MCOQualificationsViewModel(m, data.mcos, self.userId);
    }));
    self.mcoQualifications = ko.computed(function() {
        return ko.utils.arrayFilter(self.mcoQualificationsRaw(), function(m) {
            return !m.deleted();
        });
    });

};
