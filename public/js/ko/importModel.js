var MessageModel = function(message) {
    "use strict";
    var self = this;
    self.message = ko.observable(message.message);
    self.sheetName = ko.observable(message.sheetName);
    self.row = ko.observable(message.row);
    self.columnName = ko.observable(message.columnName);
    self.exception = ko.observable(message.exception);
};

//data contains: 
//  type name
var ImportModel = function(data, parent) {
    var self = new EventEmitter();
    self.processing = ko.observable(false);
    self.processingComplete = ko.observable(false);
    self.messages = ko.observableArray([]);

    self.currentSheet = ko.observable(0);
    self.totalSheets = ko.observable(0);
    self.currentRow = ko.observable(0);
    self.totalRows = ko.observable(0);
    self.validRecordCount = ko.observable(0);

    self.outputFormats = ko.observableArray([
        ".xlsx", ".xls", ".csv"
    ]);
    self.selectedFormat = ko.observable(".xlsx");
    self.process = function() {
        self.processing(true);
        self.processingComplete(false);
        importSocket.emit('import start', parent.typeName(), parent.token(),
            moment()
            .startOf('day')
            .toDate(), moment()
            .endOf('day')
            .toDate(),
            tzdetect.matches()[0]);
    };

    self.getImportReport = function() {
        $("#downloadFrame")
            .attr("src", "/report?token=" + parent.token() + "&v=" + new Date()
                .getTime());

    };

    self.discardImport = function() {
        $('#getTemplateContainer')
            .block({
                message: null
            });

        //todo: socket out to cancel - scorch redis cache for upload token
    };

    self.sheetProgress = ko.computed(function() {
        if (self.totalSheets() === 0)
            return 0;
        return Math.ceil(self.currentSheet() / self.totalSheets() * 100);
    });
    self.rowProgress = ko.computed(function() {
        if (self.totalRows() === 0)
            return 0;
        return Math.ceil(self.currentRow() / self.totalRows() * 100);
    });

    self.overallProgress = ko.computed(function() {
        if (self.totalSheets() === 0)
            return 0;
        if (self.totalRows() === 0)
            return 0;
        var x = (self.currentSheet() + 1) / (self.totalSheets() + 1);
        var y = self.currentRow() / self.totalRows();
        return Math.ceil(x * y * 100);
    })
    //set up socket.io event handling
    var importSocket = io.connect('/import');
    importSocket.on('error', function(err) {
        //todo: handle connection error
    });
    importSocket.on('import debug', function(msg) {});

    //import processing events
    importSocket.on('row validation error', function(rowEvent) {
        parent.messages.push(new MessageModel(rowEvent));

    });

    importSocket.on('sheet validation error', function(sheetEvent) {
        parent.messages.push(new MessageModel(sheetEvent));
    });

    importSocket.on('import fileEvent', function(fileEvent) {
        parent.messages.push(new MessageModel(fileEvent));
    });

    //import progress events
    importSocket.on('row progress', function(rowProgress) {
        self.currentRow(rowProgress.current);
        self.totalRows(rowProgress.total);

    });
    importSocket.on('sheet progress', function(sheetProgress) {
        self.currentSheet(sheetProgress.current);
        self.totalSheets(sheetProgress.total);
    });

    importSocket.on('sheet info', function(sheetInfo) {
        parent.messages.push(new MessageModel(sheetInfo));
    });

    importSocket.on('sheet warning', function(sheetWarning) {
        parent.messages.push(new MessageModel(sheetWarning));
    });

    importSocket.on('import finish', function(finish) {
        self.processing(false);
        self.processingComplete(true);

    });

    return self;
};

var UploadModel = function(data, parent) {
    var self = new EventEmitter();
    self.addListener('upload test', function() {
        //console.log('upload test complete');
    });
    self.emit('upload test');

    self.fileData = ko.observable();
    self.currentData = ko.observable(0);
    self.totalData = ko.observable(0);

    self.uploading = ko.observable(false);
    self.uploadComplete = ko.observable(false);

    var uploadSocket = io.connect('/upload');

    uploadSocket.on('upload setToken', function(uploadToken) {
        parent.token(uploadToken);
    });

    self.fileUpload = function() {
        //ensure it's an excel file
        if (!!self.fileData() && !self.fileData()
            .name.match(/(\.|\/)(csv|xls|xlsx)$/i)) {
            alert("Only Excel (*.csv, *.xls, *.xlsx) files are accepted.");
            return false;
        }

        //prevent changing file, reupload until complete
        self.emit('upload start');
        self.uploadComplete(false);
        self.uploading(true);
        var stream = ss.createStream();
        self.currentData(0);
        //if(!!self.fileData()){
        //    self.totalData(0);
        //}else{
        self.totalData(self.fileData()
            .size);
        //}
        ss(uploadSocket)
            .emit('upload', stream, {
                name: self.fileData()
                    .name,
                size: self.fileData()
                    .size
            });
        var blobstream = ss.createBlobReadStream(self.fileData());

        var size = 0;
        blobstream.on('data', function(chunk) {
            size += chunk.length;
            self.currentData(self.currentData() + chunk.length);
            if (self.uploadProgress() === 100) {
                setTimeout(function() {
                    self.uploadComplete(true);
                    self.emit('upload complete');
                    self.uploading(false);
                }, 1000);
            }
        });
        blobstream.pipe(stream);

    };

    self.uploadProgress = ko.computed(function() {
        if (self.totalData() === 0)
            return 0;
        var progress = Math.floor(self.currentData() / self.totalData() * 100);
        return progress;
    });

    self.currentDataDisplay = ko.computed(function() {
        var value = self.currentData();
        var measure = "bytes";
        if (value > 1000) {
            value = (value / 1000.0)
                .toFixed(2);
            measure = "kb";
        }
        if (value > 1000) {
            value = (value / 1000.0)
                .toFixed(2);
            measure = "mb";
        }
        return value + " " + measure;
    });

    self.totalDataDisplay = ko.computed(function() {
        var value = self.totalData();
        var measure = "bytes";
        if (value > 1000) {
            value = (value / 1000.0)
                .toFixed(2);
            measure = "kb";
        }
        if (value > 1000) {
            value = (value / 1000.0)
                .toFixed(2);
            measure = "mb";
        }
        return value + " " + measure;
    });

    self.fileNameDisplay = ko.computed(function() {
        if (self.fileData() === null || self.fileData() === undefined) {
            return "that file";
        }
        return self.fileData()
            .name;
    });

    self.reset = function() {
        console.log('resetting');
        self.fileData(null);
        self.currentData(0);
        self.totalData(0);
    };

    return self;
};

var TemplateModel = function(data, parent) {
    var self = this;
    self.extension = ko.observable('xlsx');
    //socket out to template - pull down template for type
    var importSocket = io.connect('/template');
    importSocket.on('template finishEvent', function(templateToken) {
        //todo: pull the template down
        console.log('about to pull down ' + templateToken);

        $("#downloadFrame")
            .attr("src", "/template?templateToken=" + templateToken + "&extension=" + self.extension() + "&typeName=" + parent.typeName() + "&v=" + new Date()
                .getTime());
    });

    self.download = function() {
        //todo: pick extension in UI
        importSocket.emit('template start', parent.typeName(), self.extension());

    };

};

var CommitChangesModel = function(data, parent) {
    var self = this;
    self.processing = ko.observable(false);
    self.processingComplete = ko.observable(false);
    self.messages = ko.observableArray([]);

    self.currentSheet = ko.observable(0);
    self.totalSheets = ko.observable(0);
    self.currentRow = ko.observable(0);
    self.totalRows = ko.observable(0);
    self.overallProgress = ko.computed(function() {
        if (self.totalRows() === 0) return 0;
        var y = self.currentRow() / self.totalRows();
        return Math.ceil(y * 100);
    })

    //set up commit socket
    var commitSocket = io.connect('/commit');
    commitSocket.on('error', function(err) {
        //todo: handle connection errors
    });
    commitSocket.on('commit row progress', function(commitRowProgress) {
        self.currentRow(commitRowProgress.current);
        self.totalRows(commitRowProgress.total);
    });
    commitSocket.on('commit error', function(err) {
        parent.messages.push(new MessageModel(err));
    });
    commitSocket.on('commit finish', function(finish) {
        parent.messages.push(new MessageModel({
            message: 'Cleaning up...'
        }));
        setTimeout(function() {
            parent.messages.push(new MessageModel({
                message: 'Done.'
            }));
            self.processing(false);
        }, 3000);

    });

    self.commitChanges = function() {
        //set up block
        self.processing(true);
        //socket out to commit - apply changes cached in redis
        commitSocket.emit('commit start', parent.typeName(), parent.token(), moment()
            .startOf('day')
            .toDate(), moment()
            .endOf('day')
            .toDate(), tzdetect.matches()[0]);

    };

};


var ExcelProcessingModel = function(data) {
    var self = this;
    self.token = ko.observable();
    self.selected = ko.observable('instructions');

    self.importModel = ko.observable(new ImportModel(data, self));
    self.uploadModel = ko.observable(new UploadModel(data, self));
    self.templateModel = ko.observable(new TemplateModel(data, self));
    self.commitChangesModel = ko.observable(new CommitChangesModel(data, self))
    self.typeName = ko.observable(data.typeName);
    self.displayName = ko.observable(data.displayName);

    self.blockOverride = ko.observable(false);
    self.blocked = ko.computed(function() {
        return !!self.importModel()
            .processing() || !!self.uploadModel()
            .uploading() || self.blockOverride() || !!self.commitChangesModel()
            .processing();
    });

    self.messages = ko.observableArray([]);
    self.throttledMessages = ko.computed(function() {
        return self.messages();
    })
        .extend({
            throttle: 500
        });

    //hack: set up a computed to enforce scroll hacking
    self.messageScrollhack = ko.computed(function() {
        var x = self.messages()
            .length;
        $(".scrollHack")
            .get(0)
            .scrollTop = $(".scrollHack")
            .parent()
            .get(0)
            .scrollHeight;
    });

    //excel processing model will...
    //1) upload the selected file (UploadModel)
    //2) process the selected file (ImportModel)
    //3) produce the import report (ImportModel)
    self.uploadAndProcess = function() {
        self.blockOverride(true);
        self.uploadModel()
            .fileUpload();
    }

    self.uploadModel()
        .addListener('upload start', function() {
            //todo: add message?
        });

    self.uploadModel()
        .addListener('upload complete', function() {
            self.importModel()
                .process();
        });

    self.importModel()
        .addListener('import start', function() {
            //todo: add message?
        });
    self.importModel()
        .addListener('import complete', function() {
            //todo: add message?
        });

    return self;
};
