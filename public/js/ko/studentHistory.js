var StudentProgressNotesModel = function(districtCode, data) {
    var self = this;
    self.districtCode = ko.observable(districtCode);
    if (!data) {
        data = {};
    }
    self.progressNoteId = ko.observable(data.progressNoteId);
    self.addedDate = ko.observable(data.addedDate);
    self.addedDateDisplay = ko.computed(function() {
        return moment(self.addedDate())
            .format('MM/DD/YYYY [at] h:mm a');
    });
    self.notedBy = ko.observable(data.notedBy);
    self.note = ko.observable(data.note);
    self.specialty = ko.observable(data.specialty);
    self.specialtyDisplay = ko.computed(function() {
        if (!self.specialty() || self.specialty() == '') {
            return 'Select one...';
        }
        return self.specialty();
    });

    self.headerText = ko.computed(function() {
        return ("{date} - {service} - {notedBy}")
            .namedFormat({
                notedBy: self.notedBy(),
                date: self.addedDateDisplay(),
                service: self.specialty()
            });
    });

    self.note.extend({
        required: true
    });
    self.specialty.extend({
        required: true
    });
    self.errors = ko.validation.group(self);

    self.medicaidProgress = ko.observable('Select One...');
    self.medicaidProgressClick = function(){
        if(self.medicaidProgress() == 'Improving'){
            self.medicaidProgress('Maintaining');
        }else if(self.medicaidProgress() == 'Maintaining'){
            self.medicaidProgress('Regressing');
        }else if(self.medicaidProgress() == 'Regressing'){
            self.medicaidProgress('Improving');
        }
    };

};


var StudentProgressNotesWrapperModel = function(districtCode, studentId) {
    var self = this;
    self.districtCode = ko.observable(districtCode);
    self.studentProgressNotes = ko.observableArray([]);
    self.specialtyOptions = ko.observableArray([]);

    var tasks = [

        function(cb) {
            $.ajax({
                url: '/' + self.districtCode() + '/student/' + studentId + '/getProgressNotes',
                type: 'POST'
            })
                .done(function(response) {
                    self.studentProgressNotes(ko.utils.arrayMap(response.value, function(p) {
                        return new StudentProgressNotesModel(self.districtCode(), p);
                    }));
                    cb(null);
                });
        },
        function(cb) {
            $.ajax({
                url: '/' + self.districtCode() + '/service/' + studentId + '/getServices',
                type: 'POST'
            })
                .done(function(response) {
                    self.specialtyOptions(ko.utils.arrayMap(response.value, function(p) {
                        return new SpecialtyOptionModel(self.districtCode(), p);
                    }));
                    cb(null);
                });
        }
    ];

    async.parallel(tasks, function(err) {});

};

var SessionModel = function(districtCode, data) {
    var self = this;
    self.districtCode = ko.observable(districtCode);
    self.therapistName = ko.observable(data.therapistName);
    self.therapy = ko.observable(data.therapy);
    self.dateTime = ko.observable(data.dateTime);
    self.sessionNotes = ko.observable(data.sessionNotes);
    self.individualNote = ko.observable(data.individualNote);
    self.absent = ko.observable(data.absent);
    self.absenceReason = ko.observable(data.absenceReason);
    self.date = ko.computed(function() {
        return moment(self.dateTime())
            .format('MM/DD/YYYY');
    });
    self.time = ko.computed(function() {
        return moment(self.dateTime())
            .format('h:mm a');
    });

    self.medicaidProgress = ko.observable('Improving');
    self.medicaidProgressClick = function(){
        if(self.medicaidProgress() == 'Improving'){
            self.medicaidProgress('Maintaining');
        }else if(self.medicaidProgress() == 'Maintaining'){
            self.medicaidProgress('Regressing');
        }else if(self.medicaidProgress() == 'Regressing'){
            self.medicaidProgress('Improving');
        }
    };

    self.duration = ko.observable(data.duration);
    self.procedureCode = ko.observable(data.procedureCode);
    //note: this diagnosis code is based on the attendee in the session,
    //note: and will not be computed based off of the student.
    self.diagnosticCode = ko.observable(data.diagnosticCode);
    self.isFinalized = ko.observable(!!data.finalizingUserId);
    self.isBilled = ko.observable(data.isBilled);
    self.scheduleId = ko.observable(data.scheduleId);
    self.isGroupSession = ko.observable(data.isGroupSession);
    console.log('group session: ' + self.isGroupSession());
    self.buttonDisplay = ko.computed(function() {
        if (self.isBilled() || self.isFinalized()) {
            return "View Session";
        }
        return "Edit Session";
    });

    self.status = ko.computed(function() {
        if (self.isBilled()) {
            return "Billed";
        } else if (self.isFinalized()) {
            return "Finalized";
        } else {
            return "Open";
        }
    });

    self.durationDisplay = ko.computed(function() {
        return ("{date} at {time}<br/> {duration} minutes")
            .namedFormat({
                date: self.date(),
                time: self.time(),
                duration: self.duration()
            });
    });
    self.therapyDisplay = ko.computed(function() {
        return ("{date} at {time} for {duration} minutes - {therapy} with {therapist}")
            .namedFormat({
                therapy: self.therapy() || 'Not specified',
                therapist: self.therapistName(),
                date: self.date(),
                time: self.time(),
                duration: self.duration()
            });
    });

    self.codesDisplay = ko.computed(function() {
        var procedure = self.procedureCode();
        if (procedure == 'No Procedure Code') {
            procedure = 'not selected';
        }
        var diagnosis = self.diagnosticCode();
        if (diagnosis == 'No Diagnostic Code') {
            diagnosis = 'not selected';

        }
        return ('Procedure {procedure}, diagnosis {diagnosis}')
            .namedFormat({
                procedure: procedure,
                diagnosis: diagnosis
            });
    });

    self.deleted = ko.observable(false);
    self.delete = function() {
        //hack: pass in table page observable for refresh
        AreYouSure.show("Are you certain you want to delete this session?", function() {
            console.log('deleting ' + self.scheduleId());
            $.ajax({
                url: '/' + self.districtCode() + '/schedule/delete/' + self.scheduleId(),
                type: 'POST'
            })
                .done(function(response) {
                    console.log('deleted ' + self.scheduleId());
                    self.deleted(true);
                });
        });
    };
};

var SessionHistoryWrapper = function(districtCode, studentId) {
    var self = this;
    self.districtCode = ko.observable(districtCode);
    self.sessionHistory = ko.observable();
    $.ajax({
        url: '/' + self.districtCode() + '/student/' + studentId + '/sessionHistory',
        type: 'POST'
    })
        .done(function(response) {
            self.sessionHistory(ko.utils.arrayMap(response.value, function(v) {
                return new SessionModel(self.districtCode(), v);
            }));
        });
};

var CombinedHistoryModel = function(data) {
    var self = this;
    self.isProgressNote = ko.observable(data.isProgressNote);
    self.isSession = ko.observable(data.isSession);
    self.date = ko.observable(data.date);
    self.data = ko.observable(data.data);

};

var StudentHistoryWrapperModel = function(districtCode, studentId) {
    var self = this;
    self.districtCode = ko.observable(districtCode);

    self.sessionHistoryWrapper = ko.observable(new SessionHistoryWrapper(self.districtCode(), studentId));
    self.progressNotesWrapper = ko.observable(new StudentProgressNotesWrapperModel(self.districtCode(), studentId));


    self.modal = {};
    self.modal.modalVisible = ko.observable(false);
    self.modal.show = function() {
        self.modal.modalVisible(true);
    };
    self.modal.action = function() {}



    self.historySpecialtySelection = ko.observable('No Filter');
    self.peopleSelection = ko.observable('No Filter');
    self.noteTypeSelection = ko.observable('No Filter');
    self.selectedStartDate = ko.observable(null);
    self.selectedStartDateDisplay = ko.computed(function() {
        if (!self.selectedStartDate()) {
            return '';
        }
        return moment(self.selectedStartDate())
            .format('MM/DD/YYYY');
    });
    self.selectedEndDate = ko.observable(null);
    self.selectedEndDateDisplay = ko.computed(function() {
        if (!self.selectedEndDate()) {
            return '';
        }
        return moment(self.selectedEndDate())
            .format('MM/DD/YYYY');
    });

    self.sessionsAndNotes = ko.computed(function() {

        self.sessionHistoryWrapper()
            .sessionHistory();;
        self.progressNotesWrapper()
            .studentProgressNotes();

        //hack: put session history and progress notes in a single collection for collapse
        var combined = [];
        if (!!self.sessionHistoryWrapper()
            .sessionHistory()) {
            combined = _.filter(combined.concat(self.sessionHistoryWrapper()
                .sessionHistory()), function(c) {
                return !c.deleted();
            });
        }
        combined = combined.concat(self.progressNotesWrapper()
            .studentProgressNotes());


        var mapped = _.map(combined, function(c) {
            console.log((c.addedDate || function() {
                return 'na';
            })() + ' || ' + (c.dateTime || function() {
                return 'na';
            })())
            return {
                date: (c.addedDate || c.dateTime || function() {
                    return new Date();
                })(),
                isProgressNote: c instanceof StudentProgressNotesModel,
                isSession: c instanceof SessionModel,
                data: c
            };
        });

        if (self.noteTypeSelection() !== 'No Filter') {
            mapped = _.filter(mapped, function(m) {
                return (self.noteTypeSelection() == 'Session' && m.isSession) || (self.noteTypeSelection() == 'Progress Note' && m.isProgressNote);
            });
        }

        if (self.historySpecialtySelection() !== 'No Filter') {
            mapped = _.filter(mapped, function(m) {
                return (m.isProgressNote && m.data.specialty() == self.historySpecialtySelection()) || (m.isSession && m.data.therapy() == self.historySpecialtySelection());
            });
        }

        if (self.peopleSelection() !== 'No Filter') {
            mapped = _.filter(mapped, function(m) {
                return (m.isProgressNote && m.data.notedBy() == self.peopleSelection()) || (m.isSession && m.data.therapistName() == self.peopleSelection());
            });
        }

        if (!!self.selectedStartDate()) {
            console.log('filtering on start date');
            mapped = _.filter(mapped, function(m) {
                var m = m.isProgressNote ? m.data.addedDate() : (m.isSession ? m.data.dateTime() : null);
                if (m == null) return m;

                return moment(m)
                    .isAfter(moment(self.selectedStartDate()));

            });
        }

        if (!!self.selectedEndDate()) {
            mapped = _.filter(mapped, function(m) {
                var m = m.isProgressNote ? m.data.addedDate() : (m.isSession ? m.data.dateTime() : null);
                if (m == null) return m;
                var eod = moment(self.selectedEndDate())
                    .endOf('day');
                return moment(m)
                    .isBefore(eod);
            });
        }

        var table = new StaticTableModel({
            rowModel: CombinedHistoryModel,
            defaultSortBy: 'date',
            table: {
                rows: mapped
            }
        });

        //hack: calling set sort by again will reverse the order
        table.setSortBy('date');
        return table;

    });

    self.peopleOptions = ko.computed(function() {

        //get the session history specialties
        var people = _.pluck(self.sessionHistoryWrapper()
            .sessionHistory(), function(sh) {
                return sh.therapistName();
            });

        //get the progress notes specialties
        people = people.concat(_.pluck(self.progressNotesWrapper()
            .studentProgressNotes(), function(pn) {
                return pn.notedBy();
            }));

        //get the unique ones
        people = _.chain(people)
            .uniq(people)
            .sort()
            .reverse()
            .push('No Filter')
            .reverse()
            .value();
        return people;

    });

    self.noteTypeOptions = ko.computed(function() {
        return [
            'No Filter',
            'Session',
            'Progress Note'
        ];
    });

    self.historySpecialties = ko.computed(function() {
        //get the session history specialties
        var specialties = _.pluck(self.sessionHistoryWrapper()
            .sessionHistory(), function(sh) {
                return sh.therapy();
            });

        //get the progress notes specialties
        specialties = specialties.concat(_.pluck(self.progressNotesWrapper()
            .studentProgressNotes(), function(pn) {
                return pn.specialty();
            }));

        //get the unique ones
        specialties = _.uniq(specialties);
        //alpha sort
        specialties.sort();
        specialties.reverse();
        specialties.push('No Filter');
        specialties.reverse();
        return specialties;
    });

    self.saveProgressNote = function(data, cb) {
        $.ajax({
            url: '/' + self.districtCode() + '/student/' + studentId + '/saveProgressNote',
            type: 'POST',
            data: data
        })
            .done(function(response) {
                if (!!cb) {
                    cb(response);
                }
            });

    };

    self.removeProgressNote = function(data) {
        AreYouSure.show("Remove progress note?", function() {
            console.log('remove: ' + ko.toJSON(data));
            $.ajax({
                url: '/' + self.districtCode() + '/student/' + studentId + '/removeProgressNote/' + data.data()
                    .progressNoteId(),
                type: 'POST',
            })
                .done(function(response) {
                    self.progressNotesWrapper()
                        .studentProgressNotes(
                            ko.utils.arrayFilter(self.progressNotesWrapper()
                                .studentProgressNotes(), function(pc) {
                                    return pc.progressNoteId() !== data.data()
                                        .progressNoteId();
                                }));
                });
        });

    };

    var InternalProgressNoteModalModel = function() {
        var modal = this;
        modal.showErrors = ko.observable(false);
        modal.onClose = function() {
            //alert('close');
            if (!!modal.originalData()) {
                //was not new data.  restore to original valuesa
                _.forIn(modal.originalData(), function(value, member) {
                    if (!ko.isComputed(modal.originalData()[member]) && ko.isObservable(modal.originalData()[member])) {
                        modal.data()[member](value());
                    }
                });
            }
        };
        modal.onAction = function() {
            //check validity
            if (modal.data()
                .errors()
                .length > 0) {
                modal.showErrors(true);
            } else {
                //save!
                self.saveProgressNote(ko.toJS(modal.data), function(response) {
                    modal.data()
                        .progressNoteId(response.value.progressNoteId);
                    modal.data()
                        .addedDate(response.value.addedDate);
                    modal.data()
                        .notedBy(response.value.notedBy);
                    if (!modal.originalData()) {
                        self.progressNotesWrapper()
                            .studentProgressNotes
                            .push(modal.data());
                    }
                    modal.show(false);
                });
            }
        };

        modal.show = ko.observable(false);

        modal.originalData = ko.observable();
        modal.data = ko.observable(new StudentProgressNotesModel(self.districtCode(), null));
        modal.open = function(mapping) {
            if (!!mapping) {
                modal.originalData(new StudentProgressNotesModel(self.districtCode(), ko.toJS(mapping)));
                modal.data(mapping);
            } else {
                modal.originalData(null);
                modal.data(new StudentProgressNotesModel(self.districtCode(), null));
            }
            modal.show(true);
        };

        modal.specialtyOptions = ko.computed(function() {
            return self.progressNotesWrapper()
                .specialtyOptions();
        });

        return modal;
    }
    self.noteModal = new InternalProgressNoteModalModel();
}
