var MessageModel = function(message) {
    "use strict";
    var self = this;
    self.message = ko.observable(message.message);
    self.sheetName = ko.observable(message.sheetName);
    self.row = ko.observable(message.row);
    self.columnName = ko.observable(message.columnName);
    self.exception = ko.observable(message.exception);
};

//data contains: 
//  type name
var ExportModel = function(data, parent) {
    var self = new EventEmitter();
    self.processing = ko.observable(false);
    self.processingComplete = ko.observable(false);
    self.messages = ko.observableArray([]);

    self.currentSheet = ko.observable(0);
    self.totalSheets = ko.observable(0);
    self.currentRow = ko.observable(0);
    self.totalRows = ko.observable(0);
    self.validRecordCount = ko.observable(0);

    self.outputFormats = ko.observableArray([
        ".xlsx", ".xls", ".csv"
    ]);
    self.selectedFormat = ko.observable(".xlsx");
    self.process = function() {
        self.processing(true);
        self.processingComplete(false);
        exportSocket.emit('export start', parent.typeName(), ko.toJS(parent.parameters));
    };

    self.getExportReport = function() {
        console.log('about to get report, token: ' + parent.token());
        $("#downloadFrame")
            .attr("src", "/exportReport?token=" + parent.token() + "&v=" + new Date()
                .getTime());

    };


    self.sheetProgress = ko.computed(function() {
        if (self.totalSheets() === 0)
            return 0;
        return Math.ceil(self.currentSheet() / self.totalSheets() * 100);
    });
    self.rowProgress = ko.computed(function() {
        if (self.totalRows() === 0)
            return 0;
        return Math.ceil(self.currentRow() / self.totalRows() * 100);
    });

    self.overallProgress = ko.computed(function() {
        if (self.totalSheets() === 0)
            return 0;
        if (self.totalRows() === 0)
            return 0;
        var x = (self.currentSheet() + 1) / (self.totalSheets() + 1);
        var y = self.currentRow() / self.totalRows();
        return Math.ceil(x * y * 100);
    })
    //set up socket.io event handling
    var exportSocket = io.connect('/export');
    exportSocket.on('error', function(err) {
        //todo: handle connection error
    });
    exportSocket.on('export debug', function(msg) {});

    exportSocket.on('export setToken', function(token) {
        console.log('setting token: ' + token);
        parent.token(token);
    });
    //export processing events
    exportSocket.on('row validation error', function(rowEvent) {
        parent.messages.push(new MessageModel(rowEvent));

    });

    exportSocket.on('sheet validation error', function(sheetEvent) {
        parent.messages.push(new MessageModel(sheetEvent));
    });

    exportSocket.on('export fileEvent', function(fileEvent) {
        parent.messages.push(new MessageModel(fileEvent));
    });

    //export progress events
    exportSocket.on('row progress', function(rowProgress) {
        self.currentRow(rowProgress.current);
        self.totalRows(rowProgress.total);

    });
    exportSocket.on('sheet progress', function(sheetProgress) {
        self.currentSheet(sheetProgress.current);
        self.totalSheets(sheetProgress.total);
    });

    exportSocket.on('sheet info', function(sheetInfo) {
        parent.messages.push(new MessageModel(sheetInfo));
    });

    exportSocket.on('sheet warning', function(sheetWarning) {
        parent.messages.push(new MessageModel(sheetWarning));
    });

    exportSocket.on('export finish', function(finish) {
        console.log('export finish');

        self.processing(false);
        self.processingComplete(true);
        self.getExportReport();

        parent.messages.push(new MessageModel({
            message: 'Done.'
        }));
    });

    return self;
};

var ExcelProcessingModel = function(data, parameterModel) {
    var self = this;
    self.token = ko.observable();
    self.selected = ko.observable('instructions');

    self.exportModel = ko.observable(new ExportModel(data, self));
    self.typeName = ko.observable(data.typeName);
    self.displayName = ko.observable(data.displayName);


    self.parameters = ko.observable(new parameterModel());

    self.blockOverride = ko.observable(false);
    self.blocked = ko.computed(function() {
        return !!self.exportModel()
            .processing() || self.blockOverride();
    });

    self.messages = ko.observableArray([]);
    self.throttledMessages = ko.computed(function() {
        return self.messages();
    })
        .extend({
            throttle: 500
        });

    //hack: set up a computed to enforce scroll hacking
    self.messageScrollhack = ko.computed(function() {
        var x = self.messages()
            .length;
        $(".scrollHack")
            .get(0)
            .scrollTop = $(".scrollHack")
            .parent()
            .get(0)
            .scrollHeight;
    });

    self.process = function() {
        //self.blockOverride(true);
        self.exportModel()
            .process();
    }

    self.exportModel()
        .addListener('export start', function() {
            //todo: add message?
        });
    self.exportModel()
        .addListener('export complete', function() {
            //todo: add message?
        });

    return self;
};
