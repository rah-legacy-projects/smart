ko.validation.configure({
    registerExtenders: true,
    messagesOnModified: true,
    insertMessages: false,
    parseInputAttributes: true,
    messageTemplate: null
});

var School = function(data){
    var self = this;
    self.name = ko.observable(data.name);
    self.id = ko.observable(data._id);
return self;
}

var District = function(data){
    var self = this;
    self.name = ko.observable(data.name);
    self.id = ko.observable(data._id);
    self.districtCode = ko.observable(data.districtCode);
    self.schools = ko.observableArray(ko.utils.arrayMap(data.schools, function(s){return new School(s);}));
    return self;
};
var PermissionsNew = function(data){
    var self = this;
    console.log(data);
    self.districts = ko.observableArray(ko.utils.arrayMap(data.locations.districts, function(d){return new District(d);}));
    self.possiblePermissions = ko.observableArray(data.possiblePermissions);
    self.selectedPermissions = ko.observableArray([]);
    self.selectedLocation = ko.observable();
    self.selectedLocationType = ko.observable();
    self.userId = ko.observable(data.user._id);

    self.selectedLocation.extend({required:true});
    self.selectedPermissions.extend({required:true});

    self.errors = ko.validation.group(self);
    self.showSubmitError = ko.observable(false);
    self.submit = function(){
        self.showSubmitError(false);
        if(self.errors().length > 0){
            self.showSubmitError(true);
        }else{
            var submission = {
                userId: self.userId(),
                selectedPermissions: self.selectedPermissions()
            };

            if(self.selectedLocationType() == 'school'){
                submission.schoolId = self.selectedLocation();
            }else if(self.selectedLocationType() == 'district'){
                submission.districtId = self.selectedLocation();
            }

            //console.log(submission);

            $.ajax({
                url:'/admin/userPermission',
                type:'POST',
                data: submission
                }).done(function(response){
                    console.log('done!');
                    window.location = '/admin/'+self.userId()+'/permissions';
                });
        }
    };

    return self;
};
