var DistrictHomeModel = function(districtCode) {
    var self = this;

    self.loading = ko.observable(true);

    self.selected = ko.observable('Upcoming Schedule');

    self.showFilter = ko.observable(false);

    //put the bindings behind the visibility
    self.caseloadListRef = ko.observable(new TableModel({
        dataMethodPath: '/'+districtCode+'/caseload/list',
        rowModel: CaseloadListItem,
        defaultSortBy: 'studentLastName',
        filters: [{
            filterName: 'studentFirstName',
            filterValue: null
        }, {
            filterName: 'studentLastName',
            filterValue: null
        }, {
            filterName: 'districtStudentId',
            filterValue: null
        }, {
            filterName: 'schoolName',
            filterValue: null
        }, {
            filterName: 'service',
            filterValue: null
        }, {
            filterName: 'frequencyAndDuration',
            filterValue: null
        }, {
            filterName: 'myStudentsOnly',
            filterValue: 'Yes'
        }]
    }));

    self.upcomingListRef = ko.observable(new TableModel({
        dataMethodPath: '/'+districtCode+'/schedule/upcoming',
        rowModel: UpcomingModel,
        defaultSortBy: 'datetime',
        filters: [{
            filterName: 'startDate',
            filterValue: moment()
                .format("M/D/YYYY")
        }, {
            filterName: 'endDate',
            filterValue: moment()
                .add(7, 'days')
                .format('M/D/YYYY')
        }]

    }));


    self.unfinishedInThePast = ko.observable();
    self.unfinishedInThePastWatcher = ko.computed(function(){
        //nb: do not delete the unwrap of echo, that's the catch trigger beyond start date
        var page = self.upcomingListRef().selectedPage();
        var echo = self.upcomingListRef().echo();
        var startDate = self.upcomingListRef().filters().startDate();

        $.ajax({
            url: '/'+districtCode+'/schedule/unfinished',
            type:'POST',
            data: {startDate: startDate}
        }).done(function(response){
            console.log(response);
            if(!!response.value){
                self.unfinishedInThePast(moment(response.value.dateTime).format('M-D-YYYY'));
            }else{
                self.unfinishedInThePast(null);
            }
        });
    });
};
