var AreYouSureModel = function() {
    var self = this;
    self.modalVisible = ko.observable(false);
    self.yesCallback=null;
    self.noCallback = null;
    self.bodyQuestion = ko.observable("Are you sure?");

    self.show = function(bodyQuestion, yesCallback, noCallback) {
        self.yesCallback = yesCallback;
        self.noCallback = noCallback;
        if(!!bodyQuestion){
            self.bodyQuestion(bodyQuestion);
        }
        //alert('showing!');
        self.modalVisible(true);
    };
    self.action = function() {
        //alert('action');
        if(!!self.yesCallback){
            self.yesCallback();
        }
        self.modalVisible(false);
    };
};

var AreYouSure = null;
$(function() {
    AreYouSure = new AreYouSureModel();
    if($('#areYouSureModal').length){
    ko.applyBindings(AreYouSure, $('#areYouSureModal')
        .get(0));
    }else{
        //don't bind
    }
});
