var CaseloadListItemService = function(data) {
    var self = this;
    self.serviceId = ko.observable(data.serviceId);
    self.caseManager = ko.observable(data.caseManager);
    self.therapist = ko.observable(data.therapist);
    self.service = ko.observable(data.service);
    self.frequencyAndDuration = ko.observable(data.frequencyAndDuration);
    self.startDate = ko.observable(data.startDate);
    self.endDate = ko.observable(data.endDate);
};
var CaseloadListItem = function(data) {
    var self = this;
    self.studentFirstName = ko.observable(data.studentFirstName);
    self.studentLastName = ko.observable(data.studentLastName);
    self.schoolName = ko.observable(data.schoolName);
    self.studentId = ko.observable(data.studentId);
    self.districtStudentId = ko.observable(data.districtStudentId);
    self.studentFullName = ko.computed(function() {
        return self.studentFirstName() + ' ' + self.studentLastName();
    });
    self.displayName = ko.computed(function() {
        return ("{fullName} ({studentId})")
            .namedFormat({
                fullName: self.studentFullName(),
                studentId: self.districtStudentId()
            });
    });

    //service data
    self.services = ko.observableArray(ko.utils.arrayMap(data.services, function(service) {
        return new CaseloadListItemService(service);
    }));

};
