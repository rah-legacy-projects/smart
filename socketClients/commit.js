var socketio = require('socket.io'),
    _ = require('underscore'),
    logger = require('winston'),
    redis = require('redis'),
    uuid = require('node-uuid'),
    util = require('util'),
    spreadsheetImport = require('spreadsheet-import');

var localstore = redis.createClient();
module.exports = function(io) {
    logger.silly('making a commit server socket');
    io.of('/commit')
        .on('connection', function(socket) {
            socket.on('commit start', function(type, token, startOfDay, endOfDay, timezone) {
                logger.info('starting commit for type %s for document %s', type, token);

                var commitXLSX = new spreadsheetImport.commit();

                commitXLSX.on('commit row progress', function(e) {
                    logger.debug('row ' + e.current + '/' + e.total);
                    socket.emit('commit row progress', e);
                });

                commitXLSX.on('commit error', function(e) {
                    logger.error(util.inspect(e));
                    socket.emit('commit error', e);
                });

                //get the imported document data out of redis
                localstore.get(token, function(err, excelProcessData) {
                    if (!!err) {
                        logger.error('there was a problem committing: ' + err);
                        socket.emit('commit finish');
                    } else {
                        var _excelProcessData = JSON.parse(excelProcessData);
                        logger.silly(excelProcessData);
                        logger.info('committing for type ' + type);
                        var deftype = new require('../importDefinitions/' + type)(null, timezone);

                        commitXLSX.process(deftype, _excelProcessData.importData[type], token, function(err, thing) {
                            logger.debug('commit complete');
                            socket.emit('commit finish');
                        });
                    }
                });
            });
        });
}
