var socketio = require('socket.io'),
    _ = require('underscore'),
    logger = require('winston'),
    redis = require('redis'),
    uuid = require('node-uuid'),
    util = require('util'),
    xlsx = require('node-xlsx'),
    path = require('path'),
    uuid = require('uuid'),
    fs = require('fs'),
    spreadsheetImport = require('spreadsheet-import'),
    xlsxutil = spreadsheetImport.util,
    EventEmitter = require('events')
        .EventEmitter;


var localstore = redis.createClient();
module.exports = function(io) {
    logger.debug('starting listening for export sockets');
    io.of('/export')
        .on('connection', function(socket) {
            logger.debug('connection in export!');

            var _type = null;
            var _token = null;
            var _excelProcessData = null;

            var exportXLSX = new spreadsheetImport.export(null, null);

            exportXLSX.on('sheet warning', function(e) {
                logger.debug(e.message);
                socket.emit('sheet warning', e);
            });

            exportXLSX.on('sheet info', function(e) {
                logger.debug(e.message);
                socket.emit('sheet info', e);
            });

            exportXLSX.on('row validation error', function(e) {
                logger.debug(e.message);
                socket.emit('row validation error', e);
            });

            exportXLSX.on('sheet validation error', function(e) {
                logger.debug(e.message);
                socket.emit('sheet validation error', e);
            });

            exportXLSX.on('sheet progress', function(e) {
                logger.debug("sheet " + e.current + '/' + e.total);
                socket.emit('sheet progress', e);
            });

            exportXLSX.on('row progress', function(e) {
                logger.debug('row ' + e.current + '/' + e.total);
                socket.emit('row progress', e);
            });

            exportXLSX.on('workbook complete', function(e) {
                logger.info('finished ' + _type);
                localstore.get(_token, function(err, excelProcessData) {
                    _excelProcessData = JSON.parse(excelProcessData);
                    //hack: do we need excelSheets from export anywhere besides in the util call below...?
                    //_excelProcessData.excelSheets = exportXLSX.excelSheets;
                    _excelProcessData.exportData = exportXLSX.exportData;


                    //build output file path
                    _excelProcessData.outputFilePath = path.join(path.dirname(require.main.filename), "downloaded-files/") + _token + '.xlsx';

                    //write the output file with helpsheets
                    var worksheets = [];

                    worksheets = worksheets.concat(exportXLSX.excelSheets);
                    logger.silly('sc: excel sheet length: ' + worksheets.length);
                    logger.silly('sc: excel sheet data length: ' + worksheets[0].data.length);

                    xlsxutil.save(worksheets, _excelProcessData.outputFilePath, function() {
                        logger.info(_excelProcessData.outputFilePath + " written");
                        localstore.set(_token, JSON.stringify(_excelProcessData));
                        socket.emit('export finish', {
                            token: _token
                        });
                    });
                });
            });

            socket.on('error', function(err) {
                logger.error('error in export');
                logger.error(util.inspect(err));
            });

            socket.on('export start', function(type, parameters) {
                logger.info('starting export for type %s', type);
                logger.debug('parameters for export: %s', util.inspect(parameters));
                _type = type;
                _token = uuid.v4();
                localstore.set(_token, JSON.stringify({
                    parameters: parameters,
                    type: _type
                    //exportData: exportXLSX.exportData
                }));

                socket.emit('export setToken', _token);

                //send the request and start processing!
                exportXLSX.process(new(require('../exportDefinitions/' + _type))(),
                    parameters, function(err, sheets) {

                        logger.debug('export finish callback');
                    });
            });
        });
}
