var socketio = require('socket.io'),
    _ = require('underscore'),
    logger = require('winston'),
    redis = require('redis'),
    uuid = require('node-uuid'),
    ss = require('socket.io-stream'),
    util = require('util'),
    path = require('path'),
    fs = require('fs');

var localstore = redis.createClient();
module.exports = function(io) {
    logger.debug('setting up upload socket');
    io.of('/upload')
        .on('connection', function(socket) {
            logger.debug('connection on upload!');

            ss(socket)
                .on('upload', function(stream, data) {
                    var filename = path.basename(data.name);
                    logger.silly("received " + filename);
                    var uploadToken = uuid.v4();

                    if(!fs.existsSync('uploaded-files')){
                        fs.mkdirSync('uploaded-files');
                    }

                    var uploadName = path.join(path.dirname(require.main.filename), "uploaded-files", uploadToken) + path.extname(filename);


                    //var uploadName = path.join('uploaded-files', uploadToken) + path.extname(filename);
                    localstore.set(uploadToken, JSON.stringify({
                        uploadName: uploadName
                    }));
                    logger.silly('upload name: ' + uploadName);
                    socket.emit('upload setToken', uploadToken);
                    stream.pipe(fs.createWriteStream(uploadName));
                });
        });
};
