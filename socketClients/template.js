var socketio = require('socket.io'),
    _ = require('underscore'),
    logger = require('winston'),
    redis = require('redis'),
    uuid = require('node-uuid'),
    path = require('path'),
    util = require('util'),
    fs = require('fs'),
    xlsx = require('node-xlsx'),
    spreadsheetImport = require('spreadsheet-import'),
    xlsxutil = spreadsheetImport.util;

var localstore = redis.createClient();
logger.silly('connecting to excel importer');

module.exports = function(io) {
    logger.debug('starting listening for template sockets');
    io.of('/template')
        .on('connection', function(socket) {
            logger.info('template socket connection made');

            var _templateToken = null;
            var _type = null;

            socket.on('template start', function(type, extension) {
                logger.info('starting template creation for type %s, extension %s ', type, extension);

                if (!(/^\./.test(extension))) {
                    //add the dot to extension
                    logger.silly('adding dot to extension');
                    extension = "." + extension;
                }

                //set up local token, local type and document location
                _templateToken = uuid.v4();
                _type = type;
                var documentLocation = path.join(path.dirname(require.main.filename), "downloaded-files/") + _templateToken + extension;
                logger.silly('doc location: ' + documentLocation);
                var def = new require('../importDefinitions/' + type)();
                logger.silly(util.inspect(def.sheet));
                var templateXLSX = new spreadsheetImport.template(def.sheet);
                var helpXLSX = new spreadsheetImport.help(def.sheet);

                var worksheets = [];
                templateXLSX.process(def.sheet, function(err, templateSheets) {
                    helpXLSX.process(def.sheet, function(err, helpSheets) {
                        worksheets = worksheets.concat(templateSheets);
                        worksheets = worksheets.concat(helpSheets);
                        var buffer = xlsx.build({
                            worksheets: worksheets
                        });
                        xlsxutil.save(worksheets, documentLocation, function() {
                            localstore.set(_templateToken, documentLocation);
                            socket.emit('template finishEvent', _templateToken);
                        });
                    });
                });
            });
        })
};
