var socketio = require('socket.io'),
    _ = require('underscore'),
    logger = require('winston'),
    redis = require('redis'),
    uuid = require('node-uuid'),
    util = require('util'),
    xlsx = require('node-xlsx'),
    path = require('path'),
    fs = require('fs'),
    spreadsheetImport = require('spreadsheet-import'),
    xlsxutil = spreadsheetImport.util,
    EventEmitter = require('events')
    .EventEmitter,
    pdfExtract = require('pdf-extract'),
    RemittanceExtract = require('../utilities/remittance/remittanceExtract');

var localstore = redis.createClient();
module.exports = function(io) {
    io.of('/remittance')
        .on('connection', function(socket) {
            logger.debug('connection in remittance!');

            var _type = null;
            var _token = null;
            var _excelProcessData = null;

            socket.on('error', function(err) {
                logger.error('error in import');
                logger.error(util.inspect(err));
            });

            socket.on('import start', function(type, token, timezoneOffset) {
                logger.silly('offset: ' + timezoneOffset);
                logger.info('starting import for type %s for document %s', type, token);
                _type = type;
                _token = token;
                //get the document data (so far) out of redis
                localstore.get(_token, function(err, excelProcessData) {
                    _excelProcessData = JSON.parse(excelProcessData);
                    _excelProcessData.type = _type;

                    var fileName = path.basename(_excelProcessData.uploadName, '.pdf');
                    _excelProcessData.outputFilePath = path.join('downloaded-files', fileName + '.xlsx');
                    logger.silly('output path: ' + _excelProcessData.outputFilePath);

                    localstore.set(_token, JSON.stringify(_excelProcessData));


                    localstore.set(_token, JSON.stringify(_excelProcessData));
                    //hack: suck the PDF in by text to determine # pages first
                    socket.emit('import message', {message: 'Initializing PDF reader.  This may take a moment.'});
                    var p = pdfExtract(_excelProcessData.uploadName, {
                        type: 'text'
                    }, function(err) {});
                    p.on('complete', function(data) {
                        socket.emit('import message', {message: 'Initialized.'});
                        var totalPages = data.text_pages.length;
                        var remittanceExtract = new RemittanceExtract();
                        remittanceExtract.on('pdf page', function(p) {
                            socket.emit('import pageEvent', {
                                page: p.index,
                                totalPages: totalPages
                            });
                        });

                        remittanceExtract.on('xl start', function(x){
                            socket.emit('import message', {message: 'Spooling data out to excel.  This may take a moment.'});
                        });

                        remittanceExtract.on('xl sheet complete', function(x){
                            socket.emit('import message', {message: x.sheet + ' done.'});
                        });

                        remittanceExtract.start(_excelProcessData.uploadName,
                            _excelProcessData.outputFilePath,
                            function(err) {
                                socket.emit('import message', {message: 'Spooled.'});
                                socket.emit('import finish');
                            });
                    });
                });
            });
        });
};
