var socketio = require('socket.io'),
    _ = require('lodash'),
    logger = require('winston'),
    redis = require('redis'),
    uuid = require('node-uuid'),
    util = require('util'),
    xlsx = require('node-xlsx'),
    path = require('path'),
    uuid = require('uuid'),
    fs = require('fs'),
    async = require('async'),
    moment = require('moment'),
    AdmZip = require('adm-zip'),
    billing = require('../billing'),
    spreadsheetImport = require('spreadsheet-import'),
    xlsxutil = spreadsheetImport.util,
    EventEmitter = require('events')
    .EventEmitter;

require('../util')();


var localstore = redis.createClient();
module.exports = function(io) {
    logger.debug('starting listening for export sockets');
    io.of('/billing')
        .on('connection', function(socket) {

            // {{{ hack: put billing stuff in it's own function with a callback so it can be used by both dry and wet runs
            var billfunc = function(token, parameters, cb) {
                logger.silly('bill func parameters: ' + util.inspect(parameters));

                var exportXLSX = new spreadsheetImport.export(null, null);

                exportXLSX.on('sheet warning', function(e) {
                    logger.debug(e.message);
                    socket.emit('sheet warning', e);
                });

                exportXLSX.on('sheet info', function(e) {
                    logger.debug(e.message);
                    socket.emit('sheet info', e);
                });

                exportXLSX.on('row validation error', function(e) {
                    logger.debug(e.message);
                    socket.emit('row validation error', e);
                });

                exportXLSX.on('sheet validation error', function(e) {
                    logger.debug(e.message);
                    socket.emit('sheet validation error', e);
                });

                exportXLSX.on('sheet progress', function(e) {
                    logger.debug("sheet " + e.current + '/' + e.total);
                    socket.emit('sheet progress', e);
                });

                exportXLSX.on('row progress', function(e) {
                    logger.debug('row ' + e.current + '/' + e.total);
                    socket.emit('row progress', e);
                });



                //kick off billing dry run
                billing.billable(parameters, function(err, flats) {
                    billing.converter(parameters, flats, function(err, billingFileContents) {

                        //make exports for successes and rejections
                        var successExport = new(require('../exportDefinitions/billing'))();
                        var rejectedExport = new(require('../exportDefinitions/billing'))();
                        //hack: use the export data to bubble up converted flats data
                        var timezone = parameters.timezone;
                        successExport.exportData = function(parameters, callback) {

                            var successFlats = _.filter(flats, function(flat) {
                                return flat.errors.length == 0;
                            });
                            var exportFlats = _.map(successFlats, function(flat) {
                                return {
                                    studentName: flat.student.firstName + ' ' + flat.student.lastName,
                                    studentId: flat.student.studentId,
                                    districtName: flat.district.name,
                                    districtCode: flat.district.districtCode,
                                    therapistName: flat.finalizingUser.firstName + ' ' + flat.finalizingUser.lastName,
                                    therapistNPI: flat.finalizingUser.therapistQualifications.npiNumber,
                                    sessionTime: moment.tz(flat.dateTime, timezone)
                                        .format('hh:mm a'),
                                    sessionDate: moment.tz(flat.dateTime, timezone)
                                        .format('MM/DD/YYYY'),
                                    therapy: flat.serviceType,
                                    billingErrors: '',
                                    echoId: flat.groupId || ''
                                };
                            });
                            logger.silly('export flats made for success');
                            callback(null, exportFlats);
                        };
                        //hack: mark success as success
                        successExport.sheet.sheetName = 'Success - ' + successExport.sheet.sheetName;

                        rejectedExport.exportData = function(parameters, callback) {

                            var rejectFlats = _.filter(flats, function(flat) {
                                return flat.errors.length > 0;
                            });
                            var exportFlats = _.map(rejectFlats, function(reject) {
                                var flat = reject;
                                var rejectionMessage = _.map(flat.errors, function(e) {
                                        return e.message;
                                    })
                                    .join(require('os')
                                        .EOL);

                                var npiNumber = null;
                                if(!!flat.finalizingUser && !!flat.finalizingUser.therapistQualifications){
                                    npiNumber = flat.finalizingUser.therapistQualifications.npiNumber;
                                }
                                else if(!!flat.modifiedByUser){
                                    npiNumber = flat.modifiedByUser.therapistQualifications.npiNumber;
                                }
                                else if (!!flat.sessionProvider && !!flat.sessionProvider.therapistQualifications) {
                                    npiNumber = flat.sessionProvider.therapistQualifications.npiNumber;
                                }

                                var displayUser = flat.finalizingUser || flat.modifiedByUser ||  flat.sessionProvider;

                                return {
                                    studentName: flat.unflat.studentName,
                                    studentId: flat.unflat.studentId,
                                    districtName: flat.unflat.districtName,
                                    districtCode: flat.unflat.districtCode,
                                    therapistName: displayUser.firstName + ' ' + displayUser.lastName,
                                    therapistNPI: npiNumber,
                                    sessionTime: moment.tz(flat.dateTime, timezone)
                                        .format('hh:mm a'),
                                    sessionDate: moment.tz(flat.dateTime, timezone)
                                        .format('MM/DD/YYYY'),
                                    therapy: flat.serviceType,
                                    billingErrors: rejectionMessage,
                                    echoId: flat.groupId || ''
                                };
                            });

                            logger.silly('export flats made for rejects');
                            callback(null, exportFlats);
                        };
                        //hack: mark reject as reject
                        rejectedExport.sheet.sheetName = 'Rejections - ' + rejectedExport.sheet.sheetName;

                        logger.silly('success and rejection export set up');


                        //process the success and rejections
                        async.series({
                            success: function(cb) {
                                exportXLSX.process(successExport, null, function(err, sheets) {
                                    logger.silly('success sheets: ' + sheets.length);
                                    cb(err, sheets);
                                });
                            },
                            reject: function(cb) {
                                exportXLSX.process(rejectedExport, null, function(err, sheets) {
                                    logger.silly('reject sheets: ' + sheets.length);
                                    cb(err, sheets);
                                });
                            }
                        }, function(err, r) {
                            var worksheets = [];
                            logger.silly(r.success.length + ', ' + r.reject.length);
                            //hack: r.success points back to exportxlsx.sheets, which has both rejects and successes
                            worksheets = worksheets.concat(r.success);
                            //worksheets = worksheets.concat(r.reject);
                            //save the xl sheets
                            var basepath = path.join(path.dirname(require.main.filename), "downloaded-files/") + token;


                            xlsxutil.save(worksheets, basepath + '.xlsx', function() {
                                logger.info(basepath + '.xlsx' + " written");

                                fs.writeFileSync(basepath + '.txt', billingFileContents);

                                var prettyBaseNameFormat = '{startDate}_{endDate}.{extension}';
                                var xlsxName = prettyBaseNameFormat.format({
                                    startDate: moment.tz(parameters.startDate, parameters.timezone)
                                        .format('MM-DD-YYYY'),
                                    endDate: moment.tz(parameters.endDate, parameters.timezone)
                                        .format('MM-DD-YYYY'),
                                    extension: 'xlsx'
                                });

                                var billingName = prettyBaseNameFormat.format({
                                    startDate: moment.tz(parameters.startDate, parameters.timezone)
                                        .format('MM-DD-YYYY'),
                                    endDate: moment.tz(parameters.endDate, parameters.timezone)
                                        .format('MM-DD-YYYY'),
                                    extension: 'txt'
                                });

                                //zip the xl and txt file
                                var zip = new AdmZip();
                                zip.addFile(xlsxName, new Buffer(fs.readFileSync(basepath + '.xlsx')));
                                zip.addFile(billingName, new Buffer(fs.readFileSync(basepath + '.txt')));
                                zip.writeZip(basepath + '.zip');

                                cb(null, flats);
                            });
                        });
                    });
                });

            };
            //}}}

            logger.debug('connection in export in billing! BILLING!');

            var _type = null;
            var _token = null;
            var _excelProcessData = null;

            socket.on('error', function(err) {
                logger.error('error in export');
                logger.error(util.inspect(err));
            });

            socket.on('export start', function(type, parameters) {
                logger.info('starting export for type %s', type);
                logger.debug('parameters for export: %s', util.inspect(parameters));
                var _type = type;
                var _token = uuid.v4();

                socket.emit('export setToken', _token);
                logger.silly('token set');
                localstore.set(_token, JSON.stringify({
                    parameters: parameters,
                    type: _type
                    //exportData: exportXLSX.exportData
                }));

                billfunc(_token, parameters, function(err, flats) {
                    socket.emit('export finish', {
                        token: _token
                    });
                });

            });

            socket.on('export commit billing', function(token, parameters) {
                billfunc(token, parameters, function(err, flats) {
                    logger.silly('about to mark as billed and rollup');
                    async.parallel({
                        markAsBilled: function(cb) {
                            billing.rollup.markAsBilled(flats, cb);
                        },
                        successRollup: function(cb) {
                            billing.rollup.success(flats, cb);
                        },
                        failureRollup: function(cb) {
                            billing.rollup.rejection(flats, cb);
                        }
                    }, function(err, r) {
                        socket.emit('export commit billing finish', {});
                    });

                });
            });


        });
}
