var socketio = require('socket.io'),
    _ = require('underscore'),
    logger = require('winston'),
    redis = require('redis'),
    uuid = require('node-uuid'),
    path = require('path'),
    util = require('util');

var localstore = redis.createClient();

module.exports = function(io) {
    logger.debug('starting listening for debug sockets');
    io.of('/debug')
        .on('connection', function(socket) {
            logger.info('debug socket connection made');

            socket.on('debug request', function(msg) {
                logger.silly('debug request!');
                logger.silly('message: ' + msg);
                socket.emit('debug response', {message: 'hello'});
            });
        })
};
