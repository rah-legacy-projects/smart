var socketio = require('socket.io'),
    _ = require('underscore'),
    logger = require('winston'),
    redis = require('redis'),
    uuid = require('node-uuid'),
    util = require('util'),
    xlsx = require('node-xlsx'),
    path = require('path'),
    fs = require('fs'),
    moment = require('moment-timezone'),

    spreadsheetImport = require('spreadsheet-import'),
    xlsxutil = spreadsheetImport.util,
    EventEmitter = require('events')
    .EventEmitter;

var localstore = redis.createClient();
module.exports = function(io) {
    logger.debug('starting listening for import sockets');
    io.of('/import')
        .on('connection', function(socket) {
            logger.debug('connection in import!');

            var _type = null;
            var _token = null;
            var _excelProcessData = null;

            var importXLSX = new spreadsheetImport.import(null, null),
                helpXLSX = new spreadsheetImport.help(null),
                diffXLSX = new spreadsheetImport.diff();

            importXLSX.on('sheet warning', function(e) {
                logger.debug(e.message);
                socket.emit('sheet warning', e);
            });

            importXLSX.on('sheet info', function(e) {
                logger.debug(e.message);
                socket.emit('sheet info', e);
            });

            importXLSX.on('row validation error', function(e) {
                logger.debug(e.message);
                socket.emit('row validation error', e);
            });

            importXLSX.on('sheet validation error', function(e) {
                logger.debug(e.message);
                socket.emit('sheet validation error', e);
            });

            importXLSX.on('sheet progress', function(e) {
                logger.debug("sheet " + e.current + '/' + e.total);
                socket.emit('sheet progress', e);
            });

            importXLSX.on('row progress', function(e) {
                logger.debug('row ' + e.current + '/' + e.total);
                socket.emit('row progress', e);
            });

            importXLSX.on('workbook complete', function(e) {
                logger.info('finished ' + _type);
                localstore.get(_token, function(err, excelProcessData) {
                    _excelProcessData = JSON.parse(excelProcessData);
                    //hack: do we need excelSheets from import anywhere besides in the util call below...?
                    //_excelProcessData.excelSheets = importXLSX.excelSheets;
                    _excelProcessData.importData = importXLSX.importData;

                    var internalType = (require('../importDefinitions/' + _excelProcessData.type));
                    //set up diff of valid data
                    diffXLSX.process(
                        new internalType(),
                        importXLSX.importData[new internalType()
                            .sheet.className],
                        _token,
                        function(err, diffSheet) {
                            logger.silly('diffsheet: ' + util.inspect(diffSheet));
                            //set up help's members
                            helpXLSX.process(
                                new internalType()
                                .sheet, function(err, helpSheets) {
                                    logger.silly('helpsheet made');

                                    //build output file path
                                    _excelProcessData.outputFilePath = path.join(path.dirname(require.main.filename), "downloaded-files/") + _token + path.extname(_excelProcessData.uploadName);

                                    //write the output file with helpsheets
                                    var worksheets = [];
                                    logger.silly('about to concat worksheets');

                                    logger.silly(util.inspect(importXLSX.excelSheets));

                                    worksheets = worksheets.concat(importXLSX.excelSheets);
                                    logger.silly('concatted import');
                                    if (!!diffSheet && !!diffSheet.data && diffSheet.data.length > 0) {
                                        worksheets.push(diffSheet);
                                        logger.silly('concatted diffsheet');
                                    }
                                    worksheets = worksheets.concat(helpSheets);
                                    logger.silly('concatted halpsheet');

                                    xlsxutil.save(worksheets, _excelProcessData.outputFilePath, function() {
                                        logger.info(_excelProcessData.outputFilePath + " written");
                                        localstore.set(_token, JSON.stringify(_excelProcessData));
                                        socket.emit('import finish', {
                                            token: _token
                                        });
                                    });
                                })
                        })
                });
            });

            socket.on('error', function(err) {
                logger.error('error in import');
                logger.error(util.inspect(err));
            });

            socket.on('import start', function(type, token, startOfDay, endOfDay, timezone) {
                var test = moment();
                logger.silly('now is: ' + test.toString());
                logger.silly('adjusted for tz is: ' + test.tz(timezone).toString());
                logger.info('starting import for type %s for document %s', type, token);
                logger.silly('start/end: %s %s', startOfDay, endOfDay);
                _type = type;
                _token = token;
                //get the document data (so far) out of redis
                localstore.get(_token, function(err, excelProcessData) {
                    _excelProcessData = JSON.parse(excelProcessData);
                    _excelProcessData.type = _type;

                    localstore.set(_token, JSON.stringify(_excelProcessData));
                    //send the request and start processing!
                    importXLSX.process(new(require('../importDefinitions/' + _type))(null, timezone)
                        .sheet, _token, _excelProcessData.uploadName, function(err, sheets) {
                            logger.debug('import finish callback');
                        });
                });
            });
        });
};
